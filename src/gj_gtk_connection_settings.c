/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_config.h"
#include "gj_log.h"

#include "gj_gtk_connection_settings.h"
#include "gj_gtk_password_change.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	/*   GtkWidget *combo_server; */
	/*   GtkWidget *combo_entry_server; */
	GtkWidget *entry_server; 
	GtkWidget *button_server; 
	GtkWidget *spinbutton_port;
	GtkWidget *entry_username;
	GtkWidget *entry_password;

	GtkWidget *entry_resource;
	GtkWidget *spinbutton_priority;
  
	GtkWidget *checkbutton_prompt_for_password;
	GtkWidget *checkbutton_use_plain_text_password;
	GtkWidget *checkbutton_use_ssl;

	GtkWidget *checkbutton_auto_connect_on_startup;
	/*   GtkWidget *checkbutton_auto_reconnect_on_disconnect; */

	GtkWidget *button_ok;
	GtkWidget *button_cancel;

} GjConnectionSettingsDialog;


/* get/set preferences */
static gboolean gj_gtk_cs_open (GjConnectionSettingsDialog *dialog);
static gboolean gj_gtk_cs_save (GjConnectionSettingsDialog *dialog);

static void gj_gtk_cs_server_cb (GtkMenuItem *menuitem, 
				 GjConnectionSettingsDialog *dialog);

static void gj_gtk_cs_server_align_cb (GtkMenu *menu, 
				       gint *x, 
				       gint *y, 
				       gboolean *push_in, 
				       GjConnectionSettingsDialog *dialog);

/*
 * gui callbacks
 */ 
static void on_checkbutton_prompt_for_password_toggled (GtkToggleButton *togglebutton, GjConnectionSettingsDialog *dialog);
static void on_button_server_clicked (GtkButton *button, GjConnectionSettingsDialog *dialog);

static void on_destroy (GtkWidget *widget, GjConnectionSettingsDialog *dialog);
static void on_response (GtkDialog *widget,  gint response, GjConnectionSettingsDialog *dialog);


static GjConnectionSettingsDialog *current_dialog = NULL;


gboolean gj_gtk_cs_load ()
{
	GjConnectionSettingsDialog *dialog = NULL;
	GladeXML *xml = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	dialog = g_new0 (GjConnectionSettingsDialog, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "connection_settings",
				     NULL,
				     "connection_settings", &dialog->window,
				     "entry_server", &dialog->entry_server,
				     "button_server", &dialog->button_server,
				     "spinbutton_port", &dialog->spinbutton_port,
				     "entry_username", &dialog->entry_username,
				     "entry_password", &dialog->entry_password,
				     "entry_resource", &dialog->entry_resource,
				     "spinbutton_priority", &dialog->spinbutton_priority,
				     "checkbutton_prompt_for_password", &dialog->checkbutton_prompt_for_password,
				     "checkbutton_use_plain_text_password", &dialog->checkbutton_use_plain_text_password,
				     "checkbutton_use_ssl", &dialog->checkbutton_use_ssl,
				     "checkbutton_auto_connect_on_startup", &dialog->checkbutton_auto_connect_on_startup,
				     /* 			      "checkbutton_auto_reconnect_on_disconnect", &dialog->checkbutton_auto_reconnect_on_disconnect, */
				     "button_ok", &dialog->button_ok,
				     "button_cancel", &dialog->button_cancel,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "connection_settings", "response", on_response,
			      "connection_settings", "destroy", on_destroy,
			      "button_server", "clicked", on_button_server_clicked,
			      "checkbutton_prompt_for_password", "toggled", on_checkbutton_prompt_for_password_toggled,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);


	/* get settings */
	gj_gtk_cs_open (dialog);

	return TRUE;
}

static gboolean gj_gtk_cs_save (GjConnectionSettingsDialog *dialog)
{
	G_CONST_RETURN gchar *server = NULL;
	gint16 port = 0;
	G_CONST_RETURN gchar *username = NULL;
	G_CONST_RETURN gchar *password = NULL;

	G_CONST_RETURN gchar *resource = NULL;
	gint8 priority = 0;

	gboolean prompt_for_password = FALSE;
	gboolean use_plain_text_password = FALSE;
	gboolean use_ssl = FALSE;
	gboolean auto_connect_on_startup = FALSE;
	/*     gboolean auto_reconnect_on_disconnect = FALSE; */

	/* get values */
	server = gtk_entry_get_text (GTK_ENTRY (dialog->entry_server));
	port = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_port));
	username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));
	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password));

	resource = gtk_entry_get_text (GTK_ENTRY (dialog->entry_resource));
	priority = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_priority));

	prompt_for_password = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_prompt_for_password));
	use_plain_text_password = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_plain_text_password));
	use_ssl = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_ssl));
    
	auto_connect_on_startup = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_auto_connect_on_startup));
	/*     auto_reconnect_on_disconnect = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_auto_reconnect_on_disconnect)); */

	/* check values */
	if (server == NULL || strlen (server) < 1) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}
    
	if (port < 1) {
		g_warning ("%s: port was < 1", __FUNCTION__);
		return FALSE;
	}
    
	if (username == NULL || strlen (username) < 1) {
		g_warning ("%s: username was NULL", __FUNCTION__);
		return FALSE;
	}  
    
	if (password == NULL || strlen (password) < 1) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return FALSE;
	}
    
	/* set config */
	gj_config_set_cs_server (server);
	gj_config_set_cs_port (port);
	gj_config_set_cs_username (username);
	gj_config_set_cs_password (password);
    
	gj_config_set_cs_resource (resource);
	gj_config_set_cs_priority (priority);
    
	gj_config_set_cs_prompt_for_password (prompt_for_password); 
	gj_config_set_cs_use_plain_text_password (use_plain_text_password);
	gj_config_set_cs_use_ssl (use_ssl);
	gj_config_set_cs_auto_connect_on_startup (auto_connect_on_startup);
	/*     gj_config_set_cs_auto_reconnect_on_disconnect (auto_reconnect_on_disconnect);   */

	/* save */
	if (gj_config_write () == FALSE) {
		g_warning ("%s: failed to write config", __FUNCTION__);
		return FALSE;
	}

	return TRUE;
}

static gboolean gj_gtk_cs_open (GjConnectionSettingsDialog *dialog)
{
	/* load */ 
	gj_config_read ();

	/* set to widgets */
	if (gj_config_get_cs_server () != NULL)
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_server), gj_config_get_cs_server ());

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton_port), 
				   (gdouble)gj_config_get_cs_port ());

	if (gj_config_get_cs_username () != NULL)
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_username), gj_config_get_cs_username ());
	if (gj_config_get_cs_password () != NULL)
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_password), gj_config_get_cs_password ());

	if (gj_config_get_cs_resource () != NULL)
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_resource), gj_config_get_cs_resource ());

	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton_priority), 
				   (gdouble)gj_config_get_cs_priority ());

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_prompt_for_password), 
				      gj_config_get_cs_prompt_for_password ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_plain_text_password), 
				      gj_config_get_cs_use_plain_text_password ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_ssl), 
				      gj_config_get_cs_use_ssl ());
  
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_auto_connect_on_startup), 
				      gj_config_get_cs_auto_connect_on_startup ());
	/*   gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_auto_reconnect_on_disconnect),  */
	/* 			       gj_config_get_cs_auto_reconnect_on_disconnect ()); */
  
	return TRUE;
}

static void gj_gtk_cs_server_cb (GtkMenuItem *menuitem, GjConnectionSettingsDialog *dialog)
{
	const gchar **server_listing = NULL;
	const gchar *server = NULL;

	gpointer pid;
	gint id;

	server_listing = gnome_jabber_get_server_list ();

	if (!server_listing)
		return;

	pid = g_object_get_data (G_OBJECT (menuitem), "id");
	id = GPOINTER_TO_INT (pid);
  
	server = server_listing[id];

	if (server)
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_server), server);
}

static void gj_gtk_cs_server_align_cb (GtkMenu *menu, 
				       gint *x, 
				       gint *y, 
				       gboolean *push_in, 
				       GjConnectionSettingsDialog *dialog)
{
	GtkWidget *button = NULL;
	GtkRequisition req;
	GdkScreen *screen = NULL;
	gint width = 0;
	gint height = 0;
	gint screen_height = 0;
  
	button = dialog->button_server;
	
	gtk_widget_size_request (GTK_WIDGET (menu), &req);
  
	gdk_window_get_origin (GTK_BUTTON (button)->event_window, x, y);
	gdk_drawable_get_size (GTK_BUTTON (button)->event_window, &width, &height);
  
	*x -= req.width - width;
	*y += height;
  
	screen = gtk_widget_get_screen (GTK_WIDGET (menu));
  
	/* Clamp to screen size. */
	screen_height = gdk_screen_get_height (screen) - *y;	

	if (req.height > screen_height) {
		/* It doesn't fit, so we see if we have the minimum space needed. */
		if (req.height > screen_height && *y - height > screen_height) {
			/* Put the menu above the button instead. */
			screen_height = *y - height;
			*y -= (req.height + height);

			if (*y < 0) 
				*y = 0;
		}
	}
}

/*
 * GUI events
 */
static void on_checkbutton_prompt_for_password_toggled (GtkToggleButton *togglebutton, GjConnectionSettingsDialog *dialog)
{
	/*   gtk_widget_set_sensitive (GTK_WIDGET (dialog->checkbutton_auto_reconnect_on_disconnect),  */
	/* 			   gtk_toggle_button_get_active (togglebutton)); */
}

static void on_button_server_clicked (GtkButton *button, GjConnectionSettingsDialog *dialog)
{
	GtkWidget *menu = NULL;

	GList *servers = NULL;
	const gchar **server_listing = NULL;
	const gchar *str = NULL;
	gint i = 0;

	server_listing = gnome_jabber_get_server_list ();

	menu = gtk_menu_new ();
  
	/* set a limit of a 1000 */
	while ((str = server_listing[i++]) != NULL && i < 1000)
		{
			GtkWidget *item = NULL;

			if (!str)
				continue;

			item = gtk_menu_item_new_with_label (str);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 

			g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (i - 1));
      
			g_signal_connect ((gpointer) item, "activate", 
					  G_CALLBACK (gj_gtk_cs_server_cb), dialog);
		}

	gtk_widget_show_all (menu);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, 
			(GtkMenuPositionFunc)gj_gtk_cs_server_align_cb,
			dialog, 1, gtk_get_current_event_time ());
  

	/* cleanup */
	g_list_free (servers);
}

static void on_destroy (GtkWidget *widget, GjConnectionSettingsDialog *dialog)
{
	current_dialog = NULL;
 
	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjConnectionSettingsDialog *dialog)
{
	if (response == GTK_RESPONSE_OK)
		gj_gtk_cs_save (dialog);
      
	gtk_widget_destroy (dialog->window);
}


#ifdef __cplusplus
}
#endif
