/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_jid.h"
#include "gj_connection.h"

#include "gj_gtk_rename_group.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry;
	GtkWidget *label;

	GtkWidget *button_ok;

	/* member data */
	GjConnection c;
	GjRoster roster;

	gchar *current_group;

} GjRenameGroupDialog;


static gboolean gj_gtk_rg_save (GjRenameGroupDialog *dialog);
     
/*
 * internal callbacks 
 */
static void gj_gtk_rg_user_changed_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

/*
 * gui functions 
 */
static void on_entry_changed (GtkEditable *editable, GjRenameGroupDialog *dialog);
static void on_entry_activate (GtkEntry *entry, GjRenameGroupDialog *dialog);

static void on_destroy (GtkWidget *widget, GjRenameGroupDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjRenameGroupDialog *dialog);


static GjRenameGroupDialog *current_dialog = NULL;


/*
 * window functions
 */
gboolean gj_gtk_rg_load (GjConnection c, GjRoster r, const gchar *group)
{
	GladeXML *xml = NULL;
	GjRenameGroupDialog *dialog = NULL;

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (group == NULL || g_utf8_strlen (group, -1) < 1) {
		g_warning ("%s: group was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjRenameGroupDialog, 1);

	dialog->c = gj_connection_ref (c);

	dialog->current_group = g_strdup (group);
	dialog->roster = r;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "rename_group",
				     NULL,
				     "rename_group", &dialog->window,
				     "entry", &dialog->entry,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "rename_group", "response", on_response,
			      "rename_group", "destroy", on_destroy,
			      "entry", "changed", on_entry_changed,
			      "entry", "activate", on_entry_activate,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set up window */
	gtk_entry_set_text (GTK_ENTRY (dialog->entry), group);
	gtk_widget_grab_focus (GTK_WIDGET (dialog->entry));

	return TRUE;
}

static gboolean gj_gtk_rg_save (GjRenameGroupDialog *dialog)
{
	GList *list = NULL;
	gint i = 0;

	G_CONST_RETURN gchar *new_group = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	new_group = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	if ((list = gj_roster_get_contacts_by_group (dialog->roster, dialog->current_group)) == NULL)
		return FALSE;

	for (i=0; i<g_list_length (list); i++) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;

		ri = (GjRosterItem) g_list_nth_data (list, i);

		if (ri == NULL) {
			continue;
		}

		j = gj_roster_item_get_jid (ri);

		if (j == NULL) {
			continue;
		}
      
		if (gj_roster_item_groups_del (ri, dialog->current_group) == FALSE) {
			continue;
		}

		if (gj_roster_item_groups_add (ri, (gchar*)new_group) == FALSE) {
			continue;  
		}

		/* using the list, modify all the users groups to include the new group */
		gj_iq_request_user_change (dialog->c,
					   gj_jid_get_full (j), 
					   gj_roster_item_get_name (ri), 
					   gj_roster_item_get_groups (ri),
					   gj_gtk_rg_user_changed_response,
					   NULL);
	}
      
	return TRUE;
}

/* callbacks */
static void gj_gtk_rg_user_changed_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: response!", __FUNCTION__);
}


/*
 * GUI events
 */
static void on_entry_changed (GtkEditable *editable, GjRenameGroupDialog *dialog)
{
	gchar *group = NULL;

	group = gtk_editable_get_chars (editable, 0, -1);

	if (group != NULL && g_utf8_strlen (group, -1) > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), TRUE);
		g_free (group);
		return;
	}
      
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), FALSE);
}

static void on_entry_activate (GtkEntry *entry, GjRenameGroupDialog *dialog)
{
	G_CONST_RETURN gchar *group = NULL;

	group = gtk_entry_get_text (entry);

	if (group != NULL && g_utf8_strlen (group, -1) > 0)
		on_response (GTK_DIALOG (dialog->window), GTK_RESPONSE_OK, dialog); 
}

static void on_destroy (GtkWidget *widget, GjRenameGroupDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);
	
	g_free (dialog->current_group);

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjRenameGroupDialog *dialog)
{
	if (response == GTK_RESPONSE_OK) {
		gj_gtk_rg_save (dialog);
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
