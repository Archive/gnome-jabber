/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$ 
 *
 */
#ifndef __gj_register_h
#define __gj_register_h

#include "gj_typedefs.h"

GjConnection gj_register_get_connection (GjRegistration reg);
 
const gchar *gj_register_get_to (GjRegistration reg);
const gchar *gj_register_get_from (GjRegistration reg);

const gchar *gj_register_get_key (GjRegistration reg);
const gchar *gj_register_get_instructions (GjRegistration reg);

gboolean gj_register_get_require_username (GjRegistration reg);
gboolean gj_register_get_require_password (GjRegistration reg);
gboolean gj_register_get_require_email (GjRegistration reg);
gboolean gj_register_get_require_nickname (GjRegistration reg);


/*
 * actions
 */
gboolean gj_register_requirements (GjConnection c,
				   const gchar *server, 
				   GjRegistrationCallback cb,
				   gpointer user_data);

gboolean gj_register_request (GjConnection c,
			      const gchar *server, 
			      const gchar *key, 
			      const gchar *username, 
			      const gchar *password, 
			      const gchar *email, 
			      const gchar *nickname,
			      GjRegistrationCallback cb, 
			      gpointer user_data);

gboolean gj_register_remove (GjConnection c,
			     const gchar *server, 
			     const gchar *key, 
			     const gchar *username, 
			     const gchar *password, 
			     const gchar *email, 
			     const gchar *nickname,
			     GjRegistrationCallback cb, 
			     gpointer user_data);

#endif

