/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_agents.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"
#include "gj_jid.h"
#include "gj_connection.h"

#include "gj_gtk_edit_groups.h"
#include "gj_gtk_register_service.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *treeview;

	GtkWidget *label;

	GtkWidget *label_name;
	GtkWidget *label_jid;
	GtkWidget *label_resource;
	GtkWidget *label_agent;

	GtkWidget *entry_add;
	GtkWidget *button_add;
  
	GtkWidget *button_ok;

	/* member functions */
	GjConnection c;
	GjRosterItem ri;

	gboolean changes_made;

} GjEditGroupsDialog;


enum {
	COL_EDIT_GROUPS_NAME,
	COL_EDIT_GROUPS_ENABLED,
	COL_EDIT_GROUPS_VISIBLE,
	COL_EDIT_GROUPS_EDITABLE,
	COL_EDIT_GROUPS_COUNT
};


/* 
 * window functions 
 */
static gboolean gj_gtk_eg_setup (GjEditGroupsDialog *dialog);
static gboolean gj_gtk_eg_save (GjEditGroupsDialog *dialog);


/* 
 * model functions 
 */
static gboolean gj_gtk_eg_model_populate_columns (GjEditGroupsDialog *dialog);
static gboolean gj_gtk_eg_model_populate (GjEditGroupsDialog *dialog);

static GtkTreeIter *gj_gtk_eg_model_find_iter_by_name (GjEditGroupsDialog *dialog, const gchar *name);

/* 
 * internal callbacks 
 */
static void gj_gtk_eg_user_changed_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

/* 
 * gui callbacks 
 */
static void on_treeview_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjEditGroupsDialog *dialog);

static void on_entry_add_changed (GtkEditable *editable, GjEditGroupsDialog *dialog);
static void on_button_add_clicked (GtkMenuItem *menuitem, GjEditGroupsDialog *dialog);

static void on_destroy (GtkWidget *widget, GjEditGroupsDialog *dialog);
static void on_response (GtkDialog *window, gint response_id, GjEditGroupsDialog *dialog);


static GjEditGroupsDialog *current_dialog = NULL;


gboolean gj_gtk_eg_load (GjConnection c, 
			 GjRosterItem ri)
{
	GladeXML *xml = NULL;
	GjEditGroupsDialog *dialog = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjEditGroupsDialog, 1);

	dialog->c = gj_connection_ref (c);

	dialog->changes_made = FALSE;
	dialog->ri = ri;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "edit_groups",
				     NULL,
				     "edit_groups", &dialog->window,
				     "treeview", &dialog->treeview,
				     "label_eg", &dialog->label,
				     "label_name", &dialog->label_name,
				     "label_jid", &dialog->label_jid,
				     "label_resource", &dialog->label_resource,
				     "label_agent", &dialog->label_agent,
				     "entry_add", &dialog->entry_add,
				     "button_add", &dialog->button_add,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "edit_groups", "response", on_response,
			      "edit_groups", "destroy", on_destroy,
			      "entry_add", "changed", on_entry_add_changed,
			      "button_add", "clicked", on_button_add_clicked,
			      NULL);

	g_object_unref (xml);
  
	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);

	gj_gtk_eg_setup (dialog);

	return TRUE;
}

static gboolean gj_gtk_eg_setup (GjEditGroupsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	GList *groups = NULL;
	gboolean success = FALSE;
	gint i = 0;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	selection = gtk_tree_view_get_selection (view);

	/* set welcome text */
	if (gj_roster_item_get_jid (dialog->ri) != NULL) {
		GjJID j = NULL;
		gchar *text = NULL;

		j = gj_roster_item_get_jid (dialog->ri);
      
		text = g_strdup_printf (_("Edit the groups using the list below for:\n\"%s\""), 
					gj_jid_get_without_resource (j));
		gtk_label_set_markup (GTK_LABEL (dialog->label), text);
		g_free (text);
	}

	/* create tree store */
	store = gtk_list_store_new (COL_EDIT_GROUPS_COUNT,
				    G_TYPE_STRING,   /* name */
				    G_TYPE_BOOLEAN,  /* enabled */
				    G_TYPE_BOOLEAN,  /* visible */
				    G_TYPE_BOOLEAN); /* editable */
  
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	/* populate columns */
	gj_gtk_eg_model_populate_columns (dialog);

	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE);
	gtk_tree_view_set_search_column (view, COL_EDIT_GROUPS_NAME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, FALSE);
	gtk_tree_view_columns_autosize (view);
	gtk_tree_view_expand_all (view);

	/* populate model */
	if ((success = gj_gtk_eg_model_populate (dialog)) == FALSE) {
		g_warning ("%s: failed to setup roster", __FUNCTION__);
		return FALSE;
	}

	/* clean up */
	g_object_unref (G_OBJECT (model));

	/* get groups */
	if ((groups = gj_roster_item_get_groups (dialog->ri)) == NULL) {
		g_message ("%s: no groups selected", __FUNCTION__);
		return TRUE;
	}
  
	/* set store */
	for (i=0;i<g_list_length (groups);i++) {
		gchar *group = NULL;
		GtkTreeIter *iter = NULL;

		if ((group = (gchar*) g_list_nth_data (groups, i)) == NULL) {
			continue;
		} else {
			g_message ("%s: checking group:'%s'", __FUNCTION__, group); 
		}

		if ((iter = gj_gtk_eg_model_find_iter_by_name (dialog, group)) == NULL) { 
			g_warning ("%s: could not find group:'%s' in list", __FUNCTION__, group);
			continue;
		}

		gtk_list_store_set (store, iter,    
				    COL_EDIT_GROUPS_ENABLED, TRUE,
				    -1);    

		/* clean up */
		gtk_tree_iter_free (iter);
	}
  
	return TRUE;
}

static gboolean gj_gtk_eg_save (GjEditGroupsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;

	gboolean valid = FALSE;

	GjJID j = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (dialog->ri);

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
 
	/* free previously allocated roster groups */
	gj_roster_item_groups_free (dialog->ri);

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gboolean enabled = FALSE;
		gchar *g = NULL;

		/* get toggled iter */
		gtk_tree_model_get (model, &iter, 
				    COL_EDIT_GROUPS_NAME, &g,
				    COL_EDIT_GROUPS_ENABLED, &enabled, 
				    -1);

		if (g != NULL) {
			/* should we add it to the list */
			if (enabled == TRUE) {
				gj_roster_item_groups_add (dialog->ri, g);
			}

			g_free (g);
		}
	}

	/* NOTE: at this point we need to save the new groups to the 
           jabber server the user is registered with  */
	gj_iq_request_user_change (dialog->c,
				   gj_jid_get_without_resource (j), 
				   gj_roster_item_get_name (dialog->ri), 
				   gj_roster_item_get_groups (dialog->ri),
				   gj_gtk_eg_user_changed_response,
				   NULL);

	return TRUE;
}

static gboolean gj_gtk_eg_model_populate_columns (GjEditGroupsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeViewColumn *column = NULL; 
	GtkCellRenderer *renderer = NULL;

	guint col_offset = 0;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));

	/* COL_ROSTER_GROUP_ENABLED */
	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled", G_CALLBACK (on_treeview_cell_toggled), dialog);

	column = gtk_tree_view_column_new_with_attributes (_("Select"),
							   renderer,
							   "active", COL_EDIT_GROUPS_ENABLED,
							   NULL);

	/* set this column to a fixed sizing (of 50 pixels) */
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column), GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_append_column (view, column);

	/* COL_ROSTER_GROUP_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Group"),
								  renderer, 
								  "text", COL_EDIT_GROUPS_NAME,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)COL_EDIT_GROUPS_NAME);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_EDIT_GROUPS_NAME);
	gtk_tree_view_column_set_resizable (column,FALSE);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	return TRUE;
}

static gboolean gj_gtk_eg_model_populate (GjEditGroupsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	const GList *groups = NULL;
	gint i = 0;

	GtkTreeIter iter;

	GjRosterItem ri = NULL;
	GjRoster r = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((ri = dialog->ri) == NULL) {
		g_warning ("%s: dialog roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((r = gj_roster_item_get_roster (ri)) == NULL) {
		g_warning ("%s: dialog roster was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	if ((groups = gj_roster_get_groups (r)) == NULL) {
		g_warning ("%s: there are no groups", __FUNCTION__);
		return TRUE;
	}

	g_message ("%s: adding %d groups", __FUNCTION__, g_list_length ((GList*)groups));  

	for (i=0;i<g_list_length ((GList*)groups);i++) {
		/* add */
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,    
				    COL_EDIT_GROUPS_NAME, (gchar*)g_list_nth_data ((GList*)groups,i),
				    COL_EDIT_GROUPS_ENABLED, FALSE,
				    COL_EDIT_GROUPS_EDITABLE, TRUE,
				    COL_EDIT_GROUPS_VISIBLE, TRUE,
				    -1);
	}

	return TRUE;
}

static GtkTreeIter *gj_gtk_eg_model_find_iter_by_name (GjEditGroupsDialog *dialog, const gchar *name)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter *iter_copy = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;
	gchar *text = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	if (name == NULL) {
		g_warning ("%s: name was NULL", __FUNCTION__);
		return NULL;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE && iter_copy == NULL; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_tree_model_get (model, &iter, 
				    COL_EDIT_GROUPS_NAME, &text,
				    -1);

		g_message ("%s: comparing '%s' with '%s'", __FUNCTION__, name, text);

		if (text != NULL && g_strncasecmp (name, text, g_utf8_strlen (name,-1)) == 0) {
			iter_copy = gtk_tree_iter_copy (&iter);
		}

		g_free (text);
	}
  
	return iter_copy;
}

/* callbacks */
static void gj_gtk_eg_user_changed_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: response!", __FUNCTION__);
}


/*
 * GUI events
 */
static void on_treeview_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjEditGroupsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;

	gboolean enabled = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	path = gtk_tree_path_new_from_string (path_string);

	/* get toggled iter */
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, 
			    COL_EDIT_GROUPS_ENABLED, &enabled, 
			    -1);

	/* do something */
	enabled ^= 1;

	/* set new value */
	gtk_list_store_set (store, &iter, 
			    COL_EDIT_GROUPS_ENABLED, enabled, 
			    -1);

	/* clean up */
	gtk_tree_path_free (path);

	dialog->changes_made = TRUE;
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), dialog->changes_made);
}

static void on_entry_add_changed (GtkEditable *editable, GjEditGroupsDialog *dialog)
{
	gchar *group = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	group = gtk_editable_get_chars (editable, 0, -1); 

	if (group == NULL || g_utf8_strlen (group, -1) < 1) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_add), FALSE);
	} else {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_add), TRUE);
	}

	/* clean up */
	g_free (group);
}


static void on_button_add_clicked (GtkMenuItem *menuitem, GjEditGroupsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	G_CONST_RETURN gchar *group = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	view = GTK_TREE_VIEW (dialog->treeview);  
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));
 
	/* get text */
	group = gtk_entry_get_text (GTK_ENTRY (dialog->entry_add));

	/* add */
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,    
			    COL_EDIT_GROUPS_NAME, group,
			    COL_EDIT_GROUPS_ENABLED, TRUE,
			    COL_EDIT_GROUPS_EDITABLE, TRUE,
			    COL_EDIT_GROUPS_VISIBLE, TRUE,
			    -1);

	/* remove text */
	gtk_entry_set_text (GTK_ENTRY (dialog->entry_add), "");

	dialog->changes_made = TRUE;
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), dialog->changes_made);
}

static void on_destroy (GtkWidget *widget, GjEditGroupsDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);

	g_free (dialog);
}

static void on_response (GtkDialog *window, gint response_id, GjEditGroupsDialog *dialog)
{
	if (response_id == GTK_RESPONSE_OK) {
		gj_gtk_eg_save (dialog);
	}

	gtk_widget_destroy (dialog->window);
}


#ifdef __cplusplus
}
#endif
