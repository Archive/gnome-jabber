/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_support_h
#define __gj_support_h

#include <time.h>


int gj_get_unique_id ();

/*
 * time/date
 */
const char *gj_get_time_zone ();

const char *gj_get_timestamp ();
const char *gj_get_timestamp_long ();
const char *gj_get_timestamp_from_timet (time_t now);

const char *gj_get_datestamp ();
const char *gj_get_datestamp_long ();
const char *gj_get_datestamp_from_timet (time_t now);

const char *gj_get_time_and_date_from_timet (time_t now);

const char *gj_get_timestamp_full ();
const char *gj_get_timestamp_nice ();
const char *gj_get_timestamp_nice_from_timet (time_t now);

/* 
 * string functions 
 */
gboolean gj_find_in_str (const gchar *needle, const gchar *haystack);
gchar *gj_find_in_str_and_remove (const gchar *needle, const gchar *haystack);
gchar *gj_find_in_str_and_replace (const gchar *needle, const gchar *haystack, const gchar *replacement);

/*
 * URL/word identifiers
 */
gboolean gj_is_word_separator (gchar c);

typedef enum t_GjURIType {
  GjURITypeUnknown = 1,
  GjURITypeURL,
  GjURITypeHost,

  GjURITypeEnd
} GjURIType;

GjURIType gj_is_uri (const gchar *word);

/*
 * SHA 1 utils
 */
char *gj_get_sha_by_session_id_and_password (const gchar *session_id, const gchar *password);

/*
 * shell functions
 */
gboolean gj_create_dir (const gchar *dir);


/*
 * base64 encoding/decoding
 */
size_t base64_encode_close (unsigned char *in, 
			   size_t inlen, 
			   gboolean break_lines, 
			   unsigned char *out, 
			   int *state, 
			   int *save);

size_t base64_encode_step (unsigned char *in, 
			  size_t len, 
			  gboolean break_lines, 
			  unsigned char *out, 
			  int *state, 
			  int *save);

size_t base64_decode_step (unsigned char *in, 
			  size_t len, 
			  unsigned char *out, 
			  int *state, 
			  unsigned int *save);

char * base64_encode_simple (const char *data, size_t len);
size_t base64_decode_simple (char *data, size_t len);

#endif

#ifdef __cplusplus
}
#endif
