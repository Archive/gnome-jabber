/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_translate.h"
#include "gj_support.h"
#include "gj_jid.h"
#include "gj_connection.h"

#include "gj_gtk_user_information.h"
#include "gj_gtk_edit_groups.h"
#include "gj_gtk_support.h"


#define INFORMATION_UNKNOWN "-"


typedef struct {
	GtkWidget *window;
  
	GtkWidget *label_jid;
	GtkWidget *label_name;
	GtkWidget *label_group;
	GtkWidget *label_resource;
	GtkWidget *label_agent;

	GtkWidget *label_presence_type;
	GtkWidget *label_presence_show;
	GtkWidget *label_presence_status;
	GtkWidget *label_presence_subscription;
	GtkWidget *image_presence;

	GtkWidget *label_on_roster;
	GtkWidget *image_on_roster;

	GtkWidget *label_full_name;
	GtkWidget *label_name_given;
	GtkWidget *label_name_family;
	GtkWidget *label_name_nickname;

	GtkWidget *label_email;
	GtkWidget *label_url;
	GtkWidget *label_tel;
  
	GtkWidget *label_street;
	GtkWidget *label_extadd;
	GtkWidget *label_locality;
	GtkWidget *label_region;
	GtkWidget *label_pcode;
	GtkWidget *label_country;
  
	GtkWidget *label_org_orgname;
	GtkWidget *label_org_orgunit;
	GtkWidget *label_title;
	GtkWidget *label_role;

	GtkWidget *label_dob;
	GtkWidget *label_description;

	GtkWidget *vbox_version;
	GtkWidget *label_version_name;
	GtkWidget *label_version_version;
	GtkWidget *label_version_os;
  
	GtkWidget *vbox_time;
	GtkWidget *label_time_utc;
	GtkWidget *label_time_zone;
	GtkWidget *label_time_display;

	GtkWidget *vbox_last_action;
	GtkWidget *label_last_action;
	GtkWidget *label_last_seconds;

	GtkWidget *button_refresh;
	GtkWidget *button_close;

	/* members */
	gint id;
	gint ri_id;

	gint responses;

	GjConnection c;
 
} GjUserInformationDialog;


/* find */
static GjUserInformationDialog *gj_gtk_ui_find (gint id);
static GjUserInformationDialog *gj_gtk_ui_find_by_ri_id (gint ri_id);

/* controls */
static gboolean gj_gtk_ui_setup (GjUserInformationDialog *dialog);
static gboolean gj_gtk_ui_get (GjUserInformationDialog *dialog);

/*
 * internal callbacks 
 */
static void gj_gtk_ui_vcard_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);
static void gj_gtk_ui_version_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);
static void gj_gtk_ui_time_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);
static void gj_gtk_ui_last_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

/*
 * gui functions 
 */
static void on_destroy (GtkWidget *widget, GjUserInformationDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjUserInformationDialog *dialog);


GList *user_information_dialogs = NULL;


static GjUserInformationDialog *gj_gtk_ui_find (gint id) {
	gint index = 0;

	if (id < 1) {
		g_warning ("%s: id was < 1", __FUNCTION__);
		return NULL;
	}

	for (index=0;index<g_list_length (user_information_dialogs);index++) {
		GjUserInformationDialog *dialog = NULL;

		dialog = g_list_nth_data (user_information_dialogs, index);
		if (dialog == NULL) continue;
		if (dialog->id == id) return dialog;
	}
 
	return NULL;
}

static GjUserInformationDialog *gj_gtk_ui_find_by_ri_id (gint ri_id) {
	gint index = 0;

	if (ri_id < 1) {
		g_warning ("%s: roster item id was < 1", __FUNCTION__);
		return NULL;
	}

	for (index=0;index<g_list_length (user_information_dialogs);index++) {
		GjUserInformationDialog *dialog = NULL;

		dialog = g_list_nth_data (user_information_dialogs, index);

		if (dialog && dialog->ri_id == ri_id) {
			return dialog; 
		}
	}
 
	return NULL;
}

/*
 * window functions 
 */
gboolean gj_gtk_ui_load (GjConnection c,
			 GjRosterItem ri)
{
	GladeXML *xml = NULL;
	GjUserInformationDialog *dialog = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	dialog = gj_gtk_ui_find_by_ri_id (gj_roster_item_get_id (ri));

	if (dialog != NULL) {
		gtk_window_present (GTK_WINDOW (dialog->window));
		return TRUE;
	}


	dialog = g_new0 (GjUserInformationDialog, 1);

	dialog->id = gj_get_unique_id ();
	dialog->ri_id = gj_roster_item_get_id (ri);

	dialog->c = gj_connection_ref (c);

	user_information_dialogs = g_list_append (user_information_dialogs, dialog);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "user_information",
				     NULL,
				     "user_information", &dialog->window,
				     "label_jid", &dialog->label_jid,
				     "label_name", &dialog->label_name,
				     "label_group", &dialog->label_group,
				     "label_resource", &dialog->label_resource,
				     "label_agent", &dialog->label_agent,
				     "label_presence_type", &dialog->label_presence_type,
				     "label_presence_show", &dialog->label_presence_show,
				     "label_presence_status", &dialog->label_presence_status,
				     "label_presence_subscription", &dialog->label_presence_subscription,
				     "image_presence", &dialog->image_presence,
				     "label_on_roster", &dialog->label_on_roster,
				     "image_on_roster", &dialog->image_on_roster,
				     "label_full_name", &dialog->label_full_name,
				     "label_name_given", &dialog->label_name_given,
				     "label_name_family", &dialog->label_name_family,
				     "label_name_nickname", &dialog->label_name_nickname,
				     "label_email", &dialog->label_email,
				     "label_url", &dialog->label_url,
				     "label_tel", &dialog->label_tel,
				     "label_street", &dialog->label_street,
				     "label_extadd", &dialog->label_extadd,
				     "label_locality", &dialog->label_locality,
				     "label_region", &dialog->label_region,
				     "label_pcode", &dialog->label_pcode,
				     "label_country", &dialog->label_country,
				     "label_org_orgname", &dialog->label_org_orgname,
				     "label_org_orgunit", &dialog->label_org_orgunit,
				     "label_title", &dialog->label_title,
				     "label_role", &dialog->label_role,
				     "label_dob", &dialog->label_dob,
				     "label_description", &dialog->label_description,
				     "vbox_version", &dialog->vbox_version,
				     "label_version_name", &dialog->label_version_name,
				     "label_version_version", &dialog->label_version_version,
				     "label_version_os", &dialog->label_version_os,
				     "vbox_time", &dialog->vbox_time,
				     "label_time_utc", &dialog->label_time_utc,
				     "label_time_zone", &dialog->label_time_zone,
				     "label_time_display", &dialog->label_time_display,
				     "vbox_last_action", &dialog->vbox_last_action,
				     "label_last_action", &dialog->label_last_action,
				     "label_last_seconds", &dialog->label_last_seconds,
				     "button_refresh", &dialog->button_refresh,
				     "button_close", &dialog->button_close,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "user_information", "response", on_response,
			      "user_information", "destroy", on_destroy,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);


	/* set up labels with local information */
	gj_gtk_ui_setup (dialog);

	/* get information from config */
	gj_gtk_ui_get (dialog);

	return TRUE;
}

static gboolean gj_gtk_ui_setup (GjUserInformationDialog *dialog) {
	GjRosterItem ri = NULL;
	GjJID j = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	ri = gj_rosters_find_item_by_id (dialog->ri_id);
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	/* load information into window */
	gj_gtk_set_label_with_args (dialog->label_jid, "%s", gj_jid_get_full (j));
	gj_gtk_set_label_with_args (dialog->label_on_roster,"%s", gj_roster_item_get_is_permanent (ri)?_("Yes"):_("No"));
	gtk_image_set_from_stock (GTK_IMAGE (dialog->image_on_roster), 
				  gj_roster_item_get_is_permanent (ri)?GTK_STOCK_YES:GTK_STOCK_NO, GTK_ICON_SIZE_BUTTON);

	return TRUE;
}

static gboolean gj_gtk_ui_get (GjUserInformationDialog *dialog) {
	GjRosterItem ri = NULL;
	GjJID j = NULL;
	GjPresence pres = NULL;

	gchar *groups = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	ri = gj_rosters_find_item_by_id (dialog->ri_id);
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	if ((pres = gj_roster_item_get_presence (ri)) == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set refresh state */
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_refresh), FALSE);

	groups = gj_roster_item_get_groups_as_string (ri);

	/* get remote information */
	dialog->responses = 4; 

	gj_iq_request_vcard (dialog->c,
			     (gchar*)gj_jid_get_full (j), 
			     gj_gtk_ui_vcard_response, 
			     GINT_TO_POINTER (dialog->id));
	gj_iq_request_version (dialog->c,
			       (gchar*)gj_jid_get_full (j), 
			       gj_gtk_ui_version_response, 
			       GINT_TO_POINTER (dialog->id));
	gj_iq_request_time (dialog->c,
			    (gchar*)gj_jid_get_full (j), 
			    gj_gtk_ui_time_response, 
			    GINT_TO_POINTER (dialog->id));
	gj_iq_request_last (dialog->c,
			    (gchar*)gj_jid_get_full (j), 
			    gj_gtk_ui_last_response, 
			    GINT_TO_POINTER (dialog->id));

	/*
	 * locally obtainable information
	 */

	gj_gtk_set_label_with_args (dialog->label_name, gj_roster_item_get_name (ri));
	gj_gtk_set_label_with_args (dialog->label_group,"%s", groups?groups:INFORMATION_UNKNOWN);
	gj_gtk_set_label_with_args (dialog->label_resource,"%s", gj_jid_get_resource (j)?gj_jid_get_resource (j):INFORMATION_UNKNOWN);
	gj_gtk_set_label_with_args (dialog->label_agent,"%s", gj_jid_get_agent (j)?gj_jid_get_agent (j):INFORMATION_UNKNOWN);

	gj_gtk_set_label_with_args (dialog->label_presence_type,"%s", gj_translate_presence_type (gj_presence_get_type (pres)));
	gj_gtk_set_label_with_args (dialog->label_presence_show,"%s", gj_translate_presence_show (gj_presence_get_show (pres)));
	gj_gtk_set_label_with_args (dialog->label_presence_status,"%s", gj_presence_get_status (pres)?gj_presence_get_status (pres):INFORMATION_UNKNOWN);
	/*   gj_gtk_set_label_with_args (dialog->label_presence_subscription,"%s", gj_translate_roster_item_subscription (gj_roster_item_get_subscription (ri))); */
	gj_gtk_set_label_with_args (dialog->label_presence_subscription,"%s", gj_translate_roster_item_subscription_as_description (gj_roster_item_get_subscription (ri)));

	gtk_image_set_from_stock (GTK_IMAGE (dialog->image_presence), 
				  gj_gtk_presence_to_stock_id (gj_presence_get_type (pres), gj_presence_get_show (pres)),
				  GTK_ICON_SIZE_MENU);

	/* clean up */
	g_free (groups);

	return TRUE;
}

static void gj_gtk_ui_vcard_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *tmp = NULL;

	GjUserInformationDialog *dialog = NULL;

	if (!iq || !iq_related) {
		return;
	}

	if ((dialog = gj_gtk_ui_find (GPOINTER_TO_INT (user_data))) == NULL) {
		g_warning ("%s: could not find dialog from id:%d", __FUNCTION__, GPOINTER_TO_INT (user_data));
		return;
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"FN");
	gj_gtk_set_label_with_args (dialog->label_full_name, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"GIVEN");
	gj_gtk_set_label_with_args (dialog->label_name_given, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"FAMILY");
	gj_gtk_set_label_with_args (dialog->label_name_family, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"NICKNAME");
	gj_gtk_set_label_with_args (dialog->label_name_nickname, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"URL");
	gj_gtk_set_label_with_args (dialog->label_url, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);
  
	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"TEL");
	gj_gtk_set_label_with_args (dialog->label_tel, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"EMAIL");
	gj_gtk_set_label_with_args (dialog->label_email, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"DESC");
	gj_gtk_set_label_with_args (dialog->label_description, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"BDAY");
	gj_gtk_set_label_with_args (dialog->label_dob, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"STREET");
	gj_gtk_set_label_with_args (dialog->label_street, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"EXTADD");
	gj_gtk_set_label_with_args (dialog->label_extadd, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"LOCALITY");
	gj_gtk_set_label_with_args (dialog->label_locality, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"REGION");
	gj_gtk_set_label_with_args (dialog->label_region, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"PCODE");
	gj_gtk_set_label_with_args (dialog->label_pcode, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"COUNTRY");
	gj_gtk_set_label_with_args (dialog->label_country, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"ORGNAME");
	gj_gtk_set_label_with_args (dialog->label_org_orgname, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"ORGUNIT");
	gj_gtk_set_label_with_args (dialog->label_org_orgunit, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"TITLE");
	gj_gtk_set_label_with_args (dialog->label_title, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"ROLE");
	gj_gtk_set_label_with_args (dialog->label_role, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

	dialog->responses--;

	/* set refresh state */
	if (dialog->responses == 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_refresh), TRUE);
	}
}

static void gj_gtk_ui_version_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *tmp = NULL;

	GjUserInformationDialog *dialog = NULL;

	if (!iq || !iq_related) {
		return;
	}

	if ((dialog = gj_gtk_ui_find (GPOINTER_TO_INT (user_data))) == NULL) {
		g_warning ("%s: could not find GjUserInformationDialog from id:%d", __FUNCTION__, GPOINTER_TO_INT (user_data));
		return;
	}

	tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"type");
	if (tmp != NULL && xmlStrcmp (tmp, (guchar*)"error") != 0) {
		gtk_widget_set_sensitive (dialog->vbox_version, TRUE);

		tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"name");
		gj_gtk_set_label_with_args (dialog->label_version_name, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);
      
		tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"os");
		gj_gtk_set_label_with_args (dialog->label_version_os, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);
      
		tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"version");
		gj_gtk_set_label_with_args (dialog->label_version_version, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);      
	} else {
		gtk_widget_set_sensitive (dialog->vbox_version, FALSE);
	}

	dialog->responses--;

	/* set refresh state */
	if (dialog->responses == 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_refresh), TRUE); 
	}
}

static void gj_gtk_ui_time_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *tmp = NULL;

	GjUserInformationDialog *dialog = NULL;

	if (!iq || !iq_related) {
		return;
	}

	if ((dialog = gj_gtk_ui_find (GPOINTER_TO_INT (user_data))) == NULL) {
		g_warning ("%s: could not find GjUserInformationDialog from id:%d", __FUNCTION__, GPOINTER_TO_INT (user_data));
		return;
	}

	tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"type");
	if (tmp != NULL && xmlStrcmp (tmp, (guchar*)"error") != 0) {
		gchar **time = NULL;
		gchar *date = NULL;

		gtk_widget_set_sensitive (dialog->vbox_time, TRUE);

		if ((tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"utc")) != NULL) {
			time = g_strsplit ((gchar*)tmp,"T",-1);

			if (time[0] != NULL) { 
				date = g_strdup_printf ("%2.2s-%2.2s-%4.4s",time[0]+6, time[0]+4, time[0]);
			}
		}

		gj_gtk_set_label_with_args (dialog->label_time_utc, "%s (%s)",
					    time[1]?time[1]:INFORMATION_UNKNOWN, 
					    date?date:INFORMATION_UNKNOWN);
      
		tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"tz");
		gj_gtk_set_label_with_args (dialog->label_time_zone, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);
      
		tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"display");
		gj_gtk_set_label_with_args (dialog->label_time_display, "%s",tmp? (gchar*)tmp:INFORMATION_UNKNOWN);

		/* clean up */
		if (time != NULL) {
			g_strfreev (time);
		}

		g_free (date);
	} else {
		gtk_widget_set_sensitive (dialog->vbox_time, FALSE);
	}

	dialog->responses--;

	/* set refresh state */
	if (dialog->responses == 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_refresh), TRUE);
	}
}

static void gj_gtk_ui_last_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *tmp = NULL;

	GjUserInformationDialog *dialog = NULL;

	if (!iq || !iq_related) {
		return;
	}

	if ((dialog = gj_gtk_ui_find (GPOINTER_TO_INT (user_data))) == NULL) {
		g_warning ("%s: could not find GjUserInformationDialog from id:%d", __FUNCTION__, GPOINTER_TO_INT (user_data));
		return;
	}

	tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"type");
	if (tmp != NULL && xmlStrcmp (tmp, (guchar*)"error") != 0) {
		xmlNodePtr node = NULL;
		gchar *action = NULL;
		gchar *time = NULL;

		if ((node = gj_parser_find_by_node (gj_iq_get_xml (iq), (guchar*)"query")) != NULL) {
			/* get seconds */
			if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"seconds")) != NULL) {
				guint x = atoi ((gchar*)tmp);
				guint seconds = 0;
				guint minutes = 0;
				guint hours = 0;
				guint days = 0;
	      
				if (x >= 0) {
					/* calculate from there */
					seconds = x % 60;
					x /= 60;
					minutes = x % 60;
					x /= 60;
					hours = x % 24;
					x /= 24;
					days = x;
		  
					time = g_strdup_printf (_("%d %s, %d %s, %d %s, %d %s ago"), 
								days, days==1?_("day"):_("days"), 
								hours, hours==1?_("hour"):_("hours"),
								minutes, minutes==1?_("minute"):_("minutes"),
								seconds, seconds==1?_("second"):_("seconds"));
				}
			}

			if ((tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"query")) != NULL) {
				action = g_strdup_printf ("%s",tmp? (gchar*)tmp:_("unknown action"));
			}

			gj_gtk_set_label_with_args (dialog->label_last_action, "%s",action?action:INFORMATION_UNKNOWN);
			gj_gtk_set_label_with_args (dialog->label_last_seconds, "%s",time?time:INFORMATION_UNKNOWN);

			/* clean up */
			g_free (action);
			g_free (time);
		}	  

		gtk_widget_set_sensitive (dialog->vbox_last_action, TRUE);
	} else {
		gtk_widget_set_sensitive (dialog->vbox_last_action, FALSE);
	}

	dialog->responses--;

	/* set refresh state */
	if (dialog->responses == 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_refresh), TRUE);
	}
}

/*
 * GUI events
 */
static void on_destroy (GtkWidget *widget, GjUserInformationDialog *dialog) 
{
	user_information_dialogs = g_list_remove (user_information_dialogs, dialog);
    
	gj_connection_unref (dialog->c);

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjUserInformationDialog *dialog) 
{
	if (response == 1) {
		gj_gtk_ui_get (dialog);
		return;
	}

	gtk_widget_destroy (dialog->window);
}


#ifdef __cplusplus
}
#endif
