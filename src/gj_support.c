/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <string.h>
#include <ctype.h>
#include <time.h> 

#ifdef HAVE_SYS_STAT_H
#  include <sys/stat.h>
#endif 

#ifdef G_OS_WIN32
#  include <direct.h>
#endif

#include <glib.h>

#include <gnet.h>

#include "gj_support.h"

int gj_get_unique_id ()
{
	static int id = 69;
	return id++;
}

const char *gj_get_time_zone ()
{
#ifndef G_OS_WIN32
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);
 
	if (tmLocal == NULL) {
		return "time zone unknown";
	} else {
		return tmLocal->tm_zone; 
	}
#else
	return "time zone unknown on win32";
#endif
}

const char *gj_get_timestamp ()
{
	static char buf[256];
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);

	if (strftime (buf, 256, "%H:%M:%S", tmLocal) == 0) {
		return "time unknown";
	} else {
		return buf;
	}
}

const char *gj_get_timestamp_long ()
{
	static char buf[256];
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);

	if (strftime (buf, 256, "%H%M%S", tmLocal) == 0) {
		return "time unknown";
	} else {
		return buf;
	}
}

const char *gj_get_timestamp_from_timet (time_t now)
{
	static char buf[256];
	struct tm *tmLocal = localtime (&now);

	if (strftime (buf, 256, "%H:%M:%S", tmLocal) == 0) {
		return "time unknown";
	} else {
		return buf;
	}
}

const char *gj_get_timestamp_full ()
{
	static char buf[256];
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);

	if (strftime (buf, 256, "%Y%m%d%H%M%S", tmLocal) == 0) {
		return "time/date unknown";
	} else {
		return buf;
	}
}

const char *gj_get_timestamp_nice ()
{
	static char buf[256];
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);

	if (strftime (buf, 256, "%a, %b %d, %H:%M:%S", tmLocal) == 0) {
		return "time/date unknown";
	} else {
		return buf; 
	}
}

const char *gj_get_timestamp_nice_from_timet (time_t now)
{
	static char buf[256];
	struct tm *tmLocal = localtime (&now);

	if (strftime (buf, 256, "%a, %b %d, %H:%M:%S", tmLocal) == 0) {
		return "time/date unknown";
	} else {
		return buf;
	}
}

const char *gj_get_datestamp ()
{
	static char buf[256];
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);

	if (strftime (buf, 256, "%a, %b %d", tmLocal) == 0) {
		return "date unknown";
	} else {
		return buf;
	}
}

const char *gj_get_datestamp_long ()
{
	static char buf[256];
	time_t tnow = time (0);
	struct tm *tmLocal = localtime (&tnow);

	if (strftime (buf, 256, "%Y%m%d", tmLocal) == 0) {
		return "date unknown";
	} else {
		return buf;
	}
}

const char *gj_get_datestamp_from_timet (time_t now)
{
	static char buf[256];
	struct tm *tmLocal = localtime (&now);

	if (strftime (buf, 256, "%a, %b %d", tmLocal) == 0) {
		return "date unknown";
	} else {
		return buf;
	}
} 

const char *gj_get_time_and_date_from_timet (time_t now)
{
	static char buf[256];
	struct tm *tmLocal = localtime (&now);

	if (strftime (buf, 256, "%A, %B %d, %H:%M:%S", tmLocal) == 0) {
		return "date/time unknown";
	} else {
		return buf;
	}
}

gboolean gj_find_in_str (const gchar *needle, const gchar *haystack)
{
	gchar *result = NULL;

	if (needle == NULL) {
		g_warning ("%s: needle is NULL", __FUNCTION__);
		return FALSE;
	}

	if (haystack == NULL) {
		g_warning ("%s: haystack is NULL", __FUNCTION__);
		return FALSE;
	}

	if ((result = g_strrstr (haystack, needle)) == NULL) {
		return FALSE;
	}
    
	return TRUE;
}

gchar *gj_find_in_str_and_remove (const gchar *needle, const gchar *haystack)
{
	gchar **vstripped = NULL;
	gchar *result = NULL;

	gint i = 0;
	gboolean end_of_array = FALSE;

	if (needle == NULL) {
		g_warning ("%s: needle is NULL", __FUNCTION__);
		return NULL;
	}

	if (haystack == NULL) {
		g_warning ("%s: haystack is NULL", __FUNCTION__);
		return NULL;
	}

	/* remove all except numbers */
	vstripped = g_strsplit (haystack, needle, -1);

	if (vstripped == NULL) {
		g_warning ("%s: could not split text with delimiter:'%s' - NULL returned..", __FUNCTION__, needle);
		return NULL;
	}
   
	while (end_of_array == FALSE) {
		if (vstripped[i] == NULL) {
			end_of_array = TRUE;
			break;
		}
		
		if (result == NULL) {
			result = g_strdup (vstripped[i]);
		} else {
			result = g_strconcat (result, vstripped[i], NULL); 
		}
		
		i++;
	}

	/* clean up */
	if (vstripped != NULL) {
		g_strfreev (vstripped);
	}

	return result;
}

gchar *gj_find_in_str_and_replace (const gchar *needle, const gchar *haystack, const gchar *replacement)
{
	gchar **vstripped = NULL;
	gchar *result = NULL;

	gint i = 0;
	gboolean end_of_array = FALSE;

	if (needle == NULL) {
		g_warning ("%s: needle is NULL", __FUNCTION__);
		return NULL;
	}

	if (haystack == NULL) {
		g_warning ("%s: haystack is NULL", __FUNCTION__);
		return NULL;
	}

	if (replacement == NULL) {
		g_warning ("%s: replacement is NULL", __FUNCTION__);
		return NULL;
	}

	/* remove all except numbers */
	vstripped = g_strsplit (haystack, needle, -1);

	if (vstripped == NULL) {
		g_warning ("%s: could not split text with delimiter:'%s' - NULL returned..", __FUNCTION__, needle);
		return NULL;
	}
   
	while (end_of_array == FALSE) {
		if (vstripped[i] == NULL) {
			end_of_array = TRUE;
			break;
		}
		
		if (result == NULL) {
			result = g_strdup (vstripped[i]);
		} else { 
			result = g_strconcat (result, replacement, vstripped[i], NULL);
		}
		
		i++;
	}

	/* clean up */
	if (vstripped != NULL) {
		g_strfreev (vstripped);
	}

	return result;
}

/*
 * utils 
 */
gboolean gj_is_word_separator (gchar c)
{
	/* 	return !isalpha (c) && c != '\''; */
	return (isspace (c));
}

GjURIType gj_is_uri (const gchar *word)
{
	gint len = 0;
	
	if (!word) {
		return GjURITypeUnknown;
	}

	len = g_utf8_strlen (word, -1);
  
	if (!strncasecmp (word, "irc://", 6)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word, "irc.", 4)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word, "ftp.", 4)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word, "ftp:", 4)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word, "www.", 4)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word, "http:", 5)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word, "https:", 6)) {
		return GjURITypeURL;
	} else if (!strncasecmp (word + len - 4, ".org", 4)) {
		return GjURITypeHost;
	} else if (!strncasecmp (word + len - 4, ".net", 4)) {
		return GjURITypeHost;
	} else if (!strncasecmp (word + len - 4, ".com", 4)) {
		return GjURITypeHost;
	} else if (!strncasecmp (word + len - 4, ".edu", 4)) {
		return GjURITypeHost;
	}
  
	return GjURITypeUnknown;
}

gchar *gj_get_sha_by_session_id_and_password (const gchar *session_id, const gchar *password)
{
	GSHA *sha = NULL;
	gchar *digest_input = NULL;
	gchar *digest_output = NULL;
  
	if (session_id == NULL) {
		g_warning ("%s: session ID was NULL", __FUNCTION__);
		return NULL;
	}
  
	if (password == NULL) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return NULL;
	}
  
	digest_input = g_strconcat (session_id, password, NULL);
	g_message ("%s: SHA input is:'%s'", __FUNCTION__, digest_input);

	sha = gnet_sha_new ((gchar*)digest_input, strlen (digest_input));
	digest_output = gnet_sha_get_string (sha);
	g_message ("%s: SHA output is:'%s'", __FUNCTION__, digest_output);
  
	/* clean up */
	g_free (digest_input);
	gnet_sha_delete (sha);
  
	return digest_output;
}

#ifdef G_OS_WIN32
# include <io.h>
#endif

gboolean gj_create_dir (const gchar *dir)
{
	if (dir == NULL || g_utf8_strlen (dir, -1) < 1) {
		g_warning ("%s: directory was NULL or an empty string", __FUNCTION__);
		return FALSE;
	}

#ifdef G_OS_WIN32
	if (mkdir (dir) != 0) {
#else
		/*   if (mkdir (dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0) */
		if (mkdir (dir, S_IRUSR | S_IWUSR | S_IXUSR) != 0) {
#endif
			g_warning ("%s: could not create directory:'%s', perhaps it already exists? an error occured", __FUNCTION__, dir);
			return FALSE;
		}

	return TRUE;
}

/*
 * base64 encoding / decoding
 * taken from evolution's camel library
 */
static char *base64_alphabet =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static unsigned char base64_rank[256];


static void base64_init (void)
{
	int i;
	static int initiated = 0;

	if (initiated) {
		return;
	}

	initiated = 1;

	memset (base64_rank, 0xff, sizeof (base64_rank));

	for (i=0;i<64;i++) {
		base64_rank[ (unsigned int)base64_alphabet[i]] = i;
	}

	base64_rank['='] = 0;
}

/* call this when finished encoding everything, to
   flush off the last little bit */
size_t base64_encode_close (unsigned char *in, size_t inlen, gboolean break_lines, unsigned char *out, int *state, int *save)
{
	int c1, c2;
	unsigned char *outptr = out;

	base64_init ();

	if (inlen > 0) {
		outptr += base64_encode_step (in, inlen, break_lines, outptr, state, save);
	}

	c1 = ((unsigned char *)save)[1];
	c2 = ((unsigned char *)save)[2];

#if 0	
	d (printf ("mode = %d\nc1 = %c\nc2 = %c\n",
		   (int) ((char *)save)[0],
		   (int) ((char *)save)[1],
		   (int) ((char *)save)[2]));
#endif

	switch (( (char *)save)[0]) {
	case 2:
		outptr[2] = base64_alphabet[ ((c2 &0x0f) << 2 ) ];
		g_assert (outptr[2] != 0);
		goto skip;
	case 1:
		outptr[2] = '=';
	skip:
		outptr[0] = base64_alphabet[ c1 >> 2 ];
		outptr[1] = base64_alphabet[ c2 >> 4 | ((c1&0x3) << 4 )];
		outptr[3] = '=';
		outptr += 4;
		break;
	}

	if (break_lines) {
		*outptr++ = '\n';
	}

	*save = 0;
	*state = 0;

	return outptr-out;
}

/*
  performs an 'encode step', only encodes blocks of 3 characters to the
  output at a time, saves left-over state in state and save (initialise to
  0 on first invocation).
*/
size_t base64_encode_step (unsigned char *in, size_t len, gboolean break_lines, unsigned char *out, int *state, int *save)
{
	register unsigned char *inptr, *outptr;

	base64_init ();

	if (len<=0) {
		return 0;
	}

	inptr = in;
	outptr = out;

#if 0
	d (printf ("we have %d chars, and %d saved chars\n", len, ((char *)save)[0]));
#endif

	if (len + ((char *)save)[0] > 2) {
		unsigned char *inend = in+len-2;
		register int c1, c2, c3;
		register int already;

		already = *state;

		switch (( (char *)save)[0]) {
		case 1:	c1 = ((unsigned char *)save)[1]; goto skip1;
		case 2:	c1 = ((unsigned char *)save)[1];
			c2 = ((unsigned char *)save)[2]; goto skip2;
		}
		
		/* yes, we jump into the loop, no i'm not going to change it, it's beautiful! */
		while (inptr < inend) {
			c1 = *inptr++;
		skip1:
			c2 = *inptr++;
		skip2:
			c3 = *inptr++;
			*outptr++ = base64_alphabet[ c1 >> 2 ];
			*outptr++ = base64_alphabet[ c2 >> 4 | ((c1&0x3) << 4 ) ];
			*outptr++ = base64_alphabet[ ((c2 &0x0f) << 2 ) | (c3 >> 6) ];
			*outptr++ = base64_alphabet[ c3 & 0x3f ];
			/* this is a bit ugly ... */
			if (break_lines && (++already)>=19) {
				*outptr++='\n';
				already = 0;
			}
		}

		((char *)save)[0] = 0;
		len = 2- (inptr-inend);
		*state = already;
	}

#if 0
	d (printf ("state = %d, len = %d\n",
		   (int) ((char *)save)[0],
		   len));
#endif

	if (len>0) {
		register char *saveout;

		/* points to the slot for the next char to save */
		saveout = & (( (char *)save)[1]) + ((char *)save)[0];

		/* len can only be 0 1 or 2 */
		switch (len) {
		case 2:	*saveout++ = *inptr++;
		case 1:	*saveout++ = *inptr++;
		}
		((char *)save)[0]+=len;
	}

#if 0
	d (printf ("mode = %d\nc1 = %c\nc2 = %c\n",
		   (int) ((char *)save)[0],
		   (int) ((char *)save)[1],
		   (int) ((char *)save)[2]));
#endif

	return outptr-out;
}


/**
 * camel_base64_decode_step: decode a chunk of base64 encoded data
 * @in: input stream
 * @len: max length of data to decode
 * @out: output stream
 * @state: holds the number of bits that are stored in @save
 * @save: leftover bits that have not yet been decoded
 *
 * Decodes a chunk of base64 encoded data
 **/
size_t base64_decode_step (unsigned char *in, size_t len, unsigned char *out, int *state, unsigned int *save)
{
	register unsigned char *inptr, *outptr;
	unsigned char *inend, c;
	register unsigned int v;
	int i;

	base64_init ();

	inend = in+len;
	outptr = out;

	/* convert 4 base64 bytes to 3 normal bytes */
	v=*save;
	i=*state;
	inptr = in;
	while (inptr<inend) {
		c = base64_rank[*inptr++];
		if (c != 0xff) {
			v = (v<<6) | c;
			i++;
			if (i==4) {
				*outptr++ = v>>16;
				*outptr++ = v>>8;
				*outptr++ = v;
				i=0;
			}
		}
	}

	*save = v;
	*state = i;

	/* quick scan back for '=' on the end somewhere */
	/* fortunately we can drop 1 output char for each trailing = (upto 2) */
	i=2;
	while (inptr>in && i) {
		inptr--;
		if (base64_rank[*inptr] != 0xff) {
			if (*inptr == '=' && outptr>out)
				outptr--;
			i--;
		}
	}

	/* if i!= 0 then there is a truncation error! */
	return outptr-out;
}

char *base64_encode_simple (const char *data, size_t len)
{
	unsigned char *out;
	int state = 0, outlen;
	unsigned int save = 0;

	base64_init ();
	
	out = g_malloc (len * 4 / 3 + 5);
	outlen = base64_encode_close ((unsigned char *)data, len, FALSE,
				      out, &state, &save);
	out[outlen] = '\0';
	return (char *)out;
}

size_t base64_decode_simple (char *data, size_t len)
{
	int state = 0;
	unsigned int save = 0;

	base64_init ();

	return base64_decode_step ((unsigned char *)data, len,
				   (unsigned char *)data, &state, &save);
}



#ifdef __cplusplus
}
#endif
