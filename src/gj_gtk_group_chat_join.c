/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_iq_requests.h"
#include "gj_config.h"
#include "gj_jid.h"
#include "gj_roster_item.h"
#include "gj_group_chat.h"
#include "gj_presence.h"
#include "gj_event.h"
#include "gj_connection.h"

#include "gj_gtk_group_chat_join.h"
#include "gj_gtk_group_chat.h"
#include "gj_gtk_support.h"
#include "gj_gtk_roster.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry_nickname;
	GtkWidget *entry_host;
	GtkWidget *entry_room;

	GtkWidget *button_ok;

	/* 
	 * members 
	 */

	GjConnection c;
	GjJID connected_jid;

	GtkWidget *progress_dialog;
	gboolean progress_cancelled;

} GjGroupChatJoinDialog;


static gboolean gj_gtk_gcj_join (GjGroupChatJoinDialog *dialog);
static gboolean gj_gtk_gcj_check (GjGroupChatJoinDialog *dialog);

static void gj_gtk_gcj_progress_cb (GtkDialog *widget, gint response, GjGroupChatJoinDialog *dialog);

/*
 * gui functions 
 */
static void on_entry_nickname_changed (GtkEditable *editable, GjGroupChatJoinDialog *dialog);
static void on_entry_host_changed (GtkEditable *editable, GjGroupChatJoinDialog *dialog);
static void on_entry_room_changed (GtkEditable *editable, GjGroupChatJoinDialog *dialog);

static void on_destroy (GtkWidget *widget, GjGroupChatJoinDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjGroupChatJoinDialog *dialog);


static GjGroupChatJoinDialog *current_dialog = NULL;


gboolean gj_gtk_gcj_load (GjConnection c, 
			  GjJID connected_jid)
{
	GjGroupChatJoinDialog *dialog = NULL;
	GladeXML *xml = NULL;

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjGroupChatJoinDialog, 1);
	
	dialog->c = gj_connection_ref (c);
	dialog->connected_jid = gj_jid_ref (connected_jid);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "group_chat_join",
				     NULL,
				     "group_chat_join", &dialog->window,
				     "entry_nickname", &dialog->entry_nickname,
				     "entry_host", &dialog->entry_host,
				     "entry_room", &dialog->entry_room,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "group_chat_join", "response", on_response,
			      "group_chat_join", "destroy", on_destroy,
			      "entry_nickname", "changed", on_entry_nickname_changed,
			      "entry_host", "changed", on_entry_host_changed,
			      "entry_room", "changed", on_entry_room_changed,
			      NULL);

	g_object_unref (xml);
 
	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set up window */
	gtk_entry_set_text (GTK_ENTRY (dialog->entry_nickname), gj_gtk_roster_config_nick ());

	return TRUE;
}

gboolean gj_gtk_gcj_post_by_event (GjEvent ev)
{
	GjPresence pres = NULL;
	GjRosterItem ri = NULL;

	G_CONST_RETURN gchar *nickname = NULL;
	G_CONST_RETURN gchar *host = NULL;
	G_CONST_RETURN gchar *room = NULL;

	gint error_code = 0;

	if (current_dialog == NULL) {
		return FALSE;
	}

	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri = gj_event_get_roster_item (ev);
	if (!ri) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_event_get_type (ev) != GjEventTypePresence) {
		g_message ("%s: unrecognised event type", __FUNCTION__);
		return FALSE;
	}
 
	pres = (GjPresence) gj_event_get_data (ev);
	if (gj_presence_get_type (pres) != GjPresenceTypeError) {

		nickname = gtk_entry_get_text (GTK_ENTRY (current_dialog->entry_nickname));
		host = gtk_entry_get_text (GTK_ENTRY (current_dialog->entry_host));
		room = gtk_entry_get_text (GTK_ENTRY (current_dialog->entry_room));

		gj_gtk_gcw_load (current_dialog->c,
				 current_dialog->connected_jid, 
				 ri, host, room, nickname);

		/* switch all other messages to group chat window instead of here */
		gj_roster_item_set_is_joining_group_chat (ri, FALSE);
      
		/* destroy the join dialog */
		gtk_widget_destroy (current_dialog->window);

		/* post to group chat window */
		return gj_gtk_gcw_post_by_event (ev);
	}
 
	/* restore settings */ 
	gj_roster_item_set_is_joining_group_chat (ri, FALSE);
	gj_group_chat_del (ri);
	gj_roster_item_free (ri);
  
	/* destroy this dialog */
	gtk_widget_destroy (current_dialog->window);

	/* handle error */
	if (gj_presence_get_error_code (pres) != NULL) {
		error_code = atoi (gj_presence_get_error_code (pres)); 
	}
  
	gj_gtk_show_error (_("Joining Group Chat Failed."), 
			   error_code, 
			   gj_presence_get_error_reason (pres));
  
	return TRUE;
}

static gboolean gj_gtk_gcj_join (GjGroupChatJoinDialog *dialog)
{
	G_CONST_RETURN gchar *nickname = NULL;
	G_CONST_RETURN gchar *host = NULL;
	G_CONST_RETURN gchar *room = NULL;

	GjRosterItem ri = NULL;
	GjJID j = NULL;
	gchar *jid = NULL;

	if (!dialog){
		return FALSE; 
	}

	nickname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_nickname));
	host = gtk_entry_get_text (GTK_ENTRY (dialog->entry_host));
	room = gtk_entry_get_text (GTK_ENTRY (dialog->entry_room));

	if (gj_gtk_gcj_check (dialog) == FALSE) {
		return FALSE;
	}

	/* set up jid */
	jid = g_strdup_printf ("%s@%s/%s", room, host, nickname);
	j = gj_jid_new (jid);

	ri = gj_group_chat_find (j);
	gj_jid_unref (j);

	if (ri) {
		GtkWidget *msg_dialog = NULL;

		g_message ("%s: there is already a window open to host:'%s', room:'%s'", __FUNCTION__, host, room);

		msg_dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (dialog->window),
						    GTK_DIALOG_DESTROY_WITH_PARENT,
						    GTK_MESSAGE_WARNING,
						    GTK_BUTTONS_OK,
						    _("You are already chatting in this room."),
						    _("If you want to chat under a different nickname, "
						      "close the window currently open for this room (%s@%s) "
						      "and try again."), 
						    room, host);

		g_signal_connect_swapped (GTK_OBJECT (msg_dialog), "response",
					  G_CALLBACK (gtk_widget_destroy),
					  GTK_OBJECT (msg_dialog));
      
		gtk_widget_show_all (msg_dialog);

		/* clean up */
		g_free (jid);
		return TRUE;
	}

	if ((ri = gj_roster_item_new (jid)) == NULL) {
		g_free (jid);
		return FALSE;
	}
  
	/* set it up for group chat */
	gj_roster_item_set_is_for_group_chat (ri, TRUE);
  
	/* switch all other messages to group chat window instead of here */
	gj_roster_item_set_is_joining_group_chat (ri, TRUE);

	/* add to list */
	gj_group_chat_add (ri);

	/* send presence... and effectively join */
	gj_presence_send_to (dialog->c,
			     gj_roster_item_get_jid (ri), 
			     TRUE, 
			     GjPresenceTypeAvailable, 
			     GjPresenceShowNormal, 
			     "Online", 
			     0);

	dialog->progress_cancelled = FALSE;
	dialog->progress_dialog = gj_gtk_hig_progress_new (GTK_WINDOW (dialog->window),
							   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							   (GCallback)gj_gtk_gcj_progress_cb,
							   dialog,
							   _("Progress"),
							   _("Joining Group Chat"), 
							   _("Waiting for room \"%s\" at \"%s\" to respond..."), 
							   room, host); 


	gtk_widget_show_all (dialog->progress_dialog);

	/* need a timeout for servers that do not respond */

	/* clean up */
	g_free (jid);

	return TRUE;
}

static gboolean gj_gtk_gcj_check (GjGroupChatJoinDialog *dialog)
{
	G_CONST_RETURN gchar *nickname = NULL;
	G_CONST_RETURN gchar *host = NULL;
	G_CONST_RETURN gchar *room = NULL;

	if (!dialog) { 
		return FALSE;
	}

	nickname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_nickname));
	host = gtk_entry_get_text (GTK_ENTRY (dialog->entry_host));
	room = gtk_entry_get_text (GTK_ENTRY (dialog->entry_room));

	if ((nickname == NULL || g_utf8_strlen (nickname, -1) < 1) ||
	    (host == NULL || g_utf8_strlen (host, -1) < 1) ||
	    (room == NULL || g_utf8_strlen (room, -1) < 1)) {
		/* disable ok */
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), FALSE);
		return FALSE;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), TRUE);
	return TRUE;
}

static void gj_gtk_gcj_progress_cb (GtkDialog *widget, gint response, GjGroupChatJoinDialog *dialog)
{
	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_DELETE_EVENT)
		dialog->progress_cancelled = TRUE;

	if (dialog->progress_cancelled) {
		G_CONST_RETURN gchar *nickname = NULL;
		G_CONST_RETURN gchar *host = NULL;
		G_CONST_RETURN gchar *room = NULL;
      
		GjRosterItem ri = NULL;
		GjJID j = NULL;
		gchar *jid = NULL;
      
		nickname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_nickname));
		host = gtk_entry_get_text (GTK_ENTRY (dialog->entry_host));
		room = gtk_entry_get_text (GTK_ENTRY (dialog->entry_room));

		jid = g_strdup_printf ("%s@%s/%s", room, host, nickname);
		j = gj_jid_new (jid);

		ri = gj_group_chat_find (j);

		if (ri) {
			gj_group_chat_del (ri);
			gj_roster_item_free (ri);
		}

		gj_jid_unref (j);
		g_free (jid);
	}

	gtk_widget_destroy (dialog->progress_dialog);
	dialog->progress_dialog = NULL;
}


/*
 * GUI events
 */
static void on_entry_nickname_changed (GtkEditable *editable, GjGroupChatJoinDialog *dialog)
{
	gj_gtk_gcj_check (dialog);
}

static void on_entry_host_changed (GtkEditable *editable, GjGroupChatJoinDialog *dialog)
{
	gj_gtk_gcj_check (dialog);
}

static void on_entry_room_changed (GtkEditable *editable, GjGroupChatJoinDialog *dialog)
{
	gj_gtk_gcj_check (dialog);
}

static void on_destroy (GtkWidget *widget, GjGroupChatJoinDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);
	gj_jid_unref (dialog->connected_jid);

	g_free (dialog);   
}

static void on_response (GtkDialog *widget, gint response, GjGroupChatJoinDialog *dialog)
{
	if (response == GTK_RESPONSE_OK) {
		gj_gtk_gcj_join (dialog);
		return;
	}

	gtk_widget_destroy (dialog->window);
}


#ifdef __cplusplus
}
#endif
