/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>

#include <glade/glade.h>
#include <gtk/gtk.h>

#include "gj_main.h"
#include "gj_support.h"

#include "gj_gtk_toaster.h"
#include "gj_gtk_support.h"


struct t_GjToasterWindow {
	GtkWidget *window;

	/* members */
	gboolean moving;
	gboolean shown;

	gboolean destroy_on_hide;
    
	gint dist_vertically;
	gint dist_horizontally;

	gint seconds;

	gint show_timeout_id;
	gint hide_timeout_id;
	gint finish_timeout_id;

	GjToasterDestroyCallback destroy_cb;
	gpointer user_data;
};


static gboolean gj_gtk_toaster_move_to_show_cb (GjToasterWindow *window);
static gboolean gj_gtk_toaster_move_to_hide_cb (GjToasterWindow *window);
static gboolean gj_gtk_toaster_timeout_cb (GjToasterWindow *window);


/*
 * actual GUI functions
 */
static void on_destroy (GtkWidget *widget, GjToasterWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjToasterWindow *window);


GjToasterWindow *gj_gtk_toaster_new (gint seconds, 
				     GjToasterDestroyCallback cb, 
				     gpointer user_data)
{
	GjToasterWindow *window = NULL;
    
	if (seconds < 2) {
		g_warning ("%s: can not shown the toaster for less than 2 seconds", __FUNCTION__);
		return NULL;
	}

	window = g_new0 (struct t_GjToasterWindow, 1);

	window->moving = FALSE;
	window->shown = FALSE;

	window->destroy_on_hide = FALSE;

	window->dist_vertically = 0;
	window->dist_horizontally = 0;
    
	window->seconds = seconds;
	window->shown = FALSE;

	window->destroy_cb = cb;
	window->user_data = user_data;

	window->window = gtk_window_new (GTK_WINDOW_POPUP);
	gtk_widget_set_size_request (window->window, 200, 100);
	gtk_window_set_default_size (GTK_WINDOW (window->window), 200, 100);

	g_signal_connect ((gpointer) window->window, "delete_event",
			  G_CALLBACK (on_delete_event),
			  window);

	g_signal_connect ((gpointer) window->window, "destroy",
			  G_CALLBACK (on_destroy),
			  window);
   
	return window;
}

gboolean gj_gtk_toaster_free (GjToasterWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	gtk_widget_destroy (window->window);
	return TRUE;
}

gboolean gj_gtk_toaster_show (GjToasterWindow *window)
{
	GdkScreen *screen = NULL;
	gint screen_width = 0;
	gint screen_height = 0;
	gint window_width = 0;
	gint window_height = 0;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (window->window == NULL) {
		g_warning ("%s: window->window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (window->moving == TRUE) {
		g_warning ("%s: toaster already in action", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: showing toaster (for %d seconds)", __FUNCTION__, window->seconds);
#if 0
	/* play sound */
	gj_gtk_sound_play ();
#endif
    
	window->moving = TRUE;
	window->shown = TRUE;

	screen = gdk_screen_get_default ();
	screen_width = gdk_screen_get_width (screen);
	screen_height = gdk_screen_get_height (screen);

	gtk_widget_show (window->window);
	gtk_widget_get_size_request (window->window, &window_width, &window_height);

	/* move window */
	gdk_window_move (GDK_WINDOW (window->window->window), 
			 (screen_width - window_width),
			 0 - window_height);   

	window->dist_vertically = window_height;
	window->dist_horizontally = window_width;

	window->show_timeout_id = g_timeout_add (10, (GSourceFunc)gj_gtk_toaster_move_to_show_cb, window);
	window->finish_timeout_id = g_timeout_add (window->seconds * 1000, (GSourceFunc)gj_gtk_toaster_timeout_cb, window);

	return TRUE;
}

gboolean gj_gtk_toaster_hide_and_free (GjToasterWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}  

	window->destroy_on_hide = TRUE;

	return gj_gtk_toaster_hide (window);
}

gboolean gj_gtk_toaster_hide (GjToasterWindow *window)
{
	GdkScreen *screen = NULL;
	gint screen_width = 0;
	gint screen_height = 0;
	gint window_width = 0;
	gint window_height = 0;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (window->window == NULL) {
		g_warning ("%s: window->window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (window->moving == TRUE) {
		g_warning ("%s: toaster already in action", __FUNCTION__);
		return FALSE;
	}

	if (window->shown == FALSE) {
		g_message ("%s: toaster not shown, nothing to hide", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: hiding toaster", __FUNCTION__);

	window->moving = TRUE;
	window->shown = FALSE;

	screen = gdk_screen_get_default ();
	screen_width = gdk_screen_get_width (screen);
	screen_height = gdk_screen_get_height (screen);

	gtk_widget_get_size_request (window->window, &window_width, &window_height);
	gdk_drawable_get_size (GDK_DRAWABLE (window->window->window), &window_width, &window_height);

	window->dist_vertically = window_height;
	window->dist_horizontally = window_width;

	window->hide_timeout_id = g_timeout_add (10, (GSourceFunc)gj_gtk_toaster_move_to_hide_cb, window);
	return TRUE;
}

static gboolean gj_gtk_toaster_move_to_show_cb (GjToasterWindow *window)
{
	gint current_x = 0;
	gint current_y = 0;
	gint move_x = 0;
	gint move_y = 0;

	if (window == NULL || window->window == NULL) {
		return FALSE;
	}
    
	gdk_window_get_root_origin (GDK_WINDOW (window->window->window), &current_x, &current_y); 

	if (window->dist_horizontally > 0) {
		move_x = 0;

		if (abs (current_x) < move_x) {
			move_x = abs (current_x);
		}

		window->dist_horizontally -= move_x;
	}

	if (window->dist_vertically > 0) {
		move_y = 2;

		if (abs (current_y) < move_y) {
			move_y = abs (current_y);
		}

		window->dist_vertically -= move_y;
	}

	gdk_window_move (GDK_WINDOW (window->window->window), 
			 current_x + move_x,
			 current_y + move_y);

	if (move_x == 0 && move_y == 0) {
		window->moving = FALSE;
		return FALSE;
	}
    
	return TRUE;
}

GtkWidget *gj_gtk_toaster_get_container (GjToasterWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return NULL;
	}   

	return window->window;
}

gboolean gj_gtk_toaster_get_is_shown (GjToasterWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}   

	return window->shown;    
}

gboolean gj_gtk_toaster_set_hide_and_free_timeout (GjToasterWindow *window, gint seconds)
{
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}       

	if (seconds < 2) {
		g_warning ("%s: can not shown the toaster for less than 2 seconds", __FUNCTION__);
		return FALSE;
	}

	if (window->finish_timeout_id > 0) {
		g_source_remove (window->finish_timeout_id);
		window->finish_timeout_id = 0;
	}

	window->seconds = seconds;
	window->destroy_on_hide = TRUE;
	window->finish_timeout_id = g_timeout_add (window->seconds * 1000, (GSourceFunc)gj_gtk_toaster_timeout_cb, window);

	return TRUE;
}

static gboolean gj_gtk_toaster_move_to_hide_cb (GjToasterWindow *window)
{
	gint current_x = 0;
	gint current_y = 0;
	gint move_x = 0;
	gint move_y = 0;

	if (window == NULL || window->window == NULL) {
		return FALSE;
	}
    
	gdk_window_get_root_origin (GDK_WINDOW (window->window->window), &current_x, &current_y); 

	if (window->dist_horizontally > 0) {
		move_x = 0;
		window->dist_horizontally -= move_x;
	}

	if (window->dist_vertically > 0) {
		move_y = 2;
		window->dist_vertically -= move_y;
	}

	gdk_window_move (GDK_WINDOW (window->window->window), 
			 current_x - move_x,
			 current_y - move_y);

	if (move_x == 0 && move_y == 0) {
		/* remove and cleanup on unload */
		window->moving = FALSE;

		if (window->destroy_on_hide == TRUE) {
			gtk_widget_destroy (window->window); 
		}

		return FALSE;
	}
    
	return TRUE;
}


static gboolean gj_gtk_toaster_timeout_cb (GjToasterWindow *window)
{
	gj_gtk_toaster_hide (window);

	return FALSE;
}

/*
 * actual gui functions
 */
static void on_destroy (GtkWidget *widget, GjToasterWindow *window)
{
	if (window == NULL) {
		return;
	}

	/* call callback */
	if (window->destroy_cb != NULL) {
		(window->destroy_cb) (window, window->user_data); 
	}

	if (window->show_timeout_id > 0) {
		g_source_remove (window->show_timeout_id);
		window->show_timeout_id = 0;
	}

	if (window->hide_timeout_id > 0) {
		g_source_remove (window->hide_timeout_id);
		window->hide_timeout_id = 0;
	}

	if (window->finish_timeout_id > 0) {
		g_source_remove (window->finish_timeout_id);
		window->finish_timeout_id = 0;
	}

	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjToasterWindow *window)
{
	gtk_widget_destroy (window->window);
	return FALSE;
}


#ifdef __cplusplus
}
#endif
