/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_gtk_stock_h
#define __gj_gtk_stock_h

#define GJ_STOCK_PRESENCE_AVAILABLE    "gj_presence_available"   
#define GJ_STOCK_PRESENCE_UNAVAILABLE  "gj_presence_unavailable"
#define GJ_STOCK_PRESENCE_CHAT         "gj_presence_chat"
#define GJ_STOCK_PRESENCE_DND          "gj_presence_dnd"  
#define GJ_STOCK_PRESENCE_AWAY         "gj_presence_away"
#define GJ_STOCK_PRESENCE_XA           "gj_presence_xa"
#define GJ_STOCK_PRESENCE_INVISIBLE    "gj_presence_invisible"  
#define GJ_STOCK_PRESENCE_NONE         "gj_presence_none"
#define GJ_STOCK_MESSAGE_NORMAL        "gj_message_normal"   
#define GJ_STOCK_MESSAGE_CHAT          "gj_message_chat"   
#define GJ_STOCK_MESSAGE_GROUPCHAT     "gj_message_groupchat"   
#define GJ_STOCK_MESSAGE_HEADLINE      "gj_message_headline"   
#define GJ_STOCK_MESSAGE_ERROR         "gj_message_error"   
#define GJ_STOCK_EMOTE_BIGGRIN         "gj_emote_biggrin"
#define GJ_STOCK_EMOTE_CONFUSED        "gj_emote_confused"
#define GJ_STOCK_EMOTE_COOL            "gj_emote_cool"
#define GJ_STOCK_EMOTE_EEK             "gj_emote_eek"
#define GJ_STOCK_EMOTE_FROWN           "gj_emote_frown"
#define GJ_STOCK_EMOTE_MAD             "gj_emote_mad"
#define GJ_STOCK_EMOTE_REDFACE         "gj_emote_redface"
#define GJ_STOCK_EMOTE_ROLLEYES        "gj_emote_rolleyes"
#define GJ_STOCK_EMOTE_SMILE           "gj_emote_smile"
#define GJ_STOCK_EMOTE_TONGUE          "gj_emote_tongue"
#define GJ_STOCK_EMOTE_WINK            "gj_emote_wink"

gboolean gj_gtk_stock_init (); 

#endif
