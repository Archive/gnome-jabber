/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_gtk_docklet_h
#define __gj_gtk_docklet_h

typedef void (*GjDockletClickCallback) (gboolean double_click, gint x, gint y, gpointer user_data);

gboolean gj_gtk_docklet_load (GjDockletClickCallback left_cb, 
			     GjDockletClickCallback right_cb,
			     gpointer user_data);

gboolean gj_gtk_docklet_unload ();

gboolean gj_gtk_docklet_set_image_from_stock (const gchar *stock_id);
gboolean gj_gtk_docklet_set_image_from_filename (const gchar *filename);

gboolean gj_gtk_docklet_set_hint (const gchar *hint);

#endif

#ifdef __cplusplus
}
#endif
