/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdio.h>

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#ifdef HAVE_LOCALE_H
#  include <locale.h>
#endif

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnet.h>

#ifndef G_OS_WIN32
#  include <gconf/gconf-client.h>
#  include <libgnome/gnome-init.h>
#  include <libgnome/gnome-i18n.h>
#  include <libgnome/gnome-program.h>
#else
#  include <windows.h>
#endif

#include "gj_main.h"
#include "gj_log.h"
#include "gj_config.h"
#include "gj_event.h"
#include "gj_support.h"

#include "gj_gtk_roster.h"
#include "gj_gtk_preferences.h"
#include "gj_gtk_first_time_run.h"
#include "gj_gtk_stock.h"

#define FILE_GLADE PACKAGE ".glade"
#define FILE_CSS PACKAGE ".css"


static gboolean gnome_jabber_init ();
static gboolean gnome_jabber_term ();

static void gnome_jabber_first_time_check ();
static gboolean gnome_jabber_first_time_setup ();
static gboolean gnome_jabber_files_check ();


struct {
	gchar *file_config;
	gchar *file_glade;
	gchar *file_css;

	gchar *dir_config;  

	gboolean first_time_run;

	gboolean debugging;
	gboolean log_to_screen;

#ifdef G_OS_WIN32
	HANDLE mutex_handle;
#endif

} gnome_jabber;


/***********************************************************************
 * JABBER SERVERS AND CONFERENCES / ROOMS
 ***********************************************************************/

const gchar *gnome_jabber_servers[] = {
	"12jabber.com",
	"aedsolucoes.com.br",
	"akl.lt",
	"amessage.at",
	"amessage.be",
	"amessage.ch",
	"amessage.de",
	"amessage.info",
	"amessage.li",
	"bloodyxml.com",
	"bluoptic.com",
	"borderlinenormal.com",
	"dhbit.ca",
	"eifelansinn.net",
	"es.tipic.com",
	"geodude.timmins.net",
	"ibericachat.com",
	"illx.org",
	"im.pizza.net",
	"jabber-fr.net",
	"jabber.2000-plus.pl",
	"jabber.anywise.com",
	"jabber.at",
	"jabber.atari-source.com",
	"jabber.bettercom.de",
	"jabber.cn",
	"jabber.com",
	"jabber.cyber-junk.de",
	"jabber.cz",
	"jabber.daemon.sh",
	"jabber.dk",
	"jabber.earth.li",
	"jabber.freenet.de",
	"jabber.gda.pl",
	"jabber.hu",
	"jabber.kelkoo.net",
	"jabber.kicks-ass.org",
	"jabber.kolbsoft.com",
	"jabber.linux.it",
	"jabber.lumina.ro",
	"jabber.mailclean.net",
	"jabber.mandog.com",
	"jabber.netsample.com",
	"jabber.ngnet.it",
	"jabber.noze.it",
	"jabber.or.id",
	"jabber.or.kr",
	"jabber.org",
	"jabber.palomine.net",
	"jabber.projecto-oasis.cx",
	"jabber.ro",
	"jabber.ru",
	"jabber.sk",
	"jabber.toble.com",
	"jabber.unesco.kz",
	"jabber.wiretrip.org",
	"jabber.wp.pl",
	"jabberlawine.de",
	"jabberpl.org",
	"kanga.nu",
	"myjabber.net",
	"nabla.net",
	"netmindz.net",
	"netnewmedia.de",
	"njs.netlab.cz",
	"nureality.ca",
	"openaether.org",
	"oracle.sweeney.ath.cx",
	"planet-linux.org",
	"rhymbox.com",
	"sml.dyndns.org",
	"sourcecode.de",
	"sucs.org",
	"swissjabber.ch",
	"syndicon.de",
	"theoretic.com",
	"tipic.com",
	"tipic.it",
	"xasa.com",
	NULL 
};

const gchar *gnome_jabber_conferences[] = {
	"jdev@conference.jabber.org",
	"jabber@conference.jabber.org",
	NULL,
};

/****************************************************************************/

int main (int argc, char **argv)
{
#ifndef G_OS_WIN32
	gboolean log_to_screen = FALSE;
	gboolean debugging = FALSE;

	GnomeProgram *program = NULL;

	struct poptOption options[] =  {
		{ "debugging",
		  'd',
		  POPT_ARG_NONE,
		  &debugging,
		  0,
		  N_("Logs more, uses local glade file, etc."),
		  NULL
		},
      		{ "log-to-screen",
		  'l',
		  POPT_ARG_NONE,
		  &log_to_screen,
		  0,
		  N_("Logs to screen (stdout)."),
		  NULL
		},
      
		{ NULL, '\0', 0, NULL, 0, NULL, NULL }
	};
#endif

	/* init variables */
	gnome_jabber_init ();

	/* sets the log handler */
	gj_log_init ();

	/* gtk / gdk / gnome initiations */
	if (gtk_init_check (&argc,&argv) == FALSE) {
		g_error ("%s: failed to initiate gtk", __FUNCTION__);
		exit (0);
	} else {
		g_message ("%s: initiated gtk", __FUNCTION__);
	}

	if (gdk_init_check (&argc,&argv) == FALSE) {
		g_error ("%s: failed to initiate gdk", __FUNCTION__);
		exit (0);
	} else {
		g_message ("%s: initiated gdk", __FUNCTION__);
	}

	gnet_init ();
	g_message ("%s: initiated gnet", __FUNCTION__);

/* 	p ("initiating libraries (gtk/glib/gnet)");  */

#ifdef HAVE_LOCALE_H
	/* internationalisation */
	setlocale (LC_ALL, "");
#endif

#ifdef HAVE_GETTEXT
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	textdomain (GETTEXT_PACKAGE);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
#endif

#ifndef G_OS_WIN32
	program = gnome_program_init (PACKAGE, VERSION,
				      LIBGNOME_MODULE,
				      argc, argv,
				      GNOME_PROGRAM_STANDARD_PROPERTIES,
				      GNOME_PARAM_ENABLE_SOUND, TRUE,
				      GNOME_PARAM_POPT_TABLE, options, 
				      GNOME_PARAM_HUMAN_READABLE_NAME, "GnomeJabber",
				      GNOME_PARAM_NONE);



	gnome_jabber_set_debugging (debugging);
	gnome_jabber_set_log_to_screen (log_to_screen);
#endif

	g_message ("%s: checking for required files", __FUNCTION__);
	if (!gnome_jabber_files_check ()) {
		return EXIT_FAILURE;
	}

#ifdef G_OS_WIN32
	/* create a mutex to stop the un/install being run while 
	   the application is still running */
	gnome_jabber.mutex_handle = CreateMutex (NULL, FALSE, PACKAGE_NAME); 
#endif

	/* check to see if we have run this app before? */
	g_message ("%s: checking for first time run", __FUNCTION__);
	gnome_jabber_first_time_check ();

	if (gnome_jabber_get_first_time_run ()) {
		gnome_jabber_first_time_setup ();
	}

	/* config */
	gj_config_init (gnome_jabber_get_config_file ());
	g_message ("%s: initiated config", __FUNCTION__);

	/* get config */
	gj_config_read ();
	g_message ("%s: read config", __FUNCTION__);

	/* logging */
	gj_log_start ();
	g_message ("%s: initiated logging", __FUNCTION__);

	/* stock icon init */
	if (gj_gtk_stock_init () == FALSE) {
		g_warning ("%s: stock icons could not be initialised", __FUNCTION__);
	} else {
		g_message ("%s: initialised stock icons", __FUNCTION__);
	}

	/* check for first time run */
	if(gnome_jabber_get_first_time_run() == TRUE) {
		g_message ("%s: running for the first time...", __FUNCTION__);
      
		/* so we load the connection settings too */
		gj_gtk_roster_load ();
		gj_gtk_ftr_load ();
	} else {
		/* roster */
		gj_gtk_roster_load ();
		g_message ("%s: initiated roster", __FUNCTION__);
	}
 
	/* start event loop */
	g_message ("%s: ready!", __FUNCTION__);
	gtk_main ();

	/* clean up variables */
	gnome_jabber_term ();

#ifdef G_OS_WIN32
	/* create a mutex to stop the un/install being run while 
	   the application is still running */
	ReleaseMutex (gnome_jabber.mutex_handle); 
#endif

	/* end application */
	g_message ("%s: exiting...", __FUNCTION__);

	return EXIT_SUCCESS;  
}

static void gnome_jabber_first_time_check ()
{
	G_CONST_RETURN gchar *home_dir = NULL;
	gchar *config_dir = NULL;

	home_dir = g_get_home_dir ();
	
#ifdef G_OS_WIN32
	config_dir = g_build_filename (home_dir, PACKAGE_NAME, NULL);
#else
	config_dir = g_build_filename (home_dir, ".gnome2", PACKAGE_NAME, NULL);
#endif

	if (g_file_test (config_dir,  (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)) == FALSE) {
		gnome_jabber_set_first_time_run (TRUE);
	} else {
		gnome_jabber_set_first_time_run (FALSE);
	}

	g_free (config_dir);
}

static gboolean gnome_jabber_first_time_setup ()
{
	if (gnome_jabber_get_first_time_run ()) {
		G_CONST_RETURN gchar *home_dir = NULL;
		gchar *config_dir = NULL;

		home_dir = g_get_home_dir ();

#ifdef G_OS_WIN32
		config_dir = g_build_filename (home_dir, PACKAGE_NAME, NULL);
#else
		config_dir = g_build_filename (home_dir, ".gnome2", PACKAGE_NAME, NULL);
#endif

		/* if this directory does not exist, it has not been run by this user before */
		if (!gj_create_dir (config_dir)) {
			g_warning ("%s: could not create config dir:'%s'", __FUNCTION__, config_dir);
			g_free (config_dir);
			return FALSE;
		}

		g_free (config_dir);
	}
}

static gboolean gnome_jabber_files_check ()
{
	G_CONST_RETURN gchar *home_dir = NULL;

	gchar *config_dir = NULL;
	gchar *file_config = NULL;
	gchar *file_glade = NULL;
	gchar *file_css = NULL;

	gchar *absolute_path = NULL;

	home_dir = g_get_home_dir ();
  	g_message ("%s: home directory is:'%s'", __FUNCTION__, home_dir); 

#ifdef G_OS_WIN32
	config_dir = g_build_filename (home_dir, PACKAGE_NAME, NULL);
#else
	config_dir = g_build_filename (home_dir, ".gnome2", PACKAGE_NAME, NULL);
#endif

	file_config = g_build_filename (config_dir, PACKAGE_NAME, FILE_CONFIG, NULL);

#ifdef G_OS_WIN32
	absolute_path = g_win32_get_package_installation_directory (PACKAGE_NAME, NULL);
	g_message ("%s: using installation path:'%s'", __FUNCTION__, absolute_path);

	file_glade = g_build_filename (absolute_path, FILE_GLADE, NULL);
	g_message ("%s: using glade file:'%s'", __FUNCTION__, file_glade);

	file_css = g_build_filename (absolute_path, FILE_CSS, NULL);
	g_message ("%s: using css file:'%s'", __FUNCTION__, file_css);
#else
	file_glade = g_build_filename (DATADIR, PACKAGE, FILE_GLADE, NULL);
	g_message ("%s: using glade file:'%s'", __FUNCTION__, file_glade);

	file_css = g_build_filename (DATADIR, PACKAGE, FILE_CSS, NULL);
	g_message ("%s: using css file:'%s'", __FUNCTION__, file_css);
#endif

	if (gnome_jabber_get_debugging ()) {
		g_message ("%s: using debug settings", __FUNCTION__);

		g_free (file_glade);
		file_glade = g_build_filename (FILE_GLADE, NULL);
		g_message ("%s: using local glade file (because debugging is on)", __FUNCTION__);
	}

	if (g_file_test (file_css, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)) == FALSE) {
		g_warning ("%s: file '%s' does NOT exist!", __FUNCTION__, file_css);
		return FALSE;
	}
  
	/* error if not found because the application is completely dependant on it. */
	if (g_file_test (file_glade,  (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)) == FALSE) {
		g_warning ("%s: file '%s' does NOT exist!", __FUNCTION__, file_glade);
		return FALSE;
	}

	/* save */
	gnome_jabber_set_config_dir (config_dir);
	gnome_jabber_set_config_file (file_config);
	gnome_jabber_set_glade_file (file_glade);
	gnome_jabber_set_css_file (file_css);

	/* clean up */
	g_free (config_dir);
	g_free (file_config);
	g_free (file_glade);
	g_free (file_css); 

	return TRUE;
}

/*
 * member functions 
 */

/* gets */
gchar *gnome_jabber_get_config_file ()
{
	return gnome_jabber.file_config;
}

gchar *gnome_jabber_get_glade_file ()
{
	return gnome_jabber.file_glade;
}

gchar *gnome_jabber_get_css_file ()
{
	return gnome_jabber.file_css;
}

gchar *gnome_jabber_get_config_dir ()
{
	return gnome_jabber.dir_config;
}

gboolean gnome_jabber_get_first_time_run ()
{
	return gnome_jabber.first_time_run;
}

gboolean gnome_jabber_get_debugging ()
{
	return gnome_jabber.debugging;
}

gboolean gnome_jabber_get_log_to_screen ()
{
	return gnome_jabber.log_to_screen;
}

const gchar **gnome_jabber_get_server_list ()
{
	return gnome_jabber_servers;
}

/* sets */
gboolean gnome_jabber_set_config_file (gchar *filename)
{
	if (filename == NULL) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gnome_jabber.file_config);
	gnome_jabber.file_config = g_strdup (filename);

	return TRUE;
}

gboolean gnome_jabber_set_glade_file (gchar *filename)
{
	if (filename == NULL) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gnome_jabber.file_glade);
	gnome_jabber.file_glade = g_strdup (filename);

	return TRUE;
}

gboolean gnome_jabber_set_css_file (gchar *filename)
{
	if (filename == NULL) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gnome_jabber.file_css);
	gnome_jabber.file_css = g_strdup (filename);

	return TRUE;
}

gboolean gnome_jabber_set_config_dir (gchar *dir)
{
	if (dir == NULL) {
		g_warning ("%s: dir was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gnome_jabber.dir_config);
	gnome_jabber.dir_config = g_strdup (dir);
	
	return TRUE;
}

gboolean gnome_jabber_set_first_time_run (gboolean first_time_run)
{
	gnome_jabber.first_time_run = first_time_run;
	return TRUE;
}

gboolean gnome_jabber_set_debugging (gboolean debugging)
{
	gnome_jabber.debugging = debugging;
	return TRUE;
}

gboolean gnome_jabber_set_log_to_screen (gboolean log_to_screen)
{
	gnome_jabber.log_to_screen = log_to_screen;
	return TRUE;
}

gboolean gnome_jabber_init ()
{
	gnome_jabber.file_config = NULL;
	gnome_jabber.file_glade = NULL;
	gnome_jabber.file_css = NULL;
	gnome_jabber.dir_config = NULL;

	gnome_jabber.first_time_run = FALSE;

	gnome_jabber.debugging = FALSE;
	gnome_jabber.log_to_screen = FALSE;

	return TRUE;
}

gboolean gnome_jabber_term ()
{
	g_free (gnome_jabber.file_config);
	g_free (gnome_jabber.file_glade);
	g_free (gnome_jabber.file_css);
	g_free (gnome_jabber.dir_config);
	
	gnome_jabber.first_time_run = FALSE;
 
	gnome_jabber.debugging = FALSE;
	gnome_jabber.log_to_screen = FALSE;
  
	return TRUE;
}

gboolean gnome_jabber_quit ()
{
	g_message ("received quit request");
	gtk_main_quit ();

	return TRUE;
}

#ifdef __cplusplus
}
#endif



