/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_log_h
#define __gj_log_h

#include <stdio.h>
#include <stdarg.h>

#define GJ_LOG_LEVEL_ERROR             1 << 2        /* - always fatal */
#define GJ_LOG_LEVEL_CRITICAL          1 << 3
#define GJ_LOG_LEVEL_WARNING           1 << 4
#define GJ_LOG_LEVEL_MESSAGE           1 << 5
#define GJ_LOG_LEVEL_INFO              1 << 6
#define GJ_LOG_LEVEL_DEBUG             1 << 7


static void p (const gchar *format, ...) {
	gchar *str;
	va_list args;

	va_start (args, format);
	str = g_strdup_vprintf (format, args);
	va_end (args);

	if (gnome_jabber_get_debugging ()) {
		printf ("%s\n", str);
		fflush (stdout);
	}

	g_free (str);
}

static void p_(const gchar *format, ...) {
	gchar *str;
	va_list args;

	va_start (args, format);
	str = g_strdup_vprintf (format, args);
	va_end (args);

	if (gnome_jabber_get_debugging ()) {
		printf ("%s", str);
		fflush (stdout);
	}

	g_free (str);
}


gboolean gj_log_init ();
gboolean gj_log_term ();

gboolean gj_log_start ();

void gj_log_handler (const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data);

#endif
