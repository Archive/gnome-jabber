/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>
#include <string.h>

#include <time.h>

#include <glib.h>

#include <libxml/tree.h>

#include "gj_message.h"
#include "gj_parser.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_support.h"
#include "gj_connection.h"
#include "gj_jid.h"
#include "gj_history.h"


/* to details, i.e. my state */
/* static GjMessageToCallback gj_message_get_cb_to (); */

/* from details, i.e. my contacts state (s) */
static GjMessageFromCallback gj_message_get_cb_from ();


static void gj_message_init ();

/* message item */
static GjMessage gj_message_new (GjMessageType type);
static gboolean gj_message_free (GjMessage m);

static void gj_message_destroy_cb (gpointer data);

/* 
 * member functions 
 */

/* sets */
static gboolean gj_message_set_connection (GjMessage m, GjConnection c);

static gboolean gj_message_set_type (GjMessage m, GjMessageType type);
static gboolean gj_message_set_id (GjMessage m, const gchar *id);

static gboolean gj_message_set_to_jid (GjMessage m, const gchar *jid);
static gboolean gj_message_set_from_jid (GjMessage m, const gchar *jid);

static gboolean gj_message_set_subject (GjMessage m, const gchar *subject);
static gboolean gj_message_set_body (GjMessage m, const gchar *body);

static gboolean gj_message_set_thread (GjMessage m, const gchar *thread);

static gboolean gj_message_set_x_event_type (GjMessage m, gint type);
static gboolean gj_message_set_x_event_message_id (GjMessage m, const gchar *message_id);

static gboolean gj_message_set_x_delay_stamp (GjMessage m, const gchar *stamp);

static gboolean gj_message_set_x_oob_url (GjMessage m, const gchar *url);
static gboolean gj_message_set_x_oob_description (GjMessage m, const gchar *description);

static gboolean gj_message_set_error_code (GjMessage m, const gchar *error_code);
static gboolean gj_message_set_error_reason (GjMessage m, const gchar *error_reason);

/* gets */


struct t_message {
	gint ref_count;

	GjConnection c;

	GjMessageType type;

	gint unique_id;

	gchar *id;

	GjJID to_jid;
	GjJID from_jid;
  
	gchar *subject;
	gchar *body;

	gchar *thread;

	/* x name spaces */
	gint x_event_type;
	gchar *x_event_message_id;

	gchar *x_delay_stamp;

	gchar *x_oob_url;
	gchar *x_oob_description;

	/* errors */
	gchar *error_code;
	gchar *error_reason;

	time_t time;
};


static GjMessageFromCallback message_cb_from;
/* static GjMessageToCallback message_cb_to;   */

static GHashTable *messages = NULL;


static void gj_message_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;

	messages = g_hash_table_new_full (g_int_hash, 
					  g_int_equal, 
					  NULL, 
					  (GDestroyNotify) gj_message_destroy_cb);   
}

void gj_message_stats ()
{
	g_message ("%s: table size:%d", __FUNCTION__, g_hash_table_size (messages));
}

static void gj_message_destroy_cb (gpointer data)
{
	GjMessage m = (GjMessage) data;

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return;
	}

	if (m->c) {
		gj_connection_unref (m->c);
	}

	g_free (m->id);
	g_free (m->to_jid);
	g_free (m->from_jid);
	g_free (m->subject);
	g_free (m->body);
	g_free (m->thread);
	g_free (m->x_event_message_id);
	g_free (m->x_delay_stamp);
	g_free (m->x_oob_url);
	g_free (m->x_oob_description);
	g_free (m->error_code);
	g_free (m->error_reason);
	
	g_free (m);
}

static GjMessage gj_message_new (GjMessageType type)
{
	GjMessage m = NULL;
 
	gj_message_init ();

	if ((m = g_new0 (struct t_message,1)) != NULL) {
		m->ref_count = 1;

		m->c = NULL;

		m->type = type;

		m->unique_id = gj_get_unique_id ();

		m->id = NULL;

		m->to_jid = NULL;
		m->from_jid = NULL;
      
		m->subject = NULL;
		m->body = NULL;

		m->thread = NULL;

		m->x_event_type = 0;
		m->x_event_message_id = NULL;

		m->x_delay_stamp = NULL;

		m->x_oob_url = NULL;
		m->x_oob_description = NULL;

		m->error_code = NULL;
		m->error_reason = NULL;

		m->time = time (0);

		g_hash_table_insert (messages, &m->unique_id, m);
	}
  
	/*   if (gnome_jabber_get_debugging ()) */
	/*     g_message ("%s: table size is:%d", __FUNCTION__, g_hash_table_size (messages));  */

	return m;  
}

static gboolean gj_message_free (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	g_hash_table_remove (messages, &m->unique_id);
	return TRUE;
}

GjMessage gj_message_ref (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  
  
	m->ref_count++;
	return m;
}

gboolean gj_message_unref (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	m->ref_count--;

	if (m->ref_count < 1) {
		gj_message_free (m);
	}

	return TRUE;
}


/*
 * member functions 
 */

/* sets */
gboolean gj_message_set_to_jid (GjMessage m, const gchar *jid)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m->to_jid != NULL) {
		gj_jid_unref (m->to_jid);
	}

	m->to_jid = gj_jid_new (jid);
	return TRUE;
}

static gboolean gj_message_set_connection (GjMessage m, GjConnection c)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	m->c = gj_connection_ref (c);
	return TRUE;
}

gboolean gj_message_set_type (GjMessage m, GjMessageType type)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (type < 1 || type > GjMessageTypeEnd) {
		g_warning ("%s: type was out of range", __FUNCTION__);
		return FALSE;
	}

	m->type = type;
	return TRUE;
}

gboolean gj_message_set_id (GjMessage m, const gchar *id)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->id);
	m->id = g_strdup (id);

	return TRUE;
}

gboolean gj_message_set_from_jid (GjMessage m, const gchar *jid)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m->from_jid) {
		gj_jid_unref (m->from_jid);
	}

	m->from_jid = gj_jid_new (jid);

	return TRUE;
}

gboolean gj_message_set_subject (GjMessage m, const gchar *subject)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (subject == NULL) {
		g_warning ("%s: subject was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->subject);
	m->subject = g_strdup (subject);

	return TRUE;
}

gboolean gj_message_set_body (GjMessage m, const gchar *body)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (body == NULL) {
		g_warning ("%s: body was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->body);
	m->body = g_strdup (body);

	return TRUE;
}

gboolean gj_message_set_thread (GjMessage m, const gchar *thread)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (thread == NULL) {
		g_warning ("%s: thread was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->thread);
	m->thread = g_strdup (thread);

	return TRUE;
}

gboolean gj_message_set_x_event_type (GjMessage m, gint type)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (type == 0) {
		g_warning ("%s: type was out of range", __FUNCTION__);
		return FALSE;
	}

	m->x_event_type = type;
	return TRUE;
}

gboolean gj_message_set_x_event_message_id (GjMessage m, const gchar *message_id)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (message_id == NULL) {
		g_warning ("%s: message id was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->x_event_message_id);
	m->x_event_message_id = g_strdup (message_id);

	return TRUE;
}

gboolean gj_message_set_x_delay_stamp (GjMessage m, const gchar *stamp)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (stamp == NULL) {
		g_warning ("%s: stamp was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->x_delay_stamp);
	m->x_delay_stamp = g_strdup (stamp);

	return TRUE;
}

gboolean gj_message_set_x_oob_url (GjMessage m, const gchar *url)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (url == NULL) {
		g_warning ("%s: url was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->x_oob_url);
	m->x_oob_url = g_strdup (url);

	return TRUE;
}

gboolean gj_message_set_x_oob_description (GjMessage m, const gchar *description)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (description == NULL) {
		g_warning ("%s: description was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->x_oob_description);
	m->x_oob_description = g_strdup (description);

	return TRUE;
}

gboolean gj_message_set_error_code (GjMessage m, const gchar *error_code)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (error_code == NULL) {
		g_warning ("%s: error code was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->error_code);
	m->error_code = g_strdup (error_code);

	return TRUE;
}

gboolean gj_message_set_error_reason (GjMessage m, const gchar *error_reason)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (error_reason == NULL) {
		g_warning ("%s: error reason was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (m->error_reason);
	m->error_reason = g_strdup (error_reason);

	return TRUE;
}

/* gets */
GjConnection gj_message_get_connection (GjMessage m)
{ 
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->c;
}

GjMessageType gj_message_get_type (GjMessage m)
{ 
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return -1;
	}  

	return m->type;
}

const gchar *gj_message_get_id (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->id;
}

gint gj_message_get_unique_id (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return -1;
	}  

	return m->unique_id;
}

const GjJID gj_message_get_to_jid (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->to_jid;
}

const GjJID gj_message_get_from_jid (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->from_jid;
}

const gchar *gj_message_get_subject (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->subject;
}

const gchar *gj_message_get_body (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->body;
}

const gchar *gj_message_get_thread (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->thread;
}

gint gj_message_get_x_event_type (GjMessage m)
{ 
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return -1;
	}  

	return m->x_event_type;
}

const gchar *gj_message_get_x_event_message_id (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->x_event_message_id;
}

const gchar *gj_message_get_x_delay_stamp (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->x_delay_stamp;
}

const gchar *gj_message_get_x_oob_url (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->x_oob_url;
}

const gchar *gj_message_get_x_oob_description (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->x_oob_description;
}

const gchar *gj_message_get_error_code (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  

	return m->error_code;
}

const gchar *gj_message_get_error_reason (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}  

	return m->error_reason;
}

time_t gj_message_get_time (GjMessage m)
{
	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return 0;
	}  

	return m->time;
}

/*
 * callbacks
 */
gboolean gj_message_set_cb_from (GjMessageFromCallback cb)
{
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	message_cb_from = cb;
	return TRUE;
}

GjMessageFromCallback gj_message_get_cb_from ()
{
	return message_cb_from;
}

/* gboolean gj_message_set_cb_to (GjMessageToCallback cb) */
/* { */
/*   if (cb == NULL) */
/*       g_warning ("%s: callback was NULL", __FUNCTION__); */
/*       return FALSE; */
/*     } */

/*   message_cb_to = cb; */
  
/*   return TRUE; */
/* } */

/*
 * external functions
 */
gboolean gj_message_send_by_ri (GjConnection c,
				GjMessageType type, 
				const gchar *from,
				GjRosterItem ri, 
				const gchar *subject, 
				const gchar *body, 
				const gchar *thread)
{
	GjJID j = NULL;
	j = gj_roster_item_get_jid (ri);

	/* record to history */
	gj_history_write_sent_by_ri_id (gj_roster_item_get_id (ri),
					(gchar*)gj_get_timestamp_nice (), 
					subject, 
					body);

	return gj_message_send (c,
				type, 
				from,
				gj_jid_get_full (j), 
				subject, 
				body, 
				thread);
}


gboolean gj_message_send (GjConnection c,
			  GjMessageType type, 
			  const gchar *from,
			  const gchar *to, 
			  const gchar *subject, 
			  const gchar *body, 
			  const gchar *thread)
{
	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *id = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (body == NULL) {
		g_warning ("%s: body was NULL", __FUNCTION__);
		return FALSE;
	}

	id = g_strdup_printf ("%d", gj_get_unique_id ());

	node = xmlNewNode (NULL, (guchar*)"message");

	xmlSetProp (node, (guchar*)"id", (guchar*)id);
	xmlSetProp (node, (guchar*)"to", (guchar*)to);
	xmlSetProp (node, (guchar*)"from", (guchar*)from);

	switch (type) {
	case GjMessageTypeNormal:
		xmlSetProp (node, (guchar*)"type", (guchar*)"normal");
		break;
	case GjMessageTypeChat:
		xmlSetProp (node, (guchar*)"type", (guchar*)"chat");
		break;
	case GjMessageTypeGroupChat:
		xmlSetProp (node, (guchar*)"type", (guchar*)"groupchat");
		break;
	case GjMessageTypeHeadline:
		xmlSetProp (node, (guchar*)"type", (guchar*)"headline");
		break;
	case GjMessageTypeError:
		xmlSetProp (node, (guchar*)"type", (guchar*)"error");
		break;
      
	default:
		g_warning ("%s: message type:%d was unknown", __FUNCTION__, type);
		return FALSE;
	}

	if (subject != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (subject, -1);
		sub = xmlNewChild (node, ns, (guchar*)"subject", (guchar*)formatted);
		g_free (formatted);
	}

	if (body != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (body, -1);
		sub = xmlNewChild (node, ns, (guchar*)"body", (guchar*)formatted);
		g_free (formatted);
	}

	if (thread != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (thread, -1);
		sub = xmlNewChild (node, ns, (guchar*)"thread", (guchar*)formatted);
		g_free (formatted);
	}

	gj_parser_push_output (c, node);

	/* clean up */
	g_free (id);

	return TRUE;
}


gboolean gj_message_send_x_event_by_ri (GjConnection c, 
					gint xevents, 
					const gchar *from,
					GjRosterItem ri, 
					const gchar *id)
{
	GjJID j = NULL;
	j = gj_roster_item_get_jid (ri);
  
	return gj_message_send_x_event (c, 
					xevents, 
					from,
					gj_jid_get_full (j), 
					id);
}

gboolean gj_message_send_x_event (GjConnection c, 
				  gint xevents, 
				  const gchar *from,
				  const gchar *to, 
				  const gchar *id)
{
	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *event = NULL;
	gchar *m = NULL;

	if (xevents == 0) {
		g_warning ("%s: invalid xevent (s)", __FUNCTION__);
		return FALSE;
	}

	if (! (xevents & GjMessageXEventTypeOffline) && 
	    ! (xevents & GjMessageXEventTypeDelivered) &&
	    ! (xevents & GjMessageXEventTypeDisplayed) &&   
	    ! (xevents & GjMessageXEventTypeComposing) &&
	    ! (xevents & GjMessageXEventTypeCancelled))	{
		g_warning ("%s: invalid xevent type", __FUNCTION__);
		return FALSE;
	}

	node = xmlNewNode (NULL, (guchar*)"message");

	xmlSetProp (node, (guchar*)"to", (guchar*)to);
	xmlSetProp (node, (guchar*)"from", (guchar*)from);

	sub = xmlNewNode (NULL, (guchar*)"x");
	ns = xmlNewNs (sub, (guchar*)"jabber:x:event", NULL);

	if (! (xevents & GjMessageXEventTypeCancelled)) {
		if (xevents & GjMessageXEventTypeOffline) {
			xmlNewChild (sub, ns, (guchar*)"offline", NULL);
		}

		if (xevents & GjMessageXEventTypeDelivered) {
			xmlNewChild (sub, ns, (guchar*)"delivered", NULL);
		}

		if (xevents & GjMessageXEventTypeDisplayed) {
			xmlNewChild (sub, ns, (guchar*)"displayed", NULL);
		}

		if (xevents & GjMessageXEventTypeComposing) {
			xmlNewChild (sub, ns, (guchar*)"composing", NULL); 
		}
	}

	if (id != NULL) {
		xmlNewChild (sub, ns, (guchar*)"id", (guchar*)id);
	}

	xmlAddChild (node, sub);

	/* send */
	gj_parser_push_output (c, node);

	/* clean up */
	g_free (event);
	g_free (m);

	return TRUE;
}

gchar *gj_message_send_with_x_event_request_by_ri (GjConnection c, 
						   GjMessageType type, 
						   const gchar *from,
						   GjRosterItem ri, 
						   const gchar *subject, 
						   const gchar *body, 
						   const gchar *thread, 
						   gint xevents)
{
	GjJID j = NULL;
	j = gj_roster_item_get_jid (ri);

	/* record to history */
	gj_history_write_sent_by_ri_id (gj_roster_item_get_id (ri),
					(gchar*)gj_get_timestamp_nice (), 
					subject, 
					body);

	return gj_message_send_with_x_event_request (c,
						     type, 
						     from,
						     gj_jid_get_full (j),
						     subject,
						     body,
						     thread,
						     xevents);
}

gchar *gj_message_send_with_x_event_request (GjConnection c, 
					     GjMessageType type, 
					     const gchar *from,
					     const gchar *to, 
					     const gchar *subject, 
					     const gchar *body, 
					     const gchar *thread, 
					     gint xevents)
{
	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *mtype = NULL;
	gchar *id = NULL;


	if (type < 1 || type > GjMessageTypeEnd) {
		g_warning ("%s: message type out of range", __FUNCTION__);
		return NULL;
	}

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return NULL;
	}

	if (body == NULL) {
		g_warning ("%s: body was NULL", __FUNCTION__);
		return NULL;
	}

	if (! (xevents & GjMessageXEventTypeOffline) && 
	    ! (xevents & GjMessageXEventTypeDelivered) &&
	    ! (xevents & GjMessageXEventTypeDisplayed) &&   
	    ! (xevents & GjMessageXEventTypeComposing) &&
	    ! (xevents & GjMessageXEventTypeCancelled))	{
		g_warning ("%s: invalid xevent type", __FUNCTION__);
		return NULL;
	}

	switch (type) {
	case GjMessageTypeNormal:
		mtype = g_strdup_printf ("normal");
		break;
	case GjMessageTypeChat:
		mtype = g_strdup_printf ("chat");
		break;
	case GjMessageTypeGroupChat:
		mtype = g_strdup_printf ("groupchat");
		break;
	case GjMessageTypeHeadline:
		mtype = g_strdup_printf ("headline");
		break;
	case GjMessageTypeError:
		mtype = g_strdup_printf ("error");
		break;

	default:
		g_warning ("%s: message type:%d was unknown", __FUNCTION__, type);
		return NULL;
	}

	id = g_strdup_printf ("%d", gj_get_unique_id ());

	node = xmlNewNode (NULL, (guchar*)"message");

	xmlSetProp (node, (guchar*)"id", (guchar*)id);
	xmlSetProp (node, (guchar*)"to", (guchar*)to);
	xmlSetProp (node, (guchar*)"from", (guchar*)from);
	xmlSetProp (node, (guchar*)"type", (guchar*)mtype);

	if (subject != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (subject, -1);
		sub = xmlNewChild (node, ns, (guchar*)"subject", (guchar*)formatted);
		g_free (formatted);
	}

	if (body != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (body, -1);
		sub = xmlNewChild (node, ns, (guchar*)"body", (guchar*)formatted);
		g_free (formatted);
	}

	if (thread != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (thread, -1);
		sub = xmlNewChild (node, ns, (guchar*)"thread", (guchar*)formatted);
		g_free (formatted);
	}

	sub = xmlNewNode (NULL, (guchar*)"x");
	ns = xmlNewNs (sub, (guchar*)"jabber:x:event", NULL);

	if (! (xevents & GjMessageXEventTypeCancelled)) {
		if (xevents & GjMessageXEventTypeOffline) {
			xmlNewChild (sub, ns, (guchar*)"offline", NULL);
		}

		if (xevents & GjMessageXEventTypeDelivered) {
			xmlNewChild (sub, ns, (guchar*)"delivered", NULL);
		}

		if (xevents & GjMessageXEventTypeDisplayed) {
			xmlNewChild (sub, ns, (guchar*)"displayed", NULL);
		}

		if (xevents & GjMessageXEventTypeComposing) {
			xmlNewChild (sub, ns, (guchar*)"composing", NULL);
		}
	}

	xmlAddChild (node, sub);

	gj_parser_push_output (c, node);

	/* clean up */
	g_free (mtype);

	return id;    
}


void gj_message_receive (GjConnection c, xmlNodePtr node)
{
	xmlNodePtr x = NULL;

	gint x_event_type = 0;

	xmlChar *stype = NULL;
	const xmlChar *tmp = NULL;

	GjMessage m = NULL;
	GjMessageType type = 0;
	GjMessageFromCallback cb = NULL;
	GjRosterItem ri = NULL;

	/* checks */
	if (node == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return;
	}

	/* get info */
	if ((cb = gj_message_get_cb_from ()) == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	/*
	 * create our message struct
	 */
	m = gj_message_new (GjMessageTypeNormal); 

	if (m == NULL) {
		g_warning ("%s: could not create new message", __FUNCTION__);
		return;
	}
    
	if ((stype = gj_parser_find_by_attr_and_ret (node, (guchar*)"type")) != NULL) {
		if (xmlStrcmp (stype, (guchar*)"normal") == 0) {
			type = GjMessageTypeNormal;
		} else if (xmlStrcmp (stype, (guchar*)"chat") == 0) {
			type = GjMessageTypeChat;
		} else if (xmlStrcmp (stype, (guchar*)"groupchat") == 0) {
			type = GjMessageTypeGroupChat;
		} else if (xmlStrcmp (stype, (guchar*)"headline") == 0) {
			type = GjMessageTypeHeadline;
		} else if (xmlStrcmp (stype, (guchar*)"error") == 0) {
			type = GjMessageTypeError;
		}
	}

	if ((x = gj_parser_find_by_node (node, (guchar*)"x")) != NULL) {
		if (xmlStrcmp (gj_parser_find_by_ns_and_ret (x), (guchar*)"jabber:x:delay") == 0) {
			if ((tmp = gj_parser_find_by_attr_and_ret (x, (guchar*)"stamp")) != NULL) {
				gj_message_set_x_delay_stamp (m, (gchar*)tmp);
			}
		} else if (xmlStrcmp (gj_parser_find_by_ns_and_ret (x), (guchar*)"jabber:x:event") == 0) {
			if (type == 0) {
				type = GjMessageTypeXEvent;
			}
			
			if (gj_parser_find_by_node (x, (guchar*)"offline") != NULL) {
				x_event_type |= GjMessageXEventTypeOffline;
			}

			if (gj_parser_find_by_node (x, (guchar*)"delivered") != NULL) {
				x_event_type |= GjMessageXEventTypeDelivered;
			}

			if (gj_parser_find_by_node (x, (guchar*)"displayed") != NULL) {
				x_event_type |= GjMessageXEventTypeDisplayed;
			}

			if (gj_parser_find_by_node (x, (guchar*)"composing") != NULL) {
				x_event_type |= GjMessageXEventTypeComposing;
			}
			
			if (x_event_type == 0) {
				x_event_type = GjMessageXEventTypeCancelled;
			}
			
			if ((tmp = gj_parser_find_by_node_on_parent_and_ret (x, (guchar*)"id")) != NULL) {
				gj_message_set_x_event_message_id (m, (gchar*)tmp); 
			}
		} else if (xmlStrcmp (gj_parser_find_by_ns_and_ret (x), (guchar*)"jabber:x:oob") == 0) {
			if ((tmp = gj_parser_find_by_node_on_parent_and_ret (x, (guchar*)"url")) != NULL) {
				gj_message_set_x_oob_url (m, (gchar*)tmp);
			}

			if ((tmp = gj_parser_find_by_node_on_parent_and_ret (x, (guchar*)"desc")) != NULL) {
					gj_message_set_x_oob_description (m, (gchar*)tmp);
			}
		}
	}

	if (type == 0) {
		type = GjMessageTypeNormal;
	}

	if ((tmp = gj_parser_find_by_node_on_parent_and_ret (node, (guchar*)"subject")) != NULL) {
		gj_message_set_subject (m, (gchar*)tmp);
	}
  
	if ((tmp = gj_parser_find_by_node_on_parent_and_ret (node, (guchar*)"body")) != NULL) {
		gj_message_set_body (m, (gchar*)tmp);
	}
  
	if ((tmp = gj_parser_find_by_node_on_parent_and_ret (node, (guchar*)"thread")) != NULL) {
		gj_message_set_thread (m, (gchar*)tmp);
	}
  
	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"id")) != NULL) {
		gj_message_set_id (m, (gchar*)tmp);
	}

	/* get 'to' field and separate resource and user */
	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"to")) != NULL && xmlStrlen (tmp) > 0) {
		gj_message_set_to_jid (m, (gchar*)tmp);
	}
  
	/* get 'from' field and separate resource and user */
	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"from")) != NULL && xmlStrlen (tmp) > 0) {
		gj_message_set_from_jid (m, (gchar*)tmp);
	}

	/* get error */
	if (type == GjMessageTypeError) {
		xmlChar *tmp = NULL;
		xmlNodePtr error = NULL;

		if ((error = gj_parser_find_by_node (node, (guchar*)"error")) != NULL) {
			if ((tmp = gj_parser_find_by_attr_and_ret (error, (guchar*)"code")) != NULL) {
				gj_message_set_error_code (m, (gchar*)tmp);
			}

			if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"error")) != NULL) {
				gj_message_set_error_reason (m, (gchar*)tmp);
			}
		}
	}

	/* FIXME: not sure if this is right but its the best way at the moment that i 
	   can think of for fixing the problem */
	if (gj_message_get_body (m) == NULL && x_event_type != 0) {
		type = GjMessageTypeXEvent;
	}

	gj_message_set_type (m, type);

	if (x_event_type != 0) {
		gj_message_set_x_event_type (m, x_event_type);
	}

	if (c) {
		gj_message_set_connection (m,c);
	}

	/* look for roster item to record history */
	ri = gj_rosters_find_item_by_jid (gj_message_get_from_jid (m));
	if (ri != NULL) {
		/* record to history */
		gj_history_write_received_by_ri_id (gj_roster_item_get_id (ri),
						    (gchar*)gj_get_timestamp_nice (), 
						    gj_message_get_subject (m), 
						    gj_message_get_body (m));
	}

	/* call callback function */
	(cb) (m);

	/* clean up */
	gj_message_unref (m);
}

#ifdef __cplusplus
}
#endif
