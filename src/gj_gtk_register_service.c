/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <glade/glade.h>
#include <gtk/gtk.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_register.h"
#include "gj_parser.h"
#include "gj_connection.h"

#include "gj_gtk_register_service.h"
#include "gj_gtk_support.h"


typedef struct  {
	/* search */
	GtkWidget *window;

	GtkWidget *label_instructions;
	GtkWidget *label_service;

	GtkWidget *label_username;
	GtkWidget *label_password;
	GtkWidget *label_email;
	GtkWidget *label_nickname;
	GtkWidget *entry_username;  
	GtkWidget *entry_password;
	GtkWidget *entry_email;
	GtkWidget *entry_nickname;

	GtkWidget *button_ok;
	GtkWidget *button_cancel;

	/* 
	 * member functions 
	 */
	GjConnection c;

	gchar *key;              /* key used as part of registration */

	gchar *service;

	gboolean require_username;
	gboolean require_password;
	gboolean require_email;
	gboolean require_nickname;

	GtkWidget *progress_dialog;
	gboolean progress_cancelled;

} GjRegisterServiceDialog;


static gboolean gj_gtk_rs_check (GjRegisterServiceDialog *dialog);

static void gj_gtk_rs_requirements_cb (GjRegistration reg, 
				       gint error_code, 
				       const gchar *error_reason, 
				       gpointer user_data);

static void gj_gtk_rs_register_cb (GjRegistration reg, 
				   gint error_code, 
				   const gchar *error_reason, 
				   gpointer user_data);

static void gj_gtk_rs_remove_requirements_cb (GjRegistration reg, 
					      gint error_code, 
					      const gchar *error_reason,
					      gpointer user_data);

static void gj_gtk_rs_remove_cb (GjRegistration reg, 
				 gint error_code, 
				 const gchar *error_reason,
				 gpointer user_data);

/* internal callbacks */
static void gj_gtk_rs_requirements_progress_cb (GtkDialog *widget, 
						gint response, 
						GjRegisterServiceDialog *dialog);

static void gj_gtk_rs_register_progress_cb (GtkDialog *widget, 
					    gint response, 
					    GjRegisterServiceDialog *dialog);


/* actual GUI functions */
static void on_entry_changed (GtkEditable *editable, GjRegisterServiceDialog *dialog);

static void on_destroy (GtkWidget *widget, GjRegisterServiceDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjRegisterServiceDialog *dialog);


static GjRegisterServiceDialog *current_dialog = NULL;


gboolean gj_gtk_rs_load (GjConnection c, const gchar *service)
{
	GjRegisterServiceDialog *dialog = NULL;
	GladeXML *xml = NULL;
  
	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (service == NULL) {
		g_warning ("%s: service was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjRegisterServiceDialog, 1);

	dialog->c = gj_connection_ref (c);

	dialog->service = g_strdup (service);

	dialog->require_username = FALSE;
	dialog->require_password = FALSE;
	dialog->require_email = FALSE;
	dialog->require_nickname = FALSE;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "register_service",
				     NULL,
				     "register_service", &dialog->window,
				     "label_instructions", &dialog->label_instructions,
				     "label_service", &dialog->label_service,
				     "label_username", &dialog->label_username,
				     "label_password", &dialog->label_password,
				     "label_email", &dialog->label_email,
				     "label_nickname", &dialog->label_nickname,
				     "entry_username", &dialog->entry_username,
				     "entry_password", &dialog->entry_password,
				     "entry_email", &dialog->entry_email,
				     "entry_nickname", &dialog->entry_nickname,
				     "button_ok", &dialog->button_ok,
				     "button_cancel", &dialog->button_cancel,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "register_service", "response", on_response,
			      "register_service", "destroy", on_destroy,
			      "entry_username", "changed", on_entry_changed,
			      "entry_password", "changed", on_entry_changed,
			      "entry_email", "changed", on_entry_changed,
			      "entry_nickname", "changed", on_entry_changed,
			      NULL);

	g_object_unref (xml);

	/* setup */
	gtk_label_set_text (GTK_LABEL (dialog->label_service), service);

	dialog->progress_cancelled = FALSE;
	dialog->progress_dialog = gj_gtk_hig_progress_new (GTK_WINDOW (dialog->window),
							   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							   (GCallback)gj_gtk_rs_requirements_progress_cb,
							   dialog,
							   _("Progress"),
							   _("Requesting Registration Details for Service"), 
							   _("Service is \"%s\""), 
							   service);

	gtk_widget_show_all (dialog->progress_dialog);

	/* get request authentication from server */
	gj_register_requirements (dialog->c, 
				  service, 
				  gj_gtk_rs_requirements_cb, 
				  NULL);  

	return TRUE;
}

static gboolean gj_gtk_rs_check (GjRegisterServiceDialog *dialog)
{
	G_CONST_RETURN gchar *username = NULL;
	G_CONST_RETURN gchar *password = NULL;
	G_CONST_RETURN gchar *email = NULL;
	G_CONST_RETURN gchar *nickname = NULL;
  
	if (!dialog) {
		return FALSE; 
	}

	username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));
	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password));
	email = gtk_entry_get_text (GTK_ENTRY (dialog->entry_email));
	nickname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_nickname));

	if (dialog->require_username && (!username || g_utf8_strlen (username, -1) < 1)) {
		return FALSE;
	}
  
	if (dialog->require_password && (!password || g_utf8_strlen (password, -1) < 1)) {
		return FALSE;
	}

	if (dialog->require_email && (!email || g_utf8_strlen (email, -1) < 1)) {
		return FALSE;
	}

	if (dialog->require_nickname && (!nickname || g_utf8_strlen (nickname, -1) < 1)) {
		return FALSE;
	}

	return TRUE;
}

static void gj_gtk_rs_requirements_cb (GjRegistration reg, 
				       gint error_code, 
				       const gchar *error_reason,
				       gpointer user_data)
{
	g_message ("%s: received response.", __FUNCTION__);

	if (!current_dialog) { 
		return;
	}

	if (!reg) {
		g_warning ("%s: reg was NULL", __FUNCTION__);
		return;
	}

	if (current_dialog->progress_dialog) {
		gtk_widget_destroy (current_dialog->progress_dialog);
		current_dialog->progress_dialog = NULL;
	}

	if (error_code != 0 || error_reason != NULL) {
		/* if error close dialog */
		gtk_widget_destroy (current_dialog->window);

		/* warn about error - usually a timeout */
		gj_gtk_show_error (_("Failed to Register Service."),
				   error_code,
				   error_reason);
 
		return;
	}

	/* show window */
	gtk_widget_show (current_dialog->window);

	gdk_window_set_decorations (GDK_WINDOW (current_dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* enable buttons */
	gtk_widget_show (current_dialog->button_ok);
	gtk_widget_show (current_dialog->button_cancel);

	gtk_widget_set_sensitive (current_dialog->button_ok, FALSE);

	/* set up controls */
	gtk_label_set_text (GTK_LABEL (current_dialog->label_instructions), gj_register_get_instructions (reg));

	current_dialog->require_username = gj_register_get_require_username (reg);
	current_dialog->require_password = gj_register_get_require_password (reg);
	current_dialog->require_email = gj_register_get_require_email (reg);
	current_dialog->require_nickname = gj_register_get_require_nickname (reg);

	if (current_dialog->require_username) {
		gtk_widget_show (current_dialog->label_username);
		gtk_widget_show (current_dialog->entry_username);
	} else {
		gtk_widget_hide (current_dialog->label_username);
		gtk_widget_hide (current_dialog->entry_username);
	}

	if (current_dialog->require_password) {
		gtk_widget_show (current_dialog->label_password);
		gtk_widget_show (current_dialog->entry_password);
		current_dialog->require_password = TRUE;
	} else {
		gtk_widget_hide (current_dialog->label_password);
		gtk_widget_hide (current_dialog->entry_password);
	}

	if (current_dialog->require_email) {
		gtk_widget_show (current_dialog->label_email);
		gtk_widget_show (current_dialog->entry_email);
		current_dialog->require_email = TRUE;
	} else {
		gtk_widget_hide (current_dialog->label_email);
		gtk_widget_hide (current_dialog->entry_email);
	}

	if (current_dialog->require_nickname) {
		gtk_widget_show (current_dialog->label_nickname);
		gtk_widget_show (current_dialog->entry_nickname);
		current_dialog->require_nickname = TRUE;
	} else {
		gtk_widget_hide (current_dialog->label_nickname);
		gtk_widget_hide (current_dialog->entry_nickname);
	}

	if (gj_register_get_key (reg) == NULL) {
		g_warning ("%s: no registration key for response", __FUNCTION__);
		return;
	}

	current_dialog->key = g_strdup (gj_register_get_key (reg));
}

static void gj_gtk_rs_register_cb (GjRegistration reg, 
				   gint error_code, 
				   const gchar *error_reason,
				   gpointer user_data)
{
	g_message ("%s: registration response", __FUNCTION__);
  
	if (!current_dialog) {
		return;
	}

	gtk_widget_destroy (current_dialog->window);

	if (error_code == 0 && error_reason == NULL) {
		/* service not entered */
		gj_gtk_show_success (_("Registration Success."));
	} else {
		gj_gtk_show_error (_("Failed to Register Service."),
				   error_code,
				   error_reason);
	}
}

gboolean gj_gtk_rs_remove (GjConnection c, const gchar *service)
{
	return gj_register_requirements (c, 
					 service, 
					 gj_gtk_rs_remove_requirements_cb, 
					 NULL);  
}

static void gj_gtk_rs_remove_requirements_cb (GjRegistration reg, 
					      gint error_code, 
					      const gchar *error_reason,
					      gpointer user_data)
{
	const gchar *key = NULL;
	const gchar *server = NULL;

	g_message ("%s: received response.", __FUNCTION__);

	if (!reg) {
		g_warning ("%s: reg was NULL", __FUNCTION__);
		return;
	}

	if (error_code != 0 || error_reason != NULL) {
		/* warn about error - usually a timeout */
		gj_gtk_show_error (_("Failed to Register Service."),
				   error_code,
				   error_reason);
 
		return;
	}

	server = gj_register_get_from (reg);
	key = gj_register_get_key (reg);

	if (!server) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return;
	}

	if (!key) {
		g_warning ("%s: registration key was NULL", __FUNCTION__);
		return;
	}

	gj_register_remove (gj_register_get_connection (reg), 
			    server, 
			    key, 
			    NULL, 
			    NULL, 
			    NULL, 
			    NULL, 
			    gj_gtk_rs_remove_cb, 
			    NULL);
}

static void gj_gtk_rs_remove_cb (GjRegistration reg, 
				 gint error_code, 
				 const gchar *error_reason,
				 gpointer user_data)
{
	if (error_code != 0 || error_reason != NULL) {
		/* warn about error - usually a timeout */
		gj_gtk_show_error (_("Failed to Register Service."),
				   error_code,
				   error_reason);
 
		return;
	}  
}

static void gj_gtk_rs_requirements_progress_cb (GtkDialog *widget, gint response, GjRegisterServiceDialog *dialog)
{
	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_DELETE_EVENT) {
		dialog->progress_cancelled = TRUE;
	}

	gtk_widget_destroy (dialog->window);
}

static void gj_gtk_rs_register_progress_cb (GtkDialog *widget, gint response, GjRegisterServiceDialog *dialog)
{
	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_DELETE_EVENT) { 
		dialog->progress_cancelled = TRUE;
	}

	gtk_widget_destroy (dialog->window);
}

/*
 * actual gui functions
 */
static void on_entry_changed (GtkEditable *editable, GjRegisterServiceDialog *dialog)
{
	gtk_widget_set_sensitive (dialog->button_ok, gj_gtk_rs_check (dialog));
}

static void on_destroy (GtkWidget *widget, GjRegisterServiceDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);

	g_free (dialog->key);
	g_free (dialog->service);

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjRegisterServiceDialog *dialog)
{
	G_CONST_RETURN gchar *username = NULL;
	G_CONST_RETURN gchar *password = NULL;
	G_CONST_RETURN gchar *email = NULL;
	G_CONST_RETURN gchar *nickname = NULL;

	if (response != GTK_RESPONSE_OK) {
		gtk_widget_destroy (dialog->window);
		return;
	}

	if (dialog->require_username) {
		username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));
	}

	if (dialog->require_password) {
		password = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password));
	}

	if (dialog->require_email) {
		email = gtk_entry_get_text (GTK_ENTRY (dialog->entry_email));
	}

	if (dialog->require_nickname) {
		nickname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_nickname));
	}
  
	dialog->progress_cancelled = FALSE;
	dialog->progress_dialog = gj_gtk_hig_progress_new (GTK_WINDOW (dialog->window),
							   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							   (GCallback)gj_gtk_rs_register_progress_cb,
							   dialog,
							   _("Progress"),
							   _("Attempting to Register Service"), 
							   _("Service is \"%s\""), 
							   dialog->service);
  
	gtk_widget_show_all (dialog->progress_dialog);

	gj_register_request (dialog->c,
			     dialog->service, 
			     dialog->key, 
			     username,
			     password, 
			     email, 
			     nickname,
			     gj_gtk_rs_register_cb, 
			     NULL);
}

#ifdef __cplusplus
}
#endif
