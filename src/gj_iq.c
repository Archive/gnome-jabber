/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h> 

#include <glib.h>

#include "gj_iq.h"
#include "gj_iq_responses.h"
#include "gj_parser.h"
#include "gj_roster.h"
#include "gj_support.h"
#include "gj_connection.h"


#define GJ_IQ_GARBAGE_COLLECT_TIMEOUT 30000 /* ms */


/* structs */
struct t_infoquery
{
	gint ref_count;

	guint event_id;
	guint related_event_id;

	GjConnection c;

	GjInfoqueryType type;
	GjInfoqueryCallback cb;

	void *user_data;

	gchar *id;
	gchar *from;
	gchar *to;

	gboolean ready_for_garbage_collection;

	xmlNodePtr xml;
};


static void gj_iq_init ();

static void gj_iq_free (GjInfoquery iq);
static void gj_iq_destroy_cb (gpointer data);

static gboolean gj_iq_garbage_collect_cb (gpointer user_data);
static void gj_iq_garbage_collect_foreach_cb (gpointer key,
					      gpointer value,
					      gpointer user_data);


/* checks */
static gboolean gj_iq_check_params (GjInfoquery iq);

static GHashTable *iqs = NULL;
static gint iqs_garbage_collect_id = 0;


static void gj_iq_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;

	iqs = g_hash_table_new_full (g_int_hash, 
				     g_int_equal, 
				     NULL, 
				     (GDestroyNotify) gj_iq_destroy_cb);   

#if 0
	iqs_garbage_collect_id = g_timeout_add (GJ_IQ_GARBAGE_COLLECT_TIMEOUT, 
						gj_iq_garbage_collect_cb, 
						NULL);
#endif 
}

void gj_iq_stats ()
{
	g_message ("%s: table size:%d", __FUNCTION__, g_hash_table_size (iqs));
}

static gboolean gj_iq_garbage_collect_cb (gpointer user_data)
{
	if (g_hash_table_size (iqs) < 1) {
		return TRUE;
	}

	g_hash_table_foreach (iqs, gj_iq_garbage_collect_foreach_cb, NULL);
	return TRUE;
}

static void gj_iq_garbage_collect_foreach_cb (gpointer key,
					      gpointer value,
					      gpointer user_data)
{
	GjInfoquery iq = NULL;
	gint *id = NULL;

	if (key == NULL || value == NULL) {
		return;
	}

	id = (gint*)key;

	iq = gj_iq_find (*id);

	if (iq == NULL) {
		return;
	}

	/* if not ready - set ready so next time it is removed */
	if (iq->ready_for_garbage_collection == FALSE) {
		g_message ("%s: setting IQ (with id:%d) ready for garbage collection", __FUNCTION__, iq->event_id);
		iq->ready_for_garbage_collection = TRUE;
		return;
	}

	/* if is ready for garbage collection, free it */
	g_message ("%s: garbage collecting IQ (with id:%d)", __FUNCTION__, (gint)iq->event_id);
	gj_iq_free (iq);
}

static void gj_iq_destroy_cb (gpointer data)
{
	GjInfoquery iq = (GjInfoquery) data;

	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return;
	}

	if (iq->c) {
		gj_connection_unref (iq->c);
	}

	if (iq->xml != NULL) {
		xmlFreeNode (iq->xml); 
	}

	g_free (iq->id);
	g_free (iq->to);
	g_free (iq->from);

	iq->ready_for_garbage_collection = FALSE;

	g_free (iq);
}

GjInfoquery gj_iq_new (GjInfoqueryType type)
{
	GjInfoquery iq = NULL;
  
	if (type < 1 || type > GjInfoqueryTypeEnd) {
		g_warning ("%s: infoquery type:%d was out of bounds", __FUNCTION__, type);
		return NULL;
	}

	gj_iq_init ();

	iq = g_new0 (struct t_infoquery, 1);
  
	iq->ref_count = 1;
  
	iq->event_id = gj_get_unique_id ();

	iq->type = type;

	g_hash_table_insert (iqs, &iq->event_id, iq);

	return iq;  
}

GjInfoquery gj_iq_ref (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return NULL;
	}  
  
	iq->ref_count++;

	return iq;
}

gboolean gj_iq_unref (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	iq->ref_count--;

	if (iq->ref_count < 1) {
		gj_iq_free (iq);
	}

	return TRUE;
}

static void gj_iq_free (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return;
	}

	g_hash_table_remove (iqs, &iq->event_id);
}

/* 
 * member functions 
 */

/* sets */
gboolean gj_iq_set_user_data (GjInfoquery iq, gpointer user_data)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	iq->user_data = user_data;

	return TRUE;
}

gboolean gj_iq_set_event_id (GjInfoquery iq, guint id)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id < 1) {
		g_warning ("%s: event id:%d was < 1", __FUNCTION__, id);
		return FALSE;
	}

	iq->event_id = id;
	return TRUE;
}

gboolean gj_iq_set_related_id (GjInfoquery iq, guint id)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id < 1) {
		g_warning ("%s: related id:%d was < 1", __FUNCTION__, id);
		return FALSE;
	}

	iq->related_event_id = id;
	return TRUE;
}

gboolean gj_iq_set_id (GjInfoquery iq, const gchar *id)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (iq->id);
	iq->id = g_strdup (id);

	return TRUE;
}

gboolean gj_iq_set_connection (GjInfoquery iq, GjConnection c)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	} 

	if (iq->c) {
		gj_connection_unref (iq->c);
	}

	iq->c = gj_connection_ref (c);
	return TRUE;
}

gboolean gj_iq_set_to (GjInfoquery iq, const gchar *to)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (iq->to);
	iq->to = g_strdup (to);

	return TRUE;
}

gboolean gj_iq_set_from (GjInfoquery iq, const gchar *from)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (from == NULL) {
		g_warning ("%s: from was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (iq->from);
	iq->from = g_strdup (from);

	return TRUE;
}

gboolean gj_iq_set_type (GjInfoquery iq, GjInfoqueryType type)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (type < 1 || type > GjInfoqueryTypeEnd) {
		g_warning ("%s: type:%d was out of bounds", __FUNCTION__, type);
		return FALSE;
	}

	iq->type = type;
	return TRUE;
}

gboolean gj_iq_set_xml (GjInfoquery iq, xmlNodePtr node)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (node == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return FALSE;
	} 

	/*   iq->xml = xmlCopyNode (node, 1); */
	iq->xml = node;
	return TRUE;
}

gboolean gj_iq_set_callback (GjInfoquery iq,  GjInfoqueryCallback cb)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	iq->cb = cb;
	return TRUE;
}

/* gets */
gpointer gj_iq_get_user_data (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	return iq->user_data;
}

const gchar *gj_iq_get_id (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return NULL;
	}

	return iq->id;
}

const GjConnection gj_iq_get_connection (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return NULL;
	}

	return iq->c;
}

const gchar *gj_iq_get_to (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return NULL;
	}

	return iq->to;
}

const gchar *gj_iq_get_from (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return NULL;
	}

	return iq->from;
}

GjInfoqueryType gj_iq_get_type (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return 0;
	}

	return iq->type;
}

xmlNodePtr gj_iq_get_xml (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	return iq->xml;
}

void *gj_iq_get_callback (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	return (void*)iq->cb;
}

guint gj_iq_get_event_id (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return -1;
	}

	return iq->event_id;
}

guint gj_iq_get_related_id (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return -1;
	}

	return iq->related_event_id;
}

/* 
 * compile iq 
 */
static gboolean gj_iq_check_params (GjInfoquery iq)
{
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	} 

	/* if iq is type GET, requires ID, To (optional) 
	   if iq is type SET, requires ID, To (optional)
	   if iq is type RESULT, requires ID, To (optional), From
	   if iq is type ERROR, requires ID, To (optional), From
	*/

	if (gj_iq_get_type (iq) != GjInfoqueryTypeGet && 
	    gj_iq_get_type (iq) != GjInfoqueryTypeSet && 
	    gj_iq_get_type (iq) != GjInfoqueryTypeResult && 
	    gj_iq_get_type (iq) != GjInfoqueryTypeError) {
		g_warning ("%s: infoquery type:%d unrecognised", __FUNCTION__, gj_iq_get_type (iq));
		return FALSE;      
	}

	return TRUE;
}

gboolean gj_iq_build (GjInfoquery iq, xmlNodePtr node)
{
	xmlNodePtr parent = NULL;

	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	} 
  
	if (node == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (gj_iq_check_params (iq) == FALSE) {
		return FALSE;
	}
  
	/* set root node */
	parent = xmlNewNode (NULL, (guchar*)"iq");
  
	/* id */
	if (gj_iq_get_id (iq) == NULL) {
		gchar *id = NULL;
      
		if ((id = g_strdup_printf ("%d",gj_iq_get_event_id (iq))) != NULL) {
			gj_iq_set_id (iq,id);
			g_free (id);
		}
	}

	xmlSetProp (parent, (guchar*)"id", (guchar*)gj_iq_get_id (iq));
  
	/* type */
	if (iq->type == GjInfoqueryTypeGet) {
		xmlSetProp (parent, (guchar*)"type", (guchar*)"get");
	} else if (iq->type == GjInfoqueryTypeSet) {
		xmlSetProp (parent, (guchar*)"type", (guchar*)"set");
	} else if (iq->type == GjInfoqueryTypeResult) {
		xmlSetProp (parent, (guchar*)"type", (guchar*)"result");
	} else if (iq->type == GjInfoqueryTypeError) {
		xmlSetProp (parent, (guchar*)"type", (guchar*)"error");      
	}

	/* 
	 * set props 
	 */
  
	/* to */
	if (gj_iq_get_to (iq) != NULL) {
		xmlSetProp (parent, (guchar*)"to", (guchar*)gj_iq_get_to (iq));
	}
    
	/* from */
	if (gj_iq_get_from (iq) != NULL) {
		xmlSetProp (parent, (guchar*)"from", (guchar*)gj_iq_get_from (iq)); 
	}

	/* add node */
	xmlAddChild (parent, node);

	/* set iq query */
	gj_iq_set_xml (iq, parent);

	return TRUE;
}

gboolean gj_iq_build_from_list (GjInfoquery iq, GList *nodes)
{
	xmlNodePtr parent = NULL;
   
	guint count = 0;

	if (iq == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return FALSE;
	} 
   
	if (nodes == NULL) {
		g_warning ("%s: list of xml nodes was NULL", __FUNCTION__);
		return FALSE;
	}
   
	if (gj_iq_check_params (iq) == FALSE) {
		return FALSE;
	}
   
	/* set root node */
	parent = xmlNewNode (NULL, (guchar*)"iq");
   
	/* id */
	if (gj_iq_get_id (iq) == NULL) {
		gchar *id = NULL;
       
		if ((id = g_strdup_printf ("%d",gj_iq_get_event_id (iq))) != NULL) {
			gj_iq_set_id (iq, id);
			g_free (id);
		}
	}
   
	xmlSetProp (parent, (guchar*)"id", (guchar*)gj_iq_get_id (iq));
   
	/* type */
	if (iq->type == GjInfoqueryTypeGet)
		xmlSetProp (parent, (guchar*)"type", (guchar*)"get");
	if (iq->type == GjInfoqueryTypeSet)
		xmlSetProp (parent, (guchar*)"type", (guchar*)"set");
	if (iq->type == GjInfoqueryTypeResult)
		xmlSetProp (parent, (guchar*)"type", (guchar*)"result");
	if (iq->type == GjInfoqueryTypeError)
		xmlSetProp (parent, (guchar*)"type", (guchar*)"error");      
   
	/* 
	 * set props 
	 */
   
	while (count < g_list_length (nodes)) {
		xmlNodePtr node = g_list_nth_data (nodes,count);
		
		if (node == NULL){
			continue;
		} else {
			count++;
		}
		
		/* add node */
		xmlAddChild (parent,node);
	}
	
	if (count == 0) {
		g_warning ("%s: there were 0 top level nodes", __FUNCTION__);
		return FALSE;
	}
   
	/* to */
	if (gj_iq_get_to (iq) != NULL) {
		xmlSetProp (parent, (guchar*)"to", (guchar*)gj_iq_get_to (iq));
	}
   
	/* from */
	if (gj_iq_get_from (iq) != NULL) {
		xmlSetProp (parent, (guchar*)"from", (guchar*)gj_iq_get_from (iq));
	}
   
	/* set iq query */
	gj_iq_set_xml (iq, parent);
   
	return TRUE;
}

gboolean gj_iq_send (GjInfoquery iq)
{
	xmlNodePtr node = NULL;

	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	} 

	if ((node = gj_iq_get_xml (iq)) == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return FALSE;
	}

	return gj_parser_push_output (iq->c, node);
}

GjInfoquery gj_iq_find (guint id)
{
	GjInfoquery iq = NULL;

	if (id < 0) {
		g_warning ("%s: id:%d was < 1", __FUNCTION__, id);
		return NULL;
	}

	iq = g_hash_table_lookup (iqs, &id);
	return iq;
}

/*
 * ENTRY POINT called by parser
 */
void gj_iq_receive (GjConnection c, xmlNodePtr node)
{
	xmlChar *s = NULL;

	GjInfoquery iq_new = NULL;
	GjInfoquery iq_related = NULL;

	GjInfoqueryType type = GjInfoqueryTypeError;

	if (node == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return;
	}

	if ((s = gj_parser_find_by_attr_and_ret (node, (guchar*)"id")) != NULL && xmlStrlen (s) > 0) {
		gint id = 0;

		/* see if there already is an infoquery with this id, 
		   if so, this is possibly the response */ 
		id = atoi ((gchar*)s);
		iq_related = gj_iq_find (id); 
	}

	if ((s = gj_parser_find_by_attr_and_ret (node, (guchar*)"type")) != NULL && xmlStrlen (s) > 0) {
		if (xmlStrncmp (s, (guchar*)"get", xmlStrlen (s)) == 0) {
			type = GjInfoqueryTypeGet;
		} else if (xmlStrncmp (s, (guchar*)"set", xmlStrlen (s)) == 0) {
			type = GjInfoqueryTypeSet;
		} else if (xmlStrncmp (s, (guchar*)"result", xmlStrlen (s)) == 0) {
			type = GjInfoqueryTypeResult;
		} else if (xmlStrncmp (s, (guchar*)"error", xmlStrlen (s)) == 0) {
			type = GjInfoqueryTypeError;
		} else {
			g_warning ("%s: could not determin xml type from:'%s', thowing away....", __FUNCTION__, s);
			return;
		}
	}
  
	if ((iq_new = gj_iq_new (type)) == NULL) {
		g_warning ("%s: could not create new infoquery", __FUNCTION__);
		return;
	}
  
	/* set xml / connection for iq */
	gj_iq_set_xml (iq_new, node);
	gj_iq_set_connection (iq_new, c);

	/* set characteristics */
	if ((s = gj_parser_find_by_attr_and_ret (node, (guchar*)"id")) != NULL && xmlStrlen (s) > 0) {
		gj_iq_set_id (iq_new, (gchar*)s);
	}
  
	if ((s = gj_parser_find_by_attr_and_ret (node, (guchar*)"to")) != NULL && xmlStrlen (s) > 0) {
		gj_iq_set_to (iq_new, (gchar*)s);
	}
  
	if ((s = gj_parser_find_by_attr_and_ret (node, (guchar*)"from")) != NULL && xmlStrlen (s) > 0) {
		gj_iq_set_from (iq_new, (gchar*)s);
	}
  
	/* 
	   see if this iq relates to a previous event, 
	   and if so call the callback 
	*/
	if (iq_related != NULL) {
		GjInfoqueryCallback cb = NULL;
		gpointer user_data = NULL;

		if ((cb = (GjInfoqueryCallback) gj_iq_get_callback (iq_related)) == NULL) {
			g_message ("%s: infoquery has no callback to original query (id:%d), freeing...", 
				   __FUNCTION__, (gint)gj_iq_get_id (iq_related));
	  
			/* free mem */
			gj_iq_unref (iq_new);  
			gj_iq_unref (iq_related); 
	  
			return;
		}
      
		/* set responseID for new response infoquery */
		gj_iq_set_related_id (iq_related, gj_iq_get_event_id (iq_new));
		gj_iq_set_related_id (iq_new, gj_iq_get_event_id (iq_related));
      
		user_data = iq_related->user_data;
      
		/*       if (!user_data && iq_new->user_data) */
		/* 	user_data = iq_new->user_data; */

		/* call callback function */
		(cb) (iq_new, iq_related, user_data);

		/* NOTE: as this is the response and we are sending them 
		   the original request and the response, we can now clean up 
		   for them. */
		gj_iq_unref (iq_related);
		gj_iq_unref (iq_new);
	} else {
		/* not related to a previous event, must be new from the server */
		gj_iq_request_new (iq_new);

		/* clean up */
		gj_iq_unref (iq_new);
	}
}

#ifdef __cplusplus
}
#endif
