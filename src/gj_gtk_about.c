/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"

#include "gj_gtk_about.h"
#include "gj_gtk_support.h"

/* jabber software foundation image */
#define FILE_JSF "gj_jsf.png"


typedef struct  {
	/* search */
	GtkWidget *window;

	GtkWidget *label_title;
	GtkWidget *label_semititle;
	GtkWidget *label_version;
	GtkWidget *label_build;
	GtkWidget *label_copyright;
	GtkWidget *label_license;
	GtkWidget *label_sn;

	GtkWidget *image_icon;

	GtkWidget *button_credits;
	GtkWidget *button_close;

	GtkWidget *credits;

	GSList *authors;
	GSList *documenters;
	GSList *translators;

} GjAboutDialog;

/* responses */
enum {
	ABOUT_RESPONSE_CREDITS = 1
};


/* local functions */
static gboolean gj_gtk_about_setup (GjAboutDialog *dialog, 
				    const gchar *window_title,
				    const gchar *title, 
				    const gchar *semititle, 
				    const gchar *version, 
				    const gchar *build, 
				    const gchar *copyright, 
				    const gchar *license,
				    const gchar *sn);

/* credits dialog */
static gboolean gj_gtk_about_credits (GjAboutDialog *dialog);

static GtkWidget *gj_gtk_about_credits_create_label ();

static void gj_gtk_about_credits_update_authors (GjAboutDialog *dialog, GtkWidget *label);
static void gj_gtk_about_credits_update_documenters (GjAboutDialog *dialog, GtkWidget *label);
static void gj_gtk_about_credits_update_translators (GjAboutDialog *dialog, GtkWidget *label);

/* event callbacks */
static void on_destroy (GtkWidget *widget, GjAboutDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjAboutDialog *dialog);


static GjAboutDialog *current_dialog = NULL;


gboolean gj_gtk_about_load (const gchar *window_title,
			    const gchar *title, 
			    const gchar *semititle, 
			    const gchar *version, 
			    const gchar *build, 
			    const gchar *copyright, 
			    const gchar *license,
			    const gchar *sn,
			    const gchar **authors,
			    const gchar **documenters,
			    const gchar **translators)
{
	GjAboutDialog *dialog = NULL;
	GladeXML *xml = NULL;

	GValueArray *authors_array = NULL;
	GValueArray *documenters_array = NULL;
	GValueArray *translators_array = NULL;

	GValueArray *value_array;
	GSList *list;

	gint i = 0;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return FALSE;
	}
    
	/* check vars */
	if (window_title == NULL) {
		g_warning ("%s: window_title was NULL", __FUNCTION__);
		return FALSE;
	}

	if (title == NULL) {
		g_warning ("%s: title was NULL", __FUNCTION__);
		return FALSE;
	}

	if (semititle == NULL) {
		g_warning ("%s: semititle was NULL", __FUNCTION__);
		return FALSE;
	}

	if (version == NULL) {
		g_warning ("%s: version was NULL", __FUNCTION__);
		return FALSE;
	}

	if (build == NULL) {
		g_warning ("%s: build was NULL", __FUNCTION__);
		return FALSE;
	}

	if (copyright == NULL) {
		g_warning ("%s: copyright was NULL", __FUNCTION__);
		return FALSE;
	}

	if (license == NULL) {
		g_warning ("%s: license was NULL", __FUNCTION__);
		return FALSE;
	}

	/*     if (sn == NULL) */
	/* 	g_warning ("gj_gtk_about_load: serial number was NULL"); */
	/* 	return FALSE; */
	/*     } */
    
	current_dialog = dialog = g_new0 (GjAboutDialog, 1);

	/* authors */
	g_message ("%s: setting authors...", __FUNCTION__);
	authors_array = g_value_array_new (0);

	for (i = 0; authors[i] != NULL; i++) {
		GValue value = {0, };
 	
		g_value_init (&value, G_TYPE_STRING);
		g_value_set_static_string (&value, authors[i]);
		authors_array = g_value_array_append (authors_array, &value);
	}

	/* documenters */
	if (documenters != NULL) {
		g_message ("%s: setting documenters...", __FUNCTION__);
		documenters_array = g_value_array_new (0);
	
		for (i = 0; documenters[i] != NULL; i++) {
			GValue value = {0, };
	    
			g_value_init (&value, G_TYPE_STRING);
			g_value_set_static_string (&value, documenters[i]);
			documenters_array = g_value_array_append (documenters_array, &value);
		}
	
	}

	/* translators */
	if (translators != NULL) {
		g_message ("%s: setting translators...", __FUNCTION__);
		translators_array = g_value_array_new (0);
	
		for (i = 0; translators[i] != NULL; i++) {
			GValue value = {0, };
	    
			g_value_init (&value, G_TYPE_STRING);
			g_value_set_static_string (&value, translators[i]);
			translators_array = g_value_array_append (translators_array, &value);
		}
	
	}

	/* authors */
	list = dialog->authors;
	if (list != NULL) {    
		g_slist_foreach (list, (GFunc) g_free, NULL);
		g_slist_free (list);
	}
    
	list = NULL;
	/*     value_array = g_value_get_boxed (authors_array); */
	value_array = authors_array;

	if (value_array != NULL) {
		for (i = 0; i < value_array->n_values; i++) {
			list = g_slist_prepend (list, g_value_dup_string (&value_array->values[i]));
		}
	
		list = g_slist_reverse (list);
		dialog->authors = list;
	}

	/* documenters */
	list = dialog->documenters;
	if (list != NULL) {
		g_slist_foreach (list, (GFunc) g_free, NULL);
		g_slist_free (list);
	}
                                                                                                                   
	list = NULL;
	/*     value_array = g_value_get_boxed (documenters_array); */
	value_array = documenters_array;
                                                                                                                   
	if (value_array != NULL) {
		for (i = 0; i < value_array->n_values; i++) {
			list = g_slist_prepend (list, g_value_dup_string (&value_array->values[i]));
		}
                                                                                                                   
		list = g_slist_reverse (list);
		dialog->documenters = list;
	}

	/* translators */
	list = dialog->translators;
	if (list != NULL) {    
		g_slist_foreach (list, (GFunc) g_free, NULL);
		g_slist_free (list);
	}
    
	list = NULL;
	/*     value_array = g_value_get_boxed (documenters_array); */
	value_array = translators_array;
    
	if (value_array != NULL) {
		for (i = 0; i < value_array->n_values; i++) {
			list = g_slist_prepend (list, g_value_dup_string (&value_array->values[i]));
		}
	
		list = g_slist_reverse (list);
		dialog->translators = list;
	}

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "about",
				     NULL,
				     "about", &dialog->window,
				     "label_title", &dialog->label_title, 
				     "label_semititle", &dialog->label_semititle,
				     "label_version", &dialog->label_version,
				     "label_build", &dialog->label_build,
				     "label_copyright", &dialog->label_copyright,
				     "label_license", &dialog->label_license,
				     "label_sn", &dialog->label_sn,
				     "image_icon", &dialog->image_icon,
				     "button_credits", &dialog->button_credits,
				     "button_close", &dialog->button_close,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "about", "response", on_response,
			      "about", "destroy", on_destroy,
			      NULL);


	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* check window is created ok */
	if (dialog->window == NULL) {
		g_warning ("%s: failed to create message box, window was NULL", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: using window title:'%s'", __FUNCTION__, window_title);
	g_message ("%s: using title:'%s'", __FUNCTION__, title);
	g_message ("%s: using semititle:'%s'", __FUNCTION__, semititle);
	g_message ("%s: using version:'%s'", __FUNCTION__, version);
	g_message ("%s: using build:'%s'", __FUNCTION__, build);
	g_message ("%s: using copyright:'%s'", __FUNCTION__, copyright);
	g_message ("%s: using license:'%s'", __FUNCTION__, license);
	g_message ("%s: using serial number:'%s'", __FUNCTION__, sn);

	gj_gtk_about_setup (dialog, 
			    window_title,
			    title,
			    semititle,
			    version,
			    build,
			    copyright,
			    license,
			    sn);

	/* redraw - for windows */
	gj_gtk_widget_redraw_when_idle (GTK_WIDGET (dialog->window));

	if (authors_array != NULL) {
		g_value_array_free (authors_array);
	}

	if (documenters_array != NULL) {
		g_value_array_free (documenters_array);
	}

	if (translators_array != NULL) {
		g_value_array_free (translators_array);
	}

	return TRUE;
}

static gboolean gj_gtk_about_setup (GjAboutDialog *dialog, 
				    const gchar *window_title,
				    const gchar *title, 
				    const gchar *semititle, 
				    const gchar *version, 
				    const gchar *build, 
				    const gchar *copyright, 
				    const gchar *license,
				    const gchar *sn)
{
	gchar *absolute_path = NULL;
	gchar *filename = NULL;
	gchar *text = NULL;
	GdkPixbufAnimation *ani = NULL; 

#if defined G_OS_WIN32
	const char *os = "Windows";
#elif defined G_OS_UNIX
	const char *os = "Unix";
#elif defined G_OS_BEOS
	const char *os = "BeOS";
#else
	const char *os = "unknown operating system";
#endif

	if (window_title != NULL) {
		gtk_window_set_title (GTK_WINDOW (dialog->window), window_title); 
	}

	if (title != NULL) {
		text = g_strdup_printf ("<span size=\"xx-large\" weight=\"heavy\" stretch=\"ultraexpanded\">%s</span>", 
					title);
		gtk_label_set_markup (GTK_LABEL (dialog->label_title), text);
		g_free (text);
	}

	if (semititle != NULL) {
		text = g_strdup_printf ("<span size=\"large\" weight=\"heavy\">%s</span>", 
					semititle);
		gtk_label_set_markup (GTK_LABEL (dialog->label_semititle), text);
		g_free (text);
	}
    
	if (version != NULL) {
		text = g_strdup_printf ("%s %s", 
					_("Version"),
					version);

		gtk_label_set_markup (GTK_LABEL (dialog->label_version), text);

		g_free (text);
	}

	if (build != NULL) {
		text = g_strdup_printf ("%s %s (for %s)", _("Build"), build, os);
		gtk_label_set_markup (GTK_LABEL (dialog->label_build), text);
		g_free (text);
	}

	if (copyright != NULL) {
		gtk_label_set_markup (GTK_LABEL (dialog->label_copyright), copyright);
	}
    
	if (license != NULL) {
		gtk_label_set_markup (GTK_LABEL (dialog->label_license), license);
	}

	if (sn != NULL) {
		gtk_label_set_markup (GTK_LABEL (dialog->label_sn), sn);
	}

#ifdef G_OS_WIN32
	absolute_path = g_win32_get_package_installation_directory (PACKAGE_NAME, NULL);
	filename = g_build_filename (absolute_path, FILE_JSF, NULL);
#else
	absolute_path = NULL;
	filename = g_build_filename (DATADIR, PACKAGE, FILE_JSF, NULL);
#endif

	ani = gj_gtk_animation_new_from_file (filename);

	g_free (absolute_path);
	g_free (filename);

	if (!ani) {
		g_warning ("%s: animation was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set image */
	gtk_image_set_from_animation (GTK_IMAGE (dialog->image_icon), ani);

	/* clean up */
	g_object_unref (G_OBJECT (ani));

	return TRUE;
}

static GtkWidget *gj_gtk_about_credits_create_label ()
{
	GtkWidget *label = NULL;
    
	label = gtk_label_new ("");
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
    
	gtk_misc_set_padding (GTK_MISC (label), 8, 8);
    
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
    
	return label;
}

static void gj_gtk_about_credits_update_authors (GjAboutDialog *dialog, GtkWidget *label)
{
	GString *string = NULL;
	GSList *list = NULL;
	gchar *tmp = NULL;
    
	if (dialog->authors == NULL) {
		gtk_widget_hide (label);
		return;
	} else {
		gtk_widget_show (label);
	}
    
	string = g_string_new (NULL);
    
	for (list=dialog->authors; list; list=list->next) {
		tmp = g_markup_escape_text (list->data, -1);
		g_string_append (string, tmp);
	
		if (list->next) {
			g_string_append_c (string, '\n');
		}
	
		g_free (tmp);
	}
    
	gtk_label_set_text (GTK_LABEL (label), string->str);
	g_string_free (string, TRUE);
}

static void gj_gtk_about_credits_update_documenters (GjAboutDialog *dialog, GtkWidget *label)
{
	GString *string = NULL;
	GSList *list = NULL;
	gchar *tmp = NULL;
                                                                                                                   
	if (dialog->documenters == NULL) {
		gtk_widget_hide (label);
		return;
	} else {
		gtk_widget_show (label);
	}
                                                                                                                   
	string = g_string_new (NULL);
                                                                                                                   
	for (list=dialog->documenters; list; list=list->next) {
		tmp = g_markup_escape_text (list->data, -1);
		g_string_append (string, tmp);
                                                                                                                   	
	if (list->next) {
			g_string_append (string, "\n");
		}
                                                                                                                   
		g_free (tmp);
	}
                                                                                                                   
	gtk_label_set_text (GTK_LABEL (label), string->str);
	g_string_free (string, TRUE);
}

static void gj_gtk_about_credits_update_translators (GjAboutDialog *dialog, GtkWidget *label)
{
	GString *string = NULL;
	GSList *list = NULL;
	gchar *tmp = NULL;
    
	if (dialog->translators == NULL) {
		gtk_widget_hide (label);
		return;
	} else {
		gtk_widget_show (label);
	}
    
	string = g_string_new (NULL);
    
	for (list=dialog->translators; list; list=list->next) {
		/*       tmp = g_markup_escape_text (list->data, -1);  */
		g_string_append (string, list->data);
	
		if (list->next) {
			g_string_append (string, "\n");
		}
	
		g_free (tmp); 
	}
    
	gtk_label_set_text (GTK_LABEL (label), string->str);
	g_string_free (string, TRUE);
}

static gboolean gj_gtk_about_credits (GjAboutDialog *dialog)
{
	GtkWidget *notebook = NULL;
	GtkWidget *label = NULL;
	GtkWidget *sw = NULL;

	GtkWidget *credits = NULL;

	if (dialog->credits != NULL) {
		gtk_window_present (GTK_WINDOW (dialog->credits));
		return TRUE;
	}

	credits = gtk_dialog_new_with_buttons (_("Credits"),
					       GTK_WINDOW (dialog->window),
					       GTK_DIALOG_DESTROY_WITH_PARENT,
					       GTK_STOCK_OK, GTK_RESPONSE_OK,
					       NULL);
    
	dialog->credits = credits;
	gtk_window_set_default_size (GTK_WINDOW (credits), 360, 260);
	gtk_dialog_set_default_response (GTK_DIALOG (credits), GTK_RESPONSE_OK);
    
	g_signal_connect (credits, "response",
			  G_CALLBACK (gtk_widget_destroy), credits);
    
	g_signal_connect (credits, "destroy",
			  G_CALLBACK (gtk_widget_destroyed),
			  & (dialog->credits));
    
	notebook = gtk_notebook_new ();
	gtk_container_set_border_width (GTK_CONTAINER (notebook), 8);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (credits)->vbox), notebook, TRUE, TRUE, 0);
    
	if (dialog->authors != NULL) {
		g_message ("%s: configuring authors...", __FUNCTION__);
		label = gj_gtk_about_credits_create_label ();
	
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
						GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), label);
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (sw)->child), GTK_SHADOW_NONE);
	
		gtk_notebook_append_page (GTK_NOTEBOOK (notebook), sw,
					  gtk_label_new_with_mnemonic (_("Written by")));

		gj_gtk_about_credits_update_authors (dialog, label);
	}
    
	/*   if (dialog->documenters != NULL)  */
	/*       g_message ("%s: configuring documenters...", __FUNCTION__); */
	/*       label = gj_gtk_about_credits_create_label (); */
	
	/*       sw = gtk_scrolled_window_new (NULL, NULL); */
	/*       gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), */
	/* 				     GTK_POLICY_AUTOMATIC, */
	/* 				     GTK_POLICY_AUTOMATIC); */
	/*       gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), label); */
	/*       gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (sw)->child), GTK_SHADOW_NONE); */
	
	/*       gtk_notebook_append_page (GTK_NOTEBOOK (notebook), sw, */
	/* 			       gtk_label_new_with_mnemonic (_("Documented by"))); */
	
	/*       gj_gtk_about_credits_update_documenters (label); */
	/*     } */
    
	if (dialog->translators != NULL)  { 
		g_message ("%s: configuring translators...", __FUNCTION__);
		label = gj_gtk_about_credits_create_label (); 
	
		sw = gtk_scrolled_window_new (NULL, NULL); 
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
						GTK_POLICY_AUTOMATIC, 
						GTK_POLICY_AUTOMATIC); 
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), label);
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (sw)->child), GTK_SHADOW_NONE); 
	
		gtk_notebook_append_page (GTK_NOTEBOOK (notebook), sw, 
					  gtk_label_new_with_mnemonic (_("Translated by"))); 
	
		gj_gtk_about_credits_update_translators (dialog, label); 
	} 
   
	gtk_widget_show_all (credits);
    
	return TRUE;
}


/*
 * actual gui functions
 */

static void on_destroy (GtkWidget *widget, GjAboutDialog *dialog)
{
	current_dialog = NULL;

	if (!dialog) {
		return;
	}

	if (dialog->authors != NULL) {    
		g_slist_foreach (dialog->authors, (GFunc) g_free, NULL);
		g_slist_free (dialog->authors);
	}

	if (dialog->documenters != NULL) {    
		g_slist_foreach (dialog->documenters, (GFunc) g_free, NULL);
		g_slist_free (dialog->documenters);
	}

	if (dialog->translators != NULL) {
		g_slist_foreach (dialog->translators, (GFunc) g_free, NULL);
		g_slist_free (dialog->translators);
	}

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjAboutDialog *dialog)
{
	if (!dialog) {
		return;
	}

	if (response == ABOUT_RESPONSE_CREDITS) {
		gj_gtk_about_credits (dialog);
		return;
	}
  
	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
