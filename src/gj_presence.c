/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>
#include <string.h>

#include <time.h>

#include <glib.h>

#include <libxml/tree.h>

#include "gj_presence.h"
#include "gj_parser.h"
#include "gj_support.h"
#include "gj_jid.h"
#include "gj_connection.h"


struct t_presence {
	gint ref_count;

	GjConnection c;

	GjPresenceType type;
	GjPresenceShow show;

	gchar *status;
	gint8 priority; /* -128 to +127 */
  
	gint unique_id;

	GjJID to_jid;
	GjJID from_jid;
  
	gchar *error_code;
	gchar *error_reason;

	time_t time;
};


static gboolean gj_presence_free (GjPresence pres); 


/*
 * member functions
 */

/* sets */
static gboolean gj_presence_set_type (GjPresence pres, GjPresenceType type);
static gboolean gj_presence_set_show (GjPresence pres, GjPresenceShow show);

static gboolean gj_presence_set_status (GjPresence pres, const gchar *status);
static gboolean gj_presence_set_priority (GjPresence pres, gint8 priority);

static gboolean gj_presence_set_to_jid (GjPresence pres, const gchar *jid);
static gboolean gj_presence_set_from_jid (GjPresence pres, const gchar *jid);

static gboolean gj_presence_set_error_code (GjPresence pres, const gchar *error_code);
static gboolean gj_presence_set_error_reason (GjPresence pres, const gchar *error_reason);

/* from details, i.e. my contacts state (s) */
static GjPresenceFromCallback gj_presence_get_cb_from ();

/* function */
static void gj_presence_init ();
static void gj_presence_destroy_cb (gpointer data);

/* sets */
static gboolean gj_presence_set_connection (GjPresence pres, GjConnection c);


static GHashTable *presences = NULL;

static GjPresenceFromCallback presence_cb_from;
static GjPresenceToCallback presence_cb_to;   /* may not get used */


static void gj_presence_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;

	presences = g_hash_table_new_full (g_int_hash, 
					   g_int_equal, 
					   NULL, 
					   (GDestroyNotify) gj_presence_destroy_cb);   
}

static void presences_foreach_cb (gint *unique_id, GjPresence pres, gpointer user_data) {
	g_message ("%s: id:%d", __FUNCTION__, *unique_id);
}

void gj_presence_stats ()
{
	g_message ("%s: table size:%d", __FUNCTION__, g_hash_table_size (presences));

	g_hash_table_foreach (presences, (GHFunc)presences_foreach_cb, NULL);
}


static void gj_presence_destroy_cb (gpointer data)
{
	GjPresence pres = (GjPresence) data;

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return;
	}

	pres->ref_count = 0;

	if (pres->c) {
		gj_connection_unref (pres->c);
	}

	pres->type = GjPresenceTypeEnd;
	pres->show = GjPresenceShowEnd;

	pres->unique_id = 0;

	g_free (pres->status);

	pres->priority = 0;

	if (pres->to_jid != NULL) {
		gj_jid_unref (pres->to_jid);
	}

	if (pres->from_jid != NULL) {
		gj_jid_unref (pres->from_jid);
	}
  
	g_free (pres->error_code);
	g_free (pres->error_reason);

	g_free (pres);
}

GjPresence gj_presence_new (GjPresenceType type, GjPresenceShow show)
{
	GjPresence pres = NULL;

	gj_presence_init ();

	if ((pres = g_new0 (struct t_presence,1)) != NULL) {
		pres->ref_count = 1;

		pres->c = NULL;

		pres->type = type;
		pres->show = show;

		pres->unique_id = gj_get_unique_id ();

		pres->to_jid = NULL;
		pres->from_jid = NULL;
      
		pres->status = NULL;
		pres->priority = 0;

		pres->error_code = NULL;
		pres->error_reason = NULL;

		g_hash_table_insert (presences, &pres->unique_id, pres);
	}
  
	/*   if (gnome_jabber_get_debugging ()) */
	/*     g_message ("%s: table size is:%d", __FUNCTION__, g_hash_table_size (presences));  */

	return pres;  
}

static gboolean gj_presence_free (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	g_hash_table_remove (presences, &pres->unique_id);
	return TRUE;
}

GjPresence gj_presence_ref (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}  
 
	pres->ref_count++;

	return pres;
}

gboolean gj_presence_unref (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	pres->ref_count--;

	if (pres->ref_count < 1)
		gj_presence_free (pres);

	return TRUE;
}

/*
 * member functions 
 */

/* sets */
gboolean gj_presence_set_to_jid (GjPresence pres, const gchar *jid)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres->to_jid != NULL) {
		gj_jid_unref (pres->to_jid);
	}

	pres->to_jid = gj_jid_new (jid);
	return TRUE;
}

static gboolean gj_presence_set_connection (GjPresence pres, GjConnection c)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	pres->c = gj_connection_ref (c);
	return TRUE;
}

gboolean gj_presence_set_type (GjPresence pres, GjPresenceType type)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (type < 1 || type > GjPresenceTypeEnd) {
		g_warning ("%s: type was out of range", __FUNCTION__);
		return FALSE;
	}

	pres->type = type;
	return TRUE;
}

gboolean gj_presence_set_show (GjPresence pres, GjPresenceShow show)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (show < 1 || show > GjPresenceShowEnd) {
		g_warning ("%s: type was out of range", __FUNCTION__);
		return FALSE;
	}

	pres->show = show;
	return TRUE;
}

gboolean gj_presence_set_status (GjPresence pres, const gchar *status)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (status == NULL) {
		g_warning ("%s: status was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (pres->status);
	pres->status = g_strdup (status);

	return TRUE;
}

gboolean gj_presence_set_priority (GjPresence pres, gint8 priority)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	pres->priority = priority;
	return TRUE;
}

gboolean gj_presence_set_from_jid (GjPresence pres, const gchar *jid)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres->from_jid != NULL) {
		gj_jid_unref (pres->from_jid);
	}

	pres->from_jid = gj_jid_new (jid);
	return TRUE;
}

gboolean gj_presence_set_error_code (GjPresence pres, const gchar *error_code)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (error_code == NULL) {
		g_warning ("%s: error code was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (pres->error_code);
	pres->error_code = g_strdup (error_code);

	return TRUE;
}

gboolean gj_presence_set_error_reason (GjPresence pres, const gchar *error_reason)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (error_reason == NULL) {
		g_warning ("%s: error reason was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (pres->error_reason);
	pres->error_reason = g_strdup (error_reason);

	return TRUE;
}

/* gets */
GjConnection gj_presence_get_connection (GjPresence pres)
{ 
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return NULL;
	}  

	return pres->c;
}

GjPresenceType gj_presence_get_type (GjPresence pres)
{ 
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return -1;
	}  

	return pres->type;
}

GjPresenceShow gj_presence_get_show (GjPresence pres)
{ 
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return -1;
	}  

	return pres->show;
}

const gchar *gj_presence_get_status (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return NULL;
	}  

	return pres->status;
}

gint8 gj_presence_get_priority (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return 0;
	}  

	return pres->priority;
}

gint gj_presence_get_unique_id (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return -1;
	}  

	return pres->unique_id;
}

const GjJID gj_presence_get_to_jid (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return NULL;
	}  

	return pres->to_jid;
}

const GjJID gj_presence_get_from_jid (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return NULL;
	}  

	return pres->from_jid;
}

const gchar *gj_presence_get_error_code (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return NULL;
	}  

	return pres->error_code;
}

const gchar *gj_presence_get_error_reason (GjPresence pres)
{
	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}  

	return pres->error_reason;
}

/*
 * FROM presence
 */
gboolean gj_presence_set_cb_from (GjPresenceFromCallback cb)
{
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	presence_cb_from = cb;
  
	return TRUE;
}

GjPresenceFromCallback gj_presence_get_cb_from ()
{
	return presence_cb_from;
}

/*
 * TO presence
 */
gboolean gj_presence_set_cb_to (GjPresenceToCallback proc)
{
	if (proc == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	presence_cb_to = proc;
  
	return TRUE;
}

/*
 * send/receive functions
 */
gboolean gj_presence_send (GjConnection c,
			   GjPresenceType type, 
			   GjPresenceShow show, 
			   const gchar *status, 
			   gint8 priority)
{
	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *id = NULL;

	if (show < 1) {
		return FALSE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	id = g_strdup_printf ("%d", gj_get_unique_id ());
  
	node = xmlNewNode (NULL, (guchar*)"presence");

	xmlSetProp (node, (guchar*)"id", (guchar*)id);

	if (type >= 1) {
		switch (type) {
		case GjPresenceTypeUnAvailable:
			xmlSetProp (node, (guchar*)"type", (guchar*)"unavailable");
			break;
		case GjPresenceTypeAvailable:
			xmlSetProp (node, (guchar*)"type", (guchar*)"available");
			break;
		case GjPresenceTypeProbe:
			xmlSetProp (node, (guchar*)"type", (guchar*)"probe");
			break;
		case GjPresenceTypeSubscribe:
			xmlSetProp (node, (guchar*)"type", (guchar*)"subscribe");
			break;
		case GjPresenceTypeUnSubscribe:
			xmlSetProp (node, (guchar*)"type", (guchar*)"unsubscribe");
			break;
		case GjPresenceTypeSubscribed:
			xmlSetProp (node, (guchar*)"type", (guchar*)"subscribed");
			break;
		case GjPresenceTypeUnSubscribed:
			xmlSetProp (node, (guchar*)"type", (guchar*)"unsubscribed");
			break;
	  
		default:
		case GjPresenceTypeEnd:
			break;	  
		}
	}
  
	/* add show */ 
	switch (show) {
	case GjPresenceShowAway:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"away");
		break;
	case GjPresenceShowChat:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"chat");
		break;
	case GjPresenceShowDND:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"dnd");
		break;
	case GjPresenceShowXA:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"xa");
		break;
	case GjPresenceShowEnd:
		break;

		/*     default: */
		/*       sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"normal"); */
		/*       break; */
	default:
		break;
	}

	/* add status */
	if (status != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (status, -1);
		sub = xmlNewChild (node, ns, (guchar*)"status", (guchar*)formatted);
		g_free (formatted);
	}

	if (type != GjPresenceTypeSubscribe && 
	    type != GjPresenceTypeSubscribed && 
	    type != GjPresenceTypeUnSubscribe &&
	    type != GjPresenceTypeUnSubscribed) {
		gchar *formatted = NULL;
		formatted = g_strdup_printf ("%d", priority);
		sub = xmlNewChild (node, ns, (guchar*)"priority", (guchar*)formatted);
		g_free (formatted);
	}
  
	gj_parser_push_output (c, node);

	/* clean up */
	g_free (id);

	return TRUE;
}

gboolean gj_presence_send_to (GjConnection c,
			      GjJID j,
			      gboolean full_jid,
			      GjPresenceType type, 
			      GjPresenceShow show, 
			      const gchar *status, 
			      gint8 priority)
{
	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	const gchar *jid_str = NULL;
	gchar *id = NULL;

	if (show < 1) {
		return FALSE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (j == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	id = g_strdup_printf ("%d", gj_get_unique_id ());
  
	node = xmlNewNode (NULL, (guchar*)"presence");

	xmlSetProp (node, (guchar*)"id", (guchar*)id);

#if 0
	if (gj_jid_get_type (j) == GjJIDTypeContact && 
	    type == GjPresenceTypeUnAvailable || type == GjPresenceTypeAvailable) {
		/* when sending subscription messages, we do not use the resource */
		xmlSetProp (node, (guchar*)"to", (guchar*)gj_jid_get_full (j));
	} else {
		xmlSetProp (node, (guchar*)"to", (guchar*)gj_jid_get_without_resource (j));
	}
#endif

	if (full_jid) {
		jid_str = gj_jid_get_full (j);
	} else {
		jid_str = gj_jid_get_without_resource (j);
	}
	
	xmlSetProp (node, (guchar*)"to", (guchar*)jid_str);

	if (type >= 1) {
		switch (type) {
		case GjPresenceTypeUnAvailable:
			xmlSetProp (node, (guchar*)"type", (guchar*)"unavailable");
			break;
		case GjPresenceTypeAvailable:
			xmlSetProp (node, (guchar*)"type", (guchar*)"available");
			break;
		case GjPresenceTypeProbe:
			xmlSetProp (node, (guchar*)"type", (guchar*)"probe");
			break;
		case GjPresenceTypeSubscribe:
			xmlSetProp (node, (guchar*)"type", (guchar*)"subscribe");
			break;
		case GjPresenceTypeUnSubscribe:
			xmlSetProp (node, (guchar*)"type", (guchar*)"unsubscribe");
			break;
		case GjPresenceTypeSubscribed:
			xmlSetProp (node, (guchar*)"type", (guchar*)"subscribed");
			break;
		case GjPresenceTypeUnSubscribed:
			xmlSetProp (node, (guchar*)"type", (guchar*)"unsubscribed");
			break;
	  
		default:
		case GjPresenceTypeEnd:
			break;	  
		}
	}
  
	/* add show */ 
	switch (show) {
	case GjPresenceShowAway:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"away");
		break;
	case GjPresenceShowChat:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"chat");
		break;
	case GjPresenceShowDND:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"dnd");
		break;
	case GjPresenceShowXA:
		sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"xa");
		break;
	case GjPresenceShowEnd:
		break;

		/*     default: */
		/*       sub = xmlNewChild (node, ns, (guchar*)"show", (guchar*)"normal"); */
		/*       break; */
	default:
		break;
	}

	/* add status */
	if (status != NULL) {
		gchar *formatted = NULL;
		formatted = g_markup_escape_text (status, -1);
		sub = xmlNewChild (node, ns, (guchar*)"status", (guchar*)formatted);
		if (formatted != NULL) g_free (formatted);
	}

	if (type != GjPresenceTypeSubscribe && 
	    type != GjPresenceTypeSubscribed && 
	    type != GjPresenceTypeUnSubscribe &&
	    type != GjPresenceTypeUnSubscribed) {
		gchar *formatted = NULL;
		formatted = g_strdup_printf ("%d", priority);
		sub = xmlNewChild (node, ns, (guchar*)"priority", (guchar*)formatted);
		g_free (formatted);
	}
  
	gj_parser_push_output (c, node);

	/* clean up */
	g_free (id);
	
	return TRUE;
}

void gj_presence_receive (GjConnection c, xmlNodePtr node)
{
	const xmlChar *tmp = NULL;

	const xmlChar *stype = NULL;
	const xmlChar *sshow = NULL;
	const xmlChar *spriority = NULL;

	GjPresence pres = NULL;
	GjPresenceFromCallback callback = NULL;

	/* checks */
	if (node == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return;
	}

	/* get info */
	if ((callback = gj_presence_get_cb_from ()) == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	if ((pres = gj_presence_new (GjPresenceTypeAvailable, GjPresenceShowNormal)) == NULL) {
		g_warning ("%s: could not create new presence", __FUNCTION__);
		return;
	}

	if (c != NULL) {
		gj_presence_set_connection (pres, c);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"to")) != NULL && xmlStrlen (tmp) > 0) { 
		gj_presence_set_to_jid (pres, (gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"from")) != NULL && xmlStrlen (tmp) > 0) {
		gj_presence_set_from_jid (pres, (gchar*)tmp);
	}

	if ((stype = gj_parser_find_by_attr_and_ret (node, (guchar*)"type")) != NULL) { 
		if (xmlStrcmp (stype, (guchar*)"available") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeAvailable);
		} else if (xmlStrcmp (stype, (guchar*)"unavailable") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeUnAvailable);
		} else if (xmlStrcmp (stype, (guchar*)"probe") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeProbe);
		} else if (xmlStrcmp (stype, (guchar*)"subscribed") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeSubscribed);
		} else if (xmlStrcmp (stype, (guchar*)"unsubscribed") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeUnSubscribed);
		} else if (xmlStrcmp (stype, (guchar*)"subscribe") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeSubscribe);
		} else if (xmlStrcmp (stype, (guchar*)"unsubscribe") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeUnSubscribe);
		} else if (xmlStrcmp (stype, (guchar*)"error") == 0) {
			gj_presence_set_type (pres, GjPresenceTypeError);
		}
	}

	if ((sshow = gj_parser_find_by_node_on_parent_and_ret (node, (guchar*)"show")) != NULL) {
		if (xmlStrcmp (sshow, (guchar*)"away") == 0) {
			gj_presence_set_show (pres, GjPresenceShowAway);
		} else if (xmlStrcmp (sshow, (guchar*)"chat") == 0) {
			gj_presence_set_show (pres, GjPresenceShowChat);
		} else if (xmlStrcmp (sshow, (guchar*)"dnd") == 0) {
			gj_presence_set_show (pres, GjPresenceShowDND);
		} else if (xmlStrcmp (sshow, (guchar*)"xa") == 0) {
			gj_presence_set_show (pres, GjPresenceShowXA);
		}
	}
  
	if ((spriority = gj_parser_find_by_node_on_parent_and_ret (node, (guchar*)"priority")) != NULL) {
		if (xmlStrlen (spriority) > 0) 
			gj_presence_set_priority (pres, atoi ((gchar*)spriority));
	}
 
	if ((tmp = gj_parser_find_by_node_on_parent_and_ret (node, (guchar*)"status")) != NULL) {
		gj_presence_set_status (pres, (gchar*)tmp);
	}
 
	/* if error deal with it */
	if (gj_presence_get_type (pres) == GjPresenceTypeError) {
		/* get error information */
		xmlChar *code = NULL;
		const xmlChar *reason = NULL;
		xmlNodePtr node_error = NULL;
		
		if ((node_error = gj_parser_find_by_node (node, (guchar*)"error")) != NULL) {
			if ((code = gj_parser_find_by_attr_and_ret (node_error, (guchar*)"code")) != NULL && 
			    xmlStrlen (code) > 0) {
				gj_presence_set_error_code (pres, (gchar*)code);
			}
	  
			if ((reason = gj_parser_get_content (node_error)) != NULL && xmlStrlen (reason) > 0) {
				gj_presence_set_error_reason (pres, (gchar*)reason);
			}
		}
	}

	/* call callback function */
	(callback) (pres);

	/* clean up */
	gj_presence_unref (pres);
}

#ifdef __cplusplus
}
#endif
