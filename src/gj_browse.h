/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$ 
 *
 */

#ifndef __gj_browse_h
#define __gj_browse_h

#include "gj_typedefs.h"

gboolean gj_browse_request (GjConnection c,
			    const gchar *server, 
			    GjBrowseCallback cb, 
			    void *user_data);

/* members */
GjBrowseCategory gj_browse_item_get_category (GjBrowseItem bi);
GjBrowseType gj_browse_item_get_type (GjBrowseItem bi);

GjJID gj_browse_item_get_jid (GjBrowseItem bi);

const gchar *gj_browse_item_get_name (GjBrowseItem bi);
const GList *gj_browse_item_get_namespaces (GjBrowseItem bi);

gboolean gj_browse_item_include_namespace (GjBrowseItem bi, const gchar *namespace);


#endif

