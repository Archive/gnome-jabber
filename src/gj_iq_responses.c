/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_iq.h"
#include "gj_iq_responses.h"
#include "gj_roster.h"
#include "gj_parser.h"
#include "gj_file_transfer.h"

#include "gj_support.h"

/*
 * ACTUAL REQUESTS from the SERVER!!!, eg:
 *
 *  - jabber:iq:last
 *  - jabber:iq:version
 *  - jabber:iq:time
 * 
 *  - http://jabber.org/protocol/disco#info
 *  - http://jabber.org/protocol/ibb (file transfer)
 *  - http://jabber.org/protocol/si (file transfer)
 *
 *
 *
 */

static gboolean gj_iq_respond_to_jabber_iq_last (const GjInfoquery iq);
static gboolean gj_iq_respond_to_jabber_iq_version (const GjInfoquery iq);
static gboolean gj_iq_respond_to_jabber_iq_time (const GjInfoquery iq);

static gboolean gj_iq_respond_to_disco_info (const GjInfoquery iq);

static gboolean gj_iq_respond_with_service_unavailable (const GjInfoquery iq);


gboolean gj_iq_request_new (const GjInfoquery iq)
{
	const xmlChar *ns = NULL;
  
	if (iq == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}
 
	if ((ns = gj_parser_find_by_ns_and_ret (gj_iq_get_xml (iq))) == NULL || xmlStrlen (ns) < 1) {
		g_warning ("%s: could not find expected xml namespace!", __FUNCTION__);
		return FALSE;
	}
 
	if (gj_iq_get_type (iq) == GjInfoqueryTypeGet) {
		if (xmlStrcmp (ns, (guchar*)"jabber:iq:last") == 0) {
			gj_iq_respond_to_jabber_iq_last (iq);
		} else if (xmlStrcmp (ns, (guchar*)"jabber:iq:time") == 0) {
			gj_iq_respond_to_jabber_iq_time (iq);
		} else if (xmlStrcmp (ns, (guchar*)"jabber:iq:version") == 0) {
			gj_iq_respond_to_jabber_iq_version (iq);
		} else if (xmlStrcmp (ns, (guchar*)"http://jabber.org/protocol/disco#info") == 0) {
			gj_iq_respond_to_disco_info (iq);
		} else {
			gj_iq_respond_with_service_unavailable (iq);
		}
	} else if (gj_iq_get_type (iq) == GjInfoqueryTypeSet) {
		if (xmlStrcmp (ns, (guchar*)"jabber:iq:roster") == 0) {
			gj_roster_parse_set_request (iq);
		} else if (xmlStrcmp (ns, (guchar*)"http://jabber.org/protocol/bytestreams") == 0) {
			gj_file_transfer_bs_receive (gj_iq_get_connection (iq), gj_iq_get_xml (iq));
		} else if (xmlStrcmp (ns, (guchar*)"http://jabber.org/protocol/si") == 0) {
			gj_file_transfer_si_receive (gj_iq_get_connection (iq), gj_iq_get_xml (iq));
		} else if (xmlStrcmp (ns, (guchar*)"http://jabber.org/protocol/ibb") == 0) {
			gj_file_transfer_ibb_receive (gj_iq_get_connection (iq), gj_iq_get_xml (iq));
		} else {
			gj_iq_respond_with_service_unavailable (iq);
		}
	} else if (gj_iq_get_type (iq) == GjInfoqueryTypeResult) {
		if (xmlStrcmp (ns, (guchar*)"jabber:iq:register") == 0) {
			GjInfoqueryCallback cb = NULL;
			gpointer user_data = NULL;
			
			cb = (GjInfoqueryCallback) gj_iq_get_callback (iq);
			user_data = gj_iq_get_user_data (iq);
			
			if (cb) {
				(cb) (iq, NULL, user_data);
			}
		}
	} else {
		/* error? why? OR result? why? */
		g_warning ("%s: unexpected infoquery type:%d, not supported yet!", __FUNCTION__, gj_iq_get_type (iq));
		return FALSE;
	}
  
	return TRUE;
}


static gboolean gj_iq_respond_to_jabber_iq_last (const GjInfoquery iq)
{
	GjConnection c = NULL;
	GjInfoquery iq_new = NULL;
  
	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;
  
	xmlChar *tmp = NULL;

	gchar *from = NULL;
	gchar *seconds = NULL;

	if ((iq_new = gj_iq_new (GjInfoqueryTypeResult)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	c = gj_iq_get_connection (iq);
  
	if ((tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"from")) == NULL || xmlStrlen (tmp) < 1) {
		g_warning ("%s: failed to obtain 'from' information", __FUNCTION__);
		return FALSE;
	} else {
		from = g_strdup_printf ("%s",tmp);
	}
  
	seconds = g_strdup_printf ("%f", gj_connection_get_uptime (c));
  
	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:last", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"seconds", (guchar*)seconds);
 
	/* set struct info */
	gj_iq_set_to (iq_new, (gchar*)from);
	gj_iq_set_id (iq_new, gj_iq_get_id (iq));

	if (gj_iq_build (iq_new, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	if (gj_iq_send (iq_new) == FALSE) {
		g_warning ("%s: failed to send query", __FUNCTION__);
		return FALSE;
	}
  
	/* clean up */
	g_free (from);
	g_free (seconds);

	return TRUE;
}

static gboolean gj_iq_respond_to_jabber_iq_version (const GjInfoquery iq)
{
	GjInfoquery iq_new = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	xmlChar *tmp = NULL;

	gchar *from = NULL;

#if defined G_OS_WIN32
	const char *os = "Windows";
#elif defined G_OS_UNIX
	const char *os = "Unix";
#elif defined G_OS_BEOS
	const char *os = "BeOS";
#else
	const char *os = "Unknown Operating System";
#endif

	if ((iq_new = gj_iq_new (GjInfoqueryTypeResult)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"from")) == NULL || xmlStrlen (tmp) < 1) {
		g_warning ("%s: failed to obtain 'from' information", __FUNCTION__);
		return FALSE;
	} else {
		from = g_strdup_printf ("%s",tmp);
	}
  
	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:version", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"name", (guchar*)PACKAGE_NAME);
	sub = xmlNewChild (node, ns, (guchar*)"os", (guchar*)os);
	sub = xmlNewChild (node, ns, (guchar*)"version", (guchar*)PACKAGE_VERSION);
 
	/* set struct info */
	gj_iq_set_to (iq_new,from);
	gj_iq_set_id (iq_new, gj_iq_get_id (iq));

	if (gj_iq_build (iq_new,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	if (gj_iq_send (iq_new) == FALSE) {
		g_warning ("%s: failed to send query", __FUNCTION__);
		return FALSE;
	}
  
	/* clean up */
	g_free (from);

	return TRUE;
}

static gboolean gj_iq_respond_to_jabber_iq_time (const GjInfoquery iq)
{
	GjInfoquery iq_new = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	xmlChar *tmp = NULL;

	gchar *from = NULL;
	gchar *utc = NULL;

	if ((iq_new = gj_iq_new (GjInfoqueryTypeResult)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"from")) == NULL || xmlStrlen (tmp) < 1) {
		g_warning ("%s: failed to obtain 'from' information", __FUNCTION__);
		return FALSE;
	} else {
		from = g_strdup_printf ("%s",tmp);
	}

	utc = g_strdup_printf ("%sT%s", gj_get_datestamp_long (), gj_get_timestamp ());
  
	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:time", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"utc", (guchar*)utc);
	sub = xmlNewChild (node, ns, (guchar*)"tz", (guchar*)gj_get_time_zone ());  /* need to get TZ */
	sub = xmlNewChild (node, ns, (guchar*)"display", (guchar*)gj_get_datestamp ());
 
	/* set struct info */
	gj_iq_set_to (iq_new,from);
	gj_iq_set_id (iq_new, gj_iq_get_id (iq));

	if (gj_iq_build (iq_new,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	if (gj_iq_send (iq_new) == FALSE) {
		g_warning ("%s: failed to send query", __FUNCTION__);
		return FALSE;
	}
  
	/* clean up */
	g_free (from);
	g_free (utc);

	return TRUE;
}

static gboolean gj_iq_respond_to_disco_info (const GjInfoquery iq)
{
	GjInfoquery iq_new = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	xmlChar *tmp = NULL;

	gchar *from = NULL;

	if ((iq_new = gj_iq_new (GjInfoqueryTypeResult)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"from")) == NULL || xmlStrlen (tmp) < 1) {
		g_warning ("%s: failed to obtain 'from' information", __FUNCTION__);
		return FALSE;
	} else {
		from = g_strdup_printf ("%s",tmp);
	}

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/disco#info", NULL);

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:agents");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:auth");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:browse");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:data");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:event");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:last");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:oob");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:register");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:roster");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:time");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"jabber:iq:version");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"vcard-temp");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"http://jabber.org/protocol/si");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"http://jabber.org/protocol/si/profile/file-transfer");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"http://jabber.org/protocol/bytestreams");

	sub = xmlNewChild (node, ns, (guchar*)"feature", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"http://jabber.org/protocol/ibb");

	/* set struct info */
	gj_iq_set_to (iq_new, from);
	gj_iq_set_id (iq_new, gj_iq_get_id (iq));

	if (gj_iq_build (iq_new,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	if (gj_iq_send (iq_new) == FALSE) {
		g_warning ("%s: failed to send query", __FUNCTION__);
		return FALSE;
	}
  
	/* clean up */
	g_free (from);

	return TRUE;
}


static gboolean gj_iq_respond_with_service_unavailable (const GjInfoquery iq)
{
	GjInfoquery iq_new = NULL;

	xmlNodePtr node1 = NULL;
	xmlNodePtr node2 = NULL;
	xmlNsPtr ns = NULL;

	GList *nodes = NULL;
  
	const xmlChar *tmp = NULL;
  
	gchar *namespace = NULL;
	gchar *from = NULL;

	if ((tmp = gj_parser_find_by_ns_and_ret (gj_iq_get_xml (iq))) == NULL || xmlStrlen (tmp) < 1) {
		g_warning ("%s: could not determin namespace from infoquery", __FUNCTION__);
		return FALSE;
	} else {
		namespace = g_strdup_printf ("%s",tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (gj_iq_get_xml (iq), (guchar*)"from")) == NULL || xmlStrlen (tmp) < 1) {
		g_warning ("%s: failed to obtain 'from' information", __FUNCTION__);
		return FALSE;
	} else {
		from = g_strdup_printf ("%s",tmp);
	}

	if ((iq_new = gj_iq_new (GjInfoqueryTypeError)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set xml */
	node1 = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node1, (guchar*)namespace, NULL);

	node2 = xmlNewNode (NULL, (guchar*)"error");
	xmlSetProp (node2, (guchar*)"code", (guchar*)"503");  
	xmlNodeSetContent (node2, (guchar*)"Service Unavailable");

	nodes = g_list_append (nodes,node1);
	nodes = g_list_append (nodes,node2);
 
	/* set struct info */
	gj_iq_set_to (iq_new, (gchar*)from);

	if (gj_iq_build_from_list (iq_new,nodes) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	if (gj_iq_send (iq_new) == FALSE) {
		g_warning ("%s: failed to send query", __FUNCTION__);
		return FALSE;
	}
  
	/* clean up */
	g_free (namespace);
	g_free (from);

	if (nodes) {
		g_list_free (nodes);
	}

	return TRUE;
}

#ifdef __cplusplus
}
#endif
