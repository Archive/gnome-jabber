/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include <unistd.h>

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_agents.h"
#include "gj_iq_requests.h"

#include "gj_gtk_add_contact.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_support.h"
#include "gj_gtk_connection_settings.h"


typedef struct {
	GtkWidget *window;

} GjFirstTimeRunDialog;


/* gui callbacks */
static void on_destroy (GtkWidget *widget, GjFirstTimeRunDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjFirstTimeRunDialog *dialog);


static GjFirstTimeRunDialog *current_dialog = NULL;


gboolean gj_gtk_ftr_load ()
{
	GladeXML *xml = NULL;
	GjFirstTimeRunDialog *dialog = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjFirstTimeRunDialog, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "first_time_run",
				     NULL,
				     "first_time_run", &dialog->window,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "first_time_run", "response", on_response,
			      "first_time_run", "destroy", on_destroy,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	return TRUE;
}

static void on_destroy (GtkWidget *widget, GjFirstTimeRunDialog *dialog)
{
	current_dialog = NULL;

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjFirstTimeRunDialog *dialog)
{
	if (response == 1) {
		gj_gtk_na_load ();
	} else if (response == 2) {
		gj_gtk_cs_load ();
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
