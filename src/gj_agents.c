/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include "gj_agents.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"


static void gj_agent_init ();

/* agent item */
static void gj_agent_destroy (GjAgent ag);

/* agent list */
static void gj_agent_list_free_each_cb (gpointer data, gpointer user_data);

static gboolean gj_agent_list_add (GjAgent ag);
static gboolean gj_agent_list_del (GjAgent ag);

static GList *gj_agent_list_get ();

static void gj_agent_list_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);
static GList *gj_agent_list_parse (xmlNodePtr parent);

/* sets */
static gboolean gj_agent_set_callback (GjAgentCallback cb);
static gboolean gj_agent_set_userdata (void *userdata);

/* gets */
static GjAgentCallback gj_agent_get_callback ();
static void *gj_agent_get_userdata ();


struct t_agent {
	gchar *jid;
	gchar *name;
	gchar *description;
 
	gchar *transport;
	gchar *groupchat;
	gchar *service;
	gchar *registers;
	gchar *search;
};


static GList *agent_list = NULL;
static GjAgentCallback agent_cb = NULL;
static void *agent_userdata = NULL;


static void gj_agent_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;
}

static gboolean gj_agent_list_add (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	agent_list = g_list_append (agent_list, ag);
	return TRUE;
}

static gboolean gj_agent_list_del (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	agent_list = g_list_remove (agent_list, ag);
	return TRUE;
}

static GList *gj_agent_list_get ()
{
	return agent_list;
}

static void gj_agent_list_free_each_cb (gpointer data, gpointer user_data)
{
	GjAgent ag = NULL;

	ag = (GjAgent) data;

	if (ag == NULL) {
		return;
	}
  
	gj_agent_free (ag);
}


/* 
 * item functions
 */
GjAgent gj_agent_new (const gchar *jid)
{
	GjAgent ag;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}
  
	gj_agent_init ();

	if ((ag = g_new (struct t_agent,1))) {
		ag->jid = g_strdup (jid);
		ag->name = NULL;
		ag->description = NULL;

		ag->transport = NULL;
		ag->groupchat = NULL;
		ag->service = NULL;
		ag->registers = NULL;
		ag->search = NULL;

		gj_agent_list_add (ag);
	}
  
	return ag;  
}

void gj_agent_free (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return;
	}

	/* free mem */
	gj_agent_list_del (ag);  

	/* destroy members */
	gj_agent_destroy (ag);

	/* free structure */
	g_free (ag);
}

static void gj_agent_destroy (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return;
	}
  
	if (ag->jid != NULL) {
		g_free (ag->jid);
		ag->jid = NULL;
	}

	if (ag->name != NULL) {
		g_free (ag->name);
		ag->name = NULL;
	}

	if (ag->description != NULL) {
		g_free (ag->description);
		ag->description = NULL;
	}

	if (ag->transport != NULL) {
		g_free (ag->transport);
		ag->transport = NULL;
	}

	if (ag->groupchat != NULL) {
		g_free (ag->groupchat);
		ag->groupchat = NULL;
	}

	if (ag->service != NULL) {
		g_free (ag->service);
		ag->service = NULL;
	}

	if (ag->registers != NULL) {
		g_free (ag->registers);
		ag->registers = NULL;
	}

	if (ag->search != NULL) {
		g_free (ag->search);
		ag->search = NULL;
	}
}

/*
 * list functions
 */
gboolean gj_agent_list_request (GjConnection c, 
				gchar *server, 
				GjAgentCallback cb, 
				void *userdata)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	/* clean list if currently populated */
	if (gj_agent_list_get () != NULL) {
		g_list_foreach (gj_agent_list_get (), gj_agent_list_free_each_cb, NULL);
	}

	/* set call back proc */
	gj_agent_set_callback (cb);
	gj_agent_set_userdata (userdata);

	/* do request */
	gj_iq_request_agents (c, server, gj_agent_list_cb, userdata);

	return TRUE;
}

void gj_agent_list_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	gint error_code = 0;
	gchar *error_message = NULL;
	GjAgentCallback cb = NULL;
	void *userdata = NULL;

	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	if (!iq || !iq_related) {
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) != NULL) {
			if ((errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code")) != NULL) {
				error_code = atoi ((gchar*)errorCode); 
			}
	  
			if ((errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error")) != NULL) {
				error_message = g_strdup ((gchar*)errorMessage);
			}
		} else {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
		}
	}

	cb = gj_agent_get_callback ();
	userdata = gj_agent_get_userdata ();

	if (cb == NULL) {
		g_warning ("%s: no callback", __FUNCTION__);
		g_free (error_message);
		return;
	}

	if (agent_list != NULL) {
		/* free list */
	}
      
	/* parse list */
	agent_list = gj_agent_list_parse (node);
  
	/* return list */
	(cb) (agent_list, error_code, error_message, userdata);
  
	/* clean up */
	g_free (error_message);
}

GList *gj_agent_list_parse (xmlNodePtr parent)
{
	gint count = 0;
	xmlNodePtr node = NULL;

	if (parent == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return NULL;
	}

	/* create list */
	gj_agent_init ();

	/* get item node */
	node = gj_parser_find_by_node (parent, (guchar*)"agent");

	/* get items in agent */
	while (node) {
			xmlChar *tmp = NULL;

			count++;
      
			/* get data */
			if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"jid")) != NULL) {
				gchar *jid = NULL;
				gchar *resource = NULL;
				gchar **vjid = NULL;

				GjAgent ag = NULL;

				vjid = g_strsplit ((gchar*)tmp,"/",2);

				if (vjid != NULL) {
					jid = g_strdup (vjid[0]);
	      
					if (vjid[1] != NULL) {
						resource = g_strdup (vjid[1]);
					}
	      
					g_strfreev (vjid);
				}
	  
				/* create new agent item */
				ag = gj_agent_new (jid);
      
				/* check it... */
				if (ag == NULL) {
					g_warning ("%s: failed to create new agent, continuing to next item...", __FUNCTION__);
					continue;
				}
	  
#if 0
				/* resource */
				if (resource != NULL) {
					ag->resource = resource;
				}
#endif

				/* find other details */
				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"name")) != NULL) {
					gj_agent_set_name (ag, (gchar*)tmp);
				}

				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"description")) != NULL) {
					gj_agent_set_description (ag, (gchar*)tmp);
				}

				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"transport")) != NULL) {
					gj_agent_set_transport (ag, (gchar*)tmp);
				}

				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"groupchat")) != NULL) {
					gj_agent_set_groupchat (ag, (gchar*)tmp);
				}

				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"service")) != NULL) {
					gj_agent_set_service (ag, (gchar*)tmp);
				}

				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"register")) != NULL) {
					gj_agent_set_registers (ag, (gchar*)tmp);
				}

				if ((tmp = gj_parser_find_by_node_and_ret (node, (guchar*)"search")) != NULL) {
					gj_agent_set_search (ag, (gchar*)tmp);
				}
			}

			node = node->next;
		}

	g_message ("%s: items:%d", __FUNCTION__, count);

	return agent_list;
}

/* sets */
gboolean gj_agent_set_callback (GjAgentCallback cb)
{
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	agent_cb = cb;
	return TRUE;
}

gboolean gj_agent_set_userdata (void *userdata)
{
	if (userdata == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	agent_userdata = userdata;
	return TRUE;
}

/* gets*/
GjAgentCallback gj_agent_get_callback ()
{
	return agent_cb;
}

void *gj_agent_get_userdata ()
{
	return agent_userdata;
}


/*
 * member functions
 */

/* sets */
gboolean gj_agent_set_jid (GjAgent ag, gchar *jid)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->jid != NULL) {
		g_free (ag->jid);
		ag->jid = NULL;
	}
  
	ag->jid = g_strdup (jid);
	return TRUE;
}

gboolean gj_agent_set_name (GjAgent ag, gchar *name)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (name == NULL) {
		g_warning ("%s: name was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->name != NULL) {
		g_free (ag->name);
		ag->name = NULL;
	}
  
	ag->name = g_strdup (name);
	return TRUE;
}

gboolean gj_agent_set_description (GjAgent ag, gchar *description)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (description == NULL) {
		g_warning ("%s: description was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->description != NULL) {
		g_free (ag->description);
		ag->description = NULL;
	}
  
	ag->description = g_strdup (description);
	return TRUE;
}

gboolean gj_agent_set_transport (GjAgent ag, gchar *transport)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (transport == NULL) {
		g_warning ("%s: transport was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->transport != NULL) {
		g_free (ag->transport);
		ag->transport = NULL;
	}
  
	ag->transport = g_strdup (transport);
	return TRUE;
}

gboolean gj_agent_set_groupchat (GjAgent ag, gchar *groupchat)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (groupchat == NULL) {
		g_warning ("%s: groupchat was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->groupchat != NULL) {
		g_free (ag->groupchat);
		ag->groupchat = NULL;
	}
  
	ag->groupchat = g_strdup (groupchat);
	return TRUE;
}

gboolean gj_agent_set_service (GjAgent ag, gchar *service)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (service == NULL) {
		g_warning ("%s: service was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->service != NULL) {
		g_free (ag->service);
		ag->service = NULL;
	}
  
	ag->service = g_strdup (service);
	return TRUE;
}

gboolean gj_agent_set_registers (GjAgent ag, gchar *registers)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (registers == NULL) {
		g_warning ("%s: registers was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->registers != NULL) {
		g_free (ag->registers);
		ag->registers = NULL;
	}
  
	ag->registers = g_strdup (registers);
	return TRUE;
}

gboolean gj_agent_set_search (GjAgent ag, gchar *search)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (search == NULL) {
		g_warning ("%s: search was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ag->search != NULL) {
		g_free (ag->search);
		ag->search = NULL;
	}
  
	ag->search = g_strdup (search);
	return TRUE;
}

/* gets */
gchar *gj_agent_get_jid (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->jid;
}

gchar *gj_agent_get_name (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->name;
}

gchar *gj_agent_get_description (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->description;
}

gchar *gj_agent_get_transport (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->transport;
}

gchar *gj_agent_get_groupchat (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->groupchat;
}

gchar *gj_agent_get_service (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->service;
}

gchar *gj_agent_get_registers (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->registers;
}

gchar *gj_agent_get_search (GjAgent ag)
{
	if (ag == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return NULL;
	}

	return ag->search;
}

#ifdef __cplusplus
}
#endif
