/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include "gj_roster_item.h"
#include "gj_roster.h"
#include "gj_jid.h"
#include "gj_support.h"
#include "gj_presence.h"


struct t_roster_item {
	gint id; /* unique id */

	GjJID jid; 

	/*   gchar *jid; */
	gchar *name;
	gchar *resource;
	gchar *agent;

	GList *groups;

	/* points back to the parent roster this item belongs to */
	GjRoster roster;

	/* is this a permanant member - i.e. not just someone that has sent me a message */
	gboolean is_permanent;        

	/* is this just my jid/some resource ? */
	gboolean is_another_resource;

	/* is this roster item about to be deleted and shouldnt be added to the roster */
	gboolean is_set_for_removal;  

	/* used for a group chat item */
	gboolean is_for_group_chat;

	/* used to redirect group chat messages to the join dialog if not connected */
	gboolean is_joining_group_chat;

	GjRosterItemSubscription subscription;
	gchar *ask;

	/* presence information */
	GjPresence pres;
	GjPresence last_pres;
};


GjRosterItem gj_roster_item_new (const gchar *jid)
{
	GjRosterItem ri = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	if (gj_jid_string_is_valid_jid (jid) == FALSE) {
		g_warning ("%s: jid is not a valid jid:'%s'", __FUNCTION__, jid);
		return NULL;
	}

	/* find to see if this is duplication ? */

	if ((ri = g_new0 (struct t_roster_item,1))) {
		ri->id = gj_get_unique_id ();

		ri->jid = gj_jid_new (jid);
		ri->name = gj_jid_get_part_name_new (ri->jid);

		ri->roster = NULL;

		ri->is_permanent = FALSE;
		ri->is_another_resource = FALSE;
		ri->is_set_for_removal = FALSE;

		ri->is_for_group_chat = FALSE;
		ri->is_joining_group_chat = FALSE;

		ri->subscription = GjRosterItemSubscriptionNone;
		ri->ask = NULL;

		ri->pres = gj_presence_new (GjPresenceTypeUnAvailable, GjPresenceShowNormal);
		ri->last_pres = NULL;

		gj_roster_index_add_item (ri);
	}
  
	return ri;  
}

GjRosterItem gj_roster_item_new_unknown_by_jid (const gchar *jid)
{
	GjRosterItem ri = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	/* create new roster item */
	if ((ri = gj_roster_item_new (jid)) == NULL) {
		g_warning ("%s: could not create new roster item", __FUNCTION__);
		return NULL;
	}

	ri->is_permanent = FALSE;
    
	return ri;  
}

void gj_roster_item_free (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return;
	}

	/* free mem */
	gj_roster_index_del_item (ri); 

	if (ri->jid != NULL) {
		gj_jid_unref (ri->jid);
	}

	g_free (ri->name);
	g_free (ri->resource);
	g_free (ri->agent);
	g_free (ri->ask);
    
	/* presence */
	if (ri->pres != NULL) {
		gj_presence_unref (ri->pres); 
	}

	if (ri->last_pres != NULL) {
		gj_presence_unref (ri->last_pres); 
	}

	/* free structure */
	g_free (ri);
}

gboolean gj_roster_item_groups_free (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri->groups == NULL) {
		return TRUE;
	}

	g_list_foreach (ri->groups, (GFunc)g_free, NULL); 
	g_list_free (ri->groups);
	ri->groups = NULL;

	return TRUE;
}

gboolean gj_roster_item_groups_add (GjRosterItem ri, const gchar *group)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (group == NULL) {
		g_warning ("%s: group was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri->groups = g_list_append (ri->groups, g_strdup (group));
	return TRUE;
}

gboolean gj_roster_item_groups_del (GjRosterItem ri, const gchar *group)
{
	gint i = 0;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (group == NULL) {
		g_warning ("%s: group was NULL", __FUNCTION__);
		return FALSE;
	}

	for (i=0; i<g_list_length (ri->groups); i++) {
		gchar *g = NULL;

		g = (gchar*) g_list_nth_data (ri->groups, i);
     
		if (g && strcmp (group, g) == 0) {
			ri->groups = g_list_remove (ri->groups, g);
			return TRUE;
		}
	}

	return FALSE;
}

gint gj_roster_item_groups_count (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	return g_list_length (ri->groups);
}


/*
 * member functions 
 */

/* sets */
gboolean gj_roster_item_set_name (GjRosterItem ri, const gchar *name)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (name == NULL) {
		g_warning ("%s: name was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (ri->name);
	ri->name = g_strdup (name);

	return TRUE;
}

gboolean gj_roster_item_set_roster (GjRosterItem ri, GjRoster r)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	/* should this be done better ?! */

	ri->roster = r;
	return TRUE;
}

gboolean gj_roster_item_set_agent (GjRosterItem ri, const gchar *agent)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (agent == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (ri->agent);
	ri->agent = g_strdup (agent);

	return TRUE;
}

gboolean gj_roster_item_set_groups (GjRosterItem ri, GList *groups)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri->groups != NULL) {
		gj_roster_item_groups_free (ri);
		ri->groups = NULL;
	}
  
	ri->groups = groups;
	return TRUE;
}

gboolean gj_roster_item_set_is_permanent (GjRosterItem ri, gboolean is_permanent)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri->is_permanent = is_permanent;
	return TRUE;
}

gboolean gj_roster_item_set_is_another_resource (GjRosterItem ri, gboolean is_another_resource)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri->is_another_resource = is_another_resource;
	return TRUE;
}

gboolean gj_roster_item_set_is_set_for_removal (GjRosterItem ri, gboolean is_set_for_removal) 
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri->is_set_for_removal = is_set_for_removal;
	return TRUE;
}

gboolean gj_roster_item_set_is_for_group_chat (GjRosterItem ri, gboolean is_for_group_chat) 
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri->is_for_group_chat = is_for_group_chat;
	return TRUE;
}

gboolean gj_roster_item_set_is_joining_group_chat (GjRosterItem ri, gboolean is_joining_group_chat)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	ri->is_joining_group_chat = is_joining_group_chat;
	return TRUE;
}

gboolean gj_roster_item_set_subscription (GjRosterItem ri, GjRosterItemSubscription subscription)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (subscription < 1 || subscription > GjRosterItemSubscriptionEnd) {
		g_warning ("%s: subscription:%d was out of range", __FUNCTION__, subscription);
		return FALSE;
	}

	ri->subscription = subscription;
	return TRUE;
}

gboolean gj_roster_item_set_ask (GjRosterItem ri, const gchar *ask)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ask == NULL) {
		g_warning ("%s: ask was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (ri->ask);
	ri->ask = g_strdup (ask);

	return TRUE;
}

gboolean gj_roster_item_set_presence (GjRosterItem ri, GjPresence pres)
{
	GjJID presence_jid = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri->pres != NULL) {
		if (ri->last_pres != NULL) {
			gj_presence_unref (ri->last_pres); 
		}

		ri->last_pres = ri->pres;  
	}
  
	ri->pres = gj_presence_ref (pres);

	/* update jid's resource */
	presence_jid = gj_presence_get_from_jid (ri->pres);

	if (gj_jid_get_resource (presence_jid)) {
		gj_jid_set_resource (ri->jid, gj_jid_get_resource (presence_jid));
	}
		      
	return TRUE;
}

/* gets */
gint gj_roster_item_get_id (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return -1;
	}

	return ri->id;
}

const GjJID gj_roster_item_get_jid (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->jid;
}

const gchar *gj_roster_item_get_name (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->name;
}

GjRoster gj_roster_item_get_roster (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->roster;
}

GList *gj_roster_item_get_groups (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->groups;
}

gchar *gj_roster_item_get_groups_as_string (GjRosterItem ri)
{
	gchar *groups = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}
  
	if (gj_roster_item_get_groups (ri) != NULL) {
		gint i = 0;
      
		for (i=0;i<g_list_length (gj_roster_item_get_groups (ri));i++) {
			gchar *g = (gchar*) g_list_nth_data (gj_roster_item_get_groups (ri), i);
	  
			if (g == NULL) {
				continue; 
			}
	  
			if (groups == NULL) {
				groups = g_strdup (g);
			} else {
				groups = g_strconcat (groups, ", ", g, NULL);
			}
		}
	}

	return groups;
}

gboolean gj_roster_item_get_is_permanent (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	return ri->is_permanent;
}

gboolean gj_roster_item_get_is_another_resource (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	return ri->is_another_resource;
}

gboolean gj_roster_item_get_is_set_for_removal (GjRosterItem ri) 
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	return ri->is_set_for_removal;
}

gboolean gj_roster_item_get_is_for_group_chat (GjRosterItem ri) 
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	return ri->is_for_group_chat;
}

gboolean gj_roster_item_get_is_joining_group_chat (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	return ri->is_joining_group_chat;
}

GjRosterItemSubscription gj_roster_item_get_subscription (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return -1;
	}

	return ri->subscription;
}

const gchar *gj_roster_item_get_ask (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->ask;
}

GjPresence gj_roster_item_get_presence (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->pres;
}

GjPresence gj_roster_item_get_last_presence (GjRosterItem ri)
{
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	return ri->last_pres;
}

#ifdef __cplusplus
}
#endif
