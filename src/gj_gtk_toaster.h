/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_gtk_roaster_h
#define __gj_gtk_toaster_h

typedef struct t_GjToasterWindow GjToasterWindow;
typedef void (*GjToasterDestroyCallback) (GjToasterWindow *window, gpointer userdata);

GjToasterWindow *gj_gtk_toaster_new (gint seconds, /* time to show for */
				     GjToasterDestroyCallback cb, 
				     gpointer user_data);   
gboolean gj_gtk_toaster_free (GjToasterWindow *window);

gboolean gj_gtk_toaster_show (GjToasterWindow *window);
gboolean gj_gtk_toaster_hide_and_free (GjToasterWindow *window);
gboolean gj_gtk_toaster_hide (GjToasterWindow *window);

/* used for people to populate their widgets here! */
GtkWidget *gj_gtk_toaster_get_container (GjToasterWindow *window);
gboolean gj_gtk_toaster_get_is_shown (GjToasterWindow *window);

/* note: this frees the window too */
gboolean gj_gtk_toaster_set_hide_and_free_timeout (GjToasterWindow *window, gint seconds); 

/* get window so we it can be used as a container to add widgets */
GtkWidget *gj_gtk_toaster_get_container (GjToasterWindow *window);

#endif

#ifdef __cplusplus
}
#endif

