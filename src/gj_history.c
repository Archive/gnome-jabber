/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_history.h"
#include "gj_support.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_config.h"
#include "gj_main.h"
#include "gj_jid.h"
#include "gj_log.h"

#define PATH_HISTORY "history"


struct t_history {
	gint ri_id;
  
	gchar *filename;
	GIOChannel *channel;

	gchar *url;
};


static void  gj_history_init ();

/* item functions */
static void gj_history_destroy_cb (gpointer data);

/* utils */
static gchar *gj_history_check_dir ();
static gchar *gj_history_check_filename (GjJID j);
static gboolean gj_history_update_url (GjHistory hist);


static gchar *history_path = NULL;
static GHashTable *historys = NULL;


static void gj_history_init ()
{
	static gboolean initiated = FALSE;
	const gchar *saved_path = NULL;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;

	historys = g_hash_table_new_full (g_int_hash, 
					  g_int_equal, 
					  NULL, 
					  (GDestroyNotify) gj_history_destroy_cb); 

	saved_path = gj_config_get_mpf_history_location ();
	if (saved_path == NULL || g_utf8_strlen (saved_path, -1) < 1) {
		gchar *path = NULL;

		path = gj_history_check_dir ();
		gj_config_set_mpf_history_location (path);

		history_path = g_strdup (path);

		g_free (path);
	} else {
		/* use saved path */
		history_path = g_strdup (saved_path);
	}
}

static gchar *gj_history_check_dir ()
{
	gchar *dir_level1 = NULL;
	gchar *dir_level2 = NULL;

#ifdef G_OS_WIN32

	dir_level1 = g_build_filename (g_get_home_dir (), PACKAGE_NAME, NULL); 
	dir_level2 = g_build_filename (g_get_home_dir (), PACKAGE_NAME, PATH_HISTORY, NULL);
#else
	dir_level1 = g_build_filename (g_get_home_dir (), ".gnome2", PACKAGE_NAME, NULL);
	dir_level2 = g_build_filename (g_get_home_dir (), ".gnome2", PACKAGE_NAME, PATH_HISTORY, NULL);
#endif

	if (!g_file_test (dir_level1, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))) {
		if (!gj_create_dir (dir_level1)) {
			g_warning ("%s: could not log create config dir:'%s'", __FUNCTION__, dir_level1);
			g_free (dir_level1);
			g_free (dir_level2);
			return NULL;
		}
	}

	if (!g_file_test (dir_level2, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))) {
		if (!gj_create_dir (dir_level2)) {
			g_warning ("%s: could not log dir:'%s'", __FUNCTION__, dir_level2);
			g_free (dir_level1);
			g_free (dir_level2);
			return NULL;
		}
	}

	g_free (dir_level1);
  
	return dir_level2;
}

static gchar *gj_history_check_filename (GjJID j)
{
	gchar *dir = NULL;
	gchar *filename = NULL;

	if (j == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	dir = gj_history_check_dir ();
	filename = g_build_filename (dir, gj_jid_get_without_resource (j), NULL);

	g_free (dir);

	return filename;
}

static void gj_history_destroy_cb (gpointer data)
{
	GjHistory hist = (GjHistory) data;

	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return;
	}

	hist->ri_id = -1;

	g_free (hist->filename);

	if (hist->channel != NULL) {
		g_io_channel_shutdown (hist->channel,TRUE,NULL);
		hist->channel = NULL;
	}

	g_free (hist->url);

	g_free (hist);
}

GjHistory gj_history_new (GjRosterItem ri)
{
	GjHistory hist = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	gj_history_init ();

	if ((hist = g_new0 (struct t_history,1))) {
		hist->ri_id = gj_roster_item_get_id (ri);

		g_hash_table_insert (historys, &hist->ri_id, hist);
	}

	return hist;
}

gboolean gj_history_free (GjHistory hist)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return FALSE;
	}

	g_hash_table_remove (historys, &hist->ri_id);
	return TRUE;
}

GjHistory gj_history_find (gint id)
{
	GjHistory hist = NULL;

	/* if no hash table is set up yet, no historys can exist. */
	if (historys == NULL) {
		return NULL; 
	}
    
	if (id < 0) {
		g_warning ("%s: id:%d was < 0", __FUNCTION__, id);
		return NULL;
	}
    
	hist = g_hash_table_lookup (historys, &id);
	return hist;
}

/*
 * member data
 */ 
gint gj_history_get_ri_id (GjHistory hist)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return -1;
	}

	return hist->ri_id;
}

gboolean gj_history_set_filename (GjHistory hist, const gchar *filename)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return FALSE;
	}

	if (filename == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (hist->filename);
	hist->filename = g_strdup (filename);

	return TRUE;
}

const gchar *gj_history_get_filename (GjHistory hist)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return NULL;
	}

	return hist->filename;
}

GIOChannel *gj_history_get_channel (GjHistory hist)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return NULL;
	}

	return hist->channel;
}

const gchar *gj_history_get_url (GjHistory hist)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return FALSE;
	}

	return hist->url;
}

static gboolean gj_history_update_url (GjHistory hist)
{
	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (hist->url);
	hist->url = g_strconcat ("file://", gj_history_get_filename (hist), NULL);
	return TRUE;
}

gboolean gj_history_start (GjHistory hist)
{
	guint bytes = 0;
	gboolean new_file = FALSE;
	gchar *app_start = NULL;

	GjRosterItem ri = NULL;
	GjJID j = NULL;

	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((ri = gj_rosters_find_item_by_id (gj_history_get_ri_id (hist))) == NULL) {
		g_warning ("%s: could not find by id:%d", __FUNCTION__, gj_history_get_ri_id (hist));
		return FALSE;
	}
  
	j = gj_roster_item_get_jid (ri);

	if (gj_history_get_filename (hist) == NULL) {
		gchar *filename = NULL;

		filename = gj_history_check_filename (j);

		if (filename != NULL) {
			gj_history_set_filename (hist, filename);
			gj_history_update_url (hist);

			g_free (filename);
		} else {
			g_warning ("%s: could not create string filename for history file, jid:'%s'",
				   __FUNCTION__, gj_jid_get_full (j));
		}
	}

	/* does the file exist */
	if (g_file_test (gj_history_get_filename (hist), G_FILE_TEST_EXISTS) == FALSE) {
		g_message ("%s: filename:'%s' does not exist, creating...", __FUNCTION__, gj_history_get_filename (hist));
		new_file = TRUE;
	}

	/* this opens a file or creates it */
	hist->channel = g_io_channel_new_file (gj_history_get_filename (hist),
					       "a+",
					       NULL);

	/* if this is a new file, add the header information */
	if (new_file == TRUE) {
		gchar *first_line = NULL;
		first_line = g_strdup_printf ("<html>"
					      "<head>"
					      "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"
					      "<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\"/>"
					      "</head>"
					      "</html>\n",
					      gnome_jabber_get_css_file ());
		
		g_io_channel_write_chars (gj_history_get_channel (hist),
					  first_line,
					  -1,
					  &bytes,
					  NULL); 
		
		g_free (first_line);
	}

	/* write the fact this is a start up procedure */
	app_start = g_strdup_printf ("<html>"
				     "<body>"
				     "<div class=\"message\">"
				     "<hr>"
				     "<span class=\"date\">%s</span> "
				     "<span class=\"title\">Application Started</span>"
				     "<br>"
				     "</div>"
				     "</body>"
				     "</html>\n", 
				     gj_get_timestamp_nice ());

	g_io_channel_write_chars (gj_history_get_channel (hist),
				  app_start,
				  -1,
				  &bytes,
				  NULL); 

	g_free (app_start);

	/* flush to make sure tailing is up to date */
	g_io_channel_flush (gj_history_get_channel (hist), NULL);

	/* check it was created */
	if (hist->channel == NULL) {
		g_warning ("%s: channel failed to append file:'%s'", __FUNCTION__, gj_history_get_filename (hist));
		return FALSE;
	}

	return TRUE;
}

gboolean gj_history_write_sent_by_ri_id (gint ri_id, 
					 const gchar *date, 
					 const gchar *subject, 
					 const gchar *message)
{
	GjHistory hist = NULL;

	if (ri_id < 0) {
		g_warning ("%s: roster item id:%d was < 0", __FUNCTION__, ri_id);
		return FALSE;
	}

	if ((hist = gj_history_find (ri_id)) == NULL) {
		g_warning ("%s: could not find by roster item id:%d", __FUNCTION__, ri_id);
		return FALSE;
	}  

	return gj_history_write_sent (hist, date, subject, message);
}

gboolean gj_history_write_sent (GjHistory hist, 
				const gchar *date, 
				const gchar *subject, 
				const gchar *message)
{
	guint bytes = 0;
	gchar *formatted = NULL;

	if (gj_config_get_mpf_use_history () == FALSE) {
		return TRUE;
	}

	if (hist == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_history_get_channel (hist) == NULL) {
		g_warning ("%s: channel not yet initalised for ri_id:%d", __FUNCTION__, gj_history_get_ri_id (hist));
		return FALSE;
	}

	formatted = g_strdup_printf ("<html>"
				     "<body>"
				     "<div class=\"message\">"
				     "<span class=\"date\">%s</span> "
				     "<span class=\"body\">%s</span> "
				     "<span class=\"from\">%s<br/></span>"
				     "</div>"
				     "</body>"
				     "</html>\n", 
				     date, 
				     "[sent]", 
				     message);

	/* write to file */
	if (g_utf8_validate (formatted, -1, NULL) == FALSE) {
		fprintf (stderr, "[CRITICAL ERROR] !!!! INVALID UTF8 - WRITING HISTORY\n");
		fflush (stderr);
		return FALSE;
	}

	g_io_channel_write_chars (gj_history_get_channel (hist),
				  formatted,
				  -1,
				  &bytes,
				  NULL);  

	g_io_channel_flush (gj_history_get_channel (hist), NULL);

	g_message ("%s: wrote %d bytes of %d characters to history (channel:0x%.8x)", 
		   __FUNCTION__, (gint)bytes, (gint)g_utf8_strlen (formatted, -1), (gint)gj_history_get_channel (hist)); 

	/* clean up */
	g_free (formatted);

	return TRUE;
}

gboolean gj_history_write_received_by_ri_id (gint ri_id, 
					     const gchar *date, 
					     const gchar *subject, 
					     const gchar *message)
{
	GjHistory hist = NULL;

	if (ri_id < 0) {
		g_warning ("%s: roster item id:%d was < 0", __FUNCTION__, ri_id);
		return FALSE;
	}

	if ((hist = gj_history_find (ri_id)) == NULL) {
		g_warning ("%s: could not find by roster item id:%d", __FUNCTION__, ri_id);
		return FALSE;
	}  

	return gj_history_write_received (hist, date, subject, message);
}

gboolean gj_history_write_received (GjHistory hist, 
				    const gchar *date, 
				    const gchar *subject, 
				    const gchar *message)
{
	guint bytes = 0;
	gchar *formatted = NULL;

	if (gj_config_get_mpf_use_history () == FALSE) {
		return TRUE;
	}

	if (hist == NULL) {
		g_warning ("%s: history was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_history_get_channel (hist) == NULL) {
		g_warning ("%s: channel not yet initalised for roster item id:%d", __FUNCTION__, gj_history_get_ri_id (hist));
		return FALSE;
	}

	formatted = g_strdup_printf ("<html>"
				     "<body>"
				     "<div class=\"message\">"
				     "<span class=\"date\">%s</span> "
				     "<span class=\"body\">%s</span> "
				     "<span class=\"to\">%s"
				     "<br>"
				     "</span>"
				     "</div>"
				     "</body>"
				     "</html>\n", 
				     date, 
				     "[recv]", 
				     message);

	g_io_channel_write_chars (gj_history_get_channel (hist),
				  formatted,
				  -1,
				  &bytes,
				  NULL);  

	g_io_channel_flush (gj_history_get_channel (hist), NULL);

	g_message ("%s: wrote %d bytes of %d characters to history (channel:0x%.8x)", 
		   __FUNCTION__, (gint)bytes, (gint)g_utf8_strlen (formatted, -1), (gint)gj_history_get_channel (hist)); 

	/* clean up */
	g_free (formatted);

	return TRUE;
}

#ifdef __cplusplus
}
#endif
