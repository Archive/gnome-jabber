/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_GNOME
#include <gnome.h> 
#endif

#include "gj_main.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"
#include "gj_config.h"
#include "gj_connection.h"

#include "gj_gtk_my_information.h"
#include "gj_gtk_connection_settings.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;
  
	GtkWidget *entry_full_name;
	GtkWidget *entry_name_given;
	GtkWidget *entry_name_family;
	GtkWidget *entry_name_nickname;

	GtkWidget *entry_email;
	GtkWidget *entry_url;
	GtkWidget *entry_tel;
  
	GtkWidget *entry_street;
	GtkWidget *entry_extadd;
	GtkWidget *entry_locality;
	GtkWidget *entry_region;
	GtkWidget *entry_pcode;
	GtkWidget *entry_country;

	GtkWidget *entry_org_orgname;
	GtkWidget *entry_org_orgunit;
	GtkWidget *entry_title;
	GtkWidget *entry_role;
  
	GtkWidget *calendar_dob;
	GtkWidget *textview_description;
  
	GtkWidget *checkbutton_publish_in_jud;

	GjConnection c;

} GjMyInformationDialog;


static gboolean gj_gtk_mi_set ();


static void gj_gtk_mi_vcard_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);


/*
 * gui callbacks
 */
static void on_destroy (GtkWidget *widget, GjMyInformationDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjMyInformationDialog *dialog);


static GjMyInformationDialog *current_dialog = NULL;


gboolean gj_gtk_mi_load (GjConnection c)
{
	GjMyInformationDialog *dialog = NULL;
	GladeXML *xml = NULL;
	gchar *jid = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjMyInformationDialog, 1);

	dialog->c = gj_connection_ref (c);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "my_information",
				     NULL,
				     "my_information", &dialog->window,
				     "entry_full_name", &dialog->entry_full_name,
				     "entry_name_given", &dialog->entry_name_given,
				     "entry_name_family", &dialog->entry_name_family,
				     "entry_name_nickname", &dialog->entry_name_nickname,
				     "entry_email", &dialog->entry_email,
				     "entry_url", &dialog->entry_url,
				     "entry_tel", &dialog->entry_tel,
				     "entry_street", &dialog->entry_street,
				     "entry_extadd", &dialog->entry_extadd,
				     "entry_locality", &dialog->entry_locality,
				     "entry_region", &dialog->entry_region,
				     "entry_pcode", &dialog->entry_pcode,
				     "entry_country", &dialog->entry_country,
				     "entry_org_orgname", &dialog->entry_org_orgname,
				     "entry_org_orgunit", &dialog->entry_org_orgunit,
				     "entry_title", &dialog->entry_title,
				     "entry_role", &dialog->entry_role,
				     "calendar_dob", &dialog->calendar_dob,
				     "textview_description", &dialog->textview_description,
				     "checkbutton_publish_in_jud", &dialog->checkbutton_publish_in_jud,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "my_information", "response", on_response,
			      "my_information", "destroy", on_destroy,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);

	/* get my information */
	jid = g_strdup_printf ("%s@%s", gj_config_get_cs_username (), gj_config_get_cs_server ());
	gj_iq_request_vcard (c, jid, gj_gtk_mi_vcard_response, NULL);

	g_free (jid);

	return TRUE;
}

static gboolean gj_gtk_mi_set (GjMyInformationDialog *dialog) 
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter bufStart;
	GtkTextIter bufStop;

	G_CONST_RETURN gchar *full_name = NULL;
	G_CONST_RETURN gchar *name_given = NULL;
	G_CONST_RETURN gchar *name_family = NULL;
	G_CONST_RETURN gchar *name_nickname = NULL;

	G_CONST_RETURN gchar *email = NULL;
	G_CONST_RETURN gchar *url = NULL;
	G_CONST_RETURN gchar *tel = NULL;
  
	G_CONST_RETURN gchar *street = NULL;
	G_CONST_RETURN gchar *extadd = NULL;
	G_CONST_RETURN gchar *locality = NULL;
	G_CONST_RETURN gchar *region = NULL;
	G_CONST_RETURN gchar *pcode = NULL;
	G_CONST_RETURN gchar *country = NULL;
  
	G_CONST_RETURN gchar *org_orgname = NULL;
	G_CONST_RETURN gchar *org_orgunit = NULL;
	G_CONST_RETURN gchar *title = NULL;
	G_CONST_RETURN gchar *role = NULL;

	guint dob_day = 0;
	guint dob_month = 0;
	guint dob_year = 0;

	gchar *dob = NULL;
	gchar *description = NULL;
  
	gboolean publish_in_jud = FALSE;

	if (!dialog)
		return FALSE;

	/* get values */
	publish_in_jud = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_publish_in_jud));
	if (!publish_in_jud) {
		g_message ("%s: publishing information on 'personal' page in Jabber User Directory!", __FUNCTION__);

		full_name = gtk_entry_get_text (GTK_ENTRY (dialog->entry_full_name));
		name_given = gtk_entry_get_text (GTK_ENTRY (dialog->entry_name_given)); 
		name_family = gtk_entry_get_text (GTK_ENTRY (dialog->entry_name_family));
		name_nickname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_name_nickname));
		email = gtk_entry_get_text (GTK_ENTRY (dialog->entry_email));
		url = gtk_entry_get_text (GTK_ENTRY (dialog->entry_url));
		tel = gtk_entry_get_text (GTK_ENTRY (dialog->entry_tel));
	}

	street = gtk_entry_get_text (GTK_ENTRY (dialog->entry_street));
	extadd = gtk_entry_get_text (GTK_ENTRY (dialog->entry_extadd));
	locality = gtk_entry_get_text (GTK_ENTRY (dialog->entry_locality));
	region = gtk_entry_get_text (GTK_ENTRY (dialog->entry_region));
	pcode = gtk_entry_get_text (GTK_ENTRY (dialog->entry_pcode));
	country = gtk_entry_get_text (GTK_ENTRY (dialog->entry_country));

	org_orgname = gtk_entry_get_text (GTK_ENTRY (dialog->entry_org_orgname));
	org_orgunit = gtk_entry_get_text (GTK_ENTRY (dialog->entry_org_orgunit));
	title = gtk_entry_get_text (GTK_ENTRY (dialog->entry_title));
	role = gtk_entry_get_text (GTK_ENTRY (dialog->entry_role));

	gtk_calendar_get_date (GTK_CALENDAR (dialog->calendar_dob), &dob_year, &dob_month, &dob_day);
	dob = g_strdup_printf ("%d-%d-%d", dob_year, ++dob_month, dob_day);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_description));
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStart, 0);
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStop, -1);
	description = gtk_text_buffer_get_text (buffer,&bufStart,&bufStop,TRUE);

	/* save vcard */
	gj_iq_request_vcard_change (dialog->c,
				    full_name,
				    name_given,
				    name_family,
				    name_nickname, /* nickname */
				    url,
				    tel,
				    email,
				    description,
				    dob,
				    street,
				    extadd,
				    locality,
				    region,
				    pcode,
				    country,
				    org_orgname,
				    org_orgunit,
				    title,
				    role,
				    gj_gtk_mi_set_response,
				    dialog);
  
	/* print to log changes... */
	g_message ("gj_gtk_mi_save: saving VCard:\n\t"
		   "name - full:'%s'\n\t"
		   "name - given:'%s'\n\t"
		   "name - family:'%s'\n\t"
		   "name - nickname:'%s'\n\t"
		   "url:'%s'\n\t"
		   "telephone:'%s'\n\t"
		   "email:'%s'\n\t"
		   "description:'%s'\n\t"
		   "dob:'%s'\n\t"
		   "street:'%s'\n\t"
		   "extadd:'%s'\n\t"
		   "locality:'%s'\n\t"
		   "region:'%s'\n\t"
		   "pcode:'%s'\n\t"
		   "country:'%s'\n\t"
		   "org_name:'%s'\n\t"
		   "org_unit:'%s'\n\t"
		   "title:'%s'\n\t"
		   "role:'%s'",
		   full_name,
		   name_given,
		   name_family,
		   name_nickname,
		   url,
		   tel,
		   email,
		   description,
		   dob,
		   street,
		   extadd,
		   locality,
		   region,
		   pcode,
		   country,
		   org_orgname,
		   org_orgunit,
		   title,
		   role);

	/* clean up */
	g_free (dob);
	g_free (description);

	return TRUE;
}

/*
 * internal callbacks
 */
void gj_gtk_mi_set_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	if (!iq || !iq_related) {
		return;
	}

	node = gj_iq_get_xml (iq_related);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		gchar *detail = NULL;

		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		if ((errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code")) != NULL) {
			detail = g_strdup_printf ("Error:%s",errorCode);
		}

		if ((errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error")) != NULL) {
			if (detail != NULL) {
				detail = g_strdup_printf ("%s - %s",detail,errorMessage);
			}
		}

		/* clean up */
		g_free (detail);
	}
}

static void gj_gtk_mi_vcard_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	GjMyInformationDialog *dialog = NULL;
	GtkTextBuffer *buffer = NULL;
	xmlChar *tmp = NULL;

	if (!iq) {
		return;
	}

	dialog = current_dialog;

	if (!dialog) {
		return; 
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"FN");
	gj_gtk_set_entry_with_args (dialog->entry_full_name, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"GIVEN");
	gj_gtk_set_entry_with_args (dialog->entry_name_given, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"FAMILY");
	gj_gtk_set_entry_with_args (dialog->entry_name_family, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"NICKNAME");
	gj_gtk_set_entry_with_args (dialog->entry_name_nickname, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"URL");
	gj_gtk_set_entry_with_args (dialog->entry_url, "%s",tmp? (gchar*)tmp:"");
  
	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"TEL");
	gj_gtk_set_entry_with_args (dialog->entry_tel, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"EMAIL");
	gj_gtk_set_entry_with_args (dialog->entry_email, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"DESC");
	if (tmp != NULL && xmlStrlen (tmp) > 0) {
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_description));
		gtk_text_buffer_set_text (buffer, (gchar*)tmp,-1);
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"BDAY");
	if (tmp != NULL && xmlStrlen (tmp) > 0) {
		/* NOTE: this must comply with ISO 8601 which shows the date
		   in the format YYYY-MM-DD
		*/
		gchar **vDate = NULL;
		vDate = g_strsplit ((gchar*)tmp,"-",4);
     
		if (vDate != NULL && vDate[0] != NULL && vDate[1] != NULL && vDate[2] != NULL) {
			gtk_calendar_select_day (GTK_CALENDAR (dialog->calendar_dob), atoi (vDate[2]));
			gtk_calendar_select_month (GTK_CALENDAR (dialog->calendar_dob), atoi (vDate[1])-1, atoi (vDate[0]));
				    
			g_strfreev (vDate);
		} else {
			g_warning ("%s: could not get birth date from:'%s' on vcard", __FUNCTION__, tmp);
		}
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"STREET");
	gj_gtk_set_entry_with_args (dialog->entry_street, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"EXTADD");
	gj_gtk_set_entry_with_args (dialog->entry_extadd, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"LOCALITY");
	gj_gtk_set_entry_with_args (dialog->entry_locality, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"REGION");
	gj_gtk_set_entry_with_args (dialog->entry_region, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"PCODE");
	gj_gtk_set_entry_with_args (dialog->entry_pcode, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"COUNTRY");
	gj_gtk_set_entry_with_args (dialog->entry_country, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"ORGNAME");
	gj_gtk_set_entry_with_args (dialog->entry_org_orgname, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"ORGUNIT");
	gj_gtk_set_entry_with_args (dialog->entry_org_orgunit, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"TITLE");
	gj_gtk_set_entry_with_args (dialog->entry_title, "%s",tmp? (gchar*)tmp:"");

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"ROLE");
	gj_gtk_set_entry_with_args (dialog->entry_role, "%s",tmp? (gchar*)tmp:"");
}

/*
 * GUI events
 */
static void on_destroy (GtkWidget *widget, GjMyInformationDialog *dialog) 
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjMyInformationDialog *dialog) 
{
	if (response == GTK_RESPONSE_OK) {
		gj_gtk_mi_set (dialog);
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
