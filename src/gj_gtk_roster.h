/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_gtk_roster_h
#define __gj_gtk_roster_h

#include "gj_presence.h"
#include "gj_message.h"
#include "gj_roster.h"
#include "gj_roster_item.h"

gboolean gj_gtk_roster_load ();

gboolean gj_gtk_roster_show (gboolean show);

void gj_gtk_roster_config_updated ();
const gchar *gj_gtk_roster_config_nick ();

/*
 * roster functions 
 */
gboolean gj_gtk_roster_model_reload ();

gboolean gj_gtk_roster_model_remove (GjRosterItem ri);

/* visual changes */
void gj_gtk_roster_set_status (gchar *message, ...);
void gj_gtk_roster_set_status_event_error (gboolean is_error);

gboolean gj_gtk_roster_handle_next_event_display (GjRosterItem ri);

#endif

#ifdef __cplusplus
}
#endif
