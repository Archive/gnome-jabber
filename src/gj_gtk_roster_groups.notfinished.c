/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_roster.h"

#include "gj_gtk_main.h"
#include "gj_gtk_roster_groups.h"
#include "gj_gtk_support.h"

/* columns */
enum
{
  COL_ROSTER_GROUPS_NAME,
  COL_ROSTER_GROUPS_VISIBLE,
  COL_ROSTER_GROUPS_EDITABLE,
  COL_ROSTER_GROUPS_COUNT
};

struct  {
  GtkWidget *window;

  GtkWidget *treeview;
  GtkTreeStore *treestore;

  GtkWidget *button_add;
  GtkWidget *button_remove;
  
  GtkWidget *button_ok;
  GtkWidget *button_cancel;

} ctlRG;

guint next_group_id = 0;

gboolean gj_gtk_rg_setup ();
void gj_gtk_rg_add_columns (GList *groups);
gboolean gj_gtk_rg_model_populate ();
gboolean gj_gtk_rg_get_selection (gchar **name, gboolean *enabled);
GtkTreeIter *gj_gtk_rg_find_iter_by_name (gchar *name);

/* gui callbacks */
gboolean on_treeview_rg_selected (GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path, gboolean path_currently_selected, gpointer data);
gboolean on_treeview_rg_button_release_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data);
void on_treeview_rg_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, gpointer data);
void on_treeview_rg_cell_edited (GtkCellRendererText *cell, const gchar *path_string, const gchar *new_name, gpointer data);

void on_button_rg_add_clicked (GtkButton *button, gpointer user_data);
void on_button_rg_remove_clicked (GtkButton *button, gpointer user_data);

void on_button_rg_ok_clicked (GtkButton *button, gpointer user_data);
void on_button_rg_cancel_clicked (GtkButton *button, gpointer user_data);

gboolean on_roster_groups_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data);

/*
 * code
 */
gboolean gj_gtk_rg_load (presenceType type, presenceShow show)
{
  GladeXML *xml;

  if ((xml = glade_xml_new (FILE_GLADE,GLADE_ROSTER_GROUPS,NULL)) == NULL) {
      g_error ("gj_gtk_rg_load: couldn't open glade file at root '%s'",GLADE_ROSTER_GROUPS);
      return FALSE;
    }
  
  ctlRG.window = glade_xml_get_widget (xml,GLADE_ROSTER_GROUPS);

  ctlRG.treeview = glade_xml_get_widget (xml,"treeview_rg");

  ctlRG.button_add = glade_xml_get_widget (xml,"button_rg_add");
  ctlRG.button_remove = glade_xml_get_widget (xml,"button_rg_remove");

  ctlRG.button_ok = glade_xml_get_widget (xml,"button_rg_ok");
  ctlRG.button_cancel = glade_xml_get_widget (xml,"button_rg_cancel");
  
  glade_xml_signal_autoconnect (xml);

  gj_gtk_rg_setup ();

  return TRUE;
}

gboolean gj_gtk_rg_unload ()
{
  gtk_widget_destroy (ctlRG.window);

  return TRUE;
}

gboolean gj_gtk_rg_setup ()
{
  gboolean success = FALSE;
  GList *groups = NULL;
  GType types[100];

  if ((groups = rosterGetGroups ()) == NULL) {
      g_warning ("gj_gtk_rg_setup: there are no groups");
      return FALSE;
    }

  /* set up types */
  types[0] = G_TYPE_STRING;
  types[1] = G_TYPE_BOOLEAN;
  types[2] = G_TYPE_BOOLEAN;

  for (i=0;i<g_list_length (groups);i++)
    types[3+i] = G_TYPE_BOOLEAN;

  /* create tree store */
  ctlRG.treestore = gtk_tree_store_newv (COL_ROSTER_GROUPS_COUNT + g_list_length (groups) - 1,
					types);
		
  /* populate model */
  if ((success = gj_gtk_rg_model_populate ()) == FALSE) {
      if (groups != NULL) g_list_free (groups);
  
      g_warning ("gj_gtk_main_roster_setup: failed to setup roster");
      return FALSE;
    }

  /* set selection / model */
  gtk_tree_view_set_model (GTK_TREE_VIEW (ctlRG.treeview),
			  GTK_TREE_MODEL (ctlRG.treestore));
  g_object_unref (G_OBJECT (GTK_TREE_MODEL (ctlRG.treestore)));

  gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (ctlRG.treeview)),
			      GTK_SELECTION_SINGLE);

  gj_gtk_rg_add_columns (GTK_TREE_VIEW (ctlRG.treeview));

  /* clean up */
  if (groups != NULL) g_list_free (groups);

  return TRUE;
}

gboolean gj_gtk_rg_checks ()
{
    return TRUE;
}

void gj_gtk_rg_add_columns (GList *groups)
{
  GtkTreeView *treeview = GTK_TREEVIEW (ctlRG.treeview);
  GtkTreeModel *model = GTK_TREE_MODEL (ctlRG.treestore);
  GtkCellRenderer *renderer = NULL;
  GtkTreeViewColumn *column = NULL; 
  guint col_offset = 0;
  
  g_return_if_fail (model != NULL);
  g_return_if_fail (groups != NULL);

  /* COL_ROSTER_GROUP_ENABLED */
  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect (G_OBJECT (renderer), "toggled", G_CALLBACK (on_treeview_rg_cell_toggled), NULL);

  column = gtk_tree_view_column_new_with_attributes ("Select",
						    renderer,
						    "active", COL_ROSTER_GROUPS_ENABLED,
						    NULL);

  /* set this column to a fixed sizing (of 50 pixels) */
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column), GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
  gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);

  /* COL_ROSTER_GROUP_NAME */
  renderer = gtk_cell_renderer_text_new ();
  col_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
							   -1, "Group",
							   renderer, 
							   "text", COL_ROSTER_GROUPS_NAME,
							   "editable",COL_ROSTER_GROUPS_EDITABLE,
							   NULL);
  
  /* set extra information */
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)COL_ROSTER_GROUPS_NAME);
  g_signal_connect (G_OBJECT (renderer), "edited", G_CALLBACK (on_treeview_rg_cell_edited), gtk_tree_view_get_model (treeview));

  column = gtk_tree_view_get_column (GTK_TREE_VIEW (treeview), col_offset - 1);
  gtk_tree_view_column_set_sort_column_id (column, COL_ROSTER_GROUPS_NAME);
  gtk_tree_view_column_set_resizable (column,FALSE);
  gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

  /* properties */
  gtk_tree_view_set_enable_search (GTK_TREE_VIEW (treeview),TRUE);
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (treeview),COL_ROSTER_GROUPS_NAME); 
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), FALSE);
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (treeview),TRUE);
  gtk_tree_view_set_headers_clickable (GTK_TREE_VIEW (treeview), TRUE);
  gtk_tree_view_columns_autosize (GTK_TREE_VIEW (treeview));
  gtk_tree_view_expand_all (GTK_TREE_VIEW (treeview));
}

gboolean gj_gtk_rg_model_populate ()
{
  GList *groups = NULL;
  gint i = 0;

  GtkTreeIter iter;

  if ((groups = rosterGetGroups ()) == NULL) {
      g_warning ("gj_gtk_rg_model_populate: there are no groups");
      return FALSE;
    }

  g_message ("gj_gtk_rg_model_populate: adding %d groups", g_list_length (groups));  

  for (i=0;i<g_list_length (groups);i++) {
      /* add */
      gtk_tree_store_append (ctlRG.treestore, &iter, NULL);
      gtk_tree_store_set (ctlRG.treestore, &iter,    
			 COL_ROSTER_GROUPS_NAME, (gchar *)g_list_nth_data (groups,i),
			 COL_ROSTER_GROUPS_ENABLED, FALSE,
			 COL_ROSTER_GROUPS_EDITABLE, TRUE,
			 COL_ROSTER_GROUPS_VISIBLE, TRUE,
			 -1);

      g_free (g_list_nth_data (groups,i));
    }

  /* clean up */
  g_list_free (groups);
  
  return TRUE;
}

gboolean gj_gtk_rg_get_selection (gchar **name, gboolean *enabled)
{
    GtkTreeModel *model = GTK_TREE_MODEL (ctlRG.treestore);
    GtkTreeSelection *selection;
    GtkTreeIter iter;

    gboolean is_selection = FALSE;
  
    if (name == NULL || enabled == NULL) {
	g_warning ("gj_gtk_rg_get_selection: name or enabled pointers passed were NULL");
	return FALSE;
    }

    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (ctlRG.treeview));
    is_selection = gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),&model,&iter);
  
    if (is_selection == FALSE)
	return FALSE;

    g_return_val_if_fail (model,FALSE);

    gtk_tree_model_get (model, &iter, 
		       COL_ROSTER_GROUPS_NAME, name,
		       COL_ROSTER_GROUPS_ENABLED, enabled,
		       -1);     

    return TRUE;
}

GtkTreeIter *gj_gtk_rg_find_iter_by_name (gchar *name)
{
    GtkTreeModel *model = GTK_TREE_MODEL (ctlRG.treestore);
    GtkTreeIter iter;
  
    gboolean valid = FALSE;
    gchar *text = NULL;

    if (model == NULL) {
	g_warning ("gj_gtk_rg_find_iter_by_name: model was NULL");
	return NULL;
    }

    if (name == NULL) {
	g_warning ("gj_gtk_rg_find_iter_by_name: name was NULL");
	return NULL;
    }

    /* Get the first iter in the list */
    valid = gtk_tree_model_get_iter_first (model, &iter);

    while (valid)
    {
	gtk_tree_model_get (model, &iter, 
			   COL_ROSTER_GROUPS_NAME, &text,
			   -1);

	g_message ("gj_gtk_rg_find_iter_by_name: comparing '%s' with '%s'",name,text);
	if (text != NULL && g_ascii_strncasecmp (name, text, g_utf8_strlen (name, -1)) == 0) {
	    g_free (text);
	    return gtk_tree_iter_copy (&iter);
	}

	g_free (text);

	/* move to next at parent level */
	valid = gtk_tree_model_iter_next (model, &iter);
    }
  
    return NULL;
}

/*
 * GUI events
 */
gboolean on_treeview_rg_selected (GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path, gboolean path_currently_selected, gpointer data)
{
    /* returns true if it can be selected */
    return TRUE;
}

gboolean on_treeview_rg_button_release_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    GtkTreeModel *model;
    GtkTreeSelection *selection = NULL;
    GtkTreeIter iter;
    gboolean is_selection = FALSE;
    
    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (ctlRG.treeview));
    is_selection = gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),&model,&iter);
    
    gtk_widget_set_sensitive (ctlRG.button_remove, is_selection);	
    
    return FALSE;
}

void on_treeview_rg_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, gpointer data)
{
  GtkTreeModel *model = GTK_TREE_MODEL (ctlRG.treestore);
  GtkTreeIter iter;
  GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
  gboolean enabled;

  /* get toggled iter */
  gtk_tree_model_get_iter (model, &iter, path);
  gtk_tree_model_get (model, &iter, 
		     COL_ROSTER_GROUPS_ENABLED, &enabled, 
		     -1);

  /* do something */
  enabled ^= 1;

  /* set new value */
  gtk_tree_store_set (GTK_TREE_STORE (ctlRG.treestore), &iter, 
		     COL_ROSTER_GROUPS_ENABLED, enabled, 
		     -1);

  /* clean up */
  gtk_tree_path_free (path);

  g_message ("on_treeview_rg_cell_toggled: item is now %s", enabled?"enabled":"disabled");
}

void on_treeview_rg_cell_edited (GtkCellRendererText *cell, const gchar *path_string, const gchar *new_name, gpointer data)
{
  GtkTreeModel *model = (GtkTreeModel *)data;
  GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
  GtkTreeIter iter;
  GtkTreeIter *iter_copy = NULL;

  gint *column;

  column = g_object_get_data (G_OBJECT (cell), "column");
  gtk_tree_model_get_iter (model, &iter, path);
  
  if (new_name == NULL || g_utf8_strlen (new_name, -1) < 1) {
      g_warning ("on_treeview_rg_cell_edited: new name was either NULL or length was < 1");
      gtk_tree_path_free (path);
      return;
  }

  if ((iter_copy = gj_gtk_rg_find_iter_by_name ((gchar *)new_name)) != NULL) {
      g_warning ("on_treeview_rg_cell_edited: new name was the same as another in the list, retry...");
      gtk_tree_path_free (path);
      gtk_tree_iter_free (iter_copy);
      return;
  }

  switch (GPOINTER_TO_INT (column)) {
    case COL_ROSTER_GROUPS_NAME:
      {
	gint i;
	gchar *original_name;
	gchar *name;

        gtk_tree_model_get (model, &iter, 
			   column, &original_name,
			   COL_ROSTER_GROUPS_NAME, &name,
			   -1);

	g_message ("on_treeview_rg_cell_edited: replacing cell at path:'%s' content:'%s' with :'%s'", 
		  path_string, 
		  original_name, 
		  new_name);

	i = gtk_tree_path_get_indices (path)[0];
	gtk_tree_store_set (GTK_TREE_STORE (model), &iter, column, new_name, -1);

	/* clean up */
	if (original_name != NULL) g_free (original_name);
	if (name != NULL) g_free (name);
      }
      break;
    }

  gtk_tree_path_free (path);
}

void on_button_rg_add_clicked (GtkButton *button, gpointer user_data)
{
    GtkTreeIter iter;
    gchar *group = NULL;

    group = g_strdup_printf ("group %d",++next_group_id);

    /* add */
    gtk_tree_store_append (ctlRG.treestore, &iter, NULL);
    gtk_tree_store_set (ctlRG.treestore, &iter,    
		       COL_ROSTER_GROUPS_NAME, group,
		       COL_ROSTER_GROUPS_ENABLED, TRUE,
		       COL_ROSTER_GROUPS_EDITABLE, TRUE,
		       COL_ROSTER_GROUPS_VISIBLE, TRUE,
		       -1);

    if (group != NULL) g_free (group);
}

void on_button_rg_remove_clicked (GtkButton *button, gpointer user_data)
{
    GtkTreeModel *model;
    GtkTreeSelection *selection = NULL;
    GtkTreeIter iter;

    gchar *name;
    gboolean enabled;

    if (gj_gtk_rg_get_selection (&name, &enabled) == FALSE) {
	g_message ("on_button_rg_remove_clicked: no group selected");
	return;
    }

    g_message ("on_button_rg_remove_clicked: removing group:'%s'",name);


    /* check users with this group and modify them... */

    /* remove from list */
    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (ctlRG.treeview));
    gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),&model,&iter);
    gtk_tree_store_remove (ctlRG.treestore, &iter);

    /* update controls */
    gtk_widget_set_sensitive (ctlRG.button_remove, FALSE);	

    /* clean up */
    if (name != NULL) g_free (name);

    return;   
}

void on_button_rg_ok_clicked (GtkButton *button, gpointer user_data)
{
    /* checks */
    gj_gtk_rg_checks ();
    


  /* close up */
  gj_gtk_rg_unload ();
}

void on_button_rg_cancel_clicked (GtkButton *button, gpointer user_data)
{
  gj_gtk_rg_unload ();
}

gboolean on_roster_groups_delete_event (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
  gj_gtk_rg_unload ();

  return TRUE;
}

#ifdef __cplusplus
}
#endif
