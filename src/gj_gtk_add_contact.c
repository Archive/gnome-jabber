/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_jid.h"
#include "gj_agents.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_support.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_connection.h"

#include "gj_gtk_add_contact.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_support.h"
#include "gj_gtk_browse_services.h"


enum {
	COL_AGENT_JID,
	COL_AGENT_NAME,
	COL_AGENT_VISIBLE,
	COL_AGENT_COUNT
};


enum {
	COL_GROUPS_NAME,
	COL_GROUPS_ENABLED,
	COL_GROUPS_VISIBLE,
	COL_GROUPS_EDITABLE,
	COL_GROUPS_COUNT
};


enum {
	ADD_CONTACT_RESPONSE_BACK = 1,
	ADD_CONTACT_RESPONSE_FORWARD,
	ADD_CONTACT_RESPONSE_FINISH
};


typedef struct  {
	GtkWidget *window;
  
	GtkWidget *notebook;

	GtkWidget *label_title;
	GtkWidget *label_text;
	GtkWidget *label_help;

	GtkWidget *table_add_by_jid;
	GtkWidget *table_add_by_agent;

	GtkWidget *radiobutton_jid;
	GtkWidget *radiobutton_agent;

	GtkWidget *entry_jid;

	GtkWidget *combo_agent;
	GtkWidget *combo_entry_agent;
	GtkWidget *treeview_agent;
	GtkWidget *treestore_agent;
	GtkWidget *entry_screen_name;
	GtkWidget *button_register_new_agent;

	GtkWidget *vbox_name;
	GtkWidget *vbox_contact;
	GtkWidget *label_waiting_for_details;
	GtkWidget *label_name_fn;
	GtkWidget *label_name_given;
	GtkWidget *label_name_family;
	GtkWidget *label_name_nickname;
	GtkWidget *label_contact_email;
	GtkWidget *label_contact_url;
	GtkWidget *label_contact_tel;

	GtkWidget *entry_name;
	GtkWidget *entry_groups_add;
	GtkWidget *button_groups_add;
	GtkWidget *treeview_groups;

	GtkWidget *textview_request;

	GtkWidget *progressbar_adding;

	GtkWidget *button_cancel;
	GtkWidget *button_forward;
	GtkWidget *button_back;
	GtkWidget *button_finish;

	GjConnection c;
	GjRoster roster;

	gchar *jid;
	gchar *name;
	gchar *request;
 
	gint progress_timer_contact_details;
	gint progress_timer_adding;

} GjAddContactDialog;


/* model functions */
static gboolean gj_gtk_ac_model_agent_setup (GjAddContactDialog *dialog);
static gboolean gj_gtk_ac_model_agent_add_columns (GtkTreeView *view);
static gboolean gj_gtk_ac_model_agent_populate (GjAddContactDialog *dialog);
static gboolean gj_gtk_ac_model_agent_get_selection (GjAddContactDialog *dialog, gchar **jid, gchar **name);

static gboolean gj_gtk_ac_model_groups_setup (GjAddContactDialog *dialog);
static gboolean gj_gtk_ac_model_groups_add_columns (GjAddContactDialog *dialog);
static gboolean gj_gtk_ac_model_groups_populate (GjAddContactDialog *dialog);

static GList *gj_gtk_ac_model_get_groups (GjAddContactDialog *dialog);

/* checks */
static void gj_gtk_ac_page_changed (GjAddContactDialog *dialog, gint page);

static gboolean gj_gtk_ac_option_toggle (GjAddContactDialog *dialog);
static gboolean gj_gtk_ac_button_update (GjAddContactDialog *dialog);

/* page functions */
static gboolean gj_gtk_ac_page_check (GjAddContactDialog *dialog, gint page);
static gboolean gj_gtk_ac_page_action (GjAddContactDialog *dialog, gint page);

static gboolean gj_gtk_ac_page_forward (GjAddContactDialog *dialog);
static gboolean gj_gtk_ac_page_back (GjAddContactDialog *dialog);

/* 
 * internal callbacks 
 */
static gint gj_gtk_ac_progress_timer_adding (GjAddContactDialog *dialog);
static void gj_gtk_ac_vcard_response (GjInfoquery iq, GjInfoquery iq_related, GjAddContactDialog *dialog);
static void gj_gtk_ac_contact_add_response (GjInfoquery iq, GjInfoquery iq_related, GjAddContactDialog *dialog);

/* 
 * gui callbacks 
 */
static void on_notebook_change_current_page (GtkNotebook *notebook, gint arg1, GjAddContactDialog *dialog);

static void on_radiobutton_jid_toggled (GtkToggleButton *togglebutton, GjAddContactDialog *dialog);
static void on_radiobutton_agent_toggled (GtkToggleButton *togglebutton, GjAddContactDialog *dialog);

static void on_treeview_groups_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjAddContactDialog *dialog);

static void on_button_register_new_agent_clicked (GtkButton *button, GjAddContactDialog *dialog);
static void on_button_groups_add_clicked (GtkButton *button, GjAddContactDialog *dialog);
static void on_entry_groups_add_changed (GtkEditable *editable, GjAddContactDialog *dialog);

static void on_button_register_new_agent_clicked (GtkButton *button, GjAddContactDialog *dialog);

static void on_entry_jid_changed (GtkEditable *editable, GjAddContactDialog *dialog);
static void on_entry_screen_name_changed (GtkEditable *editable, GjAddContactDialog *dialog);
static void on_entry_name_changed (GtkEditable *editable, GjAddContactDialog *dialog);

static void on_treeview_agents_selection_changed (GtkTreeSelection *treeselection, GjAddContactDialog *dialog);
static void on_treeview_groups_selection_changed (GtkTreeSelection *treeselection, GjAddContactDialog *dialog);

static void on_destroy (GtkWidget *widget, GjAddContactDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjAddContactDialog *dialog);


static GjAddContactDialog *current_dialog = NULL;


gboolean gj_gtk_ac_load (GjConnection c,
			 GjRoster r, 
			 GjRosterItem ri)
{
	GladeXML *xml = NULL;
	GjAddContactDialog *dialog = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjAddContactDialog, 1);

	dialog->c = gj_connection_ref (c);
	dialog->roster = r;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "add_contact",
				     NULL,
				     "add_contact", &dialog->window,
				     "notebook", &dialog->notebook,
				     "table_add_by_jid", &dialog->table_add_by_jid,
				     "table_add_by_agent", &dialog->table_add_by_agent,
				     "radiobutton_jid", &dialog->radiobutton_jid,
				     "radiobutton_agent", &dialog->radiobutton_agent,
				     "entry_jid", &dialog->entry_jid,
				     "treeview_agent", &dialog->treeview_agent,
				     "entry_screen_name", &dialog->entry_screen_name,
				     "button_register_new_agent", &dialog->button_register_new_agent,
				     "entry_name", &dialog->entry_name,
				     "treeview_groups", &dialog->treeview_groups,
				     "entry_groups_add", &dialog->entry_groups_add,
				     "button_groups_add", &dialog->button_groups_add,
				     "vbox_name", &dialog->vbox_name,
				     "vbox_contact", &dialog->vbox_contact,
				     "label_waiting_for_details", &dialog->label_waiting_for_details,
				     "label_name_fn", &dialog->label_name_fn,
				     "label_name_given", &dialog->label_name_given,
				     "label_name_family", &dialog->label_name_family,
				     "label_name_nickname", &dialog->label_name_nickname,
				     "label_contact_email", &dialog->label_contact_email,
				     "label_contact_url", &dialog->label_contact_url,
				     "label_contact_tel", &dialog->label_contact_tel,
				     "textview_request", &dialog->textview_request,
				     "progressbar_adding", &dialog->progressbar_adding,
				     "button_forward", &dialog->button_forward,
				     "button_back", &dialog->button_back,
				     "button_finish", &dialog->button_finish,
				     "button_cancel", &dialog->button_cancel,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "add_contact", "response", on_response,
			      "add_contact", "destroy", on_destroy,
			      "notebook", "change_current_page", on_notebook_change_current_page,
			      "radiobutton_jid", "toggled", on_radiobutton_jid_toggled,
			      "radiobutton_agent", "toggled", on_radiobutton_agent_toggled,
			      "button_register_new_agent", "clicked", on_button_register_new_agent_clicked,
			      "entry_groups_add", "changed", on_entry_groups_add_changed,
			      "button_groups_add", "clicked", on_button_groups_add_clicked,
			      "entry_jid", "changed", on_entry_jid_changed,
			      "entry_screen_name", "changed", on_entry_screen_name_changed,
			      "entry_name", "changed", on_entry_name_changed,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);
  

	/* setup controls */
	gj_gtk_ac_model_agent_setup (dialog);
	gj_gtk_ac_model_groups_setup (dialog);

	if (gj_gtk_ac_model_agent_populate (dialog) == FALSE) {
		g_warning ("%s: failed to populate agents", __FUNCTION__);
	}

	if (gj_gtk_ac_model_groups_populate (dialog) == FALSE) {
		g_warning ("%s: failed to populate groups", __FUNCTION__); 
	}

	if (ri != NULL) {
		GjJID j = NULL;
		j = gj_roster_item_get_jid (ri);
   
		g_message ("%s: using jid:'%s', skipping stage 1 & 2...", __FUNCTION__, gj_jid_get_full (j));
    
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_jid), gj_jid_get_full (j));
		gj_gtk_ac_page_forward (dialog);
		gj_gtk_ac_page_forward (dialog);
		gj_gtk_ac_page_forward (dialog);
	}

	return TRUE;
}

static gboolean gj_gtk_ac_model_agent_setup (GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection * selection = NULL;

	if (!dialog) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview_agent);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	/* store */
	store =  gtk_list_store_new (COL_AGENT_COUNT,
				     G_TYPE_STRING,    /* jid */
				     G_TYPE_STRING,    /* name */
				     G_TYPE_BOOLEAN);  /* visible */
  
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	g_signal_connect (G_OBJECT (selection), "changed", 
			  G_CALLBACK (on_treeview_agents_selection_changed), NULL);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	/* properties */
	gtk_tree_view_set_enable_search (view,TRUE);
	gtk_tree_view_set_search_column (view,COL_AGENT_NAME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, TRUE);
	gtk_tree_view_expand_all (view);

	/* columns */
	gj_gtk_ac_model_agent_add_columns (view);

	/* clean up */  
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

static gboolean gj_gtk_ac_model_agent_add_columns (GtkTreeView *view)
{
	GtkCellRenderer *renderer = NULL;
	GtkTreeViewColumn *column = NULL; 
	guint col_offset = 0;

	if (!view) {
		return FALSE;
	}

	/* COL_AGENT_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Name"),
								  renderer, 
								  "text", COL_AGENT_NAME,
								  NULL);
  
	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_AGENT_NAME));

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_AGENT_NAME);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	return TRUE;
}

static gboolean gj_gtk_ac_model_agent_get_selection (GjAddContactDialog *dialog, gchar **jid, gchar **name)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;

	if (!dialog) {
		return FALSE;
	}
	
	if (jid == NULL || name == NULL) {
		g_warning ("%s: jid, name or service pointers passed were NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview_agent);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return FALSE;
	}
  
	gtk_tree_model_get (model, &iter, 
			    COL_AGENT_JID, jid,
			    COL_AGENT_NAME, name,
			    -1);     

	return TRUE;
}

static gboolean gj_gtk_ac_model_groups_setup (GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;

	if (!dialog) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview_groups);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	/* create tree store */
	store = gtk_list_store_new (COL_GROUPS_COUNT,
				    G_TYPE_STRING,   /* name */
				    G_TYPE_BOOLEAN,  /* enabled */
				    G_TYPE_BOOLEAN,  /* visible */
				    G_TYPE_BOOLEAN); /* editable */
  
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	g_signal_connect (G_OBJECT (selection), "changed", 
			  G_CALLBACK (on_treeview_groups_selection_changed), NULL);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	/* properties */
	gtk_tree_view_set_enable_search (view,FALSE);
	gtk_tree_view_set_search_column (view,COL_GROUPS_NAME); 
	gtk_tree_view_set_rules_hint (view,FALSE);
	gtk_tree_view_set_headers_visible (view,FALSE);
	gtk_tree_view_set_headers_clickable (view,FALSE);
	gtk_tree_view_columns_autosize (view);
	gtk_tree_view_expand_all (view);

	/* columns */
	gj_gtk_ac_model_groups_add_columns (dialog);

	/* clean up */  
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

static gboolean gj_gtk_ac_model_groups_add_columns (GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeViewColumn *column = NULL; 
	GtkCellRenderer *renderer = NULL;
	guint col_offset = 0;
  
	if (!dialog) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview_groups);

	/* COL_ROSTER_GROUP_ENABLED */
	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled", 
			  G_CALLBACK (on_treeview_groups_cell_toggled), dialog);

	column = gtk_tree_view_column_new_with_attributes (_("Select"),
							   renderer,
							   "active", COL_GROUPS_ENABLED,
							   NULL);

	/* set this column to a fixed sizing (of 50 pixels) */
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column), GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 50);
	gtk_tree_view_append_column (view, column);

	/* COL_ROSTER_GROUP_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Group"),
								  renderer, 
								  "text", COL_GROUPS_NAME,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_GROUPS_NAME));

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_GROUPS_NAME);
	gtk_tree_view_column_set_resizable (column,FALSE);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	return TRUE;
}

static gboolean gj_gtk_ac_model_agent_populate (GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gint i = 0;
	const GList *agents = NULL;

	if (!dialog) {
		return FALSE;
	}

	if ((agents = gj_roster_get_agents (dialog->roster)) == NULL) {
		g_warning ("%s: agent list was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview_agent);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* clear list if already pressed */
	gtk_list_store_clear (store);

	for (i=0;i<g_list_length ((GList*)agents);i++) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;

		ri = (GjRosterItem) g_list_nth_data ((GList*)agents, i);

		if (ri == NULL) {
			continue;
		}

		j = gj_roster_item_get_jid (ri);

		/* add */
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,    
				    COL_AGENT_JID, gj_jid_get_full (j),
				    COL_AGENT_NAME, gj_roster_item_get_name (ri),
				    COL_AGENT_VISIBLE, TRUE,
				    -1);

		g_message ("%s: #%d, jid:'%s'", __FUNCTION__, i, gj_jid_get_full (j));
	}
  
	return TRUE;
}

static gboolean gj_gtk_ac_model_groups_populate (GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	const GList *groups = NULL;
	GList *l = NULL;

	if (!dialog) {
		return FALSE;
	}

	if ((groups = gj_roster_get_groups (dialog->roster)) == NULL) {
		g_message ("%s: there are no groups", __FUNCTION__);
		return TRUE;
	}

	view = GTK_TREE_VIEW (dialog->treeview_groups);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	g_message ("%s: adding %d groups", __FUNCTION__, g_list_length ((GList*)groups));  

	for (l = (GList*)groups; l; l = l->next) {
		gchar *str = l->data;

		g_message ("%s: #%d, group:'%s'", __FUNCTION__, g_list_index (l, str), str);

		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,    
				    COL_GROUPS_NAME, str,
				    COL_GROUPS_ENABLED, FALSE,
				    COL_GROUPS_EDITABLE, TRUE,
				    COL_GROUPS_VISIBLE, TRUE,
				    -1);
	}

	return TRUE;
}

static GList *gj_gtk_ac_model_get_groups (GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;

	GList *groups = NULL;
	gchar *text = NULL;
	gboolean enabled = FALSE;

	if (!dialog) {
		return NULL;
	}

	view = GTK_TREE_VIEW (dialog->treeview_groups);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	for (valid = gtk_tree_model_get_iter_first (model, &iter);
	     valid == TRUE;
	     valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_tree_model_get (model, &iter, 
				    COL_GROUPS_NAME, &text,
				    COL_GROUPS_ENABLED, &enabled,
				    -1);
		
		g_message ("%s: group:'%s', enabled:%s - %s", __FUNCTION__, text, enabled?"yes":"no", enabled?"including!":"");
		
		if (text != NULL && g_utf8_strlen (text, -1) > 0 && enabled == TRUE) {
			groups = g_list_append (groups, text);
		} else {
			g_free (text); 
		}
	}
  
	return groups;
}

static gboolean gj_gtk_ac_page_check (GjAddContactDialog *dialog, gint page)
{
	/* NOTE: this checks to see if we can progress a step, 
	   if not, we return FALSE 

	   This is called when details on a page change OR 
	   when when NEXT is clicked before the actions are 
	   called. */

	if (!dialog) {
		return FALSE;
	}

	switch (page) {
	case 0:
		break;

	case 1: {
		G_CONST_RETURN gchar *jid = NULL;
		
		jid = gtk_entry_get_text (GTK_ENTRY (dialog->entry_jid));

		if (gj_jid_string_is_valid_jid_contact (jid) == FALSE) {
			return FALSE;
		}
		
 		g_free (dialog->jid);
		dialog->jid = g_strdup (jid);

		break;
	}

	case 2: {
		G_CONST_RETURN gchar *screen_name = NULL;
		gchar *screen_name_filtered = NULL;
		gchar *agent_jid = NULL;
		gchar *new_contact_jid = NULL;
		gchar *name = NULL;
		
		screen_name = gtk_entry_get_text (GTK_ENTRY (dialog->entry_screen_name));

		if (screen_name == NULL || g_utf8_strlen (screen_name, -1) < 1) {
			g_message ("%s: screen name was NULL or length was < 1", __FUNCTION__);
			return FALSE;
		}	    
		
		if (gj_gtk_ac_model_agent_get_selection (dialog, &agent_jid, &name) == FALSE) {
			g_message ("%s: failed to get agent selection", __FUNCTION__);
			return FALSE;
		}
		
		/* NEED TO DO THIS PROPERLY...
		   SEE PSI - they request the conversion! */
		screen_name_filtered = gj_find_in_str_and_replace ("@", screen_name, "%");
		new_contact_jid = g_strdup_printf ("%s@%s", screen_name_filtered, agent_jid);

		g_free (dialog->jid);
		dialog->jid = new_contact_jid;
		
		g_free (screen_name_filtered);

		break;
	}

	case 3: {
		break;
	}

	case 4: {
		G_CONST_RETURN gchar *name = NULL;
		name = gtk_entry_get_text (GTK_ENTRY (dialog->entry_name));
		
		if (name == NULL || g_utf8_strlen (name, -1) < 1) {
			g_message ("%s: name was NULL or length was < 1", __FUNCTION__);
			return FALSE;
		}	
		
		g_free (dialog->name);
		dialog->name = g_strdup (name);

		break;
	}

	case 5: 
	case 6:
	case 7: {
		break;
	}


	default: {
		g_warning ("%s: page:%d is not valid, shouldn't be here, default reached.", __FUNCTION__, page);
		return FALSE;
	}
	}

	return TRUE;
}

static gboolean gj_gtk_ac_page_action (GjAddContactDialog *dialog, gint page)
{
	/* NOTE: this is called WHEN the user clicks NEXT, 
	   e.g. if on step 1, step 2 action is run on clicking NEXT */

	if (!dialog) {
		return FALSE;
	}

	switch (page) {
	case 0:
	case 1:
	case 2:
		break;

	case 3: {
		/* set up widgets */
		gtk_widget_hide (dialog->vbox_name);
		gtk_widget_hide (dialog->vbox_contact);
      
		gtk_label_set_text (GTK_LABEL (dialog->label_waiting_for_details), 
				    _("Waiting for details associated with this contact.\n" 
				      "Please wait..."));

		if (dialog->name) {
			g_free (dialog->name);
			dialog->name = NULL;
		}

		/* request vcard */
		gj_iq_request_vcard (dialog->c,
				     dialog->jid, 
				     (GjInfoqueryCallback)gj_gtk_ac_vcard_response, 
				     dialog);
		break;
	}

	case 4:	{
		GjJID j = NULL;
		
		/* copy jid */
		if (dialog->jid != NULL) {
			j = gj_jid_new (dialog->jid);
		}
		
		if (j == NULL || gj_jid_get_type (j) != GjJIDTypeContact) {
			g_warning ("%s: JID was NULL or not a real JID", __FUNCTION__);
			return FALSE;
		}
		
		if (dialog->name == NULL) {
			gchar *name = NULL;
			name = gj_jid_get_part_name_new (j);

			g_free (dialog->name);
			dialog->name = g_strdup (name);

			g_message ("%s: using jid for name:'%s'", __FUNCTION__, dialog->name);
			
			g_free (name);
		}
		
		if (dialog->name != NULL) {
			gtk_entry_set_text (GTK_ENTRY (dialog->entry_name), dialog->name);
		}
		
		if (j) {
			gj_jid_unref (j); 
		}

		break;
	}

	case 5: {
		break;
	}

	case 6:	{
		GList *groups = NULL;
		GjJID j = NULL;

		gchar *request = NULL;
		
		GtkTextBuffer *buffer = NULL;
		GtkTextIter iter_start;
		GtkTextIter iter_end;
		
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_request));	
		gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
		request = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end, TRUE);
		
		if (request == NULL || g_utf8_strlen (request, -1) < 1) {
			g_message ("%s: request was NULL or length was < 1", __FUNCTION__);
			return FALSE;
		}	    
		
		g_free (dialog->request);
		dialog->request = request;
		
		groups = gj_gtk_ac_model_get_groups (dialog);
		
		/* add timer for progress bar */
		dialog->progress_timer_adding = gtk_timeout_add (75, 
								 (GSourceFunc)gj_gtk_ac_progress_timer_adding, 
								 dialog);
		
		/* add contact to list... */
		gj_iq_request_roster_add_contact (dialog->c,
						  dialog->jid, 
						  dialog->name, 
						  groups, 
						  (GjInfoqueryCallback)gj_gtk_ac_contact_add_response,
						  dialog);
		
		j = gj_jid_new (dialog->jid); 
		
		/* request presence */
		gj_presence_send_to (dialog->c,
				     j, 
				     FALSE, 
				     GjPresenceTypeSubscribe, 
				     GjPresenceShowEnd, 
				     dialog->request, 
				     0); 
		
		/* clean up - groups ? */
		if (j != NULL) {
			gj_jid_unref (j); 
		}
	
		break;
	}

	case 7: { 
		break;
	}
	}

	return TRUE;
}

static gboolean gj_gtk_ac_page_forward (GjAddContactDialog *dialog) 
{
	gint index = 0;

	if (!dialog) {
		return FALSE;
	}

	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (dialog->notebook))) < 0) {
		return FALSE;
	}

	/* what do we want to do here? check for the right information at each stage?? */
	gtk_notebook_next_page (GTK_NOTEBOOK (dialog->notebook));
	
	/* page should now be updated */
	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (dialog->notebook))) < 0) {
		return FALSE;
	}
	
	/* do action for this page... */
	gj_gtk_ac_page_action (dialog, index);
	gj_gtk_ac_button_update (dialog);
	
	gtk_widget_set_sensitive (dialog->button_forward, gj_gtk_ac_page_check (dialog, index));

	return TRUE;
}

static gboolean gj_gtk_ac_page_back (GjAddContactDialog *dialog)
{
	gint index = 0;

	if (!dialog) {
		return FALSE;
	}

	gtk_notebook_prev_page (GTK_NOTEBOOK (dialog->notebook));

	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (dialog->notebook))) < 0) {
		return FALSE;
	}

	/* update */
	gtk_widget_set_sensitive (dialog->button_forward, gj_gtk_ac_page_check (dialog, index));
	gj_gtk_ac_button_update (dialog);
  
	return TRUE;
}

/* checks */
static void gj_gtk_ac_page_changed (GjAddContactDialog *dialog, gint page)
{
	if (!dialog) {
		return;
	}

	gtk_widget_set_sensitive (dialog->button_forward, gj_gtk_ac_page_check (dialog, page));
}

static gboolean gj_gtk_ac_option_toggle (GjAddContactDialog *dialog)
{
	if (!dialog) {
		return FALSE;
	}

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->radiobutton_jid)) == TRUE) {
		gtk_widget_show (dialog->table_add_by_jid);
		gtk_widget_hide (dialog->table_add_by_agent);
	} else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->radiobutton_agent)) == TRUE) {
		gtk_widget_hide (dialog->table_add_by_jid);
		gtk_widget_show (dialog->table_add_by_agent);
	}
	
	return TRUE;
}

static gboolean gj_gtk_ac_button_update (GjAddContactDialog *dialog)
{
	gint index = 0;
	gint count = 0;

	if (!dialog) {
		return FALSE;
	}

	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (dialog->notebook))) < 0) {
		g_warning ("%s: error, -1 returned, meaning NO notebook pages.  "
			   "This should NEVER happen", 
			   __FUNCTION__);
		return FALSE;
	}

	/*
	 * THIS function is in the documentation but not in my /usr/include/gtk header file.
	 */

	/*  count = gtk_notebook_get_n_pages (GTK_NOTEBOOK (dialog->notebook));*/   
	count = 7;
	if (count < 0 || count <= index) {
		g_warning ("%s: count:%d is < index:%d", __FUNCTION__, count, index);
		return FALSE;
	} 

	if (index < 1) {
		gtk_widget_set_sensitive (dialog->button_back, FALSE);
      
		if (count > 1) {
			gtk_widget_set_sensitive (dialog->button_forward, TRUE);
		}
	} else if (index == (count - 1)) {
		gtk_widget_set_sensitive (dialog->button_forward, FALSE);
		
		if (count > 1) {
			gtk_widget_set_sensitive (dialog->button_back, TRUE);
		}
		
		/* show finish button if at the end */
		if (index == (count -1)) {
			gtk_widget_set_sensitive (dialog->button_back, FALSE);
			gtk_widget_hide (dialog->button_forward);
			gtk_widget_show (dialog->button_finish);
		}
	} else {
		gtk_widget_set_sensitive (dialog->button_back, TRUE);
		gtk_widget_set_sensitive (dialog->button_forward, TRUE);
		
		/* hide finish button if near the end going back */
		if (index == (count -2)) {
			gtk_widget_hide (dialog->button_finish);
			gtk_widget_show (dialog->button_forward);
		}
	}

	return TRUE;
}


/* 
 * internal callbacks 
 */
static gint gj_gtk_ac_progress_timer_adding (GjAddContactDialog *dialog)
{
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (dialog->progressbar_adding));
	return TRUE;
} 

static void gj_gtk_ac_vcard_response (GjInfoquery iq, GjInfoquery iq_related, GjAddContactDialog *dialog)
{
	xmlChar *tmp = NULL;
  
	gchar *fn = NULL;
	gchar *given = NULL;
	gchar *family = NULL;
	gchar *nickname = NULL;
	gchar *url = NULL;
	gchar *tel = NULL;
	gchar *email = NULL;

	if (!iq || !iq_related) {
		return;
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"nickname");
	gj_gtk_set_label_with_args (dialog->label_name_nickname, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		nickname = g_strdup ((gchar*)tmp); 
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"fn");
	gj_gtk_set_label_with_args (dialog->label_name_fn, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		fn = g_strdup ((gchar*)tmp); 
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"given");
	gj_gtk_set_label_with_args (dialog->label_name_given, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		given = g_strdup ((gchar*)tmp); 
	}
  
	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"family");
	gj_gtk_set_label_with_args (dialog->label_name_family, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		family = g_strdup ((gchar*)tmp);
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"url");
	gj_gtk_set_label_with_args (dialog->label_contact_url, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		url = g_strdup ((gchar*)tmp);
	}
  
	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"tel");
	gj_gtk_set_label_with_args (dialog->label_contact_tel, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		tel = g_strdup ((gchar*)tmp);
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"email");
	gj_gtk_set_label_with_args (dialog->label_contact_email, "%s",tmp? (gchar*)tmp:"-");
	if (tmp != NULL) {
		email = g_strdup ((gchar*)tmp);
	}

	/* deal with name - do we use it? */
	if (nickname != NULL) {
		g_free (dialog->name);
		dialog->name = g_strdup (nickname);
	} else if (fn != NULL) {
		g_free (dialog->name);
		dialog->name = g_strdup (fn);
	} else if (given != NULL) {
		gchar *new_name = NULL;
		
		new_name = g_strdup_printf ("%s%s%s", given, family?" ":"", family?family:"");

		g_free (dialog->name);
		dialog->name = new_name;
	}
	
	if ((fn == NULL || g_utf8_strlen (fn,-1) < 1) && 
	    (given == NULL || g_utf8_strlen (given,-1) < 1) &&
	    (family == NULL || g_utf8_strlen (family,-1) < 1) &&
	    (nickname == NULL || g_utf8_strlen (nickname,-1) < 1) &&
	    (url == NULL || g_utf8_strlen (url,-1) < 1) &&
	    (tel == NULL || g_utf8_strlen (tel,-1) < 1) &&
	    (email == NULL || g_utf8_strlen (email,-1) < 1)) {
		gtk_label_set_text (GTK_LABEL (dialog->label_waiting_for_details), 
				    _("No personal information available."));
	} else {
		gtk_label_set_text (GTK_LABEL (dialog->label_waiting_for_details), 
				    _("Use these details to confirm that you have the right person."));
		
		/* set up widgets */
		gtk_widget_show (GTK_WIDGET (dialog->vbox_name));
		gtk_widget_show (GTK_WIDGET (dialog->vbox_contact));
	}

	/* clean up */
	g_free (fn);
	g_free (given);
	g_free (family);
	g_free (nickname);
	g_free (url);
	g_free (tel);
	g_free (email);
}

static void gj_gtk_ac_contact_add_response (GjInfoquery iq, GjInfoquery iq_related, GjAddContactDialog *dialog)
{
	if (!iq || !iq_related) {
		return;
	}

	/* stop progress timer */
	if (dialog->progress_timer_adding != -1) {
		gtk_timeout_remove (dialog->progress_timer_adding);
		dialog->progress_timer_adding = -1;
	}

	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (dialog->progressbar_adding), (gdouble) 1);

	/* move on to next part... */
	gj_gtk_ac_page_forward (dialog);
}

/* gui callbacks */
static void on_notebook_change_current_page (GtkNotebook *notebook, gint arg1, GjAddContactDialog *dialog)
{

}

static void on_radiobutton_jid_toggled (GtkToggleButton *togglebutton, GjAddContactDialog *dialog)
{
	gj_gtk_ac_option_toggle (dialog);
	gj_gtk_ac_page_changed (dialog, 0);
}

static void on_radiobutton_agent_toggled (GtkToggleButton *togglebutton, GjAddContactDialog *dialog)
{
	gj_gtk_ac_option_toggle (dialog);
	gj_gtk_ac_page_changed (dialog, 0);
}

static void on_treeview_groups_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	gboolean enabled = FALSE;

	if (!dialog) {
		return;
	}
	
	view = GTK_TREE_VIEW (dialog->treeview_groups);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	path = gtk_tree_path_new_from_string (path_string);

	/* get toggled iter */
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, 
			    COL_GROUPS_ENABLED, &enabled, 
			    -1);

	/* do something */
	enabled ^= 1;

	/* set new value */
	gtk_list_store_set (store, &iter, 
			    COL_GROUPS_ENABLED, enabled, 
			    -1);

	/* clean up */
	gtk_tree_path_free (path);
}

static void on_button_groups_add_clicked (GtkButton *button, GjAddContactDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;
	G_CONST_RETURN gchar *group = NULL;

	view = GTK_TREE_VIEW (dialog->treeview_groups);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	group = gtk_entry_get_text (GTK_ENTRY (dialog->entry_groups_add));

	/* add */
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,    
			    COL_GROUPS_NAME, group,
			    COL_GROUPS_ENABLED, TRUE,
			    COL_GROUPS_EDITABLE, TRUE,
			    COL_GROUPS_VISIBLE, TRUE,
			    -1);

	gtk_entry_set_text (GTK_ENTRY (dialog->entry_groups_add), "");
}

static void on_entry_groups_add_changed (GtkEditable *editable, GjAddContactDialog *dialog)
{
	gchar *group = NULL;

	group = gtk_editable_get_chars (editable, 0, -1);

	if (group != NULL && g_utf8_strlen (group, -1) > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_groups_add), TRUE);
		return;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_groups_add), FALSE);
}

static void on_button_register_new_agent_clicked (GtkButton *button, GjAddContactDialog *dialog)
{
	gj_gtk_bs_load ();
}

static void on_entry_jid_changed (GtkEditable *editable, GjAddContactDialog *dialog)
{
	gj_gtk_ac_page_changed (dialog, 1);
}

static void on_entry_screen_name_changed (GtkEditable *editable, GjAddContactDialog *dialog)
{
	gj_gtk_ac_page_changed (dialog, 2);
}

static void on_treeview_agents_selection_changed (GtkTreeSelection *treeselection, GjAddContactDialog *dialog)
{
	gj_gtk_ac_page_changed (dialog, 2);
}

static void on_entry_name_changed (GtkEditable *editable, GjAddContactDialog *dialog)
{
	gj_gtk_ac_page_changed (dialog, 4);
}

static void on_treeview_groups_selection_changed (GtkTreeSelection *treeselection, GjAddContactDialog *dialog)
{
	gj_gtk_ac_page_changed (dialog, 4);
}

static void on_destroy (GtkWidget *widget, GjAddContactDialog *dialog)
{
	current_dialog = NULL;

	if (!dialog) {
		return;
	}

	gj_connection_unref (dialog->c);

	g_free (dialog->jid);
	g_free (dialog->name);
	g_free (dialog->request);

	if (dialog->progress_timer_contact_details > 0) {
		g_source_remove (dialog->progress_timer_contact_details);
		dialog->progress_timer_contact_details = 0;
	}

	if (dialog->progress_timer_adding > 0) {
		g_source_remove (dialog->progress_timer_adding);
		dialog->progress_timer_adding = 0;
	}
  
	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjAddContactDialog *dialog)
{
	if (response == ADD_CONTACT_RESPONSE_BACK) {
		gj_gtk_ac_page_back (dialog);
	} else if (response == ADD_CONTACT_RESPONSE_FORWARD) {
		gj_gtk_ac_page_forward (dialog);
	} else {
		gtk_widget_destroy (dialog->window);
	}
}

#ifdef __cplusplus
}
#endif
