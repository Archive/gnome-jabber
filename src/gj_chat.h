/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_chat_h
#define __gj_chat_h

#include "gj_gtk_stock.h"
#include "gj_gtk_support.h"


/***********************************************************************
 * EMOTE ICONS        
 ***********************************************************************/

#define GJ_CHAT_EMOTE_BIG_GRIN ":D"
#define GJ_CHAT_EMOTE_CONFUSED ":s"
#define GJ_CHAT_EMOTE_COOL " (h)"
#define GJ_CHAT_EMOTE_EEK ":O"
#define GJ_CHAT_EMOTE_FROWN ": ("
#define GJ_CHAT_EMOTE_MAD ":@"
#define GJ_CHAT_EMOTE_RED_FACE ":$"
#define GJ_CHAT_EMOTE_ROLL_EYES "8)"
#define GJ_CHAT_EMOTE_SMILE ":)"
#define GJ_CHAT_EMOTE_TONGUE ":P"
#define GJ_CHAT_EMOTE_WINK ";)"


typedef enum {
  
	GjChatEmoteBigGrin = 1,
	GjChatEmoteConfused,
	GjChatEmoteCool,
	GjChatEmoteEek,
	GjChatEmoteFrown,
	GjChatEmoteMad,
	GjChatEmoteRedFace,
	GjChatEmoteRollEyes,
	GjChatEmoteSmile,
	GjChatEmoteTongue,
	GjChatEmoteWink,
	GjChatEmoteNull
    
} GjChatEmoteIcon;


static const char *gj_chat_emote_icon_get_as_text (guint value)
{
	switch (value) {
	case GjChatEmoteBigGrin: return GJ_CHAT_EMOTE_BIG_GRIN;
	case GjChatEmoteConfused: return GJ_CHAT_EMOTE_CONFUSED;
	case GjChatEmoteCool: return GJ_CHAT_EMOTE_COOL;
	case GjChatEmoteEek: return GJ_CHAT_EMOTE_EEK;
	case GjChatEmoteFrown: return GJ_CHAT_EMOTE_FROWN;
	case GjChatEmoteMad: return GJ_CHAT_EMOTE_MAD;
	case GjChatEmoteRedFace: return GJ_CHAT_EMOTE_RED_FACE;
	case GjChatEmoteRollEyes: return GJ_CHAT_EMOTE_ROLL_EYES;
	case GjChatEmoteSmile: return GJ_CHAT_EMOTE_SMILE;
	case GjChatEmoteTongue: return GJ_CHAT_EMOTE_TONGUE;
	case GjChatEmoteWink: return GJ_CHAT_EMOTE_WINK;
	}

	return "";
}

static GdkPixbuf *gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteIcon icon)
{
	GdkPixbuf *pb = NULL;
	GdkPixbuf *scaled = NULL;

	const gchar *stock_id = NULL;

	switch (icon) {
	case GjChatEmoteBigGrin:
		stock_id = GJ_STOCK_EMOTE_BIGGRIN;
		break;
	case GjChatEmoteConfused:
		stock_id = GJ_STOCK_EMOTE_CONFUSED;
		break;
	case GjChatEmoteCool:
		stock_id = GJ_STOCK_EMOTE_COOL;
		break;
	case GjChatEmoteEek:
		stock_id = GJ_STOCK_EMOTE_EEK;
		break;
	case GjChatEmoteFrown:
		stock_id = GJ_STOCK_EMOTE_FROWN;
		break;
	case GjChatEmoteMad:
		stock_id = GJ_STOCK_EMOTE_MAD;
		break;
	case GjChatEmoteRedFace:
		stock_id = GJ_STOCK_EMOTE_REDFACE;
		break;
	case GjChatEmoteRollEyes:
		stock_id = GJ_STOCK_EMOTE_ROLLEYES;
		break;
	case GjChatEmoteSmile:
		stock_id = GJ_STOCK_EMOTE_SMILE;
		break;
	case GjChatEmoteTongue:
		stock_id = GJ_STOCK_EMOTE_TONGUE;
		break;
	case GjChatEmoteWink:
		stock_id = GJ_STOCK_EMOTE_WINK;
		break;

	default:
		return NULL;
	}

	if ((pb = gj_gtk_pixbuf_new_from_stock (stock_id)) == NULL) {
		g_warning ("%s: failed to load stock id:'%s'", __FUNCTION__, stock_id);
		return NULL;
	}

	scaled = gdk_pixbuf_scale_simple (pb, 15, 15, GDK_INTERP_BILINEAR);
	g_object_unref (G_OBJECT (pb));

	pb = scaled;

	return pb;
}


#endif

#ifdef __cplusplus
}
#endif
