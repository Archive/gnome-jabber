/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_roster_item_h
#define __gj_roster_item_h

#include "gj_typedefs.h"

/* item functions */
GjRosterItem gj_roster_item_new (const gchar *jid);
GjRosterItem gj_roster_item_new_unknown_by_jid (const gchar *jid);
void gj_roster_item_free (GjRosterItem ri);

/* 
 * member functions 
 */

/* sets */
gboolean gj_roster_item_set_name (GjRosterItem ri, const gchar *name);
gboolean gj_roster_item_set_groups (GjRosterItem ri, GList *groups);

gboolean gj_roster_item_set_roster (GjRosterItem ri, GjRoster r);

gboolean gj_roster_item_set_is_another_resource (GjRosterItem ri, gboolean is_another_resource);
gboolean gj_roster_item_set_is_permanent (GjRosterItem ri, gboolean is_permanent);
gboolean gj_roster_item_set_is_set_for_removal (GjRosterItem ri, gboolean is_set_for_removal);
gboolean gj_roster_item_set_is_for_group_chat (GjRosterItem ri, gboolean is_for_group_chat);
gboolean gj_roster_item_set_is_joining_group_chat (GjRosterItem ri, gboolean is_joining_group_chat);

gboolean gj_roster_item_set_subscription (GjRosterItem ri, GjRosterItemSubscription subscription);
gboolean gj_roster_item_set_presence (GjRosterItem ri, GjPresence pres);
gboolean gj_roster_item_set_ask (GjRosterItem ri, const gchar *ask);

/* gets */
gint gj_roster_item_get_id (GjRosterItem ri);

const GjJID gj_roster_item_get_jid (GjRosterItem ri);
const gchar *gj_roster_item_get_name (GjRosterItem ri);

GList *gj_roster_item_get_groups (GjRosterItem ri);
gchar *gj_roster_item_get_groups_as_string (GjRosterItem ri);

GjRoster gj_roster_item_get_roster (GjRosterItem ri);

gboolean gj_roster_item_get_is_permanent (GjRosterItem ri);
gboolean gj_roster_item_get_is_another_resource (GjRosterItem ri);
gboolean gj_roster_item_get_is_set_for_removal (GjRosterItem ri);           
gboolean gj_roster_item_get_is_for_group_chat (GjRosterItem ri);           
gboolean gj_roster_item_get_is_joining_group_chat (GjRosterItem ri);           

GjRosterItemSubscription gj_roster_item_get_subscription (GjRosterItem ri);
const gchar *gj_roster_item_get_ask (GjRosterItem ri);

GjPresence gj_roster_item_get_presence (GjRosterItem ri);
GjPresence gj_roster_item_get_last_presence (GjRosterItem ri);

/* group functions */
gboolean gj_roster_item_groups_add (GjRosterItem ri, const gchar *group); 
gboolean gj_roster_item_groups_del (GjRosterItem ri, const gchar *group); 
gint gj_roster_item_groups_count (GjRosterItem ri); 
gboolean gj_roster_item_groups_free (GjRosterItem ri);

#endif

