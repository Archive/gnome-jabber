/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_event_h
#define __gj_event_h

#include "gj_typedefs.h"


void gj_event_stats ();

gboolean gj_event_init (GjEventCallback cb, gpointer user_data);
void gj_event_term ();

gboolean gj_event_set_cb (GjEventCallback cb);
GjEventCallback gj_event_get_cb ();

/* 
 * item functions
 */

/* create/delete */
GjEvent gj_event_new (GjRoster r, GjJID j, GjEventType type, void *data);
/* gboolean gj_event_new_by_post_from_jid (const gchar *jid, GjEventType type, void *data); */
/* gboolean gj_event_new_by_post_from_ri_id (gint id, GjEventType type, void *data); */
gboolean gj_event_free (GjEvent ev);
gboolean gj_event_free_by_id (gint eventID);

/* find */
GjEvent gj_event_find_by_id (gint eventID);
GjEvent gj_event_find_next_by_type (GjRosterItem ri, GjEventType type);
GjEvent gj_event_find_next_by_message_type (GjRosterItem ri, GjMessageType message_type);

/* utils */
GjEvent gj_event_nth (gint index);
GjEvent gj_event_nth_by_roster_item (GjRosterItem ri, gint index);
gint gj_event_next (GjRosterItem ri);

gint gj_event_count (GjRosterItem ri);
gint gj_event_count_by_type (GjRosterItem ri, GjEventType type);

gint gj_event_count_all ();


/* 
 * member functions 
 */

/* gets */
gint gj_event_get_id (GjEvent ev);
const GjJID gj_event_get_jid (GjEvent ev);
GjEventType gj_event_get_type (GjEvent ev);
void *gj_event_get_data (GjEvent ev);
time_t gj_event_get_time (GjEvent ev);
gint gj_event_get_ri_id (GjEvent ev);
GjRosterItem gj_event_get_roster_item (GjEvent ev);

/* sets */
gboolean gj_event_set_ri_id (GjEvent ev, gint id);

#endif

#ifdef __cplusplus
}
#endif
