/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_iq_h
#define __gj_iq_h

#include "gj_typedefs.h"

void gj_iq_stats ();

/* create / free */
GjInfoquery gj_iq_new (GjInfoqueryType type);

GjInfoquery gj_iq_ref (GjInfoquery iq);
gboolean gj_iq_unref (GjInfoquery iq);

/* formation */
gboolean gj_iq_build (GjInfoquery iq, xmlNodePtr node);
gboolean gj_iq_build_from_list (GjInfoquery iq, GList *nodes);
gboolean gj_iq_send (GjInfoquery iq);

/*
 * members 
 */

/* set */
gboolean gj_iq_set_user_data (GjInfoquery iq, gpointer user_data);
gboolean gj_iq_set_id (GjInfoquery iq, const gchar *id);
gboolean gj_iq_set_to (GjInfoquery iq, const gchar *to);
gboolean gj_iq_set_from (GjInfoquery iq, const gchar *from);
gboolean gj_iq_set_type (GjInfoquery iq, GjInfoqueryType type);
gboolean gj_iq_set_xml (GjInfoquery iq, xmlNodePtr node);
gboolean gj_iq_set_connection (GjInfoquery iq, GjConnection connection);
gboolean gj_iq_set_callback (GjInfoquery iq, GjInfoqueryCallback cb);
gboolean gj_iq_set_event_id (GjInfoquery iq, guint eventID);
gboolean gj_iq_set_related_id (GjInfoquery iq, guint id);

/* get */
gpointer gj_iq_get_user_data (GjInfoquery iq);
const gchar *gj_iq_get_id (GjInfoquery iq);
const GjConnection gj_iq_get_connection (GjInfoquery iq);
const gchar *gj_iq_get_to (GjInfoquery iq);
const gchar *gj_iq_get_from (GjInfoquery iq);
GjInfoqueryType gj_iq_get_type (GjInfoquery iq);
xmlNodePtr gj_iq_get_xml (GjInfoquery iq);
void *gj_iq_get_callback (GjInfoquery iq);
guint gj_iq_get_event_id (GjInfoquery iq);
guint gj_iq_get_related_id (GjInfoquery iq);

/* finding */
GjInfoquery gj_iq_find (guint id);

/* other - used by parser */
void gj_iq_receive (GjConnection c, xmlNodePtr node);

/*
 * Roster information
 */
typedef void (*infoqueryRosterResponseProc) (GList *roster);

#endif

#ifdef __cplusplus
}
#endif
