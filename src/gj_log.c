/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "gj_log.h"
#include "gj_support.h"
#include "gj_config.h"
#include "gj_main.h"

#include "gj_gtk_log.h"
 
#define PATH_LOG "logs"
#define FILE_LOG PACKAGE ".log"


static gboolean log_initialised = FALSE;
static gboolean log_started = FALSE;

static GIOChannel *log_channel = NULL;


static gchar *gj_log_check_dir ();


gboolean gj_log_init ()
{
	/* GLib log levels 

	G_LOG_LEVEL_ERROR             = 1 << 2,        - always fatal 
	G_LOG_LEVEL_CRITICAL          = 1 << 3,
	G_LOG_LEVEL_WARNING           = 1 << 4,
	G_LOG_LEVEL_MESSAGE           = 1 << 5,
	G_LOG_LEVEL_INFO              = 1 << 6,
	G_LOG_LEVEL_DEBUG             = 1 << 7,
  
	*/

	/* set up event handlers */
	g_log_set_handler (NULL, 
			   G_LOG_LEVEL_INFO | G_LOG_LEVEL_DEBUG | G_LOG_LEVEL_MESSAGE | G_LOG_LEVEL_WARNING | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("XML", 
			   G_LOG_LEVEL_INFO | G_LOG_LEVEL_DEBUG | G_LOG_LEVEL_MESSAGE | G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);

	g_log_set_handler ("libglade", 
			   G_LOG_LEVEL_INFO | G_LOG_LEVEL_MESSAGE | G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("Gtk", 
			   G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL,  
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("Gdk", 
			   G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL,  
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("GLib", 
			   G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("GModule", 
			   G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("GThreads", 
			   G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("Glade", 
			   G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("GLib-GObject", 
			   G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);
	g_log_set_handler ("GObject", 
			   G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, 
			   gj_log_handler, 
			   NULL);

	log_initialised = TRUE;

	return TRUE;
}

static gchar *gj_log_check_dir ()
{
	gchar *dir_level1 = NULL;
	gchar *dir_level2 = NULL;

#ifdef G_OS_WIN32

	dir_level1 = g_build_filename (g_get_home_dir (), PACKAGE_NAME, NULL); 
	dir_level2 = g_build_filename (g_get_home_dir (), PACKAGE_NAME, PATH_LOG, NULL);
#else
	dir_level1 = g_build_filename (g_get_home_dir (), ".gnome2", PACKAGE_NAME, NULL);
	dir_level2 = g_build_filename (g_get_home_dir (), ".gnome2", PACKAGE_NAME, PATH_LOG, NULL);
#endif

	if (!g_file_test (dir_level1, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))) {
		if (!gj_create_dir (dir_level1)) {
			g_warning ("%s: could not log create config dir:'%s'", __FUNCTION__, dir_level1);
			g_free (dir_level1);
			g_free (dir_level2);
			return NULL;
		}
	}

	if (!g_file_test (dir_level2, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR))) {
		if (!gj_create_dir (dir_level2)) {
			g_warning ("%s: could not log dir:'%s'", __FUNCTION__, dir_level2);
			g_free (dir_level1);
			g_free (dir_level2);
			return NULL;
		}
	}

	g_free (dir_level1);
  
	return dir_level2;
}

gboolean gj_log_start ()
{
	gsize bytes;
	gchar *message = NULL;

	gchar *filename = NULL;
	const gchar *path = NULL;

	path = gj_log_check_dir ();
	g_message ("%s: using log path:'%s'", __FUNCTION__, path);
  
	filename = g_build_filename (path,
				     FILE_LOG,
				     NULL);

	g_message ("%s: using log filename:'%s'", __FUNCTION__, filename);
	log_channel = g_io_channel_new_file (filename,"w",NULL);

	if (log_channel == NULL) {
		g_error ("%s: failed to create new file:'%s' (channel) for log (path:'%s')",
			 __FUNCTION__, filename, path);
		return FALSE;
	}

	/* start top of log */
	message = g_strdup_printf ("Log started on %s at %s\n\n", gj_get_datestamp (), gj_get_timestamp ());

	g_io_channel_write_chars (log_channel,
				  message,
				  -1,
				  &bytes,
				  NULL);

	/* clean up */
	g_free (message);

	gj_gtk_log_load ();

	/* set to inialised */
	log_started = TRUE;

	return TRUE;
}

gboolean gj_log_term ()
{
	/* close file */
	g_io_channel_shutdown (log_channel,TRUE,NULL);

	/* uninitialise logging */
	log_started = FALSE;

	return TRUE;
}

void gj_log_handler (const gchar *log_domain, GLogLevelFlags log_level, const gchar *log_message, gpointer user_data)
{
	gchar *message = NULL;
	gchar *message_log_level = NULL;
	gchar *extra = NULL;
	gchar *reformatted = NULL;
	guint bytes = 0;
	gboolean valid = FALSE;

	if (log_message == NULL) {
		fprintf (stderr, "could not log message it was NULL");
		fflush (stderr);
		return;
	}

	if (log_level == GJ_LOG_LEVEL_WARNING) {
		message_log_level = g_strdup_printf (" ** WARNING ** ");
	} else if (log_level == GJ_LOG_LEVEL_ERROR) {
		message_log_level = g_strdup_printf (" ** ERROR ** ");
	} else if (log_level == GJ_LOG_LEVEL_CRITICAL) {
		message_log_level = g_strdup_printf (" ** CRITICAL ** ");
	}

#ifdef G_OS_WIN32
	/*     extra = "\r"; */
	extra = "";
#endif

	reformatted = gj_find_in_str_and_replace ("%", log_message, "%%"); 

	/* format buffer */
	message = g_strdup_printf ("%s:%s:%s%s%s%s\n",
				   gj_get_timestamp (),
				   log_domain?log_domain:"GJ",
				   message_log_level?message_log_level:"",
				   message_log_level?" ":"\t",
				   reformatted,
				   extra != NULL?extra:"");	

	if (log_initialised == 0 || log_started == 0) {
#ifndef G_OS_WIN32
		/* if NOT initialised, used stderr */
		fprintf (stderr, message);
		fflush (stderr);
#endif
	} else {
		/* write to file */
		if ((valid = g_utf8_validate (message, -1, NULL)) == FALSE) {
			fprintf (stderr, "[CRITICAL ERROR] !!!! INVALID UTF8 - WRITING LOG\n");
			fflush (stderr);
		} else {
			g_io_channel_write_chars (log_channel,
						  message,
						  -1,
						  &bytes,
						  NULL);
	    
		}

		if (gnome_jabber_get_log_to_screen () == TRUE) {
			gboolean print_it = TRUE;

			if (log_level == G_LOG_LEVEL_DEBUG && gnome_jabber_get_debugging () == FALSE) {
				print_it = FALSE;
			}

			if (print_it == TRUE) {
#ifndef G_OS_WIN32
				fprintf (stdout, message);
				fflush (stdout);
#endif
			}
		}
	}

	/* for some reason, using g_io_channel_write_chars interprets the %, so
	   you need %%.  But the log function doesnt, so you just use the original 
	   string. */

	if (valid == TRUE) {
		gj_gtk_log_new (log_domain, log_level, log_message, NULL);
	}

	/* clean up */
	g_free (message);
	g_free (message_log_level);
	g_free (reformatted);
}

#ifdef __cplusplus
}
#endif
 
