/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$ 
 *
 */

#ifndef __gj_gatherdata_h
#define __gj_gatherdata_h

#include "gj_typedefs.h"


/* list functions */
GList *gj_gatherdata_get_list ();

/* item functions */
GjData gj_gatherdata_new ();
void gj_gatherdata_free (GjData gd);

/* find */
GjData gj_gatherdata_find_by_id (gchar *id);
GjData gj_gatherdata_find_by_to (gchar *to);
GjData gj_gatherdata_find_by_from (gchar *from);
GjData gj_gatherdata_find_by_related_id (gchar *id);

/* sets */
gboolean gj_gatherdata_set_id (GjData gd, gchar *id);
gboolean gj_gatherdata_set_to (GjData gd, gchar *to);
gboolean gj_gatherdata_set_from (GjData gd, gchar *to);
gboolean gj_gatherdata_set_type (GjData gd, GjDataFormType type);
gboolean gj_gatherdata_set_instructions (GjData gd, gchar *instructions);
gboolean gj_gatherdata_set_fields (GjData gd, GList *fields);
gboolean gj_gatherdata_set_callback (GjData gd, GjDataCallback cb);
gboolean gj_gatherdata_set_related_id (GjData gd, gchar *id);

/* gets */
gchar *gj_gatherdata_get_id (GjData gd);
gchar *gj_gatherdata_get_to (GjData gd);
gchar *gj_gatherdata_get_from (GjData gd);
GjDataFormType gj_gatherdata_get_type (GjData gd);
gchar *gj_gatherdata_get_instructions (GjData gd);
GList *gj_gatherdata_get_fields (GjData gd);
GjDataCallback gj_gatherdata_get_callback (GjData gd);
gchar *gj_gatherdata_get_related_id (GjData gd);

/* callbacks */
/* gboolean gatherdataListRequest (gchar *server, gatherdataResponseProc proc); */

/* debugging */
void gj_gatherdata_print (GjData gd);
void gj_gatherdata_print_all ();

/* 
 * GjData field
 */

/* list functions */
GList *gj_gatherdata_field_list_get (GjData gd);

/* item functions */
GjDataField gj_gatherdata_field_new (GjData gd, GjDataFieldType type);
void gj_gatherdata_field_free (GjData gd, GjDataField gdf);

/* sets */
gboolean gj_gatherdata_field_set_type (GjDataField gdf, GjDataFieldType type);
gboolean gj_gatherdata_field_set_label (GjDataField gdf, gchar *label);
gboolean gj_gatherdata_field_set_var (GjDataField gdf, gchar *var);
gboolean gj_gatherdata_field_set_value (GjDataField gdf, gchar *value);
gboolean gj_gatherdata_field_set_required (GjDataField gdf, gboolean required);
gboolean gj_gatherdata_field_set_options (GjDataField gdf, GList *options);

/* gets */
GjDataFieldType gj_gatherdata_field_get_type (GjDataField gdf);
gchar *gj_gatherdata_field_get_label (GjDataField gdf);
gchar *gj_gatherdata_field_get_var (GjDataField gdf);
gchar *gj_gatherdata_field_get_value (GjDataField gdf);
gboolean gj_gatherdata_field_get_required (GjDataField gdf);
GList *gj_gatherdata_field_get_options (GjDataField gdf);

/* 
 * GjData field option
 */

/* list functions */
GList *gj_gatherdata_field_option_list_get (GjDataField gdf);

/* item functions */
GjDataFieldOption gj_gatherdata_field_option_new (GjDataField gdf, gchar *label);
void gj_gatherdata_field_option_free (GjDataField gdf, GjDataFieldOption gdfo);

/* sets */
gboolean gj_gatherdata_field_option_set_label (GjDataFieldOption gdfo, gchar *label);
gboolean gj_gatherdata_field_option_set_value (GjDataFieldOption gdfo, gchar *value);

/* gets */
gchar *gj_gatherdata_field_option_get_label (GjDataFieldOption gdfo);
gchar *gj_gatherdata_field_option_get_value (GjDataFieldOption gdfo);

/*
 * parsing functions  
 */
gboolean gj_gatherdata_request (gchar *server, GjDataRequestCallback cb);

#endif

