/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"
#include "gj_config.h"
#include "gj_connection.h"

#include "gj_gtk_password_change.h"
#include "gj_gtk_connection_settings.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry1;
	GtkWidget *entry2;

	GtkWidget *button_ok;

	GjConnection c;

} GjPasswordChangeDialog;


static gboolean gj_gtk_pc_changed (GjPasswordChangeDialog *dialog);
static gboolean gj_gtk_pc_set (GjPasswordChangeDialog *dialog);

static void on_entry_password_1_changed (GtkEditable *editable, GjPasswordChangeDialog *dialog);
static void on_entry_password_2_changed (GtkEditable *editable, GjPasswordChangeDialog *dialog);

static void on_destroy (GtkWidget *widget, GjPasswordChangeDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjPasswordChangeDialog *dialog);


static GjPasswordChangeDialog *current_dialog = NULL;


gboolean gj_gtk_pc_load (GjConnection c)
{
	GjPasswordChangeDialog *dialog = NULL;
	GladeXML *xml = NULL;
  
	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjPasswordChangeDialog, 1);
	
	dialog->c = gj_connection_ref (c);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "password_change",
				     NULL,
				     "password_change", &dialog->window,
				     "entry1", &dialog->entry1,
				     "entry2", &dialog->entry2,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "password_change", "response", on_response,
			      "password_change", "destroy", on_destroy,
			      "entry1", "changed", on_entry_password_1_changed,
			      "entry2", "changed", on_entry_password_2_changed,
			      NULL);

	g_object_unref (xml);
  
	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set up dialog */
	gtk_widget_set_sensitive (dialog->button_ok, FALSE);

	return TRUE;
}

static gboolean gj_gtk_pc_set (GjPasswordChangeDialog *dialog)
{
	G_CONST_RETURN gchar *password = NULL;

	if (!dialog) {
		return FALSE;
	}

	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry1));
	gj_iq_request_password_change (dialog->c,
				       gj_config_get_cs_server (), 
				       password, 
				       gj_gtk_pc_set_response,
				       NULL);
  
	return TRUE;
}

void gj_gtk_pc_set_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	if (!iq || !iq_related) {  
		return;
	}

	node = gj_iq_get_xml (iq_related);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		gchar *detail = NULL;

		xmlChar *error_code = NULL;
		xmlChar *error_message = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		error_code = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		error_message = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		g_message ("%s: password change failed", __FUNCTION__);
		g_message ("%s: error code:%d, reason:'%s'", __FUNCTION__, atoi ((gchar*)error_code), error_message);

		gj_gtk_show_error (_("Error Changing Password."),
				   atoi ((gchar*)error_code),
				   (gchar*)error_message);

		g_free (detail);

		return;
	}
  
	gj_gtk_show_success (_("Password Change."));
}

static gboolean gj_gtk_pc_changed (GjPasswordChangeDialog *dialog)
{
	gboolean match = FALSE;

	G_CONST_RETURN gchar *password1 = NULL;
	G_CONST_RETURN gchar *password2 = NULL;

	if (!dialog) {
		return FALSE;
	}

	password1 = gtk_entry_get_text (GTK_ENTRY (dialog->entry1));
	password2 = gtk_entry_get_text (GTK_ENTRY (dialog->entry2));

	if (password1 != NULL && g_utf8_strlen (password1, -1) > 0 && 
	    password2 != NULL && g_utf8_strlen (password2, -1) > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), (strcmp (password1, password2) == 0));
	}

	return TRUE;
}


/*
 * GUI events
 */

static void on_entry_password_1_changed (GtkEditable *editable, GjPasswordChangeDialog *dialog)
{
	gj_gtk_pc_changed (dialog);
}

static void on_entry_password_2_changed (GtkEditable *editable, GjPasswordChangeDialog *dialog)
{
	gj_gtk_pc_changed (dialog);
}

/*
 * GUI events
 */
static void on_destroy (GtkWidget *widget, GjPasswordChangeDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);
  
	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjPasswordChangeDialog *dialog)
{
	if (response == GTK_RESPONSE_OK) {
		gj_gtk_pc_set (dialog);
	}
  
	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
