/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_config.h"
#include "gj_log.h"

#include "gj_gtk_roster.h"
#include "gj_gtk_preferences.h"
#include "gj_gtk_docklet.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *checkbutton_hide_offline_contacts;
	GtkWidget *checkbutton_hide_offline_group;
	GtkWidget *checkbutton_hide_agents;
	GtkWidget *checkbutton_hide_contact_presence_details;

	GtkWidget *radiobutton_double_click_for_chat;
	GtkWidget *radiobutton_double_click_for_message;

	GtkWidget *checkbutton_open_window_on_new_message;
	GtkWidget *checkbutton_raise_window_on_message;
	GtkWidget *checkbutton_hide_time;
	GtkWidget *checkbutton_reverse_text_direction;
	GtkWidget *checkbutton_use_smileys;

	GtkWidget *checkbutton_use_spell_checker;

	GtkWidget *checkbutton_use_auto_away;
	GtkWidget *spinbutton_auto_away_after;
	GtkWidget *spinbutton_auto_xa_after;
	GtkWidget *textview_auto_away_status;
	GtkWidget *textview_auto_xa_status;

	GtkWidget *entry_browse_log_location;

	GtkWidget *file_selector;

	GtkWidget *table_sound;
	GtkWidget *checkbutton_sound_enabled;
	GtkWidget *checkbutton_sound_on_user_online;
	GtkWidget *checkbutton_sound_on_user_offline;
	GtkWidget *checkbutton_sound_on_receive_message;
	GtkWidget *checkbutton_sound_on_receive_chat;
	GtkWidget *checkbutton_sound_on_receive_headline;
	GtkWidget *checkbutton_sound_on_receive_error;
	GtkWidget *checkbutton_sound_beep_if_no_file;

	GtkWidget *entry_sound_file_on_user_online;
	GtkWidget *entry_sound_file_on_user_offline;
	GtkWidget *entry_sound_file_on_receive_message;
	GtkWidget *entry_sound_file_on_receive_chat;
	GtkWidget *entry_sound_file_on_receive_headline;
	GtkWidget *entry_sound_file_on_receive_error;
	GtkWidget *entry_sound_beep_if_no_file;

	GtkWidget *vbox_docklet;
	GtkWidget *checkbutton_use_docklet;

	GtkWidget *checkbutton_use_history;
	GtkWidget *entry_browse_history_location;

	GtkWidget *notebook;
    
} GjPreferencesDialog;


static gboolean gj_gtk_prefs_get (GjPreferencesDialog *dialog);
static gboolean gj_gtk_prefs_set (GjPreferencesDialog *dialog);

/*
 * GUI functions
 */
static void on_checkbutton_hide_offline_contacts_toggled (GtkToggleButton *togglebutton, GjPreferencesDialog *dialog);

/* static void on_checkbutton_prefs_sound_enabled_toggled (GtkToggleButton *togglebutton, GjPreferencesDialog *dialog); */

static void on_destroy (GtkWidget *widget, GjPreferencesDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjPreferencesDialog *dialog);


static GjPreferencesDialog *current_dialog = NULL;


gboolean gj_gtk_prefs_load ()
{
	GjPreferencesDialog *dialog = NULL;
	GladeXML *xml = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjPreferencesDialog, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "preferences",
				     NULL,
				     "preferences", &dialog->window,
				     "checkbutton_hide_offline_contacts", &dialog->checkbutton_hide_offline_contacts, 
				     "checkbutton_hide_offline_group", &dialog->checkbutton_hide_offline_group,
				     "checkbutton_hide_agents", &dialog->checkbutton_hide_agents,
				     "checkbutton_hide_contact_presence_details", &dialog->checkbutton_hide_contact_presence_details,
				     "radiobutton_double_click_for_chat", &dialog->radiobutton_double_click_for_chat,
				     "radiobutton_double_click_for_message", &dialog->radiobutton_double_click_for_message,
			      
				     "checkbutton_open_window_on_new_message", &dialog->checkbutton_open_window_on_new_message,
				     "checkbutton_raise_window_on_message", &dialog->checkbutton_raise_window_on_message ,
				     "checkbutton_hide_time", &dialog->checkbutton_hide_time, 
				     "checkbutton_reverse_text_direction", &dialog->checkbutton_reverse_text_direction,
				     "checkbutton_use_smileys", &dialog->checkbutton_use_smileys,
				     "checkbutton_use_spell_checker", &dialog->checkbutton_use_spell_checker,
				     "checkbutton_use_auto_away", &dialog->checkbutton_use_auto_away,
				     "spinbutton_auto_away_after", &dialog->spinbutton_auto_away_after,
				     "spinbutton_auto_xa_after", &dialog->spinbutton_auto_xa_after,
				     "textview_auto_away_status", &dialog->textview_auto_away_status,
				     "textview_auto_xa_status", &dialog->textview_auto_xa_status,
				     "entry_browse_log_location", &dialog->entry_browse_log_location,
				     "table_sound", &dialog->table_sound,
				     /* 			      "checkbutton_sound_enabled", &dialog->checkbutton_sound_enabled, */
				     "checkbutton_sound_on_user_online", &dialog->checkbutton_sound_on_user_online, 
				     "checkbutton_sound_on_user_offline", &dialog->checkbutton_sound_on_user_offline,
				     "checkbutton_sound_on_receive_message", &dialog->checkbutton_sound_on_receive_message,
				     "checkbutton_sound_on_receive_chat", &dialog->checkbutton_sound_on_receive_chat,
				     "checkbutton_sound_on_receive_headline", &dialog->checkbutton_sound_on_receive_headline,
				     "checkbutton_sound_on_receive_error", &dialog->checkbutton_sound_on_receive_error,
				     /* 			      "checkbutton_sound_beep_if_no_file", &dialog->checkbutton_sound_beep_if_no_file, */
				     "entry_browse_sound_on_user_online", &dialog->entry_sound_file_on_user_online,
				     "entry_browse_sound_on_user_offline", &dialog->entry_sound_file_on_user_offline,
				     "entry_browse_sound_on_receive_message", &dialog->entry_sound_file_on_receive_message,
				     "entry_browse_sound_on_receive_chat", &dialog->entry_sound_file_on_receive_chat,
				     "entry_browse_sound_on_receive_headline", &dialog->entry_sound_file_on_receive_headline,
				     "entry_browse_sound_on_receive_error", &dialog->entry_sound_file_on_receive_error,
				     "checkbutton_use_docklet", &dialog->checkbutton_use_docklet,
				     "vbox_docklet", &dialog->vbox_docklet,
				     "checkbutton_use_history", &dialog->checkbutton_use_history,
				     "entry_browse_history_location", &dialog->entry_browse_history_location,
				     "notebook",  &dialog->notebook,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "preferences", "response", on_response,
			      "preferences", "destroy", on_destroy,
			      "checkbutton_hide_offline_contacts", "toggled", on_checkbutton_hide_offline_contacts_toggled,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);

#ifdef G_OS_WIN32
	gtk_widget_hide (dialog->vbox_docklet);
#endif

	/* get settings */
	gj_gtk_prefs_get (dialog);

	return TRUE;
}

static gboolean gj_gtk_prefs_set (GjPreferencesDialog *dialog)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter bufStart;
	GtkTextIter bufStop;

	/* get values */
	gj_config_set_mpf_hide_offline_contacts (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_offline_contacts)));
	gj_config_set_mpf_hide_offline_group (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_offline_group)));
	gj_config_set_mpf_hide_agents (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_agents)));
	gj_config_set_mpf_hide_contact_presence_details (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_contact_presence_details)));

	gj_config_set_mpf_double_click_for_chat (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->radiobutton_double_click_for_chat)));

	gj_config_set_mpf_open_window_on_new_message (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_open_window_on_new_message)));
	gj_config_set_mpf_raise_window_on_message (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_raise_window_on_message)));
	gj_config_set_mpf_hide_time (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_time)));
	gj_config_set_mpf_reverse_text_direction (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_reverse_text_direction)));
	gj_config_set_mpf_use_smileys (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_smileys)));

	gj_config_set_mpf_use_spell_checker (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_spell_checker)));

	gj_config_set_mpf_use_auto_away (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_auto_away)));
	gj_config_set_mpf_auto_away_after (gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_auto_away_after)));
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_auto_away_status));
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStart, 0);
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStop, -1);
	gj_config_set_mpf_auto_away_status (gtk_text_buffer_get_text (buffer,&bufStart,&bufStop,TRUE));

	gj_config_set_mpf_auto_xa_after (gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_auto_xa_after)));
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_auto_xa_status));
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStart, 0);
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStop, -1);
	gj_config_set_mpf_auto_xa_status (gtk_text_buffer_get_text (buffer,&bufStart,&bufStop,TRUE));

	gj_config_set_mpf_sound_on_user_online (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_user_online)));
	gj_config_set_mpf_sound_on_user_offline (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_user_offline)));
	gj_config_set_mpf_sound_on_receive_message (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_message)));
	gj_config_set_mpf_sound_on_receive_chat (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_chat)));
	gj_config_set_mpf_sound_on_receive_headline (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_headline)));
	gj_config_set_mpf_sound_on_receive_error (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_error)));

	gj_config_set_mpf_sound_file_on_user_online ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_sound_file_on_user_online)));
	gj_config_set_mpf_sound_file_on_user_offline ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_sound_file_on_user_offline)));
	gj_config_set_mpf_sound_file_on_receive_message ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_message)));
	gj_config_set_mpf_sound_file_on_receive_chat ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_chat)));
	gj_config_set_mpf_sound_file_on_receive_headline ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_headline)));
	gj_config_set_mpf_sound_file_on_receive_error ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_error)));
	gj_config_set_mpf_use_docklet (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_docklet)));

	gj_config_set_mpf_log_location ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_browse_log_location)));
	gj_config_set_mpf_history_location ((gchar*)gtk_entry_get_text (GTK_ENTRY (dialog->entry_browse_history_location)));
	gj_config_set_mpf_use_history (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_history)));

	if (gj_config_write () == FALSE) {
		g_warning ("%s: failed to write config", __FUNCTION__);
		return FALSE;
	}

	gj_gtk_roster_config_updated ();

#if 0
	if (gj_config_get_prefs_use_docklet () == TRUE) {
		gj_gtk_docklet_load ();
	} else {
		gj_gtk_docklet_unload ();
	}
#endif

	return TRUE;
}

static gboolean gj_gtk_prefs_get (GjPreferencesDialog *dialog)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter bufStart;

	if (gj_config_read () == FALSE) {
		g_message ("gj_gtk_prefs_get: config read failed, continuing anyway...");
	}

	/* set up settings */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_offline_contacts), 
				      gj_config_get_mpf_hide_offline_contacts ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_offline_group), 
				      gj_config_get_mpf_hide_offline_group ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_agents), 
				      gj_config_get_mpf_hide_agents ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_contact_presence_details), 
				      gj_config_get_mpf_hide_contact_presence_details ());

	if (gj_config_get_mpf_double_click_for_chat () == TRUE) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->radiobutton_double_click_for_chat), TRUE);
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->radiobutton_double_click_for_message), TRUE);
	}

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_open_window_on_new_message),
				      gj_config_get_mpf_open_window_on_new_message ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_raise_window_on_message),
				      gj_config_get_mpf_raise_window_on_message ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_hide_time),
				      gj_config_get_mpf_hide_time ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_reverse_text_direction),
				      gj_config_get_mpf_reverse_text_direction ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_smileys),
				      gj_config_get_mpf_use_smileys ());

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_spell_checker),
				      gj_config_get_mpf_use_spell_checker ());

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_auto_away),
				      gj_config_get_mpf_use_auto_away ());
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton_auto_away_after), 
				   (gdouble)gj_config_get_mpf_auto_away_after ());
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton_auto_xa_after), 
				   (gdouble)gj_config_get_mpf_auto_xa_after ());
  
	if (gj_config_get_mpf_auto_away_status () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_auto_away_status (),-1) > 0) {
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_auto_away_status));
		gtk_text_buffer_get_start_iter (buffer, &bufStart);
		gtk_text_buffer_insert (buffer, &bufStart, gj_config_get_mpf_auto_away_status (), -1);
	}

	if (gj_config_get_mpf_auto_xa_status () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_auto_xa_status (),-1) > 0) {
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (dialog->textview_auto_xa_status));
		gtk_text_buffer_get_start_iter (buffer, &bufStart);
		gtk_text_buffer_insert (buffer, &bufStart, gj_config_get_mpf_auto_xa_status (), -1);
	}

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_user_online), 
				      gj_config_get_mpf_sound_on_user_online ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_user_offline), 
				      gj_config_get_mpf_sound_on_user_offline ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_message), 
				      gj_config_get_mpf_sound_on_receive_message ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_chat), 
				      gj_config_get_mpf_sound_on_receive_chat ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_headline), 
				      gj_config_get_mpf_sound_on_receive_headline ());
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_sound_on_receive_error), 
				      gj_config_get_mpf_sound_on_receive_error ());

	if (gj_config_get_mpf_sound_file_on_user_online () && 
	    g_utf8_strlen (gj_config_get_mpf_sound_file_on_user_online (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_sound_file_on_user_online), 
				    gj_config_get_mpf_sound_file_on_user_online ());
	}

	if (gj_config_get_mpf_sound_file_on_user_offline () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_sound_file_on_user_offline (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_sound_file_on_user_offline), 
				    gj_config_get_mpf_sound_file_on_user_offline ());
	}

	if (gj_config_get_mpf_sound_file_on_receive_message () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_sound_file_on_receive_message (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_message), 
				    gj_config_get_mpf_sound_file_on_receive_message ());
	}

	if (gj_config_get_mpf_sound_file_on_receive_chat () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_sound_file_on_receive_chat (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_chat),
				    gj_config_get_mpf_sound_file_on_receive_chat ());
	}

	if (gj_config_get_mpf_sound_file_on_receive_headline () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_sound_file_on_receive_headline (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_headline), 
				    gj_config_get_mpf_sound_file_on_receive_headline ());
	}

	if (gj_config_get_mpf_sound_file_on_receive_error () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_sound_file_on_receive_error (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_sound_file_on_receive_error), 
				    gj_config_get_mpf_sound_file_on_receive_error ()); 
	}

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_docklet), 
				      gj_config_get_mpf_use_docklet ());
	
	if (gj_config_get_mpf_log_location () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_log_location (), -1) > 0) { 
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_browse_log_location), 
				    gj_config_get_mpf_log_location ());
	}

	if (gj_config_get_mpf_history_location () != NULL && 
	    g_utf8_strlen (gj_config_get_mpf_history_location (), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_browse_history_location), 
				    gj_config_get_mpf_history_location ());
	}

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->checkbutton_use_history), 
				      gj_config_get_mpf_use_history ());

	return TRUE;
}

/*
 * GUI events
 */
static void on_checkbutton_hide_offline_contacts_toggled (GtkToggleButton *togglebutton, GjPreferencesDialog *dialog)
{
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->checkbutton_hide_offline_group), 
				  !gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (togglebutton)));
}

static void on_destroy (GtkWidget *widget, GjPreferencesDialog *dialog)
{
	current_dialog = NULL;
  
	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjPreferencesDialog *dialog)
{
	if (!dialog) {
		return;
	}

	if (response == GTK_RESPONSE_OK) {
		gj_gtk_prefs_set (dialog);
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
