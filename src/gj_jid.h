/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$ 
 *
 */

#ifndef __gj_jid_h
#define __gj_jid_h

#include "gj_typedefs.h"

GjJID gj_jid_new (const gchar *s);

/* gets */
const gchar *gj_jid_get_full (GjJID jid);
const gchar *gj_jid_get_without_resource (GjJID jid);
const gchar *gj_jid_get_resource (GjJID jid);
const gchar *gj_jid_get_agent (GjJID jid);

GjJIDType gj_jid_get_type (GjJID jid);

gchar *gj_jid_get_part_name_new (GjJID jid);

/* sets */
void gj_jid_set_resource (GjJID jid, const gchar *resource);

GjJID gj_jid_ref (GjJID jid);
void gj_jid_unref (GjJID jid);

/* compares */
gboolean gj_jid_equals (GjJID jid_a, GjJID jid_b);
gboolean gj_jid_equals_without_resource (GjJID jid_a, GjJID jid_b);

/* checks */
gboolean gj_jid_string_is_valid_jid (const gchar *s);
gboolean gj_jid_string_is_valid_jid_contact (const gchar *s);
gboolean gj_jid_string_is_valid_jid_agent (const gchar *s);

#endif

