/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_parser_h
#define __gj_parser_h

#include "gj_typedefs.h"

void gj_parser_stats ();

/* item functions */
GjParser gj_parser_new (GjConnection c);
void gj_parser_free (GjParser p);

/* find functions */
GjParser gj_parser_find (GjConnection c);

/* input/output to parser */
void gj_parser_push_input (GjParser p, gchar *buf);
void gj_parser_push_input_from_connection (GjConnection c, gchar *buf);

gboolean gj_parser_push_output (GjConnection c, xmlNodePtr node);
gboolean gj_parser_push_output_from_text (GjConnection c, gchar *text);

/* stats */
gint gj_parser_doc_count_top_level_nodes (xmlDocPtr doc);
xmlNodePtr gj_parser_print_tree (xmlNodePtr node, gint depth);

/* finding values (and returning) */
xmlChar *gj_parser_find_by_attr_and_ret (xmlNodePtr node, xmlChar *attrname);
xmlChar *gj_parser_find_by_node_and_ret (xmlNodePtr node, xmlChar *text);
const xmlChar *gj_parser_find_by_ns_and_ret (xmlNodePtr node);

/* finding values on same parent (and returning) */
const xmlChar *gj_parser_find_by_node_on_parent_and_ret (xmlNodePtr node, xmlChar *text);

/* find parts of the tree */
xmlNodePtr gj_parser_find_by_node (xmlNodePtr node, xmlChar *text);
xmlNodePtr gj_parser_find_by_node_on_parent (xmlNodePtr node, xmlChar *text);
xmlNodePtr gj_parser_find_by_node_contents (xmlNodePtr node, xmlChar *contents);
xmlNodePtr gj_parser_find_by_node_with_attr (xmlNodePtr node, xmlChar *nodeName, xmlChar *attrName, xmlChar *attrVal);

xmlAttrPtr gj_parser_find_by_attr (xmlAttrPtr prop, xmlChar *text);
xmlNodePtr gj_parser_find_by_ns (xmlNodePtr node);

/* finding and returning multiples */
GList *gj_parser_find_by_node_and_ret_by_list (xmlNodePtr node, xmlChar *text);
GList *gj_parser_find_by_node_on_parent_and_ret_by_list (xmlNodePtr node, xmlChar *text);
GList *gj_parser_find_by_node_and_ret_contents_by_list (xmlNodePtr node, xmlChar *text);
GList *gj_parser_find_by_node_on_parent_and_ret_contents_by_list (xmlNodePtr node, xmlChar *text);

/* conversions */
gchar *gj_parser_get_str_from_node (const xmlNodePtr node);        /* converts an xml tree into text */

const xmlChar *gj_parser_get_element (xmlNodePtr node);
const xmlChar *gj_parser_get_content (xmlNodePtr node);

#endif

