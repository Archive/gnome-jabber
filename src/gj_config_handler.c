/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

#include <glib.h>

#include "gj_config_handler.h"
#include "gj_config.h"


gchar *gj_config_handler_get_default_filename (void);

GjConfigSection *gj_config_handler_create_section (GjConfigFile *cfg, gchar *name);
GjConfigLine *gj_config_handler_create_string (GjConfigSection *section, gchar *key, gchar *value);
GjConfigSection *gj_config_handler_find_section (GjConfigFile *cfg, gchar *name);
GjConfigLine *gj_config_handler_find_string (GjConfigSection *section, gchar *key);

GjConfigFile *gj_config_handler_new (void)
{
	GjConfigFile *cfg;

	cfg = g_malloc0 (sizeof (GjConfigFile));

	return cfg;
}

GjConfigFile *gj_config_handler_open_file (gchar *filename)
{
	GIOChannel *ch = NULL;
	GIOStatus status = 0;
	GError *error = NULL;

	gchar *buffer = NULL;
	guint length = 0;
	gchar **lines = NULL;
	gchar *tmp = NULL;
	gint i = 0;

	GjConfigFile *cfg;
	GjConfigSection *section = NULL;

	if (g_file_test (filename, G_FILE_TEST_EXISTS) == FALSE) {
		g_message ("gj_config_handler_open_file: file:'%s' does not exist", filename);
		return NULL;
	}

	if ((ch = g_io_channel_new_file (filename, "r", NULL)) == NULL) {
		g_warning ("gj_config_handler_open_file: could not open file:'%s'", filename);
		return NULL;
	}

	if ((status = g_io_channel_read_to_end (ch, &buffer, &length, &error)) != G_IO_STATUS_NORMAL) {
		g_warning ("gj_config_handler_open_file: status is:%d, error:%d->'%s'", 
			   status, error?error->code:0, error?error->message:"");
		return NULL;
	}

	/* close channel */
	g_io_channel_shutdown (ch, TRUE, NULL);

	if (buffer == NULL || strlen (buffer) < 1) {
		g_warning ("gj_config_handler_open_file: buffer was NULL or length was < 1");
		return NULL;
	}

	cfg = g_malloc0 (sizeof (GjConfigFile));
	lines = g_strsplit (buffer, "\n", 0);
	i = 0;

	while (lines[i]) {
		if (lines[i][0] == '[') {
			if ((tmp = strchr (lines[i], ']'))) {
				*tmp = '\0';
				section = gj_config_handler_create_section (cfg, &lines[i][1]);
			}
		} else if (lines[i][0] != '#' && section) {
			if ((tmp = strchr (lines[i], '='))) {
				*tmp = '\0';
				tmp++;
				gj_config_handler_create_string (section, lines[i], tmp);
			}
		}
		i++;
	}

	g_free (buffer);

	if (lines != NULL) {
		g_strfreev (lines);
	}

	return cfg;
}

gchar *gj_config_handler_get_default_filename (void)
{
	static gchar *filename = NULL;

	if (!filename) {
		filename = g_strconcat (g_get_home_dir (), FILE_CONFIG, NULL);
	}

	return filename;
}

GjConfigFile *gj_config_handler_open_default_file (void)
{
	GjConfigFile *ret;

	ret = gj_config_handler_open_file (gj_config_handler_get_default_filename ());

	if (!ret) {
		ret = gj_config_handler_new ();
	}

	return ret;
}

gboolean gj_config_handler_write_file (GjConfigFile *cfg, gchar *filename)
{
	GIOChannel *ch = NULL;
	GList *section_list = NULL;
	GList *line_list = NULL;
	GjConfigSection *section = NULL;
	GjConfigLine *line = NULL;

	if ((ch = g_io_channel_new_file (filename, "w", NULL)) == NULL) {
		g_warning ("gj_config_handler_write_file: could not open file:'%s'", filename);
		return FALSE;
	}

	section_list = cfg->sections;
	while (section_list) {
		section = (GjConfigSection *) section_list->data;
		if (section->lines) {
			gchar *str = NULL;
			guint bytes = 0;
			
			if ((str = g_strdup_printf ("[%s]\n", section->name)) != NULL) {
				g_io_channel_write_chars (ch, str, -1, &bytes, NULL);
				g_message ("gj_config_handler_write_file: wrote:%.6d bytes to file:'%s'", bytes, filename);
				g_free (str);
			}
			
			line_list = section->lines;
			while (line_list) {
				str = NULL;
				bytes = 0;
				line = (GjConfigLine *) line_list->data;
				
				if ((str = g_strdup_printf ("%s=%s\n", line->key, line->value)) != NULL) {
					g_io_channel_write_chars (ch, str, -1, &bytes, NULL);
					g_message ("gj_config_handler_write_file: wrote:%.6d bytes to file:'%s'", bytes, filename);
					g_free (str);
				}
				
				line_list = g_list_next (line_list);
			}
			
			g_io_channel_write_chars (ch, "\n", 1, &bytes, NULL);
		}
		
		section_list = g_list_next (section_list);
	}
	
	/* close channel */
	g_io_channel_shutdown (ch, TRUE, NULL);

	return TRUE;
}

gboolean gj_config_handler_write_default_file (GjConfigFile *cfg)
{
	return gj_config_handler_write_file (cfg, gj_config_handler_get_default_filename ());
}

gboolean gj_config_handler_read_string (GjConfigFile *cfg, gchar *section, gchar *key, gchar ** value)
{
	GjConfigSection *sect;
	GjConfigLine *line;

	if (! (sect = gj_config_handler_find_section (cfg, section))) {
		return FALSE;
	}

	if (! (line = gj_config_handler_find_string (sect, key))) {
		return FALSE;
	}

	*value = g_strdup (line->value);
	return TRUE;
}

gboolean gj_config_handler_read_int (GjConfigFile *cfg, gchar *section, gchar *key, gint *value)
{
	gchar *str;

	if (!gj_config_handler_read_string (cfg, section, key, &str)) {
		return FALSE;
	}

	*value = atoi (str);
	g_free (str);

	return TRUE;
}

gboolean gj_config_handler_read_int8 (GjConfigFile *cfg, gchar *section, gchar *key, gint8 *value)
{
	gchar *str;

	if (!gj_config_handler_read_string (cfg, section, key, &str)) {
		return FALSE;
	}

	*value = atoi (str);
	g_free (str);

	return TRUE;
}

gboolean gj_config_handler_read_boolean (GjConfigFile *cfg, gchar *section, gchar *key, gboolean *value)
{
	gchar *str;

	if (!gj_config_handler_read_string (cfg, section, key, &str)) {
		return FALSE;
	}

	if (!strcasecmp (str, "TRUE")) {
		*value = TRUE;
	} else {
		*value = FALSE; 
	}

	g_free (str);
	return TRUE;
}

gboolean gj_config_handler_read_float (GjConfigFile *cfg, gchar *section, gchar *key, gfloat *value)
{
	char *str, *locale;

	if (!gj_config_handler_read_string (cfg, section, key, &str)) {
		return FALSE;
	}

	locale = setlocale (LC_NUMERIC, "C");
	*value = strtod (str, NULL);
	setlocale (LC_NUMERIC, locale);
	g_free (str);

	return TRUE;
}

gboolean gj_config_handler_read_double (GjConfigFile *cfg, gchar *section, gchar *key, gdouble *value)
{
	char *str, *locale;

	if (!gj_config_handler_read_string (cfg, section, key, &str)) {
		return FALSE;
	}

	locale = setlocale (LC_NUMERIC, "C");
	*value = strtod (str, NULL);
	setlocale (LC_NUMERIC, locale);
	g_free (str);

	return TRUE;
}

void gj_config_handler_write_string (GjConfigFile *cfg, gchar *section, gchar *key, gchar *value)
{
	GjConfigSection *sect;
	GjConfigLine *line;

	sect = gj_config_handler_find_section (cfg, section);

	if (!sect) {
		sect = gj_config_handler_create_section (cfg, section);
	}

	if ((line = gj_config_handler_find_string (sect, key))) {
		g_free (line->value);
		line->value = g_strchug (g_strchomp (g_strdup (value?value:"")));
	} else {
		gj_config_handler_create_string (sect, key, value?value:"");
	}
}

void gj_config_handler_write_int (GjConfigFile *cfg, gchar *section, gchar *key, gint value)
{
	gchar *strvalue;

	strvalue = g_strdup_printf ("%d", value);
	gj_config_handler_write_string (cfg, section, key, strvalue);
	g_free (strvalue);
}

void gj_config_handler_write_int8 (GjConfigFile *cfg, gchar *section, gchar *key, gint8 value)
{
	gchar *strvalue;

	strvalue = g_strdup_printf ("%d", value);
	gj_config_handler_write_string (cfg, section, key, strvalue);
	g_free (strvalue);
}

void gj_config_handler_write_boolean (GjConfigFile *cfg, gchar *section, gchar *key, gboolean value)
{
	if (value) {
		gj_config_handler_write_string (cfg, section, key, "TRUE");
	} else {
		gj_config_handler_write_string (cfg, section, key, "FALSE");
	}
}

void gj_config_handler_write_float (GjConfigFile *cfg, gchar *section, gchar *key, gfloat value)
{
	char *strvalue, *locale;

	locale = setlocale (LC_NUMERIC, "C");
	strvalue = g_strdup_printf ("%g", value);
	setlocale (LC_NUMERIC, locale);
	gj_config_handler_write_string (cfg, section, key, strvalue);
	g_free (strvalue);
}

void gj_config_handler_write_double (GjConfigFile *cfg, gchar *section, gchar *key, gdouble value)
{
	char *strvalue, *locale;

	locale = setlocale (LC_NUMERIC, "C");
	strvalue = g_strdup_printf ("%g", value);
	setlocale (LC_NUMERIC, locale);
	gj_config_handler_write_string (cfg, section, key, strvalue);
	g_free (strvalue);
}

void gj_config_handler_remove_key (GjConfigFile *cfg, gchar *section, gchar *key)
{
	GjConfigSection *sect;
	GjConfigLine *line;

	if ((sect = gj_config_handler_find_section (cfg, section)) != NULL) {
		if ((line = gj_config_handler_find_string (sect, key)) != NULL) {
			g_free (line->key);
			g_free (line->value);
			g_free (line);
			sect->lines = g_list_remove (sect->lines, line);
		}
	}
}

void gj_config_handler_free (GjConfigFile *cfg)
{
	GjConfigSection *section;
	GjConfigLine *line;
	GList *section_list, *line_list;

	section_list = cfg->sections;

	while (section_list) {
		section = (GjConfigSection *) section_list->data;
		g_free (section->name);
		
		line_list = section->lines;

		while (line_list) {
			line = (GjConfigLine *) line_list->data;
			g_free (line->key);
			g_free (line->value);
			g_free (line);
			line_list = g_list_next (line_list);
		}

		g_list_free (section->lines);
		g_free (section);
		
		section_list = g_list_next (section_list);
	}

	g_list_free (cfg->sections);
	g_free (cfg);
}

GjConfigSection *gj_config_handler_create_section (GjConfigFile *cfg, gchar *name)
{
	GjConfigSection *section;

	section = g_malloc0 (sizeof (GjConfigSection));
	section->name = g_strdup (name);
	cfg->sections = g_list_append (cfg->sections, section);

	return section;
}

GjConfigLine *gj_config_handler_create_string (GjConfigSection *section, gchar *key, gchar *value)
{
	GjConfigLine *line;

	line = g_malloc0 (sizeof (GjConfigLine));
	line->key = g_strchug (g_strchomp (g_strdup (key)));
	line->value = g_strchug (g_strchomp (g_strdup (value)));
	section->lines = g_list_append (section->lines, line);

	return line;
}

GjConfigSection *gj_config_handler_find_section (GjConfigFile *cfg, gchar *name)
{
	GjConfigSection *section;
	GList *list;

	list = cfg->sections;
	while (list) {
		section = (GjConfigSection *) list->data;

		if (!strcasecmp (section->name, name)) {
			return section;
		}

		list = g_list_next (list);
	}
	return NULL;
}

GjConfigLine *gj_config_handler_find_string (GjConfigSection *section, gchar *key)
{
	GjConfigLine *line;
	GList *list;

	list = section->lines;

	while (list) {
		line = (GjConfigLine *) list->data;

		if (!strcasecmp (line->key, key)) {
			return line;
		}

		list = g_list_next (list);
	}

	return NULL;
}

#ifdef __cplusplus
}
#endif
