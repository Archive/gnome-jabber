/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gdk/gdk.h>

#ifdef HAVE_XSS

# include <X11/X.h>
# include <X11/Xlib.h>

# include <X11/extensions/scrnsaver.h>
# include <gdk/gdkx.h>


gint gj_autoaway_query_ss_idle_time (Display *d);


static Display *display = NULL;


#endif /* HAVE_XSS */

#include "gj_autoaway.h"
#include "gj_gtk_roster.h"


void gj_autoaway_check ();
gboolean gj_autoaway_cb (gpointer data);


/*
 * Used by most functions 
 */
static GjAutoAwayCallback timeout_cb = NULL;
static guint timeout_id = -1;
static gpointer timeout_user_data = NULL;
 

/*
 * code
 */
gboolean gj_autoaway_init (GjAutoAwayCallback cb, gpointer user_data)
{
#ifdef HAVE_XSS

	gint unused;
	Bool useXss = False;

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	/*
	 *  Find out whether there actually is a server on the other side...
	 */
	if (! (display = XOpenDisplay (0))) /* = intended */ {
		g_warning ("%s: couldn't connect to %s", __FUNCTION__, XDisplayName (0));
		return FALSE;
	}

	if (! (useXss = XScreenSaverQueryExtension (display, &unused, &unused))) {
		g_warning ("%s: could not initialise XScreenSaver", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: using XSS for autoaway time", __FUNCTION__);

#else /* HAVE_XSS */

	g_message ("%s: no way to know autoaway time", __FUNCTION__);
	return FALSE;

#endif /* HAVE_XSS */

	/* set recursive function */
	timeout_cb = cb;
	timeout_id = g_timeout_add (1000, gj_autoaway_cb, NULL);
	timeout_user_data = user_data;

	return TRUE;
}

gboolean gj_autoaway_term ()
{
	if (timeout_id != -1) {
		g_source_remove (timeout_id);
		timeout_id = -1;
	}

	return TRUE;
}

#ifndef G_OS_WIN32

void gj_autoaway_check ()
{
# ifdef HAVE_XSS

	gint time = 0; /* in ms */

	/* find out current idle time */
	time = gj_autoaway_query_ss_idle_time (display);

	if (timeout_cb != NULL) {
		(timeout_cb) (time, timeout_user_data); 
	}

# endif /* HAVE_XSS */
}

# ifdef HAVE_XSS 

gint gj_autoaway_query_ss_idle_time (Display *d)
{
	Time idleTime = 0; /* millisecs since last input event */

	static XScreenSaverInfo *mitInfo = 0; 

	if (!mitInfo) {
		mitInfo = XScreenSaverAllocInfo ();
	}
  
	XScreenSaverQueryInfo (d, DefaultRootWindow (d), mitInfo);
	idleTime = mitInfo->idle;

	return idleTime;
}

# endif /* HAVE_XSS */

#else /* G_OS_WIN32 */

void gj_autoaway_check ()
{
	/* cant do anything yet */
}

#endif /* G_OS_WIN32 */

gboolean gj_autoaway_cb (gpointer data)
{
	/* return FALSE to stop 
	   calling this function */

	/* do check */
	gj_autoaway_check ();

	return TRUE;
}

#ifdef __cplusplus
}
#endif
