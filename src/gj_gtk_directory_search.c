/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include <unistd.h>

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_search.h"
#include "gj_iq_requests.h"
#include "gj_support.h"
#include "gj_parser.h"

#include "gj_gtk_directory_search.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_support.h"


enum {
	COL_AGENT_JID,
	COL_AGENT_NAME,
	COL_AGENT_VISIBLE,
	COL_AGENT_COUNT
};


enum {
	DIRECTORY_SEARCH_RESPONSE_BACK = 1,
	DIRECTORY_SEARCH_RESPONSE_FORWARD
};


struct  {
	GtkWidget *window;
  
	GtkWidget *notebook;

	GtkWidget *progressbar_probing;

	GtkWidget *label_service;
	GtkWidget *label_instructions;

	GtkWidget *label_first;
	GtkWidget *label_last;
	GtkWidget *label_nick;
	GtkWidget *label_email;

	GtkWidget *entry_service;

	GtkWidget *entry_first;
	GtkWidget *entry_last;
	GtkWidget *entry_nick;
	GtkWidget *entry_email;

	GtkWidget *progressbar_searching;

	GtkWidget *treeview;

	GtkWidget *button_forward;
	GtkWidget *button_back;
	GtkWidget *button_close;

} ctlDS;


struct {
	gchar *service;
  
	gboolean allows_first;
	gboolean allows_last;
	gboolean allows_nick;
	gboolean allows_email;

	gint progress_timer_probing;
	gint progress_timer_searching;

} optDS;


/* 
 * member functions 
 */

/* gets */
gchar *gj_gtk_ds_get_service ();

gboolean gj_gtk_ds_get_allows_first ();
gboolean gj_gtk_ds_get_allows_last ();
gboolean gj_gtk_ds_get_allows_nick ();
gboolean gj_gtk_ds_get_allows_email ();

/* sets */
gboolean gj_gtk_ds_set_service (gchar *service);

gboolean gj_gtk_ds_set_allows_first (gboolean allows);
gboolean gj_gtk_ds_set_allows_last (gboolean allows);
gboolean gj_gtk_ds_set_allows_nick (gboolean allows);
gboolean gj_gtk_ds_set_allows_email (gboolean allows);

/* 
 * window functions 
 */
gboolean gj_gtk_ds_setup ();

/* checks */
void gj_gtk_ds_page_one_changed ();
void gj_gtk_ds_page_three_changed ();

gboolean gj_gtk_ds_button_update ();

/* page functions */
gboolean gj_gtk_ds_page_check (gint page);
gboolean gj_gtk_ds_page_action (gint page);

gboolean gj_gtk_ds_page_forward ();
gboolean gj_gtk_ds_page_back ();

gboolean gj_gtk_ds_check ();

/* 
 * model functions 
 */
gboolean gj_gtk_ds_model_setup ();
gboolean gj_gtk_ds_model_add_columns (GtkTreeView *view);
gboolean gj_gtk_ds_model_populate ();
gboolean gj_gtk_ds_model_get_selection (gchar **jid, gchar **name);


/* 
 * internal callbacks 
 */
gint gj_gtk_ds_progress_timer_probing (gpointer data);
gint gj_gtk_ds_progress_timer_searching (gpointer data);

void gj_gtk_ds_search_requirements_response (search s, gint error_code, gchar *error_reason);
void gj_gtk_ds_search_response (search s, gint error_code, gchar *error_reason);

/* 
 * gui callbacks 
 */
void on_notebook_ds_change_current_page (GtkNotebook *notebook, gint arg1, gpointer user_data);

void on_treeview_ds_selection_changed (GtkTreeSelection *treeselection, gpointer user_data);

void on_directory_search_response (GtkDialog *dialog, gint response, gpointer user_data);


/*
 * member functions
 */

/* gets */
gchar *gj_gtk_ds_get_service ()
{
	return optDS.service;
}

gboolean gj_gtk_ds_get_allows_first ()
{
	return optDS.allows_first;
}

gboolean gj_gtk_ds_get_allows_last ()
{
	return optDS.allows_last;
}

gboolean gj_gtk_ds_get_allows_nick ()
{
	return optDS.allows_nick;
}

gboolean gj_gtk_ds_get_allows_email ()
{
	return optDS.allows_email;
}

/* sets */
gboolean gj_gtk_ds_set_service (gchar *service)
{
	if (service == NULL) {
		g_warning ("gj_gtk_ds_set_service: service was NULL");
		return FALSE;
	}
  
	if (optDS.service != NULL) {
		g_free (optDS.service);
		optDS.service = NULL;
	}

	optDS.service = g_strdup (service);
	return TRUE;
}

gboolean gj_gtk_ds_set_allows_first (gboolean allows)
{
	optDS.allows_first = allows;
	return TRUE;
}

gboolean gj_gtk_ds_set_allows_last (gboolean allows)
{
	optDS.allows_last = allows;
	return TRUE;
}

gboolean gj_gtk_ds_set_allows_nick (gboolean allows)
{
	optDS.allows_nick = allows;
	return TRUE;
}

gboolean gj_gtk_ds_set_allows_email (gboolean allows)
{
	optDS.allows_email = allows;
	return TRUE;
}

/*
 * window functions
 */
gboolean gj_gtk_ds_load (gchar *jid)
{
	GladeXML *xml = NULL;

	if (ctlDS.window != NULL) {
		gtk_window_present (GTK_WINDOW (ctlDS.window));
		return TRUE;
	}

	if ((xml = glade_xml_new (gnome_jabber_get_glade_file (),
				  "directory_search",
				  NULL)) == NULL) {
		g_warning ("gj_gtk_ds_load: couldn't open glade file at root 'directory_search'");
		return FALSE;
	}
  
	ctlDS.window = glade_xml_get_widget (xml,"directory_search");

	ctlDS.notebook = glade_xml_get_widget (xml,"notebook_ds");

	ctlDS.progressbar_probing = glade_xml_get_widget (xml,"progressbar_ds_probing");

	ctlDS.label_service = glade_xml_get_widget (xml,"label_ds_service");
	ctlDS.label_instructions = glade_xml_get_widget (xml,"label_ds_instructions");
	ctlDS.label_first = glade_xml_get_widget (xml,"label_ds_first");
	ctlDS.label_last = glade_xml_get_widget (xml,"label_ds_last");
	ctlDS.label_nick = glade_xml_get_widget (xml,"label_ds_nick");
	ctlDS.label_email = glade_xml_get_widget (xml,"label_ds_email");

	ctlDS.entry_service = glade_xml_get_widget (xml,"entry_ds_service");

	ctlDS.entry_first = glade_xml_get_widget (xml,"entry_ds_first");
	ctlDS.entry_last = glade_xml_get_widget (xml,"entry_ds_last");
	ctlDS.entry_nick = glade_xml_get_widget (xml,"entry_ds_nick");
	ctlDS.entry_email = glade_xml_get_widget (xml,"entry_ds_email");

	ctlDS.progressbar_searching = glade_xml_get_widget (xml,"progressbar_ds_searching");

	ctlDS.treeview = glade_xml_get_widget (xml,"treeview_ds");

	ctlDS.button_forward = glade_xml_get_widget (xml,"button_ds_forward");
	ctlDS.button_back = glade_xml_get_widget (xml,"button_ds_back");
	ctlDS.button_close = glade_xml_get_widget (xml,"button_ds_close");
  
#ifndef G_OS_WIN32
	glade_xml_signal_autoconnect (xml);
#else
	gtk_signal_connect (GTK_OBJECT (ctlDS.window), "response",
			    GTK_SIGNAL_FUNC (on_directory_search_response),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (ctlDS.notebook), "change_current_page",
			    GTK_SIGNAL_FUNC (on_notebook_ds_change_current_page),
			    NULL);
#endif
	g_object_unref (G_OBJECT (xml));

	/* setup controls */
	gj_gtk_ds_setup ();
	gj_gtk_ds_model_setup ();

	/* start on page 0 */
	gj_gtk_ds_page_action (0);

	return TRUE;
}

gboolean gj_gtk_ds_unload ()
{
  
	if (ctlDS.window == NULL)
		return TRUE;

	/* we do this first because if it the window is destroyed before the timeout, 
	   it may try and use a widget which doesnt exist. */
	if (optDS.progress_timer_probing != -1) {
		gtk_timeout_remove (optDS.progress_timer_probing);
		optDS.progress_timer_probing = -1;
	}

	if (optDS.progress_timer_searching != -1) {
		gtk_timeout_remove (optDS.progress_timer_searching);
		optDS.progress_timer_searching = -1;
	}

	gtk_widget_destroy (GTK_WIDGET (ctlDS.window));

	ctlDS.window = NULL;
  
	ctlDS.notebook = NULL;

	ctlDS.progressbar_probing = NULL;

	ctlDS.label_service = NULL;

	ctlDS.label_first = NULL;
	ctlDS.label_last = NULL;
	ctlDS.label_nick = NULL;
	ctlDS.label_email = NULL;

	ctlDS.entry_first = NULL;
	ctlDS.entry_last = NULL;
	ctlDS.entry_nick = NULL;
	ctlDS.entry_email = NULL;

	ctlDS.progressbar_searching = NULL;

	ctlDS.treeview = NULL;

	ctlDS.button_forward = NULL;
	ctlDS.button_back = NULL;
	ctlDS.button_close = NULL;

	g_free (optDS.service);

	optDS.allows_first = FALSE;
	optDS.allows_last = FALSE;
	optDS.allows_nick = FALSE;
	optDS.allows_email = FALSE;

	return TRUE;
}

gboolean gj_gtk_ds_setup ()
{
	/* set up local vars */
	optDS.progress_timer_probing = -1;
	optDS.progress_timer_searching = -1;

	optDS.allows_first = FALSE;
	optDS.allows_last = FALSE;
	optDS.allows_nick = FALSE;
	optDS.allows_email = FALSE;

	return TRUE;
}

gboolean gj_gtk_ds_model_setup ()
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection * selection = NULL;

	if ((view = GTK_TREE_VIEW (ctlDS.treeview)) == NULL) {
		g_warning ("gj_gtk_ds_model_setup: view was NULL");
		return FALSE;
	}

	if ((selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view))) == NULL) {
		g_warning ("gj_gtk_ds_model_setup: selection was NULL");
		return FALSE;
	}

	/* store */
	store =  gtk_list_store_new (COL_AGENT_COUNT,
				     G_TYPE_STRING,    /* jid */
				     G_TYPE_STRING,    /* name */
				     G_TYPE_BOOLEAN);  /* visible */
  
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	g_signal_connect (G_OBJECT (selection), "changed", G_CALLBACK (on_treeview_ds_selection_changed), NULL);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	/* properties */
	gtk_tree_view_set_enable_search (view,TRUE);
	gtk_tree_view_set_search_column (view,COL_AGENT_NAME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, TRUE);
	gtk_tree_view_expand_all (view);

	/* columns */
	gj_gtk_ds_model_add_columns (view);

	/* clean up */  
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

gboolean gj_gtk_ds_model_add_columns (GtkTreeView *view)
{
	GtkCellRenderer *renderer = NULL;
	GtkTreeViewColumn *column = NULL; 
	guint col_offset = 0;

	if (GTK_TREE_VIEW (view) == NULL) {
		g_warning ("gj_gtk_ds_model_add_columns: view was NULL");
		return FALSE;
	}

	/* COL_AGENT_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, "Name",
								  renderer, 
								  "text", COL_AGENT_NAME,
								  NULL);
  
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)COL_AGENT_NAME);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_AGENT_NAME);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	return TRUE;
}

gboolean gj_gtk_ds_model_get_selection (gchar **jid, gchar **name)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;
  
	if (jid == NULL || name == NULL) {
		g_warning ("gj_gtk_ds_model_get_selection: jid, name or service pointers passed were NULL");
		return FALSE;
	}

	if ((view = GTK_TREE_VIEW (ctlDS.treeview)) == NULL) {
		g_warning ("gj_gtk_ds_model_get_selection: view was NULL");
		return FALSE;
	}

	if ((selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view))) == NULL) {
		g_warning ("gj_gtk_ds_model_get_selection: selection was NULL");
		return FALSE;
	}

	selection = gtk_tree_view_get_selection (view);

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return FALSE;
	}
  
	gtk_tree_model_get (model, &iter, 
			    COL_AGENT_JID, jid,
			    COL_AGENT_NAME, name,
			    -1);     

	return TRUE;
}

gboolean gj_gtk_ds_model_populate ()
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gint i = 0;
	GList *agents = NULL;
  
	if ((agents = gj_roster_get_all_agents_by_name ()) == NULL) {
		g_warning ("gj_gtk_ds_model_populate: agent list was NULL");
		return FALSE;
	}

	if ((view = GTK_TREE_VIEW (ctlDS.treeview)) == NULL) {
		g_warning ("gj_gtk_ds_model_populate: view was NULL");
		return FALSE;
	}

	if ((store = GTK_LIST_STORE (gtk_tree_view_get_model (view))) == NULL) {
		g_warning ("gj_gtk_ds_model_populate: store was NULL");
		return FALSE;
	}

	/* clear list if already pressed */
	gtk_list_store_clear (store);

	for (i=0;i<g_list_length (agents);i++) {
		gchar *jid = NULL;

		if ((jid = (gchar*) g_list_nth_data (agents, i)) == NULL) {
			g_warning ("gj_gtk_ds_model_populate: list data at index:%d was NULL", i);
			continue;
		}
     
		/* add */
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,    
				    COL_AGENT_JID, jid,
				    COL_AGENT_NAME, jid?jid:"no JID",
				    COL_AGENT_VISIBLE, TRUE,
				    -1);

		g_message ("gj_gtk_ds_model_populate: #%d, jid:'%s'", i, jid);
	}
}

/* this checks to see if we can progress a step, 
   if not, we return FALSE */
gboolean gj_gtk_ds_page_check (gint page)
{
	switch (page) {
	case 0: {
		G_CONST_RETURN gchar *service = NULL;
		service = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_service));
		
		if (service == NULL || g_utf8_strlen (service, -1) < 1) {
			return FALSE;
		}
		
		break;
	}

	case 1: {
      		break;
	}

	case 2:	{
		/* 	G_CONST_RETURN gchar *name = NULL; */
		/* 	name = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_name)); */
		
		/* 	if (name == NULL || strlen (name) < 1) */
		/* 	    g_message ("gj_gtk_ds_page_check: name was NULL or length was < 1"); */
		/* 	    return FALSE; */
		/* 	  }	 */
		
		/* 	gj_gtk_ds_set_name ((gchar*)name);	 */
		break;
	}
			
	case 3: {
		break;
	}

	case 4:
	case 5: {
		break;
	}

	default: {
		g_warning ("gj_gtk_ds_page_check: page:%d is not valid, shouldn't be here, default reached.", page);
		return FALSE;
	}
	}

	return TRUE;
}

gboolean gj_gtk_ds_page_action (gint page)
{
	switch (page) {
	case 0:
		break;

	case 1:	{
		G_CONST_RETURN gchar *service = NULL;
		
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), TRUE);
		
		/* add timer for progress bar */
		optDS.progress_timer_probing = gtk_timeout_add (75, gj_gtk_ds_progress_timer_probing, NULL);
		
		service = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_service));
		gtk_label_set_text (GTK_LABEL (ctlDS.label_service), service);
		
		/* request search requirements */
		searchRequestRequirements (service, gj_gtk_ds_search_requirements_response);  

		break;
	}
		
	case 2: {
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), TRUE);
		break;
	}

	case 3:	{
		G_CONST_RETURN gchar *service = NULL;
		G_CONST_RETURN gchar *first = NULL;
		G_CONST_RETURN gchar *last = NULL;
		G_CONST_RETURN gchar *nick = NULL;
		G_CONST_RETURN gchar *email = NULL;
		
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), TRUE);
		
		/* add timer for progress bar */
		optDS.progress_timer_searching = gtk_timeout_add (75, gj_gtk_ds_progress_timer_searching, NULL);
		
		/* get information and post request */
		service = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_service));
		first = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_first));
		last = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_last));
		nick = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_nick));
		email = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_email));
		
		searchRequest ((gchar*)service, gj_gtk_ds_search_response, 
			       (gchar*)first, (gchar*)last, (gchar*)nick, (gchar*)email);
		
		break;
	}

	case 4: {
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), FALSE); 
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), FALSE);
		break;
	}

	default: {
		g_warning ("gj_gtk_ds_page_action: page:%d is not valid, shouldn't be here, default reached.", page);
		return FALSE;
	}
	}

	return TRUE;
}

gboolean gj_gtk_ds_page_forward () {
	gint index = 0;

	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (ctlDS.notebook))) < 0) {
		g_warning ("gj_gtk_ds_page_forward: error, -1 returned, meaning NO notebook pages.  This should NEVER happen");
		return FALSE;
	} 

	/* what do we want to do here? check for the right information at each stage?? */
	gtk_notebook_next_page (GTK_NOTEBOOK (ctlDS.notebook));
	gj_gtk_ds_button_update ();
	
	/* page should now be updated */
	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (ctlDS.notebook))) < 0) {
		g_warning ("gj_gtk_ds_page_forward: error, -1 returned, meaning NO notebook pages.  This should NEVER happen");
		return FALSE;
	} 
	
	/* do action for this page... */
	gj_gtk_ds_page_action (index);

	return TRUE;
}

gboolean gj_gtk_ds_page_back ()
{
	gtk_notebook_prev_page (GTK_NOTEBOOK (ctlDS.notebook));
	gj_gtk_ds_button_update ();
  
	return TRUE;
}

/* checks */
void gj_gtk_ds_page_one_changed ()
{
	gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), gj_gtk_ds_page_check (0));
}

void gj_gtk_ds_page_three_changed ()
{
	/*   gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), gj_gtk_ds_page_check (2)); */
}

gboolean gj_gtk_ds_button_update ()
{
	gint index = 0;
	gint count = 0;

	if ((index = gtk_notebook_get_current_page (GTK_NOTEBOOK (ctlDS.notebook))) < 0) {
		g_warning ("gj_gtk_ds_button_update: error, -1 returned, meaning NO notebook pages.  This should NEVER happen");
		return FALSE;
	}

	/*
	 * THIS function is in the documentation but not in my /usr/include/gtk header file.
	 */

	/*  count = gtk_notebook_get_n_pages (GTK_NOTEBOOK (ctlDS.notebook));*/   
	count = 5;
	if (count < 0 || count <= index) {
		g_warning ("gj_gtk_ds_button_update: count:%d is < index:%d", count, index);
		return FALSE;
	} 

	if (index < 1) {
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), FALSE);
      
		if (count > 1) {
			gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), TRUE);
		}
	} else if (index == (count - 1)) {
		gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), FALSE);
		
		if (count > 1) {
			gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), TRUE);
		}
		
		/* show finish button if at the end */
		if (index == (count -1)) {
			gtk_widget_hide (GTK_WIDGET (ctlDS.button_forward));
			gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_back), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (ctlDS.button_forward), TRUE);

			/* hide finish button if near the end going back */
			if (index == (count -2)) {
				gtk_widget_show (GTK_WIDGET (ctlDS.button_forward));
			}
		}
	}

	return TRUE;
}

gboolean gj_gtk_ds_check ()
{
	gboolean passed = FALSE;
	
	G_CONST_RETURN gchar *service = NULL;
	G_CONST_RETURN gchar *first = NULL;
	G_CONST_RETURN gchar *last = NULL;
	G_CONST_RETURN gchar *nick = NULL;
	G_CONST_RETURN gchar *email = NULL;
	
	service = gtk_label_get_text (GTK_LABEL (ctlDS.label_service));
	first = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_first));
	last = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_last));
	email = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_email));
	nick = gtk_entry_get_text (GTK_ENTRY (ctlDS.entry_nick));
	
#if 0
	/* do checks */
	if (service == NULL || g_utf8_strlen (service, -1) < 1) {
		/* service not entered */
		gj_gtk_msg_new ("Details incomplete.", 
				"Register Service", 
				"Service was not specified.",
				TRUE,
				GJ_GTK_MSG_BUTTON_OK | GJ_GTK_MSG_ICON_EXCLAIMATION,
				(MsgCb)NULL,
				NULL);
		
		passed = FALSE;
	} else if (gj_gtk_ds_get_allows_first () == TRUE && (first == NULL || g_utf8_strlen (first, -1) < 1)) {
		/* first not entered */
		gj_gtk_msg_new ("Details incomplete.", 
				"Register Service", 
				"First name was not specified.",
				TRUE,
				GJ_GTK_MSG_BUTTON_OK | GJ_GTK_MSG_ICON_EXCLAIMATION,
				(MsgCb)NULL,
				NULL);
		
		passed = FALSE;
	} else if (gj_gtk_ds_get_allows_last () == TRUE && (last == NULL || g_utf8_strlen (last, -1) < 1)) {
		/* lasts not entered */
		gj_gtk_msg_new ("Details incomplete.", 
				"Register Service", 
				"Last name was not specified.",
				TRUE,
				GJ_GTK_MSG_BUTTON_OK | GJ_GTK_MSG_ICON_EXCLAIMATION,
				(MsgCb)NULL,
				NULL);
		
		passed = FALSE;
	} else if (gj_gtk_ds_get_allows_nick () == TRUE && (nick == NULL || g_utf7_strlen (nick, -1) < 1)) {
		/* nick not entered */
		gj_gtk_msg_new ("Details incomplete.", 
				"Register Service", 
				"Nick was not specified.",
				TRUE,
				GJ_GTK_MSG_BUTTON_OK | GJ_GTK_MSG_ICON_EXCLAIMATION,
				(MsgCb)NULL,
				NULL);
		
		passed = FALSE;
	} else if (gj_gtk_ds_get_allows_email () == TRUE && (email == NULL || g_utf8_strlen (email, -1) < 1)) {
		/* passwords not entered */
		gj_gtk_msg_new ("Details incomplete.", 
				"Register Service", 
				"Email address was not specified.",
				TRUE,
				GJ_GTK_MSG_BUTTON_OK | GJ_GTK_MSG_ICON_EXCLAIMATION,
				(MsgCb)NULL,
				NULL);
		
		passed = FALSE;
	} else {
		/* ok */
		passed = TRUE;
	}
#endif
	
	return passed;
}

void gj_gtk_ds_search_requirements_response (search s, gint error_code, gchar *error_reason)
{
	g_message ("gj_gtk_ds_search_requirements_response: received response.");
	
	/* stop progress timer */
	if (optDS.progress_timer_probing != -1) {
		gtk_timeout_remove (optDS.progress_timer_probing);
		optDS.progress_timer_probing = -1;
	}
	
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (ctlDS.progressbar_probing), (gdouble) 1);
	gj_gtk_ds_page_forward ();
	
	if (s == NULL) {
		g_warning ("gj_gtk_ds_search_requirements_response: s was NULL");
		return;
	}
	
	if (error_code != 0 || error_reason != NULL) {
		gj_gtk_show_error (_("Correct the error and try again."),
				   error_code,
				   error_reason);
		
		/* clean up */
		searchFree (s);
		
		return;
	}
	
	gtk_label_set_text (GTK_LABEL (ctlDS.label_instructions), searchGetInstructions (s));
	
	if (searchGetAllowsFirst (s) == TRUE) {
		gtk_widget_show (GTK_WIDGET (ctlDS.label_first));
		gtk_widget_show (GTK_WIDGET (ctlDS.entry_first));
		gj_gtk_ds_set_allows_first (TRUE);
	}
	
	if (searchGetAllowsLast (s) == TRUE) {
		gtk_widget_show (GTK_WIDGET (ctlDS.label_last));
		gtk_widget_show (GTK_WIDGET (ctlDS.entry_last));
		gj_gtk_ds_set_allows_last (TRUE);
	}
	
	if (searchGetAllowsNick (s) == TRUE) {
		gtk_widget_show (GTK_WIDGET (ctlDS.label_nick));
		gtk_widget_show (GTK_WIDGET (ctlDS.entry_nick));
		gj_gtk_ds_set_allows_nick (TRUE);
	}
	
	if (searchGetAllowsEmail (s) == TRUE) {
		gtk_widget_show (GTK_WIDGET (ctlDS.label_email));
		gtk_widget_show (GTK_WIDGET (ctlDS.entry_email));
		gj_gtk_ds_set_allows_email (TRUE);
	}
	
	/* clean up */
	searchFree (s);
}

void gj_gtk_ds_search_response (search s, gint error_code, gchar *error_reason)
{
	g_message ("gj_gtk_ds_search_response: service response");
	
	/* stop progress timer */
	if (optDS.progress_timer_searching != -1) {
		gtk_timeout_remove (optDS.progress_timer_searching);
		optDS.progress_timer_searching = -1;
	}
	
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (ctlDS.progressbar_searching), (gdouble) 1);
	gj_gtk_ds_page_forward ();
	
	if (error_code == 0 && error_reason == NULL) {
	} else {
		gchar *details = NULL;
		
		details = g_strdup_printf ("Error %d, %s", error_code, error_reason);
		
#if 0
		/* passwords not entered */
		gj_gtk_msg_new ("Service Failed!", 
				"Directory Service", 
				details,
				TRUE,
				GJ_GTK_MSG_BUTTON_OK | GJ_GTK_MSG_ICON_EXCLAIMATION,
				(MsgCb)NULL,
				NULL);   
#endif	
		
		/* clean up */
		g_free (details);
	}
	
	/* clean up */
	searchFree (s);
}

/* 
 * internal callbacks 
 */
gint gj_gtk_ds_progress_timer_probing (gpointer data)
{
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (ctlDS.progressbar_probing));
	return TRUE;
} 

gint gj_gtk_ds_progress_timer_searching (gpointer data)
{
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (ctlDS.progressbar_searching));
	return TRUE;
} 

/* gui callbacks */
void on_treeview_ds_selection_changed (GtkTreeSelection *treeselection, gpointer user_data)
{
	/*   gj_gtk_ds_page_one_changed (); */
}

void on_entry_ds_service_changed (GtkEditable *editable, gpointer user_data)
{
	gj_gtk_ds_page_one_changed ();
}

void on_directory_search_response (GtkDialog *dialog, gint response, gpointer user_data)
{
	switch (response) {
	case DIRECTORY_SEARCH_RESPONSE_BACK:
		gj_gtk_ds_page_back ();
		break;
		
	case DIRECTORY_SEARCH_RESPONSE_FORWARD:
		gj_gtk_ds_page_forward ();
		break;
		
	case GTK_RESPONSE_NONE:
	case GTK_RESPONSE_DELETE_EVENT:
	case GTK_RESPONSE_CANCEL: 
	case GTK_RESPONSE_CLOSE:
		gj_gtk_ds_unload ();
		break;
		
	default:
		g_message ("on_directory_search_response: response:%d not handled", response);
		break;
	}
}

#ifdef __cplusplus
}
#endif
