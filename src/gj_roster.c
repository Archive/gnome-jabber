/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_history.h"
#include "gj_parser.h"
#include "gj_jid.h"
#include "gj_iq.h"
#include "gj_support.h"
#include "gj_iq_requests.h"
#include "gj_connection.h"


struct t_roster {
	gint id; /* unique id */

	GList *groups;
	GList *agents;
	GList *contacts;
  
	GjConnection c;

	GjJID jid; /* jid for the connection, e.g. someone@jabber.org */

	GjRosterCallback cb;
	GjRosterSetCallback set_cb;  

	gpointer user_data;
};


static void gj_roster_init ();

static void gj_roster_destroy_groups_cb (gpointer data, gpointer user_data);
static void gj_roster_destroy_agents_cb (gpointer data, gpointer user_data);
static void gj_roster_destroy_contacts_cb (gpointer data, gpointer user_data);

static gboolean gj_roster_update_groups_from_item (GjRoster r, GjRosterItem ri);

static void gj_roster_request_cb (GjInfoquery iq, GjInfoquery iq_related, GjRoster r);

static gboolean gj_roster_parse (GjRoster r, const xmlNodePtr parent);


/* static GHashTable *rosters = NULL; */
static GList *rosters = NULL;
static GHashTable *roster_items = NULL;


static void gj_roster_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;

	roster_items = g_hash_table_new_full (g_int_hash,  
					      g_int_equal,  
					      NULL,  
					      (GDestroyNotify) NULL);    
}

void gj_roster_stats ()
{
	g_message ("%s: table size:%d", __FUNCTION__, g_hash_table_size (roster_items));
}


/*
 * roster
 */
GjRoster gj_roster_new (const GjConnection c, 
			const gchar *jid_str, 
			GjRosterCallback cb, 
			GjRosterSetCallback set_cb,
			gpointer user_data)
{
	GjRoster r = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	if (jid_str == NULL) {
		g_warning ("%s: jid string was NULL", __FUNCTION__);
		return NULL;
	}

	if (cb == NULL) {
		g_warning ("%s: roster callback was NULL", __FUNCTION__);
		return NULL;
	}

	if (set_cb == NULL) {
		g_warning ("%s: roster set callback was NULL", __FUNCTION__);
		return NULL;
	}

	gj_roster_init ();

	if ((r = g_new0 (struct t_roster,1))) {
		r->id = gj_get_unique_id ();

		r->groups = NULL;
		r->agents = NULL;
		r->contacts = NULL;

		r->c = gj_connection_ref (c);

		r->jid = gj_jid_new (jid_str);

		r->cb = cb;
		r->set_cb = set_cb;

		r->user_data = user_data;

		rosters = g_list_append (rosters, r);
	}
  
	return r;  
}

void gj_roster_free (GjRoster r)
{
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return;
	}

	rosters = g_list_remove (rosters, r);

	/* cleanup the hash table and remove all agents and 
	   contacts that belong to this roster */
	g_list_foreach (r->agents, (GFunc)gj_roster_index_del_item, NULL);
	g_list_foreach (r->contacts, (GFunc)gj_roster_index_del_item, NULL);

	if (r->groups != NULL) {
		g_list_foreach (r->groups, gj_roster_destroy_groups_cb, NULL);
	}

	if (r->agents != NULL) {
		g_list_foreach (r->agents, gj_roster_destroy_agents_cb, NULL);
	}

	if (r->contacts != NULL) {
		g_list_foreach (r->contacts, gj_roster_destroy_contacts_cb, NULL); 
	}

	if (r->c) {
		gj_connection_unref (r->c);
	}

	g_free (r);
}

static void gj_roster_destroy_groups_cb (gpointer data, gpointer user_data)
{
	g_free (data);
}

static void gj_roster_destroy_agents_cb (gpointer data, gpointer user_data)
{
	GjRosterItem ri = NULL;

	if (!data) {
		return;
	}

	ri = (GjRosterItem) data;
 
	gj_roster_item_free (ri);
}

static void gj_roster_destroy_contacts_cb (gpointer data, gpointer user_data)
{
	GjRosterItem ri = NULL;

	if (!data) {
		return;
	}

	ri = (GjRosterItem) data;
 
	gj_roster_item_free (ri);
}

GjRoster gj_roster_find (const GjConnection c)
{
	gint i = 0;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	for (i=0; i<g_list_length (rosters); i++) {
		GjRoster r = NULL;
      
		r = (GjRoster) g_list_nth_data (rosters, i);

		if (r && r->c == c) {
			return r; 
		}
	}
  
	return NULL;
}

GjRosterItem gj_roster_find_item_by_jid (GjRoster r, GjJID j)
{
	gboolean check_without_resources = TRUE;
	gint i = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return gj_rosters_find_item_by_jid (j);
	}

	if (j == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	/* if the JID we are searching IS the JID for the roster
	   (i.e. the logged on user) we dont search without the resource
	   because this could be a search for the JID *WITH* the resource. */
	if (j != NULL && r->jid != NULL && gj_jid_equals_without_resource (j, r->jid) == TRUE) {
		check_without_resources = FALSE;
	}

	for (i=0; i<g_list_length (r->contacts); i++) {
		GjRosterItem this_ri = NULL;
      
		this_ri = (GjRosterItem) g_list_nth_data (r->contacts, i);

		if (this_ri && gj_jid_equals (gj_roster_item_get_jid (this_ri), j) == TRUE) {
			return this_ri;
		}
	} 

	for (i=0; i<g_list_length (r->agents); i++) {
		GjRosterItem this_ri = NULL;
      
		this_ri = (GjRosterItem) g_list_nth_data (r->agents, i);

		if (this_ri && gj_jid_equals (gj_roster_item_get_jid (this_ri), j) == TRUE) {
			return this_ri;
		}
	} 

	/* try WITHOUT the resource */
	for (i=0; check_without_resources && i<g_list_length (r->contacts); i++) {
		GjRosterItem this_ri = NULL;
      
		this_ri = (GjRosterItem) g_list_nth_data (r->contacts, i);

		if (this_ri && gj_jid_equals_without_resource (gj_roster_item_get_jid (this_ri), j) == TRUE) {
			return this_ri; 
		}
	} 

	for (i=0; check_without_resources && i<g_list_length (r->agents); i++) {
		GjRosterItem this_ri = NULL;
      
		this_ri = (GjRosterItem) g_list_nth_data (r->agents, i);

		if (this_ri && gj_jid_equals_without_resource (gj_roster_item_get_jid (this_ri), j) == TRUE) {
			return this_ri; 
		}
	} 

	return NULL;
}

/*
 * roster items
 */
gboolean gj_roster_add_item (GjRoster r, GjRosterItem ri) 
{
	gint id = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	id = gj_roster_item_get_id (ri);

	/* add to contacts */
	if (gj_roster_find_item (r, ri) == FALSE) {
		GjJID j = NULL;
      
		j = gj_roster_item_get_jid (ri);
      
		if (gj_jid_get_type (j) == GjJIDTypeContact) {
			r->contacts = g_list_append (r->contacts, ri);
		}

		if (gj_jid_get_type (j) == GjJIDTypeAgent) {
			r->agents = g_list_append (r->agents, ri);
		}

		/* is it a resource ? */
		if (j != NULL && r->jid != NULL && gj_jid_equals_without_resource (j, r->jid) == TRUE) {
			gj_roster_item_set_is_another_resource (ri, TRUE);
		}

		/* update groups list */
		gj_roster_update_groups_from_item (r, ri);

		/* set item's roster */
		gj_roster_item_set_roster (ri, r);
	}

	return TRUE;
}

static gboolean gj_roster_update_groups_from_item (GjRoster r, GjRosterItem ri)
{
	gint i = 0;
	gint j = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	for (i=0; i<g_list_length (gj_roster_item_get_groups (ri)); i++) {
		gchar *group = NULL;
		gboolean match = FALSE;

		group = (gchar*) g_list_nth_data (gj_roster_item_get_groups (ri), i);

		if (group == NULL) {
			continue; 
		}

		if (gj_roster_get_groups (r) == NULL) {
			r->groups = g_list_append (r->groups, g_strdup (group));
			continue;
		}

		for (j=0; j<g_list_length (r->groups); j++) {
			gchar *existing_group = NULL;
	  
			existing_group = (gchar*) g_list_nth_data (r->groups, j);
			if (existing_group && strcmp (existing_group, group) == 0) {
				match = TRUE;
				break;
			}
		}

		if (match == FALSE) {
			r->groups = g_list_append (r->groups, g_strdup (group));
			continue;
		}
	}

	return FALSE;
}

gboolean gj_roster_del_item (GjRoster r, GjRosterItem ri)
{
	GjJID j = NULL;
	gint id = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	id = gj_roster_item_get_id (ri);
	j = gj_roster_item_get_jid (ri);
  
	if (gj_jid_get_type (j) == GjJIDTypeContact) {
		r->contacts = g_list_remove (r->contacts, ri);
	}

	if (gj_jid_get_type (j) == GjJIDTypeAgent) {
		r->agents = g_list_remove (r->agents, ri);
	}

	return TRUE;
}

gboolean gj_roster_find_item (GjRoster r, GjRosterItem ri)
{
	gint i = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	for (i=0; i<g_list_length (r->contacts); i++) {
		GjRosterItem this_ri = NULL;
      
		this_ri = (GjRosterItem) g_list_nth_data (r->contacts, i);
		if (this_ri && gj_roster_item_get_id (ri) == gj_roster_item_get_id (this_ri)) {
			return TRUE; 
		}
	}
  
	return FALSE;
}


/*
 * hash table for all roster items
 */
gboolean gj_roster_index_add_item (GjRosterItem ri) 
{
	GjRosterItem existing_ri = NULL;
	gint *id = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	id = g_new0 (gint, 1);
	*id = gj_roster_item_get_id (ri);

	/* add to roster items */
	existing_ri = g_hash_table_lookup (roster_items, id);
	if (existing_ri == NULL) {
		g_hash_table_insert (roster_items, id, ri);
	}

	/*   g_message ("%s: hash table size:%d", __FUNCTION__, g_hash_table_size (roster_items)); */

	return TRUE;
}

gboolean gj_roster_index_del_item (GjRosterItem ri)
{
	guint id = 0;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	id = gj_roster_item_get_id (ri);
  
	/* add to roster items */
	g_hash_table_remove (roster_items, &id);  

	/*   g_message ("%s: hash table size:%d", __FUNCTION__, g_hash_table_size (roster_items)); */

	return TRUE;
}

/*
 * across all rosters
 */
GjRosterItem gj_rosters_find_item_by_id (guint id)
{
	GjRosterItem ri = NULL;

	/* NOTE: this function uses a hashing table across ALL
	   rosters, this makes it faster to find the roster item we 
	   are interested in :) */

	ri = (GjRosterItem) g_hash_table_lookup (roster_items, &id);
	return ri;
}

GjRosterItem gj_rosters_find_item_by_jid (GjJID j)
{
	GjRosterItem ri = NULL;
	gint i = 0;

	if (j == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	for (i=0; i<g_list_length (rosters); i++) {
		GjRoster r = NULL;
      
		r = (GjRoster) g_list_nth_data (rosters, i);
		if (r == NULL) {
			continue; 
		}
      
		ri = gj_roster_find_item_by_jid (r, j);
		if (ri != NULL) {
			return ri; 
		}
	} 

	return NULL;
}

gboolean gj_rosters_is_agent_registered (const gchar *agent)
{
	gint i = 0;

	if (agent == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	for (i=0; i<g_list_length (rosters); i++) {
		GjRoster r = NULL;
      
		r = (GjRoster) g_list_nth_data (rosters, i);
		if (r == NULL) {
			continue; 
		}
      
		if (gj_roster_is_agent_registered (r, agent) == TRUE) {
			return TRUE;
		}
	} 
  
	return FALSE;
}


/*
 * get lists/tables 
 */
const GList *gj_roster_get_list ()
{
	return rosters;
}

const GList *gj_roster_get_groups (const GjRoster r)
{
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	return r->groups;
}

const GList *gj_roster_get_contacts (const GjRoster r)
{
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	return r->contacts; 
}


const GList *gj_roster_get_agents (const GjRoster r)
{
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	return r->agents;
}

GList *gj_roster_get_contacts_by_group (const GjRoster r, const gchar *group) 
{
	GList *new_list = NULL;
	gint i = 0;
	gint j = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	if (group == NULL || g_utf8_strlen (group,-1) < 0) {
		g_warning ("%s: group was NULL or length was < 0", __FUNCTION__);
		return NULL;
	}

	if (r->groups == NULL) {
		return NULL;
	}

	if (r->contacts == NULL) {
		return NULL;
	}
  
	for (i=0; i < g_list_length (r->contacts); i++) {
		GjRosterItem ri = NULL;
		GList *groups = NULL;

		ri = (GjRosterItem) g_list_nth_data (r->contacts, i);
		if (ri == NULL) {
			continue; 
		}
      
		groups = gj_roster_item_get_groups (ri);
		if (groups == NULL) {
			continue;
		}
      
		for (j=0; j<g_list_length (groups); j++) {
			gchar *s = NULL;
	  
			s = (gchar*) g_list_nth_data (groups, j);
			if (s && strcmp (s, group) == 0) { 
				new_list = g_list_append (new_list, ri);
				break;
			}
		}
	}
  
	return new_list;
}

GList *gj_roster_get_contacts_by_agent (const GjRoster r, const gchar *agent)
{
	GList *new_list = NULL;
	gint i = 0;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	if (agent == NULL || g_utf8_strlen (agent,-1) < 0) {
		g_warning ("%s: agent was NULL or length was < 0", __FUNCTION__);
		return NULL;
	}

	if (r->contacts == NULL) {
		return NULL;
	}
  
	for (i=0; i < g_list_length (r->contacts); i++) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;

		ri = (GjRosterItem) g_list_nth_data (r->contacts, i);
		if (ri == NULL) {
			continue; 
		}

		j = gj_roster_item_get_jid (ri);
		if (j == NULL) {
			continue;  
		}

		if (gj_jid_get_type (j) != GjJIDTypeContact) {
			continue; 
		}

		if (gj_jid_get_agent (j) != NULL && strcmp (gj_jid_get_agent (j), agent) == 0) {
			new_list = g_list_append (new_list, ri);
		}
	}
  
	return new_list;  
}

gboolean gj_roster_is_agent_registered (const GjRoster r, const gchar *agent)
{
	GjJID j_tmp = NULL;
	gint i = 0;
	gboolean found = FALSE;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (agent == NULL) {
		g_warning ("%s: agent was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r->agents == NULL) {
		return FALSE;
	}

	j_tmp = gj_jid_new (agent);

	/* look through roster items */
	for (i=0; found == FALSE && i<g_list_length (r->agents); i++) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;

		ri = (GjRosterItem) g_list_nth_data (r->agents, i);
		if (ri == NULL) {
			continue; 
		}

		j = gj_roster_item_get_jid (ri);
		if (j == NULL) {
			continue;  
		}

		if (gj_jid_equals_without_resource (j_tmp, j) == TRUE) {
			gj_jid_unref (j_tmp);
			return TRUE;
		}
	}  

	gj_jid_unref (j_tmp);
	return FALSE;
}


/*
 * roster handling
 */
gboolean gj_roster_request (GjConnection c, GjRoster r)
{
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r->cb == NULL) {
		g_warning ("%s: roster callback was NULL", __FUNCTION__);
		return FALSE;
	}
  
	return gj_iq_request_roster (c, (GjInfoqueryCallback)gj_roster_request_cb, r);
}

static void gj_roster_request_cb (GjInfoquery iq, GjInfoquery iq_related, GjRoster r)
{
	GjRosterCallback cb = NULL;

	xmlNodePtr node = NULL;

	if (!iq || !iq_related) {
		return;
	}

	/* get info */
	node = gj_iq_get_xml (iq);

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return;
	}

	cb = r->cb;
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	if (gj_roster_parse (r, node) == FALSE) {
		g_warning ("%s: parsing roster failed", __FUNCTION__);
		return;
	}

	(cb) (gj_roster_get_contacts (r), gj_roster_get_agents (r), r->user_data);
}

static gboolean gj_roster_parse (GjRoster r, const xmlNodePtr parent)
{
	gint count = 0;
	xmlNodePtr node = NULL;

	if (parent == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	/* create list */
	gj_roster_init ();

	/* get item node */
	node = gj_parser_find_by_node (parent, (guchar*)"item");

	/* get items in roster */
	for (;node != NULL;node = node->next) {
		GjJID j = NULL;
		GjRosterItem ri = NULL;
		GjHistory hist = NULL;

		GList *groups = NULL;
		xmlChar *tmp = NULL;
      
		count++;
      
		/* jid */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"jid")) == NULL) {
			continue;
		}
      
		/* create new roster item */
		ri = gj_roster_item_new ((gchar*)tmp);
		if (ri == NULL) {
			g_warning ("%s: roster item was NULL", __FUNCTION__);
			continue;
		}
      
		j = gj_roster_item_get_jid (ri);
      
		/* name */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"name")) != NULL) {
			gj_roster_item_set_name (ri, (gchar*)tmp);
		} else {
			gj_roster_item_set_name (ri, gj_jid_get_full (j));
		}
      
		/* groups */ 
		groups = gj_parser_find_by_node_on_parent_and_ret_contents_by_list (node, (guchar*)"group");

		if (groups != NULL) {
			gj_roster_item_set_groups (ri, groups); 
		}
      
		/* subscription type */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"subscription")) != NULL) {
			if (xmlStrcmp (tmp, (guchar*)"none") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionNone);
			} else if (xmlStrcmp (tmp, (guchar*)"to") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionTo);
			} else if (xmlStrcmp (tmp, (guchar*)"from") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionFrom);
			} else if (xmlStrcmp (tmp, (guchar*)"both") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionBoth);
			} else {
				g_warning ("%s: unexpected roster item type:'%s'", __FUNCTION__, tmp); 
			}
		}

		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"ask")) != NULL) {
			gj_roster_item_set_ask (ri, (gchar*)tmp);
		}

		gj_roster_item_set_is_permanent (ri, TRUE);
      
		/* add to list */
		gj_roster_add_item (r, ri);
      
		/* create history */
		if ((hist = gj_history_find (gj_roster_item_get_id (ri))) == NULL) {
			hist = gj_history_new (ri);
		}
      
		gj_history_start (hist);
	}

	g_message ("%s: items:%d", __FUNCTION__, count);

	return TRUE;
}

gboolean gj_roster_parse_set_request (const GjInfoquery iq)
{
	GjConnection c = NULL;
	GjRoster r = NULL;
	GjRosterSetCallback cb = NULL;

	GList *updated_items = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr parent = NULL;

	if (iq == NULL) {
		g_warning ("%s: iq was NULL", __FUNCTION__);
		return FALSE;
	}

	c = gj_iq_get_connection (iq);
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: updating roster item (s) for connection:0x%.8x...", __FUNCTION__, (gint)c);

	r = gj_roster_find (c);
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	cb = r->set_cb;
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if ((parent = gj_iq_get_xml (iq)) == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return FALSE;
	}

	/* get item node */
	node = gj_parser_find_by_node (parent, (guchar*)"item");

	/* get items in roster */
	for (;node != NULL; node = node->next) {
		gchar *ask = NULL;
		gchar *subscription = NULL;
		gchar *name = NULL;
		GList *groups = NULL;

		xmlChar *tmp = NULL;

		GjRosterItem ri = NULL;
		GjJID j = NULL;
		GjHistory hist = NULL;
     
		/* get jid */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"jid")) != NULL) {
			j = gj_jid_new ((gchar*)tmp);
		}
     
		/* look for roster item */
		ri = gj_roster_find_item_by_jid (r, j);

		if (ri == NULL) {
			if (j != NULL) {
				gj_jid_unref (j);
				j = NULL;
			}

			g_message ("%s: roster item not found by jid, creating...", __FUNCTION__);
   
			ri = gj_roster_item_new ((gchar*)tmp);
			if (ri == NULL) {
				g_warning ("%s: roster item was NULL", __FUNCTION__);
				continue;
			}
	  
			j = gj_roster_item_get_jid (ri); 

			/* add reference for this func */
			gj_jid_ref (j);
	  
			if ((hist = gj_history_find (gj_roster_item_get_id (ri))) == NULL) {
				hist = gj_history_new (ri);
			}
	  
			gj_history_start (hist);
		}

		/* get name */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"name")) != NULL) {
			name = g_strdup ((gchar*)tmp);  
		}

		if ((name && !gj_roster_item_get_name (ri)) ||
		    (name && strcmp (name, gj_roster_item_get_name (ri)) != 0)) {
			gj_roster_item_set_name (ri, name);
		} else if (!name && !gj_roster_item_get_name (ri)) {
			gj_roster_item_set_name (ri, gj_jid_get_full (j));
		}
		
		g_free (name);

		/* groups */ 
		groups = gj_parser_find_by_node_on_parent_and_ret_contents_by_list (node, (guchar*)"group");

		gj_roster_item_set_groups (ri, groups);
		gj_roster_update_groups_from_item (r, ri);

		/* get subscription */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"subscription")) != NULL) {
			subscription = g_strdup ((gchar*)tmp);
		}

		/* get ask */
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"ask")) != NULL) {
			ask = g_strdup ((gchar*)tmp);
		}

		/* subscription type */
		if (subscription != NULL) {
			if (g_strcasecmp (subscription, "none") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionNone);
			} else if (g_strcasecmp (subscription, "to") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionTo);
			} else if (g_strcasecmp (subscription, "from") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionFrom);
			} else if (g_strcasecmp (subscription, "both") == 0) {
				gj_roster_item_set_subscription (ri, GjRosterItemSubscriptionBoth);
			} else {
				g_warning ("%s: unexpected roster item type:'%s'", __FUNCTION__, subscription);
			}
	  
			/* we assume if there is a subscription element, 
			   they MUST be on our roster */
			gj_roster_item_set_is_permanent (ri, TRUE);
		}

		/* NOTE: we remove the contact here and add it later on when the
		   details have changed.  this way we can change the groups
		   and not have to worry about finding the contact and checking 
		   the groups and details...
		*/

		/* add to list of updated roster items */
		updated_items = g_list_append (updated_items, ri);
     
		if (subscription != NULL && g_strcasecmp (subscription, "remove") == 0) {
			/*
			 * REMOVE roster item
			 */
			if (j != NULL) {
				gj_jid_unref (j);
			}

			if (ri == NULL) {
				g_warning ("%s: subscription remove and roster item is NULL", __FUNCTION__);
				continue;
			}

			/* let people know it is about to be deleted */
			gj_roster_item_set_is_set_for_removal (ri, TRUE);

			continue;
		} else if (ask != NULL) {
			/*
			 * ASK for subscription or to unsubscribe
			 */
			if (g_strcasecmp (ask, "subscribe") == 0) {
				g_message ("%s: jid:'%s' is requesting subscription, name:'%s'", 
					   __FUNCTION__, gj_jid_get_full (j), name);
				
				/* automatically subscribe and agree subscription if agent */
				if (gj_jid_get_type (j) == GjJIDTypeAgent) {
					gj_presence_send_to (c,
							     j, 
							     FALSE, 
							     GjPresenceTypeSubscribed, 
							     GjPresenceShowNormal, 
							     NULL, 
							     0);
					
					g_free (ask);
					g_free (subscription);
					gj_jid_unref (j); 
					
					continue;
				}
			} else if (g_strcasecmp (ask, "unsubscribe") == 0) {
				g_message ("%s: jid:'%s' is cancelling subscription, name:'%s'", 
					   __FUNCTION__, gj_jid_get_full (j), name);
				
				if (gj_jid_get_type (j) == GjJIDTypeAgent) {
					gj_presence_send_to (c,
							     j, 
							     FALSE, 
							     GjPresenceTypeUnSubscribed, 
							     GjPresenceShowEnd, 
							     NULL, 
							     0);
					
					g_free (ask);
					g_free (subscription);
					gj_jid_unref (j); 
					
					continue;
				}
			} else {
				g_warning ("%s: ask value:'%s' not dealt with from jid:'%s' with name:'%s'", 
					   __FUNCTION__, tmp, gj_jid_get_full (j), name);
			}  
			
			/* set ASK */
			gj_roster_item_set_ask (ri, ask);
			
			/* clean up */
			gj_jid_unref (j); 
			
			continue;
		}
		
		
		/* clean up */
		if (j != NULL) {
			gj_jid_unref (j);
		}
	}

	g_message ("%s: updated %d items", __FUNCTION__, g_list_length (updated_items));

	/* call callback */
	(cb) (updated_items, r->user_data);

	/* clean up */
	g_list_free (updated_items); 

	/* !!!@@@@ AT this point do we do foreach check
	   if set for removal and remove?? - perhaps garbage collect it ? */

	return TRUE; 
}


#ifdef __cplusplus
}
#endif
