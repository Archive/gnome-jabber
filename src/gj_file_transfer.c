/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <time.h> 
#include <string.h>

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "gj_file_transfer.h"
#include "gj_roster_item.h"
#include "gj_connection.h"
#include "gj_jid.h"
#include "gj_parser.h"
#include "gj_iq_requests.h"
#include "gj_support.h"

#include "gj_gtk_file_transfer_receive.h"


struct t_GjFileTransfer {
	gint ref_count;

	gint id;

	/* roster association */
	gint ri_id;

	/* connection */
	GjConnection connection;

	/* query id */
	gchar *qid;

	/* who */
	GjJID to_jid;
	GjJID from_jid;

	/* service initiation */
	gchar *sid;

	gboolean direct_connection;

	/* shared between SI and IBB */
	gchar *mime_type;
	gchar *profile;

	gchar *file_name;
	gchar *file_size;
  
	gchar *description;

	gchar *hash;

	time_t time;

	/* stream details */
	gchar *stream_host; 
	gchar *stream_port;

	gchar *stream_jid_used;

	GjConnection file_connection;

	/* range details */
	gint offset;
	gint length;
  
	/* generic info */
	GjFileTransferErrorCallback error_cb;
  
	gboolean inbound_request;

	gpointer user_data;
};


static void gj_file_transfer_init ();

static void gj_file_transfer_free (GjFileTransfer ft);
static void gj_file_transfer_destroy_cb (gpointer data);

static void gj_file_transfer_disco_info_cb (GjInfoquery iq, GjInfoquery iq_related, GjConnection c);
static void gj_file_transfer_disco_items_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

static void gj_file_transfer_accept_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

static void gj_file_transfer_bytestreams_start_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);


static GHashTable *file_transfers = NULL;


/*
  We make use of:
  JEP - 0096 (File Transfer - covers Jeps 65/47)
  JEP - 0065 (Bytestreams)
  JEP - 0047 (In-Band Bytestream)
*/


static void gj_file_transfer_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE)
		return;

	initiated = TRUE;

	file_transfers = g_hash_table_new_full (g_str_hash, 
						g_str_equal, 
						NULL, 
						(GDestroyNotify) gj_file_transfer_destroy_cb);   
}

static void gj_file_transfer_free (GjFileTransfer ft)
{
	if (ft == NULL) {
		g_warning ("%s: GjFileTransfer was NULL", __FUNCTION__);
		return;
	}

	g_hash_table_remove (file_transfers, ft->sid);
}

static void gj_file_transfer_destroy_cb (gpointer data)
{
	GjFileTransfer ft = (GjFileTransfer) data;

	if (!ft) {
		return; 
	}

	if (ft->to_jid) {
		gj_jid_unref (ft->to_jid);
	}

	if (ft->from_jid) {
		gj_jid_unref (ft->from_jid); 
	}

	if (ft->connection) {
		gj_connection_unref (ft->connection);
	}

	g_free (ft->qid);
	g_free (ft->sid);
	g_free (ft->mime_type);
	g_free (ft->profile);
	g_free (ft->file_name);
	g_free (ft->file_size);
	g_free (ft->stream_host);
	g_free (ft->stream_port);
	g_free (ft->stream_jid_used);

	if (ft->file_connection) {
		gj_connection_unref (ft->file_connection);
	}

	g_free (ft);
}

GjFileTransfer gj_file_transfer_ref (GjFileTransfer ft)
{
	if (!ft) {
		return NULL;
	}

	ft->ref_count++;

	return ft;
}

gboolean gj_file_transfer_unref (GjFileTransfer ft)
{
	if (!ft) {
		return FALSE; 
	}

	ft->ref_count--;

	if (ft->ref_count < 1) {
		gj_file_transfer_free (ft);
	}

	return TRUE;
}

/*
 * member functions 
 */

/* gets */
gint gj_file_transfer_get_unique_id (GjFileTransfer ft)
{
	if (!ft) {
		return -1;
	}

	return ft->id;
}

GjConnection gj_file_transfer_get_connection (GjFileTransfer ft)
{
	if (!ft) {
		return NULL;
	}

	return ft->connection;
}

GjJID gj_file_transfer_get_to_jid (GjFileTransfer ft)
{
	if (!ft) {
		return NULL; 
	}

	return ft->to_jid;
}

GjJID gj_file_transfer_get_from_jid (GjFileTransfer ft)
{
	if (!ft) {
		return NULL; 
	}

	return ft->from_jid;
}

const gchar *gj_file_transfer_get_mime_type (GjFileTransfer ft)
{
	if (!ft) {
		return NULL;
	}

	return ft->mime_type;
}

const gchar *gj_file_transfer_get_file_name (GjFileTransfer ft)
{
	if (!ft) {
		return NULL;
	}

	return ft->file_name;
}

const gchar *gj_file_transfer_get_file_size (GjFileTransfer ft)
{
	if (!ft) {
		return NULL;
	}

	return ft->file_size;
}

const gchar *gj_file_transfer_get_description (GjFileTransfer ft)
{
	if (!ft) {
		return NULL;
	}

	return ft->description;
}

void gj_file_transfer_start (GjConnection c,
			     GjRosterItem ri,
			     GjFileTransferErrorCallback error_cb, 
			     gpointer user_data)
{
	GjJID j = NULL;
	
	if (!ri) {
		return;
	}

	j = gj_roster_item_get_jid (ri);
	
	gj_iq_request_disco_info (c,
				  gj_jid_get_full (j), 
				  (GjInfoqueryCallback)gj_file_transfer_disco_info_cb, 
				  gj_connection_ref (c));
}

static void gj_file_transfer_disco_info_cb (GjInfoquery iq, GjInfoquery iq_related, GjConnection c)
{
	g_message ("%s: disco info response", __FUNCTION__);

	gj_iq_request_disco_items (c, gj_connection_get_host (c), gj_file_transfer_disco_items_cb, NULL); 

	gj_connection_unref (c);
}

static void gj_file_transfer_disco_items_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: disco items response", __FUNCTION__);
}

void gj_file_transfer_accept (GjConnection c, GjFileTransfer ft)
{
	if (!ft) {
		return;
	}

	g_message ("%s: accept sid:'%s'", __FUNCTION__, ft->sid);
  
	gj_iq_request_file_transfer_si_result (c,
					       ft->qid,
					       gj_jid_get_full (ft->from_jid), 
					       ft->sid,
					       ft->mime_type,
					       ft->file_name,
					       ft->file_size,
					       gj_file_transfer_accept_cb,
					       NULL);
}

static void gj_file_transfer_accept_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: response", __FUNCTION__);
}


void gj_file_transfer_reject (GjConnection c, 
			      GjFileTransfer ft)
{
	if (!ft) {
		return; 
	}

	g_message ("%s: reject sid:'%s'", __FUNCTION__, ft->sid);
}

void gj_file_transfer_bytestreams_start (GjConnection c,
					 GjFileTransfer ft)
{
	gj_iq_request_file_transfer_bytestreams_result (c,
							ft->qid,
							gj_jid_get_full (ft->from_jid),
							ft->sid,
							ft->stream_jid_used,
							gj_file_transfer_bytestreams_start_cb,
							NULL);
}

static void gj_file_transfer_bytestreams_start_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: response", __FUNCTION__);
}


/*
 * handles inbound xml messages
 */
void gj_file_transfer_si_receive (GjConnection c, xmlNodePtr node)
{
	xmlChar *tmp = NULL;
	xmlNodePtr si = NULL;
	xmlNodePtr file = NULL;

	GjFileTransfer ft = NULL;

	gj_file_transfer_init ();

	if (!node) {
		return;
	}

	ft = g_new0 (struct t_GjFileTransfer, 1);

	ft->ref_count = 1;

	ft->id = gj_get_unique_id ();

	ft->inbound_request = TRUE;

	ft->connection = gj_connection_ref (c);
  
	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"to")) != NULL && xmlStrlen (tmp) > 0) {
		ft->to_jid = gj_jid_new ((gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"from")) != NULL && xmlStrlen (tmp) > 0) {
		ft->from_jid = gj_jid_new ((gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"id")) != NULL && xmlStrlen (tmp) > 0) {
		ft->qid = g_strdup ((gchar*)tmp);
	}

	si = gj_parser_find_by_node (node, (guchar*)"si");
  
	if ((tmp = gj_parser_find_by_attr_and_ret (si, (guchar*)"id")) != NULL && xmlStrlen (tmp) > 0) {
		ft->sid = g_strdup ((gchar*)tmp); 
	}
  
	if ((tmp = gj_parser_find_by_attr_and_ret (si, (guchar*)"mime-type")) != NULL && xmlStrlen (tmp) > 0) {
		ft->mime_type = g_strdup ((gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (si, (guchar*)"profile")) != NULL && xmlStrlen (tmp) > 0) {
		ft->profile = g_strdup ((gchar*)tmp);
	}

	file = gj_parser_find_by_node (node, (guchar*)"file");

	if ((tmp = gj_parser_find_by_attr_and_ret (file, (guchar*)"name")) != NULL && xmlStrlen (tmp) > 0) {
		ft->file_name = g_strdup ((gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (file, (guchar*)"size")) != NULL && xmlStrlen (tmp) > 0) {
		ft->file_size = g_strdup ((gchar*)tmp);
	}

	g_message ("%s: FILE TRANSFER - SI: \n"
		   "-> SI\n"
		   "\tto:'%s'\n"
		   "\tfrom:'%s'\n"
		   "\tsid:'%s'\n"
		   "\tmime-type'%s'\n"
		   "\tprofile:'%s'\n"
		   "-> FILE\n"
		   "\tname:'%s'\n"
		   "\tsize:'%s'",
		   __FUNCTION__, 
		   gj_jid_get_full (ft->to_jid),
		   gj_jid_get_full (ft->from_jid),
		   ft->sid,
		   ft->mime_type,
		   ft->profile,
		   ft->file_name,
		   ft->file_size);

	if (strcmp (ft->profile, "http://jabber.org/protocol/si/profile/file-transfer") != 0) {
		g_warning ("%s: unknown profile, ignoring request", __FUNCTION__);
		return;
	}

	g_hash_table_insert (file_transfers, ft->sid, ft);

	/* show file transfer received dialog */
	gj_gtk_file_transfer_receive_load (c, ft);
}

void gj_file_transfer_bs_receive (GjConnection c, xmlNodePtr node)
{
	xmlChar *tmp = NULL;
	xmlNodePtr query = NULL;
	xmlNodePtr streamhost = NULL;

	GjFileTransfer ft = NULL;

	gchar *sid = NULL;

	if (!node) {
		return;
	}

	query = gj_parser_find_by_node (node, (guchar*)"query");

	if ((tmp = gj_parser_find_by_attr_and_ret (query, (guchar*)"sid")) != NULL && xmlStrlen (tmp) > 0) {
		sid = g_strdup ((gchar*)tmp); 
	}
  
	if (!sid) {
		return; 
	}

	ft = g_hash_table_lookup (file_transfers, sid);

	if (!ft) {
		return;
	}

	streamhost = gj_parser_find_by_node (node, (guchar*)"streamhost");

	if ((tmp = gj_parser_find_by_attr_and_ret (streamhost, (guchar*)"host")) != NULL && xmlStrlen (tmp) > 0) {
		ft->stream_host = g_strdup ((gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (streamhost, (guchar*)"port")) != NULL && xmlStrlen (tmp) > 0) {
		ft->stream_port = g_strdup ((gchar*)tmp); 
	}

	g_message ("%s: FILE TRANSFER - BYTESTREAMS: \n"
		   "\tto:'%s'\n"
		   "\tfrom:'%s'\n"
		   "\thost:'%s'\n"
		   "\tport:'%s'",
		   __FUNCTION__, 
		   gj_jid_get_full (ft->to_jid),
		   gj_jid_get_full (ft->from_jid),
		   ft->stream_host,
		   ft->stream_port);

	/* set host jid to use, this could be a proxy */
	ft->stream_jid_used = g_strdup (gj_jid_get_full (ft->from_jid));

	/* listen for connection */
	ft->file_connection = gj_connection_create (ft->stream_host, atoi (ft->stream_port));

	/* show file transfer dialog */
	gj_file_transfer_bytestreams_start (c, ft);

	/* clean up */
	g_free (sid);
}

void gj_file_transfer_ibb_receive (GjConnection c, xmlNodePtr node)
{
	xmlChar *tmp = NULL;

	GjJID to_jid = NULL;
	GjJID from_jid = NULL;

	if (!node) {
		return;
	}
  
	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"to")) != NULL && xmlStrlen (tmp) > 0) {
		to_jid = gj_jid_new ((gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"from")) != NULL && xmlStrlen (tmp) > 0) {
		from_jid = gj_jid_new ((gchar*)tmp);
	}
}


#if 0
{
	gchar *text = NULL;
	gchar *encoded = NULL;

	text = g_strdup_printf ("Hello World");

	encoded = base64_encode_simple (text, strlen (text));
	if (encoded) {
		gchar *decoded = NULL;
	
		g_message ("%s: encoded text:'%s' to '%s'", 
			   __FUNCTION__, text, encoded);

	
		decoded = g_strdup (encoded);
		base64_decode_simple (decoded, strlen (decoded));
	
		g_message ("%s: decoded text:'%s' to '%s'", 
			   __FUNCTION__, encoded, decoded);
	
		g_free (encoded);
		g_free (decoded);
	}

	g_free (text);
}

return 0;
#endif


#ifdef __cplusplus
}
#endif
