/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_presence.h"
#include "gj_support.h"
#include "gj_translate.h"

#include "gj_gtk_roster.h"
#include "gj_gtk_presence_presets.h"
#include "gj_gtk_support.h"


typedef struct {
	GtkWidget *window;

	GtkWidget *spinbutton;
	GtkWidget *entry;
  
	GtkWidget *optionmenu;

	GtkWidget *treeview;

	GtkWidget *button_add;
	GtkWidget *button_remove;
	GtkWidget *button_update;


	/* 
	 * members 
	 */

	GjPresenceShow presence_show;

} GjPresencePresetsDialog;


enum {
	COL_PP_IMAGE,
	COL_PP_INDEX,
	COL_PP_SHOW,
	COL_PP_SHOW_STR,
	COL_PP_STATUS,
	COL_PP_PRIORITY,
	COL_PP_CHANGED,
	COL_PP_EDITABLE,
	COL_PP_COUNT
};


static GtkWidget *gj_gtk_pp_generate_menu (gpointer user_data);

/* model */
static gboolean gj_gtk_pp_model_setup (GjPresencePresetsDialog *dialog);
static gboolean gj_gtk_pp_model_populate_columns (GjPresencePresetsDialog *dialog);

static void gj_gtk_pp_model_add (GjPresencePresetsDialog *dialog, gint index,
				 GjPresenceShow show, gint priority, const gchar *status);

static void on_treeview_selection_changed (GtkTreeSelection *treeselection, GjPresencePresetsDialog *dialog);

static void on_optionmenu_cb (GtkMenuItem *menuitem, GjPresencePresetsDialog *dialog);

/* gui callbacks */
static void on_entry_changed (GtkEditable *editable, GjPresencePresetsDialog *dialog);
static void on_spinbutton_value_changed (GtkSpinButton *spinbutton, GjPresencePresetsDialog *dialog);

static void on_button_add_clicked (GtkButton *button, GjPresencePresetsDialog *dialog);
static void on_button_remove_clicked (GtkButton *button, GjPresencePresetsDialog *dialog);
static void on_button_update_clicked (GtkButton *button, GjPresencePresetsDialog *dialog);

static void on_destroy (GtkWidget *widget, GjPresencePresetsDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjPresencePresetsDialog *dialog);


static GjPresencePresetsDialog *current_dialog = NULL;


gboolean gj_gtk_pp_load ()
{
	GjPresencePresetsDialog *dialog = NULL;
	GladeXML *xml = NULL;

	GtkWidget *menu = NULL;
  
	gint i = 0;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	dialog = g_new0 (GjPresencePresetsDialog, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "presence_presets",
				     NULL,
				     "presence_presets", &dialog->window,
				     "spinbutton", &dialog->spinbutton,
				     "entry", &dialog->entry,
				     "optionmenu", &dialog->optionmenu,
				     "treeview", &dialog->treeview,
				     "button_add", &dialog->button_add,
				     "button_remove", &dialog->button_remove,
				     "button_update", &dialog->button_update,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "presence_presets", "response", on_response,
			      "presence_presets", "destroy", on_destroy,
			      "entry", "changed", on_entry_changed,
			      "spinbutton", "value_changed", on_spinbutton_value_changed,
			      "button_add", "clicked", on_button_add_clicked,
			      "button_remove", "clicked", on_button_remove_clicked,
			      "button_update", "clicked", on_button_update_clicked,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);


	/* remember */
	current_dialog = dialog;

	/* set up menu */
	menu = gj_gtk_pp_generate_menu (dialog);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (dialog->optionmenu), menu);

	dialog->presence_show = GjPresenceShowAway;

	/* buttons */
	gtk_widget_set_sensitive (dialog->button_add, FALSE);
	gtk_widget_set_sensitive (dialog->button_remove, FALSE);
	gtk_widget_set_sensitive (dialog->button_update, FALSE);

	/* set up model for treeview */
	gj_gtk_pp_model_setup (dialog);

	/* get current presets */
	for (i = 1; i < 100; i++) {
		gboolean success = FALSE;

		gchar *show = NULL;
		gchar *status = NULL;
		gchar *priority = NULL;

		GjPresenceShow show_int = 0;
		gint priority_int = 0;
      
		success = gj_config_get_pp_index (i, &show, &status, &priority);

		if (!success || !show) {
			break;
		}

		priority_int = atoi (priority);
		show_int = gj_translate_presence_show_str (show);
      
		gj_gtk_pp_model_add (dialog, i, show_int, priority_int, status);

		g_free (show);
		g_free (status);
		g_free (priority);
	}
  
	return TRUE;
}

static GtkWidget *gj_gtk_pp_generate_menu (gpointer user_data)
{
	GtkWidget *menu = NULL;

	gint i = 0;

	menu = gtk_menu_new ();

	for (i = GjPresenceShowNull + 1; i<GjPresenceShowEnd; i++) {
		GtkWidget *item = NULL;
		GtkWidget *hbox = NULL;
	
		GtkWidget *image = NULL;
		GtkWidget *label = NULL;
	
		const gchar *stock_id = NULL;
		const gchar *str = NULL;

		if (i == GjPresenceShowInvisible) {
			continue;
		}

		str = gj_translate_presence_show (i);
		stock_id = gj_gtk_presence_to_stock_id (GjPresenceTypeAvailable, i);

		hbox = gtk_hbox_new (FALSE, 4);
	
		image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_MENU);
		gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
	
		label = gtk_label_new (str);
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);
	
		item = gtk_menu_item_new ();
		gtk_container_add (GTK_CONTAINER (item), hbox);
	
		g_object_set_data (G_OBJECT (item), "presence_show", 
				   GUINT_TO_POINTER (i)); 
	
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
    
		g_signal_connect ((gpointer) item, "activate", 
				  G_CALLBACK (on_optionmenu_cb), user_data);
	}

	gtk_widget_show_all (menu);

	return menu;
}

static gboolean gj_gtk_pp_model_setup (GjPresencePresetsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	GList *groups = NULL;
	gboolean success = FALSE;
	gint i = 0;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	selection = gtk_tree_view_get_selection (view);

	/* create tree store */
	store = gtk_list_store_new (COL_PP_COUNT,
				    GDK_TYPE_PIXBUF, /* presence show (pb)  */
				    G_TYPE_INT,      /* index */
				    G_TYPE_INT,      /* presence show (int) */
				    G_TYPE_STRING,   /* presence show (str) */ 
				    G_TYPE_STRING,   /* presence status */
				    G_TYPE_INT,      /* presence priority */
				    G_TYPE_BOOLEAN,  /* changed */
				    G_TYPE_BOOLEAN); /* editable */
  
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection), "changed", 
			  G_CALLBACK (on_treeview_selection_changed), dialog);

	/* populate columns */
	gj_gtk_pp_model_populate_columns (dialog);

	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE);
	gtk_tree_view_set_search_column (view, COL_PP_SHOW); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, FALSE);
	gtk_tree_view_columns_autosize (view);
	gtk_tree_view_expand_all (view);

	/* clean up */
	g_object_unref (G_OBJECT (model));

	return TRUE;
}

static gboolean gj_gtk_pp_model_populate_columns (GjPresencePresetsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeViewColumn *column = NULL; 
	GtkCellRenderer *renderer = NULL;

	guint col_offset = 0;

	if (!dialog) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));

	/* COL_PP_SHOW (str) */
	renderer = gtk_cell_renderer_pixbuf_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, "",
								  renderer, 
								  "pixbuf", COL_PP_IMAGE,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_PP_SHOW));

#if 0
	/* COL_PP_SHOW (str) */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Show"),
								  renderer, 
								  "text", COL_PP_SHOW_STR,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_PP_SHOW_STR));

	/* COL_PP_PRIORITY */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Priority"),
								  renderer, 
								  "text", COL_PP_PRIORITY,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_PP_PRIORITY));
#endif

	/* COL_PP_STATUS */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Status"),
								  renderer, 
								  "text", COL_PP_STATUS,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", GINT_TO_POINTER (COL_PP_STATUS));

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_PP_SHOW);

	return TRUE;
}

static void gj_gtk_pp_model_add (GjPresencePresetsDialog *dialog, gint index, 
				 GjPresenceShow show, gint priority, const gchar *status)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	const gchar *stock_id = NULL;
	const gchar *show_str = NULL;
	GdkPixbuf *pb = NULL;

	if (!dialog) {
		return;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	stock_id = gj_gtk_presence_to_stock_id (GjPresenceTypeAvailable, show);
	show_str = gj_translate_presence_show (show);
	pb = gj_gtk_pixbuf_new_from_stock (stock_id); 
    
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,    
			    COL_PP_IMAGE, pb,
			    COL_PP_INDEX, index, 
			    COL_PP_SHOW, show,
			    COL_PP_SHOW_STR, show_str,
			    COL_PP_PRIORITY, priority,
			    COL_PP_STATUS, status,
			    -1);

	g_object_unref (G_OBJECT (pb));
}

static void on_treeview_selection_changed (GtkTreeSelection *treeselection, GjPresencePresetsDialog *dialog)
{
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;

	gboolean changed = FALSE;
	GjPresenceShow show = GjPresenceShowNull;
	gchar *status = NULL;
	gint priority = 0;

	GtkWidget *menu = NULL;
	GList *node = NULL;
	gint menu_index = 0;

	if (!dialog) {
		return;
	}

	if (gtk_tree_selection_get_selected (treeselection, &model, &iter) == FALSE) {
		g_message ("%s: there is nothing selected", __FUNCTION__);
		return;
	}

	if (GTK_TREE_MODEL (model) == NULL) {
		return;
	}

	gtk_tree_model_get (model, &iter,
			    COL_PP_CHANGED, &changed,
			    COL_PP_SHOW, &show,
			    COL_PP_STATUS, &status,
			    COL_PP_PRIORITY, &priority,
			    -1);

	gtk_entry_set_text (GTK_ENTRY (dialog->entry), status);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton), (gdouble)priority);

	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (dialog->optionmenu));

	node = GTK_MENU_SHELL (menu)->children;
	for (menu_index = 0; node; node = node->next, menu_index++) {
		GjPresenceShow menu_show = GjPresenceShowNull;
		gpointer p = NULL;

		p = g_object_get_data (G_OBJECT (node->data), "presence_show");
		menu_show = GPOINTER_TO_INT (p);

		if (menu_show == show) {
			break;
		}
	}

	gtk_option_menu_set_history (GTK_OPTION_MENU (dialog->optionmenu), menu_index);

	gtk_widget_set_sensitive (dialog->button_remove, TRUE);
	gtk_widget_set_sensitive (dialog->button_update, changed);

	g_free (status);
}

static void on_optionmenu_cb (GtkMenuItem *menuitem, GjPresencePresetsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeSelection *selection = NULL;
	
	GList *l = NULL;
	
	gpointer p = NULL;
	guint show = 0;

	p = g_object_get_data (G_OBJECT (menuitem), "presence_show");
	show = GPOINTER_TO_UINT (p);

	dialog->presence_show = show;

	view = GTK_TREE_VIEW (dialog->treeview);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	l = gtk_tree_selection_get_selected_rows (selection, NULL);

	if (g_list_length (l) > 0) {
		gtk_widget_set_sensitive (dialog->button_update, TRUE);
	}

	g_list_foreach (l, (GFunc)gtk_tree_path_free, NULL);
	g_list_free (l);
}

/* GUI events */
static void on_entry_changed (GtkEditable *editable, GjPresencePresetsDialog *dialog)
{
	G_CONST_RETURN gchar *text = NULL;

	text = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	gtk_widget_set_sensitive (dialog->button_add, (text && strlen (text) > 0));
	gtk_widget_set_sensitive (dialog->button_update, (text && strlen (text) > 0));
}

static void on_spinbutton_value_changed (GtkSpinButton *spinbutton, GjPresencePresetsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeSelection *selection = NULL;
	
	GList *l = NULL;

	view = GTK_TREE_VIEW (dialog->treeview);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	l = gtk_tree_selection_get_selected_rows (selection, NULL);

	if (g_list_length (l) > 0) {
		gtk_widget_set_sensitive (dialog->button_update, TRUE);
	}

	g_list_foreach (l, (GFunc)gtk_tree_path_free, NULL);
	g_list_free (l);
}

static void on_button_add_clicked (GtkButton *button, GjPresencePresetsDialog *dialog)
{
	gint index = 1;

	const gchar *show_str = NULL;
	G_CONST_RETURN gchar *text = NULL;

	gint priority = 0;
	gchar *priority_str = NULL;

	index = gj_config_get_pp_count ();

	show_str = gj_translate_presence_show (dialog->presence_show);
	text = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	priority = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton)); 
	priority_str = g_strdup_printf ("%d", priority);
    
	gj_gtk_pp_model_add (dialog, index, dialog->presence_show, priority, text);

	/* remove in config */
	gj_config_set_pp_index (index, show_str, text, priority_str);

	g_free (priority_str);

	/* reset on display */
	gtk_option_menu_set_history (GTK_OPTION_MENU (dialog->optionmenu), 0);
	gtk_entry_set_text (GTK_ENTRY (dialog->entry), "");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton), (gdouble)0);
}

static void on_button_remove_clicked (GtkButton *button, GjPresencePresetsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;

	gint index = 0;
  
	if (!dialog) {
		return;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return;
	}
  
	gtk_tree_model_get (model, &iter, 
			    COL_PP_INDEX, &index, 
			    -1);     

	/* remove on gui */
	gtk_list_store_remove (store, &iter);

	/* remove in config */
	gj_config_set_pp_index_delete (index);

	/* reset on display */
	gtk_option_menu_set_history (GTK_OPTION_MENU (dialog->optionmenu), 0);
	gtk_entry_set_text (GTK_ENTRY (dialog->entry), "");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (dialog->spinbutton), (gdouble)0);

	return;
}

static void on_button_update_clicked (GtkButton *button, GjPresencePresetsDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;

	gint index = 0;
  
	const gchar *stock_id = NULL;
	const gchar *show_str = NULL;
	G_CONST_RETURN gchar *status = NULL;

	GdkPixbuf *pb = NULL;

	gint priority = 0;
	gchar *priority_str = NULL;

	if (!dialog) {
		return;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	stock_id = gj_gtk_presence_to_stock_id (GjPresenceTypeAvailable, dialog->presence_show);
	show_str = gj_translate_presence_show (dialog->presence_show);
	status = gtk_entry_get_text (GTK_ENTRY (dialog->entry));
	priority = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton));
	pb = gj_gtk_pixbuf_new_from_stock (stock_id); 

	priority_str = g_strdup_printf ("%d", priority);

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return;
	}

	gtk_tree_model_get (model, &iter, 
			    COL_PP_INDEX, &index,
			    -1);

	/* update on gui */
	gtk_list_store_set (store, &iter, 
			    COL_PP_INDEX, &index, 
			    COL_PP_IMAGE, pb,
			    COL_PP_SHOW, dialog->presence_show, 
			    COL_PP_SHOW_STR, show_str,
			    COL_PP_STATUS, status, 
			    COL_PP_PRIORITY, priority,
			    -1);     

	/* update in config */
	gj_config_set_pp_index (index, show_str, status, priority_str);

	g_free (priority_str);
	g_object_unref (G_OBJECT (pb));
}

static void on_destroy (GtkWidget *widget, GjPresencePresetsDialog *dialog)
{
	current_dialog = NULL;

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjPresencePresetsDialog *dialog)
{
	if (!dialog) {
		return;
	}

/* 	switch (response) { */

/* 	case 1: */
/* 		on_button_update_clicked (dialog->button_update, dialog);	 */
/* 		break; */
/* 	case 2: */
/* 		on_button_update_clicked (dialog->button_update, dialog);	 */
/* 		break; */
/* 	case 3: */
/* 		on_button_update_clicked (dialog->button_update, dialog);	 */
/* 		break; */
/* 	default: */

	if (response != 1 && response != 2 && response != 3) {
		gtk_widget_destroy (dialog->window);
	}
}

#if 0
gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), _("About the place..."), -1);
gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), _("Eating"), -1);
gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), _("Out for a bit"), -1);
gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), _("In the shower"), -1);
gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), _("Sleeping... zZzZ"), -1);
gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), _("Working!") ,-1);
#endif

#ifdef __cplusplus
}
#endif
