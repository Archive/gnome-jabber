/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_main_h
#define __gj_main_h

#include <glib.h>

gboolean gnome_jabber_quit ();

/*
 * member functions
 */

/* gets */
gchar *gnome_jabber_get_config_file ();
gchar *gnome_jabber_get_glade_file ();
gchar *gnome_jabber_get_css_file ();

gchar *gnome_jabber_get_config_dir ();

gboolean gnome_jabber_get_first_time_run ();

gboolean gnome_jabber_get_debugging ();
gboolean gnome_jabber_get_log_to_screen ();

const gchar **gnome_jabber_get_server_list ();

/* sets */
gboolean gnome_jabber_set_config_file (gchar *filename);
gboolean gnome_jabber_set_glade_file (gchar *filename);
gboolean gnome_jabber_set_css_file (gchar *filename);

gboolean gnome_jabber_set_config_dir (gchar *dir);

gboolean gnome_jabber_set_first_time_run (gboolean first_time_run);

gboolean gnome_jabber_set_debugging (gboolean debugging);
gboolean gnome_jabber_set_log_to_screen (gboolean log_to_screen);

#endif
