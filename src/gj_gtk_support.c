/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_support.h"
#include "gj_main.h"
#include "gj_chat.h"

#include "gj_gtk_support.h"
#include "gj_gtk_stock.h"


static gboolean widget_redraw_when_idle_cb (gpointer user_data);

static gboolean textview_find_space_cb (gunichar ch, gpointer user_data);

static void hig_dialog_realize_cb (GtkWidget *dialog, gpointer user_data);

static gboolean hig_progress_pulse_cb (GtkWidget *progressbar);
static void hig_progress_destroy_cb (GtkWidget *dialog, gpointer pid);


#ifdef G_OS_WIN32

#include <windows.h>
#include <shellapi.h>

/*
 * Windows code
 */
const char *gj_gtk_open_url_translate_error (int err);

gboolean gj_gtk_open_url (const gchar *url)
{
	HINSTANCE err;

	if (url == NULL) {
		g_warning ("%s: url was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gnome_jabber_get_debugging ()) {
		g_message ("%s: starting browser for url:'%s'...", __FUNCTION__, url);
	}
    
	err = ShellExecute ((HWND)NULL, /* parent window */
			    "open",     /* verb */
			    url,        /* file */
			    NULL,       /* parameters */
			    NULL,       /* path */
			    SW_SHOWNORMAL);
    
	if ((gint)err <= 32) {
		g_warning ("%s: failed to execute browser, error:%d->'%s', ", __FUNCTION__,
			   (gint)err, gj_gtk_open_url_translate_error ((gint)err));
		return FALSE;
	}

	return TRUE;
}

const char *gj_gtk_open_url_translate_error (int err)
{
	switch (err) {
	case 0: return "The operating system is out of memory or resources.";
	case ERROR_FILE_NOT_FOUND: return "The specified file was not found.";
	case ERROR_PATH_NOT_FOUND: return "The specified path was not found."; 
	case ERROR_BAD_FORMAT: return "The .exe file is invalid (non-Microsoft Win32� .exe or error in .exe image).";
	case SE_ERR_ACCESSDENIED: return "The operating system denied access to the specified file."; 
	case SE_ERR_ASSOCINCOMPLETE: return "The file name association is incomplete or invalid."; 
	case SE_ERR_DDEBUSY: return "The Dynamic Data Exchange (DDE) transaction could not be completed because other DDE transactions were being processed.";
	case SE_ERR_DDEFAIL: return "The DDE transaction failed.";
	case SE_ERR_DDETIMEOUT: return "The DDE transaction could not be completed because the request timed out.";
	case SE_ERR_DLLNOTFOUND: return "The specified dynamic-link library (DLL) was not found.";
		/*     case SE_ERR_FNF: return "The specified file was not found."; */
	case SE_ERR_NOASSOC: return "There is no application associated with the given file name extension. This error will also be returned if you attempt to print a file that is not printable.";
	case SE_ERR_OOM: return "There was not enough memory to complete the operation.";
		/*     case SE_ERR_PNF: return "The specified path was not found."; */
	case SE_ERR_SHARE: return "A sharing violation occurred.";
	}

	return "unknown error";
}

#include <mmsystem.h>

gboolean gj_gtk_sound_play (const gchar *filename)
{
	if (filename == NULL) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return FALSE;
	}

	if (PlaySound (filename, NULL, SND_ASYNC) == FALSE) {
		g_warning ("%s: failed to play filename:'%s'", __FUNCTION__, filename);
		return FALSE;
	}

	if (gnome_jabber_get_debugging ()) {
		g_message ("%s: playing filename:'%s'", __FUNCTION__, filename);
	}

	return TRUE;
}


#else

#include <libgnome/libgnome.h>

/*
 * Linux code
 */
gboolean gj_gtk_open_url (const gchar *url)
{
	GError *error = NULL;
	/*     gchar *full_url = NULL; */

	if (url == NULL) {
		g_warning ("%s: url was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gnome_jabber_get_debugging ()) {
		g_message ("%s: starting browser for url:'%s'...", __FUNCTION__, url);
	}

	/*     full_url = g_strconcat ("\"", url, "\"", NULL); */
    
	if (gnome_url_show (url, &error) == FALSE) {
		g_warning ("%s: gnome_url_show failed for url:'%s'", __FUNCTION__, url);
	}
    
	/* look at error */
	if (error != NULL) {
		g_warning ("%s: error domain:'%s', code:%d, message:'%s'", __FUNCTION__, 
			   g_quark_to_string (error->domain), error->code, error->message);

		g_error_free (error);
		return FALSE;
	}

	return TRUE;    
}

gboolean gj_gtk_sound_play (const gchar *filename)
{
	if (filename == NULL) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gnome_jabber_get_debugging ()) {
		g_message ("%s: playing sound file:'%s'", __FUNCTION__, filename); 
	}

	gnome_sound_play (filename);
	return TRUE;
}

#endif


GtkWidget *gj_gtk_image_new_from_file (const gchar *filename)
{
	GtkWidget *image = NULL;

	if (filename == NULL || g_utf8_strlen (filename, -1) < 1) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return NULL;
	} else {
		if (gnome_jabber_get_debugging ()) {
			g_message ("%s: opening file:'%s'", __FUNCTION__, filename);
		}
	}

	image = gtk_image_new_from_file (filename);

	if (image == NULL) {
		g_warning ("%s: image was NULL", __FUNCTION__);
		return NULL;
	}

	return image;
}

GdkPixbuf *gj_gtk_pixbuf_new_from_file (const gchar *filename)
{
	GdkPixbuf *pb = NULL;
	GError *error = NULL;

	if (filename == NULL || g_utf8_strlen (filename, -1) < 1) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return NULL;
	} else {
		if (gnome_jabber_get_debugging ()) {
			g_message ("%s: opening file:'%s'", __FUNCTION__, filename);
		}	
	}

	pb = gdk_pixbuf_new_from_file (filename, &error);

	if (pb == NULL) {
		g_warning ("%s: error loading file:'%s'", __FUNCTION__, filename);
	
		/* look at error */
		if (error != NULL) {
			g_warning ("%s: error domain:'%s', code:%d, message:'%s'", __FUNCTION__,
				   g_quark_to_string (error->domain), error->code, error->message);

			g_error_free (error);
		}
	
		return NULL;
	}

	/* NOTE: this has a refcount of 1 it needs to be unref'd */
	return pb;
}

GdkPixbuf *gj_gtk_pixbuf_new_from_stock (const gchar *stock_item)
{
	GdkPixbuf *pb = NULL;
	GtkWidget *image = NULL;

	if (stock_item == NULL || g_utf8_strlen (stock_item, -1) < 1) {
		g_warning ("%s: stock_item was NULL", __FUNCTION__);
		return NULL;
	} else {
		if (gnome_jabber_get_debugging ()) {
			g_message ("%s: opening stock item:'%s'", __FUNCTION__, stock_item); 
		}
	}

	/* create image to have a widget to render the icon */
	image = gtk_image_new ();

	/* get pixbuf */
	pb = gtk_widget_render_icon (GTK_WIDGET (image), stock_item, GTK_ICON_SIZE_SMALL_TOOLBAR, NULL);

	if (pb == NULL) {
		g_warning ("%s: could not get pixbuf from stock item:'%s'", __FUNCTION__, stock_item);
		return NULL;
	}

	/* clean up */
	if (image != NULL) {
		gtk_object_sink (GTK_OBJECT (image)); 
	}

	return pb;
}

GdkPixbufAnimation *gj_gtk_animation_new_from_file (const gchar *filename)
{
	GdkPixbufAnimation *pba = NULL;
	GError *error = NULL;

	if (filename == NULL || g_utf8_strlen (filename, -1) < 1) {
		g_warning ("%s: filename was NULL", __FUNCTION__);
		return NULL;
	} else {
		if (gnome_jabber_get_debugging ()) {
			g_message ("%s: opening file:'%s'", __FUNCTION__, filename);
		}
	}

	pba = gdk_pixbuf_animation_new_from_file (filename, &error);

	if (pba == NULL) {
		g_warning ("%s: error loading file:'%s'", __FUNCTION__, filename);
	
		/* look at error */
		if (error != NULL) {
			g_warning ("%s: error domain:'%s', code:%d, message:'%s'", __FUNCTION__, 
				   g_quark_to_string (error->domain), error->code, error->message);

			g_error_free (error);
		}
	
		return NULL;
	}

	/* NOTE: this has a refcount of 1 it needs to be unref'd */
	return pba;
}

#ifdef G_OS_WIN32

# include <gdk/gdk.h> 
# include <gdk/gdkwin32.h>
# include <gdk-pixbuf/gdk-pixbuf.h>
# include <windows.h>

HICON gj_gtk_pixbuf_to_hicon (GdkPixbuf *pb)
{
	/*   GdkScreen *screen = NULL; */
  
	GdkPixmap *pm = NULL;
	GdkBitmap *bm = NULL;

	if (!pb) {
		g_warning ("%s: pixbuf was NULL", __FUNCTION__);
		return NULL;
	}

	/*   screen = gdk_drawable_get_screen (NULL); */

	gdk_pixbuf_render_pixmap_and_mask (pb, 
					   &pm,
					   &bm,
					   128);

	/*   gdk_pixbuf_render_pixmap_and_mask_for_colormap (pb,  */
	/* 						  gdk_screen_get_system_colormap (screen), */
	/* 						  &pm, */
	/* 						  &bm, */
	/* 						  128); */
  
	return gj_gtk_pixmap_to_hicon (pm, bm);
}

/*
 * taken and modified from gdk/win32/gdkwindow-win32.c
 */
HICON gj_gtk_pixmap_to_hicon (GdkPixmap *pixmap, GdkBitmap *mask)
{
	ICONINFO ii;
	HICON hIcon;
	gint w = 0, h = 0;
	HANDLE hpixmap = NULL;
	HBITMAP hbitmap = NULL;
    
	/* Create Drawing Context */
	HDC hdc1 = NULL;
	HBITMAP hbitmaptmp1 = NULL;

	if (!pixmap) {
		g_warning ("%s: pixmap was NULL", __FUNCTION__);
		return NULL;
	}

	gdk_drawable_get_size (GDK_DRAWABLE (pixmap), &w, &h);
	g_message ("%s: width:%d, height:%d", __FUNCTION__, w, h);
    
	hpixmap = gdk_win32_drawable_get_handle (pixmap); 

	/* we need the inverted mask for the XOR op */
	hdc1 = CreateCompatibleDC (NULL);
	hbitmap = CreateCompatibleBitmap (hdc1, w, h);
	hbitmaptmp1 = SelectObject (hdc1, hbitmap);

	if (mask) {
		HANDLE hmask = NULL;
		HDC hdc2 = NULL;
		HBITMAP hbitmaptmp2 = NULL;
      
		hmask = gdk_win32_drawable_get_handle (mask); 

		hdc2 = CreateCompatibleDC (NULL);
		hbitmaptmp2 = SelectObject (hdc2, hmask);
      
		BitBlt (hdc1, 0,0,w,h, hdc2, 0,0, NOTSRCCOPY);    
      
		SelectObject (hdc2, hbitmaptmp2);
		DeleteDC (hdc2);
      
		g_message ("%s: USING mask", __FUNCTION__);
	} else {
		RECT rect;
		GetClipBox (hdc1, &rect);
		FillRect (hdc1, &rect, GetStockObject (WHITE_BRUSH));

		g_message ("%s: NO mask", __FUNCTION__);
	}

	SelectObject (hdc1, hbitmaptmp1); 
	DeleteDC (hdc1); 
 
	ii.fIcon = TRUE;
	ii.xHotspot = ii.yHotspot = 0; /* ignored for icons */
	ii.hbmMask = hbitmap;
	ii.hbmColor = hpixmap; 

	hIcon = CreateIconIndirect (&ii);
	if (!hIcon) {
		g_warning ("%s: CreateIconIndirect failed", __FUNCTION__);
		return NULL;
	}

#if 0 /* to debug pixmap and mask setting */
	{
		GdkPixbuf* pixbuf = NULL;
		char name[256]; /* to debug pixmap and mask setting */
		static gint num = 0;
 
		pixbuf = gdk_pixbuf_get_from_drawable (NULL, pixmap, NULL, 0, 0, 0, 0, w, h);
		if (pixbuf) {
			num = (num + 1) % 999; /* restrict maximim number */
			sprintf (name, "c:\\temp\\ico%03dpixm.png", num); 
			gdk_pixbuf_save (pixbuf, name, "png", NULL, NULL);
			gdk_pixbuf_unref (pixbuf);
		}
		pixbuf = !mask ? NULL : gdk_pixbuf_get_from_drawable (NULL, mask, NULL, 0, 0, 0, 0, w, h);
		if (pixbuf) {
			sprintf (name, "c:\\temp\\ico%03dmask.png", num);
			gdk_pixbuf_save (pixbuf, name, "png", NULL, NULL);
			gdk_pixbuf_unref (pixbuf);
		}
	}
#endif

	DeleteObject (hbitmap);

	return hIcon;
}


#endif

gboolean gj_gtk_find_in_str (const gchar *needle, const gchar *haystack)
{
	gchar *result = NULL;

	if (needle == NULL) {
		g_warning ("%s: needle is NULL", __FUNCTION__);
		return FALSE;
	}

	if (haystack == NULL) {
		g_warning ("%s: haystack is NULL", __FUNCTION__);
		return FALSE;
	}

	if ((result = g_strrstr (haystack, needle)) == NULL) {
		return FALSE;
	}
    
	return TRUE;
}

gchar *gj_gtk_filter_number (const gchar *original_number)
{
	gchar *number_filtered = NULL;
	gchar *number = NULL;
	gchar **vnumber = NULL;

	/* checks */
	if (original_number == NULL) {
		g_warning ("%s: original_number is NULL", __FUNCTION__);
		return NULL;
	}  

	/* remove all except numbers */
	number = g_strdup (original_number);
	number = g_strcanon (number, "0123456789", 0x20);
	vnumber = g_strsplit (number, " ", -1);
    
	if (vnumber != NULL) {
		number_filtered = g_strjoinv (NULL, vnumber);
		g_strfreev (vnumber);
		vnumber = NULL;
	}

	/* clean up */
	g_free (number);

	return number_filtered;
}

void gj_gtk_set_label_with_args (GtkWidget *widget, const gchar *fmt, ...)
{
	va_list args;
	gchar *buffer = NULL;

	if (widget == NULL) {
		g_warning ("%s: widget was NULL", __FUNCTION__);
		return;
	}
  
	va_start (args,fmt);
	buffer = g_strdup_vprintf (fmt,args);
	va_end (args);

	if (buffer == NULL) {
		g_warning ("%s: buffer to use was NULL", __FUNCTION__);
		return;
	}

	gtk_label_set_markup (GTK_LABEL (widget),buffer);
	g_free (buffer);
}

void gj_gtk_set_entry_with_args (GtkWidget *widget, const gchar *fmt, ...)
{
	va_list args;
	gchar *buffer = NULL;

	if (widget == NULL) {
		g_warning ("%s: widget was NULL", __FUNCTION__);
		return;
	}
  
	va_start (args,fmt);
	buffer = g_strdup_vprintf (fmt,args);
	va_end (args);

	if (buffer == NULL) {
		g_warning ("%s: buffer to use was NULL", __FUNCTION__);
		return;
	}

	gtk_entry_set_text (GTK_ENTRY (widget), buffer);
	g_free (buffer);
}

void gj_gtk_set_progress_bar_label_with_args (GtkWidget *widget, const gchar *fmt, ...)
{
	va_list args;
	gchar *buffer = NULL;

	if (widget == NULL) {
		g_warning ("%s: widget was NULL", __FUNCTION__);
		return;
	}
  
	va_start (args,fmt);
	buffer = g_strdup_vprintf (fmt,args);
	va_end (args);

	if (buffer == NULL) {
		g_warning ("%s: buffer to use was NULL", __FUNCTION__);
		return;
	}

	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (widget), buffer);
	g_free (buffer);
}

void gj_gtk_set_window_title_with_args (GtkWidget *widget, const gchar *fmt, ...)
{
	va_list args;
	gchar *buffer = NULL;

	if (widget == NULL) {
		g_warning ("%s: widget was NULL", __FUNCTION__);
		return;
	}
  
	va_start (args,fmt);
	buffer = g_strdup_vprintf (fmt,args);
	va_end (args);

	if (buffer == NULL) {
		g_warning ("%s: buffer to use was NULL", __FUNCTION__);
		return;
	}

	gtk_window_set_title (GTK_WINDOW (widget), buffer);
	g_free (buffer);
}

GladeXML *gj_gtk_glade_get_file (const gchar *filename,
				 const gchar *root,
				 const gchar *domain,
				 const gchar *first_widget, 
				 ...)
{
	GladeXML *gui = NULL;
	const char *name = NULL;
	GtkWidget **widget_ptr;
	va_list args;
  
	va_start (args, first_widget);
  
	gui = glade_xml_new (filename, root, domain);
	if (!gui) {
		g_warning ("%s: couldn't find necessary glade file:'%s'", __FUNCTION__, filename);
		return NULL;
	}
  
	for (name = first_widget; name; name = va_arg (args, char *)) {
		widget_ptr = va_arg (args, void *);

		if (!widget_ptr) {
			continue;
		}

		*widget_ptr = glade_xml_get_widget (gui, name);
		if (!*widget_ptr) {
			g_warning ("%s: glade file:'%s' is missing widget:'%s'.", __FUNCTION__, filename, name);
			continue;
		}
	}

	va_end (args);
 
	if (!gui) {
		return NULL; 
	}

	return gui;
}

void gj_gtk_glade_connect (GladeXML *gui,
			   gpointer user_data,
			   gchar *first_widget, 
			   ...)
{
	va_list args;
	const gchar *name = NULL;
	const gchar *signal = NULL;
	GtkWidget *widget = NULL;
	gpointer *callback = NULL;

	va_start (args, first_widget);
	
	for (name = first_widget; name; name = va_arg (args, char*)) {
		signal = va_arg (args, void*);
		callback = va_arg (args, void*);
      
		if (! (widget = glade_xml_get_widget (gui, name))) {
			g_warning ("%s: glade file is missing widget:'%s', aborting...", __FUNCTION__, name);
			continue;
		}
      
		g_signal_connect (widget,
				  signal,
				  G_CALLBACK (callback),
				  user_data);
	}
  
	va_end (args);
}


gboolean gj_gtk_textview_handle_motion_notify_event (GtkWidget *widget,
						     GdkEventMotion *event, 
						     gpointer user_data)
{
	GdkModifierType state = 0;
	GdkWindow *window = NULL;

	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter;
	GtkTextIter iter_start;
	GtkTextIter iter_end;
	GtkTextWindowType text_window_type = 0;

	gboolean found_start = FALSE;
	gboolean found_end = FALSE;

	gint pointer_x = 0;
	gint pointer_y = 0;
	gint x = 0;
	gint y = 0;

	gchar *buf = NULL;

	if (event->is_hint) {
		window = gdk_window_get_pointer (event->window, &pointer_x, &pointer_y, &state);
	} else {
		pointer_x = event->x;
		pointer_y = event->y;
		state = event->state;
	}
  
	if ((textview = GTK_TEXT_VIEW (widget)) == NULL) {
		g_warning ("%s: textview was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((buffer = GTK_TEXT_BUFFER (gtk_text_view_get_buffer (textview))) == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return FALSE;
	}

	text_window_type = gtk_text_view_get_window_type (textview, event->window); 
	gtk_text_view_window_to_buffer_coords (textview, text_window_type, pointer_x, pointer_y, &x, &y);
	gtk_text_view_get_iter_at_location (textview, &iter, x, y);

	if (g_unichar_isspace (gtk_text_iter_get_char (&iter)) == TRUE) {
		gdk_window_set_cursor (event->window, NULL);
		return FALSE;
	}

	iter_start = iter;
	iter_end = iter;

	/* search for next space */
	found_start = gtk_text_iter_backward_find_char (&iter_start, textview_find_space_cb, NULL, NULL);
	found_end = gtk_text_iter_forward_find_char (&iter_end, textview_find_space_cb, NULL, NULL);

	if (found_start == FALSE && found_end == FALSE) {
		gdk_window_set_cursor (event->window, NULL);
		return FALSE;
	}

	if (gtk_text_iter_is_start (&iter_start) == FALSE) {
		gtk_text_iter_forward_char (&iter_start);
	}
  
	if ((buf = gtk_text_buffer_get_text (buffer, &iter_start, &iter_end, FALSE)) == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (gj_is_uri (buf) != GjURITypeUnknown) {
		/* width wise it works... */
		GdkCursor *cursor = NULL;
		cursor = gdk_cursor_new (GDK_HAND2);
		gdk_window_set_cursor (event->window, cursor);
      
		/* clean up */
		gdk_cursor_unref (cursor);
	} else {
		/* return to parent window's cursor */
		gdk_window_set_cursor (event->window, NULL);
	}

      
	/* clean up */
	g_free (buf);

	return FALSE;
}

gboolean gj_gtk_textview_handle_button_press_event (GtkWidget *widget, 
						    GdkEventButton *event, 
						    gpointer user_data)
{
	GdkModifierType state;
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter;
	GtkTextIter iter_start;
	GtkTextIter iter_end;
	GtkTextWindowType text_window_type = 0;

	gboolean found_start = FALSE;
	gboolean found_end = FALSE;
    
	gint pointer_x = 0;
	gint pointer_y = 0;
	gint x = 0;
	gint y = 0;

	gchar *buf = NULL;

	/* if not mouse 1 return */
	if (event->button != 1) {
		return FALSE;
	}

	gdk_window_get_pointer (event->window, &pointer_x, &pointer_y, &state);
    
	if ((buffer = GTK_TEXT_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget)))) == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return FALSE;
	}
    
	text_window_type = gtk_text_view_get_window_type (GTK_TEXT_VIEW (widget), event->window); 
	gtk_text_view_window_to_buffer_coords (GTK_TEXT_VIEW (widget), text_window_type, pointer_x, pointer_y, &x, &y);
	gtk_text_view_get_iter_at_location (GTK_TEXT_VIEW (widget), &iter, x, y);
 
	iter_start = iter;
	iter_end = iter;
    
	/* search for next space */
	found_start = gtk_text_iter_backward_find_char (&iter_start, textview_find_space_cb, NULL, NULL);
	found_end = gtk_text_iter_forward_find_char (&iter_end, textview_find_space_cb, NULL, NULL);
    
	if (found_start == FALSE && found_end == FALSE) {
		return FALSE;
	}
    
	if (gtk_text_iter_is_start (&iter_start) == FALSE) {
		gtk_text_iter_forward_char (&iter_start);
	}
    
	if ((buf = gtk_text_buffer_get_text (buffer, &iter_start, &iter_end, FALSE)) == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (gj_is_uri (buf) != GjURITypeUnknown) {
		gj_gtk_open_url (buf);
	}
    
	/* clean up */
	g_free (buf);

	return FALSE;
}

void gj_gtk_textbuffer_handle_changed (GtkTextBuffer *textbuffer, gpointer user_data)
{
	GtkTextMark *mark = NULL;
	GtkTextIter iter_space;
	GtkTextIter iter_word_start;
	GtkTextIter iter_word_end;
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	gint length = 0;

	/* check WHOLE buffer for urls */
	gtk_text_buffer_get_bounds (textbuffer, &iter_start, &iter_end);
	length = gtk_text_buffer_get_char_count (textbuffer);

	if ((mark = gtk_text_buffer_get_mark (textbuffer, "url")) != NULL)
		gtk_text_buffer_get_iter_at_mark (textbuffer, &iter_start, mark);

	/* save last iter */
	iter_word_start = iter_start;
	iter_word_end = iter_start;
	iter_space = iter_start;

	while (TRUE) {
		gboolean found = FALSE;
		gchar *buf = NULL;
		
		/* search for next space */
		found = gtk_text_iter_forward_find_char (&iter_space, textview_find_space_cb, NULL, NULL);
		
		if (found == FALSE) {
			break; 
		}
		
		/* set word end to the space */
		iter_word_start = iter_word_end;
		iter_word_end = iter_space;
		
		/* move word end iter forward 1 */
		gtk_text_iter_forward_char (&iter_word_start);
		
		/* get */
		buf = gtk_text_iter_get_text (&iter_word_start, &iter_word_end);
		
		if (buf != NULL) {
			if (gj_is_uri (buf) != GjURITypeUnknown) {
				/* highlight as uri */
				gtk_text_buffer_apply_tag_by_name (textbuffer, "url", &iter_word_start, &iter_word_end);
			} else {
				/* none highlighted */
				gtk_text_buffer_remove_tag_by_name (textbuffer, "url", &iter_word_start, &iter_word_end);
			}
			
			g_free (buf);
		}
		
		/* set mark, then we can check next time from 
		   the last mark and it is more efficient */
		if ((mark = gtk_text_buffer_get_mark (textbuffer, "url")) == NULL) {
			if ((mark = gtk_text_buffer_create_mark (textbuffer, "url", &iter_space, FALSE)) == NULL) {
				g_warning ("%s: could not create mark!", __FUNCTION__);
				return;
			}
			
			/* no need to move it, just continue */
			continue;
		}
		
		/* move mark to latest iter */
		gtk_text_buffer_move_mark_by_name (textbuffer, "url", &iter_space);
	}
}

static gboolean textview_find_space_cb (gunichar ch, gpointer user_data)
{
	if (g_unichar_isspace (ch) == TRUE) {
		return TRUE;
	}

	return FALSE;
}

gboolean gj_gtk_textbuffer_insert_with_emote_icons (GtkTextBuffer *buffer, 
						    GtkTextIter *iter,
						    const gchar *tag,
						    const gchar *message)
{
	GdkPixbuf *pb = NULL;
	gint i = 0;

	if (!buffer || !iter || !message) {
		return FALSE;
	}

	/* search for occurences of emote icons */
	for (i=0;i<g_utf8_strlen (message, -1);i++) {
		gchar *p = NULL;
		p = g_utf8_offset_to_pointer (message, i);
		
		if (strncmp (p, GJ_CHAT_EMOTE_BIG_GRIN, g_utf8_strlen (GJ_CHAT_EMOTE_BIG_GRIN,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteBigGrin)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_BIG_GRIN,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_CONFUSED, g_utf8_strlen (GJ_CHAT_EMOTE_CONFUSED,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteConfused)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_CONFUSED,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_COOL, g_utf8_strlen (GJ_CHAT_EMOTE_COOL,-1)) == 0)	{
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteCool)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_COOL,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_EEK, g_utf8_strlen (GJ_CHAT_EMOTE_EEK,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteEek)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_EEK,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_FROWN, g_utf8_strlen (GJ_CHAT_EMOTE_FROWN,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteFrown)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_FROWN,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_MAD, g_utf8_strlen (GJ_CHAT_EMOTE_MAD,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteMad)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_MAD,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_RED_FACE, g_utf8_strlen (GJ_CHAT_EMOTE_RED_FACE,-1)) == 0)	{
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteRedFace)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_RED_FACE,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_ROLL_EYES, g_utf8_strlen (GJ_CHAT_EMOTE_ROLL_EYES,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteRollEyes)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_ROLL_EYES,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_SMILE, g_utf8_strlen (GJ_CHAT_EMOTE_SMILE,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteSmile)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_SMILE,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_TONGUE, g_utf8_strlen (GJ_CHAT_EMOTE_TONGUE,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteTongue)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_TONGUE,-1)-1);
		} else if (strncmp (p, GJ_CHAT_EMOTE_WINK, g_utf8_strlen (GJ_CHAT_EMOTE_WINK,-1)) == 0) {
			if ((pb = gj_chat_emote_icon_get_as_pixbuf (GjChatEmoteWink)) != NULL) {
				gtk_text_buffer_insert_pixbuf (buffer, iter, pb);
				g_object_unref (G_OBJECT (pb));
			}
			
			i += (g_utf8_strlen (GJ_CHAT_EMOTE_WINK,-1)-1);
		} else {
			gchar *str = NULL;
			
			/* create mem - utf should be no longer than 6 bytes */
			str = g_new0 (char, 10);
			
			/* copy char */
			str = g_utf8_strncpy (str, p, 1);
			
			if (tag == NULL) {
				gtk_text_buffer_insert (buffer, iter, str, -1);
			} else {
				gtk_text_buffer_insert_with_tags_by_name (buffer, iter, str, -1, tag, NULL);
			}
			g_free (str);
		}
	}

	return TRUE;
}

gboolean gj_gtk_textbuffer_insert (GtkTextBuffer *buffer, 
				   GtkTextIter *iter,
				   const gchar *tag,
				   const gchar *message)
{
	if (!buffer || !iter || !message) 
		return FALSE;

	if (tag == NULL) {
		gtk_text_buffer_insert (buffer, iter, message, -1);
	} else {
		gtk_text_buffer_insert_with_tags_by_name (buffer, iter, message, -1, tag, NULL); 
	}
  
	return TRUE;
}

const gchar *gj_gtk_presence_to_stock_id (GjPresenceType type, GjPresenceShow show)
{
	const gchar *stock_id = NULL;

	if (type != GjPresenceTypeAvailable) {
		switch (type) {
		case GjPresenceTypeUnAvailable:
			stock_id = GJ_STOCK_PRESENCE_UNAVAILABLE;
			break;
		default:
		case GjPresenceTypeNone:
			stock_id = GJ_STOCK_PRESENCE_NONE;
			break;
		}
	} else {
		switch (show) {
		case GjPresenceShowAway:
			stock_id = GJ_STOCK_PRESENCE_AWAY;
			break;
		case GjPresenceShowChat:
			stock_id = GJ_STOCK_PRESENCE_CHAT;
			break;
		case GjPresenceShowDND:
			stock_id = GJ_STOCK_PRESENCE_DND;
			break;
		case GjPresenceShowXA:
			stock_id = GJ_STOCK_PRESENCE_XA;
			break;
		case GjPresenceShowNormal:
			stock_id = GJ_STOCK_PRESENCE_AVAILABLE;
			break;
		case GjPresenceShowInvisible:
			stock_id = GJ_STOCK_PRESENCE_INVISIBLE;
			break;
			
		default:
			g_warning ("%s: failed to determin presence 'show'", __FUNCTION__);
			break;
		}
	}
	
	return stock_id;
}

GdkPixbuf *gj_gtk_presence_to_pixbuf_new (GjPresenceType type, GjPresenceShow show)
{
	GdkPixbuf *pb = NULL;
	const gchar *stock_id = NULL;
  
	if ((stock_id = gj_gtk_presence_to_stock_id (type, show)) != NULL) {
		pb = gj_gtk_pixbuf_new_from_stock (stock_id);
	}

	return pb;
}

gchar *gj_gtk_presence_to_show_text_new (GjPresenceType type, GjPresenceShow show)
{
	gchar *text = NULL;

	if (type != GjPresenceTypeAvailable) {
		switch (type) {
		case GjPresenceTypeUnAvailable:
			text = g_strdup_printf (_("Disconnected"));
			break;

		default:
			g_warning ("%s: failed to determin presence 'type' from %d", __FUNCTION__, type);
			break;
		}
	} else {
		switch (show) {
		case GjPresenceShowAway:
			text = g_strdup_printf (_("Away"));
			break;
		case GjPresenceShowChat:
			text = g_strdup_printf (_("Chat"));
			break;
		case GjPresenceShowDND:
			text = g_strdup_printf (_("Busy"));
			break;
		case GjPresenceShowNormal:
			text = g_strdup_printf (_("Available"));
			break;
		case GjPresenceShowXA:
			text = g_strdup_printf (_("Not Available"));
			break;
		case GjPresenceShowInvisible:
			text = g_strdup_printf (_("Invisible"));
			break;
			
		default:
			g_warning ("%s: failed to determin presence 'show' from %d", __FUNCTION__, show);
			break;
		}
	}
	
	return text;
}

gchar *gj_gtk_presence_to_status_text_new (GjPresenceType type, GjPresenceShow show)
{
	gchar *text = NULL;
	
	if (type != GjPresenceTypeAvailable) {
		switch (type) {
		case GjPresenceTypeUnAvailable:
			text = g_strdup_printf (_("Disconnected"));
			break;
			
		default:
			g_warning ("%s: failed to determin presence 'type' from %d", __FUNCTION__, type);
			return NULL;
		}
	} else {
		switch (show) {
		case GjPresenceShowAway:
			text = g_strdup_printf (_("Away (be right back)")); 
			break;
		case GjPresenceShowChat:
			text = g_strdup_printf (_("Chat (open to talk...)")); 
			break;
		case GjPresenceShowDND:
			text = g_strdup_printf (_("Busy!")); 
			break;
		case GjPresenceShowNormal:
			text = g_strdup_printf (_("Available"));
			break;
		case GjPresenceShowXA:
			text = g_strdup_printf (_("Not Available")); 
			break;
		case GjPresenceShowInvisible:
			text = g_strdup_printf (_("Invisible")); 
			break;
			
		default:
			g_warning ("%s: failed to determin presence 'show' from %d", __FUNCTION__, show);
			return NULL;
		}
	}
	
	return text;
}

const gchar *gj_gtk_message_to_stock_id (GjMessageType type)
{
	const gchar *stock_id = NULL;

	switch (type) {
	case GjMessageTypeNormal:
		stock_id = GJ_STOCK_MESSAGE_NORMAL;
		break;
	case GjMessageTypeChat:
		stock_id = GJ_STOCK_MESSAGE_CHAT;
		break;
	case GjMessageTypeGroupChat:
		stock_id = GJ_STOCK_MESSAGE_GROUPCHAT;
		break;
	case GjMessageTypeHeadline:
		stock_id = GJ_STOCK_MESSAGE_HEADLINE;
		break;
	case GjMessageTypeError:
	case GjMessageTypeXEvent:
		g_message ("%s: not doing anything with GjMessageTypeError, this is handled by the event viewer now.", __FUNCTION__);
		break;
      
		/*       file = FILE_MESSAGE_ERROR; */
		/*       break; */
      
	default:
		g_warning ("%s: failed to determin message 'type'", __FUNCTION__);
		return FALSE;
	}
  
	return stock_id;
}

GdkPixbuf *gj_gtk_message_to_pixbuf_new (GjMessageType type)
{
	GdkPixbuf *pb = NULL;
	const gchar *stock_id = NULL;

	if ((stock_id = gj_gtk_message_to_stock_id (type)) != NULL) {
		pb = gj_gtk_pixbuf_new_from_stock (stock_id);
	}

	return pb;
}

GtkWidget *gj_gtk_presence_menu_new (GjMenuCallback cb, gpointer user_data)
{
	GtkWidget *menu = NULL;
	GtkWidget *item = NULL;
	GtkWidget *image = NULL;

	menu = gtk_menu_new ();
  
	item = gtk_image_menu_item_new_with_mnemonic (_("Available"));
	image = gtk_image_new_from_stock (GJ_STOCK_PRESENCE_AVAILABLE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (GjPresenceShowNormal));
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (cb), user_data);
	
	item = gtk_image_menu_item_new_with_mnemonic (_("Chat"));
	image = gtk_image_new_from_stock (GJ_STOCK_PRESENCE_CHAT, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (GjPresenceShowChat));
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (cb), user_data);
	
	item = gtk_image_menu_item_new_with_mnemonic (_("Away"));
	image = gtk_image_new_from_stock (GJ_STOCK_PRESENCE_AWAY, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (GjPresenceShowAway));
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (cb), user_data);

	item = gtk_image_menu_item_new_with_mnemonic (_("Not Available"));
	image = gtk_image_new_from_stock (GJ_STOCK_PRESENCE_XA, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (GjPresenceShowXA));
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (cb), user_data);

	item = gtk_image_menu_item_new_with_mnemonic (_("Busy"));
	image = gtk_image_new_from_stock (GJ_STOCK_PRESENCE_DND, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (GjPresenceShowDND));
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (cb), user_data);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_image_menu_item_new_with_mnemonic (_("Invisible"));
	image = gtk_image_new_from_stock (GJ_STOCK_PRESENCE_UNAVAILABLE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (GjPresenceShowInvisible));
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (cb), user_data);
  
	/* show */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_widget_set_sensitive (GTK_WIDGET (menu), TRUE);

	return menu;

}

GtkWidget *gj_gtk_emote_menu_new (GjMenuCallback cb, gpointer user_data)
{
	GtkWidget *menu = NULL;
	GtkWidget *item = NULL;
	GtkWidget *image = NULL;

	menu = gtk_menu_new ();

	item = gtk_image_menu_item_new_with_mnemonic (_("Big Grin"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_BIGGRIN, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteBigGrin));

	item = gtk_image_menu_item_new_with_mnemonic (_("Confused"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_CONFUSED, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteConfused));

	item = gtk_image_menu_item_new_with_mnemonic (_("Cool"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_COOL, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteCool));

	item = gtk_image_menu_item_new_with_mnemonic (_("Eek"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_EEK, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteEek));

	item = gtk_image_menu_item_new_with_mnemonic (_("Frown"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_FROWN, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteFrown));

	item = gtk_image_menu_item_new_with_mnemonic (_("Mad"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_MAD, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteMad));

	item = gtk_image_menu_item_new_with_mnemonic (_("Red Face"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_REDFACE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteRedFace));

	item = gtk_image_menu_item_new_with_mnemonic (_("Roll Eyes"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_ROLLEYES, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteRollEyes));

	item = gtk_image_menu_item_new_with_mnemonic (_("Smile"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_SMILE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteSmile));  

	item = gtk_image_menu_item_new_with_mnemonic (_("Tongue"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_TONGUE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteTongue));

	item = gtk_image_menu_item_new_with_mnemonic (_("Wink"));
	image = gtk_image_new_from_stock (GJ_STOCK_EMOTE_WINK, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	if (user_data) {
		g_object_set_data (G_OBJECT (item), "user_data", user_data); 
	}
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", 
			  G_CALLBACK (cb), 
			  GINT_TO_POINTER (GjChatEmoteWink));
  
	/* show */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_widget_set_sensitive (GTK_WIDGET (menu), TRUE);

	return menu;
}

void gj_gtk_option_menu_setup (GtkWidget *option_menu, GCallback cb, gpointer user_data, gconstpointer first_str, ...)
{
	va_list args;

	const gchar *str;
	const gchar *stock_id;

	gint i = 0;
	gint type = 0;

	GtkWidget *menu = NULL;
	GtkWidget *item = NULL;
	GtkWidget *image = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *label = NULL;
    
	menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (option_menu));
	if (menu) {
		gtk_widget_destroy (menu); 
	}

	/* new menu */
	menu = gtk_menu_new ();
    
	/* start args */
	va_start (args, first_str);

	for (str=first_str, i=0; str!=NULL; str=va_arg (args, gpointer), i++) {
		stock_id = va_arg (args, gpointer);
		
		if (( (gchar *)str)[0] == 0) {
			/* separator */
			item = gtk_separator_menu_item_new ();
		} else {
			hbox = gtk_hbox_new (FALSE, 4);

			image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_MENU);
			gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, TRUE, 0);
	    
			label = gtk_label_new (str);
			gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 0);
	    
			item = gtk_menu_item_new ();
	    
			gtk_container_add (GTK_CONTAINER (item), hbox);
		}
	
		gtk_widget_show_all (item);
		gtk_menu_append (GTK_MENU (menu), item);
	
		type = va_arg (args, gint);
		g_object_set_data (G_OBJECT (item), "data", GINT_TO_POINTER (type));

		if (cb) {
			g_signal_connect (item, "activate", cb, user_data); 
		}
	}

	va_end (args);
    
	gtk_widget_show (menu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
}

/* stolen from gsearchtool */
GtkWidget *gj_gtk_hig_dialog_new (GtkWindow *parent,
				  GtkDialogFlags flags,
				  GtkMessageType type,
				  GtkButtonsType buttons,
				  const gchar *header,
				  const gchar *messagefmt,
				  ...)
{
	GtkWidget *dialog = NULL;
	GtkWidget *dialog_vbox = NULL;
	GtkWidget *dialog_action_area = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *vbox = NULL;
	GtkWidget *label = NULL;
	GtkWidget *button = NULL;
	GtkWidget *image = NULL;
	gchar *title = NULL;
	va_list args;
	gchar *msg = NULL;
	gchar *hdr = NULL;
    
	if (messagefmt && strlen (messagefmt) > 0) {
		va_start (args, messagefmt);
		msg = g_strdup_vprintf (messagefmt, args);
		va_end (args);
	} else {
		msg = NULL;
	}
    
	hdr = g_markup_escape_text (header, -1);
    
	dialog = gtk_dialog_new ();
    
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_window_set_title (GTK_WINDOW (dialog), " ");
    
	dialog_vbox = GTK_DIALOG (dialog)->vbox;
	gtk_box_set_spacing (GTK_BOX (dialog_vbox), 12);
    
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), hbox, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
	gtk_widget_show (hbox);
    
	if (type == GTK_MESSAGE_ERROR) {
		image = gtk_image_new_from_stock ("gtk-dialog-error", GTK_ICON_SIZE_DIALOG);
	} else if (type == GTK_MESSAGE_QUESTION) {
		image = gtk_image_new_from_stock ("gtk-dialog-question", GTK_ICON_SIZE_DIALOG);
	} else if (type == GTK_MESSAGE_INFO) {
		image = gtk_image_new_from_stock ("gtk-dialog-info", GTK_ICON_SIZE_DIALOG);
	} else if (type == GTK_MESSAGE_WARNING) {
		image = gtk_image_new_from_stock ("gtk-dialog-warning", GTK_ICON_SIZE_DIALOG);
	} else {
		image = NULL;
		g_assert_not_reached ();
	}
    
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_widget_show (image);
    
	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
	gtk_widget_show (vbox);
    
	title = g_strconcat ("<span weight='bold' size='larger'>", hdr, "</span>\n", NULL);
	label = gtk_label_new (title);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_widget_show (label);
	g_free (title);
    
	if (msg && strlen (msg) > 0) {
		label = gtk_label_new (msg);
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_widget_show (label);
	}
    
	dialog_action_area = GTK_DIALOG (dialog)->action_area;
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area), GTK_BUTTONBOX_END);
    
	switch (buttons) {
	case GTK_BUTTONS_NONE:
		break;
	
	case GTK_BUTTONS_OK:
	
		button = gtk_button_new_from_stock (GTK_STOCK_OK);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
	
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_OK);
		break;
	
	case GTK_BUTTONS_CLOSE:
	
		button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CLOSE);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
	
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_CLOSE);
		break;
	
	case GTK_BUTTONS_CANCEL:
	
		button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CANCEL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
	
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_CLOSE);
		break;
	
	case GTK_BUTTONS_YES_NO:
	
		button = gtk_button_new_from_stock (GTK_STOCK_NO);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_NO);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
	
		button = gtk_button_new_from_stock (GTK_STOCK_YES);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_YES);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_widget_show (button);
	
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_YES);
		break;
	
	
	case GTK_BUTTONS_OK_CANCEL:
	
		button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
		gtk_widget_show (button);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_CANCEL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	
		button = gtk_button_new_from_stock (GTK_STOCK_OK);
		gtk_widget_show (button);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
	
		gtk_dialog_set_default_response (GTK_DIALOG (dialog),
						 GTK_RESPONSE_OK);
		break;
	
	default:
		g_assert_not_reached ();
		break;
	}
    
	if (parent != NULL) {
		gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	}

	if (flags & GTK_DIALOG_MODAL) {
		gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	}

	if (flags & GTK_DIALOG_DESTROY_WITH_PARENT) {
		gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog), TRUE);
	}
    
	g_signal_connect ((gpointer) dialog, "realize",
			  G_CALLBACK (hig_dialog_realize_cb),
			  NULL);

	g_free (msg);
	g_free (hdr);
    
	return dialog;
}

GtkWidget *gj_gtk_hig_progress_new (GtkWindow *parent,
				    GtkDialogFlags flags,
				    GCallback destroy_cb,
				    gpointer user_data,
				    const gchar *title,
				    const gchar *header,
				    const gchar *messagefmt,
				    ...)
{
	GtkWidget *progress = NULL;
	GtkWidget *dialog_vbox = NULL;
	GtkWidget *vbox = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *label = NULL;
	GtkWidget *progressbar = NULL;
	GtkWidget *dialog_action_area = NULL;
	GtkWidget *button_cancel = NULL;

	va_list args;
	gchar *text = NULL;
	gchar *msg = NULL;
	gchar *hdr = NULL;
	guint id = 0;
    
	if (messagefmt && strlen (messagefmt) > 0) {
		va_start (args, messagefmt);
		msg = g_strdup_vprintf (messagefmt, args);
		va_end (args);
	} else {
		msg = NULL;
	}
    
	hdr = g_markup_escape_text (header, -1);
   
	progress = gtk_dialog_new ();
	gtk_widget_set_name (progress, "progress");
	gtk_container_set_border_width (GTK_CONTAINER (progress), 4);
	gtk_window_set_default_size (GTK_WINDOW (progress), 300, -1);
	gtk_window_set_resizable (GTK_WINDOW (progress), FALSE);
	gtk_window_set_title (GTK_WINDOW (progress), 
			      (title && strlen (title) > 0) ? title : _("Progress"));
	gtk_dialog_set_has_separator (GTK_DIALOG (progress), FALSE);
    
	dialog_vbox = GTK_DIALOG (progress)->vbox;
	gtk_widget_set_name (dialog_vbox, "dialog_vbox");
	gtk_widget_show (dialog_vbox);
    
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_widget_set_name (vbox, "vbox");
	gtk_widget_show (vbox);
	gtk_box_pack_start (GTK_BOX (dialog_vbox), vbox, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
    
	text = g_strconcat ("<span weight='bold' size='larger'>", hdr, "</span>", NULL);
	label = gtk_label_new (text);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_widget_show (label);
	g_free (text);
    
	if (msg && strlen (msg) > 0) {
		label = gtk_label_new (msg);
		gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
		gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_widget_show (label);
	}

	progressbar = gtk_progress_bar_new ();
	gtk_widget_set_name (progressbar, "progressbar");
	gtk_widget_show (progressbar);
	gtk_box_pack_start (GTK_BOX (vbox), progressbar, TRUE, TRUE, 0);
	gtk_widget_set_size_request (progressbar, 300, -2); 
	gtk_progress_bar_set_pulse_step (GTK_PROGRESS_BAR (progressbar), 0.02);
    
	dialog_action_area = GTK_DIALOG (progress)->action_area;
	gtk_widget_set_name (dialog_action_area, "dialog_action_area");
	gtk_widget_show (dialog_action_area);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area), GTK_BUTTONBOX_END);
    
	button_cancel = gtk_button_new_from_stock ("gtk-cancel");
	gtk_widget_set_name (button_cancel, "button_cancel");
	gtk_widget_show (button_cancel);
	gtk_dialog_add_action_widget (GTK_DIALOG (progress), button_cancel, GTK_RESPONSE_CANCEL);
	GTK_WIDGET_SET_FLAGS (button_cancel, GTK_CAN_DEFAULT);

	if (parent != NULL) {
		gtk_window_set_transient_for (GTK_WINDOW (progress), GTK_WINDOW (parent));
	}

	if (flags & GTK_DIALOG_MODAL) {
		gtk_window_set_modal (GTK_WINDOW (progress), TRUE);
	}

	if (flags & GTK_DIALOG_DESTROY_WITH_PARENT) {
		gtk_window_set_destroy_with_parent (GTK_WINDOW (progress), TRUE);
	}
      
	g_free (msg);
	g_free (hdr);
    
	id = g_timeout_add (50, (GSourceFunc)hig_progress_pulse_cb, progressbar);

	g_signal_connect ((gpointer) progress, "destroy",
			  G_CALLBACK (hig_progress_destroy_cb),
			  GINT_TO_POINTER (id));

	g_signal_connect ((gpointer) progress, "realize",
			  G_CALLBACK (hig_dialog_realize_cb),
			  NULL);

	if (destroy_cb) {
		g_signal_connect ((gpointer) progress, "response",
				  destroy_cb, user_data);
	}

	return progress;
}

static gboolean hig_progress_pulse_cb (GtkWidget *progressbar)
{
	if (progressbar == NULL) {
		return FALSE;
	}

	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (progressbar));
	return TRUE;
}

static void hig_dialog_realize_cb (GtkWidget *dialog, gpointer user_data)
{
	gdk_window_set_decorations (GDK_WINDOW (dialog->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);
}

static void hig_progress_destroy_cb (GtkWidget *dialog, gpointer pid)
{
	guint id = 0;

	if (pid == NULL) {
		return;
	}

	id = GPOINTER_TO_INT (pid);
	g_source_remove (id);
}

gboolean gj_gtk_show_error (const gchar *action, gint error_code, const gchar *error_reason)
{
	GtkWidget *dialog = NULL;

	if (error_reason == NULL || error_code < 0) {
		g_warning ("%s: error reason or error code were invalid", __FUNCTION__);
		return FALSE;
	}

	dialog = gj_gtk_hig_dialog_new (NULL,
					GTK_DIALOG_MODAL,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					action ? action : _("Error"),
					_("Error Code: %d\n"
					  "Error Reason: %s"),
					error_code,
					error_reason ? error_reason : _("None Given"));
 
   
	/* run dialog */
	gtk_dialog_run (GTK_DIALOG (dialog));

	/* clean up */
	gtk_widget_destroy (dialog);

	return TRUE;
}

gboolean gj_gtk_show_success (const gchar *action)
{
	GtkWidget *dialog = NULL;

	if (action == NULL) {
		g_warning ("%s: action was NULL", __FUNCTION__);
		return FALSE;
	}

	dialog = gj_gtk_hig_dialog_new (NULL,
					GTK_DIALOG_MODAL,
					GTK_MESSAGE_INFO,
					GTK_BUTTONS_OK,
					action,
					_("Completed Successfully.")); 
   
	/* run dialog */
	gtk_dialog_run (GTK_DIALOG (dialog));

	/* clean up */
	gtk_widget_destroy (dialog);

	return TRUE;
}

gboolean gj_gtk_widget_redraw_when_idle (GtkWidget *widget)
{
	if (widget == NULL) {
		g_warning ("%s: widget is NULL", __FUNCTION__);
		return FALSE;
	}

	/* add idle function to redraw */
	g_idle_add (widget_redraw_when_idle_cb, (void*)widget);
	return TRUE;
}

static gboolean widget_redraw_when_idle_cb (gpointer user_data)
{
	GtkWidget *widget = NULL;

	if ((widget = (GtkWidget*) user_data) == NULL) {
		g_warning ("%s: widget is NULL", __FUNCTION__);
		return FALSE;
	}
  
	/* redraw - for windows */
	gtk_widget_queue_draw (GTK_WIDGET (widget));
  
	return FALSE;
}


#ifdef __cplusplus
}
#endif
 
