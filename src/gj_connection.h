/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_connection_h
#define __gj_connection_h

#include "gj_typedefs.h"

void gj_connection_stats ();

GjConnection gj_connection_ref (GjConnection c);
gboolean gj_connection_unref (GjConnection c);

/* 
 * member functions 
 */

/* gets */
const guint gj_connection_get_id (GjConnection c);
const gchar *gj_connection_get_host (GjConnection c);
const guint gj_connection_get_port (GjConnection c);

GTcpSocket *gj_connection_get_tcpsocket (GjConnection c);
GTcpSocketConnectAsyncID gj_connection_get_connect_id (GjConnection c);

gboolean gj_connection_get_connected (GjConnection c);
gdouble gj_connection_get_uptime (GjConnection c);

GjConnectionCallbackIn gj_connection_get_cb_in (GjConnection c);
GjConnectionCallbackHup gj_connection_get_cb_hup (GjConnection c);
GjConnectionCallbackConnect gj_connection_get_cb_connect (GjConnection c);
GjConnectionCallbackDisconnect gj_connection_get_cb_disconnect (GjConnection c);

/* sets */
gboolean gj_connection_set_host (GjConnection c, const gchar *host);
gboolean gj_connection_set_port (GjConnection c, const guint port);

gboolean gj_connection_set_tcpsocket (GjConnection c, GTcpSocket *ts);
gboolean gj_connection_set_connect_id (GjConnection c, GTcpSocketConnectAsyncID connect_id);

gboolean gj_connection_set_connected (GjConnection c, gboolean connected);

gboolean gj_connection_set_cb_in (GjConnection c, GjConnectionCallbackIn cb);
gboolean gj_connection_set_cb_hup (GjConnection c, GjConnectionCallbackHup cb);
gboolean gj_connection_set_cb_connect (GjConnection c, GjConnectionCallbackConnect cb);
gboolean gj_connection_set_cb_disconnect (GjConnection c, GjConnectionCallbackDisconnect cb);
gboolean gj_connection_set_cb_error (GjConnection c, GjConnectionCallbackError cb);

/* find */
GjConnection gj_connection_find_by_host_and_port (const gchar *host, const guint port);
GjConnection gj_connection_find_by_id (const guint id);


/*
 * connect / disconnect / etc
 */
GjConnection gj_connection_create (const gchar *host, const guint port);

gboolean gj_connection_connect (GjConnection c);

gboolean gj_connection_disconnect (GjConnection c);

void gj_connection_connect_cb (GTcpSocket *socket, GTcpSocketConnectAsyncStatus status, gpointer data);

gboolean gj_connection_write (GjConnection c, const gchar *text);

/* callbacks */
gboolean gj_connection_watch_cb (GIOChannel *source, GIOCondition condition, gpointer data);

/* translations */
const gchar *gj_connection_connect_translate_status (GTcpSocketConnectAsyncStatus status);
const gchar *gj_connection_io_status_translate (GIOStatus status);
const gchar *gj_connection_io_error_translate (gint error);
const gchar *gj_connection_io_condition_translate (gint condition);

#endif
