/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef G_OS_WIN32
#include <gconf/gconf-client.h>
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_config.h"
#include "gj_config_handler.h"
#include "gj_main.h"
#include "gj_log.h"

#define GCONF_PATH "/apps/gnome-jabber"


static void load_pp ();
static void save_pp ();


typedef struct  {
	gchar *cs_server;
	gint cs_port;
	gchar *cs_username;
	gchar *cs_password;

	gchar *cs_resource;
	gint8 cs_priority;

	gboolean cs_prompt_for_password;
	gboolean cs_use_plain_text_password;
	gboolean cs_use_ssl;

	gboolean cs_auto_connect_on_startup;
	gboolean cs_auto_reconnect_on_disconnect;

	gboolean mpf_hide_offline_contacts;
	gboolean mpf_hide_offline_group;
	gboolean mpf_hide_agents;
	gboolean mpf_hide_contact_presence_details;

	gboolean mpf_double_click_for_chat;

	gboolean mpf_open_window_on_new_message;
	gboolean mpf_raise_window_on_message;
	gboolean mpf_hide_time;
	gboolean mpf_reverse_text_direction;
	gboolean mpf_use_smileys;

	gboolean mpf_use_spell_checker;

	gboolean mpf_use_auto_away;
	gint mpf_auto_away_after;
	gint mpf_auto_xa_after;
	gchar *mpf_auto_away_status;
	gchar *mpf_auto_xa_status;

	gboolean mpf_sound_enabled;
	gboolean mpf_sound_on_user_online;
	gboolean mpf_sound_on_user_offline;
	gboolean mpf_sound_on_receive_message;
	gboolean mpf_sound_on_receive_chat;
	gboolean mpf_sound_on_receive_headline;
	gboolean mpf_sound_on_receive_error;
	gboolean mpf_sound_beep_if_no_file;

	gchar *mpf_sound_file_on_user_online;
	gchar *mpf_sound_file_on_user_offline;
	gchar *mpf_sound_file_on_receive_message;
	gchar *mpf_sound_file_on_receive_chat;
	gchar *mpf_sound_file_on_receive_headline;
	gchar *mpf_sound_file_on_receive_error;

	gboolean mpf_use_docklet;

	gchar *mpf_log_location;
	gchar *mpf_history_location;
	gboolean mpf_use_history;

   
	GList *pp_show;
	GList *pp_status;
	GList *pp_priority;

} GjConfig;


#ifndef G_OS_WIN32
static GConfClient *gconf_client = NULL;
#endif

static GjConfig *cfg = NULL;
static gchar *filename = NULL;


gboolean gj_config_init ()
{
	cfg = g_new0 (GjConfig, 1); 

	filename = g_strdup (gnome_jabber_get_config_file ());

	/* set defaults */
	cfg->cs_server = NULL;
	cfg->cs_port = 5222;
	cfg->cs_username = NULL;
	cfg->cs_password = NULL;

	cfg->cs_resource = NULL;
	cfg->cs_priority = 10;

	cfg->cs_prompt_for_password = FALSE;
	cfg->cs_use_plain_text_password = FALSE;
	cfg->cs_use_ssl = FALSE;
	
	cfg->cs_auto_connect_on_startup = FALSE;
	cfg->cs_auto_reconnect_on_disconnect = FALSE;
	
	cfg->mpf_hide_offline_contacts = TRUE;
	cfg->mpf_hide_offline_group = FALSE;
	cfg->mpf_hide_agents = FALSE;
	cfg->mpf_hide_contact_presence_details = TRUE;

	cfg->mpf_double_click_for_chat = TRUE;

	cfg->mpf_open_window_on_new_message = TRUE;
	cfg->mpf_raise_window_on_message = FALSE;
	cfg->mpf_hide_time = FALSE;
	cfg->mpf_reverse_text_direction = FALSE;
	cfg->mpf_use_smileys = TRUE;

	cfg->mpf_use_spell_checker = FALSE;

	cfg->mpf_use_auto_away = TRUE;
	cfg->mpf_auto_away_after = 5;
	cfg->mpf_auto_xa_after = 15;
	cfg->mpf_auto_away_status = NULL;
	cfg->mpf_auto_xa_status = NULL;

	cfg->mpf_sound_enabled = FALSE;
	cfg->mpf_sound_on_user_online = FALSE;
	cfg->mpf_sound_on_user_offline = FALSE;
	cfg->mpf_sound_on_receive_message = FALSE;
	cfg->mpf_sound_on_receive_chat = FALSE;
	cfg->mpf_sound_on_receive_headline = FALSE;
	cfg->mpf_sound_on_receive_error = FALSE;
	cfg->mpf_sound_beep_if_no_file = FALSE;
	
	cfg->mpf_sound_file_on_user_online = g_strdup_printf ("contact.wav");
	cfg->mpf_sound_file_on_user_offline = g_strdup_printf ("contact.wav");
	cfg->mpf_sound_file_on_receive_message = g_strdup_printf ("message.wav");
	cfg->mpf_sound_file_on_receive_chat = g_strdup_printf ("message.wav");
	cfg->mpf_sound_file_on_receive_headline = g_strdup_printf ("message.wav");
	cfg->mpf_sound_file_on_receive_error = g_strdup_printf ("message.wav");

	cfg->mpf_use_docklet = FALSE;

	cfg->mpf_log_location = NULL;
	cfg->mpf_history_location = NULL;
	cfg->mpf_use_history = TRUE;

#ifndef G_OS_WIN32
	gconf_client = gconf_client_get_default ();
    
	gconf_client_add_dir (gconf_client,
			      GCONF_PATH,
			      GCONF_CLIENT_PRELOAD_ONELEVEL,
			      NULL);
#endif
  
	return TRUE;
}

gboolean gj_config_term ()
{
	/* set defaults */
	if (cfg->cs_server != NULL) {
		g_free (cfg->cs_server);
		cfg->cs_server = NULL;
	}

	cfg->cs_port = 5222;

	if (cfg->cs_username != NULL) {
		g_free (cfg->cs_username);
		cfg->cs_username = NULL;
	}

	if (cfg->cs_password != NULL) {
		g_free (cfg->cs_password);
		cfg->cs_password = NULL;
	}

	if (cfg->cs_resource != NULL) {
		g_free (cfg->cs_resource);
		cfg->cs_resource = NULL;
	}

	cfg->cs_priority = 10;

	cfg->cs_prompt_for_password = FALSE;
	cfg->cs_use_plain_text_password = FALSE;
	cfg->cs_use_ssl = FALSE;
	
	cfg->cs_auto_connect_on_startup = FALSE;
	cfg->cs_auto_reconnect_on_disconnect = FALSE;
	
	cfg->mpf_hide_offline_contacts = TRUE;
	cfg->mpf_hide_offline_group = FALSE;
	cfg->mpf_hide_agents = FALSE;
	cfg->mpf_hide_contact_presence_details = TRUE;

	cfg->mpf_double_click_for_chat = TRUE;

	cfg->mpf_open_window_on_new_message = TRUE;
	cfg->mpf_raise_window_on_message = TRUE;
	cfg->mpf_hide_time = FALSE;
	cfg->mpf_reverse_text_direction = FALSE;
	cfg->mpf_use_smileys = TRUE;

	cfg->mpf_use_spell_checker = FALSE;

	cfg->mpf_use_auto_away = TRUE;
	cfg->mpf_auto_away_after = 5;
	cfg->mpf_auto_xa_after = 15;
	cfg->mpf_auto_away_status = NULL;
	cfg->mpf_auto_xa_status = NULL;

	cfg->mpf_sound_enabled = FALSE;
	cfg->mpf_sound_on_user_online = FALSE;
	cfg->mpf_sound_on_user_offline = FALSE;
	cfg->mpf_sound_on_receive_message = FALSE;
	cfg->mpf_sound_on_receive_chat = FALSE;
	cfg->mpf_sound_on_receive_headline = FALSE;
	cfg->mpf_sound_on_receive_error = FALSE;
	cfg->mpf_sound_beep_if_no_file = FALSE;
	
	if (cfg->mpf_sound_file_on_user_online != NULL) {
		g_free (cfg->mpf_sound_file_on_user_online);
		cfg->mpf_sound_file_on_user_online = NULL;
	}

	if (cfg->mpf_sound_file_on_user_offline != NULL) {
		g_free (cfg->mpf_sound_file_on_user_offline);
		cfg->mpf_sound_file_on_user_offline = NULL;
	}

	if (cfg->mpf_sound_file_on_receive_message != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_message);
		cfg->mpf_sound_file_on_receive_message = NULL;
	}

	if (cfg->mpf_sound_file_on_receive_chat != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_chat);
		cfg->mpf_sound_file_on_receive_chat = NULL;
	}

	if (cfg->mpf_sound_file_on_receive_headline != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_headline);
		cfg->mpf_sound_file_on_receive_headline = NULL;
	}

	if (cfg->mpf_sound_file_on_receive_error != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_error);
		cfg->mpf_sound_file_on_receive_error = NULL;
	}

	cfg->mpf_use_docklet = FALSE;

	if (cfg->mpf_log_location != NULL) {
		g_free (cfg->mpf_log_location);
		cfg->mpf_log_location = NULL;
	}

	if (cfg->mpf_history_location != NULL) {
		g_free (cfg->mpf_history_location);
		cfg->mpf_history_location = NULL;
	}

	cfg->mpf_use_history = TRUE;

#ifndef G_OS_WIN32
	/* clean up */
	if (gconf_client != NULL) {
		g_object_unref (G_OBJECT (gconf_client));
	}
#endif

	g_free (cfg);
	cfg = NULL;

	g_free (filename);
	filename = NULL;
  
	return TRUE;
}

#ifndef G_OS_WIN32 

gboolean gj_config_read ()
{
	cfg->cs_server = gconf_client_get_string (gconf_client, GCONF_PATH "/connection/server", NULL);
	cfg->cs_port = gconf_client_get_int (gconf_client, GCONF_PATH "/connection/port", NULL);
	cfg->cs_username = gconf_client_get_string (gconf_client, GCONF_PATH "/connection/username", NULL);
	cfg->cs_password = gconf_client_get_string (gconf_client, GCONF_PATH "/connection/password", NULL);
    
	cfg->cs_resource = gconf_client_get_string (gconf_client, GCONF_PATH "/connection/resource", NULL);
	cfg->cs_priority = gconf_client_get_int (gconf_client, GCONF_PATH "/connection/priority", NULL);
    
	cfg->cs_prompt_for_password = gconf_client_get_bool (gconf_client, GCONF_PATH "/connection/prompt_for_password", NULL);
	cfg->cs_use_plain_text_password = gconf_client_get_bool (gconf_client, GCONF_PATH "/connection/use_plain_text_password", NULL);
	cfg->cs_use_ssl = gconf_client_get_bool (gconf_client, GCONF_PATH "/connection/use_ssl", NULL);
    
	cfg->cs_auto_connect_on_startup = gconf_client_get_bool (gconf_client, GCONF_PATH "/connection/auto_connect_on_startup", NULL);
	cfg->cs_auto_reconnect_on_disconnect = gconf_client_get_bool (gconf_client, GCONF_PATH "/connection/auto_reconnect_on_disconnect", NULL);
    
	cfg->mpf_hide_offline_contacts = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_offline_contacts", NULL);
	cfg->mpf_hide_offline_group = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_offline_group", NULL);
	cfg->mpf_hide_agents = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_agents", NULL);
	cfg->mpf_hide_contact_presence_details = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_contact_presence_details", NULL);
    
	cfg->mpf_double_click_for_chat = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/roster/double_click_for_chat", NULL);
    
	cfg->mpf_open_window_on_new_message = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/chat/open_window_on_new_message", NULL);
	cfg->mpf_raise_window_on_message = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/chat/raise_window_on_message", NULL);
	cfg->mpf_hide_time = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/chat/hide_time", NULL);
	cfg->mpf_reverse_text_direction = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/chat/reverse_text_direction", NULL);
	cfg->mpf_use_smileys = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/chat/use_smileys", NULL);
    
	cfg->mpf_use_spell_checker = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/chat/use_spell_checker", NULL);
    
	cfg->mpf_use_auto_away = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/presence/use_auto_away", NULL);
	cfg->mpf_auto_away_after = gconf_client_get_int (gconf_client, GCONF_PATH "/preferences/presence/auto_away_after", NULL);
	cfg->mpf_auto_xa_after = gconf_client_get_int (gconf_client, GCONF_PATH "/preferences/presence/auto_xa_after", NULL);
	cfg->mpf_auto_away_status = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/presence/auto_away_status", NULL);
	cfg->mpf_auto_xa_status = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/presence/auto_xa_status", NULL);
    
	cfg->mpf_sound_on_user_online = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_user_online", NULL);
	cfg->mpf_sound_on_user_offline = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_user_offline", NULL);
	cfg->mpf_sound_on_receive_message = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_message", NULL);
	cfg->mpf_sound_on_receive_chat = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_chat", NULL);
	cfg->mpf_sound_on_receive_headline = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_headline", NULL);
	cfg->mpf_sound_on_receive_error = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_error", NULL);
	cfg->mpf_sound_file_on_user_online = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_user_online", NULL);
	cfg->mpf_sound_file_on_user_offline = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_user_offline", NULL);
	cfg->mpf_sound_file_on_receive_message = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_message", NULL);
	cfg->mpf_sound_file_on_receive_chat = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_chat", NULL);
	cfg->mpf_sound_file_on_receive_headline = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_headline", NULL);
	cfg->mpf_sound_file_on_receive_error = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_error", NULL);
    
	cfg->mpf_use_docklet = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/notifications/use_docklet", NULL);
    
	cfg->mpf_log_location = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/misc/log_location", NULL);
	cfg->mpf_history_location = gconf_client_get_string (gconf_client, GCONF_PATH "/preferences/misc/history_location", NULL);
	cfg->mpf_use_history = gconf_client_get_bool (gconf_client, GCONF_PATH "/preferences/misc/use_history", NULL);
  
	/*   load_pp (); */
      
	return TRUE;
}

gboolean gj_config_write ()
{
	gconf_client_set_string (gconf_client, GCONF_PATH "/connection/server", cfg->cs_server, NULL);
	gconf_client_set_int (gconf_client, GCONF_PATH "/connection/port", cfg->cs_port, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/connection/username", cfg->cs_username, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/connection/password", cfg->cs_password, NULL);

	gconf_client_set_string (gconf_client, GCONF_PATH "/connection/resource", cfg->cs_resource, NULL);
	gconf_client_set_int (gconf_client, GCONF_PATH "/connection/priority", cfg->cs_priority, NULL);


	gconf_client_set_bool (gconf_client, GCONF_PATH "/connection/prompt_for_password", cfg->cs_prompt_for_password, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/connection/use_plain_text_password", cfg->cs_use_plain_text_password, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/connection/use_ssl", cfg->cs_use_ssl, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/connection/auto_connect_on_startup", cfg->cs_auto_connect_on_startup, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/connection/auto_reconnect_on_disconnect", cfg->cs_auto_reconnect_on_disconnect, NULL);


	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_offline_contacts", cfg->mpf_hide_offline_contacts, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_offline_group", cfg->mpf_hide_offline_group, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_agents", cfg->mpf_hide_agents, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/roster/hide_contact_presence_details", cfg->mpf_hide_contact_presence_details, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/roster/double_click_for_chat", cfg->mpf_double_click_for_chat, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/chat/open_window_on_new_message", cfg->mpf_open_window_on_new_message, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/chat/raise_window_on_message", cfg->mpf_raise_window_on_message, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/chat/hide_time", cfg->mpf_hide_time, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/chat/reverse_text_direction", cfg->mpf_reverse_text_direction, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/chat/use_smileys", cfg->mpf_use_smileys, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/chat/use_spell_checker", cfg->mpf_use_spell_checker, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/presence/use_auto_away", cfg->mpf_use_auto_away, NULL);
	gconf_client_set_int (gconf_client, GCONF_PATH "/preferences/presence/auto_away_after", cfg->mpf_auto_away_after, NULL);
	gconf_client_set_int (gconf_client, GCONF_PATH "/preferences/presence/auto_xa_after", cfg->mpf_auto_xa_after, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/presence/auto_away_status", cfg->mpf_auto_away_status, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/presence/auto_xa_status", cfg->mpf_auto_xa_status, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_user_online", cfg->mpf_sound_on_user_online, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_user_offline", cfg->mpf_sound_on_user_offline, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_message", cfg->mpf_sound_on_receive_message, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_chat", cfg->mpf_sound_on_receive_chat, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_headline", cfg->mpf_sound_on_receive_headline, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/sound_on_receive_error", cfg->mpf_sound_on_receive_error, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_user_online", cfg->mpf_sound_file_on_user_online, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_user_offline", cfg->mpf_sound_file_on_user_offline, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_message", cfg->mpf_sound_file_on_receive_message, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_chat", cfg->mpf_sound_file_on_receive_chat, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_headline", cfg->mpf_sound_file_on_receive_headline, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/notifications/sound_file_on_receive_error", cfg->mpf_sound_file_on_receive_error, NULL);

	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/notifications/use_docklet", cfg->mpf_use_docklet, NULL);

	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/misc/log_location", cfg->mpf_log_location, NULL);
	gconf_client_set_string (gconf_client, GCONF_PATH "/preferences/misc/history_location", cfg->mpf_history_location, NULL);
	gconf_client_set_bool (gconf_client, GCONF_PATH "/preferences/misc/use_history", cfg->mpf_use_history, NULL);

	/*   save_pp (); */

	return TRUE;
}

static gboolean pp_get_index (gint i, gchar **show, gchar **status, gchar **priority)
{
	gchar *str = NULL;
    
	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	if (!gconf_client_dir_exists (gconf_client, GCONF_PATH "/presence_presets", NULL)) {
		return FALSE;
	}

	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("show"));
	s1 = gconf_client_get_string (gconf_client, str, NULL);
	g_free (str);
    
	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("status"));
	s2 = gconf_client_get_string (gconf_client, str, NULL);
	g_free (str);
    
	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("priority"));
	s3 = gconf_client_get_string (gconf_client, str, NULL);
	g_free (str);
    
	if (!s1 || !s2) {
		g_free (s1);
		g_free (s2);
	
		return FALSE;
	}
	  
	if (show) {
		*show = g_strdup (s1);
	}

	if (status) {
		*status = g_strdup (s2); 
	}

	if (priority) {
		*priority = g_strdup (s3); 
	}
    
	return TRUE;
}

static gboolean pp_set_index (gint i, gchar *show, gchar *status, gchar *priority)
{
	gchar *str = NULL;

	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("show"));
	gconf_client_set_string (gconf_client, str, show, NULL);
	g_free (str);
    
	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("status"));
	gconf_client_set_string (gconf_client, str, status, NULL);
	g_free (str);
    
	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("priority"));
	gconf_client_set_string (gconf_client, str, priority, NULL);
	g_free (str);

	return TRUE;
}

static gboolean pp_set_index_delete (gint i)
{
	gchar *str = NULL;

	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("show"));
	gconf_client_unset (gconf_client, str, NULL);
	g_free (str);
    
	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("status"));
	gconf_client_unset (gconf_client, str, NULL);
	g_free (str);
    
	str = g_strdup_printf ("%s%2.2d_%s", GCONF_PATH "/presence_presets/", i, _("priority"));
	gconf_client_unset (gconf_client, str, NULL);
	g_free (str);

	return TRUE;
}


#else

gboolean gj_config_read ()
{
	GjConfigFile *cf = NULL;
	GjConfigSection *section = NULL;

	if (filename == NULL || strlen (filename) < 1) {
		g_warning ("%s: filename was NULL or len < 1", __FUNCTION__);
		return FALSE;
	}

	/* get file */
	if ((cf = gj_config_handler_open_file (filename)) == FALSE) {
		g_warning ("%s: could not open config file:'%s'", __FUNCTION__, filename);

		/* set defaults... */
		cfg->cs_server = g_strdup_printf ("jabber.org");
		cfg->cs_username = NULL;
		cfg->cs_password = NULL;
		cfg->cs_resource = g_strdup_printf ("gnome-jabber");
      
		cfg->mpf_auto_away_status = g_strdup_printf (_("Not at my computer"));
		cfg->mpf_auto_xa_status = g_strdup_printf (_("Away for a while"));

		/*       cfg->mpf_log_location = g_get_current_dir (); */

		return FALSE;
	}

	gj_config_handler_read_string (cf, CONFIG_SECT_CS, CONFIG_CS_SERVER, &cfg->cs_server);
	gj_config_handler_read_int (cf, CONFIG_SECT_CS, CONFIG_CS_PORT, &cfg->cs_port);
	gj_config_handler_read_string (cf, CONFIG_SECT_CS, CONFIG_CS_USERNAME, &cfg->cs_username);
	gj_config_handler_read_string (cf, CONFIG_SECT_CS, CONFIG_CS_PASSWORD, &cfg->cs_password);

	gj_config_handler_read_string (cf, CONFIG_SECT_CS, CONFIG_CS_RESOURCE, &cfg->cs_resource);
	gj_config_handler_read_int8 (cf, CONFIG_SECT_CS, CONFIG_CS_PRIORITY, &cfg->cs_priority);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_PROMPT_FOR_PASSWORD, &cfg->cs_prompt_for_password);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_USE_PLAIN_TEXT_PASSWORD, &cfg->cs_use_plain_text_password);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_USE_SSL, &cfg->cs_use_ssl);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_AUTO_CONNECT_ON_STARTUP, &cfg->cs_auto_connect_on_startup);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_AUTO_RECONNECT_ON_DISCONNECT, &cfg->cs_auto_reconnect_on_disconnect);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_OFFLINE_CONTACTS, &cfg->mpf_hide_offline_contacts);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_OFFLINE_GROUP, &cfg->mpf_hide_offline_group);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_AGENTS, &cfg->mpf_hide_agents);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_CONTACT_PRESENCE_DETAILS, &cfg->mpf_hide_contact_presence_details);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_DOUBLE_CLICK_FOR_CHAT, &cfg->mpf_double_click_for_chat);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_OPEN_WINDOW_ON_NEW_MESSAGE, &cfg->mpf_open_window_on_new_message);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_RAISE_WINDOW_ON_MESSAGE, &cfg->mpf_raise_window_on_message);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_TIME, &cfg->mpf_hide_time);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_REVERSE_TEXT_DIRECTION, &cfg->mpf_reverse_text_direction);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_SMILEYS, &cfg->mpf_use_smileys);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_SPELL_CHECKER, &cfg->mpf_use_spell_checker);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_AUTO_AWAY, &cfg->mpf_use_auto_away);
	gj_config_handler_read_int (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_AWAY_AFTER, &cfg->mpf_auto_away_after);
	gj_config_handler_read_int (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_XA_AFTER, &cfg->mpf_auto_xa_after);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_AWAY_STATUS, &cfg->mpf_auto_away_status);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_XA_STATUS, &cfg->mpf_auto_xa_status);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_USER_ONLINE, &cfg->mpf_sound_on_user_online);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_USER_OFFLINE, &cfg->mpf_sound_on_user_offline);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_MESSAGE, &cfg->mpf_sound_on_receive_message);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_CHAT, &cfg->mpf_sound_on_receive_chat);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_HEADLINE, &cfg->mpf_sound_on_receive_headline);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_ERROR, &cfg->mpf_sound_on_receive_error);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_USER_ONLINE, &cfg->mpf_sound_file_on_user_online);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_USER_OFFLINE, &cfg->mpf_sound_file_on_user_offline);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_MESSAGE, &cfg->mpf_sound_file_on_receive_message);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_CHAT, &cfg->mpf_sound_file_on_receive_chat);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_HEADLINE, &cfg->mpf_sound_file_on_receive_headline);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_ERROR, &cfg->mpf_sound_file_on_receive_error);

	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_DOCKLET, &cfg->mpf_use_docklet);

	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_LOG_LOCATION, &cfg->mpf_log_location);
	gj_config_handler_read_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_HISTORY_LOCATION, &cfg->mpf_history_location);
	gj_config_handler_read_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_HISTORY, &cfg->mpf_use_history);

	/*   load_pp (); */

	/* clean up */
	gj_config_handler_free (cf);

	return TRUE;
}

gboolean gj_config_write ()
{
	GjConfigFile *cf = NULL;

	if (filename == NULL || strlen (filename) < 1) {
		p ("config filename was NULL");

		g_warning ("%s: filename was NULL or len < 1", __FUNCTION__);
		return FALSE;
	}

	if ((cf = gj_config_handler_open_file (filename)) == FALSE) {
		p_("creating new config filename:'%s'...", filename);
		fflush (stdout);

		g_warning ("%s: could not open config file, creating...", __FUNCTION__);

		if ((cf = gj_config_handler_new ()) == FALSE) {
			p ("failed!");

			g_warning ("%s: could not create new config file.", __FUNCTION__);
			return FALSE;
		} else {
			p ("ok");
		}
	}

	p ("writing values");

	/* set values */
	gj_config_handler_write_string (cf, CONFIG_SECT_CS, CONFIG_CS_SERVER, cfg->cs_server);
	gj_config_handler_write_int (cf, CONFIG_SECT_CS, CONFIG_CS_PORT, cfg->cs_port);
	gj_config_handler_write_string (cf, CONFIG_SECT_CS, CONFIG_CS_USERNAME, cfg->cs_username);
	gj_config_handler_write_string (cf, CONFIG_SECT_CS, CONFIG_CS_PASSWORD, cfg->cs_password);

	gj_config_handler_write_string (cf, CONFIG_SECT_CS, CONFIG_CS_RESOURCE, cfg->cs_resource);
	gj_config_handler_write_int8 (cf, CONFIG_SECT_CS, CONFIG_CS_PRIORITY, cfg->cs_priority);


	gj_config_handler_write_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_PROMPT_FOR_PASSWORD, cfg->cs_prompt_for_password);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_USE_PLAIN_TEXT_PASSWORD, cfg->cs_use_plain_text_password);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_USE_SSL, cfg->cs_use_ssl);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_AUTO_CONNECT_ON_STARTUP, cfg->cs_auto_connect_on_startup);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_CS, CONFIG_CS_AUTO_RECONNECT_ON_DISCONNECT, cfg->cs_auto_reconnect_on_disconnect);


	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_OFFLINE_CONTACTS, cfg->mpf_hide_offline_contacts);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_OFFLINE_GROUP, cfg->mpf_hide_offline_group);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_AGENTS, cfg->mpf_hide_agents);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_CONTACT_PRESENCE_DETAILS, cfg->mpf_hide_contact_presence_details);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_DOUBLE_CLICK_FOR_CHAT, cfg->mpf_double_click_for_chat);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_OPEN_WINDOW_ON_NEW_MESSAGE, cfg->mpf_open_window_on_new_message);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_RAISE_WINDOW_ON_MESSAGE, cfg->mpf_raise_window_on_message);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_HIDE_TIME, cfg->mpf_hide_time);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_REVERSE_TEXT_DIRECTION, cfg->mpf_reverse_text_direction);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_SMILEYS, cfg->mpf_use_smileys);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_SPELL_CHECKER, cfg->mpf_use_spell_checker);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_AUTO_AWAY, cfg->mpf_use_auto_away);
	gj_config_handler_write_int (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_AWAY_AFTER, cfg->mpf_auto_away_after);
	gj_config_handler_write_int (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_XA_AFTER, cfg->mpf_auto_xa_after);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_AWAY_STATUS, cfg->mpf_auto_away_status);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_XA_STATUS, cfg->mpf_auto_xa_status);

	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_AUTO_XA_STATUS, cfg->mpf_auto_xa_status);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_USER_ONLINE, cfg->mpf_sound_on_user_online);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_USER_OFFLINE, cfg->mpf_sound_on_user_offline);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_MESSAGE, cfg->mpf_sound_on_receive_message);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_CHAT, cfg->mpf_sound_on_receive_chat);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_HEADLINE, cfg->mpf_sound_on_receive_headline);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_ON_RECEIVE_ERROR, cfg->mpf_sound_on_receive_error);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_USER_ONLINE, cfg->mpf_sound_file_on_user_online);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_USER_OFFLINE, cfg->mpf_sound_file_on_user_offline);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_MESSAGE, cfg->mpf_sound_file_on_receive_message);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_CHAT, cfg->mpf_sound_file_on_receive_chat);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_HEADLINE, cfg->mpf_sound_file_on_receive_headline);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_SOUND_FILE_ON_RECEIVE_ERROR, cfg->mpf_sound_file_on_receive_error);

	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_DOCKLET, cfg->mpf_use_docklet);

	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_LOG_LOCATION, cfg->mpf_log_location);
	gj_config_handler_write_string (cf, CONFIG_SECT_MPF, CONFIG_MPF_HISTORY_LOCATION, cfg->mpf_history_location);
	gj_config_handler_write_boolean (cf, CONFIG_SECT_MPF, CONFIG_MPF_USE_HISTORY, cfg->mpf_use_history);

 
	/*   save_pp (); */

	/* write file */
	gj_config_handler_write_file (cf, filename);

	/* clean up */
	gj_config_handler_free (cf);

	return TRUE;
}

static gboolean pp_get_index (gint i, gchar **show, gchar **status, gchar **priority)
{
	GjConfigFile *cf = NULL;

	gchar *str = NULL;
    
	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	cf = gj_config_handler_open_file (filename);
	
	if (!cf) {
		return FALSE;
	}

	str = g_strdup_printf ("%2.2d_%s", i, _("show"));
	gj_config_handler_read_string (cf, CONFIG_SECT_PP, str, &s1);
	g_free (str);
    
	str = g_strdup_printf ("%2.2d_%s", i, _("status"));
	gj_config_handler_read_string (cf, CONFIG_SECT_PP, str, &s2);
	g_free (str);
    
	str = g_strdup_printf ("%2.2d_%s", i, _("priority"));
	gj_config_handler_read_string (cf, CONFIG_SECT_PP, str, &s3);
	g_free (str);
    
	if (!s1 || !s2) {
		g_free (s1);
		g_free (s2);
	
		return FALSE;
	}
	  
	if (show) {
		*show = g_strdup (s1); 
	}

	if (status) {
		*status = g_strdup (s2); 
	}

	if (priority) {
		*priority = g_strdup (s3);
	}

	/* clean up */
	gj_config_handler_free (cf);
    
	return TRUE;
}

static gboolean pp_set_index (gint i, gchar *show, gchar *status, gchar *priority)
{
	gchar *str = NULL;

	GjConfigFile *cf = NULL;
    
	if (filename == NULL || strlen (filename) < 1) {
		p ("config filename was NULL");
	
		g_warning ("%s: filename was NULL or len < 1", __FUNCTION__);
		return FALSE;
	}
    
	if ((cf = gj_config_handler_open_file (filename)) == FALSE) {
		p_("creating new config filename:'%s'...", filename);
	
		g_warning ("%s: could not open config file, creating...", __FUNCTION__);
	
		if ((cf = gj_config_handler_new ()) == FALSE) {
			p ("failed!");
	    
			g_warning ("%s: could not create new config file.", __FUNCTION__);
			return FALSE;
		} else {
			p ("ok");
		}
	}

	p ("writing values");

    
	str = g_strdup_printf ("%2.2d_%s", i, _("show"));
	gj_config_handler_read_string (cf, CONFIG_SECT_PP, str, &show);
	g_free (str);
    
	str = g_strdup_printf ("%2.2d_%s", i, _("status"));
	gj_config_handler_read_string (cf, CONFIG_SECT_PP, str, &status);
	g_free (str);
    
	str = g_strdup_printf ("%2.2d_%s", i, _("priority"));
	gj_config_handler_read_string (cf, CONFIG_SECT_PP, str, &priority);
	g_free (str);

	/* write file */
	gj_config_handler_write_file (cf, filename);
    
	/* clean up */
	gj_config_handler_free (cf);

	return TRUE;
}

static gboolean pp_set_index_delete (gint i)
{
	gchar *str = NULL;

	GjConfigFile *cf = NULL;
    
	if (filename == NULL || strlen (filename) < 1) {
		p ("config filename was NULL");
	
		g_warning ("%s: filename was NULL or len < 1", __FUNCTION__);
		return FALSE;
	}
    
	if ((cf = gj_config_handler_open_file (filename)) == FALSE) {
		p_("creating new config filename:'%s'...", filename);
	
		g_warning ("%s: could not open config file, creating...", __FUNCTION__);
	
		if ((cf = gj_config_handler_new ()) == FALSE) {
			p ("failed!");
	    
			g_warning ("%s: could not create new config file.", __FUNCTION__);
			return FALSE;
		} else {
			p ("ok");
		}
	}

	p ("writing values");
    
	str = g_strdup_printf ("%2.2d_%s", i, _("show"));
	gj_config_handler_remove_key (cf, CONFIG_SECT_PP, str);
	g_free (str);
    
	str = g_strdup_printf ("%2.2d_%s", i, _("status"));
	gj_config_handler_remove_key (cf, CONFIG_SECT_PP, str);
	g_free (str);
    
	str = g_strdup_printf ("%2.2d_%s", i, _("priority"));
	gj_config_handler_remove_key (cf, CONFIG_SECT_PP, xsastr);
	g_free (str);

	/* write file */
	gj_config_handler_write_file (cf, filename);
    
	/* clean up */
	gj_config_handler_free (cf);

	return TRUE;
}

#endif

/*
 * GETS 
 */
const gchar *gj_config_get_cs_server () { return cfg->cs_server; }
guint gj_config_get_cs_port () { return cfg->cs_port; }
const gchar *gj_config_get_cs_username () { return cfg->cs_username; }
const gchar *gj_config_get_cs_password () { return cfg->cs_password; }

const gchar *gj_config_get_cs_resource () { return cfg->cs_resource; }
gint8 gj_config_get_cs_priority () { return cfg->cs_priority; }

gboolean gj_config_get_cs_prompt_for_password () { return cfg->cs_prompt_for_password; }
gboolean gj_config_get_cs_use_plain_text_password () { return cfg->cs_use_plain_text_password; }
gboolean gj_config_get_cs_use_ssl () { return cfg->cs_use_ssl; }

gboolean gj_config_get_cs_auto_connect_on_startup () { return cfg->cs_auto_connect_on_startup; } 
gboolean gj_config_get_cs_auto_reconnect_on_disconnect () { return cfg->cs_auto_reconnect_on_disconnect; } 

gboolean gj_config_get_mpf_hide_offline_contacts () { return cfg->mpf_hide_offline_contacts; } 
gboolean gj_config_get_mpf_hide_offline_group () { return cfg->mpf_hide_offline_group; } 
gboolean gj_config_get_mpf_hide_agents () { return cfg->mpf_hide_agents; } 
gboolean gj_config_get_mpf_hide_contact_presence_details () { return cfg->mpf_hide_contact_presence_details; } 

gboolean gj_config_get_mpf_double_click_for_chat () { return cfg->mpf_double_click_for_chat; } 

gboolean gj_config_get_mpf_open_window_on_new_message () { return cfg->mpf_open_window_on_new_message; } 
gboolean gj_config_get_mpf_raise_window_on_message () { return cfg->mpf_raise_window_on_message; } 
gboolean gj_config_get_mpf_hide_time () { return cfg->mpf_hide_time; } 
gboolean gj_config_get_mpf_reverse_text_direction () { return cfg->mpf_reverse_text_direction; } 
gboolean gj_config_get_mpf_use_smileys () { return cfg->mpf_use_smileys; } 

gboolean gj_config_get_mpf_use_spell_checker () { return cfg->mpf_use_spell_checker; } 

gboolean gj_config_get_mpf_use_auto_away () { return cfg->mpf_use_auto_away; } 
guint gj_config_get_mpf_auto_away_after () { return cfg->mpf_auto_away_after; } 
guint gj_config_get_mpf_auto_xa_after () { return cfg->mpf_auto_xa_after; } 
const gchar *gj_config_get_mpf_auto_away_status () { return cfg->mpf_auto_away_status; }
const gchar *gj_config_get_mpf_auto_xa_status () { return cfg->mpf_auto_xa_status; }

gboolean gj_config_get_mpf_sound_on_user_online () { return cfg->mpf_sound_on_user_online; }
gboolean gj_config_get_mpf_sound_on_user_offline () { return cfg->mpf_sound_on_user_offline; }
gboolean gj_config_get_mpf_sound_on_receive_message () { return cfg->mpf_sound_on_receive_message; }
gboolean gj_config_get_mpf_sound_on_receive_chat () { return cfg->mpf_sound_on_receive_chat; }
gboolean gj_config_get_mpf_sound_on_receive_headline () { return cfg->mpf_sound_on_receive_headline; }
gboolean gj_config_get_mpf_sound_on_receive_error () { return cfg->mpf_sound_on_receive_error; }
const gchar *gj_config_get_mpf_sound_file_on_user_online () { return cfg->mpf_sound_file_on_user_online; }
const gchar *gj_config_get_mpf_sound_file_on_user_offline () { return cfg->mpf_sound_file_on_user_offline; }
const gchar *gj_config_get_mpf_sound_file_on_receive_message () { return cfg->mpf_sound_file_on_receive_message; }
const gchar *gj_config_get_mpf_sound_file_on_receive_chat () { return cfg->mpf_sound_file_on_receive_chat; }
const gchar *gj_config_get_mpf_sound_file_on_receive_headline () { return cfg->mpf_sound_file_on_receive_headline; }
const gchar *gj_config_get_mpf_sound_file_on_receive_error () { return cfg->mpf_sound_file_on_receive_error; }

gboolean gj_config_get_mpf_use_docklet () { return cfg->mpf_use_docklet; }

const gchar *gj_config_get_mpf_log_location () { return cfg->mpf_log_location; }
const gchar *gj_config_get_mpf_history_location () { return cfg->mpf_history_location; }
gboolean gj_config_get_mpf_use_history () { return cfg->mpf_use_history; }


/*
 * SETS
 */
gboolean gj_config_set_cs_server (const gchar *string) 
{
	if (string == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->cs_server != NULL) {
		g_free (cfg->cs_server);
		cfg->cs_server = NULL;
	}

	cfg->cs_server = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_cs_port (guint integer) 
{
	if (integer < 1 || integer > 65535) {
		g_warning ("%s: port value:%d was out of range (1 to 64k)", __FUNCTION__, integer);
		return FALSE;
	}

	cfg->cs_port = integer;

	return TRUE;
}

gboolean gj_config_set_cs_username (const gchar *string) 
{
	if (string == NULL) {
		g_warning ("%s: username was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->cs_username != NULL) {
		g_free (cfg->cs_username);
		cfg->cs_username = NULL;
	}

	cfg->cs_username = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_cs_password (const gchar *string) 
{
	if (string == NULL) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->cs_password != NULL) {
		g_free (cfg->cs_password);
		cfg->cs_password = NULL;
	}

	cfg->cs_password = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_cs_resource (const gchar *string) 
{
	if (string == NULL) {
		g_warning ("%s: resource was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->cs_resource != NULL) {
		g_free (cfg->cs_resource);
		cfg->cs_resource = NULL;
	}

	cfg->cs_resource = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_cs_priority (gint8 integer) 
{
	cfg->cs_priority = integer;
	return TRUE;
}


gboolean gj_config_set_cs_prompt_for_password (gboolean bool) 
{
	if (bool == TRUE) {
		cfg->cs_prompt_for_password = TRUE;
	} else {
		cfg->cs_prompt_for_password = FALSE; 
	}

	return TRUE;
}

gboolean gj_config_set_cs_use_plain_text_password (gboolean bool) 
{
	if (bool == TRUE) {
		cfg->cs_use_plain_text_password = TRUE; 
	} else {
		cfg->cs_use_plain_text_password = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_cs_use_ssl (gboolean bool) 
{
	if (bool == TRUE) {
		cfg->cs_use_ssl = TRUE;
	} else {
		cfg->cs_use_ssl = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_cs_auto_connect_on_startup (gboolean bool) 
{
	if (bool == TRUE) {
		cfg->cs_auto_connect_on_startup = TRUE;
	} else {
		cfg->cs_auto_connect_on_startup = FALSE; 
	}

	return TRUE;
}

gboolean gj_config_set_cs_auto_reconnect_on_disconnect (gboolean bool) 
{
	if (bool == TRUE) {
		cfg->cs_auto_reconnect_on_disconnect = TRUE;
	} else {
		cfg->cs_auto_reconnect_on_disconnect = FALSE; 
	}

	return TRUE;
}

gboolean gj_config_set_mpf_hide_offline_contacts (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_hide_offline_contacts = TRUE;
	} else {
		cfg->mpf_hide_offline_contacts = FALSE; 
	}

	return TRUE;
}

gboolean gj_config_set_mpf_hide_offline_group (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_hide_offline_group = TRUE;
	} else {
		cfg->mpf_hide_offline_group = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_hide_agents (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_hide_agents = TRUE;
	} else {
		cfg->mpf_hide_agents = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_hide_contact_presence_details (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_hide_contact_presence_details = TRUE;
	} else {
		cfg->mpf_hide_contact_presence_details = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_double_click_for_chat (gboolean bool) 
{
	if (bool == TRUE) {
		cfg->mpf_double_click_for_chat = TRUE;
	} else {
		cfg->mpf_double_click_for_chat = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_open_window_on_new_message (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_open_window_on_new_message = TRUE;
	} else {
		cfg->mpf_open_window_on_new_message = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_raise_window_on_message (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_raise_window_on_message = TRUE;
	} else {
		cfg->mpf_raise_window_on_message  = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_hide_time (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_hide_time = TRUE;
	} else {
		cfg->mpf_hide_time  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_reverse_text_direction (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_reverse_text_direction = TRUE;
	} else {
		cfg->mpf_reverse_text_direction  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_use_smileys (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_use_smileys = TRUE;
	} else {
		cfg->mpf_use_smileys = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_use_spell_checker (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_use_spell_checker = TRUE;
	} else {
		cfg->mpf_use_spell_checker = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_use_auto_away (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_use_auto_away = TRUE;
	} else {
		cfg->mpf_use_auto_away = FALSE;
	}

	return TRUE;
}

gboolean gj_config_set_mpf_auto_away_after (guint integer)
{
	if (integer < 1 || integer > 1000) {
		g_warning ("%s: value:%d was out of range (1 to 1000)", __FUNCTION__, integer);
		return FALSE;
	}

	cfg->mpf_auto_away_after = integer;

	return TRUE;
}

gboolean gj_config_set_mpf_auto_xa_after (guint integer)
{
	if (integer < 1 || integer > 1000) {
		g_warning ("%s: value:%d was out of range (1 to 1000)", __FUNCTION__, integer);
		return FALSE;
	}

	cfg->mpf_auto_xa_after = integer;

	return TRUE;
}

gboolean gj_config_set_mpf_auto_away_status (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: status was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_auto_away_status != NULL) {
		g_free (cfg->mpf_auto_away_status);
		cfg->mpf_auto_away_status = NULL;
	}

	cfg->mpf_auto_away_status = g_strdup (string);
	
	return TRUE;
}

gboolean gj_config_set_mpf_auto_xa_status (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: status was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_auto_xa_status != NULL) {
		g_free (cfg->mpf_auto_xa_status);
		cfg->mpf_auto_xa_status = NULL;
	}

	cfg->mpf_auto_xa_status = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_sound_on_user_online (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_sound_on_user_online = TRUE;
	} else {
		cfg->mpf_sound_on_user_online  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_sound_on_user_offline (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_sound_on_user_offline = TRUE;
	} else {
		cfg->mpf_sound_on_user_offline  = FALSE;
	}
	return TRUE;
} 

gboolean gj_config_set_mpf_sound_on_receive_message (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_sound_on_receive_message = TRUE;
	} else {
		cfg->mpf_sound_on_receive_message  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_sound_on_receive_chat (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_sound_on_receive_chat = TRUE;
	} else {
		cfg->mpf_sound_on_receive_chat  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_sound_on_receive_headline (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_sound_on_receive_headline = TRUE;
	} else {
		cfg->mpf_sound_on_receive_headline  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_sound_on_receive_error (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_sound_on_receive_error = TRUE;
	} else {
		cfg->mpf_sound_on_receive_error  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_sound_file_on_user_online (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_sound_file_on_user_online != NULL) {
		g_free (cfg->mpf_sound_file_on_user_online);
		cfg->mpf_sound_file_on_user_online = NULL;
	}

	cfg->mpf_sound_file_on_user_online = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_sound_file_on_user_offline (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_sound_file_on_user_offline != NULL) {
		g_free (cfg->mpf_sound_file_on_user_offline);
		cfg->mpf_sound_file_on_user_offline = NULL;
	}

	cfg->mpf_sound_file_on_user_offline = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_sound_file_on_receive_message (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_sound_file_on_receive_message != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_message);
		cfg->mpf_sound_file_on_receive_message = NULL;
	}

	cfg->mpf_sound_file_on_receive_message = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_sound_file_on_receive_chat (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_sound_file_on_receive_chat != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_chat);
		cfg->mpf_sound_file_on_receive_chat = NULL;
	}

	cfg->mpf_sound_file_on_receive_chat = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_sound_file_on_receive_headline (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_sound_file_on_receive_headline != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_headline);
		cfg->mpf_sound_file_on_receive_headline = NULL;
	}

	cfg->mpf_sound_file_on_receive_headline = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_sound_file_on_receive_error (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_sound_file_on_receive_error != NULL) {
		g_free (cfg->mpf_sound_file_on_receive_error);
		cfg->mpf_sound_file_on_receive_error = NULL;
	}

	cfg->mpf_sound_file_on_receive_error = g_strdup (string);

	return TRUE;
}
 
gboolean gj_config_set_mpf_use_docklet (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_use_docklet = TRUE;
	} else {
		cfg->mpf_use_docklet  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_set_mpf_log_location (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_log_location != NULL) {
		g_free (cfg->mpf_log_location);
		cfg->mpf_log_location = NULL;
	}

	cfg->mpf_log_location = g_strdup (string);

	return TRUE;
}

gboolean gj_config_set_mpf_history_location (const gchar *string)
{
	if (string == NULL) {
		g_warning ("%s: location was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cfg->mpf_history_location != NULL) {
		g_free (cfg->mpf_history_location);
		cfg->mpf_history_location = NULL;
	}

	cfg->mpf_history_location = g_strdup (string);

	return TRUE;
}
 
gboolean gj_config_set_mpf_use_history (gboolean bool)
{
	if (bool == TRUE) {
		cfg->mpf_use_history = TRUE;
	} else {
		cfg->mpf_use_history  = FALSE;
	}

	return TRUE;
} 

gboolean gj_config_get_pp_index (gint index, gchar **show, gchar **status, gchar **priority)
{
	return pp_get_index (index, show, status, priority);
}

gboolean gj_config_set_pp_index (gint index, gchar *show, gchar *status, gchar *priority)
{
	return pp_set_index (index, show, status, priority);
}

gboolean gj_config_set_pp_index_delete (gint index, gchar *show, gchar *status, gchar *priority)
{
	return pp_set_index_delete (index);
}
 
gint gj_config_get_pp_count ()
{
	gint i = 0;
	gchar *show = NULL;

	for (i=1; i<100; i++) {
		if (!pp_get_index (i, &show, NULL, NULL) || !show) {
			break;
		}
	}
    
	return i;
}

#ifdef __cplusplus
}
#endif
