/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#include <libxml/parser.h>
#include <libxml/parserInternals.h>

#include "gj_parser.h"
#include "gj_connection.h"
#include "gj_stream.h"
#include "gj_iq.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_support.h"


struct t_parser {
	gint id;

	gchar *saved_buffer;
	gboolean received_stream;

	xmlParserCtxtPtr ctxt;

	GjConnection c;
    
	GjParserErrorCallback error_cb;
	GjParserPresenceCallback presence_cb;
	GjParserMessageCallback message_cb;
	GjParserIQCallback iq_cb;
	GjParserStreamCallback stream_cb;
	GjParserOtherCallback other_cb;
};


static void gj_parser_init ();

static void gj_parser_destroy_cb (gpointer data);

/* parsing */
static gboolean gj_parser_input_preliminary_check (gchar *buf);
static gboolean gj_parser_input_node (GjParser p, xmlNodePtr node);
static gboolean gj_parser_parse_xml (GjParser p, const gchar *input);


/* parser callbacks */
static void gj_parser_doc_start_cb (void *user_data);
static void gj_parser_doc_end_cb (void *user_data);
static void gj_parser_doc_element_start_cb (void *user_data, const xmlChar *name, const xmlChar **attrs);
static void gj_parser_doc_element_end_sb (void *user_data, const xmlChar *name);
static xmlEntityPtr gj_parser_doc_entity_cb (void *user_data, const xmlChar *name) ;
static void gj_parser_doc_warning_cb (void *user_data, const char *msg, ...);
static void gj_parser_doc_error_cb (void *user_data, const char *msg, ...);
static void gj_parser_doc_fatal_error_cb (void *user_data, const char *msg, ...);


static GHashTable *parsers = NULL;
static GjConnection last_connection = NULL;


static void gj_parser_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;

	parsers = g_hash_table_new_full (g_direct_hash, 
					 g_direct_equal, 
					 NULL, 
					 (GDestroyNotify) gj_parser_destroy_cb);   
}

void gj_parser_stats ()
{
	g_message ("%s: table size:%d", __FUNCTION__, g_hash_table_size (parsers));
}

static void gj_parser_destroy_cb (gpointer data)
{
	GjParser p = (GjParser) data;

	if (p == NULL) {
		g_warning ("%s: parser was NULL", __FUNCTION__);
		return;
	}
  
	if (p->saved_buffer != NULL) {
		g_free (p->saved_buffer);
		p->saved_buffer = NULL;
	}

	if (p->c) {
		gj_connection_unref (p->c);
	}

	g_free (p);
}

GjParser gj_parser_new (GjConnection c)
{
	GjParser p = NULL;
  
	gj_parser_init ();

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	if ((p = (GjParser) g_new0 (struct t_parser,1)) != NULL) {
		p->id = gj_get_unique_id ();

		p->saved_buffer = NULL;
		p->received_stream = FALSE;

		p->ctxt = NULL;

		p->c = gj_connection_ref (c);
      
		p->error_cb = NULL;
		p->presence_cb = NULL;
		p->message_cb = NULL;
		p->iq_cb = NULL;
		p->stream_cb = NULL;
		p->other_cb = NULL;
      
		g_hash_table_insert (parsers, c, p);
	}

	return p;  
}

void gj_parser_free (GjParser p)
{
	if (p == NULL) {
		g_warning ("%s: parser was NULL", __FUNCTION__);
		return;
	}

	g_hash_table_remove (parsers, p->c);
}

GjParser gj_parser_find (GjConnection c)
{
	GjParser p = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	p = g_hash_table_lookup (parsers, c);
	return p;
}

/* global vars */
static xmlSAXHandler sax = {
	0, /* internalSubset */
	0, /* isStandalone */
	0, /* hasInternalSubset */
	0, /* hasExternalSubset */
	0, /* resolveEntity */
	(getEntitySAXFunc)gj_parser_doc_entity_cb, /* getEntity */
	0, /* entityDecl */
	0, /* notationDecl */
	0, /* attributeDecl */
	0, /* elementDecl */
	0, /* unparsedEntityDecl */
	0, /* setDocumentLocator */
	(startDocumentSAXFunc)gj_parser_doc_start_cb, /* startDocument */
	(endDocumentSAXFunc)gj_parser_doc_end_cb, /* endDocument */
	(startElementSAXFunc)gj_parser_doc_element_start_cb, /* startElement */
	(endElementSAXFunc)gj_parser_doc_element_end_sb, /* endElement */
	0, /* reference */
	0, /* (charactersSAXFunc)gladeCharacters, *//* characters */
	0, /* ignorableWhitespace */
	0, /* processingInstruction */
	0, /* (commentSAXFunc)gladeComment, *//* comment */
	(warningSAXFunc)gj_parser_doc_warning_cb, /* warning */
	(errorSAXFunc)gj_parser_doc_error_cb, /* error */
	(fatalErrorSAXFunc)gj_parser_doc_fatal_error_cb, /* fatalError */
};

static gboolean gj_parser_input_preliminary_check (gchar *buf)
{
	/* check start has opening tag */
	if (buf[0] != '<') {
		return FALSE;
	}

	/* check end has closing tag */
	if (buf[strlen (buf)-1] != '>') {
		return FALSE;
	}
  
	/* we do not want to check tag names:
	   what happens if the message is -

	   <presence></presence><message></message>

	   the tags will not match.
	   this is just a basic test for opening and closing chevrons 
	*/

	return TRUE;
}

void gj_parser_push_input_from_connection (GjConnection c, gchar *buf)
{
	GjParser p = NULL;
	GjConnection c_to_use = NULL;

	if (buf == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return;
	}

	if (c == NULL && last_connection == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return;
	} else if (c == NULL && last_connection != NULL) {
		/* use last connection */
		c_to_use = last_connection;
	} else {
		/* use connection given */
		c_to_use = c;
		last_connection = c;
	}

	p = gj_parser_find (c_to_use);
	if (p == NULL) {
		g_warning ("%s: parser could not be found", __FUNCTION__);
		return;
	}

	g_message ("%s: parsing %d bytes, %d characters from connection:0x%.8x",
		   __FUNCTION__, (gint)strlen (buf), (gint)g_utf8_strlen (buf, -1), (gint)c);

	/* parse */
	gj_parser_push_input (p, buf);
}

void gj_parser_push_input (GjParser p, gchar *buf)
{
	gint len = 0;
	gchar *xml = NULL;
	gchar *xml_well_formed = NULL;
	gchar *stripped = NULL;

	xmlDocPtr doc = NULL;
	xmlNodePtr current = NULL;
  
	if (p == NULL) {
		g_warning ("%s: parser was NULL", __FUNCTION__);
		return;
	}

	if (buf == NULL || strlen (buf) < 1) {
		g_warning ("%s: buf is NULL or size is < 1", __FUNCTION__);
		return;
	}

	/* check for valid utf8 */
	if(g_utf8_validate(buf, -1, NULL) == FALSE) {
		/* SEND ERROR UP HERE... */
		g_warning ("%s: buf is NOT valid utf8!", __FUNCTION__);
		return;
	}

	/* check there is more than white space */
	stripped = g_strdup (buf);
	if (stripped != NULL) {
		g_strstrip (stripped);
		len = g_utf8_strlen (stripped,-1);
      
		g_free (stripped);
      
		if (len < 1) {
			g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: empty string, ignoring...", __FUNCTION__);
			return;
		}
	}

	if (p->saved_buffer != NULL) {
		g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: buffer saved previously, concating to new buffer", __FUNCTION__);
		xml = g_strconcat (p->saved_buffer, buf, NULL);
      
		g_free (p->saved_buffer);
		p->saved_buffer = NULL;
	} else {
		/* if no saved buffer, set xml to the new buf */
		g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: duplicating buffer for xml preliminary check", __FUNCTION__);
		xml = g_strdup (buf);
	}

	if (gj_parser_input_preliminary_check (xml) == FALSE) {
		g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: preliminary check failed - perhaps there is more data to come...", __FUNCTION__);
		p->saved_buffer = g_strdup (xml);
      
		g_free (xml);
		return;
	} else {
		g_free (p->saved_buffer);
		p->saved_buffer = NULL;
	}

	if (p->received_stream == FALSE) {
		/* parse and return - 
		   we know if will not be a well formed document */
		gj_parser_parse_xml (p, xml);
		g_free (xml);
		return;
	}

	/* add tags otherwise SAX parser complains it is not well formed */
	xml_well_formed = g_strconcat ("<gj>", xml, "</gj>", NULL);

	/* set up the parser - and check for well formedness */
	if (gj_parser_parse_xml (p, xml_well_formed) == FALSE) {
		g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: xml is NOT well formed", __FUNCTION__);
		
		p->saved_buffer = g_strdup (xml);
	
		/* clean up */
		g_free (xml);
		g_free (xml_well_formed);
		return;
	}

	/* call the parser to do its job */
	doc = xmlSAXParseDoc (NULL, (xmlChar*)xml_well_formed, 1);
	
	if (doc == NULL) {
		g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: parser context was NULL, can not continue...", __FUNCTION__);
		return;
	}

	/* clean up */
	g_free (xml);
	g_free (xml_well_formed);

	/* set first node */
	if (doc->xmlChildrenNode != NULL) {
		current = doc->xmlChildrenNode->xmlChildrenNode;
	}
  
	while (current != NULL) {
		gj_parser_input_node (p, xmlCopyNode (current,1));
		current = current->next;
	}
}

static gboolean gj_parser_input_node (GjParser p, xmlNodePtr node)
{
	const xmlChar *type = NULL;
	GjParserOtherCallback other_cb = NULL;

	if (p == NULL) {
		g_warning ("%s: parser was NULL", __FUNCTION__);
		return FALSE;
	}   
    
	if (node == NULL) {
		g_warning ("%s: failed to continue, node was NULL", __FUNCTION__);
		return FALSE;
	}

#if 0
	/* print tree */
	g_log ("XML", G_LOG_LEVEL_DEBUG,"****** started printing tree node:0x%.8x *********", (gint)node); 
	gj_parser_print_tree (node, 0); 
	g_log ("XML", G_LOG_LEVEL_DEBUG,"****** stopped printing tree node:0x%.8x *********", (gint)node); 
#endif

	/* parse type */
	if (node == NULL) {
		g_warning ("%s: node passed was NULL", __FUNCTION__);
		return FALSE;
	}

	/* get type */
	type = gj_parser_get_element (node);

	/* should really call callbacks instead of 
	   statically defined functions ! */
	if (xmlStrcmp (type, (guchar*)"presence") == 0) {
		/*       GjParserPresenceCallback cb = NULL; */

		/*       if ((cb = p->presence_cb) == NULL) */
		/* 	  g_warning ("%s: no presence callback", __FUNCTION__); */
		/* 	  return TRUE; */
		/*       } */

		gj_presence_receive (p->c, node); 
		/*       (cb) (node); */
		return TRUE;
	}

	if (xmlStrcmp (type, (guchar*)"message") == 0) {
		/*       GjParserMessageCallback cb = NULL; */

		/*       if ((cb = p->message_cb) == NULL) */
		/* 	  g_warning ("%s: no message callback", __FUNCTION__); */
		/* 	  return TRUE; */
		/*       } */

		gj_message_receive (p->c, node); 
		/*       (cb) (node); */
		return TRUE;
	}
  
	if (xmlStrcmp (type, (guchar*)"iq") == 0) {
		/*       GjParserIQCallback cb = NULL; */

		/*       if ((cb = p->iq_cb) == NULL) */
		/* 	  g_warning ("%s: no iq callback", __FUNCTION__); */
		/* 	  return TRUE; */
		/*       } */

		gj_iq_receive (p->c, node); 
		/*       (cb) (node); */
		return TRUE;
	}
  
	if (xmlStrcmp (type, (guchar*)"stream") == 0) {
		/*       GjParserStreamCallback cb = NULL; */
      
		/*       if ((cb = p->stream_cb) == NULL) */
		/* 	  g_warning ("%s: no stream callback", __FUNCTION__); */
		/* 	  return TRUE; */
		/*       } */

		/*       (cb) (node); */
		return TRUE;
	}
  
	if (xmlStrcmp (type, (guchar*)"error") == 0) {
		/*       GjParserErrorCallback cb = NULL; */
      
		/*       if ((cb = p->error_cb) == NULL) */
		/* 	  g_warning ("%s: no error callback", __FUNCTION__); */
		/* 	  return TRUE; */
		/*       } */
      
		gj_stream_receive_error (p->c, node); 
		/*       (cb) (node); */
		return TRUE;
	}

	/* NOTE: we *REALLY* should ignore the previous buffers and 
	   wait for the next message if this happens */
	g_warning ("%s: unexpected type of msg from connection:0x%.8x", __FUNCTION__, (gint)p->c);
  
	if ((other_cb = p->other_cb) == NULL) {
		g_warning ("%s: no default/other callback", __FUNCTION__);
		return FALSE;
	}

	(other_cb) (p,node);
	return FALSE;
}

static gboolean gj_parser_parse_xml (GjParser p, const gchar *input) 
{
	gboolean error = 0;

	if (p == NULL) {
		g_warning ("%s: parser was NULL", __FUNCTION__);
		return FALSE;
	}

	if (input == NULL) {
		g_warning ("%s: input was NULL", __FUNCTION__);
		return FALSE;
	}

	p->ctxt = xmlCreatePushParserCtxt (&sax, (void*)p, input, strlen (input), NULL);
	if (p->ctxt == NULL) {
		g_warning ("%s: could not create push parser", __FUNCTION__);
		return FALSE;      
	}

	xmlParseDocument (p->ctxt);
   
	if (p->ctxt->wellFormed) {
		error = TRUE;
	} else {
		error = FALSE;
	}

	if (&sax != NULL) {
		p->ctxt->sax = NULL;
	}

	xmlFreeParserCtxt (p->ctxt);
	p->ctxt = NULL;

	return error;
}

gboolean gj_parser_push_output (GjConnection c, xmlNodePtr node)
{
	gchar* output = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (node == NULL) {
		g_warning ("%s: failed to continue, node was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if ((output = gj_parser_get_str_from_node (node)) == NULL) {
		g_warning ("%s: out was NULL from node:0x%.8x", __FUNCTION__, (gint)node);
		return FALSE;
	}

	gj_connection_write (c, output);

	/* clean up */
	g_free (output);

	return TRUE;
}

gboolean gj_parser_push_output_from_text (GjConnection c, gchar *text)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}
	
	if (text == NULL) {
		g_warning ("%s: text was NULL", __FUNCTION__);
		return FALSE;
	}

	gj_connection_write (c, text);

	return TRUE;
}

/* utils */
xmlNodePtr gj_parser_print_tree (xmlNodePtr node, gint depth)
{
	xmlNodePtr result = NULL;
	gchar buffer[1024];

	if (node == NULL) {
		return NULL;
	}

	/* set buffer to spaces */
	memset (buffer, 32, sizeof (buffer));
	buffer[(depth*2)] = '\0';

	if (node->name != NULL) {
		if (xmlStrcasecmp (node->name, (guchar*)"text") != 0 && xmlStrlen (node->name) > 0) {
			g_log ("XML", G_LOG_LEVEL_DEBUG, "%s > name:'%s' (0x%.8x)", buffer, node->name, (gint)node->name);
		}
      
		if (node->content != NULL && xmlStrlen (node->content) > 0) {
			g_log ("XML", G_LOG_LEVEL_DEBUG, "%s * content:'%s' (0x%.8x)", buffer, node->content, (gint)node->content);
		}
	}

	result = gj_parser_print_tree (node->xmlChildrenNode,depth+1);

	return result?result:gj_parser_print_tree (node->next,depth);
}

gchar *gj_parser_get_str_from_node (const xmlNodePtr node)
{
	xmlDocPtr doc = NULL;
	xmlNodePtr copy = NULL;
	xmlChar *buf = NULL;
  
	gint size = 0;

	gchar* output = NULL;

	if (node == NULL) {
		g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: failed to continue, node was NULL", __FUNCTION__);
		return NULL;
	}

	/* copy the node first */
	copy = xmlCopyNode (node, 1);

	/* create doc */
	if ((doc = xmlNewDoc ((guchar*)"1.0")) == NULL) {
		/* failed */
		g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: failed to create new xml document, returning...", __FUNCTION__);
		return NULL;
	}
  
	/* set root node */
	xmlDocSetRootElement (doc, copy);
  
	/* save to file and get mem */
	xmlDocDumpMemory (doc, &buf, &size);

	if (buf == NULL) {
		g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: failed because xmlDocDumpMemory returned NULL", __FUNCTION__);
		return NULL;
	}

	/* 
	   skip header with ver 
	   and create for fl 
	*/
	buf[size] = '\0';
	buf+=21;

	xmlFreeDoc (doc);
  
	output = g_strdup_printf ("%s", buf);
	return output;
}


gint gj_parser_doc_count_top_level_nodes (xmlDocPtr doc)
{
	xmlNodePtr root = NULL;

	gint count = 0;

	if (doc == NULL) {
		return -1;
	}

	/*root = doc->xmlRootNode;*/
	root = doc->xmlRootNode->xmlChildrenNode;

	while (root != NULL) {
		root = root->next;
		count++;
	}

	return count;
}

const xmlChar *gj_parser_get_element (xmlNodePtr node)
{
	if (node == NULL) {
		return NULL;
	}

	return node->name;
}

const xmlChar *gj_parser_get_content (xmlNodePtr node)
{
	if (node == NULL) {
		return NULL;
	}

	if (node->xmlChildrenNode == NULL) {
		return NULL;
	}

	return node->xmlChildrenNode->content;
}

xmlChar *gj_parser_find_by_attr_and_ret (xmlNodePtr node, xmlChar *attrname)
{
	xmlAttrPtr attr = NULL;

	if (node == NULL) {
		return NULL;
	}

	if (attrname == NULL) {
		return NULL;
	}

	attr = gj_parser_find_by_attr (node->properties,attrname);
  
	if (attr == NULL) {
		return NULL;
	}

	if (attr->xmlChildrenNode != NULL) {
		return attr->xmlChildrenNode->content;
	} else {
		return NULL;
	}
}

const xmlChar *gj_parser_find_by_ns_and_ret (xmlNodePtr node)
{
	xmlNodePtr nsnode = NULL;

	if (node == NULL) {
		return NULL;
	}

	nsnode = gj_parser_find_by_ns (node);
  
	if (nsnode == NULL) {
		return NULL;
	}

	if (nsnode->ns != NULL && nsnode->ns->href != NULL) {
		return nsnode->ns->href;
	} else {
		return NULL;
	}
}

xmlChar * gj_parser_find_by_node_and_ret (xmlNodePtr node, xmlChar *text)
{
	xmlNodePtr found = NULL;
	xmlChar *contents = NULL;

	if ((found = gj_parser_find_by_node (node,text)) != NULL) {
		if (found->xmlChildrenNode != NULL) {
			contents = found->xmlChildrenNode->content;  
		}
	}

	return contents;
}

const xmlChar * gj_parser_find_by_node_on_parent_and_ret (xmlNodePtr node, xmlChar *text)
{
	GList *node_list = NULL;

	node_list = gj_parser_find_by_node_and_ret_by_list (node, text);
  
	if (node_list != NULL) {
		gint i = 0;

		for (i=0;i < g_list_length (node_list);i++) {
			xmlNodePtr node = NULL;
	  
			/* checks */
			if ((node = (xmlNodePtr) g_list_nth_data (node_list, i)) == NULL) {
				continue;
			}
	  
			if (node->xmlChildrenNode == NULL || node->xmlChildrenNode->content == NULL) {
				continue;
			}
	  
			return node->xmlChildrenNode->content;
		}
	}

	return NULL;
}

GList *gj_parser_find_by_node_and_ret_contents_by_list (xmlNodePtr node, xmlChar *text)
{
	GList *node_list = NULL;
	GList *contents_list = NULL;

	node_list = gj_parser_find_by_node_and_ret_by_list (node, text);
  
	if (node_list != NULL) {
		gint i = 0;

		for (i=0;i < g_list_length (node_list);i++) {
			xmlNodePtr node = NULL;
	  
			/* checks */
			if ((node = (xmlNodePtr) g_list_nth_data (node_list, i)) == NULL) {
				continue;
			}
	  
			if (node->xmlChildrenNode == NULL || node->xmlChildrenNode->content == NULL) {
				continue;
			}
	  
			/* get contents and add to list */
			contents_list = g_list_append (contents_list, 
						       g_strdup ((gchar*)node->xmlChildrenNode->content));

			/* 	  g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: contents[%d]:'%s'", __FUNCTION__, i, node->xmlChildrenNode->content); */
		}
	}

	return contents_list;
}

GList *gj_parser_find_by_node_on_parent_and_ret_contents_by_list (xmlNodePtr node, xmlChar *text)
{
	GList *node_list = NULL;
	GList *contents_list = NULL;

	node_list = gj_parser_find_by_node_on_parent_and_ret_by_list (node, text);
  
	if (node_list != NULL) {
		gint i = 0;

		for (i=0;i < g_list_length (node_list);i++) {
			xmlNodePtr node = NULL;
	  
			/* checks */
			if ((node = (xmlNodePtr) g_list_nth_data (node_list, i)) == NULL) {
				continue;
			}
	  
			if (node->xmlChildrenNode == NULL || node->xmlChildrenNode->content == NULL) {
				continue;
			}
	  
			/* get contents and add to list */
			contents_list = g_list_append (contents_list, 
						       g_strdup ((gchar*)node->xmlChildrenNode->content));

			/* 	  g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: contents[%d]:'%s'", __FUNCTION__, i, node->xmlChildrenNode->content); */
		}
	}

	return contents_list;
}

xmlNodePtr gj_parser_find_by_node (xmlNodePtr node, xmlChar *text)
{
	xmlNodePtr result;
  
	if (node == NULL || text == NULL) {
		return NULL;
	}

	if (node->name != NULL && xmlStrcasecmp (node->name, text) == 0) {
		return node;
	}

	result = gj_parser_find_by_node (node->xmlChildrenNode,text);

	return result?result:gj_parser_find_by_node (node->next,text);
}

xmlNodePtr gj_parser_find_by_node_on_parent (xmlNodePtr parent, xmlChar *text)
{
	xmlNodePtr result = NULL;
	xmlNodePtr node = NULL;

	if (parent == NULL || text == NULL) {
		return NULL;
	}

	node = parent->xmlChildrenNode;

	while (node != NULL) {
		if (node->name != NULL && xmlStrcasecmp (node->name, text) == 0) { 
			result = node;
			break;
		}
		
		node = node->next;
	}

	return result;
}

GList *gj_parser_find_by_node_and_ret_by_list (xmlNodePtr node, xmlChar *text)
{
	GList *list = NULL;

	while (node != NULL) {
		if ((node = gj_parser_find_by_node (node, text)) == NULL) {
			break;
		}
		
		list = g_list_append (list, node);
		
		node = node->next;
	}

	return list;
}

GList *gj_parser_find_by_node_on_parent_and_ret_by_list (xmlNodePtr node, xmlChar *text)
{
	GList *list = NULL;
  
	if (node == NULL) {
		return NULL;
	}

	if ((node = gj_parser_find_by_node_on_parent (node, text)) == NULL) {
		return NULL;
	}

	while (node != NULL) {
		if (node->type == XML_ELEMENT_NODE) {
			list = g_list_append (list, node);
		}

		node = node->next;
	}
	
	return list;
}

xmlNodePtr gj_parser_find_by_node_contents (xmlNodePtr node, xmlChar *contents)
{
	xmlNodePtr result;
  
	if (node == NULL || contents == NULL) {
		return NULL;
	}

	if (node->content != NULL && xmlStrcasecmp (node->content,contents) == 0) {
		return node;
	}

	result = gj_parser_find_by_node_contents (node->xmlChildrenNode,contents);

	return result?result:gj_parser_find_by_node_contents (node->next,contents);
}

xmlAttrPtr gj_parser_find_by_attr (xmlAttrPtr prop, xmlChar *text)
{
	if (text == NULL) {
		return NULL;
	}

	while (prop != NULL) {
		if (xmlStrcasecmp (prop->name,text) == 0) {
			return prop;
		}

		prop = prop->next;
	}

	return NULL;
}

xmlNodePtr gj_parser_find_by_ns (xmlNodePtr node)
{
	xmlNodePtr result;
  
	if (node == NULL) {
		return NULL;
	}

	if (node->ns != NULL) {
		return node;
	}

	result = gj_parser_find_by_ns (node->xmlChildrenNode);

	return result?result:gj_parser_find_by_ns (node->next);
}

xmlNodePtr gj_parser_find_by_node_with_attr (xmlNodePtr node, xmlChar *nodeName, xmlChar *attrName, xmlChar *attrVal)
{
	xmlNodePtr resultNode = NULL;
	xmlAttrPtr resultAttr = NULL;
	xmlAttrPtr attr = NULL;

	if (attrVal == NULL) {
		return NULL;
	}

	while ((resultNode = gj_parser_find_by_node (node,nodeName)) != NULL) {
		attr = resultNode->properties;
		
		while ((resultAttr = gj_parser_find_by_attr (attr,attrName)) != NULL) {
			if (resultAttr->xmlChildrenNode != NULL && resultAttr->xmlChildrenNode->content != NULL) { 
				if (xmlStrcasecmp (resultAttr->xmlChildrenNode->content,attrVal) == 0){
					return resultAttr->parent;
				}
			}
			
			attr = resultAttr->next;
		}
		
		node = resultNode->next;
	}

	return NULL;
}

/*
 * parser callbacks
 */

static void gj_parser_doc_start_cb (void *user_data)
{
	/*   g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: user_data:0x%.8x (parser)", __FUNCTION__, (gint)user_data); */
}

static void gj_parser_doc_end_cb (void *user_data)
{
	/*   g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: user_data:0x%.8x (parser)", __FUNCTION__, (gint)user_data); */
}

static void gj_parser_doc_element_start_cb (void *user_data, const xmlChar *name, const xmlChar **attrs)
{
	GjParser p = NULL;

	/*   g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: user_data:0x%.8x (parser), name:'%s'", __FUNCTION__, (gint)user_data, name); */

	p = (GjParser) user_data;

	if (p == NULL) {
		return;
	}

	if (name != NULL && xmlStrcmp (name, (guchar*)"stream:stream") == 0) {
		gint i = 0;
		gchar *from = NULL;
		gchar *id = NULL;

		for (i=0; attrs[i]; i++) {
			if (xmlStrcmp (attrs[i], (guchar*)"from") == 0) {
				from = g_strdup_printf ("%s", attrs[++i]);
			} else if (xmlStrcmp (attrs[i], (guchar*)"id") == 0) {
				id = g_strdup_printf ("%s", attrs[++i]);
			}
		}

		g_message ("%s: stream started from:'%s', with id:'%s'", __FUNCTION__, from, id);
		
		p->received_stream = TRUE;
		gj_stream_receive (p->c, from, id);
		
		/* clean up */
		g_free (from);
		g_free (id);
	} else if (name != NULL && xmlStrcmp (name, (guchar*)"stream:error") == 0 && 
		   g_strcasecmp ((gchar*)name, "Connection is closing") == 0) {
		p->received_stream = TRUE;
		gj_stream_receive_closing (p->c);
	}
}

static void gj_parser_doc_element_end_sb (void *user_data, const xmlChar *name)
{
	/*   g_log ("XML", G_LOG_LEVEL_DEBUG,"%s: user_data:0x%.8x (parser), name:'%s'", __FUNCTION__, (gint)user_data, name); */
}

static xmlEntityPtr gj_parser_doc_entity_cb (void *user_data, const xmlChar *name) 
{
	/*   g_log ("XML", G_LOG_LEVEL_DEBUG, "%s: user_data:0x%.8x (parser), name:'%s'", __FUNCTION__, (gint)user_data, name); */
	return xmlGetPredefinedEntity (name);
}

static void gj_parser_doc_warning_cb (void *user_data, const char *msg, ...) 
{
	GjParser p = NULL;
	va_list args;

	va_start (args, msg);
	g_logv ("XML", G_LOG_LEVEL_WARNING, msg, args);
	va_end (args);

	p = (GjParser) user_data;
	
	if (p == NULL) { 
		return;
	}

	if (p->saved_buffer == NULL) {
		return;
	}

	g_free (p->saved_buffer);
	p->saved_buffer = NULL;
}

static void gj_parser_doc_error_cb (void *user_data, const char *msg, ...) 
{
	GjParser p = NULL;
	va_list args;

	va_start (args, msg);
	g_logv ("XML", G_LOG_LEVEL_CRITICAL, msg, args);
	va_end (args);

	p = (GjParser) user_data;

	if (p == NULL) {
		return; 
	}

	if (p->saved_buffer == NULL) {
		return;
	}

	g_free (p->saved_buffer);
	p->saved_buffer = NULL;
}

static void gj_parser_doc_fatal_error_cb (void *user_data, const char *msg, ...) 
{
	GjParser p = NULL;
	va_list args;
    
	va_start (args, msg);
	g_logv ("XML", G_LOG_LEVEL_ERROR, msg, args);
	va_end (args);

	p = (GjParser) user_data;

	if (p == NULL) {
		return; 
	}

	if (p->saved_buffer == NULL) {
		return;
	}

	g_free (p->saved_buffer);
	p->saved_buffer = NULL;
}

#ifdef __cplusplus
}
#endif
