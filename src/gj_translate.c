/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>
#include <string.h>

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_typedefs.h"


const gchar *gj_translate_jid_type (GjJIDType type)
{
	switch (type) {
	case GjJIDTypeNull: return _("NULL");
	case GjJIDTypeNone: return _("None");
	case GjJIDTypeContact: return _("Contact");
	case GjJIDTypeAgent: return _("Agent");
	case GjJIDTypeEnd: return "JID TYPE NOT SET";
	}

	return _("unknown");
}

const gchar *gj_translate_roster_item_subscription (GjRosterItemSubscription subscription)
{
	switch (subscription) {
	case GjRosterItemSubscriptionNull: return _("NULL");
	case GjRosterItemSubscriptionNone: return _("None");
	case GjRosterItemSubscriptionTo: return _("To");
	case GjRosterItemSubscriptionFrom: return _("From");
	case GjRosterItemSubscriptionBoth: return _("Both");
	case GjRosterItemSubscriptionEnd: return "ROSTER ITEM SUBSCRIPTION NOT SET";
	}

	return _("unknown");
}

const gchar *gj_translate_roster_item_subscription_as_description (GjRosterItemSubscription subscription)
{
	switch (subscription) {
	case GjRosterItemSubscriptionNull: return _("NULL");
	case GjRosterItemSubscriptionNone: return _("Neither users have presence.");
	case GjRosterItemSubscriptionTo: return _("You can see the users presence.");
	case GjRosterItemSubscriptionFrom: return _("The user can see your presence.");
	case GjRosterItemSubscriptionBoth: return _("Presence is available to and from the user.");
	case GjRosterItemSubscriptionEnd: return _("Subscription has not been set.");
	}

	return _("unknown");
}

const gchar *gj_translate_event_type (GjEventType type)
{
	switch (type) {
	case GjEventTypeNull: return _("NULL");
	case GjEventTypePresence: return _("Presence");
	case GjEventTypeMessage: return _("Message");
	case GjEventTypeFileTransfer: return _("File Transfer");
	case GjEventTypeEnd: return "EVENT TYPE NOT SET";
	}

	return _("unknown");
}

const gchar *gj_translate_presence_type (GjPresenceType type)
{
	switch (type) {
	case GjPresenceTypeNull: return _("NULL");
	case GjPresenceTypeAvailable: return _("Available");
	case GjPresenceTypeUnAvailable: return _("Unavailable");
	case GjPresenceTypeProbe: return _("Probe");
	case GjPresenceTypeSubscribe: return _("Subscribe");
	case GjPresenceTypeUnSubscribe: return _("Unsubscribe");
	case GjPresenceTypeSubscribed: return _("Subscribed");
	case GjPresenceTypeUnSubscribed: return _("Unsubscribed");
	case GjPresenceTypeError: return _("Error");
	case GjPresenceTypeNone: return _("None");
	case GjPresenceTypeEnd: return "PRESENCE TYPE NOT SET";
	}

	return _("unknown");
}

const gchar *gj_translate_presence_show (GjPresenceShow show)
{
	switch (show) {
	case GjPresenceShowNull: return _("NULL");
	case GjPresenceShowAway: return _("Away");
	case GjPresenceShowChat: return _("Chat");
	case GjPresenceShowDND: return _("Busy");
	case GjPresenceShowNormal: return _("Available");
	case GjPresenceShowXA: return _("Not Available");
	case GjPresenceShowInvisible: return _("Invisible");
	case GjPresenceShowEnd: return "PRESENCE SHOW NOT SET";
	}

	return _("unknown");
}

GjPresenceShow gj_translate_presence_show_str (const gchar *show)
{
	if (strcmp (show, _("NULL")) == 0) {
		return GjPresenceShowNull;
	} else if (strcmp (show, _("Away")) == 0) {
		return GjPresenceShowAway;
	} else if (strcmp (show, _("Chat")) == 0) {
		return GjPresenceShowChat;
	} else if (strcmp (show, _("Busy")) == 0) {
		return GjPresenceShowDND;
	} else if (strcmp (show, _("Available")) == 0) {
		return GjPresenceShowNormal;
	} else if (strcmp (show, _("Not Available")) == 0) {
		return GjPresenceShowXA;
	} else if (strcmp (show, _("Invisible")) == 0) {
		return GjPresenceShowInvisible;
	}

	return GjPresenceShowEnd;
}

const gchar *gj_translate_message_type (GjMessageType type)
{
	switch (type) {
	case GjMessageTypeNull: return _("NULL");
	case GjMessageTypeNormal: return _("Normal");
	case GjMessageTypeChat: return _("Chat");
	case GjMessageTypeGroupChat: return _("Group Chat");
	case GjMessageTypeHeadline: return _("Headline");
	case GjMessageTypeError: return _("Error");
	case GjMessageTypeXEvent: return _("X Event");
	case GjMessageTypeEnd: return "MESSAGE TYPE NOT SET";
	}

	return _("unknown");
}

gchar *gj_translate_message_x_event_type (gint type)
{
	gchar *retval = "";

	if (type & GjMessageXEventTypeCancelled) {
		return g_strdup_printf ("cancelled");
	}

	if (type & GjMessageXEventTypeOffline) {
		retval = g_strconcat (retval, "offline | ", NULL);
	}
	
	if (type & GjMessageXEventTypeDelivered) {
		retval = g_strconcat (retval, "delivered | ", NULL);
	}

	if (type & GjMessageXEventTypeDisplayed) {
		retval = g_strconcat (retval, "displayed | ", NULL);
	}

	if (type & GjMessageXEventTypeComposing) {
		retval = g_strconcat (retval, "composing | ", NULL);
	}

	return retval;
}

const gchar *gj_translate_browse_category (GjBrowseCategory category)
{
	switch (category) {
	case GjBrowseCategoryNull: return _("NULL");
	case GjBrowseCategoryApplication: return _("application");
	case GjBrowseCategoryConference: return _("conference");
	case GjBrowseCategoryHeadline: return _("headline");
	case GjBrowseCategoryKeyword: return _("keyword");
	case GjBrowseCategoryRender: return _("render");
	case GjBrowseCategoryService: return _("service");
	case GjBrowseCategoryUser: return _("user");
	case GjBrowseCategoryValidate: return _("validate");
	case GjBrowseCategoryEnd: return "BROWSE CATEGORY NOT SET";
	}
  
	return _("unknown");
}

const gchar *gj_translate_browse_type (GjBrowseType type)
{
	switch (type) {
	case GjBrowseTypeNull: return _("NULL");

		/* application */
	case GjBrowseTypeApplicationBot: return _("Automated conversations");
	case GjBrowseTypeApplicationCalendar: return _("Calendaring and scheduling service");
	case GjBrowseTypeApplicationEditor: return _("Collaborative editor");
	case GjBrowseTypeApplicationFileserver: return _("Available files");
	case GjBrowseTypeApplicationGame: return _("Multi-player game");
	case GjBrowseTypeApplicationWhiteboard: return _("Whiteboard tool");

		/* conference */
	case GjBrowseTypeConferenceIRC: return _("IRC rooms (note: this enables Jabber users to connect to Internet Relay Chat rooms)");
	case GjBrowseTypeConferenceList: return _("Mailing-list-style conferences");
	case GjBrowseTypeConferencePrivate: return _("Private, dynamically-generated conference rooms");
	case GjBrowseTypeConferencePublic: return _("Public, permanent conference rooms");
	case GjBrowseTypeConferenceTopic: return _("Topic-based conferences");
	case GjBrowseTypeConferenceURL: return _("Website-hosted conferences");

		/* headline */
	case GjBrowseTypeHeadlineLogger: return _("Log messages (usually presented in a scrolling GUI)");
	case GjBrowseTypeHeadlineNotice: return _("Alerts and warnings (usually presented as popup messages)");
	case GjBrowseTypeHeadlineRSS: return _("Rich Site Summary syndication");
	case GjBrowseTypeHeadlineStock: return _("Stock market information by symbol (ticker)");

		/* keyword */
	case GjBrowseTypeKeywordDictionary: return _("Dictionary lookup service");
	case GjBrowseTypeKeywordDNS: return _("DNS resolver");
	case GjBrowseTypeKeywordSoftware: return _("Software search");
	case GjBrowseTypeKeywordThesaurus: return _("Thesaurus lookup service");
	case GjBrowseTypeKeywordWeb: return _("Web search");
	case GjBrowseTypeKeywordWhoIs: return _("Whois query service");

		/* render */
	case GjBrowseTypeRenderEn2Fr: return _("English to French");
	case GjBrowseTypeRenderA2B: return _("Other language to language (using standard language codes)");
	case GjBrowseTypeRenderTTS: return _("Text to Speech");

		/* service */
	case GjBrowseTypeServiceAIM: return _("AIM transport");
	case GjBrowseTypeServiceICQ: return _("ICQ transport");
	case GjBrowseTypeServiceIRC: return _("IRC gateway (note: this enables IRC users to connect to Jabber)");
	case GjBrowseTypeServiceJabber: return _("A Jabber server which conforms to the specification for the 'jabber:client' namespace");
	case GjBrowseTypeServiceJUD: return _("Jabber User Directory");
	case GjBrowseTypeServiceMSN: return _("MSN transport");
	case GjBrowseTypeServicePager: return _("Pager gateway");
	case GjBrowseTypeServiceServerList: return _("A list of servers. It is assumed that this node has service/* children");
	case GjBrowseTypeServiceSMS: return _("SMS gateway");
	case GjBrowseTypeServiceSMTP: return _("SMTP gateway");
	case GjBrowseTypeServiceYahoo: return _("Yahoo! transport");

		/* user */
	case GjBrowseTypeUserClient: return _("A standard or fully-featured Jabber client compliant with the 'jabber:client' namespace");
	case GjBrowseTypeUserForward: return _("A forward alias");
	case GjBrowseTypeUserInbox: return _("An alternate inbox");
	case GjBrowseTypeUserPortable: return _("A portable device implementing some of the 'jabber:client' namespace");
	case GjBrowseTypeUserVoice: return _("A node providing phone or voice access");

		/* validate */
	case GjBrowseTypeValidateGrammar: return _("Grammar-checking tool");
	case GjBrowseTypeValidateSpell: return _("Spell-checking tool");
	case GjBrowseTypeValidateXML: return _("XML validator");

	case GjBrowseTypeEnd: return "BROWSE TYPE NOT SET";
	}

	return _("unknown");
}

#ifdef __cplusplus
}
#endif
