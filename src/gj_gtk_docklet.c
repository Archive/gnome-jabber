/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif


#include <stdio.h>

#include <gtk/gtk.h>

#include "gj_main.h"
#include "gj_config.h"

#ifndef G_OS_WIN32
# include "eggtrayicon.h"
#else
# include <windows.h>
#endif

#include "gj_gtk_docklet.h"
#include "gj_gtk_stock.h"
#include "gj_gtk_support.h"


typedef struct  {

#ifndef G_OS_WIN32
	EggTrayIcon *docklet;

	/* widgets */
	GtkWidget *box;
	GtkWidget *image;
#else
	HINSTANCE hinstance;
	HWND hwnd;
	NOTIFYICONDATA nid;

	HICON hicon;
#endif

	/* callbacks */
	GjDockletClickCallback left_cb;
	GjDockletClickCallback right_cb;
    
	gpointer user_data;

	/* info */
	gchar *hint;


} GjDocklet;


static GjDocklet *docklet = NULL;


#ifndef G_OS_WIN32 

/* callbacks */
static void gj_gtk_docklet_embedded (GtkWidget *widget, void *data); 
static void gj_gtk_docklet_destroyed (GtkWidget *widget, void *data); 
static void gj_gtk_docklet_clicked (GtkWidget *button, GdkEventButton *event, void *data);


gboolean gj_gtk_docklet_load (GjDockletClickCallback left_cb,
			      GjDockletClickCallback right_cb,
			      gpointer user_data) 
{
	if (docklet != NULL) {
		return TRUE;
	}

	docklet = g_new0 (GjDocklet, 1);

	docklet->left_cb = left_cb;
	docklet->right_cb = right_cb;
    
	docklet->user_data = user_data;
    
	docklet->docklet = egg_tray_icon_new (PACKAGE_NAME);
	docklet->box = gtk_event_box_new ();
	docklet->image = gtk_image_new ();
    
	gj_gtk_docklet_set_image_from_stock (GTK_STOCK_DIALOG_INFO); 
    
	gtk_container_add (GTK_CONTAINER (docklet->box), docklet->image);
	gtk_container_add (GTK_CONTAINER (docklet->docklet), docklet->box);
	gtk_widget_show_all (GTK_WIDGET (docklet->docklet));
    
	/* hook up signals */
	g_signal_connect (G_OBJECT (docklet->docklet), "embedded", G_CALLBACK (gj_gtk_docklet_embedded), NULL);
	g_signal_connect (G_OBJECT (docklet->docklet), "destroy", G_CALLBACK (gj_gtk_docklet_destroyed), NULL);
	g_signal_connect (G_OBJECT (docklet->box), "button-press-event", G_CALLBACK (gj_gtk_docklet_clicked), NULL);
    
	/* ref the docklet before we bandy it about the place */
	g_object_ref (G_OBJECT (docklet->docklet));

	return TRUE;
}

gboolean gj_gtk_docklet_unload () 
{
	if (docklet == NULL) {
		return TRUE;
	}
    
	/* what is the point in this? */
	g_object_unref (G_OBJECT (docklet->docklet));

	gtk_widget_destroy (docklet->box);

	docklet->docklet = NULL;
	docklet->box = NULL;
	docklet->image = NULL;

	docklet->left_cb = NULL;
	docklet->right_cb = NULL;

	docklet->user_data = NULL;
    
	g_free (docklet->hint);
    
	docklet = NULL;
    
	return TRUE;
}

gboolean gj_gtk_docklet_set_image_from_filename (const gchar *filename)
{
	/* TODO */
	return TRUE;
}

gboolean gj_gtk_docklet_set_image_from_stock (const gchar *stock_id)
{
	if (!docklet) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return FALSE;
	}

	gtk_image_set_from_stock (GTK_IMAGE (docklet->image), stock_id, GTK_ICON_SIZE_LARGE_TOOLBAR);
	return TRUE;
}

gboolean gj_gtk_docklet_set_hint (const gchar *hint)
{
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return FALSE;
	}

	if (hint == NULL) {
		g_warning ("%s: hint was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (docklet->hint);
	docklet->hint = g_strdup (hint);

	return TRUE;
}

static void gj_gtk_docklet_embedded (GtkWidget *widget, void *data) 
{
	g_message ("%s: embedded", __FUNCTION__);
}

static void gj_gtk_docklet_destroyed (GtkWidget *widget, void *data) 
{
	g_message ("%s: destroyed", __FUNCTION__);
}

static void gj_gtk_docklet_clicked (GtkWidget *button, GdkEventButton *event, void *data)
{
	GjDockletClickCallback cb = NULL;

	g_message ("%s: clicked", __FUNCTION__);

	if (docklet == NULL)
		return;
    
	if (event->button == 1) {
		if (docklet->left_cb == NULL) {
			return;
		}

		cb = docklet->left_cb;
	
	} else if (event->button == 3) {
		if (docklet->right_cb == NULL) {
			return;
		}
		
		cb = docklet->right_cb;
	}

	if (cb != NULL) {
		(cb) (event->type == GDK_2BUTTON_PRESS ? TRUE : FALSE, 
		      event->x_root, 
		      event->y_root,
		      docklet->user_data); 
	}
}

#else

# define DEFAULT_HINT "Docklet"

LRESULT CALLBACK gj_gtk_docklet_systray_cb (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

HWND gj_gtk_docklet_create_window ();

void gj_gtk_docklet_systray_add (HWND hWnd, HICON icon, const char *tooltip);
void gj_gtk_docklet_systray_modify (HICON icon, const char *tooltip);
void gj_gtk_docklet_systray_delete ();

HICON gj_gtk_docklet_get_icon_from_file (const char *filename);

gboolean gj_gtk_docklet_load (GjDockletClickCallback left_cb,
			      GjDockletClickCallback right_cb,
			      gpointer user_data)
{
	if (docklet != NULL) {
		return TRUE;
	}

	docklet = g_new0 (GjDocklet, 1);
    
	/* callbacks */
	docklet->left_cb = left_cb;
	docklet->right_cb = right_cb;

	docklet->user_data = user_data;

	/* create hidden window */
	docklet->hwnd = gj_gtk_docklet_create_window ();

	/* hide window - just in case (for XP) */
	ShowWindow (docklet->hwnd, SW_HIDE);

	/* load icon */
	gj_gtk_docklet_set_image_from_stock (GJ_STOCK_PRESENCE_AVAILABLE);
	docklet->hint = g_strdup_printf (PACKAGE_NAME); 

	g_message ("%s: adding systray icon hwnd:0x%.8x, hicon:0x%.8x, hint:'%s'", 
		   __FUNCTION__, (gint)docklet->hwnd, (gint)docklet->hicon, docklet->hint);

	/* add sys tray */
	gj_gtk_docklet_systray_add (docklet->hwnd, docklet->hicon, docklet->hint);  
    
	return TRUE;
}

gboolean gj_gtk_docklet_unload ()
{
	if (docklet == NULL) {
		return TRUE;
	}

	gj_gtk_docklet_systray_delete ();

	/* free members */
	if (docklet->hicon != NULL) {
		DestroyIcon (docklet->hicon);
		docklet->hicon = NULL;
	}

	docklet->left_cb = NULL;
	docklet->right_cb = NULL;

	docklet->user_data = NULL;

	if (docklet->hint != NULL) {
		g_free (docklet->hint);
		docklet->hint = NULL;
	}

	g_free (docklet);
	docklet = NULL;

	return TRUE;
}

gboolean gj_gtk_docklet_set_image_from_stock (const gchar *stock_id)
{
	GdkPixbuf *pb = NULL;

	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return FALSE;
	}

	if (stock_id == NULL) {
		g_warning ("%s: stock id was NULL", __FUNCTION__);
		return FALSE;
	}

#ifdef G_OS_WIN32
	if (docklet->hicon != NULL) {
		DestroyIcon (docklet->hicon);
		docklet->hicon = NULL;
	}

	if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_AVAILABLE) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "available16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_UNAVAILABLE) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "unavailable16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_CHAT) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "chat16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_DND) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "busy16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_AWAY) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "away16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_XA) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "xa16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_PRESENCE_INVISIBLE) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "unknown16.ico");
	} else if (g_strcasecmp (stock_id, GJ_STOCK_MESSAGE_CHAT) == 0) {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "message16.ico");
	} else {
		docklet->hicon = gj_gtk_docklet_get_icon_from_file ("icons" G_DIR_SEPARATOR_S "unknown16.ico");
	}

	gj_gtk_docklet_systray_modify (docklet->hicon, docklet->hint);
	return TRUE;
#endif

#if 1
	gchar *file = NULL;
	gchar *filename = NULL;
	gchar *absolute_path = NULL;

	file = g_strdup_printf ("%s.png", stock_id);

#ifdef G_OS_WIN32
	absolute_path = g_win32_get_package_installation_directory (PACKAGE_NAME, NULL);
	filename = g_build_filename (absolute_path, file, NULL);
#else
	absolute_path = NULL;
	filename = g_build_filename (DATADIR, PACKAGE, file, NULL);
#endif

	pb = gdk_pixbuf_new_from_file (filename, NULL);

	g_free (file);
	g_free (filename);
	g_free (absolute_path);

#else
	GtkImage *image = NULL;

	/* create image to have a widget to render the icon */
	image = gtk_image_new ();

	/* get pixbuf */
	pb = gtk_widget_render_icon (GTK_WIDGET (image), stock_id, GTK_ICON_SIZE_MENU, NULL);
#endif

	if (!pb) {
		g_warning ("%s: pixbuf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (docklet->hicon != NULL) {
		DestroyIcon (docklet->hicon);
		docklet->hicon = NULL;
	}

	docklet->hicon = gj_gtk_pixbuf_to_hicon (pb);
	if (!docklet->hicon) {
		g_warning ("%s: HICON was NULL", __FUNCTION__);
		return FALSE;
	}

	gj_gtk_docklet_systray_modify (docklet->hicon, docklet->hint);  

	/* clean up */
	if (pb != NULL) g_object_unref (G_OBJECT (pb));

	return TRUE;
}

gboolean gj_gtk_docklet_set_image_from_filename (const gchar *filename)
{
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return FALSE;
	}

	if (filename == NULL || g_utf8_strlen (filename,-1) < 1) {
		g_warning ("%s: filename was NULL or length was < 1", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: setting image to filename:'%s'", __FUNCTION__, filename);

	/* load icon */

	if (docklet->hicon != NULL) {
		DestroyIcon (docklet->hicon);
		docklet->hicon = NULL;
	}

	docklet->hicon = (HICON) gj_gtk_docklet_get_icon_from_file (filename);
	gj_gtk_docklet_systray_modify (docklet->hicon, docklet->hint);  

	return TRUE;
}

gboolean gj_gtk_docklet_set_hint (const gchar *hint)
{
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return FALSE;
	}

	if (hint == NULL) {
		g_warning ("%s: hint was NULL", __FUNCTION__);
		return FALSE;
	}

	if (docklet->hint != NULL) {
		g_free (docklet->hint);
		docklet->hint = NULL;
	}
    
	docklet->hint = g_strdup (hint);
	gj_gtk_docklet_systray_modify (docklet->hicon, docklet->hint);

	return TRUE;
}

LRESULT CALLBACK gj_gtk_docklet_systray_cb (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) 
{
	switch (msg) {
	case WM_CREATE:
		g_message ("%s: WM_CREATE", __FUNCTION__);
		break;
      
	case WM_DESTROY:
		g_message ("%s: WM_DESTROY", __FUNCTION__);
		break;
      
	case WM_USER: {
		GjDockletClickCallback cb = NULL;
		
		if (docklet != NULL) {
			POINT mouse;   
			
			GetCursorPos (&mouse);
			ScreenToClient (docklet->hwnd, &mouse);
			
			if (lparam == WM_LBUTTONUP) {
				/* Double Click */
				g_message ("%s: left button click", __FUNCTION__);
				
				cb = docklet->left_cb;
				
				if (cb != NULL) {
						(cb) (FALSE, mouse.x, mouse.y, docklet->user_data); 
				}
			}
			
			if (lparam == WM_LBUTTONDBLCLK) {
				/* Double Click */
				g_message ("%s: double left button click", __FUNCTION__);
				
				cb = docklet->left_cb;
				
				if (cb != NULL) {
					(cb) (TRUE, mouse.x, mouse.y, docklet->user_data);  
				}
			}
			
			if (lparam == WM_RBUTTONUP) {
				/* Right Click */
				g_message ("%s: right button click", __FUNCTION__);
				
				cb = docklet->right_cb;
				
				if (cb != NULL) {
					(cb) (FALSE, mouse.x, mouse.y, docklet->user_data); 
				}
			}
		}
		
		break;
	}
		
	default:
		break;
	}
  
	return DefWindowProc (hwnd, msg, wparam, lparam);
}

HWND gj_gtk_docklet_create_window () 
{
	WNDCLASSEX wcex;
	TCHAR name[32];
 
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return NULL;
	}
 
	if (docklet->hint != NULL) {
		strcpy (name, docklet->hint);
	} else {
		strcpy (name, DEFAULT_HINT); 
	}
  
	wcex.cbSize = sizeof (WNDCLASSEX);
  
	wcex.style = 0;
	wcex.lpfnWndProc = (WNDPROC)gj_gtk_docklet_systray_cb;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = docklet->hinstance;
	wcex.hIcon = NULL;
	wcex.hCursor = NULL;
	wcex.hbrBackground = NULL;
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = name;
	wcex.hIconSm = NULL;
  
	RegisterClassEx (&wcex);

	/* create the window */
	return (CreateWindow (name, "", 0, -500, -500, 1, 1, 
			      GetDesktopWindow (), NULL, docklet->hinstance, 0));
}

void gj_gtk_docklet_systray_add (HWND hWnd, HICON icon, const char *tooltip) 
{
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return;
	}
  
	/* clean memory */
	ZeroMemory (&docklet->nid, sizeof (docklet->nid));

	/* set properties */
	docklet->nid.cbSize = sizeof (NOTIFYICONDATA);
	docklet->nid.hWnd = hWnd;
	docklet->nid.uID = 0;
	docklet->nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	docklet->nid.uCallbackMessage = WM_USER;
	docklet->nid.hIcon = icon;

#if 0 
	/* version 5.0 or newer - Windows 2000 and newer */
	strcpy (docklet->nid.szInfo, "http://www.bt.com");
	docklet->nid.uTimeout = 10000;
	strcpy (docklet->nid.szInfoTitle, "BT Office Phone");
	docklet->nid.dwInfoFlags = NIF_INFO;
#endif

	/* set hint */
	if (tooltip != NULL) {
		strcpy (docklet->nid.szTip, tooltip);
	}

	/* add */
	Shell_NotifyIcon (NIM_ADD, &docklet->nid);
}

void gj_gtk_docklet_systray_modify (HICON icon, const char *tooltip) 
{
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return;
	}

	g_message ("%s: systray_modify, tooltip is:'%s'", __FUNCTION__, tooltip);
	g_message ("%s: systray_modify, HICON:%#x", __FUNCTION__, (int)icon);

	docklet->nid.hIcon = icon;
    
	if (tooltip != NULL) {
		strcpy (docklet->nid.szTip, tooltip); 
	}

	/* update */
	Shell_NotifyIcon (NIM_MODIFY, &docklet->nid);
}

void gj_gtk_docklet_systray_delete () 
{
	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return;
	}

	/* remove */
	Shell_NotifyIcon (NIM_DELETE, &docklet->nid);
}

HICON gj_gtk_docklet_get_icon_from_file (const char *filename)
{
	HICON i = 0;

	if (docklet == NULL) {
		g_warning ("%s: docklet was NULL", __FUNCTION__);
		return NULL;
	}

	i = LoadImage (docklet->hinstance, /* small class icon */
		       filename,           /* load from filename */
		       IMAGE_ICON,
		       GetSystemMetrics (SM_CXSMICON),
		       GetSystemMetrics (SM_CYSMICON),
		       LR_LOADFROMFILE);

	g_message ("%s: opening filename:'%s', HICON:0x%.8x", __FUNCTION__, filename, (gint)i);
  
	return i;
}

#endif /* G_OS_WIN32 */

#ifdef __cplusplus
}
#endif
