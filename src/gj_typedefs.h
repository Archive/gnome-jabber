/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_typedefs_h
#define __gj_typedefs_h

#include <time.h> 

#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glade/glade.h>

#include <gnet.h>

#include <libxml/tree.h>
#include <libxml/parser.h>


/*
 * JID
 */
typedef struct t_jid *GjJID;

typedef enum {
	GjJIDTypeNull = 1,

	GjJIDTypeNone,
	GjJIDTypeContact,
	GjJIDTypeAgent,

	GjJIDTypeEnd 
} GjJIDType;

/*
 * presence
 */
typedef struct t_presence *GjPresence;

typedef void (*GjPresenceFromCallback) (GjPresence pres);
typedef void (*GjPresenceToCallback) (gint requestID);

typedef struct t_presenceTo *GjPresenceTo; 

typedef enum {
	GjPresenceTypeNull = 1,

	GjPresenceTypeAvailable,       /* online */  
	GjPresenceTypeUnAvailable,     /* offline */
	GjPresenceTypeProbe,           /* ask another user's status */
	GjPresenceTypeSubscribe,       /* request to subscribe to presence of user */
	GjPresenceTypeUnSubscribe,     /* request to unsubscribe to presence of user */
	GjPresenceTypeSubscribed,      /* result to subscribe */ 
	GjPresenceTypeUnSubscribed,    /* result to unsubscribe */
	GjPresenceTypeError,           /* error */
	GjPresenceTypeNone,            /* none - we are not given presence */
  
	GjPresenceTypeEnd              /* end of types */
} GjPresenceType;

typedef enum {
	GjPresenceShowNull = 1,

	GjPresenceShowAway,        /* temporarily away */
	GjPresenceShowChat,        /* similar to normal except in a conversation */
	GjPresenceShowDND,         /* do not disturb */ 
	GjPresenceShowNormal,      /* default */
	GjPresenceShowXA,          /* eXtended Away, almost unavailable */
	GjPresenceShowInvisible,   /* Invisible, - this is like being disconnected except you arent */
  
	GjPresenceShowEnd          /* end of types */
} GjPresenceShow;


/*
 * message
 */
typedef struct t_message *GjMessage;

typedef void (*GjMessageFromCallback) (GjMessage m);
typedef void (*GjMessageToCallback) (gint requestID);

typedef enum {
	GjMessageTypeNull = 1,

	GjMessageTypeNormal,
	GjMessageTypeChat,
	GjMessageTypeGroupChat,
	GjMessageTypeHeadline,
	GjMessageTypeError,
	GjMessageTypeXEvent,
    
	GjMessageTypeEnd
} GjMessageType;
 

#define GjMessageXEventTypeOffline      1 << 1
#define GjMessageXEventTypeDelivered    1 << 2
#define GjMessageXEventTypeDisplayed    1 << 3 
#define GjMessageXEventTypeComposing    1 << 4
#define GjMessageXEventTypeCancelled    1 << 5


/*
 * history
 */
typedef struct t_history *GjHistory;


/* 
 * roster
 */
typedef struct t_roster *GjRoster;

typedef void (*GjRosterCallback) (const GList *contacts, 
				  const GList *agents, 
				  gpointer user_data);

typedef void (*GjRosterSetCallback) (const GList *updated_roster_items, 
				     gpointer user_data);


/*
 * roster item
 */
typedef struct t_roster_item *GjRosterItem; 

typedef enum {
	GjRosterItemSubscriptionNull = 1,

	GjRosterItemSubscriptionNone,
	GjRosterItemSubscriptionTo,
	GjRosterItemSubscriptionFrom,
	GjRosterItemSubscriptionBoth,
  
	GjRosterItemSubscriptionEnd
} GjRosterItemSubscription;


/* 
 * agents 
 */
typedef struct t_agent *GjAgent;

typedef void (*GjAgentCallback) (GList *agents, 
				 gint error_code, 
				 const gchar *error_message, 
				 gpointer user_data);


/*
 * Registration (new jabber accounts)
 */
typedef struct t_registration *GjRegistration;

typedef void (*GjRegistrationCallback) (GjRegistration rgr, 
					gint error_code, 
					const gchar *error_reason, 
					gpointer user_data);


/*
 * Search (JUD)
 */
typedef struct t_search *search;

typedef void (*searchCallback) (search rgr, 
				gint error_code, 
				gchar *error_reason, 
				gpointer user_data);

typedef void (*searchRequestCallback) (search rgr, 
				       gint error_code, 
				       gchar *error_reason, 
				       gpointer user_data);


/*
 * gatherdata 
 */
typedef struct t_GjData *GjData;
typedef struct t_GjDataField *GjDataField;
typedef struct t_GjDataFieldOption *GjDataFieldOption;

typedef void (*GjDataCallback) (GjData gd, 
				gint error_code, 
				gchar *error_reason, 
				gpointer user_data);

typedef void (*GjDataRequestCallback) (GList *list, 
				       gint error_code, 
				       gchar *error_reason, 
				       gpointer user_data);


typedef enum {
	GjDataFormTypeNull = 1,

	GjDataFormTypeForm,            /* This packet contains a form to fill out. Display it to the user */    
	GjDataFormTypeSubmit,          /* The form is filled out, and this is the data that is being returned from the form. */
	GjDataFormTypeCancel,          /* The form was cancelled. Tell the asker that piece of information. */
	GjDataFormTypeResult,          /* Data results being returned from a search, or some other query. */

	GjDataFormTypeEnd

} GjDataFormType;

typedef enum {
	GjDataFieldTypeNull = 1,
 
	GjDataFieldTypeTextSingle,     /* single line or word of text */
	GjDataFieldTypeTextPrivate,    /* instead of showing the user what they typed, you show ***** to protect it */
	GjDataFieldTypeTextMulti,      /* multiple lines of text entry */
	GjDataFieldTypeListSingle,     /* given a list of choices, pick one */
	GjDataFieldTypeListMulti,      /* given a list of choices, pick one or more */
	GjDataFieldTypeBoolean,        /* 0 or 1, true or false, yes or no. Default value is 0 */
	GjDataFieldTypeFixed,          /* fixed for putting in text to show sections, 
					  or just advertise your web site in the middle of the form */
	GjDataFieldTypeHidden,         /* is not given to the user at all, but returned with the questionnaire
					  (e.g., a unique number at the top of a sheet of paper to identify the form) */
	GjDataFieldTypeJIDSingle,      /* Jabber ID - choosing a JID from your roster, and entering one based on the rules for a JID. */
	GjDataFieldTypeJIDMulti,       /* multiple entries for JIDs */

	GjDataFieldTypeEnd
  
} GjDataFieldType;



/*
 * IQs
 */
typedef struct t_infoquery *GjInfoquery;

typedef void (*GjInfoqueryCallback) (GjInfoquery iq, GjInfoquery related_iq, gpointer user_data);

typedef enum {
	GjInfoqueryTypeNull = 1,

	GjInfoqueryTypeGet,
	GjInfoqueryTypeSet,
	GjInfoqueryTypeResult,
	GjInfoqueryTypeError,
  
	GjInfoqueryTypeEnd
} GjInfoqueryType;


/*
 * events
 */
typedef enum {
	GjEventTypeNull = 1,

	GjEventTypePresence,
	GjEventTypeMessage,
	GjEventTypeFileTransfer,

	GjEventTypeEnd
} GjEventType;


typedef struct t_event *GjEvent;

typedef void (*GjEventCallback) (gint event_id, gpointer user_data);


/* 
 * parser
 */
typedef struct t_parser *GjParser;

typedef void (*GjParserStreamCallback) (GjParser p, const gchar *from, const gchar *id);
typedef void (*GjParserMessageCallback) (GjParser p, xmlNodePtr n);
typedef void (*GjParserPresenceCallback) (GjParser p, xmlNodePtr n);
typedef void (*GjParserIQCallback) (GjParser p, xmlNodePtr n);
typedef void (*GjParserErrorCallback) (GjParser p, xmlNodePtr n);
typedef void (*GjParserOtherCallback) (GjParser p, xmlNodePtr n);

/*
 * stream
 */
typedef struct t_stream *GjStream;

typedef void (*GjStreamCallback) (GjStream stm, gint error_code, const gchar *error_message, gpointer user_data);


/*
 * connection
 */
typedef struct t_connection *GjConnection;

typedef void (*GjConnectionCallbackIn) (GjConnection c, gchar *text);
typedef void (*GjConnectionCallbackHup) (GjConnection c);
typedef void (*GjConnectionCallbackConnect) (GjConnection c);
typedef void (*GjConnectionCallbackDisconnect) (GjConnection c);
typedef void (*GjConnectionCallbackError) (GjConnection c, GTcpSocketConnectAsyncStatus status);


/*
 * AutoAway
 */
typedef void (*GjAutoAwayCallback) (guint msec, gpointer user_data);


/*
 * Browse (jabber:iq:browse)
 */
typedef void (*GjBrowseCallback) (const gchar *server, 
				  const GList *list, 
				  gint error_code, 
				  const gchar *error_message, 
				  gpointer userdata);

typedef struct t_GjBrowseItem *GjBrowseItem;

typedef enum {
	GjBrowseCategoryNull = 1,

	GjBrowseCategoryApplication,
	GjBrowseCategoryConference,
	GjBrowseCategoryHeadline,
	GjBrowseCategoryKeyword,
	GjBrowseCategoryRender,
	GjBrowseCategoryService,
	GjBrowseCategoryUser,
	GjBrowseCategoryValidate,

	GjBrowseCategoryEnd 
} GjBrowseCategory;

typedef enum {
	GjBrowseTypeNull = 1,

	/* application */
	GjBrowseTypeApplicationBot,
	GjBrowseTypeApplicationCalendar,
	GjBrowseTypeApplicationEditor,
	GjBrowseTypeApplicationFileserver,
	GjBrowseTypeApplicationGame,
	GjBrowseTypeApplicationWhiteboard,

	/* conference */
	GjBrowseTypeConferenceIRC,
	GjBrowseTypeConferenceList,
	GjBrowseTypeConferencePrivate,
	GjBrowseTypeConferencePublic,
	GjBrowseTypeConferenceTopic,
	GjBrowseTypeConferenceURL,

	/* headline */
	GjBrowseTypeHeadlineLogger,
	GjBrowseTypeHeadlineNotice,
	GjBrowseTypeHeadlineRSS,
	GjBrowseTypeHeadlineStock,

	/* keyword */
	GjBrowseTypeKeywordDictionary,
	GjBrowseTypeKeywordDNS,
	GjBrowseTypeKeywordSoftware,
	GjBrowseTypeKeywordThesaurus,
	GjBrowseTypeKeywordWeb,
	GjBrowseTypeKeywordWhoIs,

	/* render */
	GjBrowseTypeRenderEn2Fr,
	GjBrowseTypeRenderA2B,
	GjBrowseTypeRenderTTS,

	/* service */
	GjBrowseTypeServiceAIM,
	GjBrowseTypeServiceICQ,
	GjBrowseTypeServiceIRC,
	GjBrowseTypeServiceJabber,
	GjBrowseTypeServiceJUD,
	GjBrowseTypeServiceMSN,
	GjBrowseTypeServicePager,
	GjBrowseTypeServiceServerList,
	GjBrowseTypeServiceSMS,
	GjBrowseTypeServiceSMTP,
	GjBrowseTypeServiceYahoo,

	/* user */
	GjBrowseTypeUserClient,
	GjBrowseTypeUserForward,
	GjBrowseTypeUserInbox,
	GjBrowseTypeUserPortable,
	GjBrowseTypeUserVoice,

	/* validate */
	GjBrowseTypeValidateGrammar,
	GjBrowseTypeValidateSpell,
	GjBrowseTypeValidateXML,

	GjBrowseTypeEnd
} GjBrowseType;



/*
 * File Transfer (http://jabber.org/protocol/ibb)
 */
typedef struct t_GjFileTransfer *GjFileTransfer;

typedef void (*GjFileTransferErrorCallback) (GjRosterItem ri, 
					     gint error_code, 
					     gchar *error_reason, 
					     gpointer user_data);


#endif

