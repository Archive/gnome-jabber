/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_gtk_about_h_
#define __gj_gtk_about_h_


gboolean gj_gtk_about_load (const gchar *window_title,
			   const gchar *title, 
			   const gchar *semititle, 
			   const gchar *version, 
			   const gchar *build, 
			   const gchar *copyright, 
			   const gchar *license,
			   const gchar *sn,
			   const gchar **authors,
			   const gchar **documenters,
			   const gchar **translators);

#endif

#ifdef __cplusplus
}
#endif

