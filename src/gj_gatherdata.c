/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include "gj_gatherdata.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"


static void gj_gatherdata_init ();

/* gatherdata item */
static void gj_gatherdata_destroy (GjData gd);

/* gatherdata list */
static gboolean gj_gatherdata_list_add (GjData gd);
static gboolean gj_gatherdata_list_del (GjData gd);

/* gatherdata field item */
static void gj_gatherdata_field_destroy (GjDataField gdf);

/* gatherdata field list */
static gboolean gj_gatherdata_field_list_add (GjData gd, GjDataField gdf);
static gboolean gj_gatherdata_field_list_del (GjData gd, GjDataField gdf);

static gboolean gj_gatherdata_field_list_free (GjData gd);

/* gatherdata field option item */
static void gj_gatherdata_field_option_destroy (GjDataFieldOption gdfo);

/* gatherdata field option list */
static gboolean gj_gatherdata_field_option_list_add (GjDataField gdf, GjDataFieldOption gdfo);
static gboolean gj_gatherdata_field_option_list_del (GjDataField gdf, GjDataFieldOption gdfo);

static gboolean gj_gatherdata_field_option_list_free (GjDataField gdf);

/* parsing functions */
static void gj_gatherdata_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

/* parsing functions */
/* GList *gatherdataListParse (xmlNodePtr parent); */
static GList *gj_gatherdata_parse (xmlNodePtr parent);

/* call backs */
/* gboolean gatherdataSetResponseProc (gatherdataResponseProc proc); */
/* gatherdataResponseProc gatherdataGetResponseProc (); */


struct t_GjDataFieldOption {
	gchar *label;
	gchar *value;
};

struct t_GjDataField {
	GjDataFieldType type;

	gchar *label;
	gchar *var;
	gchar *value;

	gboolean required;

	GList *options;
};

struct t_GjData {
	gchar *id;
	gchar *to;
	gchar *from;

	GjDataFormType type; 
 
	gchar *instructions;
	GList *fields;

	GjDataCallback cb; 

	gpointer user_data;

	gchar *related_id;
};


static GList *gatherdata_list = NULL;
static GjDataRequestCallback gatherdata_cb = NULL;


static void gj_gatherdata_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;
}

static gboolean gj_gatherdata_list_add (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	gatherdata_list = g_list_append (gatherdata_list, gd);
	return TRUE;
}

static gboolean gj_gatherdata_list_del (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	gatherdata_list = g_list_remove (gatherdata_list, gd);
	return TRUE;
}

GList *gj_gatherdata_get_list ()
{
	return gatherdata_list;
}

/* 
 * item functions
 */
GjData gj_gatherdata_new ()
{
	GjData gd = NULL;

	gj_gatherdata_init ();

	if ((gd = g_new (struct t_GjData,1))) {
		gd->id = NULL;
		gd->to = NULL;
		gd->from = NULL;
      
		gd->instructions = NULL;
		gd->fields = NULL;
      
		gd->cb = NULL;

		gd->related_id = NULL;

		gj_gatherdata_list_add (gd);
	}
  
	return gd;  
}

void gj_gatherdata_free (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return;
	}

	/* free mem */
	gj_gatherdata_list_del (gd);  

	/* destroy members */
	gj_gatherdata_destroy (gd);

	/* free structure */
	g_free (gd);
}

void gj_gatherdata_destroy (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return;
	}
  
	g_free (gd->id);
	g_free (gd->to);
	g_free (gd->from);
	g_free (gd->instructions);

	gj_gatherdata_field_list_free (gd);
	gd->fields = NULL;

	gd->cb = NULL;

	g_free (gd->related_id);
}

/*
 * find 
 */
GjData gj_gatherdata_find_by_id (gchar *id)
{
	gint index = 0;

	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return NULL;
	}

	for (index=0; index<g_list_length (gj_gatherdata_get_list ()); index++) {
		GjData gd = (GjData) g_list_nth_data (gj_gatherdata_get_list (), index);

		if (g_strcasecmp (gd->id, id) == 0) {
			return gd; 
		}
	}

	return NULL;
}

GjData gj_gatherdata_find_by_to (gchar *to)
{
	gint index = 0;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return NULL;
	}

	for (index=0; index<g_list_length (gj_gatherdata_get_list ()); index++) {
		GjData gd = (GjData) g_list_nth_data (gj_gatherdata_get_list (), index);

		if (g_strcasecmp (gd->to, to) == 0) {
			return gd; 
		}
	}

	return NULL;
}

GjData gj_gatherdata_find_by_from (gchar *from)
{
	gint index = 0;

	if (from == NULL) {
		g_warning ("%s: from was NULL", __FUNCTION__);
		return NULL;
	}

	for (index=0; index<g_list_length (gj_gatherdata_get_list ()); index++) {
		GjData gd = (GjData) g_list_nth_data (gj_gatherdata_get_list (), index);

		if (g_strcasecmp (gd->from, from) == 0) {
			return gd;
		}
	}

	return NULL;
}

GjData gj_gatherdata_find_by_related_id (gchar *id)
{
	gint index = 0;

	if (id == NULL) {
		g_warning ("%s: related id was NULL", __FUNCTION__);
		return NULL;
	}

	for (index=0; index<g_list_length (gj_gatherdata_get_list ()); index++) {
		GjData gd = (GjData) g_list_nth_data (gj_gatherdata_get_list (), index);

		if (g_strcasecmp (gd->related_id, id) == 0) {
			return gd;
		}
	}

	return NULL;
}

/*
 * member functions  
 */

/* sets */
gboolean gj_gatherdata_set_id (GjData gd, gchar *id)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gd->id);
  	gd->id = g_strdup (id);

	return TRUE;
}

gboolean gj_gatherdata_set_to (GjData gd, gchar *to)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gd->to);
  	gd->to = g_strdup (to);

	return TRUE;
}

gboolean gj_gatherdata_set_from (GjData gd, gchar *from)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (from == NULL) {
		g_warning ("%s: from was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gd->from);
  	gd->from = g_strdup (from);
	
	return TRUE;
}

gboolean gj_gatherdata_set_type (GjData gd, GjDataFormType type)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (type < GjDataFormTypeForm || type > GjDataFormTypeEnd) {
		g_warning ("%s: type:%d was out of valid range", __FUNCTION__, type);
		return FALSE;
	}

	gd->type = type;
	return TRUE;
}

gboolean gj_gatherdata_set_instructions (GjData gd, gchar *instructions) 
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (instructions == NULL) {
		g_warning ("%s: instructions was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gd->instructions);
	gd->instructions = g_strdup (instructions);

	return TRUE;
}

gboolean gj_gatherdata_set_fields (GjData gd, GList *fields)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (fields == NULL) {
		g_warning ("%s: fields list was NULL", __FUNCTION__);
		return FALSE;
	}

	gd->fields = fields;
	return TRUE;
}

gboolean gj_gatherdata_set_callback (GjData gd, GjDataCallback cb)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	gd->cb = cb;
	return TRUE;
}

gboolean gj_gatherdata_set_related_id (GjData gd, gchar *id)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id == NULL) {
		g_warning ("%s: related id was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gd->related_id);
	gd->related_id = g_strdup (id);

	return TRUE;
}

/* gets */
gchar *gj_gatherdata_get_id (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->id;
}

gchar *gj_gatherdata_get_to (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->to;
}

gchar *gj_gatherdata_get_from (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->from;
}

GjDataFormType gj_gatherdata_get_type (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return GjDataFormTypeEnd;
	}

	return gd->type;
}

gchar *gj_gatherdata_get_instructions (GjData gd) 
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->instructions;
}

GList *gj_gatherdata_get_fields (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->fields;
}

GjDataCallback gj_gatherdata_get_callback (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->cb;
}

gchar *gj_gatherdata_get_related_id (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->related_id;
}


/* debugging */
void gj_gatherdata_print_all ()
{
	GjData gd = NULL;
	gint index = 0;

	g_message ("++++++++++++++++++++++ Gatherdata - Print All, Begin. +++++++++++++++++++++");

	for (index=0;index<g_list_length (gj_gatherdata_get_list ());index++) {
		gd = (GjData) g_list_nth_data (gj_gatherdata_get_list (),index);
		gj_gatherdata_print (gd);
	}

	g_message ("++++++++++++++++++++++ Gatherdata - Print All, End.   +++++++++++++++++++++");
  
	return;  
}

void gj_gatherdata_print (GjData gd)
{
	if (gd == NULL)
		return;

	g_message ("+++++++++++++++++++++++++++ Gatherdata +++++++++++++++++++++++++");
	g_message ("++++");
	g_message ("++++ id:'%s'",gd->id);
	g_message ("++++ to:'%s'",gd->to);
	g_message ("++++ from:'%s'",gd->from);
	g_message ("++++");
	g_message ("++++ instructions:'%s'",gd->instructions);
	g_message ("++++");
	g_message ("++++ related id:'%s'",gd->related_id);
	g_message ("++++");
	g_message ("+++++++++++++++++++++++++++ Gatherdata +++++++++++++++++++++++++");
}

/*
 *
 * GATHERDATA FIELD functions
 *
 */
static gboolean gj_gatherdata_field_list_add (GjData gd, GjDataField gdf)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	gd->fields = g_list_append (gd->fields, gd);

	return TRUE;
}

static gboolean gj_gatherdata_field_list_del (GjData gd, GjDataField gdf)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	gd->fields = g_list_remove (gd->fields, gdf);
	return TRUE;
}

static gboolean gj_gatherdata_field_list_free (GjData gd)
{
	gint index = 0;

	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (gj_gatherdata_get_fields (gd) == NULL) {
		return TRUE;
	}

	for (index=0;index<g_list_length (gj_gatherdata_get_fields (gd));index++) {
		GjDataField gdf = g_list_nth_data (gj_gatherdata_get_fields (gd), index);
      
		gj_gatherdata_field_free (gd, gdf);
	}

	return TRUE;
}

GList *gj_gatherdata_field_list_get (GjData gd)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	return gd->fields;
}

/* 
 * item functions
 */
GjDataField gj_gatherdata_field_new (GjData gd, GjDataFieldType type)
{
	GjDataField gdf = NULL;

	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return NULL;
	}

	if (type < 1 || type > GjDataFieldTypeEnd) {
		g_warning ("%s: type:%d was out of bounds", __FUNCTION__, type);
		return NULL;
	}

	if ((gdf = g_new (struct t_GjDataField,1))) {
		gdf->type = type;

		gdf->label = NULL;
		gdf->var = NULL;
		gdf->value = NULL;
      
		gdf->required = FALSE;

		gdf->options = NULL;

		gj_gatherdata_field_list_add (gd, gdf);
	}
  
	return gdf;  
}

void gj_gatherdata_field_free (GjData gd, GjDataField gdf)
{
	if (gd == NULL) {
		g_warning ("%s: gd was NULL", __FUNCTION__);
		return;
	}  

	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return;
	}

	/* free mem */
	gj_gatherdata_field_list_del (gd, gdf);  

	/* destroy members */
	gj_gatherdata_field_destroy (gdf);

	/* free structure */
	g_free (gdf);
}

static void gj_gatherdata_field_destroy (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return;
	}

	gdf->type = GjDataFieldTypeEnd;
  
	g_free (gdf->label);
	g_free (gdf->var);
	g_free (gdf->value);

	gdf->required = FALSE;

	gj_gatherdata_field_option_list_free (gdf);
	gdf->options = NULL;  
}

/*
 * member functions  
 */

/* sets */
gboolean gj_gatherdata_field_set_type (GjDataField gdf, GjDataFieldType type)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (type < 1 || type > GjDataFieldTypeEnd) {
		g_warning ("%s: type:%d was out of bounds", __FUNCTION__, type);
		return FALSE;
	}

	gdf->type = type;
	return TRUE;
}

gboolean gj_gatherdata_field_set_label (GjDataField gdf, gchar *label)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (label == NULL) {
		g_warning ("%s: label was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gdf->label);
	gdf->label = g_strdup (label);

	return TRUE;
}

gboolean gj_gatherdata_field_set_var (GjDataField gdf, gchar *var)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (var == NULL) {
		g_warning ("%s: var was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gdf->var);
	gdf->var = g_strdup (var);

	return TRUE;
}

gboolean gj_gatherdata_field_set_value (GjDataField gdf, gchar *value)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (value == NULL) {
		g_warning ("%s: value was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gdf->value);
	gdf->value = g_strdup (value);

	return TRUE;
}

gboolean gj_gatherdata_field_set_required (GjDataField gdf, gboolean required)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	gdf->required = required;
	return TRUE;
}

gboolean gj_gatherdata_field_set_options (GjDataField gdf, GList *options)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	/* warning ... what happens to previous list? */
	gdf->options = options;
	return TRUE;
}

/* gets */
GjDataFieldType gj_gatherdata_field_get_type (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return GjDataFieldTypeEnd;
	}

	return gdf->type;
}


gchar *gj_gatherdata_field_get_label (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return NULL;
	}

	return gdf->label;
}


gchar *gj_gatherdata_field_get_var (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return NULL;
	}

	return gdf->var;
}


gchar *gj_gatherdata_field_get_value (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return NULL;
	}

	return gdf->value;
}


gboolean gj_gatherdata_field_get_required (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	return gdf->required;
}


GList *gj_gatherdata_field_get_options (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return NULL;
	}

	return gdf->options;
}

/*
 *
 * GATHERDATA FIELD OPTION functions
 *
 */
static gboolean gj_gatherdata_field_option_list_add (GjDataField gdf, GjDataFieldOption gdfo)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return FALSE;
	}

	gdf->options = g_list_append (gdf->options, gdfo);
	return TRUE;
}

static gboolean gj_gatherdata_field_option_list_del (GjDataField gdf, GjDataFieldOption gdfo)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return FALSE;
	}

	gdf->options = g_list_remove (gdf->options, gdfo);
	return TRUE;
}

static gboolean gj_gatherdata_field_option_list_free (GjDataField gdf)
{
	gint index = 0;

	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (gj_gatherdata_field_get_options (gdf) == NULL) {
		return TRUE;
	}

	for (index=0;index<g_list_length (gj_gatherdata_field_get_options (gdf));index++) {
		GjDataFieldOption gdfo = g_list_nth_data (gj_gatherdata_field_get_options (gdf), index);
      
		gj_gatherdata_field_option_free (gdf, gdfo);
	}

	return TRUE;
}

GList *gj_gatherdata_field_option_list_get (GjDataField gdf)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return NULL;
	}

	return gdf->options;
}

/* 
 * item functions
 */
GjDataFieldOption gj_gatherdata_field_option_new (GjDataField gdf, gchar *label)
{
	GjDataFieldOption gdfo = NULL;

	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return NULL;
	}

	if (label == NULL) {
		g_warning ("%s: label was NULL", __FUNCTION__);
		return NULL;
	}

	if ((gdfo = g_new (struct t_GjDataFieldOption,1))) {
		gdf->label = g_strdup (label);
		gdf->value = NULL;

		gj_gatherdata_field_option_list_add (gdf, gdfo);
	}
  
	return gdfo;  
}

void gj_gatherdata_field_option_free (GjDataField gdf, GjDataFieldOption gdfo)
{
	if (gdf == NULL) {
		g_warning ("%s: gdf was NULL", __FUNCTION__);
		return;
	}  

	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return;
	}

	/* free mem */
	gj_gatherdata_field_option_list_del (gdf, gdfo);  

	/* destroy members */
	gj_gatherdata_field_option_destroy (gdfo);

	/* free structure */
	g_free (gdfo);
}

static void gj_gatherdata_field_option_destroy (GjDataFieldOption gdfo)
{
	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return;
	}

	g_free (gdfo->label);
	g_free (gdfo->value);
}

/*
 * member functions  
 */

/* sets */
gboolean gj_gatherdata_field_option_set_label (GjDataFieldOption gdfo, gchar *label)
{
	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return FALSE;
	}

	if (label == NULL) {
		g_warning ("%s: label was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gdfo->label);
	gdfo->label = g_strdup (label);

	return TRUE;
}

gboolean gj_gatherdata_field_option_set_value (GjDataFieldOption gdfo, gchar *value)
{
	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return FALSE;
	}

	if (value == NULL) {
		g_warning ("%s: value was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (gdfo->value);
	gdfo->value = g_strdup (value);

	return TRUE;
}

/* gets */
gchar *gj_gatherdata_field_option_get_label (GjDataFieldOption gdfo)
{
	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return NULL;
	}

	return gdfo->label;
}

gchar *gj_gatherdata_field_option_get_value (GjDataFieldOption gdfo)
{
	if (gdfo == NULL) {
		g_warning ("%s: gdfo was NULL", __FUNCTION__);
		return NULL;
	}

	return gdfo->value;
}





/*
 *
 * SERVICES
 *
 */
gboolean gj_gatherdata_request (gchar *server, GjDataRequestCallback cb)
{
	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	gatherdata_cb = cb;

#if 0
	/* do request */
	gj_iq_request_register (server, gj_gatherdata_cb, NULL);
#endif

	return TRUE;
}

static void gj_gatherdata_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	GList *list = NULL;
  
	gint error_code = 0;
	gchar *error_reason = NULL;

	if (!iq)
		return;

	if (gatherdata_cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		if ((errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code")) != NULL) {
			error_code = atoi ((gchar*)errorCode);
		}

		if ((errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error")) != NULL) {
			error_reason = g_strdup ((gchar*)errorMessage);
		}
	}

	if (gj_gatherdata_get_list () != NULL) {
		/* free list */

	}
  
	/* parse list */
	if ((list = gj_gatherdata_parse (node)) == NULL) {
		g_warning ("%s: did not complete the parse from xml -> list", __FUNCTION__);
		return;
	}

	/* set */
	gatherdata_list = list;
  
	/* return list */
	(gatherdata_cb) (gj_gatherdata_get_list (), error_code, error_reason, NULL);

	/* clean up */
	g_free (error_reason);
}

GList *gj_gatherdata_parse (xmlNodePtr parent)
{
	if (parent == NULL) {
		g_warning ("%s: failed to get gatherdata because query (xml) was NULL", __FUNCTION__);
		return NULL;
	}

	return NULL;
}

#ifdef __cplusplus
}
#endif
