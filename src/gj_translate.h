/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_translate_h
#define __gj_translate_h

#include <glib.h>

#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_event.h"

const gchar *gj_translate_jid_type (GjJIDType type);

const gchar *gj_translate_roster_item_subscription (GjRosterItemSubscription subscription);
const gchar *gj_translate_roster_item_subscription_as_description (GjRosterItemSubscription subscription);

const gchar *gj_translate_event_type (GjEventType type);

const gchar *gj_translate_presence_type (GjPresenceType type);
const gchar *gj_translate_presence_show (GjPresenceShow show);

GjPresenceShow gj_translate_presence_show_str (const gchar *show);

const gchar *gj_translate_message_type (GjMessageType type);

gchar *gj_translate_message_x_event_type (gint type);

const gchar *gj_translate_browse_category (GjBrowseCategory category);
const gchar *gj_translate_browse_type (GjBrowseType type);

#endif
