/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_message.h"
#include "gj_jid.h"
#include "gj_presence.h"
#include "gj_connection.h"

#include "gj_gtk_group_message.h"
#include "gj_gtk_support.h"


/* columns */
enum {
	COL_GROUP_MESSAGE_NAME,
	COL_GROUP_MESSAGE_RI_ID,
	COL_GROUP_MESSAGE_ENABLED,
	COL_GROUP_MESSAGE_COUNT
};


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry;
	GtkWidget *textview;
	GtkWidget *treeview;

	GtkWidget *button_send;
	GtkWidget *button_close;

	/* members */
	GjConnection c;
	GjJID connected_jid;
	GjRoster roster;
	gchar *group;

} GjGroupMessageWindow;


/*
 * model functions
 */
static gboolean gj_gtk_gm_model_setup (GjGroupMessageWindow *window);
static gboolean gj_gtk_gm_model_populate_columns (GjGroupMessageWindow *window);
static gboolean gj_gtk_gm_model_populate_from_roster (GjGroupMessageWindow *window);

static gboolean gj_gtk_gm_model_add_contact (GjGroupMessageWindow *window, GjRosterItem ri);

/*
 *  window functions 
 */
static gboolean gj_gtk_gm_setup (GjGroupMessageWindow *window);
static gboolean gj_gtk_gm_send (GjGroupMessageWindow *window);


/*
 * gui functions 
 */
static void on_treeview_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjGroupMessageWindow *window);

static void on_textbuffer_changed (GtkTextBuffer *textbuffer, GjGroupMessageWindow *window);

static void on_button_send_clicked (GtkButton *button, GjGroupMessageWindow *window);
static void on_button_close_clicked (GtkButton *button, GjGroupMessageWindow *window);

static void on_destroy (GtkWidget *widget, GjGroupMessageWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjGroupMessageWindow *window);


static GjGroupMessageWindow *current_window = NULL;


/*
 * window functions
 */
gboolean gj_gtk_gm_load (GjConnection c,
			 GjJID connected_jid, 
			 GjRoster r, 
			 const gchar *group)
{
	GladeXML *xml = NULL;
	GjGroupMessageWindow *window = NULL;

	if (current_window != NULL) {
		gtk_window_present (GTK_WINDOW (current_window->window));
		return TRUE;
	}

	if (!c) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (!connected_jid) {
		g_warning ("%s: connected_server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	if (group == NULL || strlen (group) < 1) {
		g_warning ("%s: group was NULL", __FUNCTION__);
		return FALSE;
	}

	current_window = window = g_new0 (GjGroupMessageWindow, 1);
  
	window->c = gj_connection_ref (c);
	window->connected_jid = gj_jid_ref (connected_jid);
	window->roster = r;
	window->group = g_strdup (group);


	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "group_message",
				     NULL,
				     "group_message", &window->window,
				     "entry", &window->entry,
				     "textview", &window->textview,
				     "treeview", &window->treeview,
				     "button_send", &window->button_send,
				     "button_close", &window->button_close,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "group_message", "delete_event", on_delete_event,
			      "group_message", "destroy", on_destroy,
			      "button_send", "clicked", on_button_send_clicked,
			      "button_close", "clicked", on_button_close_clicked,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (window->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);

	gj_gtk_gm_setup (window);

	return TRUE;
}

static gboolean gj_gtk_gm_setup (GjGroupMessageWindow *window)
{
	GtkTextBuffer *buffer = NULL;
 
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set title */
	gj_gtk_set_window_title_with_args (window->window, "%s %s", _("Sending Message to Group"), window->group);

	/* set buffer changed callback */
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview));

	g_signal_connect (G_OBJECT (buffer), "changed", 
			  G_CALLBACK (on_textbuffer_changed), 
			  window);

	/* set up model */
	gj_gtk_gm_model_setup (window);
	gj_gtk_gm_model_populate_from_roster (window);

	return TRUE;
}

static gboolean gj_gtk_gm_model_setup (GjGroupMessageWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	selection = gtk_tree_view_get_selection (view);

	/* store */
	store = gtk_list_store_new (COL_GROUP_MESSAGE_COUNT,
				    G_TYPE_STRING,   /* name */
				    G_TYPE_INT,      /* ri_id */
				    G_TYPE_BOOLEAN); /* include */

			     
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	gj_gtk_gm_model_populate_columns (window);

	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE);
	gtk_tree_view_set_search_column (view, COL_GROUP_MESSAGE_NAME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, FALSE);
	gtk_tree_view_columns_autosize (view);

	/* clean up */
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

static gboolean gj_gtk_gm_model_populate_columns (GjGroupMessageWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeViewColumn *column = NULL;
	GtkCellRenderer *renderer = NULL;
	gint col_offset = 0;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);

	/* COL_ROSTER_GROUP_ENABLED */
	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled", G_CALLBACK (on_treeview_cell_toggled), window);

	column = gtk_tree_view_column_new_with_attributes ("",
							   renderer,
							   "active", COL_GROUP_MESSAGE_ENABLED,
							   NULL);

	/* set this column to a fixed sizing (of 50 pixels) */
	gtk_tree_view_column_set_sort_column_id (column, COL_GROUP_MESSAGE_ENABLED);
	gtk_tree_view_append_column (view, column);

	/* COL_ROSTER_GROUP_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, "Group",
								  renderer, 
								  "text", COL_GROUP_MESSAGE_NAME,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", (gint *)COL_GROUP_MESSAGE_NAME);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_GROUP_MESSAGE_NAME);

	return TRUE;
}

static gboolean gj_gtk_gm_model_populate_from_roster (GjGroupMessageWindow *window)
{
	gint index = 0;
	gint count = 0;
	GList *list = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}
  
	list = gj_roster_get_contacts_by_group (window->roster, window->group);
  
	for (index=0,count=0;index<g_list_length (list);index++) {
		GjRosterItem ri = NULL;
		GjPresence pres = NULL;

		ri = (GjRosterItem) g_list_nth_data (list, index);

		if (ri == NULL) {
			continue; 
		}

		pres = gj_roster_item_get_presence (ri);

		if (pres == NULL || gj_presence_get_type (pres) != GjPresenceTypeAvailable) {
			continue; 
		}

		gj_gtk_gm_model_add_contact (window, ri);
		count++;
	}

	return TRUE;
}

static gboolean gj_gtk_gm_model_add_contact (GjGroupMessageWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gchar *name = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: rosterItem was NULL", __FUNCTION__); 
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* check details */
	if (gj_roster_item_get_name (ri) == NULL) {
		GjJID j = NULL;

		j = gj_roster_item_get_jid (ri);
		name = gj_jid_get_part_name_new (j);
		g_message ("%s: roster item had no name, using:'%s' as name", __FUNCTION__, name);
	} else {
		/* copy */
		name = g_strdup (gj_roster_item_get_name (ri));
	}
 
	/* add to tree */
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,
			    COL_GROUP_MESSAGE_NAME, name,
			    COL_GROUP_MESSAGE_RI_ID, gj_roster_item_get_id (ri),
			    COL_GROUP_MESSAGE_ENABLED, TRUE,
			    -1);

	/* clean up */
	g_free (name);

	return TRUE;
}

static gboolean gj_gtk_gm_send (GjGroupMessageWindow *window)
{
	GtkTreeView *treeview = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean valid = FALSE;

	gchar *name = NULL;
	gint ri_id = 0;
	gboolean enabled = FALSE;

	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start, iter_end;

	G_CONST_RETURN gchar *subject = NULL;
	gchar *body = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	treeview = GTK_TREE_VIEW (window->treeview);
	model = gtk_tree_view_get_model (treeview);
	textview = GTK_TEXT_VIEW (window->textview);
	buffer = gtk_text_view_get_buffer (textview);

	gtk_text_buffer_get_bounds (buffer, &iter_start, &iter_end);
	body = gtk_text_buffer_get_text (buffer, &iter_start, &iter_end, FALSE);
	subject = gtk_entry_get_text (GTK_ENTRY (window->entry));

	if (body == NULL || g_utf8_strlen (body, -1) < 1) {
		g_free (body);
		g_warning ("%s: body was NULL or length was < 1", __FUNCTION__);
		return FALSE;
	}
  
	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (;valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;

		gtk_tree_model_get (model, &iter, 
				    COL_GROUP_MESSAGE_NAME, &name,
				    COL_GROUP_MESSAGE_RI_ID, &ri_id,
				    COL_GROUP_MESSAGE_ENABLED, &enabled,
				    -1);
		  
		if (ri_id == 0 || enabled == FALSE) {
			g_free (name);
			continue;
		}

		ri = gj_rosters_find_item_by_id (ri_id);

		if (ri == NULL) {
			continue; 
		}

		j = gj_roster_item_get_jid (ri);

		if (j == NULL) {
			continue;
		}
      
		gj_message_send (window->c, 
				 GjMessageTypeNormal, 
				 gj_jid_get_full (window->connected_jid),
				 gj_jid_get_full (j), 
				 (gchar*)subject, 
				 body, 
				 NULL);
	}

	return TRUE;
}


/*
 * GUI events
 */
static void on_treeview_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjGroupMessageWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;

	gboolean enabled = FALSE;

	view = GTK_TREE_VIEW (window->treeview);
	model = gtk_tree_view_get_model (view);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	path = gtk_tree_path_new_from_string (path_string);

	/* get toggled iter */
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, 
			    COL_GROUP_MESSAGE_ENABLED, &enabled, 
			    -1);

	/* do something */
	enabled ^= 1;

	/* set new value */
	gtk_list_store_set (store, &iter, 
			    COL_GROUP_MESSAGE_ENABLED, enabled, 
			    -1);

	/* clean up */
	gtk_tree_path_free (path);
}

static void on_textbuffer_changed (GtkTextBuffer *textbuffer, GjGroupMessageWindow *window)
{
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	gint length = 0;

	/* check WHOLE buffer for urls */
	gtk_text_buffer_get_bounds (textbuffer, &iter_start, &iter_end);
	length = gtk_text_buffer_get_char_count (textbuffer);

	if (length > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (window->button_send), TRUE);
	} else {
		gtk_widget_set_sensitive (GTK_WIDGET (window->button_send), FALSE);
	}
}

static void on_button_send_clicked (GtkButton *button, GjGroupMessageWindow *window)
{  
	gj_gtk_gm_send (window);
	on_delete_event (window->window, NULL, window);
}

static void on_button_close_clicked (GtkButton *button, GjGroupMessageWindow *window)
{
	on_delete_event (window->window, NULL, window);
}

static void on_destroy (GtkWidget *widget, GjGroupMessageWindow *window)
{
	current_window = NULL;

	if (window == NULL) {
		return;
	}

	g_free (window->group);

	gj_connection_unref (window->c);
	gj_jid_unref (window->connected_jid);

	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjGroupMessageWindow *window)
{
	if (window == NULL) {
		return FALSE;
	}

	gtk_widget_destroy (window->window);

	return TRUE;
}

#ifdef __cplusplus
}
#endif
