/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <gtk/gtk.h> 

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_gtk_stock.h"


static GtkStockItem items[] = {
	{ GJ_STOCK_PRESENCE_AVAILABLE,    NULL },     
	{ GJ_STOCK_PRESENCE_UNAVAILABLE,  NULL },
	{ GJ_STOCK_PRESENCE_CHAT,         NULL },
	{ GJ_STOCK_PRESENCE_DND,          NULL },    
	{ GJ_STOCK_PRESENCE_AWAY,         NULL },
	{ GJ_STOCK_PRESENCE_XA,           NULL },    
	{ GJ_STOCK_PRESENCE_INVISIBLE,    NULL },
	{ GJ_STOCK_PRESENCE_NONE,         NULL },
	{ GJ_STOCK_MESSAGE_NORMAL,        NULL },
	{ GJ_STOCK_MESSAGE_CHAT,          NULL },
	{ GJ_STOCK_MESSAGE_HEADLINE,      NULL },
	{ GJ_STOCK_MESSAGE_ERROR,         NULL },
	{ GJ_STOCK_EMOTE_BIGGRIN,         NULL },
	{ GJ_STOCK_EMOTE_CONFUSED,        NULL },
	{ GJ_STOCK_EMOTE_COOL,            NULL },
	{ GJ_STOCK_EMOTE_EEK,             NULL },
	{ GJ_STOCK_EMOTE_FROWN,           NULL },
	{ GJ_STOCK_EMOTE_MAD,             NULL },
	{ GJ_STOCK_EMOTE_REDFACE,         NULL },
	{ GJ_STOCK_EMOTE_ROLLEYES,        NULL },
	{ GJ_STOCK_EMOTE_SMILE,           NULL },
	{ GJ_STOCK_EMOTE_TONGUE,          NULL },
	{ GJ_STOCK_EMOTE_WINK,            NULL }
};


gboolean gj_gtk_stock_init ()
{
	GtkIconFactory *icon_factory = NULL;
	GtkIconSet *icon_set = NULL;
	GdkPixbuf *pb = NULL;
	gint i = 0;

	gchar *absolute_path = NULL;
	gchar *filename = NULL;
  
	gtk_stock_add (items, G_N_ELEMENTS (items));
  
	icon_factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (icon_factory);
  
	for (i = 0; i < G_N_ELEMENTS (items); i++) {
		gchar *file = NULL;
		/* get image */
		file = g_strdup_printf ("%s.png", items[i].stock_id);

#ifdef G_OS_WIN32
		absolute_path = g_win32_get_package_installation_directory (PACKAGE_NAME, NULL);
		filename = g_build_filename (absolute_path, file, NULL);
#else
		absolute_path = NULL;
		filename = g_build_filename (DATADIR, PACKAGE, file, NULL);
#endif

		pb = gdk_pixbuf_new_from_file (filename, NULL);

		g_free (file);
		g_free (absolute_path);
		g_free (filename);
      
		/* update icon set with image */
		icon_set = gtk_icon_set_new_from_pixbuf (pb);
      
		/* add to factory icon set and stock name. */
		gtk_icon_factory_add (icon_factory, items[i].stock_id, icon_set);
	}

	return TRUE;
}


#ifdef __cplusplus
}
#endif

