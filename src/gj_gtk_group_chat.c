/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <gdk/gdkkeysyms.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_message.h"
#include "gj_presence.h"
#include "gj_event.h"
#include "gj_support.h"
#include "gj_config.h"
#include "gj_jid.h"
#include "gj_group_chat.h"
#include "gj_connection.h"

#include "gj_gtk_group_chat.h"
#include "gj_gtk_support.h"


enum {
	COL_GROUP_CHAT_IMAGE,
	COL_GROUP_CHAT_JID,
	COL_GROUP_CHAT_NAME,
	COL_GROUP_CHAT_ENABLED,
	COL_GROUP_CHAT_COUNT
};


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry;
	GtkWidget *textview_receive;
	GtkWidget *textview_send;
	GtkWidget *treeview;

	/*
	 * members
	 */

	GjConnection c;
	GjJID connected_jid;

	gchar *host;
	gchar *room;
	gchar *nickname;

	gint ri_id;

} GjGroupChatWindow;


/* find functions */
static GjGroupChatWindow *gj_gtk_gcw_find_by_ri_id (gint id);

/*
 * window functions 
 */
static gboolean gj_gtk_gcw_setup (GjGroupChatWindow *window);
static gboolean gj_gtk_gcw_send ();

static gboolean gj_gtk_gcw_disconnect (GjGroupChatWindow *window);

static gboolean gj_gtk_gcw_handle_presence (GjGroupChatWindow *window, GjPresence pres);
static gboolean gj_gtk_gcw_handle_message (GjGroupChatWindow *window, GjMessage m);

static gboolean gj_gtk_gcw_send (GjGroupChatWindow *window, gchar *text);
static gboolean gj_gtk_gcw_receive (GjGroupChatWindow *window, GjMessage m);


/* 
 * model functions 
 */
static gboolean gj_gtk_gcw_model_setup (GjGroupChatWindow *window);
static gboolean gj_gtk_gcw_model_populate_columns (GjGroupChatWindow *window);

static gboolean gj_gtk_gcw_model_find_iter_by_jid (GjGroupChatWindow *window, const gchar *jid, GtkTreeIter *ret_iter);

static gboolean gj_gtk_gcw_model_add (GjGroupChatWindow *window, GjPresence pres);
static gboolean gj_gtk_gcw_model_remove (GjGroupChatWindow *window, GjPresence pres);
static gboolean gj_gtk_gcw_model_update (GjGroupChatWindow *window,  GjPresence pres);

/*
 * gui functions 
 */
static void on_textbuffer_receive_changed (GtkTextBuffer *textbuffer, GjGroupChatWindow *window); 
static gboolean on_textview_receive_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjGroupChatWindow *window);
static gboolean on_textview_receive_button_press_event (GtkWidget *widget, GdkEventButton *event, GjGroupChatWindow *window);
static gboolean on_textview_send_key_release_event (GtkWidget *widget, GdkEventKey *event, GjGroupChatWindow *window);
static gboolean on_textview_send_key_press_event (GtkWidget *widget, GdkEventKey *event, GjGroupChatWindow *window);

static void on_realize (GtkWidget *widget, GjGroupChatWindow *window);
static void on_destroy (GtkWidget *widget, GjGroupChatWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjGroupChatWindow *window);


GList *group_chat_window_list = NULL;


static GjGroupChatWindow *gj_gtk_gcw_find_by_ri_id (gint id)
{
	gint index = 0;

	if (id < 0) {
		g_warning ("%s: id:%d was NULL", __FUNCTION__, id);
		return NULL;
	}

	for (index=0;index<g_list_length (group_chat_window_list);index++) {
		GjGroupChatWindow *window = g_list_nth_data (group_chat_window_list, index);

		if (!window) {
			continue;
		}

		if (window->ri_id == id) {
			return window; 
		}
	}

	return NULL;  
}

/*
 * window functions
 */
gboolean gj_gtk_gcw_load (GjConnection c,
			  GjJID connected_jid, 
			  GjRosterItem ri,
			  const gchar *host, 
			  const gchar *room, 
			  const gchar *nickname)
{
	GladeXML *xml = NULL;
	GjGroupChatWindow *window = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (connected_jid == NULL) {
		g_warning ("%s: connected jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (host == NULL || g_utf8_strlen (host, -1) < 1) {
		g_warning ("%s: host was NULL", __FUNCTION__);
		return FALSE;
	}

	if (room == NULL || g_utf8_strlen (room, -1) < 1) {
		g_warning ("%s: room was NULL", __FUNCTION__);
		return FALSE;
	}

	if (nickname == NULL || g_utf8_strlen (nickname, -1) < 1) {
		g_warning ("%s: nickname was NULL", __FUNCTION__);
		return FALSE;
	}  

	window = g_new0 (GjGroupChatWindow, 1);

	window->c = gj_connection_ref (c);
	window->connected_jid = gj_jid_ref (connected_jid);
     
	window->ri_id = gj_roster_item_get_id (ri);
  
	window->host = g_strdup (host);
	window->room = g_strdup (room);
	window->nickname = g_strdup (nickname);
  
	group_chat_window_list = g_list_append (group_chat_window_list, window);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "group_chat",
				     NULL,
				     "group_chat", &window->window,
				     "entry", &window->entry,
				     "textview_receive", &window->textview_receive,
				     "textview_send", &window->textview_send,
				     "treeview", &window->treeview,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "group_chat", "delete_event", on_delete_event,
			      "group_chat", "destroy", on_destroy,
			      "group_chat", "realize", on_realize,
			      "textview_receive", "button_press_event", on_textview_receive_button_press_event,
			      "textview_receive", "motion_notify_event", on_textview_receive_motion_notify_event,
			      "textview_send", "key_press_event", on_textview_send_key_press_event,
			      "textview_send", "key_release_event", on_textview_send_key_release_event,
			      NULL);

	g_object_unref (xml);

	gj_gtk_gcw_setup (window);

	return TRUE;
}

static gboolean gj_gtk_gcw_setup (GjGroupChatWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	gchar *text = NULL;

	if (window == NULL) {
		g_warning ("%s: GjGroupChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set title */
	text = g_strdup_printf ("%s %s@%s", 
				_("Chatting in"), 
				window->room,
				window->host);

	gtk_window_set_title (GTK_WINDOW (window->window), text);
	g_free (text);

	/* set buffer changed callback */
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_receive));

	g_signal_connect (G_OBJECT (buffer), "changed",  
			  G_CALLBACK (on_textbuffer_receive_changed),  
			  window); 

	/* set styles */
	gtk_text_buffer_create_tag (buffer, "bold", 
				    "weight", PANGO_WEIGHT_BOLD, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "italic", 
				    "style", PANGO_STYLE_ITALIC, 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "blue_foreground", 
				    "foreground", "darkblue", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "red_foreground", 
				    "foreground", "darkred", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "green_foreground", 
				    "foreground", "darkgreen", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "black_foreground", 
				    "foreground", "black", 
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "url", 
				    "foreground", "blue", 
				    NULL); 

	/* set up model */
	gj_gtk_gcw_model_setup (window);

	return TRUE;
}

static gboolean gj_gtk_gcw_disconnect (GjGroupChatWindow *window)
{
	gchar *jid = NULL;
	GjJID j = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set up jid */
	jid = g_strdup_printf ("%s@%s/%s", 
			       window->room,
			       window->host,
			       window->nickname);

	j = gj_jid_new (jid);
  
	/* send unavailable presence back */
	gj_presence_send_to (window->c,
			     j, 
			     TRUE, 
			     GjPresenceTypeUnAvailable, 
			     GjPresenceShowNormal, 
			     NULL, 
			     0);

	/* clean up */
	g_free (jid);

	if (j != NULL) {
		gj_jid_unref (j); 
	}
  
	return TRUE;
}

gboolean gj_gtk_gcw_post_by_event (GjEvent ev)
{
	GjGroupChatWindow *window = NULL;

	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if ((window = gj_gtk_gcw_find_by_ri_id (gj_event_get_ri_id (ev))) == NULL) {
		g_warning ("%s: could not find GjGroupChatWindow by roster item id:%d", __FUNCTION__, gj_event_get_ri_id (ev));
		return FALSE;
	}

	switch (gj_event_get_type (ev)) {
	case GjEventTypePresence:
		gj_gtk_gcw_handle_presence (window, (GjPresence)gj_event_get_data (ev));
		break;

	case GjEventTypeMessage:
		gj_gtk_gcw_handle_message (window, (GjMessage)gj_event_get_data (ev));
		break;

	default:
		break;
	}

	return TRUE;
}

static gboolean gj_gtk_gcw_handle_presence (GjGroupChatWindow *window, GjPresence pres)
{
	GtkTreeIter iter;

	GjJID j = NULL;
	j = gj_presence_get_from_jid (pres);

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_presence_get_type (pres) == GjPresenceTypeAvailable) {
		/* not found, add contact else update */
		if (gj_gtk_gcw_model_find_iter_by_jid (window, gj_jid_get_full (j), &iter) == FALSE) {
			gj_gtk_gcw_model_add (window, pres);
		} else {
			gj_gtk_gcw_model_update (window, pres); 
		}
	} else {
		gchar *jid = NULL;

		/* set up jid */
		jid = g_strdup_printf ("%s@%s/%s", 
				       window->room,
				       window->host,
				       window->nickname);
      
		/* remove */
		if (g_strcasecmp (gj_jid_get_full (j), jid) == 0) {
			gtk_widget_destroy (window->window);
		} else {
			gj_gtk_gcw_model_remove (window, pres);
		}

		/* clean up */
		g_free (jid);
	}

	return TRUE;
}

static gboolean gj_gtk_gcw_handle_message (GjGroupChatWindow *window, GjMessage m)
{
	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) { 
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_message_get_type (m) != GjMessageTypeGroupChat) {
		return TRUE;
	}
  
	gj_gtk_gcw_receive (window, m);
	return TRUE;
}

static gboolean gj_gtk_gcw_send (GjGroupChatWindow *window, gchar *text)
{
	gchar *jid = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (text == NULL) {
		g_warning ("%s: text was NULL, can not send NULL", __FUNCTION__);
		return FALSE;
	}

	/* set up jid */
	jid = g_strdup_printf ("%s@%s", window->room, window->host);

	/* send message */
	gj_message_send (window->c,
			 GjMessageTypeGroupChat, 
			 gj_jid_get_full (window->connected_jid),
			 jid, 
			 NULL, 
			 text, 
			 NULL);
  
#if 0
	/* log to history */
	gj_history_write_sent_by_ri_id (gj_gtk_cw_get_ri_id (cw), 
					(gchar*)gj_get_timestamp_nice (), 
					NULL, 
					text);
#endif

	/* clean up */
	g_free (jid);

	return TRUE;
}

static gboolean gj_gtk_gcw_receive (GjGroupChatWindow *window, GjMessage m)
{
	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextMark *mark = NULL;
	GtkTextIter iter;

	GjJID j = NULL;

	gchar *message = NULL;
	gchar *time = NULL;
	gchar *colour_time_and_name = NULL;
	gchar *colour_content = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	textview = GTK_TEXT_VIEW (window->textview_receive);

	j = gj_message_get_from_jid (m);
  
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_receive));
	time = g_strdup_printf ("[%s]", gj_get_timestamp_from_timet (gj_message_get_time (m)));

	/* set details */
	message = g_strdup_printf ("%s%s\n", gj_jid_get_resource (j) == NULL ? "" : ": ", gj_message_get_body (m));

	/* set colours */
	if (gj_jid_get_resource (j) != NULL) {
		if (g_strcasecmp (gj_jid_get_resource (j), window->nickname) == 0) {
			colour_time_and_name = g_strdup_printf ("green_foreground");
		} else {
			colour_time_and_name = g_strdup_printf ("blue_foreground");
		}

		colour_content = g_strdup_printf ("black_foreground");
	} else {
		colour_time_and_name = g_strdup_printf ("red_foreground");
		colour_content = g_strdup_printf ("red_foreground");
	}


#if 0
	/* get text direction */
	if (gj_gtk_cw_get_reverse_text_direction (cw) == TRUE) {
		gtk_text_buffer_get_start_iter (buffer, &iter);
	} else {
		gtk_text_buffer_get_end_iter (buffer, &iter); 
	}
#else
	gtk_text_buffer_get_end_iter (buffer, &iter);
#endif

	if (gj_config_get_mpf_hide_time () == FALSE) {
		/* insert at iter */
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  time, -1,
							  colour_time_and_name,
							  NULL);
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  " ", -1,
							  colour_time_and_name,
							  NULL);
	}

	if (gj_jid_get_resource (j) != NULL) {
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  gj_jid_get_resource (j), -1,
							  "bold", 
							  colour_time_and_name,
							  NULL);
	}

	if (gj_config_get_mpf_use_smileys () == TRUE) {
		gj_gtk_textbuffer_insert_with_emote_icons (buffer, &iter, colour_content, message);
	} else {
		gj_gtk_textbuffer_insert (buffer, &iter, colour_content, message);
	}

	/* set subject */
	if (gj_message_get_subject (m) != NULL) {
		gtk_entry_set_text (GTK_ENTRY (window->entry), gj_message_get_subject (m));
	}

#if 0
	/* log to history */
	gj_history_write_received_by_ri_id (gj_gtk_cw_get_ri_id (cw), 
					    (gchar*)gj_get_timestamp_nice_from_timet (gj_message_get_time (m)), 
					    NULL, 
					    gj_message_get_body (m));

	if (gj_gtk_cw_get_reverse_text_direction (cw) == FALSE) {
		mark = gtk_text_buffer_create_mark (buffer, NULL, &iter, FALSE);
		gtk_text_view_scroll_mark_onscreen (textview, mark);
		gtk_text_buffer_delete_mark (buffer, mark);
	}

#endif

	mark = gtk_text_buffer_create_mark (buffer, NULL, &iter, FALSE);
	gtk_text_view_scroll_mark_onscreen (textview, mark);
	gtk_text_buffer_delete_mark (buffer, mark);

	/* clean up */
	g_free (colour_time_and_name);
	g_free (colour_content);
	g_free (time);
	g_free (message);

	return TRUE;
}


/*
 * model functions
 */
static gboolean gj_gtk_gcw_model_setup (GjGroupChatWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	/* store */
	store = gtk_list_store_new (COL_GROUP_CHAT_COUNT,
				    GDK_TYPE_PIXBUF,   /* presence */
				    G_TYPE_STRING,     /* jid */
				    G_TYPE_STRING,     /* name */
				    G_TYPE_BOOLEAN);   /* include */
			     
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	gj_gtk_gcw_model_populate_columns (window);

	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE);
	gtk_tree_view_set_search_column (view, COL_GROUP_CHAT_NAME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, FALSE);
	gtk_tree_view_columns_autosize (view);

	/* clean up */
	if (store != NULL) {
		g_object_unref (G_OBJECT (store)); 
	}

	return TRUE;
}

static gboolean gj_gtk_gcw_model_populate_columns (GjGroupChatWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeViewColumn *column = NULL;
	GtkCellRenderer *renderer = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;

	}

	view = GTK_TREE_VIEW (window->treeview);

	/* cell renderer's */
	column = gtk_tree_view_column_new ();

	/* COL_ROSTER_IMAGE */  
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_attributes (column,
					     renderer,
					     "pixbuf", COL_GROUP_CHAT_IMAGE,
					     NULL);

	/* COL_ROSTER_TEXT */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_attributes (column,
					     renderer,
					     "text", COL_GROUP_CHAT_NAME,
					     NULL);

	/* insert column */
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);

	return TRUE;
}

static gboolean gj_gtk_gcw_model_find_iter_by_jid (GjGroupChatWindow *window, const gchar *jid, GtkTreeIter *ret_iter)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;
	gboolean found = FALSE;
	gchar *text = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ret_iter == NULL) {
		g_warning ("%s: ret_iter pointer was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (found = FALSE; valid == TRUE && found == FALSE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_tree_model_get (model, &iter, 
				    COL_GROUP_CHAT_JID, &text,
				    -1);

		if (text != NULL && strcmp (jid, text) == 0) {
			*ret_iter = iter;
			found = TRUE;
		}

		g_free (text);
	}

	return found;
}

static gboolean gj_gtk_gcw_model_add (GjGroupChatWindow *window, GjPresence pres)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	GdkPixbuf *pb = NULL;

	GjJID j = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	} 

	j = gj_presence_get_from_jid (pres);
	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	pb = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));

	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter, 
			    COL_GROUP_CHAT_IMAGE, pb,
			    COL_GROUP_CHAT_JID, gj_jid_get_full (j),
			    COL_GROUP_CHAT_NAME, gj_jid_get_resource (j),
			    -1);

	/* clean up */
	if (pb != NULL) {
		g_object_unref (G_OBJECT (pb));
	}

	return TRUE;
}

static gboolean gj_gtk_gcw_model_remove (GjGroupChatWindow *window, GjPresence pres)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	GjJID j = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	} 

	j = gj_presence_get_from_jid (pres);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	if (gj_gtk_gcw_model_find_iter_by_jid (window, gj_jid_get_full (j), &iter) == FALSE) {
		g_warning ("%s: user not found in model with jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return TRUE;
	}

	gtk_list_store_remove (store, &iter);
	return TRUE;
}

static gboolean gj_gtk_gcw_model_update (GjGroupChatWindow *window, GjPresence pres)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	GdkPixbuf *pb = NULL;

	GjJID j = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	} 

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	} 

	j = gj_presence_get_from_jid (pres);
  
	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	if (gj_gtk_gcw_model_find_iter_by_jid (window, gj_jid_get_full (j), &iter) == FALSE) {
		g_warning ("%s: user not found in model with jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return TRUE;
	}

	pb = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));
  
	gtk_list_store_set (store, &iter, 
			    COL_GROUP_CHAT_IMAGE, pb,
			    COL_GROUP_CHAT_JID, gj_jid_get_full (j),
			    COL_GROUP_CHAT_NAME, gj_jid_get_resource (j),
			    -1);

	/* clean up */
	if (pb != NULL) {
		g_object_unref (G_OBJECT (pb));
	}

	return TRUE;
}

/*
 * GUI events
 */
static void on_textbuffer_receive_changed (GtkTextBuffer *textbuffer, GjGroupChatWindow *window)
{
	gj_gtk_textbuffer_handle_changed (textbuffer, window);
}

static gboolean on_textview_receive_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjGroupChatWindow *window)
{
	return gj_gtk_textview_handle_motion_notify_event (widget, event, window);
}

static gboolean on_textview_receive_button_press_event (GtkWidget *widget, GdkEventButton *event, GjGroupChatWindow *window)
{
	return gj_gtk_textview_handle_button_press_event (widget, event, window);
}

static gboolean on_textview_send_key_release_event (GtkWidget *widget, GdkEventKey *event, GjGroupChatWindow *window)
{
	GtkTextBuffer *buffer = NULL;

	if (!window) {
		return FALSE; 
	}

	if ((buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget))) == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return FALSE;
	}

	/* if was enter then clear box */
	if (event != NULL && event->keyval == GDK_Return && (event->state & GDK_SHIFT_MASK) == FALSE) {
		GtkTextIter iter_start;
		GtkTextIter iter_end; 
		
		gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
		gtk_text_buffer_delete (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	} else {
		gint length = 0;
		
		/* set toolbar icon */  
		length = gtk_text_buffer_get_char_count (buffer);
	}

	return FALSE;
}

static gboolean on_textview_send_key_press_event (GtkWidget *widget, GdkEventKey *event, GjGroupChatWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end;  

	gchar *text = NULL;
	gint length = 0;

	if (!window) {
		return FALSE;
	}
    
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	length = gtk_text_buffer_get_char_count (buffer);
 
	if (length < 1) {
		/* if enter was pressed... warn user that we can not do this */
		if (event != NULL && event->keyval == GDK_Return && (event->state & GDK_SHIFT_MASK) == FALSE)
			g_warning ("%s: can't send NULL or 0 length message", __FUNCTION__);
      
		return FALSE;
	}

	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	text = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end, TRUE);
   
	/* send message and clear text box */
	if (event->keyval == GDK_Return && (event->state & GDK_SHIFT_MASK) == FALSE) {
		gj_gtk_gcw_send (window, text);
	}
    
	/* cleanup */
	g_free (text);
    
	return FALSE;
}

static void on_realize (GtkWidget *widget, GjGroupChatWindow *window)
{
	if (!window) {
		return; 
	}
  
	gdk_window_set_decorations (GDK_WINDOW (window->window), 
				    GDK_DECOR_ALL);

	gtk_widget_set_events (GTK_WIDGET (window->textview_receive),
			       GDK_EXPOSURE_MASK | 
			       GDK_POINTER_MOTION_MASK | 
			       GDK_POINTER_MOTION_HINT_MASK | 
			       GDK_BUTTON_RELEASE_MASK | 
			       GDK_STRUCTURE_MASK | 
			       GDK_PROPERTY_CHANGE_MASK);  
}

static void on_destroy (GtkWidget *widget, GjGroupChatWindow *window)
{
	GjRosterItem ri = NULL;

	if (!window) {
		return;
	}

	group_chat_window_list = g_list_remove (group_chat_window_list, window);

	ri = gj_rosters_find_item_by_id (window->ri_id);
	if (ri) {
		gj_group_chat_del (ri);
		gj_roster_item_free (ri);
	}

	window->ri_id = -1;

	g_free (window->host);
	g_free (window->room);
	g_free (window->nickname);
	
	gj_connection_unref (window->c);
	gj_jid_unref (window->connected_jid);
	
	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjGroupChatWindow *window)
{
	gj_gtk_gcw_disconnect (window);
	return TRUE;
}

#ifdef __cplusplus
}
#endif
