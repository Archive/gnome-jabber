/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_config_h
#define __gj_config_h

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


typedef union t_GjConfigUnion *GjConfigUnion;


/***********************************************************************
 * CONFIG - used for Windows Only.        
 ***********************************************************************/

#define FILE_CONFIG PACKAGE ".config"


#define CONFIG_APP "gnome-jabber"

#define CONFIG_SECT_CS "connection"
#define CONFIG_SECT_MPF "preferences"
#define CONFIG_SECT_MI "information"
#define CONFIG_SECT_ROSTER "roster"
#define CONFIG_SECT_PP "presence_presets"

#define CONFIG_CS_SERVER "server"
#define CONFIG_CS_PORT "port"
#define CONFIG_CS_USERNAME "username"
#define CONFIG_CS_PASSWORD "password"

#define CONFIG_CS_RESOURCE "resource"
#define CONFIG_CS_PRIORITY "priority"

#define CONFIG_CS_PROMPT_FOR_PASSWORD "prompt_for_password"
#define CONFIG_CS_USE_PLAIN_TEXT_PASSWORD "use_plain_text_password"
#define CONFIG_CS_USE_SSL "use_ssl"

#define CONFIG_CS_AUTO_CONNECT_ON_STARTUP "auto_connect_on_startup"
#define CONFIG_CS_AUTO_RECONNECT_ON_DISCONNECT "auto_reconnect_on_disconnect"


#define CONFIG_MPF_HIDE_OFFLINE_CONTACTS "hide_offline_contacts"
#define CONFIG_MPF_HIDE_OFFLINE_GROUP "hide_offline_group"
#define CONFIG_MPF_HIDE_AGENTS "hide_agents"
#define CONFIG_MPF_HIDE_CONTACT_PRESENCE_DETAILS "hide_contact_presence_details"

#define CONFIG_MPF_DOUBLE_CLICK_FOR_CHAT "double_click_for_chat"

#define CONFIG_MPF_OPEN_WINDOW_ON_NEW_MESSAGE "open_window_on_new_message"
#define CONFIG_MPF_RAISE_WINDOW_ON_MESSAGE "raise_window_on_message"
#define CONFIG_MPF_HIDE_TIME "hide_time"
#define CONFIG_MPF_REVERSE_TEXT_DIRECTION "reverse_text_direction"
#define CONFIG_MPF_USE_SMILEYS "use_smileys"

#define CONFIG_MPF_USE_SPELL_CHECKER "use_spell_checker"

#define CONFIG_MPF_USE_AUTO_AWAY "use_auto_away"
#define CONFIG_MPF_AUTO_AWAY_AFTER "auto_away_after"
#define CONFIG_MPF_AUTO_XA_AFTER "auto_xa_after"
#define CONFIG_MPF_AUTO_AWAY_STATUS "auto_away_status"
#define CONFIG_MPF_AUTO_XA_STATUS "auto_xa_status"

#define CONFIG_MPF_SOUND_ENABLED "sound_enabled"
#define CONFIG_MPF_SOUND_ON_USER_ONLINE "sound_on_user_online"
#define CONFIG_MPF_SOUND_ON_USER_OFFLINE "sound_on_user_offline"
#define CONFIG_MPF_SOUND_ON_RECEIVE_MESSAGE "sound_on_receive_message"
#define CONFIG_MPF_SOUND_ON_RECEIVE_CHAT "sound_on_receive_chat"
#define CONFIG_MPF_SOUND_ON_RECEIVE_HEADLINE "sound_on_receive_headline"
#define CONFIG_MPF_SOUND_ON_RECEIVE_ERROR "sound_on_receive_error"
#define CONFIG_MPF_SOUND_BEEP_IF_NO_FILE "sound_beep_if_no_file"
#define CONFIG_MPF_SOUND_FILE_ON_USER_ONLINE "sound_file_on_user_online"
#define CONFIG_MPF_SOUND_FILE_ON_USER_OFFLINE "sound_file_on_user_offline"
#define CONFIG_MPF_SOUND_FILE_ON_RECEIVE_MESSAGE "sound_file_on_receive_message"
#define CONFIG_MPF_SOUND_FILE_ON_RECEIVE_CHAT "sound_file_on_receive_chat"
#define CONFIG_MPF_SOUND_FILE_ON_RECEIVE_HEADLINE "sound_file_on_receive_headline"
#define CONFIG_MPF_SOUND_FILE_ON_RECEIVE_ERROR "sound_file_on_receive_error"

#define CONFIG_MPF_USE_DOCKLET "use_docklet"

#define CONFIG_MPF_LOG_LOCATION "log_location"
#define CONFIG_MPF_HISTORY_LOCATION "history_location"
#define CONFIG_MPF_USE_HISTORY "use_history"


#define CONFIG_MI_PERSONAL_FULL_NAME "full_name"
#define CONFIG_MI_PERSONAL_NAME_GIVEN "name_given"
#define CONFIG_MI_PERSONAL_NAME_FAMILY "name_family"
#define CONFIG_MI_PERSONAL_EMAIL "email"
#define CONFIG_MI_PERSONAL_URL "url"
#define CONFIG_MI_PERSONAL_TELEPHONE "telephone"

#define CONFIG_MI_LOCATION_STREET "street"
#define CONFIG_MI_LOCATION_EXTADD "extadd"
#define CONFIG_MI_LOCATION_LOCALITY "locality"
#define CONFIG_MI_LOCATION_REGION "region"
#define CONFIG_MI_LOCATION_PCODE "pcode"
#define CONFIG_MI_LOCATION_COUNTRY "country"

#define CONFIG_MI_ORG_ORGNAME "org_orgname"
#define CONFIG_MI_ORG_ORGUNIT "org_orgunit"
#define CONFIG_MI_TITLE "title"
#define CONFIG_MI_ROLE "role"

#define CONFIG_MI_ABOUT_DOB_DAY "dob_day"
#define CONFIG_MI_ABOUT_DOB_MONTH "dob_month"
#define CONFIG_MI_ABOUT_DOB_YEAR "dob_year"
#define CONFIG_MI_ABOUT_DESCRIPTION "description"

#define CONFIG_MI_OPTIONS_PUBLISH_IN_JUD "publish_in_jud"


gboolean gj_config_init ();
gboolean gj_config_term ();

/* read and write the config */
gboolean gj_config_read ();
gboolean gj_config_write ();

/* gets */
const gchar *gj_config_get_cs_server ();
guint gj_config_get_cs_port ();
const gchar *gj_config_get_cs_username ();
const gchar *gj_config_get_cs_password ();
const gchar *gj_config_get_cs_resource ();
gint8 gj_config_get_cs_priority ();

gboolean gj_config_get_cs_prompt_for_password ();
gboolean gj_config_get_cs_use_plain_text_password ();
gboolean gj_config_get_cs_use_ssl ();

gboolean gj_config_get_cs_auto_connect_on_startup ();
gboolean gj_config_get_cs_auto_reconnect_on_disconnect ();

gboolean gj_config_get_mpf_hide_offline_contacts ();
gboolean gj_config_get_mpf_hide_offline_group ();
gboolean gj_config_get_mpf_hide_agents ();
gboolean gj_config_get_mpf_hide_contact_presence_details ();

gboolean gj_config_get_mpf_double_click_for_chat ();

gboolean gj_config_get_mpf_open_window_on_new_message ();
gboolean gj_config_get_mpf_raise_window_on_message ();
gboolean gj_config_get_mpf_hide_time ();
gboolean gj_config_get_mpf_reverse_text_direction ();
gboolean gj_config_get_mpf_use_smileys ();

gboolean gj_config_get_mpf_use_spell_checker ();

gboolean gj_config_get_mpf_use_auto_away ();
guint gj_config_get_mpf_auto_away_after ();
guint gj_config_get_mpf_auto_xa_after ();
const gchar *gj_config_get_mpf_auto_away_status ();
const gchar *gj_config_get_mpf_auto_xa_status ();

gboolean gj_config_get_mpf_sound_on_user_online ();
gboolean gj_config_get_mpf_sound_on_user_offline ();
gboolean gj_config_get_mpf_sound_on_receive_message ();
gboolean gj_config_get_mpf_sound_on_receive_chat ();
gboolean gj_config_get_mpf_sound_on_receive_headline ();
gboolean gj_config_get_mpf_sound_on_receive_error ();

const gchar *gj_config_get_mpf_sound_file_on_user_online ();
const gchar *gj_config_get_mpf_sound_file_on_user_offline ();
const gchar *gj_config_get_mpf_sound_file_on_receive_message ();
const gchar *gj_config_get_mpf_sound_file_on_receive_chat ();
const gchar *gj_config_get_mpf_sound_file_on_receive_headline ();
const gchar *gj_config_get_mpf_sound_file_on_receive_error ();

gboolean gj_config_get_mpf_use_docklet ();

const gchar *gj_config_get_mpf_log_location ();
const gchar *gj_config_get_mpf_history_location ();
gboolean gj_config_get_mpf_use_history ();


GList *gj_config_get_pp_all ();


/* sets */
gboolean gj_config_set_cs_server (const gchar *string);
gboolean gj_config_set_cs_port (guint integer);
gboolean gj_config_set_cs_username (const gchar *string);
gboolean gj_config_set_cs_password (const gchar *string);
gboolean gj_config_set_cs_resource (const gchar *string);
gboolean gj_config_set_cs_priority (gint8 integer);

gboolean gj_config_set_cs_prompt_for_password (gboolean bool);
gboolean gj_config_set_cs_use_plain_text_password (gboolean bool);
gboolean gj_config_set_cs_use_ssl (gboolean bool);

gboolean gj_config_set_cs_auto_connect_on_startup (gboolean bool);
gboolean gj_config_set_cs_auto_reconnect_on_disconnect (gboolean bool);

gboolean gj_config_set_mpf_hide_offline_contacts (gboolean bool);
gboolean gj_config_set_mpf_hide_offline_group (gboolean bool);
gboolean gj_config_set_mpf_hide_agents (gboolean bool);
gboolean gj_config_set_mpf_hide_contact_presence_details (gboolean bool);

gboolean gj_config_set_mpf_double_click_for_chat (gboolean bool);

gboolean gj_config_set_mpf_open_window_on_new_message (gboolean bool);
gboolean gj_config_set_mpf_raise_window_on_message (gboolean bool);
gboolean gj_config_set_mpf_hide_time (gboolean bool);
gboolean gj_config_set_mpf_reverse_text_direction (gboolean bool);
gboolean gj_config_set_mpf_use_smileys (gboolean bool);

gboolean gj_config_set_mpf_use_spell_checker (gboolean bool);

gboolean gj_config_set_mpf_use_auto_away (gboolean bool);
gboolean gj_config_set_mpf_auto_away_after (guint integer);
gboolean gj_config_set_mpf_auto_xa_after (guint integer);
gboolean gj_config_set_mpf_auto_away_status (const gchar *string);
gboolean gj_config_set_mpf_auto_xa_status (const gchar *string);

gboolean gj_config_set_mpf_sound_on_user_online (gboolean bool);
gboolean gj_config_set_mpf_sound_on_user_offline (gboolean bool);
gboolean gj_config_set_mpf_sound_on_receive_message (gboolean bool);
gboolean gj_config_set_mpf_sound_on_receive_chat (gboolean bool);
gboolean gj_config_set_mpf_sound_on_receive_headline (gboolean bool);
gboolean gj_config_set_mpf_sound_on_receive_error (gboolean bool);
gboolean gj_config_set_mpf_sound_file_on_user_online (const gchar *string);
gboolean gj_config_set_mpf_sound_file_on_user_offline (const gchar *string);
gboolean gj_config_set_mpf_sound_file_on_receive_message (const gchar *string);
gboolean gj_config_set_mpf_sound_file_on_receive_chat (const gchar *string);
gboolean gj_config_set_mpf_sound_file_on_receive_headline (const gchar *string);
gboolean gj_config_set_mpf_sound_file_on_receive_error (const gchar *string);

gboolean gj_config_set_mpf_use_docklet (gboolean bool);

gboolean gj_config_set_mpf_log_location (const gchar *string);
gboolean gj_config_set_mpf_history_location (const gchar *string);
gboolean gj_config_set_mpf_use_history (gboolean bool);

gboolean gj_config_get_pp_index (gint index, gchar **show, gchar **status, gchar **priority);
gint gj_config_get_pp_count ();


#endif

#ifdef __cplusplus
}
#endif
