/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <gdk/gdkkeysyms.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_translate.h"
#include "gj_autoaway.h"
#include "gj_parser.h"
#include "gj_event.h"
#include "gj_history.h"
#include "gj_config.h"
#include "gj_jid.h"
#include "gj_connection.h"
#include "gj_file_transfer.h"
#include "gj_stream.h"
#include "gj_support.h"
#include "gj_register.h"

#include "gj_gtk_roster.h"
#include "gj_gtk_support.h"
#include "gj_gtk_message.h"
#include "gj_gtk_send_message.h"
#include "gj_gtk_chat.h"
#include "gj_gtk_group_chat.h"
#include "gj_gtk_headline.h"
#include "gj_gtk_presence_presets.h"
#include "gj_gtk_my_information.h"
#include "gj_gtk_preferences.h"
#include "gj_gtk_connection_settings.h"
#include "gj_gtk_password_change.h"
#include "gj_gtk_log.h"
#include "gj_gtk_user_information.h"
#include "gj_gtk_new_account.h"
#include "gj_gtk_edit_groups.h"
#include "gj_gtk_group_message.h"
#include "gj_gtk_add_contact.h"
#include "gj_gtk_subscription_request.h"
#include "gj_gtk_about.h"
#include "gj_gtk_debug.h"
#include "gj_gtk_event_viewer.h"
#include "gj_gtk_group_chat_join.h"
#include "gj_gtk_browse_services.h"
#include "gj_gtk_docklet.h"
#include "gj_gtk_rename_contact.h"
#include "gj_gtk_rename_group.h"
#include "gj_gtk_stock.h"
#include "gj_gtk_register_service.h"
#include "gj_gtk_password_required.h"


#define ROSTER_GROUP_OFFLINE _("Offline")
#define ROSTER_GROUP_NOT_ON_ROSTER _("Not On Roster")
#define ROSTER_GROUP_RESOURCE _("Resources")
#define ROSTER_GROUP_SUBSCRIBING _("Subscribing")
#define ROSTER_GROUP_AGENTS _("Agents")

#define ROSTER_CONTACT_PRESENCE_DETAILS_FORMAT "%s\n<span size=\"smaller\">%s</span>"


typedef struct  {
	GjRoster r;

	gchar *group;
  
	gboolean found;
	GtkTreeIter found_iter;

} GjRosterWindowFindGroup;


typedef struct  {
	GjRoster r;
	GjRosterItem ri;

	GjJID jid;

	gboolean found;
	GList *iters;

} GjRosterWindowFindUser;


/* 
 * roster columns 
 */
enum {
	COL_ROSTER_IMAGE = 0,
	COL_ROSTER_IMAGE_FLASH,
	COL_ROSTER_IMAGE_FLASH_ON,
	COL_ROSTER_TEXT,
	COL_ROSTER_STATUS,
	COL_ROSTER_ID,
	COL_ROSTER_NAME,
	COL_ROSTER_NOT_GROUP,
	COL_ROSTER_COUNT 
};

typedef struct  {
	gchar *server;
	gint16 port;
	gchar *username;
	gchar *password;
	gchar *resource;

	GjStream stream;
	GjConnection c;

	gboolean is_disconnect_planned;
	gboolean is_logged_in;

	gint timeout_id;

	gchar *nick;
	
	GjJID jid;

	/* auto away */
	gboolean is_auto_away;
	GjPresenceShow auto_away_presence_show;  /* either xa or away */
	
	/* presence */
	GjPresenceType presence_type;
	GjPresenceShow presence_show;
	gchar *presence_status;
	gint8 presence_priority;

} GjRosterConnection;


typedef struct  {
	GtkWidget *window; 

	GtkWidget *treeview;

	GtkWidget *hbox;

	GtkWidget *menubar;
	GtkWidget *mnu_jabber_connect;
	GtkWidget *mnu_jabber_disconnect;
	GtkWidget *mnu_jabber_register_new_account;
	GtkWidget *mnu_jabber_unregister;
	GtkWidget *mnu_jabber_change_password;
	GtkWidget *mnu_jabber_preferences;
	GtkWidget *mnu_jabber_connection_settings;
	GtkWidget *mnu_jabber_my_information;
	GtkWidget *mnu_jabber_quit;

	GtkWidget *mnu_action;
	GtkWidget *mnu_action_add;
	GtkWidget *mnu_action_send_message;
	GtkWidget *mnu_action_group_chat;
	GtkWidget *mnu_action_edit_status_messages;
	GtkWidget *mnu_action_import;
	GtkWidget *mnu_action_event_viewer;
	GtkWidget *mnu_action_directory_search;
	GtkWidget *mnu_action_browse_services;

	GtkWidget *mnu_help_homepage;
	GtkWidget *mnu_help_project_information;
	GtkWidget *mnu_help_log;
	GtkWidget *mnu_help_debug;
	GtkWidget *mnu_help_about;

	GtkWidget *eventbox_statusbar;

	GtkWidget *hbox_status;
	GtkWidget *eventbox_presence;
	GtkWidget *image_presence;
	GtkWidget *label_presence;
	GtkWidget *entry_status;

	GtkWidget *hbox_connection;
	GtkWidget *label_connection;
	GtkWidget *progressbar_connection;

	/* members */
	gint flash_timeout_id;
	GList *flash_list;

	GjRosterConnection *connection;

} GjRosterWindow;


const gchar *authors[] = {
	"Martyn Russell (ginxd@btopenworld.com)",
	NULL
};

const gchar *documenters[] = {
	NULL
};

const gchar *translators[] = {
	"tradgnome@softcatala.org (ca)",
	"cs@li.org (cs)",
	"de@li.org (de)",
	"adamw@FreeBSD.org (en_CA)",
	"gowen72@yahoo.comg (en_GB)",
	"traductores@es.gnome.org (es)"
	"gnomefr@traduc.org (fr)",
	"gaeilge-gnulinux@lists.sourceforge.net (ga)",
	"lokalizacija@linux.hr (hr)",
	"komp_lt@konferencijos.lt (lt)",
	"vertaling@nl.linux.org (nl)",
	"i18n-nb@lister.ping.uio.no (no)",
	"translation-team-pl@lists.sourceforge.net (pl)",
	"gnome_pt@yahoogroups.com (pt)",
	"gnome-l10n-br@listas.cipsga.org.br (pt_BR)",
	"ru@li.org (ru)",
	"serbiangnome-lista@nongnu.org (sr)",
	"serbiangnome-lista@nongnu.org (sr@Latn)",
	"sv@li.org (sv)",
	NULL
};


/* states */
static gboolean gj_gtk_roster_set_mode_connected (GjRosterWindow *window);
static gboolean gj_gtk_roster_set_mode_disconnected (GjRosterWindow *window);

static gboolean gj_gtk_roster_set_my_presence (GjRosterWindow *window, 
					       GjPresenceType type, 
					       GjPresenceShow show, 
					       const gchar *status, 
					       gint8 priority,
					       gboolean send_it);

static gboolean gj_gtk_roster_set_my_presence_and_save (GjRosterWindow *window,
							GjPresenceType type, 
							GjPresenceShow show, 
							const gchar *status, 
							gint8 priority,
							gboolean send_it);

/* model functions */
static gboolean gj_gtk_roster_model_setup ();
static gboolean gj_gtk_roster_model_populate_columns (GjRosterWindow *window);

static void gj_gtk_roster_model_cell_image_func (GtkTreeViewColumn *tree_column,
						 GtkCellRenderer *cell,
						 GtkTreeModel *model,
						 GtkTreeIter *iter,
						 GjRosterWindow *window);

static void gj_gtk_roster_model_cell_text_func (GtkTreeViewColumn *tree_column,
						GtkCellRenderer *cell,
						GtkTreeModel *model,
						GtkTreeIter *iter,
						GjRosterWindow *window);

static gboolean gj_gtk_roster_model_add (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_model_add_group (GjRosterWindow *window, const gchar *group, GtkTreeIter *iter);
static gboolean gj_gtk_roster_model_add_agent (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_model_add_contact_resource (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_model_add_contact_not_permanent (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_model_add_contact_subscribing (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_model_add_contact_offline (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_model_add_contact_permanent (GjRosterWindow *window, GjRosterItem ri);

static gboolean gj_gtk_roster_model_set_presence (GjRosterWindow *window, 
						  GjRosterItem ri, 
						  GjPresenceType type, 
						  GjPresenceShow show, 
						  const gchar *status);

static gboolean gj_gtk_roster_model_set_message (GjRosterWindow *window, GjRosterItem ri, GjMessageType type);

/* model finds */
static gboolean gj_gtk_roster_model_find_jid (GtkTreeView *view, GjJID jid, GList **iters);
static gboolean gj_gtk_roster_model_find_jid_foreach (GtkTreeModel *model, GtkTreePath *path, 
						      GtkTreeIter *iter, GjRosterWindowFindUser *data);

static gboolean gj_gtk_roster_model_find_group (GtkTreeView *view, const gchar *group, GtkTreeIter *iter);
static gboolean gj_gtk_roster_model_find_group_foreach (GtkTreeModel *model, GtkTreePath *path, 
							GtkTreeIter *iter, GjRosterWindowFindGroup *data);

static void gj_gtk_roster_model_iter_list_destroy_cb (gpointer data, gpointer user_data);

/* model selections */
static GjRosterItem gj_gtk_roster_model_selection_get_ri (GjRosterWindow *window);
static gboolean gj_gtk_roster_model_selection_get_is_group (GjRosterWindow *window, gboolean *is_group, gchar **group);

/* roster contact functions */
static gboolean gj_gtk_roster_contact_read (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_new_message (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_new_chat (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_file_transfer (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_headlines (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_history (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_information (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_sub_request (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_sub_auth (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_sub_cancel (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_edit_groups (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_rename (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_add (GjRosterWindow *window);
static gboolean gj_gtk_roster_contact_remove (GjRosterWindow *window);

/* roster agent functions */
static gboolean gj_gtk_roster_agent_log_in (GjRosterWindow *window);
static gboolean gj_gtk_roster_agent_log_out (GjRosterWindow *window);


/* roster group functions */
static gboolean gj_gtk_roster_group_message (GjRosterWindow *window);
static gboolean gj_gtk_roster_group_rename (GjRosterWindow *window);
static gboolean gj_gtk_roster_group_remove (GjRosterWindow *window);

/* other */
static gboolean gj_gtk_roster_unregister (GjRosterWindow *window);

/* events */
static gboolean gj_gtk_roster_handle_next_event (GjRosterWindow *window, GjRosterItem ri);
/* static gboolean gj_gtk_roster_handle_next_event_display (GjRosterWindow *window, GjRosterItem ri);  */

static gboolean gj_gtk_roster_flash_timeout_add (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_flash_timeout_remove (GjRosterWindow *window, GjRosterItem ri);
static gboolean gj_gtk_roster_flash_timeout_cb (GjRosterWindow *window);
static gboolean gj_gtk_roster_flash_timeout_update_roster (GjRosterWindow *window, GjRosterItem ri);

/* menus */
static gboolean gj_gtk_roster_generate_mnu_user (GjRosterWindow *window, GjRosterItem ri, GdkEventButton *gdk_event);
static gboolean gj_gtk_roster_generate_mnu_agent (GjRosterWindow *window, GjRosterItem ri, GdkEventButton *gdk_event);
static gboolean gj_gtk_roster_generate_mnu_group (GjRosterWindow *window, GdkEventButton *gdk_event);

static gboolean gj_gtk_roster_generate_mnu_notification_tray (gint x, gint y, GjRosterWindow *window);

/* callbacks */
static void gj_gtk_roster_event_cb (gint event_id, GjRosterWindow *window);

static void gj_gtk_roster_get_response (const GList *contacts, 
					const GList *agents, 
					GjRosterWindow *window);
static void gj_gtk_roster_set_response (const GList *updated_roster_items, 
					GjRosterWindow *window);

static void gj_gtk_roster_user_changed_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

static void gj_gtk_roster_autoaway_cb (guint msec, GjRosterWindow *window);

static void gj_gtk_roster_contact_remove_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

static void gj_gtk_roster_unregister_requirements_cb (GjRegistration reg, 
						      gint error_code, 
						      const gchar *error_reason,
						      GjRosterWindow *window);

static void gj_gtk_roster_unregister_cb (GjRegistration reg, 
					 gint error_code, 
					 const gchar *error_reason,
					 GjRosterWindow *window);

/* static void gj_gtk_roster_unregister_cb (GjInfoquery iq, GjInfoquery iq_related, GjRosterWindow *window); */

static void gj_gtk_roster_presence_image_align_cb (GtkMenu *menu, 
						   gint *x, 
						   gint *y, 
						   gboolean *push_in, 
						   GjRosterWindow *window);


/* docklet callbacks */
static void gj_gtk_roster_docklet_left_click_cb (gboolean double_click, 
						 gint x_root, 
						 gint y_root, 
						 GjRosterWindow *window);

static void gj_gtk_roster_docklet_right_click_cb (gboolean double_click, 
						  gint x_root, 
						  gint y_root, 
						  GjRosterWindow *window);


/* menu callbacks  */
static void gj_gtk_roster_menu_contact_read_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_new_message_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_new_chat_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_file_transfer_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_headlines_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_history_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_information_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_sub_request_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_sub_auth_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_sub_cancel_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_edit_groups_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_rename_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_add_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_contact_remove_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_agent_log_in_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_agent_log_out_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_group_message_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_group_rename_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_group_remove_cb (GtkMenuItem *menuitem, GjRosterWindow *window);

static void gj_gtk_roster_menu_presence_cb (GtkMenuItem *menuitem, GjRosterWindow *window);

static void gj_gtk_roster_menu_presence_preset_cb (GtkMenuItem *menuitem, GjRosterWindow *window);

#if 0
static void gj_gtk_roster_presence_preset_menu_cb (GtkMenuItem *menuitem, gpointer user_data);
#endif

static void gj_gtk_roster_menu_show_window_cb (GtkMenuItem *menuitem, GjRosterWindow *window);
static void gj_gtk_roster_menu_quit_cb (GtkMenuItem *menuitem, GjRosterWindow *window);

static void gj_gtk_roster_connect (GjRosterWindow *window);
static void gj_gtk_roster_connect_password_cb (gboolean success, const gchar *password, GjRosterWindow *window);
static gboolean gj_gtk_roster_connect_timeout_cb (GjRosterWindow *window);

static void gj_gtk_roster_connect_start (GjRosterWindow *window, const gchar *password);
static void gj_gtk_roster_connect_start_cb (GjStream stm, gint error_code, gchar *error_message, GjRosterWindow *window);
static void gj_gtk_roster_connect_stop_cb (GjStream stm, gint error_code, gchar *error_message, GjRosterWindow *window);
static void gj_gtk_roster_connect_error_cb (GjStream stm, gint error_code, gchar *error_message, GjRosterWindow *window);

static void gj_gtk_roster_connect_auth_method_cb (GjInfoquery iq, 
						  GjInfoquery iq_related, 
						  GjRosterWindow *window);
static void gj_gtk_roster_connect_auth_cb (GjInfoquery iq, 
					   GjInfoquery iq_related, 
					   GjRosterWindow *window);


static void gj_gtk_roster_connect_cleanup (GjRosterWindow *window);

static void gj_gtk_roster_connect_get_vcard_cb (GjInfoquery iq, GjInfoquery iq_related, GjRosterWindow *window);


/* gui callbacks */
static void on_mnu_jabber_connect_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_disconnect_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_register_new_account_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_unregister_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_change_password_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_preferences_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_connection_settings_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_my_information_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_jabber_quit_activate (GtkMenuItem *menuitem, GjRosterWindow *window);

static void on_mnu_action_add_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_action_send_message_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_action_group_chat_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_action_edit_status_messages_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_action_event_viewer_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_action_directory_search_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_action_browse_services_activate (GtkMenuItem *menuitem, GjRosterWindow *window);

static void on_mnu_help_homepage_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_help_project_information_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_help_log_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_help_debug_activate (GtkMenuItem *menuitem, GjRosterWindow *window);
static void on_mnu_help_about_activate (GtkMenuItem *menuitem, GjRosterWindow *window);

static void on_treeview_row_inserted (GtkTreeModel *treemodel, GtkTreePath *path, GtkTreeIter *iter, GjRosterWindow *window);
static void on_treeview_row_deleted (GtkTreeModel *treemodel, GtkTreePath *path, GjRosterWindow *window);
static gboolean on_treeview_button_press_event (GtkWidget *widget, GdkEventButton *gdk_event, GjRosterWindow *window);
static gboolean on_treeview_button_release_event (GtkWidget *widget, GdkEventButton *gdk_event, GjRosterWindow *window);

static gboolean on_eventbox_statusbar_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window);
/* static gboolean on_eventbox_error_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window); */

static void on_button_error_clicked (GtkButton *button, GjRosterWindow *window);

static void on_eventbox_presence_image_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window);
static void on_eventbox_presence_text_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window);
static void on_entry_status_activate (GtkEntry *entry, GjRosterWindow *window);
static gboolean on_entry_status_focus_out_event (GtkWidget *widget, GdkEventFocus *event, GjRosterWindow *window);
static gboolean on_entry_status_key_release_event (GtkWidget *widget, GdkEventKey *event, GjRosterWindow *window);

static gboolean on_focus_out_event (GtkWidget *widget, GdkEventFocus *event, GjRosterWindow *window);

static void on_destroy (GtkWidget *widget, GjRosterWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjRosterWindow *window);


static GjRoster roster = NULL;
static GjRosterWindow *current_window = NULL;


static gboolean stats_cb (gpointer user_data)
{
	gj_connection_stats ();
	gj_parser_stats ();
	gj_iq_stats ();
	gj_message_stats ();
	gj_presence_stats ();
	gj_roster_stats ();
	gj_event_stats ();

	return FALSE;
}

static void gj_gtk_roster_get_response (const GList *contacts, const GList *agents, GjRosterWindow *window)
{
	gint i = 0;
	gint count = 0;

	if (!window) {
		return;
	}

	for (i=0,count=0; i<g_list_length ((GList*)contacts); i++) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;
		gchar *groups = NULL;

		ri = (GjRosterItem) g_list_nth_data ((GList*)contacts, i);

		if (ri == NULL) {
			continue; 
		}

		groups = gj_roster_item_get_groups_as_string (ri);
		j = gj_roster_item_get_jid (ri);
      
		g_message ("[CONTACT] (%2.2d) jid:'%s', name:'%s', groups:'%s', type:'%d'->'%s', subscription:'%d'->'%s', resource:'%s', agent:'%s'",
			   i,
			   gj_jid_get_full (j),
			   gj_roster_item_get_name (ri),
			   groups?groups:"none",
			   gj_jid_get_type (j),
			   gj_translate_jid_type (gj_jid_get_type (j)),
			   gj_roster_item_get_subscription (ri),
			   gj_translate_roster_item_subscription (gj_roster_item_get_subscription (ri)),
			   gj_jid_get_resource (j)?gj_jid_get_resource (j):"",
			   gj_jid_get_agent (j)?gj_jid_get_agent (j):"");
      
		gj_gtk_roster_model_add (window, ri);
		count++;

		/* clean up */
		g_free (groups);
	}

	g_message ("%s: %d contacts all together", __FUNCTION__, count);

	for (i=0,count=0; i<g_list_length ((GList*)agents); i++) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;
		gchar *groups = NULL;

		ri = (GjRosterItem) g_list_nth_data ((GList*)agents, i);

		if (ri == NULL) {
			continue; 
		}

		groups = gj_roster_item_get_groups_as_string (ri);
		j = gj_roster_item_get_jid (ri);
      
		g_message ("[AGENT] (%2.2d) jid:'%s', name:'%s', groups:'%s', type:'%d'->'%s', subscription:'%d'->'%s', resource:'%s', agent:'%s'",
			   i,
			   gj_jid_get_full (j),
			   gj_roster_item_get_name (ri),
			   groups?groups:"none",
			   gj_jid_get_type (j),
			   gj_translate_jid_type (gj_jid_get_type (j)),
			   gj_roster_item_get_subscription (ri),
			   gj_translate_roster_item_subscription (gj_roster_item_get_subscription (ri)),
			   gj_jid_get_resource (j)?gj_jid_get_resource (j):"",
			   gj_jid_get_agent (j)?gj_jid_get_agent (j):"");
      
		gj_gtk_roster_model_add_agent (window, ri);
		count++;

		/* clean up */
		g_free (groups);
	}

	g_message ("%s: %d agents all together", __FUNCTION__, count);
}

static void gj_gtk_roster_set_response (const GList *updated_items, GjRosterWindow *window)
{
	gint i = 0;

	if (updated_items == NULL) {
		return;
	}

	for (i=0; i<g_list_length ((GList*)updated_items); i++) {
		GjRosterItem ri = NULL;

		ri = (GjRosterItem) g_list_nth_data ((GList*)updated_items, i);

		if (ri == NULL) { 
			continue; 
		}

		gj_gtk_roster_model_remove (ri);
      
		if (gj_roster_item_get_is_set_for_removal (ri) == FALSE) {
			gj_gtk_roster_model_add (window, ri); 
		}
	}
}


/*
 * code
 */
gboolean gj_gtk_roster_load ()
{
	GladeXML *xml = NULL;

	GjRosterWindow *window = NULL;

#ifndef G_OS_WIN32
	GdkPixbuf *pb = NULL;
#endif

	current_window = window = g_new0 (GjRosterWindow, 1);

	window->flash_timeout_id = -1;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "roster",
				     NULL,
				     "roster", &window->window,
				     "treeview", &window->treeview,
				     "menubar", &window->menubar,
				     "mnu_jabber_connect", &window->mnu_jabber_connect,
				     "mnu_jabber_disconnect", &window->mnu_jabber_disconnect,
				     "mnu_jabber_register_new_account", &window->mnu_jabber_register_new_account,
				     "mnu_jabber_unregister", &window->mnu_jabber_unregister,
				     "mnu_jabber_change_password", &window->mnu_jabber_change_password,
				     "mnu_jabber_preferences", &window->mnu_jabber_preferences,
				     "mnu_jabber_connection_settings", &window->mnu_jabber_connection_settings,
				     "mnu_jabber_my_information", &window->mnu_jabber_my_information,
				     "mnu_jabber_quit", &window->mnu_jabber_quit,
				     "mnu_action", &window->mnu_action,
				     "mnu_action_add", &window->mnu_action_add,
				     "mnu_action_send_message", &window->mnu_action_send_message,
				     "mnu_action_group_chat", &window->mnu_action_group_chat,
				     "mnu_action_edit_status_messages", &window->mnu_action_edit_status_messages,
				     "mnu_action_event_viewer", &window->mnu_action_event_viewer,
				     "mnu_action_directory_search", &window->mnu_action_directory_search,
				     "mnu_action_browse_services", &window->mnu_action_browse_services,
				     "mnu_help_homepage", &window->mnu_help_homepage,
				     "mnu_help_project_information", &window->mnu_help_project_information,
				     "mnu_help_log", &window->mnu_help_log,
				     "mnu_help_debug", &window->mnu_help_debug,
				     "mnu_help_about", &window->mnu_help_about,
				     "eventbox_statusbar", &window->eventbox_statusbar,
				     "hbox_status", &window->hbox_status,
				     "image_presence", &window->image_presence,
				     "label_presence", &window->label_presence,
				     "entry_status", &window->entry_status,
				     "hbox_connection", &window->hbox_connection,
				     "label_connection", &window->label_connection,
				     "progressbar_connection", &window->progressbar_connection,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "roster", "delete_event", on_delete_event,
			      "roster", "destroy", on_destroy,
			      "roster", "focus-out-event", on_focus_out_event,
			      "eventbox_statusbar", "button_press_event", on_eventbox_statusbar_button_press_event,
			      "treeview", "button_press_event", on_treeview_button_press_event,
			      "treeview", "button_release_event", on_treeview_button_release_event,
			      "mnu_jabber_connect", "activate", on_mnu_jabber_connect_activate,
			      "mnu_jabber_disconnect", "activate", on_mnu_jabber_disconnect_activate,
			      "mnu_jabber_register_new_account", "activate", on_mnu_jabber_register_new_account_activate,
			      "mnu_jabber_unregister", "activate", on_mnu_jabber_unregister_activate,
			      "mnu_jabber_change_password", "activate", on_mnu_jabber_change_password_activate,
			      "mnu_jabber_preferences", "activate", on_mnu_jabber_preferences_activate,
			      "mnu_jabber_connection_settings", "activate", on_mnu_jabber_connection_settings_activate,
			      "mnu_jabber_my_information", "activate", on_mnu_jabber_my_information_activate,
			      "mnu_jabber_quit", "activate", on_mnu_jabber_quit_activate,
			      "mnu_action_add", "activate", on_mnu_action_add_activate,
			      "mnu_action_send_message", "activate", on_mnu_action_send_message_activate,
			      "mnu_action_group_chat", "activate", on_mnu_action_group_chat_activate,
			      "mnu_action_edit_status_messages", "activate", on_mnu_action_edit_status_messages_activate,
			      "mnu_action_event_viewer", "activate", on_mnu_action_event_viewer_activate,
			      "mnu_action_directory_search", "activate", on_mnu_action_directory_search_activate,
			      "mnu_action_browse_services", "activate", on_mnu_action_browse_services_activate,
			      "mnu_help_homepage", "activate", on_mnu_help_homepage_activate,
			      "mnu_help_project_information", "activate", on_mnu_help_project_information_activate,
			      "mnu_help_log", "activate", on_mnu_help_log_activate,
			      "mnu_help_debug", "activate", on_mnu_help_debug_activate,
			      "mnu_help_about", "activate", on_mnu_help_about_activate,
			      "eventbox_presence_image", "button_press_event", on_eventbox_presence_image_button_press_event,
			      "eventbox_presence_text", "button_press_event", on_eventbox_presence_text_button_press_event,
			      "entry_status", "activate", on_entry_status_activate,
			      "entry_status", "focus_out_event", on_entry_status_focus_out_event,
			      "entry_status", "key_release_event", on_entry_status_key_release_event,
			      NULL);


	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (window->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | 
				    GDK_DECOR_MENU | GDK_DECOR_MINIMIZE | GDK_DECOR_RESIZEH);

#ifndef G_OS_WIN32
	/* set window icon */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_PRESENCE_AVAILABLE);
	gtk_window_set_icon (GTK_WINDOW (window->window), pb);
	g_object_unref (G_OBJECT (pb));
#endif

	/* start flash timer - for events */
	window->flash_timeout_id = g_timeout_add (1000, (GSourceFunc)gj_gtk_roster_flash_timeout_cb, window);

	/* set up roster */
	if (!gj_gtk_roster_model_setup (window)) {
		return FALSE;
	}

	/* 
	 * set callbacks up for messages/presence 
	 */

	/* auto away */
	if (gj_autoaway_init ((GjAutoAwayCallback)gj_gtk_roster_autoaway_cb, window) == FALSE) {
		g_warning ("%s: auto away could not be initialised and so is unavailable", __FUNCTION__); 
	}

	/* events init */
	if (gj_event_init ((GjEventCallback)gj_gtk_roster_event_cb, window) == FALSE) {
		g_warning ("%s: events could not be initialised", __FUNCTION__);
	}

	/*
	 * start external things:
	 *  - event viewer, docklet, etc 
	 */

	/* docklet */
	gj_gtk_docklet_load ((GjDockletClickCallback)gj_gtk_roster_docklet_left_click_cb,
			     (GjDockletClickCallback)gj_gtk_roster_docklet_right_click_cb,
			     (gpointer)window);
  
	gj_gtk_docklet_set_image_from_stock (GJ_STOCK_PRESENCE_UNAVAILABLE);
  
	/* set event viewer up */
	gj_gtk_ev_load (FALSE);

	/* get connection information */
	gj_config_read ();

	/* should we auto connect ? */
	if (gj_config_get_cs_auto_connect_on_startup () == TRUE) {
		gj_gtk_roster_connect (window);
	}

	return TRUE;
}

gboolean gj_gtk_roster_show (gboolean show)
{
	if (!current_window || !current_window->window) {
		return FALSE;
	}

  	if (show) {
		gtk_window_present (GTK_WINDOW (current_window->window));
	} else {
		gtk_widget_hide_all (GTK_WIDGET (current_window->window));
	}

	return TRUE;
}

void gj_gtk_roster_config_updated ()
{
	GjRosterConnection *rc = NULL;

	if (!current_window || !current_window->connection) {
		return;
	}

	rc = current_window->connection;
	
	if (rc->is_logged_in) {
		gj_gtk_roster_model_reload ();
	}
}

const gchar *gj_gtk_roster_config_nick ()
{
	GjRosterConnection *rc = NULL;

	if (!current_window || !current_window->connection) {
		return;
	}

	rc = current_window->connection;
	
	return rc->nick;
}

static gboolean gj_gtk_roster_set_mode_connected (GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GjConnection c = NULL;
	GjRosterConnection *rc = NULL;

	if (!window) {
		return FALSE;
	}

	rc = window->connection;

	roster = gj_roster_new (rc->c, 
				gj_jid_get_full (rc->jid),
				(GjRosterCallback) gj_gtk_roster_get_response, 
				(GjRosterSetCallback) gj_gtk_roster_set_response,
				current_window);

	gj_roster_request (rc->c, roster);

	view = GTK_TREE_VIEW (window->treeview);

	/* set presence */
	gj_gtk_roster_set_my_presence_and_save (window, 
						GjPresenceTypeAvailable, 
						GjPresenceShowNormal, 
						NULL,
						gj_config_get_cs_priority (),
						TRUE);

	/* enable roster */
	gtk_widget_set_sensitive (window->treeview, TRUE);
  
	/* enable menu */
	gtk_widget_set_sensitive (window->mnu_jabber_connect,FALSE);
	gtk_widget_set_sensitive (window->mnu_jabber_disconnect,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_register_new_account,FALSE);
	gtk_widget_set_sensitive (window->mnu_jabber_unregister,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_change_password,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_preferences,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_connection_settings,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_my_information,TRUE);

	gtk_widget_set_sensitive (window->mnu_action, TRUE);
	gtk_widget_set_sensitive (window->mnu_action_event_viewer, TRUE);
	gtk_widget_set_sensitive (window->mnu_action_directory_search, TRUE);
	gtk_widget_set_sensitive (window->mnu_action_browse_services, TRUE);

	gtk_widget_set_sensitive (window->mnu_help_debug, TRUE);

	gtk_widget_show (window->hbox_status);
	gtk_widget_hide (window->hbox_connection);

	if (GTK_BIN (window->mnu_jabber_connect)->child) {
		GtkWidget *child = GTK_BIN (window->mnu_jabber_connect)->child;
		
		/* do stuff with child */
		if (GTK_IS_LABEL (child)) {
			gtk_label_set_text_with_mnemonic (GTK_LABEL (child), _("_Connect"));
		}
	}

	/* get vcard so we know what to use in chat. */
	gj_iq_request_vcard (rc->c,
			     gj_jid_get_full (rc->jid), 
			     (GjInfoqueryCallback)gj_gtk_roster_connect_get_vcard_cb, 
			     window);

	return TRUE;
}

static gboolean gj_gtk_roster_set_mode_connecting (GjRosterWindow *window)
{
	if (!window) {
		return FALSE;
	}

	gtk_widget_set_sensitive (window->mnu_jabber_connect, TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_disconnect, FALSE);
	gtk_widget_set_sensitive (window->mnu_jabber_register_new_account, FALSE);

	gtk_widget_hide (window->hbox_status);
	gtk_widget_show (window->hbox_connection);

	if (GTK_BIN (window->mnu_jabber_connect)->child) {
		GtkWidget *child = GTK_BIN (window->mnu_jabber_connect)->child;
		
		/* do stuff with child */
		if (GTK_IS_LABEL (child)) {
			gtk_label_set_text_with_mnemonic (GTK_LABEL (child), _("Cancel _Connect"));
		}
	}
}

static gboolean gj_gtk_roster_set_mode_disconnected (GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;

	if (!window) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));

	/* clear roster */
	gtk_tree_store_clear (store);

	/* clean up roster */
	gj_roster_free (roster);
	roster = NULL;

	g_timeout_add (1000, stats_cb, NULL);

	/* disable roster */
	gtk_widget_set_sensitive (GTK_WIDGET (view), FALSE);

	/* enable menu */
	gtk_widget_set_sensitive (window->mnu_jabber_connect,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_disconnect,FALSE);
	gtk_widget_set_sensitive (window->mnu_jabber_register_new_account,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_unregister,FALSE);
	gtk_widget_set_sensitive (window->mnu_jabber_change_password,FALSE);
	gtk_widget_set_sensitive (window->mnu_jabber_preferences,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_connection_settings,TRUE);
	gtk_widget_set_sensitive (window->mnu_jabber_my_information,FALSE);

	gtk_widget_set_sensitive (window->mnu_action, FALSE);
	gtk_widget_set_sensitive (window->mnu_action_event_viewer, FALSE);
	gtk_widget_set_sensitive (window->mnu_action_directory_search, FALSE);
	gtk_widget_set_sensitive (window->mnu_action_browse_services, FALSE);

	gtk_widget_set_sensitive (window->mnu_help_debug, FALSE);

	gtk_widget_hide (window->hbox_status);
	gtk_widget_hide (window->hbox_connection);

	if (GTK_BIN (window->mnu_jabber_connect)->child) {
		GtkWidget *child = GTK_BIN (window->mnu_jabber_connect)->child;
		
		/* do stuff with child */
		if (GTK_IS_LABEL (child)) {
			gtk_label_set_text (GTK_LABEL (child), _("Connect"));
		}
	}

	return TRUE;
}

void gj_gtk_roster_set_status_event_error (gboolean is_error)
{
	if (!current_window) {
		return;
	}

	if (is_error == TRUE) {
/* 		gtk_widget_show (GTK_WIDGET (current_window->button_error)); */
	} else {
/* 		gtk_widget_hide (GTK_WIDGET (current_window->button_error)); */
	}
}

/*
 * MODEL functions
 */
static gboolean gj_gtk_roster_model_setup (GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	if (!window) {
		return FALSE; 
	}

	view = GTK_TREE_VIEW (window->treeview);
	selection = gtk_tree_view_get_selection (view);

	/* store */
	store = gtk_tree_store_new (COL_ROSTER_COUNT,
				    GDK_TYPE_PIXBUF, /* image */
				    GDK_TYPE_PIXBUF, /* old image (used for message/presence icon...etc) */
				    G_TYPE_BOOLEAN,  /* boolean for flash */
				    G_TYPE_STRING,   /* text */
				    G_TYPE_STRING,   /* status */
				    G_TYPE_INT,      /* id - roster item id */
				    G_TYPE_STRING,   /* name - used only for groups */
				    G_TYPE_BOOLEAN); /* is group */


	gj_gtk_roster_model_populate_columns (window);
			     
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	g_signal_connect (G_OBJECT (model), "row-inserted", G_CALLBACK (on_treeview_row_inserted), window);
	g_signal_connect (G_OBJECT (model), "row-deleted", G_CALLBACK (on_treeview_row_deleted), window);

	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE); 
	gtk_tree_view_set_search_column (view, COL_ROSTER_NAME);  
	gtk_tree_view_set_rules_hint (view, FALSE); 
	gtk_tree_view_set_headers_visible (view, FALSE);  
	gtk_tree_view_set_headers_clickable (view, FALSE); 
	gtk_tree_view_set_reorderable (view, FALSE);
	gtk_tree_view_columns_autosize (view); 
	/*    gtk_tree_view_expand_all (view);   */

	/* sort list */
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model), COL_ROSTER_TEXT, GTK_SORT_ASCENDING);

	/* clean up */
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

gboolean gj_gtk_roster_model_reload ()
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;

	gint i = 0;
	gint count = 0;

	const GList *contacts = NULL;
	const GList *agents = NULL;

	if (!current_window) {
		return FALSE;
	}

	contacts = gj_roster_get_contacts (roster);
	agents = gj_roster_get_agents (roster);

	view = GTK_TREE_VIEW (current_window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));

	/* clear roster */
	gtk_tree_store_clear (store);
  
	/* populate roster */
	for (i=0,count=0; i<g_list_length ((GList*)contacts); i++) { 
		GjRosterItem ri = NULL;

		ri = (GjRosterItem) g_list_nth_data ((GList*)contacts, i);

		if (ri == NULL) {
			continue; 
		}

		gj_gtk_roster_model_add (current_window, ri);
	}

	for (i=0,count=0; i<g_list_length ((GList*)agents); i++) {
		GjRosterItem ri = NULL;

		ri = (GjRosterItem) g_list_nth_data ((GList*)agents, i);

		if (ri == NULL) {
			continue;
		}

		gj_gtk_roster_model_add (current_window, ri);
	}
  
	return TRUE;
}

static gboolean gj_gtk_roster_model_populate_columns (GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeViewColumn *column = NULL;
	GtkCellRenderer *renderer = NULL;

	if (!window) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);

	/* cell renderer's */
	column = gtk_tree_view_column_new ();

	/* COL_ROSTER_IMAGE */  
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);

	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 (GtkTreeCellDataFunc) gj_gtk_roster_model_cell_image_func,
						 window,
						 NULL);

	/* COL_ROSTER_TEXT */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);

	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 (GtkTreeCellDataFunc) gj_gtk_roster_model_cell_text_func,
						 window,
						 NULL);

	/* insert column */
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);

	return TRUE;
}

static void gj_gtk_roster_model_cell_image_func (GtkTreeViewColumn *tree_column,
						 GtkCellRenderer *cell,
						 GtkTreeModel *model,
						 GtkTreeIter *iter,
						 GjRosterWindow *window)
{
	gboolean not_group = FALSE;
	GdkPixbuf *pb = NULL;

	if (!window) {
		return;
	}
  
	/* is group ? */
	gtk_tree_model_get (model, iter, COL_ROSTER_NOT_GROUP, &not_group, -1);

	if (!not_group) {
		g_object_set (cell, "visible", FALSE, NULL);
		return;
	}

	/* get image */
	gtk_tree_model_get (model, iter, COL_ROSTER_IMAGE, &pb, -1);

	g_object_set (cell, 
		      "pixbuf", pb,
		      "visible", TRUE, 
		      NULL);
  
	/* clean up */
	if (pb) {
		g_object_unref (pb);
	}
}

static void gj_gtk_roster_model_cell_text_func (GtkTreeViewColumn *tree_column,
						GtkCellRenderer *cell,
						GtkTreeModel *model,
						GtkTreeIter *iter,
						GjRosterWindow *window)
{
	PangoAttrList *attr_list = NULL;
	PangoAttribute *attr_color = NULL;
	PangoAttribute *attr_style = NULL;
	PangoAttribute *attr_size = NULL;
 
	GtkTreeSelection *selection = NULL;
	GtkStyle *style = NULL;
	GdkColor colour;

	gboolean not_group = FALSE;
	gchar *text = NULL;
	gchar *status = NULL;
	gchar *str = NULL;
	gint id = 0;

	GjRosterItem ri = NULL;
	GjJID j = NULL;

	if (!window) {
		return;
	}

	/* is group ? */
	gtk_tree_model_get (model, iter, 
			    COL_ROSTER_NOT_GROUP, &not_group, 
			    COL_ROSTER_TEXT, &text,
			    -1);
   
	if (!not_group) {
		g_object_set (cell,
			      "attributes", NULL,
			      "weight", PANGO_WEIGHT_BOLD,
			      "text", text,
			      NULL);

		g_free (text);
		return;
	}
  
	/* must be a contact */
	gtk_tree_model_get (model, iter, 
			    COL_ROSTER_ID, &id,
			    COL_ROSTER_STATUS, &status,
			    -1);

	ri = gj_rosters_find_item_by_id (id);
	j = gj_roster_item_get_jid (ri);

	/* text without status information */
	if (gj_config_get_mpf_hide_contact_presence_details () == TRUE || 
	    (ri == NULL || j == NULL || gj_jid_get_type (j) != GjJIDTypeContact) || 
	    (status == NULL || status[0] == '\0')) {
		g_object_set (cell,
			      "attributes", NULL,
			      "weight", PANGO_WEIGHT_NORMAL,
			      "text", text,
			      NULL);
		
		
		
		g_free (text);
		g_free (status);
		return;
	}

	str = g_strdup_printf ("%s\n%s", text, status);

	style = gtk_widget_get_style (window->treeview);
	colour = style->text_aa[GTK_STATE_NORMAL];
  
	attr_list = pango_attr_list_new ();
  
	attr_style = pango_attr_style_new (PANGO_STYLE_NORMAL);
	attr_style->start_index = strlen (text) + 1;
	attr_style->end_index = -1;
	pango_attr_list_insert (attr_list, attr_style);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (window->treeview));
	if (!gtk_tree_selection_iter_is_selected (selection, iter)) {
		/* selected */
		attr_color = pango_attr_foreground_new (colour.red, colour.green, colour.blue);
		attr_color->start_index = attr_style->start_index;
		attr_color->end_index = -1;
		pango_attr_list_insert (attr_list, attr_color);
	}
  
	attr_size = pango_attr_size_new (pango_font_description_get_size (style->font_desc) / 1.2);
	attr_size->start_index = attr_style->start_index;
	attr_size->end_index = -1;
	pango_attr_list_insert (attr_list, attr_size);
  
	g_object_set (cell,
		      "weight", PANGO_WEIGHT_NORMAL,
		      "text", str,
		      "attributes", attr_list,
		      NULL);

	/* clean up */
	pango_attr_list_unref (attr_list);

	g_free (text);
	g_free (status);
	g_free (str);
}

static gboolean gj_gtk_roster_model_add (GjRosterWindow *window, GjRosterItem ri)
{
	GjPresence pres = NULL;
	GjJID j = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((j = gj_roster_item_get_jid (ri)) == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if ((pres = gj_roster_item_get_presence (ri)) == NULL) {
		g_warning ("%s: could not get presence from roster item with JID:'%s'", __FUNCTION__, gj_jid_get_full (j)); 
		return FALSE;
	}

	/* agents */
	if (gj_jid_get_type (j) == GjJIDTypeAgent) {
		if (gj_config_get_mpf_hide_agents () == TRUE) {
			g_message ("%s: agents not to be shown, not adding jid:'%s'", 
				   __FUNCTION__, gj_jid_get_full (j));
			return FALSE;
		}

		return gj_gtk_roster_model_add_agent (window, ri);
	}

	/* other resources */
	if (gj_roster_item_get_is_another_resource (ri) == TRUE) {
		return gj_gtk_roster_model_add_contact_resource (window, ri);
	}

	/* subscribing contact */
	if (gj_presence_get_type (pres) == GjPresenceTypeSubscribe) {
		return gj_gtk_roster_model_add_contact_subscribing (window, ri);
	}

	/* unknown contacts - was && */
	if (gj_roster_item_get_is_permanent (ri) == FALSE || 
	    gj_roster_item_get_subscription (ri) == GjRosterItemSubscriptionNone) {
		return gj_gtk_roster_model_add_contact_not_permanent (window, ri);
	}

	g_message ("%s: roster item's current presence type:%d->'%s'", 
		   __FUNCTION__, 
		   gj_presence_get_type (pres), 
		   gj_translate_presence_type (gj_presence_get_type (pres)));

	/* offline contacts */
	if (gj_presence_get_type (pres) == GjPresenceTypeUnAvailable || 
	    gj_presence_get_type (pres) == GjPresenceTypeSubscribed || 
	    gj_presence_get_type (pres) == GjPresenceTypeError) {
		return gj_gtk_roster_model_add_contact_offline (window, ri);
	}

	/* all others */
	if (gj_gtk_roster_model_add_contact_permanent (window, ri) == FALSE) {
		return FALSE;
	}
  
	/* should we play sound ? */
	if (gj_config_get_mpf_sound_on_user_online () == TRUE) {
		gj_gtk_sound_play (gj_config_get_mpf_sound_file_on_user_online ());
	}

	return TRUE;
}

static gboolean gj_gtk_roster_model_add_contact_resource (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GtkTreeIter iter_child;

	gchar *group = NULL;    /* ROSTER_GROUP_NOT_ON_ROSTER; */
	GdkPixbuf *pb = NULL;

	GjJID j = NULL;
	GjPresence pres = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__); 
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	pres = gj_roster_item_get_presence (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	/* set pixbuf and group */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_PRESENCE_AVAILABLE);
	group = g_strdup (ROSTER_GROUP_RESOURCE);

	/* find group */
	if (gj_gtk_roster_model_add_group (window, group, &iter) == FALSE) {
		g_free (group);
		g_object_unref (G_OBJECT (pb));
		return FALSE;
	}

	/* add to tree */
	gtk_tree_store_insert (store, &iter_child, &iter, 0);
	gtk_tree_store_set (store, &iter_child,
			    COL_ROSTER_IMAGE, pb,
			    COL_ROSTER_IMAGE_FLASH, NULL,
			    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
			    COL_ROSTER_TEXT, gj_jid_get_resource (j),
			    COL_ROSTER_ID, gj_roster_item_get_id (ri),
			    COL_ROSTER_NAME, gj_roster_item_get_name (ri),
			    COL_ROSTER_NOT_GROUP, TRUE,
			    -1);

	path = gtk_tree_model_get_path (model, &iter_child);
	gtk_tree_model_row_changed (model, path, &iter_child);

	/* clean up */
	g_free (group);
	gtk_tree_path_free (path);
	g_object_unref (G_OBJECT (pb));

	return TRUE;
}

static gboolean gj_gtk_roster_model_add_contact_not_permanent (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GtkTreeIter iter_child;

	gchar *group = NULL;    /* ROSTER_GROUP_NOT_ON_ROSTER; */
	GdkPixbuf *pb = NULL;

	GjPresence pres = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__); 
		return FALSE;
	}

	pres = gj_roster_item_get_presence (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	/* set pixbuf and group */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_PRESENCE_NONE);
	group = g_strdup (ROSTER_GROUP_NOT_ON_ROSTER);

	/* find group */
	if (gj_gtk_roster_model_add_group (window, group, &iter) == FALSE) {
		g_free (group);
		g_object_unref (G_OBJECT (pb));
		return FALSE;
	}

	/* add to tree */
	gtk_tree_store_insert (store, &iter_child, &iter, 0);
	gtk_tree_store_set (store, &iter_child,
			    COL_ROSTER_IMAGE, pb,
			    COL_ROSTER_IMAGE_FLASH, NULL,
			    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
			    COL_ROSTER_TEXT, gj_roster_item_get_name (ri),
			    COL_ROSTER_ID, gj_roster_item_get_id (ri),
			    COL_ROSTER_NAME, gj_roster_item_get_name (ri),
			    COL_ROSTER_NOT_GROUP, TRUE,
			    -1);

	path = gtk_tree_model_get_path (model, &iter_child);
	gtk_tree_model_row_changed (model, path, &iter_child);

	/* clean up */
	g_free (group);
	gtk_tree_path_free (path);
	g_object_unref (G_OBJECT (pb));
      
	return TRUE;
}

static gboolean gj_gtk_roster_model_add_contact_subscribing (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GtkTreeIter iter_child;

	gchar *group = NULL;    /* ROSTER_GROUP_NOT_ON_ROSTER; */
	GdkPixbuf *pb = NULL;

	GjJID j = NULL;
	GjPresence pres = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__); 
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	pres = gj_roster_item_get_presence (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	/* set pixbuf and group */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_PRESENCE_UNAVAILABLE);
	group = g_strdup (ROSTER_GROUP_SUBSCRIBING);

	/* find group */
	if (gj_gtk_roster_model_add_group (window, group, &iter) == FALSE) {
		g_free (group);
		g_object_unref (G_OBJECT (pb));
		return FALSE;
	}

	/* add to tree */
	gtk_tree_store_insert (store, &iter_child, &iter, 0);
	gtk_tree_store_set (store, &iter_child,
			    COL_ROSTER_IMAGE, pb,
			    COL_ROSTER_IMAGE_FLASH, NULL,
			    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
			    COL_ROSTER_TEXT, gj_roster_item_get_name (ri),
			    COL_ROSTER_ID, gj_roster_item_get_id (ri),
			    COL_ROSTER_NAME, gj_roster_item_get_name (ri),
			    COL_ROSTER_NOT_GROUP, TRUE,
			    -1);

	path = gtk_tree_model_get_path (model, &iter_child);
	gtk_tree_model_row_changed (model, path, &iter_child);

	/* clean up */
	g_free (group);
	gtk_tree_path_free (path);
	g_object_unref (G_OBJECT (pb));

	return TRUE;
}

static gboolean gj_gtk_roster_model_add_contact_offline (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GtkTreeIter iter_child;

	gchar *group = ROSTER_GROUP_OFFLINE;
	gchar *group_list = NULL;
	gchar *name = NULL;
	GdkPixbuf *pb = NULL;

	GjPresence pres = NULL;
	GjJID j = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__); 
		return FALSE;
	}

	pres = gj_roster_item_get_presence (ri);
	j = gj_roster_item_get_jid (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);


	/* if hide offline contacts do not show */ 
	/* if hide offline group do not show */
	/* if show offline group put contacts in offline group */
	/* if show offline contacts and hide offline group show as normal */

	/*
	  if hide offline contacts and hide offline group return
	  if hide offline contacts and show offline group SHOULD NOT GET HERE. - so return
	  if show offline contacts and hide offline group call gj_gtk_roster_model_add_contact_permanent
	  if show offline contacts and show offline group continue
	*/

	if (gj_config_get_mpf_hide_offline_contacts () == TRUE && gj_config_get_mpf_hide_offline_group () == TRUE) {
		g_message ("%s: hiding offline contacts, hiding offline group, not adding jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return FALSE;
	}

	if (gj_config_get_mpf_hide_offline_contacts () == TRUE && gj_config_get_mpf_hide_offline_group () == FALSE) {
		g_message ("%s: hiding offline contacts, showing offline group, not adding jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return FALSE;
	}

	if (gj_config_get_mpf_hide_offline_contacts () == FALSE && gj_config_get_mpf_hide_offline_group () == TRUE) {
		g_message ("%s: offline group hidden, showing offline contacts, adding permanent contact jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return gj_gtk_roster_model_add_contact_permanent (window, ri);
	}

	/* find group */
	if (gj_gtk_roster_model_add_group (window, group, &iter) == FALSE) {
		return FALSE;
	}
 
	/* set pixbuf */
	pb = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));

	/* set name - include groups */
	group_list = gj_roster_item_get_groups_as_string (ri);
	name = g_strdup_printf ("%s %s%s%s",
				gj_roster_item_get_name (ri),
				group_list?"[":"",
				group_list?group_list:"",
				group_list?"]":"");
  
	g_free (group_list);

	/* add to tree */
	gtk_tree_store_insert (store, &iter_child, &iter, 0);
	gtk_tree_store_set (store, &iter_child,
			    COL_ROSTER_IMAGE, pb,
			    COL_ROSTER_IMAGE_FLASH, NULL,
			    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
			    COL_ROSTER_TEXT, name,
			    COL_ROSTER_ID, gj_roster_item_get_id (ri),
			    COL_ROSTER_NAME, name,
			    COL_ROSTER_NOT_GROUP, TRUE, 
			    -1);

	path = gtk_tree_model_get_path (model, &iter_child);
	gtk_tree_model_row_changed (model, path, &iter_child);

	/* clean up */
	if (path != NULL) {
		gtk_tree_path_free (path); 
	}

	if (pb != NULL) {
		g_object_unref (G_OBJECT (pb));
	}
      
	return TRUE;
}

static gboolean gj_gtk_roster_model_add_contact_permanent (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter_child;
  
	gint i = 0;

	GjPresence pres = NULL;
	GjJID j = NULL;

	if (!window) { 
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__); 
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	pres = gj_roster_item_get_presence (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	g_message ("%s: contact:'%s' belongs to %d groups", 
		   __FUNCTION__, gj_jid_get_full (j), g_list_length (gj_roster_item_get_groups (ri)));
  
	/* loop through and add contact for each group they belong to */
	for (i=0;gj_roster_item_get_groups (ri) != NULL && i < g_list_length (gj_roster_item_get_groups (ri));i++) {
		GtkTreeIter iter;
		
		GdkPixbuf *pb = NULL;
		gchar *group = NULL;
		gchar *name = NULL;
		gchar *status = NULL;
		
		group = (gchar*) g_list_nth_data (gj_roster_item_get_groups (ri), i); 
		g_message ("%s: looking for group:'%s', index:%d", __FUNCTION__, group, i);
		
		/* find group */
		if (gj_gtk_roster_model_add_group (window, group, &iter) == FALSE) {
			return FALSE;
		}
		
		/* pixbuf */
		pb = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));
		
		name = g_strdup (gj_roster_item_get_name (ri));
		
		if ((gj_presence_get_type (pres) == GjPresenceTypeAvailable ||
		     gj_presence_get_type (pres) == GjPresenceTypeUnAvailable ||
		     gj_presence_get_type (pres) == GjPresenceTypeNone) && gj_presence_get_status (pres)) {
			status = g_strdup (gj_presence_get_status (pres));
		} else { 
			status = gj_gtk_presence_to_status_text_new (gj_presence_get_type (pres), 
								     gj_presence_get_show (pres));
		}
		
		/* add to store */
		gtk_tree_store_insert (store, &iter_child, &iter, 0);
		
		/* set store */
		gtk_tree_store_set (store, &iter_child,
				    COL_ROSTER_IMAGE, pb,
				    COL_ROSTER_IMAGE_FLASH, NULL,
				    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
				    COL_ROSTER_TEXT, name,
				    COL_ROSTER_STATUS, status,
				    COL_ROSTER_ID, gj_roster_item_get_id (ri),
				    COL_ROSTER_NAME, name,
				    COL_ROSTER_NOT_GROUP, TRUE,
				    -1);
		
		path = gtk_tree_model_get_path (model, &iter_child);
		gtk_tree_model_row_changed (model, path, &iter_child);
		
		/* clean up */
		gtk_tree_path_free (path);
		g_object_unref (G_OBJECT (pb));
		g_free (name);
		g_free (status);
		
		path = NULL;
	}
	
	/*
	  NOTE: this section is used for contacts which do not have a group
	  and although this is totally plausable, future considerations 
	  may involve contacts having a default pool/group when they do 
	  not already belong to one.
	*/
	if (gj_roster_item_get_groups (ri) == NULL) {
		GdkPixbuf *pb = NULL;
		gchar *name = NULL;
		gchar *status = NULL;

		g_message ("%s: contact:'%s' has no group", __FUNCTION__, gj_jid_get_full (j));

		/* get image */
		pb = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));

		name = g_strdup (gj_roster_item_get_name (ri));
		if ((gj_presence_get_type (pres) == GjPresenceTypeAvailable ||
		     gj_presence_get_type (pres) == GjPresenceTypeUnAvailable ||
		     gj_presence_get_type (pres) == GjPresenceTypeNone) && gj_presence_get_status (pres)) {
			status = g_strdup (gj_presence_get_status (pres)); 
		} else {
			status = gj_gtk_presence_to_status_text_new (gj_presence_get_type (pres), 
								     gj_presence_get_show (pres));
		}

		/* add to store */
		gtk_tree_store_insert (store, &iter_child, NULL, 0);
		gtk_tree_store_set (store, &iter_child,
				    COL_ROSTER_IMAGE, pb,
				    COL_ROSTER_IMAGE_FLASH, NULL,
				    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
				    COL_ROSTER_TEXT, name!=NULL?name:gj_roster_item_get_name (ri),
				    COL_ROSTER_STATUS, status,
				    COL_ROSTER_ID, gj_roster_item_get_id (ri),
				    COL_ROSTER_NAME, name!=NULL?name:gj_roster_item_get_name (ri),
				    COL_ROSTER_NOT_GROUP, TRUE,
				    -1);

		path = gtk_tree_model_get_path (model, &iter_child);
		gtk_tree_model_row_changed (model, path, &iter_child);

		/* clean up */
		gtk_tree_path_free (path);
		g_object_unref (G_OBJECT (pb));
		g_free (name);
		g_free (status);
	}

	return TRUE;
}

static gboolean gj_gtk_roster_model_add_group (GjRosterWindow *window, const gchar *group, GtkTreeIter *iter)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;

	gchar *s1 = NULL;
	gchar *markup = NULL;

	if (!window) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	if (group == NULL) {
		g_warning ("%s: group was NULL", __FUNCTION__);
		return FALSE;
	}

	if (iter == NULL) {
		g_warning ("%s: iter was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_gtk_roster_model_find_group (view, group, iter) == TRUE) {
		g_message ("%s: group:'%s' already exists", __FUNCTION__, group); 
		return TRUE;
	} 

	s1 = g_markup_escape_text (group, -1);
	markup = g_strdup_printf ("%s", s1);
      
	/* add contact to store */
	gtk_tree_store_insert (store, iter, NULL, 0);
	gtk_tree_store_set (store, iter,
			    COL_ROSTER_IMAGE, NULL, /* pb */
			    COL_ROSTER_IMAGE_FLASH, NULL,
			    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
			    COL_ROSTER_TEXT, markup,
			    COL_ROSTER_ID, -1,
			    COL_ROSTER_NAME, s1,
			    COL_ROSTER_NOT_GROUP, FALSE,
			    -1);

	path = gtk_tree_model_get_path (model, iter);
	gtk_tree_model_row_changed (model, path, iter);

	/* clean up */
	gtk_tree_path_free (path);
	g_free (s1);
	g_free (markup);

	return TRUE;
}

static gboolean gj_gtk_roster_model_add_agent (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GtkTreeIter iter_child;

	gchar *group = ROSTER_GROUP_AGENTS;
	GdkPixbuf *pb = NULL;

	GjPresence pres = NULL;
	GjJID j = NULL;

	if (!window) {
		return FALSE;
	}
  
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__); 
		return FALSE;
	}
  
	j = gj_roster_item_get_jid (ri);
	pres = gj_roster_item_get_presence (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	if (gj_config_get_mpf_hide_agents () == TRUE) {
		g_message ("%s: hiding offline agents, not adding jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return FALSE;
	}

	/* find group */
	if (gj_gtk_roster_model_add_group (window, group, &iter) == FALSE) {
		return FALSE;
	}
 
	/* set pixbuf */
	pb = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));

	/* add to tree */
	gtk_tree_store_insert (store, &iter_child, &iter, 0);
	gtk_tree_store_set (store, &iter_child,
			    COL_ROSTER_IMAGE, pb,
			    COL_ROSTER_IMAGE_FLASH, NULL,
			    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
			    COL_ROSTER_TEXT, gj_roster_item_get_name (ri),
			    COL_ROSTER_ID, gj_roster_item_get_id (ri),
			    COL_ROSTER_NAME, gj_roster_item_get_name (ri),
			    COL_ROSTER_NOT_GROUP, TRUE, 
			    -1);

	path = gtk_tree_model_get_path (model, &iter_child);
	gtk_tree_model_row_changed (model, path, &iter_child);

	/* clean up */
	if (path != NULL) {
		gtk_tree_path_free (path);
	}

	if (pb != NULL) {
		g_object_unref (G_OBJECT (pb));
	}

	return TRUE;
}

gboolean gj_gtk_roster_model_remove (GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeStore *store = NULL;

	GList *iter_list = NULL;
	gint i = 0;

	GjJID j = NULL;

	if (!current_window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	view = GTK_TREE_VIEW (current_window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	/* NOTE: at this point, we need to continue searching
	   the roster for other copies of the same JID for different
	   groups. */

	if (gj_gtk_roster_model_find_jid (view, gj_roster_item_get_jid (ri), &iter_list) == FALSE) {
		g_message ("%s: could not find jid:'%s' on roster", __FUNCTION__, gj_jid_get_full (j));
		return FALSE;
	}

	for (i=0; i<g_list_length (iter_list); i++) {
		GtkTreeIter *iter = NULL;
		GtkTreeIter parent_iter;

		gint children = 0;
		gboolean has_parent = FALSE;

		iter = (GtkTreeIter*) g_list_nth_data (iter_list, i);

		if (!iter) {
			continue;
		}
      
		/* get parent and children info */
		if ((has_parent = gtk_tree_model_iter_parent (model, &parent_iter, iter)) == TRUE) {
			children = gtk_tree_model_iter_n_children (model, &parent_iter);
		}

		if (has_parent == TRUE && children <= 1) {
			gtk_tree_store_remove (store, &parent_iter);
		} else { 
			gtk_tree_store_remove (store, iter);
		}
	}

	g_list_foreach (iter_list, gj_gtk_roster_model_iter_list_destroy_cb, NULL);
	g_list_free (iter_list);

	return TRUE;
}

static gboolean gj_gtk_roster_model_find_jid (GtkTreeView *view, GjJID jid, GList **iters)
{
	GtkTreeModel *model = NULL;
	GjRosterWindowFindUser data;
  
	if (view == NULL) {
		g_warning ("%s: view was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	data.r = NULL;
	data.ri = NULL;
	data.found = FALSE;
	data.jid = jid;
	data.iters = NULL;
  
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));
  
	gtk_tree_model_foreach (model,
				(GtkTreeModelForeachFunc) gj_gtk_roster_model_find_jid_foreach,
				&data);
  
	if (data.found == TRUE) {
		*iters = data.iters;
		return TRUE;
	}
  
	return FALSE;
}

static gboolean gj_gtk_roster_model_find_jid_foreach (GtkTreeModel *model, GtkTreePath *path, 
						      GtkTreeIter *iter, GjRosterWindowFindUser *data)
{
	gint id = -1;
	GjRosterItem ri = NULL;

	gtk_tree_model_get (model, iter, COL_ROSTER_ID, &id, -1);

	if (id == -1) {
		return FALSE;
	}

	ri = gj_rosters_find_item_by_id (id);

	if (ri == NULL) {
		return FALSE;
	}

	if (data->jid && gj_jid_equals (data->jid, gj_roster_item_get_jid (ri)) == TRUE) {
		data->found = TRUE;
		data->iters = g_list_append (data->iters, gtk_tree_iter_copy (iter));

		/* we would return TRUE here to stop, but there may be more ... */
	}
  
	return FALSE;
}

static gboolean gj_gtk_roster_model_find_group (GtkTreeView *view, const gchar *group, GtkTreeIter *iter)
{
	GtkTreeModel *model = NULL;
	GjRosterWindowFindGroup data;
  
	if (view == NULL) {
		g_warning ("%s: view was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (group == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}
  
	data.r = NULL;
	data.group = (gchar*)group;
	data.found = FALSE;
	/*   data.found_iter = NULL; */
  
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (view));
  
	gtk_tree_model_foreach (model,
				(GtkTreeModelForeachFunc) gj_gtk_roster_model_find_group_foreach,
				&data);
  
	if (data.found == TRUE) {
		*iter = data.found_iter;
		return TRUE;
	}
  
	return FALSE;
}

static gboolean gj_gtk_roster_model_find_group_foreach (GtkTreeModel *model, GtkTreePath *path, 
							GtkTreeIter *iter, GjRosterWindowFindGroup *data) 
{
	gboolean not_group = TRUE;
	gchar *group = NULL;

	gtk_tree_model_get (model, iter, 
			    COL_ROSTER_NOT_GROUP, &not_group, 
			    COL_ROSTER_NAME, &group,
			    -1);
  
	if (group == NULL) {
		return FALSE; 
	}

	if (not_group) {
		g_free (group);
		return FALSE;
	}

	if (data->group && g_strcasecmp (data->group, group) == 0) {
		data->found = TRUE;
		data->found_iter = *iter;
		return TRUE;
	}
  
	return FALSE;
}

static void gj_gtk_roster_model_iter_list_destroy_cb (gpointer data, gpointer user_data)
{
	GtkTreeIter *iter = NULL;

	iter = (GtkTreeIter*) data;

	if (iter == NULL) {
		return;
	}
  
	gtk_tree_iter_free (iter);
}

static gboolean gj_gtk_roster_model_selection_get_is_group (GjRosterWindow *window, gboolean *not_group, gchar **group)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;

	if (!window) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	model = gtk_tree_view_get_model (view);
	selection = gtk_tree_view_get_selection (view);

	if (not_group == NULL || group == NULL) {
		g_warning ("%s: is_group or group pointers passed were NULL", __FUNCTION__);
		return FALSE;
	}

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return FALSE;
	}
  
	gtk_tree_model_get (model, &iter, 
			    COL_ROSTER_NAME, group,
			    COL_ROSTER_NOT_GROUP, not_group, 
			    -1);     
	return TRUE;
}

static GjRosterItem gj_gtk_roster_model_selection_get_ri (GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;

	gint id = 0;
 
	GjRosterItem ri = NULL;

	if (!window) {
		return NULL;
	}

	view = GTK_TREE_VIEW (window->treeview);
	model = gtk_tree_view_get_model (view);
	selection = gtk_tree_view_get_selection (view);

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE)
		return NULL;
  
	gtk_tree_model_get (model, &iter, COL_ROSTER_ID, &id, -1);     
	ri = gj_rosters_find_item_by_id (id);

	return ri;
}

/* 
 * ROSTER properties
 */
static gboolean gj_gtk_roster_model_set_presence (GjRosterWindow *window,
						  GjRosterItem ri, 
						  GjPresenceType type, 
						  GjPresenceShow show, 
						  const gchar *status)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeStore *store = NULL;

	GList *iter_list = NULL;

	gboolean ignore_presence = FALSE;

	GdkPixbuf *pb = NULL;
	gchar *name = NULL;
	gchar *name_status = NULL;
	gint i = 0;

	GjJID j = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) { 
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	/* this statement should only be true when removing an agent.
	   for some reason, the presence of the agent is sent AFTER 
	   you request to remove it and your subscription from the roster */
	if (type == GjPresenceTypeUnAvailable && status != NULL && strcmp (status,"Unregistered") == 0 &&
	    gj_roster_item_get_subscription (ri) == GjRosterItemSubscriptionNone &&
	    gj_roster_item_get_is_permanent (ri) == FALSE) {
		ignore_presence = TRUE;
	}
	
	/* the event viewer handles presence errors */
	if (type == GjPresenceTypeError)
		ignore_presence = TRUE;

	/* throw this call out if conditions are met */
	if (ignore_presence == TRUE) {
		g_message ("%s: presence type was error or presence was for "
			   "a roster item just removed, not adding to the "
			   "roster, ignoring this event", 
			   __FUNCTION__);
		return TRUE;
	}

	/*
	 * do we keep it or remove it? 
	 */
	if (type == GjPresenceTypeUnAvailable) {
		gint remove = 0;

		if (gj_jid_get_type (j) == GjJIDTypeAgent && gj_config_get_mpf_hide_agents () == TRUE) {
			remove += 1;
		}
      
		if (gj_roster_item_get_is_permanent (ri) == TRUE && 
		    gj_roster_item_get_subscription (ri) != GjRosterItemSubscriptionNone) {
			remove += 1;
		}

		if (gj_roster_item_get_is_another_resource (ri) == TRUE) {
			remove += 1;
		}
    
		/* if hide offline contacts remove from list and return */
		/* if show offline contacts and show offline group, remove and add again and return */
		/* if show offline contacts and hide offline group, remove from list and continue */

		/* agents remain constantly there UNLESS they are hidden 
		   in preferences - but then if that was the case, we wouldnt
		   have to worry about it being removed anyway. */
		if (gj_jid_get_type (j) == GjJIDTypeContact && remove > 0) {
			g_message ("%s: removing jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
			gj_gtk_roster_model_remove (ri);

			if (gj_roster_item_get_is_another_resource (ri) == TRUE) {
				g_message ("%s: removed resource from the roster... no further action needed", 
					   __FUNCTION__);
				return TRUE;
			} else if (gj_config_get_mpf_hide_offline_contacts () == FALSE && 
				   gj_config_get_mpf_hide_offline_group () == FALSE) {
				g_message ("%s: re-adding jid:'%s', into offline group", 
					   __FUNCTION__, gj_jid_get_full (j));
				gj_gtk_roster_model_add (window, ri);
				return TRUE;
			} else if (gj_config_get_mpf_hide_offline_group () == TRUE) {
				/* continue */
				g_message ("%s: continuing to set presence for jid:'%s'", 
					   __FUNCTION__, gj_jid_get_full (j));
			} else {
				return TRUE;
			}
		}
	} else {
		/* if hide offline contacts do nothing, contact will be added */
		/* if show offline contacts and show offline group remove contact so it will be re-added */
		/* if show offline contacts and hide offline group do nothing, contact will be updated */
		
		if (gj_config_get_mpf_hide_offline_contacts () == FALSE && 
		    gj_config_get_mpf_hide_offline_group () == FALSE &&
		    gj_jid_get_type (j) == GjJIDTypeContact) {
			GjPresence last_pres = NULL;
			GjPresenceType last_type = GjPresenceTypeEnd;
			
			last_pres = gj_roster_item_get_last_presence (ri);
			last_type = gj_presence_get_type (last_pres);
			
			if ((last_type == GjPresenceTypeUnAvailable && type == GjPresenceTypeAvailable) || 
			    (last_type == GjPresenceTypeSubscribed)) {
				g_message ("%s: removing jid:'%s' from offline group, item will be re-added.", 
					   __FUNCTION__, gj_jid_get_full (j));
				gj_gtk_roster_model_remove (ri);
			}
		}
	}
	
	if (gj_gtk_roster_model_find_jid (view, gj_roster_item_get_jid (ri), &iter_list) == FALSE) {
		g_message ("%s: jid:'%s' is not on the roster, adding...", __FUNCTION__, gj_jid_get_full (j));
		if (gj_gtk_roster_model_add (window, ri) == FALSE) {
			return FALSE;
		}
		
		if (gj_gtk_roster_model_find_jid (view, gj_roster_item_get_jid (ri), &iter_list) == FALSE) {
			return FALSE;
		}
	}

	/* get new text/image */
	if (gj_roster_item_get_is_another_resource (ri) == TRUE) {
		name = g_strdup (gj_jid_get_resource (j));
	} else {
		name = g_strdup (gj_roster_item_get_name (ri));
	}
  
	if ((type == GjPresenceTypeAvailable ||
	     type == GjPresenceTypeUnAvailable ||
	     type == GjPresenceTypeNone) && status) {
		name_status = g_strdup (status);
	} else {
		name_status = gj_gtk_presence_to_status_text_new (type, show);
	}
  
	if (gj_roster_item_get_subscription (ri) != GjRosterItemSubscriptionNone) {
		pb = gj_gtk_presence_to_pixbuf_new (type, show);
	} else {
		pb = gj_gtk_presence_to_pixbuf_new (GjPresenceTypeNone, 0);
	}

	/* NOTE: at this point, we need to continue searching
	   the roster for other copies of the same JID for different
	   groups. */

	for (i=0; i<g_list_length (iter_list); i++) {
		GtkTreeIter *iter = NULL;
		GtkTreePath *path = NULL;

		gchar *original_name = NULL;
      
		iter = (GtkTreeIter*) g_list_nth_data (iter_list, i);

		gtk_tree_model_get (model, iter, COL_ROSTER_NAME, &original_name, -1);
		gtk_tree_store_set (store, iter,
				    COL_ROSTER_IMAGE, pb,
				    COL_ROSTER_IMAGE_FLASH, NULL,
				    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
				    COL_ROSTER_TEXT, name == NULL ? original_name : name,
				    COL_ROSTER_STATUS, name_status,
				    -1);
	  
		path = gtk_tree_model_get_path (model, iter);
		gtk_tree_model_row_changed (model, path, iter);

		gtk_tree_path_free (path);
		g_free (original_name);
	}

	g_list_foreach (iter_list, gj_gtk_roster_model_iter_list_destroy_cb, NULL);
	g_list_free (iter_list);

	/* clean up */
	g_free (name);
	g_free (name_status);
	g_object_unref (G_OBJECT (pb));

	return TRUE;
}

gboolean gj_gtk_roster_model_set_message (GjRosterWindow *window, GjRosterItem ri, GjMessageType type)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeStore *store = NULL;

	GList *iter_list = NULL;
	gint i = 0;

	GdkPixbuf *pb = NULL;

	GjJID j = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	/* get image */
	pb = gj_gtk_message_to_pixbuf_new (type);

	if (gj_gtk_roster_model_find_jid (view, gj_roster_item_get_jid (ri), &iter_list) == FALSE) {
		g_message ("%s: could not find jid:'%s' on roster", __FUNCTION__, gj_jid_get_full (j));

		if (gj_gtk_roster_model_add (window, ri) == FALSE) {
			return FALSE;
		}
	}

	for (i=0; i<g_list_length (iter_list); i++) {
		GtkTreeIter *iter = NULL;
		GtkTreePath *path = NULL;
      
		iter = (GtkTreeIter*) g_list_nth_data (iter_list, i);

		gtk_tree_store_set (store, iter,
				    COL_ROSTER_IMAGE_FLASH, pb,
				    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
				    -1);
      
		path = gtk_tree_model_get_path (model, iter);
		gtk_tree_model_row_changed (model, path, iter);
      
		if (path != NULL) {
			gtk_tree_path_free (path); 
		}
	}

	g_list_foreach (iter_list, gj_gtk_roster_model_iter_list_destroy_cb, NULL);
	g_list_free (iter_list);

	if (pb != NULL) {
		g_object_unref (G_OBJECT (pb)); 
	}

	return TRUE;
}

static gboolean gj_gtk_roster_set_my_presence (GjRosterWindow *window,
					       GjPresenceType type, 
					       GjPresenceShow show, 
					       const gchar *status, 
					       const gint8 priority,
					       gboolean send_it)
{
	if (!window) {
		return FALSE;
	}

	gtk_image_set_from_stock (GTK_IMAGE (window->image_presence), 
				  gj_gtk_presence_to_stock_id (type, show), 
				  GTK_ICON_SIZE_BUTTON);

	if (status != NULL) {
		gchar *s1 = NULL;
		gchar *s2 = NULL;
		gchar *s3 = NULL;
		gchar *text = NULL;

		s1 = gj_gtk_presence_to_show_text_new (type, show);

		if (s1 != NULL) {
			s2 = g_markup_escape_text (s1, -1);
		}

		if (status != NULL) {
			s3 = g_markup_escape_text (status, -1);
		}

		if (s3 == NULL) {
			s3 = gj_gtk_presence_to_status_text_new (type, show); 
		}
      
		text = g_strdup_printf (ROSTER_CONTACT_PRESENCE_DETAILS_FORMAT, s2, s3==NULL?"":s3);
		gtk_label_set_markup (GTK_LABEL (window->label_presence), text);
      
		g_free (s1);
		g_free (s2);
		g_free (s3);
		g_free (text);
	} else {
		gchar *text = NULL;
		text = gj_gtk_presence_to_status_text_new (type, show);
		gtk_label_set_markup (GTK_LABEL (window->label_presence), text);
		g_free (text);
	}
			     
	/* send the status */
	if (send_it) {
		gj_presence_send (window->connection->c,
				  type, 
				  show, 
				  status, 
				  priority);
	}

	return TRUE;
}

static gboolean gj_gtk_roster_set_my_presence_and_save (GjRosterWindow *window,
							GjPresenceType type, 
							GjPresenceShow show, 
							const gchar *status, 
							gint8 priority,
							gboolean send_it)
{
	gchar *status_dup = NULL;

	if (gj_gtk_roster_set_my_presence (window, type, show, status, priority, send_it) == FALSE) {
		g_warning ("%s: not saving config, setting presence failed", __FUNCTION__);
		return FALSE;
	}

	status_dup = g_strdup (status);

	/* set docklet */
	gj_gtk_docklet_set_image_from_stock (gj_gtk_presence_to_stock_id (type, show));

	/* remember details */
	window->connection->presence_type = type;
	window->connection->presence_show = show;

	g_free (window->connection->presence_status);
	window->connection->presence_status = status_dup;  

	window->connection->presence_priority = priority;  

	return TRUE;
}

static void gj_gtk_roster_autoaway_cb (guint msec, GjRosterWindow *window)
{
	if (!window || !window->connection || !window->connection->is_logged_in) {
		return;
	}

	if (msec > (60 * 1000)) {
		/* set to autoaway mode */
		window->connection->is_auto_away = TRUE;
	  
		/* check the options */
		if (gj_config_get_mpf_use_auto_away () == TRUE &&
		    (msec/1000) >= (gj_config_get_mpf_auto_away_after () * 60) && 
		    (msec/1000) < (gj_config_get_mpf_auto_xa_after () * 60) && 
		    window->connection->auto_away_presence_show != GjPresenceShowAway && 
		    window->connection->auto_away_presence_show != GjPresenceShowXA) {

			g_message ("%s: setting presence to automatically be away, "
				   "idle time: %d seconds, configured auto away "
				   "time:%d minutes", 
				   __FUNCTION__, 
				   msec / 1000, 
				   gj_config_get_mpf_auto_away_after ());
			
			/* set to away */
			gj_gtk_roster_set_my_presence (window, 
						       GjPresenceTypeAvailable,
						       GjPresenceShowAway,
						       gj_config_get_mpf_auto_away_status (),
						       gj_config_get_cs_priority (),
						       TRUE);
			
			/* set state */
			window->connection->auto_away_presence_show = GjPresenceShowAway;
		} else if (gj_config_get_mpf_use_auto_away () == TRUE && 
			   (msec/1000) >= (gj_config_get_mpf_auto_xa_after () * 60) && 
			   window->connection->auto_away_presence_show != GjPresenceShowXA) {

			g_message ("%s: setting presence to automatically be xa, "
				   "idle time: %d seconds, configured auto xa "
				   "time:%d minutes", 
				   __FUNCTION__, 
				   msec / 1000, 
				   gj_config_get_mpf_auto_xa_after ());
			
			/* set to xa */
			gj_gtk_roster_set_my_presence (window,
						       GjPresenceTypeAvailable,
						       GjPresenceShowXA,
						       gj_config_get_mpf_auto_xa_status (),
						       gj_config_get_cs_priority (),
						       TRUE);
			
			/* set state */
			window->connection->auto_away_presence_show = GjPresenceShowXA;
		}
	} else {
		if (window->connection->is_auto_away == TRUE) {
			GjPresenceShow previous_presence_show = 0;
			GjPresenceType previous_presence_type = 0;
			const gchar *previous_presence_status = NULL;
			
			g_message ("%s: restoring presence to original state.", __FUNCTION__);
			
			/* change presence */
			/* what was it before?? - check the store? */
			previous_presence_show = window->connection->presence_show;
			previous_presence_type = window->connection->presence_type;
			previous_presence_status = window->connection->presence_status;
			
			/* set to normal */
			gj_gtk_roster_set_my_presence (window,
						       previous_presence_type,
						       previous_presence_show,
						       previous_presence_status,
						       gj_config_get_cs_priority (),
						       TRUE);
			
			/* set to autoaway mode */
			window->connection->is_auto_away = FALSE;
			window->connection->auto_away_presence_show = previous_presence_show;
		}
	}
}

/* roster contact functions */
static gboolean gj_gtk_roster_contact_read (GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;

	GjRosterItem ri = NULL;

	gint id = 0;

	if (!window) {
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	model = gtk_tree_view_get_model (view);
	selection = gtk_tree_view_get_selection (view);

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return FALSE;
	}
  
	gtk_tree_model_get (model, &iter, COL_ROSTER_ID, &id, -1);      
	ri = gj_rosters_find_item_by_id (id);
  
	gj_gtk_roster_handle_next_event (window, ri);
	gj_gtk_roster_handle_next_event_display (ri);
  
	return TRUE;
}

static gboolean gj_gtk_roster_contact_new_message (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	gj_gtk_message_send_new (window->connection->c,
				 window->connection->jid, 
				 ri); 
	return TRUE;
}

static gboolean gj_gtk_roster_contact_new_chat (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;

	if (!window) {
		return FALSE; 
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	gj_gtk_chat_send_new (window->connection->c, 
			      window->connection->jid, 
			      ri);
	return TRUE;
}

static gboolean gj_gtk_roster_contact_file_transfer (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;

	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}
  
	/*   gj_file_transfer_si_init_send (ri, NULL, NULL); */
	gj_file_transfer_start (window->connection->c, ri, NULL, NULL);
	return TRUE;
}

static gboolean gj_gtk_roster_contact_headlines (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;

	if (!window) {
		return FALSE; 
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) { 
		return FALSE;
	}

	if (gj_gtk_hw_load_by_ri (ri) == FALSE) {
		GtkWidget *dialog = NULL;

		dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_INFO,
						GTK_BUTTONS_OK,
						_("No Headlines."),
						_("There are no headlines for this user."),
						NULL);

		g_signal_connect_swapped (GTK_OBJECT (dialog), "response",
					  G_CALLBACK (gtk_widget_destroy),
					  GTK_OBJECT (dialog));

		gtk_widget_show_all (dialog);
	}
  
	return TRUE;
}

static gboolean gj_gtk_roster_contact_history (GjRosterWindow *window)
{
	GjHistory hist = NULL;
	GjRosterItem ri = NULL;

	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	if ((hist = gj_history_find (gj_roster_item_get_id (ri))) == NULL) { 
		g_warning ("%s: could not find history based on roster item id:%d", __FUNCTION__, gj_roster_item_get_id (ri));
		return FALSE;
	}
  
	gj_gtk_open_url (gj_history_get_url (hist));
	return TRUE;
}

static gboolean gj_gtk_roster_contact_information (GjRosterWindow *window) {
	GjRosterItem ri = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	gj_gtk_ui_load (window->connection->c, ri);
	return TRUE;
}

static gboolean gj_gtk_roster_contact_sub_request (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) { 
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	gj_presence_send_to (window->connection->c,
			     j, 
			     FALSE, 
			     GjPresenceTypeSubscribe, 
			     GjPresenceShowEnd, 
			     NULL, 
			     0);

	return TRUE;
}

static gboolean gj_gtk_roster_contact_sub_auth (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	gj_presence_send_to (window->connection->c,
			     j, 
			     TRUE, 
			     GjPresenceTypeSubscribed, 
			     GjPresenceShowEnd, 
			     NULL, 
			     0);

	return TRUE;
}

static gboolean gj_gtk_roster_contact_sub_cancel (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	gj_presence_send_to (window->connection->c, 
			     j, 
			     TRUE, 
			     GjPresenceTypeUnSubscribed, 
			     GjPresenceShowEnd, 
			     NULL, 
			     0);

	gj_presence_send_to (window->connection->c,
			     j, 
			     FALSE, 
			     GjPresenceTypeUnSubscribe, 
			     GjPresenceShowEnd, 
			     NULL, 
			     0);

	return TRUE;
}

static gboolean gj_gtk_roster_contact_edit_groups (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	gj_gtk_eg_load (window->connection->c, ri);
	return TRUE;
}

static gboolean gj_gtk_roster_contact_rename (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	gj_gtk_rc_load (window->connection->c, ri);
	return TRUE;
}

static gboolean gj_gtk_roster_contact_add (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	gj_gtk_ac_load (window->connection->c,
			roster, 
			ri);
	return TRUE;
}

static gboolean gj_gtk_roster_contact_remove (GjRosterWindow *window)
{
	GtkWidget *dialog = NULL;
	GtkWidget *checkbox = NULL;

	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	GList *list = NULL;

	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	if (gj_jid_get_type (j) == GjJIDTypeAgent) {
		list = gj_roster_get_contacts_by_agent (roster, gj_jid_get_without_resource (j)); 
	}

	if (gj_jid_get_type (j) != GjJIDTypeAgent) {
		dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_QUESTION,
						GTK_BUTTONS_OK_CANCEL,
						_("You are about to remove a contact."),
						_("Proceed to remove contact \"%s\"?"), 
						gj_jid_get_without_resource (j));
	} else {
		gchar *text = NULL;

		if (g_list_length (list) == 1) {
			text = g_strdup_printf (_("This agent is responsible for 1 contact!\nProceed to remove agent \"%s\"?"),
						gj_jid_get_without_resource (j));
		} else {
			text = g_strdup_printf (_("This agent is responsible for %d contacts!\nProceed to remove agent \"%s\"?"), 
						g_list_length (list), gj_jid_get_without_resource (j));
		}
				     
		dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_QUESTION,
						GTK_BUTTONS_OK_CANCEL,
						_("You are about to remove an agent."),
						text);

		checkbox = gtk_check_button_new_with_label (_("Remove all contacts this agent is responsible for?"));
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), checkbox, FALSE, FALSE, 6);
     
		gtk_widget_show_all (dialog);

		if (g_list_length (list) < 1) {
			gtk_widget_hide (checkbox);
		}

		g_free (text);
	}
  
	/* run dialog */
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
		const gchar *j_str = NULL;
		gint i = 0;

		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbox))) {
			for (i=0; i<g_list_length (list); i++) {
				GjRosterItem this_ri = NULL;
				GjJID this_j = NULL;
				const gchar *this_j_str;

				this_ri = (GjRosterItem) g_list_nth_data (list, i);
			
				if (this_ri == NULL) {
					continue; 
				}

				this_j = gj_roster_item_get_jid (this_ri);

				if (gj_jid_get_type (this_j) == GjJIDTypeAgent) {
					this_j_str = gj_jid_get_full (this_j);
				} else {
					this_j_str = gj_jid_get_without_resource (this_j);
				}

				gj_iq_request_roster_remove_contact (window->connection->c,
								     this_j_str,
								     gj_gtk_roster_contact_remove_cb,
								     NULL);
			}
		}

		if (gj_jid_get_type (j) == GjJIDTypeAgent) {
			j_str = gj_jid_get_full (j);
		} else {
			j_str = gj_jid_get_without_resource (j);
		}
      
		g_message ("%s: removing from roster:'%s'", __FUNCTION__, j_str);
		gj_iq_request_roster_remove_contact (window->connection->c,
						     j_str, 
						     gj_gtk_roster_contact_remove_cb,
						     NULL);

		if (gj_jid_get_type (j) == GjJIDTypeAgent) {
			g_message ("%s: unregistering agent:'%s'", __FUNCTION__, j_str);
			gj_gtk_rs_remove (window->connection->c, 
					  j_str);
		}
	}

	gtk_widget_destroy (dialog);
 
	/* clean up */
	g_list_free (list);

	return TRUE;
}

static gboolean gj_gtk_roster_agent_log_in (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	gj_presence_send_to (window->connection->c,
			     j, 
			     TRUE, 
			     GjPresenceTypeAvailable,
			     GjPresenceShowNormal, 
			     NULL, 
			     0);

	return TRUE;
}

static gboolean gj_gtk_roster_agent_log_out (GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	if (!window) {
		return FALSE;
	}

	if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	gj_presence_send_to (window->connection->c, 
			     j, 
			     TRUE, 
			     GjPresenceTypeUnAvailable, 
			     GjPresenceShowNormal, 
			     NULL, 
			     0);

	return TRUE;
}

static gboolean gj_gtk_roster_group_message (GjRosterWindow *window)
{
	gboolean not_group = TRUE;
	gchar *group = NULL;
  
	if (!window) {
		return FALSE;
	}

	if (gj_gtk_roster_model_selection_get_is_group (window, &not_group, &group) == FALSE) {
		return FALSE;
	}
  
	gj_gtk_gm_load (window->connection->c,
			window->connection->jid, 
			roster, 
			group);

	g_free (group);
	return TRUE;
}

static gboolean gj_gtk_roster_group_rename (GjRosterWindow *window)
{
	gboolean not_group = TRUE;
	gchar *group = NULL;
  
	if (!window) {
		return FALSE;
	}

	if (gj_gtk_roster_model_selection_get_is_group (window, &not_group, &group) == FALSE) {
		return FALSE;
	}
  
	gj_gtk_rg_load (window->connection->c, roster, group);

	g_free (group);
	return TRUE;
}

static gboolean gj_gtk_roster_group_remove (GjRosterWindow *window)
{
	GtkWidget *dialog = NULL;
	GtkWidget *checkbox = NULL;

	gboolean not_group = TRUE;
	gchar *group = NULL;
  
	GList *list = NULL;
	gchar *text = NULL;

	if (!window) {
		return FALSE;
	}

	if (gj_gtk_roster_model_selection_get_is_group (window, &not_group, &group) == FALSE) {
		return FALSE;
	}
  
	if (group == NULL || g_utf8_strlen (group, -1) < 1) { 
		g_warning ("%s: group was NULL or len was < 1", __FUNCTION__);
		return FALSE;
	}
  
	dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_QUESTION,
					GTK_BUTTONS_OK_CANCEL,
					_("You are about to remove a group."),
					_("Proceed to remove group \"%s\"?"), 
					group);


	list = gj_roster_get_contacts_by_group (roster, group);

	if (g_list_length (list) == 1) {
		text = g_strdup_printf (_("Remove the contact in this group from roster?"));
	} else {
		text = g_strdup_printf (_("Remove all %d contacts in this group from roster?"), g_list_length (list));
	}
  
	checkbox = gtk_check_button_new_with_label (text);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), checkbox, FALSE, FALSE, 6);

	gtk_widget_show_all (dialog);
  
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
		GjRosterItem ri = NULL;
		GjJID j = NULL;
		gint i = 0;
      
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbox))) {
			for (i=0; i<g_list_length (list); i++) {
				ri = (GjRosterItem) g_list_nth_data (list, i);
				
				if (ri == NULL) {
					continue;
				}

				j = gj_roster_item_get_jid (ri);

				gj_iq_request_roster_remove_contact (window->connection->c,
								     gj_jid_get_without_resource (j), 
								     gj_gtk_roster_contact_remove_cb,
								     NULL);

				/* remove roster item ? */
			}
		} else {
			for (i=0; i<g_list_length (list); i++) {
				ri = (GjRosterItem) g_list_nth_data (list, i);

				if (ri == NULL) {
					continue;
				}
				
				j = gj_roster_item_get_jid (ri);
				gj_roster_item_groups_del (ri, group);
				
				/* using the list, modify all the users groups to include the new group */
				gj_iq_request_user_change (window->connection->c,
							   gj_jid_get_full (j), 
							   gj_roster_item_get_name (ri), 
							   gj_roster_item_get_groups (ri),
							   gj_gtk_roster_user_changed_cb,
							   NULL);
			}
		}
	}

	/* clean up */
	g_free (text);
	g_list_free (list);
	g_free (group);
  
	gtk_widget_destroy (dialog);
  
	return TRUE;
}

static gboolean gj_gtk_roster_unregister (GjRosterWindow *window)
{
	GtkWidget *dialog = NULL;

	if (!window) {
		return FALSE; 
	}

	dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_QUESTION,
					GTK_BUTTONS_OK_CANCEL,
					_("Unregister this account?"),
					"%s\n\n%s",
					_("Note: The server may not be updated immediately.  "
					  "There can be some time before your account is "
					  "unavailable."),
					_("If you continue you will be disconnected from the "
					  "server immediately."));

	/* run dialog */
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
		gj_register_requirements (window->connection->c,
					  window->connection->server, 
					  (GjRegistrationCallback)gj_gtk_roster_unregister_requirements_cb, 
					  window);  
	}

	gtk_widget_destroy (dialog);
 
	return TRUE;
}

static void gj_gtk_roster_unregister_requirements_cb (GjRegistration reg, 
						      gint error_code, 
						      const gchar *error_reason,
						      GjRosterWindow *window)
{
	const gchar *key = NULL;
	const gchar *server = NULL;

	g_message ("%s: received response.", __FUNCTION__);

	if (!reg) {
		g_warning ("%s: reg was NULL", __FUNCTION__);
		return;
	}

	if (error_code != 0 || error_reason != NULL) {
		/* warn about error - usually a timeout */
		gj_gtk_show_error (_("Failed to get requirements to unregister Service."),
				   error_code,
				   error_reason);
 
		return;
	}

	server = gj_register_get_from (reg);
	key = gj_register_get_key (reg);

	if (!server) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return;
	}

	if (!key) {
		g_warning ("%s: registration key was NULL", __FUNCTION__);
		return;
	}

	gj_register_remove (window->connection->c,
			    server, 
			    key, 
			    NULL, 
			    NULL, 
			    NULL, 
			    NULL, 
			    (GjRegistrationCallback)gj_gtk_roster_unregister_cb, 
			    window);
}

static void gj_gtk_roster_unregister_cb (GjRegistration reg, 
					 gint error_code, 
					 const gchar *error_reason,
					 GjRosterWindow *window)
{
	if (error_code != 0 || error_reason != NULL) {
		/* warn about error - usually a timeout */
		gj_gtk_show_error (_("Failed to Unregister Service."),
				   error_code,
				   error_reason);
 
		return;
	} 

	gj_gtk_show_success (_("Account unregistered successfully"));

	/* disconnect */
	gj_iq_request_goodbye (window->connection->c,
			       window->connection->server);
}

static gboolean gj_gtk_roster_unsubscribed (GjRosterWindow *window, GjPresence pres)
{
	GtkWidget *dialog = NULL;
	GtkWidget *checkbox = NULL;
	GjJID j = NULL;

	if (!window) {
		return FALSE; 
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_presence_get_from_jid (pres);

	dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_QUESTION,
					GTK_BUTTONS_OK,
					_("Subscription Canceled"),
					_("You have been denied status information from \"%s\"."),
					gj_jid_get_full (j));

	/* To translators: this label comes after 'You have been denied status information from "%s".' */
	checkbox = gtk_check_button_new_with_label (_("Do you want to remove them from your roster?"));
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), checkbox, FALSE, FALSE, 6);
  
	gtk_widget_show_all (dialog);

	/* run dialog */
	gtk_dialog_run (GTK_DIALOG (dialog));

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (checkbox))) {
		g_message ("%s: removing unsubscribed contact:'%s'", __FUNCTION__, gj_jid_get_full (j));
		gj_iq_request_roster_remove_contact (window->connection->c,
						     gj_jid_get_without_resource (j), 
						     gj_gtk_roster_contact_remove_cb,
						     NULL);
	}

	gtk_widget_destroy (dialog);
 
	return TRUE;
}

/*
 * EVENT HANDLING
 */
static gboolean gj_gtk_roster_handle_next_event (GjRosterWindow *window, GjRosterItem ri)
{
	GjEvent ev = NULL;
	GjEventType type = -1;
  
	gint id = -1;
	gint count = 0;
  
	/* if no events waiting, do default action */
	if ((count = gj_event_count (ri)) < 1) {
		GjJID j = NULL;
		GjJIDType type = GjJIDTypeContact;

		j = gj_roster_item_get_jid (ri);
		
		switch (gj_jid_get_type (j)) {
		case GjJIDTypeContact: {
			g_message ("%s: %d events waiting, therefore starting new chat...",
				   __FUNCTION__, count);
			
			/* we can start a chat... */
			if (gj_config_get_mpf_double_click_for_chat () == TRUE) {
				gj_gtk_chat_send_new (window->connection->c,
						      window->connection->jid, 
						      ri);
			} else {
				gj_gtk_message_send_new (window->connection->c,
							 window->connection->jid, 
							 ri);
			}
			
			break;
		} 

		case GjJIDTypeAgent: {
			gj_gtk_ui_load (window->connection->c, ri);
			break;
		}

		default:
			break;
		}

		return TRUE;
	}

	if ((id = gj_event_next (ri)) < 0) { 
		g_warning ("%s: failed to get eventID from roster item", __FUNCTION__);
		return FALSE;
	}
  
	if ((ev = gj_event_find_by_id (id)) == NULL) {
		g_warning ("%s: failed to find event by ID:%d", __FUNCTION__, id);
		return FALSE;
	}

	/* get event type */
	type = gj_event_get_type (ev);

	g_message ("%s: handling eventID:%d, type:%d", __FUNCTION__, id, type);
  
	switch (type) {
	case GjEventTypeMessage: {
		GjMessage m = (GjMessage) gj_event_get_data (ev);
		
		if (gj_message_get_type (m) == GjMessageTypeNormal) {
			/* DONT free the event, that is done when it has been handled */
			if (!gj_gtk_message_post_by_event (window->connection->c,
							   window->connection->jid, 
							   ev, 
							   TRUE)) {
				break;
			}
		} else if (gj_message_get_type (m) == GjMessageTypeChat) {
			if (gj_gtk_chat_post_by_event (window->connection->c,
						       window->connection->jid, 
						       ev, 
						       TRUE) == TRUE) {
				gj_event_free (ev);
			} else {
				break;
			}

			/* look for futher events of type 'chat' in queue */
			while ((ev = gj_event_find_next_by_message_type (ri, GjMessageTypeChat)) != NULL) {
				m = (GjMessage) gj_event_get_data (ev);
				id = gj_event_get_id (ev);
				
				if (m == NULL) {
					g_warning ("%s: msg data was NULL, not handling...", __FUNCTION__);
					
					/* free event - no use to me now */
					gj_event_free (ev);
					continue;
				}
				
				g_message ("%s: handling next event id:%d, type:%d->'%s'", 
					   __FUNCTION__, id, type, gj_translate_event_type (type));
				
				if (gj_gtk_chat_post_by_event (window->connection->c,
							       window->connection->jid, 
							       ev,
							       TRUE) == TRUE) {
					gj_event_free (ev);
				} else {
					break;
				}
			}
		} else if (gj_message_get_type (m) == GjMessageTypeHeadline) {
			GjHeadlineWindow *hw = NULL;
			
			/* notify of headline */
			if ((hw = gj_gtk_hw_notification_new (id, m)) == NULL) {
				g_warning ("%s: new notification for eventID:%d returned NULL chat window", 
					   __FUNCTION__, id);
				return FALSE;
			}
			
			/* free event */
			gj_event_free (ev);
			
			/* look for futher events of type 'headline' in queue */
			while ((ev = gj_event_find_next_by_message_type (ri, GjMessageTypeHeadline)) != NULL) {
				m = (GjMessage) gj_event_get_data (ev);
				id = gj_event_get_id (ev);
				
				if (m == NULL) {
					g_warning ("%s: m data was NULL, not handling...", __FUNCTION__);
					
					/* free event - no use to me now */
					gj_event_free (ev);
					continue;
				}
		
				g_message ("%s: handling next event of type:%d, id:%d", __FUNCTION__, type, id);
				gj_gtk_hw_notification_new (id, m);
				
				/* free event */
				gj_event_free (ev);
			}	    
		} else {
			g_message ("%s: unhandled message type:%d", __FUNCTION__, gj_message_get_type (m));
			
			/* if we dont handle certain types of messages (e.g. headlines, groupchats...), free it. */
			gj_event_free (ev);
		}
	
		break;
	}
		
	default: {
		g_warning ("%s: unhandled event type:%d->'%s', freeing...", __FUNCTION__, type, gj_translate_event_type (type));
		
		/* if we dont know about it, free it. */
		gj_event_free (ev);

		break;
	}
	}
	
	return TRUE;
}

gboolean gj_gtk_roster_handle_next_event_display (GjRosterItem ri)
{
	GjEvent ev = NULL;
	GjEventType type = -1;
	GjRosterWindow *window = NULL;

	gint id = 0;

	if (! (window = current_window)) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	id = gj_event_next (ri);
	
	if (id != -1) {
		ev = gj_event_find_by_id (id); 
	}

	if (ev != NULL) {
		type = gj_event_get_type (ev);
	}
 
	switch (type) {
	case GjEventTypeMessage: {
		GjMessage m = (GjMessage) gj_event_get_data (ev);
		
		/* start flashing */
		if (gj_message_get_type (m) != GjMessageTypeError &&
		    gj_message_get_type (m) != GjMessageTypeXEvent) {
			/* returns true if added false if not */
			if (gj_gtk_roster_flash_timeout_add (window, ri) == TRUE) {
				gj_gtk_roster_model_set_message (window, ri, gj_message_get_type (m)); 
			}

		} else {
			gj_gtk_roster_model_set_message (window, ri, gj_message_get_type (m));
		}

		break;
	}

	default: {
		GjPresence pres = NULL;
		
		/* stop flashing */
		gj_gtk_roster_flash_timeout_remove (window, ri);
		
		if ((pres = gj_roster_item_get_presence (ri)) == NULL) {
			g_warning ("%s: could not get presence from roster item with id:%d", 
				   __FUNCTION__, gj_roster_item_get_id (ri)); 
			return FALSE;
		}
	
		/* no events pending, set icon to status */
		gj_gtk_roster_model_set_presence (window, 
						  ri, 
						  gj_presence_get_type (pres), 
						  gj_presence_get_show (pres), 
						  gj_presence_get_status (pres)); 
		
		break;
	}
	}
	
	/* check for ANY events... */
	if(gj_event_count_all() < 1) {
		GjPresenceType type = 0;
		GjPresenceShow show = 0;
		
		type = window->connection->presence_type;
		show = window->connection->presence_show;

		gj_gtk_docklet_set_image_from_stock (gj_gtk_presence_to_stock_id (type, show));
	}


#if 0
	gj_gtk_docklet_set_active (TRUE);
	else
		gj_gtk_docklet_set_active (FALSE);
#endif

	return TRUE;
}

static gboolean gj_gtk_roster_flash_timeout_add (GjRosterWindow *window, GjRosterItem ri)
{
	gint i = 0;
	gboolean found = FALSE;

	if (!window) {
		return FALSE; 
	}

	if (ri == NULL) { 
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	for (i=0; i<g_list_length (window->flash_list) && !found; i++) {
		gint ri_id = 0;
		gpointer data = NULL;

		data = (gpointer) g_list_nth_data (window->flash_list, i);

		if (data == NULL) {
			continue; 
		}

		ri_id = GPOINTER_TO_INT (data);

		if (ri_id == gj_roster_item_get_id (ri)) {
			found = TRUE;
		}
	}
   
	if (!found) {
		/* add to list */
		window->flash_list = g_list_append (window->flash_list, GINT_TO_POINTER (gj_roster_item_get_id (ri)));
		return TRUE;
	}
  
	return FALSE;
}

static gboolean gj_gtk_roster_flash_timeout_remove (GjRosterWindow *window, GjRosterItem ri)
{ 
	gint i = 0;

	/* NOTE: this should REALLY be a HASH TABLE not a LIST */

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
 
	if (window->flash_list == NULL) {
		return TRUE;
	}

	for (i=0; i<g_list_length (window->flash_list); i++) {
		gint ri_id = 0;
		gpointer data = NULL;

		data = (gpointer) g_list_nth_data (window->flash_list, i);

		if (data == NULL) {
			continue;
		}

		ri_id = GPOINTER_TO_INT (data);

		if (ri_id == gj_roster_item_get_id (ri)) {
			window->flash_list = g_list_remove (window->flash_list, data); 
		}
	}

	return TRUE;
}

static gboolean gj_gtk_roster_flash_timeout_update_roster (GjRosterWindow *window, GjRosterItem ri)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeStore *store = NULL;

	GList *iter_list = NULL;
	gint i = 0;

	GjJID j = NULL;
	GjPresence pres = NULL;

	if (!window) {
		return FALSE;
	}

	if (ri == NULL) { 
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}
  
	j = gj_roster_item_get_jid (ri);
	pres = gj_roster_item_get_presence (ri);

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));
	model = gtk_tree_view_get_model (view);

	if (gj_gtk_roster_model_find_jid (view, gj_roster_item_get_jid (ri), &iter_list) == FALSE) {
		g_message ("%s: could not find jid:'%s' on roster", __FUNCTION__, gj_jid_get_full (j));

		if (gj_gtk_roster_model_add (window, ri) == FALSE) {
			return FALSE;
		}
	}

	for (i=0; i<g_list_length (iter_list); i++) {
		GtkTreeIter *iter = NULL;
		GtkTreePath *path = NULL;
      
		GdkPixbuf *pb_flash = NULL;

		gboolean flash = FALSE;
     
		iter = (GtkTreeIter*) g_list_nth_data (iter_list, i);
      
		gtk_tree_model_get (model, iter,
				    COL_ROSTER_IMAGE_FLASH, &pb_flash,
				    COL_ROSTER_IMAGE_FLASH_ON, &flash,
				    -1);
      
		/* FALSE = draw presence, TRUE = draw image */
		if (!flash) {
			GdkPixbuf *pb_presence = NULL;

			if (gj_roster_item_get_subscription (ri) != GjRosterItemSubscriptionNone) {
				pb_presence = gj_gtk_presence_to_pixbuf_new (gj_presence_get_type (pres), gj_presence_get_show (pres));
			} else {
				pb_presence = gj_gtk_presence_to_pixbuf_new (GjPresenceTypeNone, 0);
			}
	  
			gtk_tree_store_set (store, iter,
					    COL_ROSTER_IMAGE, pb_presence,
					    COL_ROSTER_IMAGE_FLASH_ON, TRUE,
					    -1);

			/* only do this once */
			if (i == 0) {
				const gchar *stock_id = NULL;
	      
				/* flash notification area */
				stock_id = gj_gtk_presence_to_stock_id (gj_presence_get_type (pres), 
									gj_presence_get_show (pres));
				gj_gtk_docklet_set_image_from_stock (stock_id);
			}
	  
			if (pb_presence != NULL) g_object_unref (G_OBJECT (pb_presence));
		} else {
			/* only do this once */
			if (i == 0) {
				const gchar *stock_id = NULL;
	      
				/* flash notification area */
				stock_id = GJ_STOCK_MESSAGE_CHAT;
				gj_gtk_docklet_set_image_from_stock (stock_id);
			}

			gtk_tree_store_set (store, iter,
					    COL_ROSTER_IMAGE, pb_flash,
					    COL_ROSTER_IMAGE_FLASH_ON, FALSE,
					    -1);
		}
      
		path = gtk_tree_model_get_path (model, iter);
		gtk_tree_model_row_changed (model, path, iter);
      
		/* clean up */
		if (path != NULL) {
			gtk_tree_path_free (path);
		}

		if (pb_flash != NULL) {
			g_object_unref (G_OBJECT (pb_flash));
		}
	}
  
	g_list_foreach (iter_list, gj_gtk_roster_model_iter_list_destroy_cb, NULL);
	g_list_free (iter_list);

	/* return FALSE to stop timeout */
	return TRUE;
}

static gboolean gj_gtk_roster_flash_timeout_cb (GjRosterWindow *window)
{
	gint i = 0;

	if (!window) {
		return FALSE;
	}

	if (window->flash_list == NULL) {
		return TRUE; 
	}

	for (i=0; i<g_list_length (window->flash_list); i++) {
		gint ri_id = 0;
		gpointer data = NULL;

		GjRosterItem ri = NULL;

		data = (gpointer) g_list_nth_data (window->flash_list, i);

		if (data == NULL) {
			continue;
		}

		ri_id = GPOINTER_TO_INT (data);

		ri = gj_rosters_find_item_by_id (ri_id);
		if (ri == NULL) {
			continue;
		}
		
		if (gj_gtk_roster_flash_timeout_update_roster (window, ri) == FALSE) {
			return FALSE;
		}
	}

	return TRUE;
}

/*
 * CALLBACK functions
 */
static void gj_gtk_roster_event_cb (gint event_id, GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
	GjEvent ev = NULL;
	GjEventType type = 0;
  
	if (!window) {
		return;
	}

	if ((ev = gj_event_find_by_id (event_id)) == NULL) {
		g_warning ("%s: failed to find event by ID:%d", __FUNCTION__, event_id);
		return;
	}
  
	/* event type */
	type = gj_event_get_type (ev);
	g_message ("%s: received new event, event_id:%d, type:%d->'%s'", 
		   __FUNCTION__, event_id, type, gj_translate_event_type (type));
  
	/* find roster item */
	if ((ri = gj_event_get_roster_item (ev)) == NULL) {
		g_warning ("%s: no roster item for this event, this is a *SERIOUS* error.", __FUNCTION__);
		return;
	}

	/* get jid */
	j = gj_roster_item_get_jid (ri);
  
	/* notify event viewer */
	gj_gtk_ev_model_add_from_event (ev);

	/* this is for group chats */
	if (gj_roster_item_get_is_for_group_chat (ri) == TRUE) {
		if(gj_roster_item_get_is_joining_group_chat(ri) == TRUE) {
			gj_gtk_gcj_post_by_event (ev);
		} else {
			gj_gtk_gcw_post_by_event (ev);
		}

		gj_event_free (ev);
		
		/* we should return here really - 
		   we are not interested in roster events for group chat */ 
		return;
	}

	/* handle event */
	switch (type) {
	case GjEventTypePresence: {
		GjPresence pres = NULL;
		pres = (GjPresence) gj_event_get_data (ev);
		
		/* what if there are MORE than one chat window available in the future? */
		gj_gtk_chat_presence_update (ri, (GjPresence) gj_event_get_data (ev));
		
		/* exceptions are subscriptions */
		if (gj_presence_get_type (pres) == GjPresenceTypeSubscribe) {
			gj_gtk_sr_load (window->connection->c, 
					window->connection->jid, 
					pres);
		} else if (gj_presence_get_type (pres) == GjPresenceTypeUnSubscribed) {
			gj_gtk_roster_unsubscribed (window, pres);
		} 

		gj_event_free (ev);
		break;
	}

	case GjEventTypeMessage: {
		GjMessage m = (GjMessage) gj_event_get_data (ev);
		
		if (gj_message_get_type (m) == GjMessageTypeNormal) {
			gj_gtk_message_post_by_event (window->connection->c,
						      window->connection->jid, 
						      ev, 
						      FALSE);
			
			/* should we play sound ? */
			if (gj_config_get_mpf_sound_on_receive_message () == TRUE) {
				gj_gtk_sound_play (gj_config_get_mpf_sound_file_on_receive_message ());
			}
		} else if (gj_message_get_type (m) == GjMessageTypeChat) {
			if (gj_gtk_chat_post_by_event (window->connection->c,
						       window->connection->jid, 
						       ev, 
						       FALSE) == TRUE) {
				gj_event_free (ev);
			}
			
			/* dont play sound for xevents */
			if (gj_config_get_mpf_sound_on_receive_chat () == TRUE) {
				gj_gtk_sound_play (gj_config_get_mpf_sound_file_on_receive_chat ());
			}
		} else if (gj_message_get_type (m) == GjMessageTypeXEvent) {
			gj_gtk_chat_post_by_event (window->connection->c,
						   window->connection->jid, 
						   ev, 
						   FALSE);
			gj_event_free (ev);
		} else if (gj_message_get_type (m) == GjMessageTypeHeadline) {
			/* should we play sound ? */
			if (gj_config_get_mpf_sound_on_receive_headline () == TRUE) {
				gj_gtk_sound_play (gj_config_get_mpf_sound_file_on_receive_headline ());
			}
		} else if (gj_message_get_type (m) == GjMessageTypeError) {
			/* should we play sound ? */
			if (gj_config_get_mpf_sound_on_receive_error () == TRUE) {
				gj_gtk_sound_play (gj_config_get_mpf_sound_file_on_receive_error ()); 
			}
		} else {
			/* do we free the event because it is not handled ?? */
			g_message ("%s: freeing unhandled event", __FUNCTION__);
			gj_event_free (ev);
		}
		
		break;
	}
      
	default: {
		g_warning ("%s: unhandled event type:%d->'%s'", __FUNCTION__, type, gj_translate_event_type (type));
		break;
	}
	}

	gj_gtk_roster_handle_next_event_display (ri);
}

static gboolean gj_gtk_roster_generate_mnu_user (GjRosterWindow *window, GjRosterItem ri, GdkEventButton *gdk_event)
{
	GtkWidget *menu = NULL;
	GtkWidget *submenu = NULL;
	GtkWidget *item = NULL;
	GtkWidget *image = NULL;

	GjEvent ev = NULL;

	if (!window) { 
		return FALSE;
	}

	if (gdk_event == NULL) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	ev = gj_event_find_next_by_type (ri, GjEventTypeMessage);

	menu = gtk_menu_new ();
  
	item = gtk_menu_item_new_with_mnemonic (_("Read..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_read_cb), window);
  
	if (ev == NULL) {
		gtk_widget_set_sensitive (GTK_WIDGET (item), FALSE);
	}

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_image_menu_item_new_with_mnemonic (_("New _Message"));
	image = gtk_image_new_from_stock (GJ_STOCK_MESSAGE_NORMAL, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_new_message_cb), window);

	item = gtk_image_menu_item_new_with_mnemonic (_("New _Chat")); 
	image = gtk_image_new_from_stock (GJ_STOCK_MESSAGE_CHAT, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_new_chat_cb), window);

#if 0
	item = gtk_menu_item_new_with_mnemonic (_("File Transfer..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_file_transfer_cb), window);
#endif

	item = gtk_image_menu_item_new_with_mnemonic (_("Vie_w Headlines"));
	image = gtk_image_new_from_stock (GJ_STOCK_MESSAGE_HEADLINE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_headlines_cb), window);
  
	if (gj_roster_item_get_is_another_resource (ri) == FALSE) {
		item = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
      
		item = gtk_image_menu_item_new_with_mnemonic (_("_History"));
		image = gtk_image_new_from_stock (GTK_STOCK_JUSTIFY_FILL, GTK_ICON_SIZE_MENU);
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
		g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_history_cb), window);
      
		item = gtk_image_menu_item_new_from_stock (GTK_STOCK_DIALOG_INFO, NULL);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);  
		g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_information_cb), window);
     
		if (gj_roster_item_get_is_permanent (ri) == TRUE) {
			item = gtk_separator_menu_item_new ();
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

			item = gtk_menu_item_new_with_mnemonic (_("Edit _Groups..."));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
			g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_edit_groups_cb), window);
	  
			item = gtk_menu_item_new_with_mnemonic (_("Rename..."));
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
			g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_rename_cb), window);
		}
      
		item = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
      
		submenu = gtk_menu_new ();
      
		/* To translators: menu with _Request, _Cancel, Subscription */
		item = gtk_menu_item_new_with_mnemonic (_("_Request"));
		gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item); 
		g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_sub_request_cb), window);
		item = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item);

		item = gtk_menu_item_new_with_mnemonic (_("_Authorise"));
		gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item); 
		g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_sub_auth_cb), window);
	      
		item = gtk_menu_item_new_with_mnemonic (_("_Cancel"));
		gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item); 
		g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_sub_cancel_cb), window);
      
		item = gtk_menu_item_new_with_mnemonic (_("_Subscription"));
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
      
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
      
		item = gtk_separator_menu_item_new ();
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
      
		if (gj_roster_item_get_is_permanent (ri) == FALSE) {
			item = gtk_image_menu_item_new_from_stock (GTK_STOCK_ADD, NULL);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
			g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_add_cb), window);
		}

		item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REMOVE, NULL);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
		g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_remove_cb), window);
	}
  
	/* show */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, gdk_event->button, gdk_event->time);

	/* clean up */
	gtk_object_sink (GTK_OBJECT (menu));

	return TRUE;
}

static gboolean gj_gtk_roster_generate_mnu_agent (GjRosterWindow *window, GjRosterItem ri, GdkEventButton *gdk_event)
{
	GtkWidget *menu = NULL;
	GtkWidget *submenu = NULL;
	GtkWidget *item = NULL;
	GtkWidget *image = NULL;

	if (!window) {
		return FALSE;
	}

	if (gdk_event == NULL) {
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	menu = gtk_menu_new ();
      
	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_DIALOG_INFO, NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);  
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_information_cb), window);

	item = gtk_menu_item_new_with_mnemonic (_("Rename..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_rename_cb), window);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
  
	item = gtk_image_menu_item_new_with_mnemonic (_("Log _In"));
	image = gtk_image_new_from_stock (GTK_STOCK_YES, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);  
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_agent_log_in_cb), window);

	item = gtk_image_menu_item_new_with_mnemonic (_("Log _Out"));
	image = gtk_image_new_from_stock (GTK_STOCK_NO, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);   
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_agent_log_out_cb), window);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	submenu = gtk_menu_new ();

	/* To translators: menu with _Request, _Cancel, Subscription */
	item = gtk_menu_item_new_with_mnemonic (_("_Request"));
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_sub_request_cb), window);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item);

	item = gtk_menu_item_new_with_mnemonic (_("_Authorise"));
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_sub_auth_cb), window);

	item = gtk_menu_item_new_with_mnemonic (_("_Cancel"));
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_sub_cancel_cb), window);

	item = gtk_menu_item_new_with_mnemonic (_("_Subscription"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
  
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REMOVE, NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_remove_cb), window);

	/* show */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, gdk_event->button, gdk_event->time);

	/* clean up */
	gtk_object_sink (GTK_OBJECT (menu));

	return TRUE;
}

static gboolean gj_gtk_roster_generate_mnu_group (GjRosterWindow *window, GdkEventButton *gdk_event)
{
	GtkWidget *menu = NULL;
	GtkWidget *item = NULL;
	GtkWidget *image = NULL;
  
	if (!window) {
		return FALSE;
	}

	if (gdk_event == NULL) { 
		return FALSE;
	}

	menu = gtk_menu_new ();
      
	item = gtk_image_menu_item_new_with_mnemonic (_("Send _Message to All"));
	image = gtk_image_new_from_stock (GJ_STOCK_MESSAGE_NORMAL, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_group_message_cb), window);

	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
  
	item = gtk_menu_item_new_with_mnemonic (_("Rename"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_group_rename_cb), window);

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REMOVE, NULL);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_group_remove_cb), window);

	/* show */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, gdk_event->button, gdk_event->time);

	/* clean up */
	gtk_object_sink (GTK_OBJECT (menu));

	return TRUE;
}

static gboolean gj_gtk_roster_generate_mnu_notification_tray (gint x, gint y, GjRosterWindow *window)
{
	GtkWidget *menu = NULL;
	/*   GtkWidget *submenu = NULL; */
	GtkWidget *item = NULL;
	/*   GtkWidget *image = NULL; */

	menu = gtk_menu_new ();
  
	item = gtk_menu_item_new_with_mnemonic (_("Show/Hide Window"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_show_window_cb), window);
   
	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	/*   item = gtk_image_menu_item_new_with_mnemonic ("New _Message"); */
	/*   image = gtk_image_new_from_stock (GJ_STOCK_MESSAGE_NORMAL, GTK_ICON_SIZE_MENU); */
	/*   gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image); */
	/*   gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); */
	/*   g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_contact_new_message_cb), window); */

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, NULL); 
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
	g_signal_connect ((gpointer) item, "activate", G_CALLBACK (gj_gtk_roster_menu_quit_cb), window);
  
	/* show */
	gtk_widget_show_all (GTK_WIDGET (menu));
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 1, gtk_get_current_event_time ());

	/* clean up */
	gtk_object_sink (GTK_OBJECT (menu));

	return TRUE;
}

/*
 * roster IQ callbacks
 */
void gj_gtk_roster_contact_remove_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	xmlNodePtr node = NULL;
	xmlChar *type = NULL;

	if (!iq || !iq_related) {
		return;
	}

	/* check for errors? */
	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		gchar *detail = NULL;

		xmlChar *error_code = NULL;
		xmlChar *error_message = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		error_code = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		error_message = gj_parser_find_by_node_and_ret (node, (guchar*)"error"); 

		gj_gtk_show_error (_("Failed to Remove Contact."),
				   atoi ((gchar*)error_code),
				   (gchar*)error_message);
       
		g_warning ("%s: registration failed", __FUNCTION__);
        
		/* clean up */
		g_free (detail);
		return;
	}

	/* NOTE: we do not remove from the roster here... 
	   we are sent a message to do this */
}

static void gj_gtk_roster_user_changed_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: response!", __FUNCTION__);
}

static void gj_gtk_roster_docklet_left_click_cb (gboolean double_click, 
						 gint x_root, 
						 gint y_root, 
						 GjRosterWindow *window)
{
	/*   g_message ("%s: left click, double:%s, x:%d, y:%d", */
	/* 	    __FUNCTION__, double_click ? "yes" : "no", x_root, y_root); */

	if (!window) {
		return;
	}

	if (!GTK_WIDGET_VISIBLE (window->window)) {
		gtk_window_present (GTK_WINDOW (window->window));
		gdk_window_raise (GDK_WINDOW (window->window->window));
	} else {
		gtk_widget_hide (window->window);
	}    
}

static void gj_gtk_roster_docklet_right_click_cb (gboolean double_click, 
						  gint x_root, 
						  gint y_root, 
						  GjRosterWindow *window)
{
	/*   g_message ("%s: right click, double:%s, x:%d, y:%d", */
	/* 	    __FUNCTION__, double_click ? "yes" : "no", x_root, y_root); */
	gj_gtk_roster_generate_mnu_notification_tray (x_root, y_root, window);
}







/*
 * connection progress
 * 
 */

static void gj_gtk_roster_connect (GjRosterWindow *window)
{
	if (GTK_BIN (window->mnu_jabber_connect)->child) {
		GtkWidget *child = GTK_BIN (window->mnu_jabber_connect)->child;
		
		/* do stuff with child */
		if (GTK_IS_LABEL (child)) {
			G_CONST_RETURN gchar *text = NULL;
			text = gtk_label_get_text (GTK_LABEL (child));

			if (strcmp (text, _("Cancel Connect")) == 0) {
				gj_gtk_roster_connect_cleanup (window);
				gj_gtk_roster_set_mode_disconnected (window);

				return;
			}
		}
	}

	if (gj_config_get_cs_prompt_for_password () == TRUE) {
		gtk_label_set_text (GTK_LABEL (window->label_connection), 
				    _("Waiting for Password"));

		gj_gtk_pr_load (FALSE, 
				NULL, 
				(GjPasswordRequiredCallback)gj_gtk_roster_connect_password_cb,
				window);
	} else {
		g_message ("%s: auto connecting...", __FUNCTION__);
		gj_gtk_roster_connect_start (window, NULL);
	}
}

static void gj_gtk_roster_connect_password_cb (gboolean success, const gchar *password, GjRosterWindow *window)
{
	if (!success) {
		gj_gtk_roster_connect_cleanup (window);
		return;
	}

	gj_gtk_roster_connect_start (window, password);
}

static gboolean gj_gtk_roster_connect_timeout_cb (GjRosterWindow *window)
{
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (window->progressbar_connection));
  
	/* As this is a timeout function, return TRUE so that it
	 * continues to get called */
	return TRUE;
} 

static void gj_gtk_roster_connect_start (GjRosterWindow *window, const gchar *password)
{
	GjRosterConnection *rc = NULL;
	GjStream stm = NULL;

	if (window->connection) {
		g_warning ("%s: there was already a connection set up.  cleaning it up first...", __FUNCTION__);
		gj_gtk_roster_connect_cleanup (window);
	}

	/* creater new roster connection */
	window->connection = rc = g_new0 (GjRosterConnection, 1);

	rc->server = g_strdup (gj_config_get_cs_server ());
	rc->port = gj_config_get_cs_port ();
	rc->username = g_strdup (gj_config_get_cs_username ());

	if (password) {
		rc->password = g_strdup (password);
	} else {
		rc->password = g_strdup (gj_config_get_cs_password ());
	}

	rc->resource = g_strdup (gj_config_get_cs_resource ());

	rc->is_disconnect_planned = FALSE;

	/* auto away */
	rc->auto_away_presence_show = GjPresenceShowNormal;
  
	/* presence */
	rc->presence_type = GjPresenceTypeAvailable;
	rc->presence_show = GjPresenceShowNormal;
	rc->presence_priority = gj_config_get_cs_priority ();

	/* create stream */
	rc->stream = gj_stream_new (rc->server);

	if (!rc->stream) {
		GtkWidget *dialog = NULL;

		dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_ERROR,
						GTK_BUTTONS_OK,
						_("Connection Error"),
						_("Could not create new stream."),
						NULL);

		g_signal_connect_swapped (GTK_OBJECT (dialog), "response",
					  G_CALLBACK (gtk_widget_destroy),
					  GTK_OBJECT (dialog));

		gtk_widget_show_all (dialog);
		
		g_warning ("%s: could not create new stream", __FUNCTION__);

		gj_gtk_roster_connect_cleanup (window);
		
		return;
	}

	/* set struct info */
	gj_stream_set_port (rc->stream, rc->port);
	gj_stream_set_connected_cb (rc->stream, (GjStreamCallback)gj_gtk_roster_connect_start_cb, window);
	gj_stream_set_disconnected_cb (rc->stream, (GjStreamCallback)gj_gtk_roster_connect_stop_cb, window);
	gj_stream_set_error_cb (rc->stream, (GjStreamCallback)gj_gtk_roster_connect_error_cb, window);

	gj_stream_build (rc->stream);

	gtk_label_set_text (GTK_LABEL (window->label_connection), 
			    _("Connecting..."));

	if (gj_stream_open (rc->stream) == FALSE) {
		GtkWidget *dialog = NULL;

		dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_ERROR,
						GTK_BUTTONS_OK,
						_("Connection Error"),
						_("Could not open stream."),
						NULL);

		g_signal_connect_swapped (GTK_OBJECT (dialog), "response",
					  G_CALLBACK (gtk_widget_destroy),
					  GTK_OBJECT (dialog));

		gtk_widget_show_all (dialog);
		
		g_warning ("%s: could not create new stream", __FUNCTION__);

		gj_gtk_roster_connect_cleanup (window);

		return;
	}

	gj_gtk_roster_set_mode_connecting (window);

	/* add timeout for pulsing */
	rc->timeout_id = gtk_timeout_add (25, 
					  (GSourceFunc)gj_gtk_roster_connect_timeout_cb, 
					  window);
}


static void gj_gtk_roster_connect_start_cb (GjStream stm, gint error_code, gchar *error_message, GjRosterWindow *window)
{
	gchar *jid = NULL;

	GjRosterConnection *rc = NULL;

	rc = window->connection;

	/* create jid */
	jid = g_strdup_printf ("%s@%s%s%s", 
			       rc->username, 
			       rc->server, 
			       rc->resource ? "/" : "",
			       rc->resource ? rc->resource : "");

	rc->jid = gj_jid_new (jid);
	rc->c = gj_connection_ref (gj_stream_get_connection (stm));

	g_free (jid);

	gtk_label_set_text (GTK_LABEL (window->label_connection), 
			    _("Requesting Authentication Method"));

	/* NOTE: no need to free the iq by event id because there was no iq
	   created to generate this response! */

	/* next request */
	gj_iq_request_auth_method (rc->c,
				   rc->username, 
				   (GjInfoqueryCallback)gj_gtk_roster_connect_auth_method_cb, 
				   window);
}

static void gj_gtk_roster_connect_stop_cb (GjStream stm, gint error_code, gchar *error_message, GjRosterWindow *window)
{
	GjRosterConnection *rc = NULL;
	gchar *password = NULL;

	rc = window->connection;

	if (!rc) {
		return;
	}

	if (gj_stream_get_is_disconnect_planned (stm) == TRUE) {
		rc->is_disconnect_planned = TRUE; 
	}

	/* update roster */
	gj_gtk_roster_set_mode_disconnected (window);

	/* set stream to NULL */
	rc->stream = NULL;

	if (!rc->is_disconnect_planned && 
	    gj_config_get_cs_auto_reconnect_on_disconnect () == TRUE) {
		password = g_strdup (rc->password);

		/* cleanup old connection */
		gj_gtk_roster_connect_cleanup (window);

		/* reconnect */
		gj_gtk_roster_connect_start (window, password);

		g_free (password);

		return;
	}

	gj_gtk_roster_connect_cleanup (window);
}

static void gj_gtk_roster_connect_error_cb (GjStream stm, gint error_code, gchar *error_message, GjRosterWindow *window)
{
	GjRosterConnection *rc = NULL;
	GtkWidget *dialog = NULL;
	gchar *password = NULL;

	rc = window->connection;

	if (!rc) {
		return;
	}

	rc->stream = NULL;

	/* update roster */
	gj_gtk_roster_set_mode_disconnected (window);

	/* clean up */
	gj_gtk_roster_connect_cleanup (window);

	/* tell user what is happening */
	dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_OK,
					_("Connection Error."),
					error_message,
					NULL);
	
	g_signal_connect_swapped (GTK_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy),
				  GTK_OBJECT (dialog));
	
	gtk_widget_show_all (dialog);
	
	g_warning ("%s: connection error:%d->'%s'", __FUNCTION__, error_code, error_message);
}

static void gj_gtk_roster_connect_auth_method_cb (GjInfoquery iq, 
						  GjInfoquery iq_related, 
						  GjRosterWindow *window)
{
	GjConnection c = NULL;
	GjRosterConnection *rc = NULL;
	
	/* should look thru xml message to check we can do this first! */
	gboolean can_use_digest = TRUE;

	const gchar *session_id = NULL;
  	const gchar *password_plain = NULL;
	gchar *password_digest = NULL;

	g_message ("%s: received available authentication methods", __FUNCTION__);

	rc = window->connection;
	c = gj_iq_get_connection (iq);

	session_id = gj_stream_get_id (rc->stream);

	gtk_label_set_text (GTK_LABEL (window->label_connection), 
			    _("Requesting Authentication..."));

	/* next request */
	if (!session_id || can_use_digest || gj_config_get_cs_use_plain_text_password () == TRUE) {
		/* use plain text */
		gj_iq_request_auth_with_plain_password (rc->c,
							rc->username,
							rc->password,
							rc->resource,
							(GjInfoqueryCallback)gj_gtk_roster_connect_auth_cb,
							window);
	} else {
		/* use digest (SHA1) */
		password_plain = rc->password;
		password_digest = gj_get_sha_by_session_id_and_password (session_id, password_plain);

		gj_iq_request_auth_with_digest_password (rc->c,
							 rc->username,
							 password_digest,
							 rc->resource,
							 (GjInfoqueryCallback)gj_gtk_roster_connect_auth_cb,
							 window);
      
		g_free (password_digest);
	}
}

static void gj_gtk_roster_connect_auth_cb (GjInfoquery iq, 
					   GjInfoquery iq_related, 
					   GjRosterWindow *window)
{
	xmlNodePtr node = NULL;
	xmlChar *type = NULL;

	g_message ("%s: received authentication response", __FUNCTION__);

	if (!iq || !iq_related) {
		return;
	}

	window = current_window;

	if (!window) {
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		GtkWidget *dialog = NULL;

		gchar *error_code = NULL;
		gchar *error_message = NULL;

		node = gj_parser_find_by_node (node, (guchar*)"error");

		error_code = (gchar*)gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		error_message = (gchar*)gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		dialog = gj_gtk_hig_dialog_new (GTK_WINDOW (window->window),
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_ERROR,
						GTK_BUTTONS_OK,
						_("Connection Error."),
						"%s\n%s",
						_("Failed to authenticate."),
						error_message,
						NULL);

		g_signal_connect_swapped (GTK_OBJECT (dialog), "response",
					  G_CALLBACK (gtk_widget_destroy),
					  GTK_OBJECT (dialog));

		gtk_widget_show_all (dialog);
		
		g_warning ("%s: could not create new stream", __FUNCTION__);

		gj_gtk_roster_connect_cleanup (window);
		gj_gtk_roster_set_mode_disconnected (window);

		return;
	}

	window->connection->is_logged_in = TRUE;

	/* update roster */
	gj_gtk_roster_set_mode_connected (window);
}

static void gj_gtk_roster_connect_cleanup (GjRosterWindow *window)
{
	GjRosterConnection *rc = NULL;

	rc = window->connection;

	if (!rc) {
		return;
	}

	g_free (rc->server);
	g_free (rc->username);
	g_free (rc->password);
	g_free (rc->resource);

 	if (rc->stream) { 
 		gj_stream_free (rc->stream); 
 	} 
	
	if (rc->timeout_id != -1) {
		g_source_remove (rc->timeout_id);
		rc->timeout_id = -1;
	}

	g_free (rc->nick);

	if (rc->c) {
		gj_connection_unref (rc->c);
	}

	if (rc->jid) {
		gj_jid_unref (rc->jid);
	}

	g_free (rc);
	window->connection = NULL;
}

static void gj_gtk_roster_connect_get_vcard_cb (GjInfoquery iq, GjInfoquery iq_related, GjRosterWindow *window)
{
	xmlChar *tmp = NULL;
  
	gchar *fn = NULL;
	gchar *given = NULL;
	gchar *family = NULL;
	gchar *nickname = NULL;

	GjRosterConnection *rc = window->connection;
	
	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"nickname");
	if (tmp != NULL) {
		nickname = g_strdup ((gchar*)tmp); 
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"fn");
	if (tmp != NULL) {
		fn = g_strdup ((gchar*)tmp); 
	}

	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"given");
	if (tmp != NULL) {
		given = g_strdup ((gchar*)tmp); 
	}
  
	tmp = gj_parser_find_by_node_and_ret (gj_iq_get_xml (iq), (guchar*)"family");
	if (tmp != NULL) {
		family = g_strdup ((gchar*)tmp);
	}

	/* deal with name - do we use it? */
	if (nickname != NULL) {
		g_free (rc->nick);
		rc->nick = g_strdup (nickname);
	} else if (fn != NULL) {
		g_free (rc->nick);
		rc->nick = g_strdup (fn);
	} else if (given != NULL) {
		gchar *new_name = NULL;
		
		new_name = g_strdup_printf ("%s%s%s", given, family ? " " : "", family ? family : "");

		g_free (rc->nick);
		rc->nick = new_name;
	} else {
		GjJID j = NULL;
		const gchar *to = NULL;

		to = gj_iq_get_to (iq);
		j = gj_jid_new (to);

		g_free (rc->nick);
		rc->nick = gj_jid_get_part_name_new (j);

		gj_jid_unref (j);
	}

	/* clean up */
	g_free (fn);
	g_free (given);
	g_free (family);
	g_free (nickname);
}

/*
 * GUI events
 */
static void on_treeview_row_inserted (GtkTreeModel *treemodel, GtkTreePath *path, GtkTreeIter *iter, GjRosterWindow *window)
{
	GtkTreeIter iter_new;
	GtkTreePath *path_new = NULL;
	GtkTreePath *path_to_emit = NULL;

	gboolean not_group = TRUE;
	gchar *group = NULL;
	gchar *title = NULL;
	guint count = 0;
	gchar *path_str = NULL;

	gchar *s1 = NULL;

	if (!window) {
		return; 
	}

	if (path == NULL) {
		g_warning ("%s: path was NULL", __FUNCTION__);
		return;
	}

	if (iter == NULL) {
		g_warning ("%s: iter was NULL", __FUNCTION__);
		return;
	}

	if (gtk_tree_model_iter_parent (GTK_TREE_MODEL (treemodel), &iter_new, iter) == FALSE) {
		g_message ("%s: iter is at the toplevel already, doing nothing...", __FUNCTION__);
		return;
	}
  
	if ((path_new = gtk_tree_model_get_path (GTK_TREE_MODEL (treemodel), &iter_new)) == NULL) {
		g_warning ("%s: could not get path from toplevel iterator", __FUNCTION__);
		return;
	}

	path_str = gtk_tree_path_to_string (path_new);

	if (path_str != NULL) {
		g_message ("%s: path is:'%s'", __FUNCTION__, path_str);
		g_free (path_str);
		path_str = NULL;
	}

	count = gtk_tree_model_iter_n_children (GTK_TREE_MODEL (treemodel), &iter_new);
	g_message ("%s: %d contacts in group", __FUNCTION__, count);

	gtk_tree_model_get (GTK_TREE_MODEL (treemodel), &iter_new,
			    COL_ROSTER_NAME, &group,
			    COL_ROSTER_NOT_GROUP, &not_group,
			    -1);
  
	if (not_group == TRUE) {
		return; 
	}

	s1 = g_markup_escape_text (group, -1);
	title = g_strdup_printf ("%s (%d)",s1,count);

	gtk_tree_store_set (GTK_TREE_STORE (treemodel), &iter_new,
			    COL_ROSTER_TEXT, title,
			    -1);

	path_to_emit = gtk_tree_model_get_path (treemodel, &iter_new);
	gtk_tree_model_row_changed (treemodel, path_to_emit, &iter_new);

	if (group != NULL && strcmp (group, ROSTER_GROUP_OFFLINE) != 0) {
		/* expand */
		if (gtk_tree_view_row_expanded (GTK_TREE_VIEW (window->treeview), path_new) == FALSE)
			gtk_tree_view_expand_row (GTK_TREE_VIEW (window->treeview), path_new, FALSE);
	}

	/* clean up */
	gtk_tree_path_free (path_to_emit);
	g_free (s1);
	g_free (group);
	g_free (title);
       
	return;
}

static void on_treeview_row_deleted (GtkTreeModel *treemodel, GtkTreePath *path, GjRosterWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeStore *store = NULL;
	GtkTreeIter iter_parent; 
  
	gchar *group = NULL; 
	gchar *title = NULL; 
	gchar *path_str = NULL;
	gchar **v_path_str = NULL;
	guint count = 0; 
	gboolean can_delete = FALSE;

	if (!window) {
		return;
	}

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_TREE_STORE (gtk_tree_view_get_model (view));

	if (path == NULL) { 
		g_warning ("%s: path was NULL", __FUNCTION__);
		return;
	}

	path_str = gtk_tree_path_to_string (path);
	if (path_str != NULL) {
		g_message ("%s: path is:'%s'", __FUNCTION__, path_str);

		/* hack for the get parent function which doesnt work correctly. */
		if ((v_path_str = g_strsplit (path_str, ":", -1)) == NULL) {
			g_warning ("%s: could not split path str:'%s'", __FUNCTION__, path_str);
		}

		/* clean up */
		g_free (path_str);
		path_str = NULL;

		if (v_path_str == NULL) {
			return;
		}

		if (v_path_str[1] != NULL) {
			can_delete = TRUE;
		}

		/* clean up */
		g_strfreev (v_path_str);
	}

	/* get parent */
	if (gtk_tree_path_up (path) == FALSE || can_delete == FALSE) {
		g_message ("%s: no parent associated with row deleted.", __FUNCTION__);
		return;
	}

	if (gtk_tree_model_get_iter (GTK_TREE_MODEL (treemodel), &iter_parent, path) == FALSE) {
		g_warning ("%s: could not get iter for parent associated with row deleted.", __FUNCTION__);
		return;
	}

	/* get count */
	count = gtk_tree_model_iter_n_children (GTK_TREE_MODEL (treemodel), &iter_parent);
	g_message ("%s: %d contacts in this group", __FUNCTION__, count);

	if (count < 1) {
		g_message ("%s: removing group, it is empty", __FUNCTION__);
	} else {
		GtkTreePath *path = NULL;
		gchar *s1 = NULL;

		gtk_tree_model_get (treemodel, &iter_parent,
				    COL_ROSTER_NAME, &group,
				    -1);

		s1 = g_markup_escape_text (group, -1);
		title = g_strdup_printf ("%s (%d)", s1, count);
      
		gtk_tree_store_set (store, &iter_parent,
				    COL_ROSTER_TEXT, title,
				    -1);

		path = gtk_tree_model_get_path (treemodel, &iter_parent);
		gtk_tree_model_row_changed (treemodel, path, &iter_parent);

		/* clean up */
		gtk_tree_path_free (path);
		g_free (s1);
		g_free (group);  
		g_free (title); 
	}
}

static gboolean on_treeview_button_press_event (GtkWidget *widget, GdkEventButton *gdk_event, GjRosterWindow *window)
{
	if (gdk_event->type == GDK_2BUTTON_PRESS && gdk_event->button == 1) {
		GjRosterItem ri = NULL;

		if ((ri = gj_gtk_roster_model_selection_get_ri (window)) == NULL) {
			return FALSE;
		}
      
		gj_gtk_roster_handle_next_event (window, ri);
		gj_gtk_roster_handle_next_event_display (ri);
	}

	return FALSE;
}

static gboolean on_treeview_button_release_event (GtkWidget *widget, GdkEventButton *gdk_event, GjRosterWindow *window)
{
	GjRosterItem ri = NULL;
	GjJID j = NULL;
  
	ri = gj_gtk_roster_model_selection_get_ri (window);

	if (gdk_event->type == GDK_BUTTON_RELEASE && gdk_event->button == 3) {
		/* 
		 * RIGHT SINGLE CLICK 
		 */
		gboolean not_group = TRUE;
		gchar *group = NULL;

		gj_gtk_roster_model_selection_get_is_group (window, &not_group, &group);
      
		if (not_group) {
			if (!ri) {
				return FALSE; 
			}

			j = gj_roster_item_get_jid (ri);

			if (gj_jid_get_type (j) != GjJIDTypeAgent) {
				gj_gtk_roster_generate_mnu_user (window, ri, gdk_event);
			} else {
				gj_gtk_roster_generate_mnu_agent (window, ri, gdk_event);
			}
		} else {
			if (group != NULL &&
			    strcmp (group, ROSTER_GROUP_OFFLINE) != 0 &&
			    strcmp (group, ROSTER_GROUP_RESOURCE) != 0 &&
			    strcmp (group, ROSTER_GROUP_SUBSCRIBING) != 0 &&
			    strcmp (group, ROSTER_GROUP_AGENTS) != 0 &&
			    strcmp (group, ROSTER_GROUP_NOT_ON_ROSTER) != 0) {
				gj_gtk_roster_generate_mnu_group (window, gdk_event);
			}
		}
	}
	else if (gdk_event->type == GDK_2BUTTON_PRESS && gdk_event->button == 1) {
		/* 
		 * LEFT DOUBLE CLICK 
		 */
		gj_gtk_roster_handle_next_event (window, ri);
		gj_gtk_roster_handle_next_event_display (ri);
	}
  
	return FALSE;
}

static void on_mnu_jabber_connect_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_connect (window);
}

static void on_mnu_jabber_disconnect_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_iq_request_goodbye (window->connection->c,
			       window->connection->server);
}

static void on_mnu_jabber_register_new_account_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_na_load ();
}

static void on_mnu_jabber_unregister_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_unregister (window);
}

static void on_mnu_jabber_change_password_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_pc_load (window->connection->c);
}

static void on_mnu_jabber_preferences_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_prefs_load ();
}

static void on_mnu_jabber_connection_settings_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_cs_load ();
}

static void on_mnu_jabber_my_information_activate (GtkMenuItem *menuitem, GjRosterWindow *window) 
{
	gj_gtk_mi_load (window->connection->c);
}

static void on_mnu_jabber_quit_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gnome_jabber_quit ();
}

static void on_mnu_action_add_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_ac_load (window->connection->c,
			roster, 
			NULL);
}

static void on_mnu_action_send_message_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_sm_load (window->connection->c,
			window->connection->jid,
			roster);
}

static void on_mnu_action_event_viewer_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_ev_load (TRUE);
}

static void on_mnu_action_group_chat_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_gcj_load (window->connection->c,
			 window->connection->jid);
}

static void on_mnu_action_edit_status_messages_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_pp_load ();
}

static void on_mnu_action_directory_search_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	/*   gj_gtk_ds_load (); */
}

static void on_mnu_action_browse_services_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_bs_load (window->connection->c);
}

static void on_mnu_help_homepage_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_open_url ("http://gnome-jabber.sourceforge.net/");
}

static void on_mnu_help_project_information_activate (GtkMenuItem *menuitem, GjRosterWindow *window) {
	gj_gtk_open_url (PACKAGE_BUGREPORT);
}

static void on_mnu_help_log_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_log_load ();
}

static void on_mnu_help_debug_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_debug_load (window->connection->c);
}

static void on_mnu_help_about_activate (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_about_load (_("About"),
			   PACKAGE_NAME,
			   "Instant Messenger using the Jabber Protocol",
			   VERSION, /* major version */
			   BUILD_ID,
			   "Protected Under the GNU General Public License.\n"
			   "http://www.gnu.org/licenses/gpl.txt",
			   "http://sourceforge.net/projects/gnome-jabber/\n"
			   "http://www.jabber.org",
			   NULL,
			   authors,
			   documenters,
			   translators);
}

static void gj_gtk_roster_menu_contact_read_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_read (window);
}

static void gj_gtk_roster_menu_contact_new_message_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_new_message (window);
}

static void gj_gtk_roster_menu_contact_new_chat_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_new_chat (window);
}

static void gj_gtk_roster_menu_contact_file_transfer_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_file_transfer (window);
}

static void gj_gtk_roster_menu_contact_headlines_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_headlines (window);
}

static void gj_gtk_roster_menu_contact_history_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_history (window);
}

static void gj_gtk_roster_menu_contact_information_cb (GtkMenuItem *menuitem, GjRosterWindow *window) {
	gj_gtk_roster_contact_information (window);
}

static void gj_gtk_roster_menu_contact_sub_request_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_sub_request (window);
}

static void gj_gtk_roster_menu_contact_sub_auth_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_sub_auth (window);
}

static void gj_gtk_roster_menu_contact_sub_cancel_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_sub_cancel (window);
}

static void gj_gtk_roster_menu_contact_edit_groups_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_edit_groups (window);
}

static void gj_gtk_roster_menu_contact_rename_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_rename (window);
}

static void gj_gtk_roster_menu_contact_add_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_add (window);
}

static void gj_gtk_roster_menu_contact_remove_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_contact_remove (window);
}

static void gj_gtk_roster_menu_agent_log_in_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_agent_log_in (window);
}

static void gj_gtk_roster_menu_agent_log_out_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_agent_log_out (window);
}

static void gj_gtk_roster_menu_group_message_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_group_message (window);
}

static void gj_gtk_roster_menu_group_rename_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_group_rename (window);
}

static void gj_gtk_roster_menu_group_remove_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gj_gtk_roster_group_remove (window);
}

static void gj_gtk_roster_menu_presence_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gint callback_action = -1;

	gpointer pid = NULL;
	gint id = 0;

	pid = g_object_get_data (G_OBJECT (menuitem), "id");
	id = GPOINTER_TO_INT (pid);
	
	g_message ("%s: agent menu activated menuitem:0x%.8x, callback action:%d", 
		   __FUNCTION__, (gint)menuitem, id);

	switch (id) {
	case GjPresenceShowNormal:
		gj_gtk_roster_set_my_presence_and_save (window, 
							GjPresenceTypeAvailable, 
							GjPresenceShowNormal, 
							window->connection->presence_status,
							window->connection->presence_priority,
							TRUE);
		break;
	case GjPresenceShowChat:
		gj_gtk_roster_set_my_presence_and_save (window,
							GjPresenceTypeAvailable, 
							GjPresenceShowChat, 
							window->connection->presence_status,
							window->connection->presence_priority,
							TRUE);
		break;
	case GjPresenceShowAway:
		gj_gtk_roster_set_my_presence_and_save (window, 
							GjPresenceTypeAvailable, 
							GjPresenceShowAway, 
							window->connection->presence_status,
							window->connection->presence_priority,
							TRUE);
		break;
	case GjPresenceShowXA:
		gj_gtk_roster_set_my_presence_and_save (window, 
							GjPresenceTypeAvailable, 
							GjPresenceShowXA, 
							window->connection->presence_status,
							window->connection->presence_priority,
							TRUE);
		break;
	case GjPresenceShowDND:
		gj_gtk_roster_set_my_presence_and_save (window,
							GjPresenceTypeAvailable, 
							GjPresenceShowDND, 
							window->connection->presence_status,
							window->connection->presence_priority,
							TRUE);
		break;
	case GjPresenceShowInvisible:
		gj_gtk_roster_set_my_presence_and_save (window,
							GjPresenceTypeAvailable, 
							GjPresenceShowInvisible, 
							window->connection->presence_status,
							window->connection->presence_priority,
							TRUE);
		break;
	}
}

static void gj_gtk_roster_menu_presence_preset_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	gchar *show = NULL;
	gchar *status = NULL;
	gchar *priority = NULL;

	GjPresenceShow show_int = 0;
	gint priority_int = 0;

	gpointer pid = NULL;
	gint id = 0;
	
	pid = g_object_get_data (G_OBJECT (menuitem), "id");
	
	if (!pid) {
		return;
	}

	id = GPOINTER_TO_INT (pid);

	if (!gj_config_get_pp_index (id, &show, &status, &priority)) {
		return;
	}

	priority_int = atoi (priority);
	show_int = gj_translate_presence_show_str (show);

	gj_gtk_roster_set_my_presence_and_save (window, 
						GjPresenceTypeAvailable, 
						show_int, 
						status,
						priority_int,
						TRUE);
}

static void gj_gtk_roster_menu_show_window_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	if (!window) {
		return;
	}

	if (!GTK_WIDGET_VISIBLE (window->window)) {
		gtk_window_present (GTK_WINDOW (window->window));
		gdk_window_raise (GDK_WINDOW (window->window->window));
	} else {
		gtk_widget_hide (window->window);
	}
}

static void gj_gtk_roster_menu_quit_cb (GtkMenuItem *menuitem, GjRosterWindow *window)
{
	if (!window) {
		return;
	}

	gtk_widget_destroy (window->window);
}


static gboolean on_eventbox_statusbar_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window)
{
	if (event->type == GDK_BUTTON_PRESS && event->button == 1) {
		gj_gtk_ev_load (TRUE);
	}

	return FALSE;
}

static void on_button_error_clicked (GtkButton *button, GjRosterWindow *window)
{
	gj_gtk_ev_load (TRUE);
}

static void gj_gtk_roster_presence_image_align_cb (GtkMenu *menu, 
						   gint *x, 
						   gint *y, 
						   gboolean *push_in, 
						   GjRosterWindow *window)
{
	GtkWidget *widget = NULL;
	GtkRequisition req;
	GdkScreen *screen = NULL;
	gint width = 0;
	gint height = 0;
	gint screen_height = 0;
  
	widget = window->image_presence;
	
	gtk_widget_size_request (GTK_WIDGET (menu), &req);
  
	gdk_window_get_origin (widget->window, x, y);
	gdk_drawable_get_size (widget->window, &width, &height);
  
	*x -= req.width - width;
	*y += height;
  
	screen = gtk_widget_get_screen (GTK_WIDGET (menu));
  
	/* Clamp to screen size. */
	screen_height = gdk_screen_get_height (screen) - *y;	

	if (req.height > screen_height) {
		/* It doesn't fit, so we see if we have the minimum space needed. */
		if (req.height > screen_height && *y - height > screen_height) {
			/* Put the menu above the button instead. */
			screen_height = *y - height;
			*y -= (req.height + height);

			if (*y < 0) {
				*y = 0;
			}
		}
	}
}


static void on_eventbox_presence_image_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window)
{
	if (event->type == GDK_BUTTON_PRESS && event->button == 1) {
		GtkWidget *menu = NULL;
		GtkWidget *item = NULL;
		
		gint i = 0;
		
		menu = gj_gtk_presence_menu_new ((GjMenuCallback)gj_gtk_roster_menu_presence_cb, window);

		item = gtk_separator_menu_item_new ();
		gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), item);

		/* should be done better */
		for (i = 1; i < 100; i++) {
			gchar *show = NULL;
			gchar *status = NULL;
			gchar *priority = NULL;
			
			GjPresenceShow show_int = 0;
			gint priority_int = 0;

			const gchar *stock_id = NULL;
			GtkWidget *image = NULL;
			
			if (!gj_config_get_pp_index (i, &show, &status, &priority)) {
				break;
			}
			
			priority_int = atoi (priority);
			show_int = gj_translate_presence_show_str (show);
			stock_id = gj_gtk_presence_to_stock_id (GjPresenceTypeAvailable, show_int);

			item = gtk_image_menu_item_new_with_label (status);
			image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_MENU);
			gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item), image);
			gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), item);
			g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (i));
			g_signal_connect ((gpointer) item, "activate", 
					  G_CALLBACK (gj_gtk_roster_menu_presence_preset_cb), window);
		}

		gtk_widget_show_all (menu);

		gtk_menu_popup (GTK_MENU (menu), NULL, NULL, 
				(GtkMenuPositionFunc)gj_gtk_roster_presence_image_align_cb, 
				window, 1, gtk_get_current_event_time ());
		
		if (menu) {
			gtk_object_sink (GTK_OBJECT (menu));
		}
	}
}

static void on_eventbox_presence_text_button_press_event (GtkWidget *widget, GdkEventButton *event, GjRosterWindow *window)
{
	if (!window) {
		return;
	}

	if (event->type == GDK_BUTTON_PRESS && event->button == 1) {
		gtk_widget_hide (window->label_presence);
		gtk_widget_show (window->entry_status);
		
		if (window->connection->presence_status) {
			gtk_entry_set_text (GTK_ENTRY (window->entry_status), 
					    window->connection->presence_status);
		}
		
		gtk_widget_grab_focus (window->entry_status);
	}
}

static void on_entry_status_activate (GtkEntry *entry, GjRosterWindow *window)
{
	G_CONST_RETURN gchar *text = NULL;

	if (!window) {
		return;
	}

	gtk_widget_hide (window->entry_status);
	gtk_widget_show (window->label_presence);

	/* get text */
	text = gtk_entry_get_text (entry);

	if (text && strlen (text) < 1) {
		text = NULL;
	}

	/* set presence */
	gj_gtk_roster_set_my_presence_and_save (window, 
						window->connection->presence_type, 
						window->connection->presence_show, 
						text,
						window->connection->presence_priority,
						TRUE);
}

static gboolean on_entry_status_focus_out_event (GtkWidget *widget, GdkEventFocus *event, GjRosterWindow *window)
{
	if (!window) {
		return FALSE;
	}

	gtk_widget_hide (window->entry_status);
	gtk_widget_show (window->label_presence); 

	return FALSE;
}

static gboolean on_entry_status_key_release_event (GtkWidget *widget, GdkEventKey *event, GjRosterWindow *window)
{
	if (!window)
		return FALSE;

	if (event->keyval == GDK_Escape) {
		gtk_widget_hide (window->entry_status);
		gtk_widget_show (window->label_presence); 
	}

	return FALSE;
}

static gboolean on_focus_out_event (GtkWidget *widget,
				    GdkEventFocus *event,
				    GjRosterWindow *window)
{
	if (!window) {
		return FALSE;
	}
  
	/* WIN32 hack */
	if (window->menubar) {
		gtk_menu_shell_deactivate (GTK_MENU_SHELL (window->menubar));
	}

	return FALSE;
}

static void on_destroy (GtkWidget *widget, GjRosterWindow *window)
{
	current_window = NULL;

	if (!window) {
		return;
	}

	/* stop receiving events */
	gj_event_term ();

	/* stop receiving autoaway updates */
	gj_autoaway_term ();

	/* stop all flashings */
	if (window->flash_list == NULL) {
		gint i = 0;

		for (i=0; i<g_list_length (window->flash_list); i++) {
			gint ri_id = 0;
			gpointer data = NULL;
	  
			data = (gpointer) g_list_nth_data (window->flash_list, i);

			if (data == NULL) {
				continue;
			}
	  
			ri_id = GPOINTER_TO_INT (data);
			window->flash_list = g_list_remove (window->flash_list, data);
		}

		g_free (window->flash_list);
	}

	if (window->flash_timeout_id != -1) {
		g_source_remove (window->flash_timeout_id);
		window->flash_timeout_id = -1;
	}

	if (window->connection) {
		gj_gtk_roster_connect_cleanup (window);
		window->connection = NULL;
	}

	/* remove icon from systray/notification area */
	gj_gtk_docklet_unload ();

	g_free (window);

	/* quit app */
	gnome_jabber_quit ();
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjRosterWindow *window)
{
	gtk_widget_destroy (window->window);
 
	return TRUE;
}

#ifdef __cplusplus
}
#endif
