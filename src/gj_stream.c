/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include <glib.h>

#include <libxml/tree.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_stream.h"
#include "gj_parser.h"
#include "gj_connection.h"
#include "gj_support.h"

#include "gj_gtk_support.h"


struct t_stream
{
	gint request_id;

	GjStreamCallback connected_cb;
	gpointer connected_user_data;

	GjStreamCallback disconnected_cb;
	gpointer disconnected_user_data;

	GjStreamCallback error_cb;
	gpointer error_user_data;

	gboolean is_built;
	gboolean is_set_for_removal;
	gboolean is_disconnect_planned;

	gchar *id;
	gchar *stream;
	gchar *to;

	guint unique_id;

	guint port;

	GjConnection c;
	GjParser p; 

	/*xmlDocPtr doc;*/
	xmlDocPtr xml;
};


static void gj_stream_init ();

/*
 * member functions
 */

/* sets */
static gboolean gj_stream_set_id (GjStream stm, const gchar *id);

static gboolean gj_stream_set_xml (GjStream stm, xmlDocPtr doc);

static void gj_stream_set_is_built (GjStream stm, gboolean is_built);
static void gj_stream_set_is_set_for_removal (GjStream stm, gboolean is_set_for_removal);

/* gets */
static gboolean gj_stream_get_is_built (GjStream stm);
static gboolean gj_stream_get_is_set_for_removal (GjStream stm);

/* utils */
static gchar *gj_stream_convert_doc_to_str (GjStream stm);

/* callbacks */
static void gj_stream_connected_cb (GjConnection c);
static void gj_stream_disconnected_cb (GjConnection c);
static void gj_stream_error_cb (GjConnection c, GTcpSocketConnectAsyncStatus status);

/* find */
static GjStream gj_stream_find_by_to (const gchar *to); 

/*
 * member functions
 */

/* set */

/* get */
static gint gj_stream_get_request_id (GjStream stm);

static const gchar *gj_stream_get_to (GjStream stm);
static guint gj_stream_get_port (GjStream stm);

static GjStreamCallback gj_stream_get_disconnected_cb (GjStream stm);
static GjStreamCallback gj_stream_get_error_cb (GjStream stm);

static xmlDocPtr gj_stream_get_xml (GjStream stm);

/* 
 * debugging 
 */


static GList *stream_list = NULL;


static void gj_stream_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;
}

GjStream gj_stream_new (const gchar *host)
{
	GjStream stm = NULL;

	if (host == NULL) {
		g_warning ("%s: host was NULL", __FUNCTION__);
		return NULL;
	}

	/* find stream for this host */
	stm = gj_stream_find_by_to (host);

	/* is it part of a reconnect (i.e. is the old going to be removed ?) */
	if (stm != NULL && gj_stream_get_is_set_for_removal (stm) == FALSE) {
		g_warning ("%s: there is already a stream to:'%s'", __FUNCTION__, host);
		return NULL;
	}

	gj_stream_init ();
  
	if ((stm = (GjStream) g_new0 (struct t_stream,1))) {
		stm->to = g_strdup (host);

		stm->request_id = gj_get_unique_id (); /* get next */

		stm->xml = NULL;

		stm->connected_cb = NULL;
		stm->disconnected_cb = NULL;
		stm->error_cb = NULL;

		stm->is_built = FALSE;
		stm->is_set_for_removal = FALSE;
		stm->is_disconnect_planned = FALSE;

		stm->id = NULL;
		stm->stream = NULL;

		stm->port = 5222;

		stm->c = NULL;

		stm->unique_id = gj_get_unique_id ();

		stream_list = g_list_append (stream_list, stm);
	}
  
	return stm;  
}

gboolean gj_stream_free (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	stream_list = g_list_remove (stream_list, stm);

	if (stm->xml != NULL) {
		xmlFreeDoc (stm->xml);
		stm->xml = NULL;
	}
	
	g_free (stm->id);
	g_free (stm->to);
	g_free (stm->stream);
	
	if (stm->c != NULL) {
		gj_connection_unref (stm->c);
		stm->c = NULL;
	}

	if (stm->p != NULL) {
		gj_parser_free (stm->p);
		stm->p = NULL;
	}

	g_free (stm);

	return TRUE;
}

GjStream gj_stream_find_by_connection (GjConnection c)
{
	gint index = 0;

	if (c == NULL) {
		g_warning ("%s: GjConnection was NULL", __FUNCTION__);
		return NULL;
	}

	for (index=0;index<g_list_length (stream_list);index++) {
		GjStream stm = (GjStream) g_list_nth_data (stream_list,index);

		if (stm && stm->c == c) {
			return stm; 
		}
	}
  
	return NULL;
}

static GjStream gj_stream_find_by_to (const gchar *to)
{
	gint index = 0;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return NULL;
	}

	for (index=0;index<g_list_length (stream_list);index++) {
		GjStream stm = (GjStream) g_list_nth_data (stream_list,index);

		if (stm && strcmp (stm->to,to) == 0) {
			return stm; 
		}
	}
  
	return NULL;
}

/*
 * member functions
 */

/* sets */
static gboolean gj_stream_set_id (GjStream stm, const gchar *id)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return FALSE;
	}

	g_free (stm->id);
  	stm->id = g_strdup (id);
	
	return TRUE;
}

gboolean gj_stream_set_port (GjStream stm, guint port)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (port < 1024) {
		g_warning ("%s: port:%d < 1024", __FUNCTION__, port);
		return FALSE;
	}

	stm->port = port;
	return TRUE;
}

static gboolean gj_stream_set_xml (GjStream stm, xmlDocPtr doc)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (doc == NULL) {
		g_warning ("%s: failed, XML was NULL", __FUNCTION__);
		return FALSE;
	}

	if (stm->xml != NULL) {
		xmlFreeDoc (stm->xml);
		stm->xml = NULL;
	}

	if ((stm->xml = xmlCopyDoc (doc,TRUE)) == NULL) {
		g_warning ("%s: xmlCopyDoc failed", __FUNCTION__);
		return FALSE;
	}
 
	return TRUE;
}

static void gj_stream_set_is_built (GjStream stm, gboolean is_built)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return;
	}

	stm->is_built = is_built;
	return;
}

static void gj_stream_set_is_set_for_removal (GjStream stm, gboolean is_set_for_removal) {
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return;
	}

	stm->is_set_for_removal = is_set_for_removal;
	return;
}

void gj_stream_set_is_disconnect_planned (GjStream stm, gboolean is_disconnect_planned)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return;
	}

	stm->is_disconnect_planned = is_disconnect_planned;
	return;
}

gboolean gj_stream_set_connected_cb (GjStream stm, GjStreamCallback cb, gpointer user_data)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	stm->connected_cb = cb;
	stm->connected_user_data = user_data;
	return TRUE;
}

gboolean gj_stream_set_disconnected_cb (GjStream stm, GjStreamCallback cb, gpointer user_data)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	stm->disconnected_cb = cb;
	stm->disconnected_user_data = user_data;
	return TRUE;
}

gboolean gj_stream_set_error_cb (GjStream stm, GjStreamCallback cb, gpointer user_data)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	stm->error_cb = cb;
	stm->error_user_data = user_data;
	return TRUE;
}

/* gets */
const gchar *gj_stream_get_id (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->id;
}

static const gchar *gj_stream_get_to (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->to;
}

static guint gj_stream_get_port (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->port;
}

static GjStreamCallback gj_stream_get_disconnected_cb (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->disconnected_cb;
}

static GjStreamCallback gj_stream_get_error_cb (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->error_cb;
}

static xmlDocPtr gj_stream_get_xml (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->xml;
}

static gint gj_stream_get_request_id (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return -1;
	}

	return stm->request_id;
}

static gboolean gj_stream_get_is_built (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->is_built;
}

static gboolean gj_stream_get_is_set_for_removal (GjStream stm) 
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->is_set_for_removal;
}

gboolean gj_stream_get_is_disconnect_planned (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	return stm->is_disconnect_planned;
}

GjConnection gj_stream_get_connection (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return NULL;
	}

	return stm->c;
}

/* 
 * compile stream
 */
gboolean gj_stream_open (GjStream stm)
{
	GjConnection c = NULL;
	GjParser p = NULL;

	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	} 

	if (gj_stream_get_is_built (stm) == FALSE) {
		g_warning ("%s: stream is not built, not continuing...", __FUNCTION__);
		return FALSE;
	}

	if ((c = gj_connection_create (gj_stream_get_to (stm), gj_stream_get_port (stm))) == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	g_message ("%s: connection:0x%.8x created, host:'%s', port:%d", 
		   __FUNCTION__, (gint)c, gj_stream_get_to (stm), gj_stream_get_port (stm));

	/* open tcp GjConnection and make GjConnection */
	gj_connection_set_cb_connect (c, gj_stream_connected_cb);
	gj_connection_set_cb_disconnect (c, gj_stream_disconnected_cb);
	gj_connection_set_cb_error (c, gj_stream_error_cb);
	gj_connection_set_cb_in (c, gj_parser_push_input_from_connection);

	p = gj_parser_new (c);

	/* set connection */
	stm->c = c;
	stm->p = p;

	if (gj_connection_connect (c) == FALSE) {
		gj_parser_free (p);
		gj_connection_unref (c);

		g_warning ("%s: failed to connect (locally)", __FUNCTION__);
		return FALSE;
	}

	return TRUE;
}

gboolean gj_stream_close (GjStream stm)
{
	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}    

	/* set planned disconnect - stops reconnecting */
	gj_stream_set_is_disconnect_planned (stm, TRUE);

	gj_connection_write (stm->c, "</stream:stream>");
    
	/* free off stream ?? */

	return TRUE;
}

gboolean gj_stream_close_by_to (const gchar *to)
{
	GjStream stm = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((stm = gj_stream_find_by_to (to)) == NULL) {
		g_warning ("%s: could not find stream by to:'%s'", __FUNCTION__, to);
		return FALSE;
	}

	return gj_stream_close (stm);
}

/*
 * NOTE: the connected function means we have a socket GjConnection NOT another
 *       stream from the server.  the connected callback is called at the 
 *       'stream receive' function.
 */
void gj_stream_connected_cb (GjConnection c)
{
	gchar *msg = NULL;
	GjStream stm = NULL;

	if ((stm = gj_stream_find_by_connection (c)) == NULL) {
		g_warning ("%s: could not find stream from connection", __FUNCTION__);
		return;
	}

	/* use stream and introduce ourselves... */
	if ((msg = gj_stream_convert_doc_to_str (stm)) == NULL) {
		g_warning ("%s: failed to convert doc to string, NULL returned", __FUNCTION__);
		return;
	}

	g_message ("%s: stream is connected", __FUNCTION__);
	gj_connection_write (c, msg);  

	/* clean up */
	g_free (msg);
}

void gj_stream_disconnected_cb (GjConnection c)
{
	GjStream stm = NULL;
	GjStreamCallback cb = NULL;

	if ((stm = gj_stream_find_by_connection (c)) == NULL) {
		g_warning ("%s: could not find stream from connection", __FUNCTION__);
		return;
	}  
  
	gj_stream_set_is_set_for_removal (stm, TRUE);

	/* send up */
	if ((cb = (GjStreamCallback) gj_stream_get_disconnected_cb (stm)) == NULL) {
		g_message ("%s: no callback for disconnection!", __FUNCTION__);
		return;
	}

	(cb) (stm, 0, NULL, stm->disconnected_user_data);

	/* free stream */
	gj_stream_free (stm);
}

void gj_stream_error_cb (GjConnection c, GTcpSocketConnectAsyncStatus status)
{
	GjStream stm = NULL;
	GjStreamCallback cb = NULL;

	if ((stm = gj_stream_find_by_connection (c)) == NULL) {
		g_warning ("%s: could not find stream from connection", __FUNCTION__);
		return;
	}  

	gj_stream_set_is_set_for_removal (stm, TRUE);
  
	/* send up */
	if ((cb = (GjStreamCallback) gj_stream_get_error_cb (stm)) == NULL) {
		g_message ("%s: no callback for error!", __FUNCTION__);
		return;
	}

	(cb) (stm, status, gj_connection_connect_translate_status (status), stm->error_user_data);

	/* free stream */
	gj_stream_free (stm);
}

gboolean gj_stream_build (GjStream stm)
{
	xmlDocPtr doc = NULL;
	xmlNsPtr ns = NULL;

	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_stream_get_to (stm) == NULL) {
		g_warning ("%s: 'to' field was NULL", __FUNCTION__);
		return FALSE;
	}

	/* create new doc */
	if ((doc = xmlNewDoc ((guchar*)"1.0")) == NULL) {
		/* failed */
		g_warning ("%s: xmlNewDoc failed", __FUNCTION__);
		return FALSE;
	}
  
	ns = xmlNewNs (NULL, (guchar*)"stream", (guchar*)"stream");
	doc->xmlChildrenNode = xmlNewDocNode (doc, ns, (guchar*)"stream", NULL);

	ns = xmlNewNs (doc->xmlChildrenNode, (guchar*)"http://etherx.jabber.org/streams", (guchar*)"stream");
	xmlSetProp (doc->xmlChildrenNode, (guchar*)"to", (guchar*)gj_stream_get_to (stm));
	xmlSetProp (doc->xmlChildrenNode, (guchar*)"xmlns", (guchar*)"jabber:client");
 
	gj_stream_set_xml (stm, doc);
	/* xmlAddChild (doc->xmlChildrenNode,gj_stream_get_xml (stm));*/

	/* set flag to say stream is built */
	gj_stream_set_is_built (stm,TRUE);

	/* free doc ? */

	return TRUE;
}

static gchar *gj_stream_convert_doc_to_str (GjStream stm)
{
	xmlChar *buf;
	gint size = 0;
	gchar *output = NULL;

	if (stm == NULL) {
		g_warning ("%s: stream was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_stream_get_is_built (stm) == FALSE) {
		g_warning ("%s: stream is not built, not continuing...", __FUNCTION__);
		return FALSE;
	}

	if (gj_stream_get_xml (stm) == NULL) {
		g_warning ("%s: 'xml' is NULL", __FUNCTION__);
		return FALSE;
	}

	xmlDocDumpMemory (gj_stream_get_xml (stm),&buf,&size); 
	/*   buf = g_strdup_printf ("<?xml version='1.0'?><stream:stream to='%s' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams'>", gj_stream_get_to (stm)); */

	if (buf == NULL) {
		g_warning ("%s: xmlDocDumpMemory failed", __FUNCTION__);
		return NULL;
	}

	/* add a fix for end of xml tag */
	buf[(size-3)] = '>'; 
	buf[(size-2)] = ' '; 

	output = g_strdup_printf ("%s",buf);

	/* need to free buf... but free crashes it */

	return output;
}

void gj_stream_receive (GjConnection c, gchar *from, gchar *id)
{
	GjStream stm = NULL;
	gint request_id = -1;

	/* find stream */
	if ((stm = gj_stream_find_by_connection (c)) == NULL) {
		g_warning ("%s: failed to find stream based on connection:0x%.8x", __FUNCTION__, (gint)c);
		return;
	}
     
	/* set id */
	gj_stream_set_id (stm, id);
  
	/* get call back information */
	if(stm->connected_cb == NULL) {
		g_warning ("%s: connected callback is NULL", __FUNCTION__);
		return;
	}
  
	/* get request_id - required for callback */
	request_id = gj_stream_get_request_id (stm);
  
	/* call callback function */
	(stm->connected_cb) (stm, request_id, NULL, stm->connected_user_data);
}

void gj_stream_receive_error (GjConnection c, xmlNodePtr node)
{
	gchar *tmp = NULL;

	if (c == NULL) {
		g_warning ("%s: connection is NULL", __FUNCTION__);
		return;
	}

	if (node == NULL) {
		g_warning ("%s: node is NULL", __FUNCTION__);
		return;
	}

	if ((tmp = (gchar*)gj_parser_get_content (node)) == NULL) {
		g_warning ("%s: unknown error", __FUNCTION__);
		return;
	}

	gj_gtk_show_error (_("Stream Error."), 0, tmp);
}

void gj_stream_receive_closing (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection is NULL", __FUNCTION__);
		return;
	}

	/* do something */
}

#ifdef __cplusplus
}
#endif
