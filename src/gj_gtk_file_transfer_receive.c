/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_jid.h"
#include "gj_file_transfer.h"
#include "gj_connection.h"

#include "gj_gtk_file_transfer_receive.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *label_jid;

	GtkWidget *label_file_name;
	GtkWidget *label_file_size;
	GtkWidget *label_description;

	/* 
	 * members 
	 */

	GjConnection c;
	GjFileTransfer ft;

} GjFileTransferReceiveDialog;


static void on_destroy (GtkWidget *widget, GjFileTransferReceiveDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjFileTransferReceiveDialog *dialog);


static GjFileTransferReceiveDialog *current_dialog = NULL;


gboolean gj_gtk_file_transfer_receive_load (GjConnection c, GjFileTransfer ft)
{
	GjFileTransferReceiveDialog *dialog = NULL;
	GladeXML *xml = NULL;

	gchar *size_in_units = NULL;

	GjJID jid = NULL;

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (!c) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (!ft) {
		g_warning ("%s: file transfer was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjFileTransferReceiveDialog, 1);

	dialog->c = gj_connection_ref (c);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "file_transfer_receive",
				     NULL,
				     "file_transfer_receive", &dialog->window,
				     "label_jid", &dialog->label_jid,
				     "label_file_name", &dialog->label_file_name,
				     "label_file_size", &dialog->label_file_size,
				     "label_description", &dialog->label_description,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "file_transfer_receive", "response", on_response,
			      "file_transfer_receive", "destroy", on_destroy,
			      NULL);

	g_object_unref (xml);
 
	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set up window */
	if (gj_file_transfer_get_file_size (ft)) {
		gint i = 0;
		const gchar *bytes = NULL;
		gfloat f = 0;
		const gchar* units[] = {"bytes", "K", "MB", "GB", "TB"};
      
		bytes = gj_file_transfer_get_file_size (ft);

		if (bytes) { 
			f = atoi (bytes);
		}

		for (i = 0; f > 1024; i++) {
			f /= 1024.0;
		}
      
		if (i > 0) {
			size_in_units = g_strdup_printf ("%.1f %s", f, units[i]);
		} else {
			size_in_units = g_strdup_printf ("%d %s", atoi (bytes), units[i]); 
		}
	}


	jid = gj_file_transfer_get_from_jid (ft);

	gtk_label_set_text (GTK_LABEL (dialog->label_jid), gj_jid_get_full (jid));

	gtk_label_set_text (GTK_LABEL (dialog->label_file_name), gj_file_transfer_get_file_name (ft));
	gtk_label_set_text (GTK_LABEL (dialog->label_file_size), size_in_units);

	gtk_label_set_text (GTK_LABEL (dialog->label_description), gj_file_transfer_get_description (ft));

	g_free (size_in_units);

	dialog->ft = gj_file_transfer_ref (ft);

	return TRUE;
}

static void on_destroy (GtkWidget *widget, GjFileTransferReceiveDialog *dialog)
{
	current_dialog = NULL;

	if (!dialog) {
		return;
	}

	if (dialog->ft) {
		gj_file_transfer_unref (dialog->ft);
	}

	gj_connection_unref (dialog->c);

	g_free (dialog);   
}

static void on_response (GtkDialog *widget, gint response, GjFileTransferReceiveDialog *dialog)
{
	if (response == GTK_RESPONSE_ACCEPT) {
		gj_file_transfer_accept (dialog->c, dialog->ft);
	} else {
		gj_file_transfer_reject (dialog->c, dialog->ft);
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
