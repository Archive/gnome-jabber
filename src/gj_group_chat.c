/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include "gj_group_chat.h"
#include "gj_roster_item.h"
#include "gj_jid.h"


static GList *group_chats = NULL;


/*
 * roster items
 */
gboolean gj_group_chat_add (GjRosterItem ri) 
{
	GjJID j = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
  
	if (gj_group_chat_find (j) == NULL) {
		group_chats = g_list_append (group_chats, ri);
	}

	g_message ("%s: list size:%d", __FUNCTION__, g_list_length (group_chats));

	return TRUE;
}

gboolean gj_group_chat_del (GjRosterItem ri)
{
	GjJID j = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	group_chats = g_list_remove (group_chats, ri);

	g_message ("%s: list size:%d", __FUNCTION__, g_list_length (group_chats));

	return TRUE;
}

GjRosterItem gj_group_chat_find (GjJID j)
{
	gboolean check_without_resources = TRUE;
	gint i = 0;

	if (j == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	/* if the JID we are searching IS the JID for the roster
	   (i.e. the logged on user) we dont search without the resource
	   because this could be a search for the JID *WITH* the resource. */
	for (i=0; i<g_list_length (group_chats); i++) {
		GjRosterItem this_ri = NULL;
		
		this_ri = (GjRosterItem) g_list_nth_data (group_chats, i);

		if (this_ri == NULL) { 
			continue;
		}

		if (gj_jid_equals (gj_roster_item_get_jid (this_ri), j) == TRUE) {
			return this_ri;
		}
	} 

	/* try WITHOUT the resource */
	for (i=0; check_without_resources && i<g_list_length (group_chats); i++) {
		GjRosterItem this_ri = NULL;
      
		this_ri = (GjRosterItem) g_list_nth_data (group_chats, i);

		if (this_ri == NULL) {
			continue;
		}

		if (gj_jid_equals_without_resource (gj_roster_item_get_jid (this_ri), j) == TRUE) {
			return this_ri; 
		}
	} 

	return NULL;
}


/*
 * get lists/tables 
 */
const GList *gj_group_chat_get_list ()
{
	return group_chats;
}

#ifdef __cplusplus
}
#endif
