/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_translate.h"
#include "gj_event.h"
#include "gj_support.h"
#include "gj_config.h"
#include "gj_jid.h"
#include "gj_chat.h"
#include "gj_connection.h"

#include "gj_gtk_message.h" 
#include "gj_gtk_roster.h"
#include "gj_gtk_preferences.h"
#include "gj_gtk_support.h"
#include "gj_gtk_stock.h"


struct t_GjMessageWindow {
	GtkWidget *window;

	GtkWidget *entry_contact;
	GtkWidget *entry_time;
	GtkWidget *hbox_time;
	GtkWidget *entry_subject;
	GtkWidget *textview;
  
	GtkWidget *label_subject;
	GtkWidget *label_contact;
	GtkWidget *label_next;

	GtkWidget *button_next;
	GtkWidget *button_send;
	GtkWidget *button_reply;
	GtkWidget *button_close;

	/* members */
	GjRosterItem ri;

	/* properties */
	GjMessageWindowType type;

	gint last_msg_recv_xevent_type;
	gchar *last_msg_recv_xevent_id;

	gint last_msg_sent_xevent_type;
	gchar *last_msg_sent_xevent_id;

	GjJID connected_jid;
	GjConnection c;
};


/* window functions */
static GjMessageWindow *gj_gtk_message_load (GjConnection c, 
					     GjJID connected_jid, 
					     GjRosterItem ri, 
					     GjMessageWindowType type);

/* utilities */
static gint gj_gtk_message_waiting_count (GjMessageWindow *window);
static void gj_gtk_message_waiting_update (GjMessageWindow *window);

static gboolean gj_gtk_message_send (GjMessageWindow *window);
static gboolean gj_gtk_message_receive (GjMessageWindow *window, GjMessage m);

static gboolean gj_gtk_message_handle_message (GjMessageWindow *window, GjMessage m);
gboolean gj_gtk_message_waiting_next (GjMessageWindow *window);

/* gui callbacks */
static void on_textbuffer_changed (GtkTextBuffer *textbuffer, GjMessageWindow *window);
static gboolean on_textview_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjMessageWindow *window);
static gboolean on_textview_button_press_event (GtkWidget *widget, GdkEventButton *event, GjMessageWindow *window);
 
static void on_button_send_clicked (GtkButton *button, GjMessageWindow *window);
static void on_button_reply_clicked (GtkButton *button, GjMessageWindow *window);
static void on_button_next_clicked (GtkButton *button, GjMessageWindow *window);
static void on_button_close_clicked (GtkButton *button, GjMessageWindow *window);

static void on_realize (GtkWidget *widget, GjMessageWindow *window);
static void on_destroy (GtkWidget *widget, GjMessageWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjMessageWindow *window);


static GList *message_windows = NULL;


static GjMessageWindow *gj_gtk_message_load (GjConnection c, 
					     GjJID connected_jid, 
					     GjRosterItem ri, 
					     GjMessageWindowType type)
{
	GladeXML *xml = NULL;
	GjMessageWindow *window = NULL;

	GdkPixbuf *pb = NULL;
	GtkTextBuffer *buffer = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	if (connected_jid == NULL) {
		g_warning ("%s: connected_jid was NULL", __FUNCTION__);
		return NULL;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	window = g_new0 (struct t_GjMessageWindow,1);

	window->ri = ri;

	window->type = type;
  
	window->last_msg_recv_xevent_type = 0;
	window->last_msg_recv_xevent_id = NULL;
  
	window->last_msg_sent_xevent_type = 0;
	window->last_msg_sent_xevent_id = NULL;
  
	window->connected_jid = gj_jid_ref (connected_jid);
	window->c = gj_connection_ref (c);

	message_windows = g_list_append (message_windows, window);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "message",
				     NULL,
				     "message", &window->window,
				     "entry_contact", &window->entry_contact,
				     "entry_time", &window->entry_time,
				     "hbox_time", &window->hbox_time,
				     "entry_subject", &window->entry_subject,
				     "textview", &window->textview,
				     "label_contact", &window->label_contact,
				     "label_subject", &window->label_subject,
				     "label_next", &window->label_next,
				     "button_next", &window->button_next,
				     "button_send", &window->button_send,
				     "button_reply", &window->button_reply,
				     "button_close", &window->button_close,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "message", "delete_event", on_delete_event,
			      "message", "destroy", on_destroy,
			      "message", "realize", on_realize,
			      "button_next", "clicked", on_button_next_clicked,
			      "button_send", "clicked", on_button_send_clicked,
			      "button_reply", "clicked", on_button_reply_clicked,
			      "button_close", "clicked", on_button_close_clicked,
			      "textview", "button_press_event", on_textview_button_press_event,
			      "textview", "motion_notify_event", on_textview_motion_notify_event,
			      NULL);

	g_object_unref (xml);
 
	/* set icon */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_MESSAGE_NORMAL);
	gtk_window_set_icon (GTK_WINDOW (window->window), pb);
	g_object_unref (G_OBJECT (pb));

	/* get buffer */
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview));

	/* set function callbacks */
	g_signal_connect (G_OBJECT (buffer), "changed", 
			  G_CALLBACK (on_textbuffer_changed), 
			  window);

	/* set styles */
	gtk_text_buffer_create_tag (buffer, "url", 
				    "foreground", "blue", 
				    NULL); 

	/* set up types */
	if (type == GjMessageWindowTypeOutgoing) {
		gj_gtk_set_window_title_with_args (GTK_WIDGET (window->window), _("Send Message"));
		gj_gtk_set_label_with_args (GTK_WIDGET (window->label_contact), "%s:", _("To"));
      
		/* set properies */
		gtk_widget_show (GTK_WIDGET (window->button_send));
		gtk_widget_hide (GTK_WIDGET (window->button_reply));
		gtk_widget_hide (GTK_WIDGET (window->button_next));
		gtk_widget_hide (GTK_WIDGET (window->hbox_time));
		gtk_editable_set_editable (GTK_EDITABLE (window->entry_subject),TRUE);
		gtk_text_view_set_editable (GTK_TEXT_VIEW (window->textview), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (window->button_send), FALSE);

		gtk_widget_show (window->window);
      
		/* set specifics */
		gj_gtk_message_send (window);
	} else {
		gj_gtk_set_window_title_with_args (GTK_WIDGET (window->window), _("Message Received"));
		gj_gtk_set_label_with_args (GTK_WIDGET (window->label_contact), "%s:", _("From"));
      
		/* set properies */
		gtk_widget_show (GTK_WIDGET (window->button_reply));
		gtk_widget_hide (GTK_WIDGET (window->button_send));
		/*  gtk_widget_set_sensitive (window->button_next,FALSE);*/
		gtk_editable_set_editable (GTK_EDITABLE (window->entry_subject),FALSE);
		gtk_text_view_set_editable (GTK_TEXT_VIEW (window->textview), FALSE);
	}

	return window;
}

GjMessageWindow *gj_gtk_message_find (gint ri_id, GjMessageWindowType type)
{
	gint index = 0;

	if (ri_id < 0) {
		g_warning ("%s: id:%d was < 0", __FUNCTION__, ri_id);
		return NULL;
	}

	for (index=0;index<g_list_length (message_windows);index++) {
		GjMessageWindow *window = NULL;
		window = g_list_nth_data (message_windows, index);

		if (window && 
		    window->type == type && 
		    gj_roster_item_get_id (window->ri) == ri_id) {
			return window; 
		}
	}

	return NULL;  
}

GjMessageWindow *gj_gtk_message_send_new_by_jid (GjConnection c,
						 GjJID connected_jid, 
						 const gchar *jid)
{
	GjRosterItem ri = NULL;  
	GjJID j = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	j = gj_jid_new (jid);
	if (j == NULL) {
		g_warning ("%s: jid was invalid", __FUNCTION__);
		return FALSE;
	}
  
	ri = gj_rosters_find_item_by_jid (j);
	gj_jid_unref (j);

	if ((ri = gj_roster_item_new_unknown_by_jid (jid)) == NULL) {
		g_warning ("%s: could not create new temporary roster item", __FUNCTION__);
		return NULL;
	}

	return gj_gtk_message_send_new (c, connected_jid, ri);
}

GjMessageWindow *gj_gtk_message_send_new (GjConnection c,
					  GjJID connected_jid, 
					  GjRosterItem ri)
{
	return gj_gtk_message_load (c, connected_jid, ri, GjMessageWindowTypeOutgoing);
}

gboolean gj_gtk_message_set_transient_for_window (GjMessageWindow *transient_for, 
						  GtkWindow *window)
{
	if (transient_for == NULL) {
		g_warning ("%s: message window (transient window) was NULL", __FUNCTION__);
		return FALSE;
	}

	if (window == NULL) {
		g_warning ("%s: window (parent window) was NULL", __FUNCTION__);
		return FALSE;
	}

	/*   gdk_window_raise (GDK_WINDOW (transient_for->window->window)); */
	gtk_window_set_transient_for (GTK_WINDOW (transient_for->window), 
				      GTK_WINDOW (window)); 

	return TRUE;
}

static gboolean gj_gtk_message_send (GjMessageWindow *window)
{
	GjJID j = NULL;

	const gchar *contact = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (window->ri);
	contact = gj_jid_get_full (j);
 
	if (contact != NULL && g_utf8_strlen (contact, -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (window->entry_contact), contact);
	}

	return TRUE;
}

static gboolean gj_gtk_message_receive (GjMessageWindow *window, GjMessage m)
{
	gchar *time = NULL;

	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter;

	GjJID j = NULL;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	time = g_strdup (gj_get_timestamp_from_timet (gj_message_get_time (m)));
	/*  time = timestampByTimeT (messageGetTime (m));*/

	j = gj_message_get_from_jid (m); 

	if (gj_jid_get_full (j) != NULL) {
		gtk_entry_set_text (GTK_ENTRY (window->entry_contact), gj_jid_get_full (j));
	}

	if (time != NULL && g_utf8_strlen (time, -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (window->entry_time), time);
	}

	if (gj_message_get_subject (m) != NULL && g_utf8_strlen (gj_message_get_subject (m), -1) > 0) {
		gtk_entry_set_text (GTK_ENTRY (window->entry_subject), gj_message_get_subject (m));
		gtk_widget_show (window->label_subject);
		gtk_widget_show (window->entry_subject);
	} else {
		gtk_widget_hide (window->label_subject);
		gtk_widget_hide (window->entry_subject);
	}

	if ((textview = GTK_TEXT_VIEW (window->textview)) == NULL) {
		g_warning ("%s: textview was NULL", __FUNCTION__);
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (textview);
	gtk_text_buffer_set_text (buffer, "", -1);
	gtk_text_buffer_get_iter_at_offset (buffer, &iter, -1);
  
	if (gj_config_get_mpf_use_smileys () == TRUE) {
		gj_gtk_textbuffer_insert_with_emote_icons (buffer, &iter, NULL, gj_message_get_body (m));
	} else {
		gj_gtk_textbuffer_insert (buffer, &iter, NULL, gj_message_get_body (m));
	}
  
	/* clean up */
	g_free (time);

	return TRUE;
}

static void gj_gtk_message_waiting_update (GjMessageWindow *window)
{
	gint count = 0;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return;
	}

	/* get count waiting */
	if ((count = gj_gtk_message_waiting_count (window)) < 0) {
		g_warning ("%s: count:%d was < 0, could not get the count", __FUNCTION__, count);
		return;
	}
  
	/* change button to say "next (i)" */
	g_message ("%s: updating number waiting to %d", __FUNCTION__, count);
  
	if (count > 1) {
		gj_gtk_set_label_with_args (GTK_WIDGET (window->label_next), "%s (<b>%d</b>)", _("Next"), count);
		gtk_widget_set_sensitive (window->button_next,TRUE);
	} else if (count == 1) {
		gj_gtk_set_label_with_args (GTK_WIDGET (window->label_next), _("Next"));
		gtk_widget_set_sensitive (window->button_next,TRUE);
	} else {
		gtk_widget_set_sensitive (window->button_next,FALSE);
	}
}

static gint gj_gtk_message_waiting_count (GjMessageWindow *window)
{
	GjEvent ev = NULL;

	gint count = 0;
	gint index = 0;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return -1;
	}

	/* look for futher events of type 'message' in queue */
	while ((ev = gj_event_nth_by_roster_item (window->ri, index++)) != NULL) {
		GjMessage m = NULL;
		
		if (gj_event_get_type (ev) != GjEventTypeMessage) { 
			continue; 
		}

		if ((m = (GjMessage) gj_event_get_data (ev)) == NULL) {
			continue; 
		}

		if (gj_message_get_type (m) == GjMessageTypeNormal) {
			count++;
		}
	}
  
	/* should we?? take one off because the current event is also in this queue */
	return count;
}

gboolean gj_gtk_message_waiting_next (GjMessageWindow *window)
{
	GjMessage m = NULL;
	GjEvent ev = NULL;

	gint eventID = 0;
	gint count = 0;
	gint index = 0;

	if (window == NULL) {
		g_warning ("%s: window was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((count = gj_gtk_message_waiting_count (window)) < 1) {
		g_warning ("%s: count:%d was < 1", __FUNCTION__, count);
		return FALSE;
	}

	/* look for futher events of type 'message' in queue */
	while ((ev = gj_event_nth_by_roster_item (window->ri, index++)) != NULL) {
		GjMessage m = NULL;
		
		if (gj_event_get_type (ev) != GjEventTypeMessage) {
			continue;
		}

		if ((m = (GjMessage) gj_event_get_data (ev)) == NULL) {
			continue;
		}

		if (gj_message_get_type (m) == GjMessageTypeNormal) {
			count++;
			break;
		}
	}

	if (count == 0) {
		g_warning ("%s: could not find next roster item event", __FUNCTION__);
		return FALSE;
	}

	m = (GjMessage) gj_event_get_data (ev);
	eventID = gj_event_get_id (ev);
  
	if (m == NULL) {
		g_warning ("%s: message was NULL, not handling...", __FUNCTION__);
      
		/* free event - no use to me now */
		gj_event_free (ev);
		return FALSE;
	}
  
	g_message ("%s: handling next event of type:%d, id:%d", __FUNCTION__, GjEventTypeMessage, eventID);
    
	/* setup msg */
	gj_gtk_message_handle_message (window, m);	   

	/* free event */
	gj_event_free (ev);

	/* check messages still waiting and update display */
	gj_gtk_message_waiting_update (window);

	/* update roster */
	gj_gtk_roster_handle_next_event_display (window->ri); 

	return TRUE;
}

gboolean gj_gtk_message_post_by_event (GjConnection c, 
				       GjJID connected_jid, 
				       GjEvent ev, 
				       gboolean show_window)
{
	GjMessageWindow *window = NULL;
	GjRosterItem ri = NULL;

	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}

	window = gj_gtk_message_find (gj_event_get_ri_id (ev), GjMessageWindowTypeIncoming);
	ri = gj_rosters_find_item_by_id (gj_event_get_ri_id (ev));
  
	if (window == NULL) {
		g_message ("%s: could not find GjMessageWindow by roster item id:%d, creating new...", 
			   __FUNCTION__, gj_event_get_ri_id (ev));

		if (ri == NULL) {
			g_warning ("%s: could not find roster item from id:%d", __FUNCTION__, gj_event_get_ri_id (ev));
			return FALSE;
		}
     
		/* create */
		window = gj_gtk_message_load (c, connected_jid, ri, GjMessageWindowTypeIncoming);

		if (!GTK_WIDGET_VISIBLE (window->window) && gj_config_get_mpf_open_window_on_new_message () == TRUE) {
			/* SHESKARS request for poping like Gabber when 
			   we get messages from unknown contacts. */
			g_message ("%s: presenting message (option)", __FUNCTION__);
			gtk_widget_show (window->window);
			gdk_window_raise (window->window->window);
		} else if (show_window) {
			/* show or not based on the event stack and if the 
			   user has it flashing at the moment or not. */
			gtk_widget_show (window->window);
		}

		if (!GTK_WIDGET_VISIBLE (window->window)) {
			return FALSE;
		}
      
		/* this is the next message in the queue */
		gj_gtk_message_handle_message (window, (GjMessage)gj_event_get_data (ev));      

		/* free event */
		gj_event_free (ev);
	} else {
		if (!GTK_WIDGET_VISIBLE (window->window) && show_window) {
			/* show or not based on the event stack and if the 
			   user has it flashing at the moment or not. */
			gtk_widget_show (window->window);
		}

		if (show_window) {  
			/* this is the next message in the queue */
			gj_gtk_message_handle_message (window, (GjMessage)gj_event_get_data (ev));      

			/* free event */
			gj_event_free (ev);

			/* check messages still waiting and update display */
			gj_gtk_message_waiting_update (window);
		} else {
			/* already shown, just want to update waiting list */
			gj_gtk_message_waiting_update (window);
		}
	}

	return TRUE;
}

static gboolean gj_gtk_message_handle_message (GjMessageWindow *window, GjMessage m)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if (gj_message_get_type (m) == GjMessageTypeNormal) {
		gj_gtk_message_receive (window, m);
	}
  
	return TRUE;
}

void gj_gtk_message_default_action (GjRosterItem ri)
{
	GjMessageWindow *window = NULL;
	GjJID j = NULL;
	gint count = 0;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return;
	}

	j = gj_roster_item_get_jid (ri);

	if ((window = gj_gtk_message_find (gj_roster_item_get_id (ri), GjMessageWindowTypeIncoming)) != NULL) {
		g_message ("%s: found incoming msg window for jid:'%s'", __FUNCTION__, gj_jid_get_full (j));

		if ((count = gj_gtk_message_waiting_count (window)) > 0) {
			/* 1 or more msgs waiting... */

			/* show window mw */
			gtk_widget_show (window->window);
			gj_gtk_message_waiting_next (window);
		}

		return;
	} else if ((window = gj_gtk_message_find (gj_roster_item_get_id (ri), GjMessageWindowTypeOutgoing)) != NULL) {
		/* show window */
		g_message ("%s: found outgoing msg window for jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return;
	} else {
		GjMessageWindow *window = NULL;
		
		g_message ("%s: found NO incoming or outgoing msg window for jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		
		/* if no messages waiting */
		gtk_widget_show (window->window);
		window = gj_gtk_message_load (window->c,
					      window->connected_jid, 
					      ri, 
					      GjMessageWindowTypeOutgoing);
	}
}

/*
 * GUI events
 */
static void on_textbuffer_changed (GtkTextBuffer *textbuffer, GjMessageWindow *window)
{
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	gint length = 0;

	/* handle URL highlighting */
	gj_gtk_textbuffer_handle_changed (textbuffer, window);

	/* enable/disable buttons */
	gtk_text_buffer_get_bounds (textbuffer, &iter_start, &iter_end);
	length = gtk_text_buffer_get_char_count (textbuffer);

	if (length > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (window->button_send), TRUE);
	} else {
		gtk_widget_set_sensitive (GTK_WIDGET (window->button_send), FALSE); 
	}
}

static gboolean on_textview_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjMessageWindow *window)
{
	return gj_gtk_textview_handle_motion_notify_event (widget, event, window);
}

static gboolean on_textview_button_press_event (GtkWidget *widget, GdkEventButton *event, GjMessageWindow *window)
{
	return gj_gtk_textview_handle_button_press_event (widget, event, window);
}

static void on_button_send_clicked (GtkButton *button, GjMessageWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter bufStart;
	GtkTextIter bufStop;

	gchar *subject = NULL;
	gchar *body = NULL;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview));
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStart, 0);
	gtk_text_buffer_get_iter_at_offset (buffer, &bufStop, -1);
	body = gtk_text_buffer_get_text (buffer,&bufStart,&bufStop,TRUE);

	if (body == NULL || g_utf8_strlen (body, -1) < 1) {
		g_warning ("%s: cant send NULL or 0 length message", __FUNCTION__);
		return;
	}

	subject = gtk_editable_get_chars (GTK_EDITABLE (window->entry_subject),0,-1); 

	/* send */
	gj_message_send_by_ri (window->c,
			       GjMessageTypeNormal, 
			       gj_jid_get_full (window->connected_jid),
			       window->ri, 
			       subject, 
			       body, 
			       NULL);

	/* unload */
	gtk_widget_destroy (window->window);
}

static void on_button_reply_clicked (GtkButton *button, GjMessageWindow *window)
{
	/* create new */
	gj_gtk_message_load (window->c,
			     window->connected_jid, 
			     window->ri, 
			     GjMessageWindowTypeOutgoing);

	/* unload old ? */
	gtk_widget_destroy (window->window); 
}

static void on_button_next_clicked (GtkButton *button, GjMessageWindow *window)
{
	gj_gtk_message_waiting_next (window);
}

static void on_button_close_clicked (GtkButton *button, GjMessageWindow *window)
{
	on_delete_event (window->window, NULL, window);
}

static void on_realize (GtkWidget *widget, GjMessageWindow *window)
{
	gtk_widget_set_events (GTK_WIDGET (window->textview),
			       GDK_EXPOSURE_MASK | 
			       GDK_POINTER_MOTION_MASK | 
			       GDK_POINTER_MOTION_HINT_MASK | 
			       GDK_BUTTON_RELEASE_MASK | 
			       GDK_STRUCTURE_MASK | 
			       GDK_PROPERTY_CHANGE_MASK);

	gdk_window_set_decorations (GDK_WINDOW (window->window->window), 
				    GDK_DECOR_ALL);
}

static void on_destroy (GtkWidget *widget, GjMessageWindow *window)
{
	message_windows = g_list_remove (message_windows, window);

	window->last_msg_recv_xevent_type = 0;
    
	g_free (window->last_msg_recv_xevent_id);
    
	window->last_msg_sent_xevent_type = 0;
    
	g_free (window->last_msg_sent_xevent_id);

	gj_jid_unref (window->connected_jid);
	gj_connection_unref (window->c);

	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjMessageWindow *window)
{
	gtk_widget_destroy (window->window);
	return FALSE;
}

#ifdef __cplusplus
}
#endif
