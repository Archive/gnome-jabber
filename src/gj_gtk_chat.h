/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_gtk_chat_h
#define __gj_gtk_chat_h

#include "gj_typedefs.h"


typedef struct t_GjChatWindow GjChatWindow;

gboolean gj_gtk_chat_is_window_open (GjChatWindow *window);

gboolean gj_gtk_chat_post_by_event (GjConnection c,
				    GjJID connected_jid, 
				    GjEvent ev, 
				    gboolean show_window);

void gj_gtk_chat_default_action (GjRosterItem ri); 

GjChatWindow *gj_gtk_chat_receive_new (GjRosterItem ri); 
gboolean gj_gtk_chat_send_new (GjConnection c,
			       GjJID connected_jid, 
			       GjRosterItem ri);

GjChatWindow *gj_gtk_chat_send_new_by_jid (GjConnection c,
					   GjJID connected_jid, 
					   GjRoster r, 
					   const gchar *jid);


gboolean gj_gtk_chat_presence_update (GjRosterItem ri, GjPresence pres);

#endif
