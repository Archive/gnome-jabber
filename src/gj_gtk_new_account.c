/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_stream.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"
#include "gj_config.h"
#include "gj_connection.h"

#include "gj_gtk_new_account.h"
#include "gj_gtk_support.h"
#include "gj_gtk_connection_settings.h"


typedef struct  {
	/* search */
	GtkWidget *window;

	GtkWidget *button_ok;
	GtkWidget *button_cancel;

	GtkWidget *entry_server;
	GtkWidget *button_server;
	GtkWidget *spinbutton_port;
	GtkWidget *entry_username;  
	GtkWidget *entry_password1;
	GtkWidget *entry_password2;

	/*
	 * members
	 */

	GjStream stream;

	GtkWidget *progress_dialog;
	gboolean progress_cancelled;

} GjNewAccountDialog;


static gboolean gj_gtk_na_check (GjNewAccountDialog *dialog);
static gboolean gj_gtk_na_save_settings (GjNewAccountDialog *dialog);

static gboolean gj_gtk_na_connection (GjNewAccountDialog *dialog);

static void gj_gtk_na_connected_cb (GjStream stm, gint error_code, const gchar *error_message, gpointer user_data);
static void gj_gtk_na_registered_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);
static void gj_gtk_na_progress_cb (GtkDialog *widget, gint response, GjNewAccountDialog *dialog);


static void gj_gtk_na_server_cb (GtkMenuItem *menuitem, GjNewAccountDialog *dialog);
static void gj_gtk_na_server_align_cb (GtkMenu *menu, 
				       gint *x, 
				       gint *y, 
				       gboolean *push_in, 
				       GjNewAccountDialog *dialog);

/* 
 * gui callbacks 
 */
static void on_button_server_clicked (GtkButton *button, GjNewAccountDialog *dialog);

static void on_entry_password_changed (GtkEditable *editable, GjNewAccountDialog *dialog);

static void on_destroy (GtkWidget *widget, GjNewAccountDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjNewAccountDialog *dialog);


static GjNewAccountDialog *current_dialog = NULL;


gboolean gj_gtk_na_load ()
{
	GjNewAccountDialog *dialog = NULL;
	GladeXML *xml = NULL;

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjNewAccountDialog, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "new_account",
				     NULL,
				     "new_account", &dialog->window,
				     "entry_server", &dialog->entry_server,
				     "button_server", &dialog->button_server,
				     "spinbutton_port", &dialog->spinbutton_port,
				     "entry_username", &dialog->entry_username,
				     "entry_password1", &dialog->entry_password1,
				     "entry_password2", &dialog->entry_password2,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "new_account", "response", on_response,
			      "new_account", "destroy", on_destroy,
			      "button_server", "clicked", on_button_server_clicked,
			      "entry_password1", "changed", on_entry_password_changed,
			      "entry_password2", "changed", on_entry_password_changed,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set default */
	gtk_entry_set_text (GTK_ENTRY (dialog->entry_server), "jabber.org");

	return TRUE;
}

static gboolean gj_gtk_na_check (GjNewAccountDialog *dialog)
{
	G_CONST_RETURN gchar *username = NULL;
	G_CONST_RETURN gchar *server = NULL;

	G_CONST_RETURN gchar *password1 = NULL;
	G_CONST_RETURN gchar *password2 = NULL;

	if (!dialog) {
		return FALSE; 
	}

	server = gtk_entry_get_text (GTK_ENTRY (dialog->entry_server));
	username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));

	password1 = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password1));
	password2 = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password2));

	if (!server || g_utf8_strlen (server, -1) < 1) {
		gj_gtk_show_error (_("Details Incomplete."),
				   1,
				   _("Server was not specified."));

		return FALSE;
	}

	if (!username || g_utf8_strlen (username, -1) < 1) {
		gj_gtk_show_error (_("Details Incomplete."),
				   1,
				   _("Username was not specified."));

		return FALSE;
	}

	if (!password1 || !password2 || 
	    g_utf8_strlen (password1, -1) < 1 || 
	    g_utf8_strlen (password2, -1) < 1) {
		gj_gtk_show_error (_("Details Incomplete."),
				   1,
				   _("Password was not specified."));
		
		return FALSE;
	}
	
	if (strcmp (password1, password2) != 0) {
		gj_gtk_show_error (_("Details Incomplete."),
				   1,
				   _("Passwords do NOT match."));

		return FALSE;
	}

	return TRUE;
}

static gboolean gj_gtk_na_save_settings (GjNewAccountDialog *dialog)
{
	G_CONST_RETURN gchar *server = NULL;
	gint16 port = 0;
	G_CONST_RETURN gchar *username = NULL;
	G_CONST_RETURN gchar *password = NULL;

	/* if checkbutton is selected, change the connection settings */
	server = gtk_entry_get_text (GTK_ENTRY (dialog->entry_server));
	port = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_port));
	username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));
	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password1));

	/* set */
	gj_config_set_cs_server (server);
	gj_config_set_cs_port (port);
	gj_config_set_cs_username (username);
	gj_config_set_cs_password (password);

	/* save */
	gj_config_write ();

	return TRUE;
}

static gboolean gj_gtk_na_connection (GjNewAccountDialog *dialog)
{
	G_CONST_RETURN gchar *server = NULL;
	G_CONST_RETURN gchar *username = NULL;
	gint16 port = 0;

	if (!dialog) {
		return FALSE;
	}

	server = gtk_entry_get_text (GTK_ENTRY (dialog->entry_server));
	username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));
	port = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_port));

	dialog->stream = gj_stream_new (server);

	/* set struct info */
	gj_stream_set_port (dialog->stream, port);
	gj_stream_set_connected_cb (dialog->stream, gj_gtk_na_connected_cb, NULL);

	gj_stream_build (dialog->stream);

	/* set controls state */
	dialog->progress_cancelled = FALSE;
	dialog->progress_dialog = gj_gtk_hig_progress_new (GTK_WINDOW (dialog->window),
							   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							   (GCallback)gj_gtk_na_progress_cb,
							   dialog,
							   _("Progress"),
							   _("Registering New Account"), 
							   _("Username is \"%s\""), 
							   username); 


	gtk_widget_show_all (dialog->progress_dialog);

	gtk_widget_hide (dialog->window);

	/* start */
	gj_stream_open (dialog->stream);

	return TRUE;
}

static void gj_gtk_na_connected_cb (GjStream stm, gint error_code, const gchar *error_message, gpointer user_data)
{
	GjNewAccountDialog *dialog = NULL;
	G_CONST_RETURN gchar *server = NULL;
	G_CONST_RETURN gchar *username = NULL;
	G_CONST_RETURN gchar *password = NULL;
	guint port = 5222;

	dialog = current_dialog;

	if (!dialog) {
		return;
	}

	/* get information */
	server = gtk_entry_get_text (GTK_ENTRY (dialog->entry_server));
	port = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (dialog->spinbutton_port));
	username = gtk_entry_get_text (GTK_ENTRY (dialog->entry_username));
	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password1));

	/* do request */
	gj_iq_request_register_user (gj_stream_get_connection (stm),
				     server,
				     username,
				     password,
				     gj_gtk_na_registered_cb,
				     NULL);
}

static void gj_gtk_na_registered_cb (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	GjNewAccountDialog *dialog = NULL;
	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	dialog = current_dialog;

	if (!dialog) {
		return;
	}

	if (dialog->progress_dialog) {
		gtk_widget_destroy (dialog->progress_dialog);
		dialog->progress_dialog = NULL;
	}

	if (!iq) {
		return; 
	}

	/* check for errors? */
	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *error_code = NULL;
		xmlChar *error_message = NULL;

		g_warning ("%s: registration failed", __FUNCTION__);

		if ((node = gj_parser_find_by_node (node, (guchar*)"error"))) {
			error_code = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
			error_message = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

			gj_gtk_show_error (_("Failed to Register New Account."),
					   atoi ((gchar*)error_code),
					   (gchar*)error_message);
		} else {
			gj_gtk_show_error (_("Failed to Register New Account."),
					   1,
					   _("Unknown Error"));
		}
      
		/* find stream and close */
		if (dialog->stream != NULL) {
			gj_stream_free (dialog->stream);
			dialog->stream = NULL;
		}

		/* re-show window */
		gtk_widget_show (dialog->window);

		/* if err 409 - user n/a, select user entry */
		if (error_code && atoi ((gchar*)error_code) == 409) { 
			gtk_widget_grab_focus (dialog->entry_username);
		}
      
		return;
	}

	/* ok */
	gj_gtk_show_success (_("Registration Success."));

	if (dialog->stream != NULL) {
		/* find stream and close */
		gj_stream_free (dialog->stream);
		dialog->stream = NULL;
	}

	/* set connection settings */
	gj_gtk_na_save_settings (dialog);

	/* close window */
	gtk_widget_destroy (dialog->window);
}

static void gj_gtk_na_progress_cb (GtkDialog *widget, gint response, GjNewAccountDialog *dialog)
{
	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_DELETE_EVENT) {
		dialog->progress_cancelled = TRUE;
	}

	if (dialog->progress_cancelled) {
		gtk_widget_show (dialog->window); 
	}

	gtk_widget_destroy (dialog->progress_dialog);
	dialog->progress_dialog = NULL;
}

static void gj_gtk_na_server_cb (GtkMenuItem *menuitem, GjNewAccountDialog *dialog)
{
	const gchar **server_listing = NULL;
	const gchar *server = NULL;

	gpointer pid;
	gint id;

	server_listing = gnome_jabber_get_server_list ();

	if (!server_listing) {
		return;
	}

	pid = g_object_get_data (G_OBJECT (menuitem), "id");
	id = GPOINTER_TO_INT (pid);
  
	server = server_listing[id];

	if (server) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_server), server); 
	}
}

static void gj_gtk_na_server_align_cb (GtkMenu *menu, 
				       gint *x, 
				       gint *y, 
				       gboolean *push_in, 
				       GjNewAccountDialog *dialog)
{
	GtkWidget *button = NULL;
	GtkRequisition req;
	GdkScreen *screen = NULL;
	gint width = 0;
	gint height = 0;
	gint screen_height = 0;
  
	button = dialog->button_server;
	
	gtk_widget_size_request (GTK_WIDGET (menu), &req);
  
	gdk_window_get_origin (GTK_BUTTON (button)->event_window, x, y);
	gdk_drawable_get_size (GTK_BUTTON (button)->event_window, &width, &height);
  
	*x -= req.width - width;
	*y += height;
  
	screen = gtk_widget_get_screen (GTK_WIDGET (menu));
  
	/* Clamp to screen size. */
	screen_height = gdk_screen_get_height (screen) - *y;	

	if (req.height > screen_height) {
		/* It doesn't fit, so we see if we have the minimum space needed. */
		if (req.height > screen_height && *y - height > screen_height) {
			/* Put the menu above the button instead. */
			screen_height = *y - height;
			*y -= (req.height + height);

			if (*y < 0) {
				*y = 0;
			}
		}
	}
}

/*
 * GUI events
 */
static void on_button_server_clicked (GtkButton *button, GjNewAccountDialog *dialog)
{
	GtkWidget *menu = NULL;

	GList *servers = NULL;
	const gchar **server_listing = NULL;
	const gchar *str = NULL;
	gint i = 0;

	server_listing = gnome_jabber_get_server_list ();

	menu = gtk_menu_new ();
  
	/* set a limit of a 1000 */
	while ((str = server_listing[i++]) != NULL && i < 1000) {
		GtkWidget *item = NULL;
		
		if (!str) {
			continue;
		}
		
		item = gtk_menu_item_new_with_label (str);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
		
		g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (i - 1));
		
		g_signal_connect ((gpointer) item, "activate", 
				  G_CALLBACK (gj_gtk_na_server_cb), dialog);
	}
	
	gtk_widget_show_all (menu);
	
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, 
			(GtkMenuPositionFunc)gj_gtk_na_server_align_cb,
			dialog, 1, gtk_get_current_event_time ());
  

	/* cleanup */
	g_list_free (servers);
}

static void on_entry_password_changed (GtkEditable *editable, GjNewAccountDialog *dialog)
{
	gboolean match = FALSE;

	G_CONST_RETURN gchar *password1 = NULL;
	G_CONST_RETURN gchar *password2 = NULL;

	if (!dialog) {
		return;
	}

	password1 = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password1));
	password2 = gtk_entry_get_text (GTK_ENTRY (dialog->entry_password2));

	if (password1 != NULL && g_utf8_strlen (password1, -1) > 0 && 
	    password2 != NULL && g_utf8_strlen (password2, -1) > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), (strcmp (password1, password2) == 0));
	}
}

static void on_destroy (GtkWidget *widget, GjNewAccountDialog *dialog)
{
	current_dialog = NULL;

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjNewAccountDialog *dialog)
{
	if (response == GTK_RESPONSE_OK) { 
		if (!gj_gtk_na_check (dialog)) {
			return;
		}
      
		gj_gtk_na_connection (dialog);
		return;
	}

	gtk_widget_destroy (dialog->window);
}


#ifdef __cplusplus
}
#endif
