/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$ 
 *
 */

#ifndef __gj_agents_h
#define __gj_agents_h

#include "gj_typedefs.h"

/* item functions */
GjAgent gj_agent_new (const gchar *jid);
void gj_agent_free (GjAgent ag);

/* sets */
gboolean gj_agent_set_jid (GjAgent ag, gchar *jid);
gboolean gj_agent_set_name (GjAgent ag, gchar *name);
gboolean gj_agent_set_description (GjAgent ag, gchar *description);
gboolean gj_agent_set_transport (GjAgent ag, gchar *transport);
gboolean gj_agent_set_groupchat (GjAgent ag, gchar *groupchat);
gboolean gj_agent_set_service (GjAgent ag, gchar *service);
gboolean gj_agent_set_registers (GjAgent ag, gchar *registers);
gboolean gj_agent_set_search (GjAgent ag, gchar *search);

/* gets */
gchar *gj_agent_get_jid (GjAgent ag);
gchar *gj_agent_get_name (GjAgent ag);
gchar *gj_agent_get_description (GjAgent ag);
gchar *gj_agent_get_transport (GjAgent ag);
gchar *gj_agent_get_groupchat (GjAgent ag);
gchar *gj_agent_get_service (GjAgent ag);
gchar *gj_agent_get_registers (GjAgent ag);
gchar *gj_agent_get_search (GjAgent ag);

/* request */
gboolean gj_agent_list_request (GjConnection c,
				gchar *server, 
				GjAgentCallback cb, 
				void *userdata);

#endif

