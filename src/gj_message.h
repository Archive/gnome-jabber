/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_message_h
#define __gj_message_h

#include "gj_typedefs.h"

void gj_message_stats ();

GjMessage gj_message_ref (GjMessage m);
gboolean gj_message_unref (GjMessage m);

/*
 * member functions
 */

/* sets */

/* gets */
GjConnection gj_message_get_connection (GjMessage m);

GjMessageType gj_message_get_type (GjMessage m);

const gchar *gj_message_get_id (GjMessage m);

gint gj_message_get_unique_id (GjMessage m);

const GjJID gj_message_get_to_jid (GjMessage m);
const GjJID gj_message_get_from_jid (GjMessage m);

const gchar *gj_message_get_subject (GjMessage m);
const gchar *gj_message_get_body (GjMessage m);

const gchar *gj_message_get_thread (GjMessage m);

gint gj_message_get_x_event_type (GjMessage m);
const gchar *gj_message_get_x_event_message_id (GjMessage m);

const gchar *gj_message_get_x_delay_stamp (GjMessage m);

const gchar *gj_message_get_x_oob_url (GjMessage m);
const gchar *gj_message_get_x_oob_description (GjMessage m);

const gchar *gj_message_get_error_code (GjMessage m);
const gchar *gj_message_get_error_reason (GjMessage m);

time_t gj_message_get_time (GjMessage m);

/*
 * message functions to tcp / parser layer 
 */
gboolean gj_message_send (GjConnection c, 
			  GjMessageType type, 
			  const gchar *from,
			  const gchar *to, 
			  const gchar *subject, 
			  const gchar *body, 
			  const gchar *thread);

gboolean gj_message_send_by_ri (GjConnection c, 
				GjMessageType type,
				const gchar *from,
				GjRosterItem ri, 
				const gchar *subject, 
				const gchar *body, 
				const gchar *thread);

gchar *gj_message_send_with_x_event_request (GjConnection c, 
					     GjMessageType type, 
					     const gchar *from,
					     const gchar *to, 
					     const gchar *subject, 
					     const gchar *body, 
					     const gchar *thread, 
					     gint xevents);

gchar *gj_message_send_with_x_event_request_by_ri (GjConnection c,
						   GjMessageType type, 
						   const gchar *from,
						   GjRosterItem ri, 
						   const gchar *subject, 
						   const gchar *body, 
						   const gchar *thread, 
						   gint xevents);

gboolean gj_message_send_x_event (GjConnection c,
				  gint type, 
				  const gchar *from,
				  const gchar *to, 
				  const gchar *id);

gboolean gj_message_send_x_event_by_ri (GjConnection c,
					gint type, 
					const gchar *from,
					GjRosterItem ri, 
					const gchar *id);

void gj_message_receive (GjConnection c, 
			 xmlNodePtr node);

/*
 * callbacks
 */
gboolean gj_message_set_cb_to (GjMessageToCallback cb);
gboolean gj_message_set_cb_from (GjMessageFromCallback cb);


#endif
