/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include "gj_search.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_connection.h"


struct t_search {
	gchar *id;
	gchar *to;
	gchar *from;

	gchar *instructions;

	gchar *jid;

	gchar *first;
	gchar *last;
	gchar *nick;
	gchar *email;

	gboolean allows_first;
	gboolean allows_last;
	gboolean allows_nick;
	gboolean allows_email;

	searchCallback cb; 
};


/* search item */
void searchDestroy (search s);

/* search list */
gboolean searchListAdd (search s);
gboolean searchListDel (search s);

/* parsing functions */
void searchResponse (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

/* parsing functions */
search searchResponseParse (xmlNodePtr parent);


GList *searchList = NULL;
searchRequestCallback searchCB = NULL;

gboolean searchInit ()
{
	if (searchList != NULL)
		searchTerm ();

	/* create mem  - nothing to do, created on g_list_append */
	return TRUE;
}

gboolean searchTerm ()
{
	gint index = 0;

	/* list */
	if (searchList == NULL) {
		return TRUE;
	}

	/* free all items on the list */
	for (index=0;index<g_list_length (searchList);index++) {
		search s = NULL;
      
		if ((s = (search) g_list_nth_data (searchList,index)) != NULL) {
			searchFree (s);
		}
	}

	g_list_free (searchList);
	searchList = NULL;

	return TRUE;
}

gboolean searchListAdd (search s)
{
	if (s == NULL) {
		g_warning ("searchListAdd: s was NULL");
		return FALSE;
	}

	searchList = g_list_append (searchList, s);

	return TRUE;
}

gboolean searchListDel (search s)
{
	if (s == NULL) {
		g_warning ("searchListDel: s was NULL");
		return FALSE;
	}

	searchList = g_list_remove (searchList, s);
	return TRUE;
}

GList *searchListGet ()
{
	return searchList;
}

/* 
 * item functions
 */
search searchNew ()
{
	search s;

	if ((s = g_new (struct t_search,1))) {
		s->id = NULL;
		s->to = NULL;
		s->from = NULL;

		s->instructions = NULL;

		s->first = NULL;
		s->last = NULL;
		s->nick = NULL;
		s->email = NULL;

		s->allows_first = FALSE;
		s->allows_last = FALSE;
		s->allows_nick = FALSE;
		s->allows_email = FALSE;

		s->cb = NULL;

		searchListAdd (s);
	}
  
	return s;  
}

void searchFree (search s)
{
	if (s == NULL) {
		g_warning ("searchFree: s was NULL");
		return;
	}

	/* free mem */
	searchListDel (s);  

	/* destroy members */
	searchDestroy (s);

	/* free structure */
	g_free (s);
}

void searchDestroy (search s)
{
	if (s == NULL) {
		g_warning ("searchDestroy: s was NULL");
		return;
	}
 
	g_free (s->id);
	g_free (s->to);
	g_free (s->from);
	g_free (s->instructions);
	g_free (s->first);
	g_free (s->last);
	g_free (s->nick);
	g_free (s->email);

	s->allows_first = FALSE;
	s->allows_last = FALSE;
	s->allows_nick = FALSE;
	s->allows_email = FALSE;
  
	s->cb = NULL;
}

/*
 * find 
 */

search searchFindByID (gchar *id)
{
	gint index = 0;

	if (id == NULL) {
		g_warning ("searchFindByID: name was NULL");
		return NULL;
	}

	for (index=0;index<g_list_length (searchList);index++) {
		search s  = g_list_nth_data (searchList,
					     index);

		if (s == NULL) {
			g_warning ("searchFindByID: getting nth item (%d) in list was NULL",index);
			continue;
		}

		if (g_strcasecmp (s->id, id) == 0) {
			return s;
		}
	}

	return NULL;
}

/*
 * member functions  
 */

/* sets */
gboolean searchSetID (search s, gchar *id)
{
	if (s == NULL) {
		g_warning ("searchSetID: s was NULL");
		return FALSE;
	}

	if (id == NULL) {
		g_warning ("searchSetID: id was NULL");
		return FALSE;
	}

	g_free (s->id);
	s->id = g_strdup (id);

	return TRUE;
}

gboolean searchSetTo (search s, gchar *to)
{
	if (s == NULL) {
		g_warning ("searchSetTo: s was NULL");
		return FALSE;
	}

	if (to == NULL) {
		g_warning ("searchSetTo: to was NULL");
		return FALSE;
	}

	g_free (s->to);
 	s->to = g_strdup (to);

	return TRUE;
}

gboolean searchSetFrom (search s, gchar *from)
{
	if (s == NULL) {
		g_warning ("searchSetFrom: s was NULL");
		return FALSE;
	}

	if (from == NULL) {
		g_warning ("searchSetFrom: from was NULL");
		return FALSE;
	}

	g_free (s->from);
	s->from = g_strdup (from);

	return TRUE;
}

gboolean searchSetInstructions (search s, gchar *instructions) {
	if (s == NULL) {
		g_warning ("searchSetInstructions: s was NULL");
		return FALSE;
	}

	if (instructions == NULL) {
		g_warning ("searchSetInstructions: id was NULL");
		return FALSE;
	}

	g_free (s->instructions);
  	s->instructions = g_strdup (instructions);

	return TRUE;
}

gboolean searchSetJID (search s, gchar *jid)
{
	if (s == NULL) {
		g_warning ("searchSetJID: s was NULL");
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("searchSetJID: jid was NULL");
		return FALSE;
	}

	g_free (s->jid);
	s->jid = g_strdup (jid);

	return TRUE;
}

gboolean searchSetFirst (search s, gchar *first)
{
	if (s == NULL) {
		g_warning ("searchSetFirst: s was NULL");
		return FALSE;
	}

	if (first == NULL) {
		g_warning ("searchSetFirst: id was NULL");
		return FALSE;
	}

	g_free (s->first);
  	s->first = g_strdup (first);

	return TRUE;
}

gboolean searchSetLast (search s, gchar *last)
{
	if (s == NULL) {
		g_warning ("searchSetLast: s was NULL");
		return FALSE;
	}

	if (last == NULL) {
		g_warning ("searchSetLast: id was NULL");
		return FALSE;
	}

	g_free (s->last);
	s->last = g_strdup (last);

	return TRUE;
}

gboolean searchSetNick (search s, gchar *nick)
{
	if (s == NULL) {
		g_warning ("searchSetNick: s was NULL");
		return FALSE;
	}

	if (nick == NULL) {
		g_warning ("searchSetNick: id was NULL");
		return FALSE;
	}

	g_free (s->nick);
  	s->nick = g_strdup (nick);

	return TRUE;
}

gboolean searchSetEmail (search s, gchar *email)
{
	if (s == NULL) {
		g_warning ("searchSetEmail: s was NULL");
		return FALSE;
	}

	if (email == NULL) {
		g_warning ("searchSetEmail: id was NULL");
		return FALSE;
	}

	g_free (s->email);
	s->email = g_strdup (email);

	return TRUE;
}

gboolean searchSetAllowsFirst (search s, gboolean allowed)
{
	if (s == NULL) {
		g_warning ("searchSetAllowsFirst: s was NULL");
		return FALSE;
	}
  
	s->allows_first = allowed;
	return TRUE;
}

gboolean searchSetAllowsLast (search s, gboolean allowed)
{
	if (s == NULL) {
		g_warning ("searchSetAllowsLast: s was NULL");
		return FALSE;
	}
  
	s->allows_last = allowed;
	return TRUE;
}

gboolean searchSetAllowsEmail (search s, gboolean allowed)
{
	if (s == NULL) {
		g_warning ("searchSetAllowsEmail: s was NULL");
		return FALSE;
	}
  
	s->allows_email = allowed;
	return TRUE;
}

gboolean searchSetAllowsNick (search s, gboolean allowed)
{
	if (s == NULL) {
		g_warning ("searchSetAllowsNick: s was NULL");
		return FALSE;
	}
  
	s->allows_nick = allowed;
	return TRUE;
}

gboolean searchSetCallback (search s, searchCallback cb)
{
	if (s == NULL) {
		g_warning ("searchSetCallback: s was NULL");
		return FALSE;
	}
  
	if (cb == NULL) {
		g_warning ("searchSetFields: callback was NULL");
		return FALSE;
	}

	s->cb = cb;
	return TRUE;
}

/* gets */
gchar *searchGetID (search s)
{
	if (s == NULL) {
		g_warning ("searchGetID: s was NULL");
		return NULL;
	}

	return s->id;
}

gchar *searchGetTo (search s)
{
	if (s == NULL) {
		g_warning ("searchGetTo: s was NULL");
		return NULL;
	}

	return s->to;
}

gchar *searchGetFrom (search s)
{
	if (s == NULL) {
		g_warning ("searchGetFrom: s was NULL");
		return NULL;
	}

	return s->from;
}

gchar *searchGetInstructions (search s) {
	if (s == NULL) {
		g_warning ("searchGetInstructions: s was NULL");
		return NULL;
	}

	return s->instructions;
}

gchar *searchGetJID (search s)
{
	if (s == NULL) {
		g_warning ("searchGetJID: s was NULL");
		return NULL;
	}

	return s->jid;
}

gchar *searchGetFirst (search s)
{
	if (s == NULL) {
		g_warning ("searchGetFirst: s was NULL");
		return NULL;
	}

	return s->first;
}

gchar *searchGetLast (search s)
{
	if (s == NULL) {
		g_warning ("searchGetLast: s was NULL");
		return NULL;
	}

	return s->last;
}

gchar *searchGetNick (search s)
{
	if (s == NULL) {
		g_warning ("searchGetNick: s was NULL");
		return NULL;
	}

	return s->nick;
}

gchar *searchGetEmail (search s)
{
	if (s == NULL) {
		g_warning ("searchGetEmail: s was NULL");
		return NULL;
	}

	return s->email;
}


gboolean searchGetAllowsFirst (search s)
{
	if (s == NULL) {
		g_warning ("searchGetAllowsFirst: s was NULL");
		return FALSE;
	}

	return s->allows_first;
}

gboolean searchGetAllowsLast (search s)
{
	if (s == NULL) {
		g_warning ("searchGetAllowsLast: s was NULL");
		return FALSE;
	}

	return s->allows_last;
}

gboolean searchGetAllowsNick (search s)
{
	if (s == NULL) {
		g_warning ("searchGetAllowsNick: s was NULL");
		return FALSE;
	}

	return s->allows_nick;
}

gboolean searchGetAllowsEmail (search s)
{
	if (s == NULL) {
		g_warning ("searchGetAllowsEmail: s was NULL");
		return FALSE;
	}

	return s->allows_email;
}

searchCallback searchGetCallback (search s)
{
	if (s == NULL) {
		g_warning ("searchGetCallback: s was NULL");
		return NULL;
	}

	return s->cb;
}

/*
 *
 * SERVICES
 *
 */
gboolean searchRequestRequirements (GjConnection c, gchar *server, searchRequestCallback cb)
{
	if (c == NULL) {
		g_warning ("searchRequestRequirements: connection was NULL");
		return FALSE;
	}

	if (server == NULL) {
		g_warning ("searchRequestRequirements: server was NULL");
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("searchRequestRequirements: cb was NULL");
		return FALSE;
	}

	searchCB = cb;

	/* do request */
	gj_iq_request_search_requirements (c, server, searchResponse, NULL);

	return TRUE;
}

gboolean searchRequest (GjConnection c, 
			gchar *server, 
			searchRequestCallback cb, 
			gchar *first, 
			gchar *last, 
			gchar *nick, 
			gchar *email)
{
	if (c == NULL) {
		g_warning ("searchRequestRequirements: connection was NULL");
		return FALSE;
	}

	if (server == NULL) {
		g_warning ("searchRequestRequirements: server was NULL");
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("searchRequestRequirements: cb was NULL");
		return FALSE;
	}

	searchCB = cb;

	/* do request */
	gj_iq_request_search (c,
			      server, searchResponse, 
			      "first", first?first:"",
			      "last", last?last:"",
			      "nick", nick?nick:"",
			      "email", email?email:"",
			      NULL);

	return TRUE;
}

void searchResponse (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	search s = NULL;

	gint error_code = 0;
	gchar *error_reason = NULL;

	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	if (!iq) 
		return;

	if (searchCB == NULL) {
		g_warning ("searchResponse: callback was NULL");
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("searchResponse: failed to find node 'error'");
			return;
		}

		errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		error_code = atoi ((gchar*)errorCode);
		error_reason = g_strdup ((gchar*)errorMessage);
	}

	/* parse list */
	if ((s = searchResponseParse (node)) == NULL) {
		g_warning ("searchResponse: did not complete the parse from xml -> s");
		return;
	}

	/* return list */
	(searchCB) (s, error_code, error_reason, NULL);

	/* clean up */
	if (error_reason != NULL) g_free (error_reason);
}

search searchResponseParse (xmlNodePtr parent)
{
	xmlChar *tmp = NULL;
	search s = NULL;

	if (parent == NULL) {
		g_warning ("searchResponseParse: failed to get search because query (xml) was NULL");
		return NULL;
	}

	/* create new roster item */
	if ((s = searchNew ()) == NULL) {
		g_warning ("searchResponseParse: could not create s");
		return NULL;
	}

	/* get important information */
	if ((tmp = gj_parser_find_by_attr_and_ret (parent, (guchar*)"id")) != NULL) {
		searchSetID (s, (gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (parent, (guchar*)"to")) != NULL) {
		searchSetTo (s, (gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (parent, (guchar*)"from")) != NULL) {
		searchSetFrom (s, (gchar*)tmp);
	}
  
	/* get instructions */ 
	if ((tmp = gj_parser_find_by_node_and_ret (parent, (guchar*)"instructions")) != NULL) {
		searchSetInstructions (s, (gchar*)tmp);
	}

	/* get first, last, etc... */
	if (gj_parser_find_by_node (parent, (guchar*)"first") != NULL) {
		searchSetAllowsFirst (s, TRUE);
	}

	if (gj_parser_find_by_node (parent, (guchar*)"last") != NULL) {
		searchSetAllowsLast (s, TRUE);
	}

	if (gj_parser_find_by_node (parent, (guchar*)"nick") != NULL) {
		searchSetAllowsNick (s, TRUE);
	}

	if (gj_parser_find_by_node (parent, (guchar*)"email") != NULL) {
		searchSetAllowsEmail (s, TRUE);
	}

	return s;
}

#ifdef __cplusplus
}
#endif
