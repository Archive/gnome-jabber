/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <glib.h>
#include <gdk/gdkkeysyms.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"
#include "gj_message.h"
#include "gj_translate.h"
#include "gj_history.h"
#include "gj_support.h"
#include "gj_config.h"
#include "gj_jid.h"
#include "gj_chat.h"
#include "gj_connection.h"

#include "gj_gtk_chat.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_connection_settings.h"
#include "gj_gtk_preferences.h"
#include "gj_gtk_user_information.h"
#include "gj_gtk_support.h"
#include "gj_gtk_add_contact.h"
#include "gj_gtk_stock.h"


struct t_GjChatWindow  {
	GtkWidget *window;

	GtkWidget *textview_send;
	GtkWidget *textview_receive;

	GtkWidget *image_composing;
	GtkWidget *image_status;
	GtkWidget *label_status;

	GtkWidget *button_send;
	GtkWidget *button_add;
	GtkWidget *button_close;
/* 	GtkWidget *button_emote; */

	GtkWidget *menubar;

	GtkWidget *menu_emote;

	/* members */
	GjConnection c;
	GjJID connected_jid;

	gint id;

	gint ri_id;

	gchar *nickname;
	gchar *groups;

	gchar *thread;
 
	gboolean reverse_text_direction;

	gint last_recv_x_event_type;
	gchar *last_recv_x_event_id;
  
	gint last_sent_x_event_type;
	gchar *last_sent_x_event_id;

	gint composing_timeout_id;
	gboolean composing_timeout_stop;
    
	gboolean composing_typing;
	guint composing_duration;
	guint composing_stopped;

	gboolean other_party_composing;
	guint other_party_idle;
};


static GjChatWindow *gj_gtk_chat_load (GjConnection c,
				       GjJID connected_jid, 
				       GjRosterItem ri);

static gboolean gj_gtk_chat_handle_message (GjChatWindow *window, GjMessage m);

gboolean gj_gtk_chat_send (GjChatWindow *window, gchar *text);
gboolean gj_gtk_chat_receive (GjChatWindow *window, GjMessage m);
static void gj_gtk_chat_receive_x_event (GjChatWindow *window, GjMessage m);

/* find functions */
static GjChatWindow *gj_gtk_chat_find_by_id (gint id);
static GjChatWindow *gj_gtk_chat_find_by_ri_id (gint id);

/* gets */
static gint gj_gtk_chat_get_id (GjChatWindow *window);

static gint gj_gtk_chat_get_ri_id (GjChatWindow *window);

static const gchar *gj_gtk_chat_get_nickname (GjChatWindow *window);
static const gchar *gj_gtk_chat_get_groups (GjChatWindow *window);
static const gchar *gj_gtk_chat_get_thread (GjChatWindow *window);

static gboolean gj_gtk_chat_get_reverse_text_direction (GjChatWindow *window);

static gint gj_gtk_chat_get_last_recv_x_event_type (GjChatWindow *window);
static const gchar *gj_gtk_chat_get_last_recv_x_event_id (GjChatWindow *window);

static guint gj_gtk_chat_get_composing_timeout_id (GjChatWindow *window);
static gboolean gj_gtk_chat_get_composing_timeout_stop (GjChatWindow *window);

static gint gj_gtk_chat_get_composing_duration (GjChatWindow *window);
static gint gj_gtk_chat_get_composing_stopped (GjChatWindow *window);

static gboolean gj_gtk_chat_other_party_composing_cb (gpointer data);
static gboolean gj_gtk_chat_other_party_composing (GjChatWindow *window, gboolean composing);

/* sets */
static gboolean gj_gtk_chat_set_thread (GjChatWindow *window, const gchar *thread);

static gboolean gj_gtk_chat_set_reverse_text_direction (GjChatWindow *window, gboolean reverse_text_direction);

static gboolean gj_gtk_chat_set_last_recv_x_event_type (GjChatWindow *window, gint type);
static gboolean gj_gtk_chat_set_last_recv_x_event_id (GjChatWindow *window, const gchar *id);

static gboolean gj_gtk_chat_set_last_sent_x_event_type (GjChatWindow *window, gint type);
static gboolean gj_gtk_chat_set_last_sent_x_event_id (GjChatWindow *window, const gchar *id);

static gboolean gj_gtk_chat_set_composing_timeout_id (GjChatWindow *window, guint id);
static gboolean gj_gtk_chat_set_composing_timeout_stop (GjChatWindow *window, gboolean stop);

static gboolean gj_gtk_chat_set_composing_duration (GjChatWindow *window, gint duration);
static gboolean gj_gtk_chat_set_composing_stopped (GjChatWindow *window, gint stopped);

/* utilities */
static gboolean gj_gtk_chat_setup (GjChatWindow *window);

static gboolean gj_gtk_chat_print_offline (GjChatWindow *window, GjMessage m);

/* composing */
static gboolean gj_gtk_chat_composing (GjChatWindow *window);
static gboolean gj_gtk_chat_cancelled (GjChatWindow *window);

static gboolean gj_gtk_chat_composing_start (GjChatWindow *window);
static gboolean gj_gtk_chat_composing_stop (GjChatWindow *window);
static gboolean gj_gtk_chat_composing_timeout_cb (gpointer user_data);

/* internal callbacks */
static void gj_gtk_chat_emote_menu_cb (GtkMenuItem *menuitem, gpointer user_data);

/* gui callbacks */
static void on_textbuffer_receive_changed (GtkTextBuffer *textbuffer, GjChatWindow *window);
static gboolean on_textview_receive_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjChatWindow *window);
static gboolean on_textview_receive_button_press_event (GtkWidget *widget, GdkEventButton *event, GjChatWindow *window);
static gboolean on_textview_send_key_release_event (GtkWidget *widget, GdkEventKey *event, GjChatWindow *window);
static gboolean on_textview_send_key_press_event (GtkWidget *widget, GdkEventKey *event, GjChatWindow *window);

static void on_button_emote_clicked (GtkButton *button, GjChatWindow *window);

static void on_information_activate (GtkMenuItem *menuitem, GjChatWindow *window);
static void on_history_activate (GtkMenuItem *menuitem, GjChatWindow *window);

static void on_realize (GtkWidget *widget, GjChatWindow *window);
static void on_destroy (GtkWidget *widget, GjChatWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjChatWindow *window);


static GList *chat_windows = NULL;


/*
 * find functions 
 */
static GjChatWindow *gj_gtk_chat_find_by_id (gint id)
{
	gint index = 0;

	if (id < 0) {
		g_warning ("%s: roster item id:%d was < 0", __FUNCTION__, id);
		return NULL;
	}

	for (index=0;index<g_list_length (chat_windows);index++) {
		GjChatWindow *window = g_list_nth_data (chat_windows, index);

		if (window == NULL) { 
			continue; 
		}

		if (gj_gtk_chat_get_id (window) == id) {
			return window;
		}
	}

	return NULL;  
}

static GjChatWindow *gj_gtk_chat_find_by_ri_id (gint id)
{
	gint index = 0;

	if (id < 0) {
		g_warning ("%s: roster item id:%d was < 0", __FUNCTION__, id);
		return NULL;
	}

	for (index=0;index<g_list_length (chat_windows);index++) {
		GjChatWindow *window = g_list_nth_data (chat_windows, index);

		if (window == NULL) {
			continue;
		}

		if (gj_gtk_chat_get_ri_id (window) == id) {
			return window; 
		}
	}

	return NULL;  
}

/* 
 * member functions 
 */

/* gets */ 
static gint gj_gtk_chat_get_id (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return -1;
	}

	return window->id;
}

static gint gj_gtk_chat_get_ri_id (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return -1;
	}

	return window->ri_id;
}

static const gchar *gj_gtk_chat_get_nickname (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return NULL;
	}

	return window->nickname;
}

static const gchar *gj_gtk_chat_get_groups (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return NULL;
	}

	return window->groups;
}

static const gchar *gj_gtk_chat_get_thread (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return NULL;
	}

	return window->thread;
}

static gboolean gj_gtk_chat_get_reverse_text_direction (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	return window->reverse_text_direction;
}

static gint gj_gtk_chat_get_last_recv_x_event_type (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return 0;
	}

	return window->last_recv_x_event_type;
}

static const gchar *gj_gtk_chat_get_last_recv_x_event_id (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return NULL;
	}

	return window->last_recv_x_event_id;
}

static guint gj_gtk_chat_get_composing_timeout_id (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	return window->composing_timeout_id;
}

static gboolean gj_gtk_chat_get_composing_timeout_stop (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	return window->composing_timeout_stop;
}

static gboolean gj_gtk_chat_get_composing_typing (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	return window->composing_typing;
}

static gint gj_gtk_chat_get_composing_duration (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	return window->composing_duration;

}

static gint gj_gtk_chat_get_composing_stopped (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	return window->composing_stopped;
}


/* sets */
static gboolean gj_gtk_chat_set_thread (GjChatWindow *window, const gchar *thread)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	if (thread == NULL) {
		g_warning ("%s: thread was NULL", __FUNCTION__);
		return FALSE;
	}    

	g_free (window->thread);
	window->thread = g_strdup (thread);

	return TRUE;
}

static gboolean gj_gtk_chat_set_reverse_text_direction (GjChatWindow *window, gboolean reverse_text_direction)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	window->reverse_text_direction = reverse_text_direction;
	return TRUE;
}

static gboolean gj_gtk_chat_set_last_recv_x_event_type (GjChatWindow *window, gint type)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	window->last_recv_x_event_type = type;
	return TRUE;
}

static gboolean gj_gtk_chat_set_last_recv_x_event_id (GjChatWindow *window, const gchar *id)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return FALSE;
	}    

	if (window->last_recv_x_event_id != NULL) {
		g_free (window->last_recv_x_event_id);
		window->last_recv_x_event_id = NULL;
	}

	g_message ("%s: GjChatWindow:0x%.8x, id:'%s'", __FUNCTION__, (gint)window, id);
	window->last_recv_x_event_id = g_strdup (id);
	return TRUE;
}

static gboolean gj_gtk_chat_set_last_sent_x_event_type (GjChatWindow *window, gint type)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	window->last_sent_x_event_type = type;
	return TRUE;
}

static gboolean gj_gtk_chat_set_last_sent_x_event_id (GjChatWindow *window, const gchar *id)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return FALSE;
	}    

	g_free (window->last_sent_x_event_id);
	g_message ("%s: GjChatWindow:0x%.8x id:'%s'", __FUNCTION__, (gint)window, id);
	window->last_sent_x_event_id = g_strdup (id);

	return TRUE;
}

static gboolean gj_gtk_chat_set_composing_timeout_id (GjChatWindow *window, guint id)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	window->composing_timeout_id = id;
	return TRUE;
}

static gboolean gj_gtk_chat_set_composing_timeout_stop (GjChatWindow *window, gboolean stop)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	window->composing_timeout_stop = stop;
	return TRUE;
}

static gboolean gj_gtk_chat_set_composing_typing (GjChatWindow *window, gboolean composing)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	window->composing_typing = composing;
	return TRUE;
}

static gboolean gj_gtk_chat_set_composing_duration (GjChatWindow *window, gint duration)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}
    
	if (duration < 0) {
		g_warning ("%s: duration value:%d is < 0", __FUNCTION__, duration);
		return FALSE;
	}
    
	window->composing_duration = duration;
	return TRUE;
}

static gboolean gj_gtk_chat_set_composing_stopped (GjChatWindow *window, gint stopped)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}
    
	if (stopped < 0) {
		g_warning ("%s: stopped value:%d is < 0", __FUNCTION__, stopped);
		return FALSE;
	}
    
	window->composing_stopped = stopped;
	return TRUE;
}


/*
 * window functions 
 */
static GjChatWindow *gj_gtk_chat_load (GjConnection c,
				       GjJID connected_jid, 
				       GjRosterItem ri)
{
	GladeXML *xml = NULL;
	GdkPixbuf *pb = NULL;

	GtkWidget *menu = NULL;

	GjChatWindow *window = NULL;

	if (c == NULL) {
		g_warning ("%s: connected was NULL", __FUNCTION__);
		return NULL;
	}

	if (connected_jid == NULL) {
		g_warning ("%s: connected jid was NULL", __FUNCTION__);
		return NULL;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	window = (GjChatWindow*) g_new0 (struct t_GjChatWindow, 1);

	window->id = gj_get_unique_id ();

	window->ri_id = gj_roster_item_get_id (ri);
  
	window->nickname = g_strdup (gj_roster_item_get_name (ri));
	window->groups = gj_roster_item_get_groups_as_string (ri);
   
	window->composing_timeout_id = -1;

	window->c = gj_connection_ref (c);
	window->connected_jid = gj_jid_ref (connected_jid);

  	chat_windows = g_list_append (chat_windows, window);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "chat",
				     NULL,
				     "chat", &window->window,
				     "textview_send", &window->textview_send,
				     "textview_receive", &window->textview_receive,
				     "image_composing", &window->image_composing,
				     "image_status", &window->image_status,
				     "label_status", &window->label_status,
/* 				     "button_emote", &window->button_emote, */
				     "menubar", &window->menubar,
				     "emote", &window->menu_emote,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "chat", "delete_event", on_delete_event,
			      "chat", "destroy", on_destroy,
			      "chat", "realize", on_realize,
/* 			      "button_emote", "clicked", on_button_emote_clicked, */
			      "information", "activate", on_information_activate,
			      "history", "activate", on_history_activate,
			      "textview_receive", "button_press_event", on_textview_receive_button_press_event,
			      "textview_receive", "motion_notify_event", on_textview_receive_motion_notify_event,
			      "textview_send", "key_press_event", on_textview_send_key_press_event,
			      "textview_send", "key_release_event", on_textview_send_key_release_event,
			      NULL);

	g_object_unref (xml);

	/* set icon */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_MESSAGE_CHAT);
	gtk_window_set_icon (GTK_WINDOW (window->window), pb);
	g_object_unref (G_OBJECT (pb));

	/* set emote icon menu */
	menu = gj_gtk_emote_menu_new ((GjMenuCallback)gj_gtk_chat_emote_menu_cb, 
				      GINT_TO_POINTER (gj_gtk_chat_get_id (window)));

	/* set as child to toplevel menu */
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (window->menu_emote), menu);

	return window;
}

static gboolean gj_gtk_chat_setup (GjChatWindow *window)
{
	gchar *title = NULL;
  
	GtkTextBuffer *buffer;

	GjRosterItem ri = NULL;
	GjJID j = NULL;
	GjPresence pres = NULL;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set title */
	title = g_strdup_printf ("%s %s %s%s%s",
				 _("Chatting with"),
				 gj_gtk_chat_get_nickname (window),
				 gj_gtk_chat_get_groups (window)?"[":"",
				 gj_gtk_chat_get_groups (window)?gj_gtk_chat_get_groups (window):"",
				 gj_gtk_chat_get_groups (window)?"]":"");
	gtk_window_set_title (GTK_WINDOW (window->window), title);
	g_free (title);

	/* set buffer iterator at position 0 */
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_receive));
  
	/* set function callbacks */
	g_signal_connect ((gpointer) buffer, "changed", 
			  G_CALLBACK (on_textbuffer_receive_changed), 
			  window);
 
	/* set styles */
	gtk_text_buffer_create_tag (buffer, "bold", 
				    "weight", PANGO_WEIGHT_BOLD, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "italic", 
				    "style", PANGO_STYLE_ITALIC, 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "blue_foreground", 
				    "foreground", "darkblue", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "red_foreground", 
				    "foreground", "darkred", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "green_foreground", 
				    "foreground", "darkgreen", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "black_foreground", 
				    "foreground", "black", 
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "grey_foreground", 
				    "foreground", "grey", 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "purple_foreground", 
				    "foreground", "purple", 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "url", 
				    "foreground", "blue", 
				    NULL); 

	/* find roster item by JID */
	if ((ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window))) == NULL) {
		g_message ("%s: could not find roster item from id:%d", __FUNCTION__, gj_gtk_chat_get_ri_id (window)); 
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	if ((pres = gj_roster_item_get_presence (ri)) == NULL) {
		g_warning ("%s: could not get presence from roster item with jid:'%s'", __FUNCTION__, gj_jid_get_full (j)); 
		return FALSE;
	}  

	/* set text direction */
	gj_gtk_chat_set_reverse_text_direction (window, gj_config_get_mpf_reverse_text_direction ());

	/* set presence */
	gj_gtk_chat_presence_update (ri, pres);

	return TRUE;
}

gboolean gj_gtk_chat_post_by_event (GjConnection c,
				    GjJID connected_jid, 
				    GjEvent ev, 
				    gboolean show_window)
{
	GjChatWindow *window = NULL;
	GjRosterItem ri = NULL;

	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}

	window = gj_gtk_chat_find_by_ri_id (gj_event_get_ri_id (ev));
	ri = gj_rosters_find_item_by_id (gj_event_get_ri_id (ev));
  
	if (window == NULL) {
		g_message ("%s: could not find GjChatWindow by roster item id:%d, creating new...", 
			   __FUNCTION__, gj_event_get_ri_id (ev));

		if (ri == NULL) {
			g_warning ("%s: could not find roster item from id:%d", __FUNCTION__, gj_event_get_ri_id (ev));
			return FALSE;
		}
     
		/* create */
		window = gj_gtk_chat_load (c, connected_jid, ri);
		gj_gtk_chat_setup (window);
	}

	if (!GTK_WIDGET_VISIBLE (window->window) && gj_config_get_mpf_open_window_on_new_message () == TRUE) {
		/* SHESKARS request for poping like Gabber when 
		   we get messages from unknown contacts. */
		g_message ("%s: presenting chat (option)", __FUNCTION__);
		gtk_widget_show (window->window);
		gdk_window_raise (window->window->window);
	} else if (show_window) {
		/* show or not based on the event stack and if the 
		   user has it flashing at the moment or not. */
		gtk_widget_show (window->window);
	}
  
	if (!GTK_WIDGET_VISIBLE (window->window)) {
		return FALSE;
	}

	gj_gtk_chat_handle_message (window, (GjMessage)gj_event_get_data (ev));
	return TRUE;
}

gboolean gj_gtk_chat_send_new (GjConnection c,
			       GjJID connected_jid, 
			       GjRosterItem ri)
{
	GjChatWindow *window = NULL;

	/* check for current chat window for this JID first */
	window = gj_gtk_chat_find_by_ri_id (gj_roster_item_get_id (ri));
	
	if (!window) {
		window = gj_gtk_chat_load (c, connected_jid, ri);

		if (!window) {
			return FALSE;
		}

		gj_gtk_chat_setup (window);
	}

	gtk_widget_show (window->window);
	return TRUE;
}

GjChatWindow *gj_gtk_chat_send_new_by_jid (GjConnection c,
					   GjJID connected_jid, 
					   GjRoster r, 
					   const gchar *jid)
{
	GjChatWindow *window = NULL;
	GjRosterItem ri = NULL;  
	GjJID j = NULL;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	j = gj_jid_new (jid);
	if (j == NULL) {
		g_warning ("%s: jid was invalid", __FUNCTION__);
		return FALSE;
	}
  
	ri = gj_rosters_find_item_by_jid (j);
	gj_jid_unref (j);

	if (ri == NULL) {
		ri = gj_roster_item_new_unknown_by_jid (jid);
		if (ri == NULL) {
			g_warning ("%s: could not create new temporary roster item", __FUNCTION__);
			return NULL;
		}

		/* add to roster */
		gj_roster_add_item (r, ri);

		g_message ("%s: created temporary roster item for new outbound chat", __FUNCTION__);

		/* use ri */
		g_message ("%s: found previous roster item for new outbound chat", __FUNCTION__); 
	}

	if ((window = gj_gtk_chat_find_by_ri_id (gj_roster_item_get_id (ri))) == NULL) {
		window = gj_gtk_chat_load (c, connected_jid, ri);
		gj_gtk_chat_setup (window);
	}

	gtk_widget_show (window->window);
	return window;
}

static gboolean gj_gtk_chat_handle_message (GjChatWindow *window, GjMessage m)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

  
	/*   if (gj_message_get_type (m) != messageTypeChat) */
	/*     return TRUE; */

	if (gj_message_get_type (m) == GjMessageTypeChat) {
		gj_gtk_chat_receive (window, m);
	}

	if (gj_message_get_type (m) == GjMessageTypeXEvent) {
		gj_gtk_chat_receive_x_event (window, m);
	}

	return TRUE;
}

gboolean gj_gtk_chat_send (GjChatWindow *window, gchar *text)
{
	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextMark *mark = NULL;
	GtkTextIter iter;

	gchar *message = NULL;
	gchar *message_id = NULL;
	gint message_event = 0;
	gchar *time = NULL;

	GjRosterItem ri = NULL;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	if (text == NULL) {
		g_warning ("%s: text was NULL, can not send NULL", __FUNCTION__);
		return FALSE;
	}

	if ((textview = GTK_TEXT_VIEW (window->textview_receive)) == NULL) {
		g_warning ("%s: textview was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window))) == NULL) {
		g_warning ("%s: could not find roster item by id:%d", __FUNCTION__, gj_gtk_chat_get_ri_id (window));
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (textview);
	time = g_strdup_printf ("[%s]", gj_get_timestamp ());
  
	/* get text direction */
	if (gj_gtk_chat_get_reverse_text_direction (window) == TRUE) {
		gtk_text_buffer_get_start_iter (buffer, &iter);
	} else {
		gtk_text_buffer_get_end_iter (buffer, &iter);
	}

	/* insert at iter */
	if (gj_config_get_mpf_hide_time () == FALSE) {
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  time, -1,
							  "grey_foreground",
							  NULL);
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  " ", -1,
							  "grey_foreground",
							  NULL);
	}

	if (strncmp (text, "/me", 3) == 0) {
		message = g_strdup_printf ("%s %s\n", gj_gtk_roster_config_nick (), text+4);
      
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  message, -1,
							  /* 					       "bold",  */
							  "purple_foreground",
							  NULL);
	} else {
		/* set details */
		message = g_strdup_printf (": %s\n", text);

		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  gj_gtk_roster_config_nick (), -1,
							  "bold", 
							  "green_foreground",
							  NULL);
      
		if (gj_config_get_mpf_use_smileys () == TRUE) {
			gj_gtk_textbuffer_insert_with_emote_icons (buffer, &iter, "black_foreground", message);
		} else {
			gj_gtk_textbuffer_insert (buffer, &iter, "black_foreground", message); 
		}
	}


	/* scroll to end if not reversing text direction */
	if (gj_gtk_chat_get_reverse_text_direction (window) == FALSE) {
		mark = gtk_text_buffer_create_mark (buffer, NULL, &iter, FALSE);
		gtk_text_view_scroll_mark_onscreen (textview, mark);
		gtk_text_buffer_delete_mark (buffer, mark);
	}

	/* set message */
	message_event = GjMessageXEventTypeOffline | 
		GjMessageXEventTypeDelivered | 
		GjMessageXEventTypeDisplayed | 
		GjMessageXEventTypeComposing;

	message_id = gj_message_send_with_x_event_request_by_ri (window->c,
								 GjMessageTypeChat, 
								 gj_jid_get_full (window->connected_jid),
								 ri, 
								 NULL, 
								 text, 
								 gj_gtk_chat_get_thread (window), 
								 message_event);

	/* send cancelled event */
	gj_gtk_chat_cancelled (window);

#if 0
	/* log to history */
	gj_history_write_sent_by_ri_id (gj_gtk_chat_get_ri_id (window), 
					(gchar*)gj_get_timestamp_nice (), 
					NULL, 
					text);
#endif

	/* save details */
	gj_gtk_chat_set_last_sent_x_event_id (window, message_id);
	gj_gtk_chat_set_last_sent_x_event_type (window, message_event);
	/* should we use a thread? */

	/* clean up */
	g_free (time);
	g_free (message);
	g_free (message_id);

	return TRUE;
}

gboolean gj_gtk_chat_receive (GjChatWindow *window, GjMessage m)
{
	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextMark *mark = NULL;
	GtkTextIter iter;

	gchar *message = NULL;
	gchar *time = NULL;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((textview = GTK_TEXT_VIEW (window->textview_receive)) == NULL) {
		g_warning ("%s: textview was NULL", __FUNCTION__);
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_receive));
	time = g_strdup_printf ("[%s]", gj_get_timestamp_from_timet (gj_message_get_time (m)));

	/* get text direction */
	if (gj_gtk_chat_get_reverse_text_direction (window) == TRUE) {
		gtk_text_buffer_get_start_iter (buffer, &iter);
	} else {
		gtk_text_buffer_get_end_iter (buffer, &iter);
	}

	if (gj_config_get_mpf_hide_time () == FALSE) {
		/* insert at iter */
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  time, -1,
							  "grey_foreground",
							  NULL);
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  " ", -1,
							  "grey_foreground",
							  NULL);
	}

	if (gj_message_get_body (m) != NULL && strncmp (gj_message_get_body (m), "/me", 3) == 0) {
		message = g_strdup_printf ("%s %s\n", gj_gtk_chat_get_nickname (window), gj_message_get_body (m)+4);
      
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  message, -1,
							  /* 					       "bold",  */
							  "purple_foreground",
							  NULL);
	} else {
		message = g_strdup_printf (": %s\n", gj_message_get_body (m));

		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  gj_gtk_chat_get_nickname (window), -1,
							  "bold", 
							  "blue_foreground",
							  NULL);

		if (gj_config_get_mpf_use_smileys () == TRUE) {
			gj_gtk_textbuffer_insert_with_emote_icons (buffer, &iter, "black_foreground", message);
		} else {
			gj_gtk_textbuffer_insert (buffer, &iter, "black_foreground", message); 
		}
	}

#if 0 
	/* log to history */
	gj_history_write_received_by_ri_id (gj_gtk_chat_get_ri_id (window), 
					    (gchar*)gj_get_timestamp_nice_from_timet (gj_message_get_time (m)), 
					    NULL, 
					    gj_message_get_body (m));
#endif

	if (gj_gtk_chat_get_reverse_text_direction (window) == FALSE) {
		mark = gtk_text_buffer_create_mark (buffer, NULL, &iter, FALSE);
		gtk_text_view_scroll_mark_onscreen (textview, mark);
		gtk_text_buffer_delete_mark (buffer, mark);
	}

	/* save details */
	gj_gtk_chat_set_last_recv_x_event_type (window, gj_message_get_x_event_type (m));
	gj_gtk_chat_set_last_recv_x_event_id (window, gj_message_get_id (m)?gj_message_get_id (m):gj_message_get_thread (m));
	gj_gtk_chat_set_thread (window, gj_message_get_thread (m));

	/* if options request it, raise the window */
	if (gj_config_get_mpf_raise_window_on_message () == TRUE) {
		/*       gtk_window_present (GTK_WINDOW (window->window));  */
		gdk_window_raise (GDK_WINDOW (window->window->window)); 
		/*       gtk_widget_hide (GTK_WIDGET (window->window)); */
		/*       gtk_widget_show (GTK_WIDGET (window->window)); */
	}
    
	/* clean up */
	g_free (time);
	g_free (message);

	return TRUE;
}

static void gj_gtk_chat_receive_x_event (GjChatWindow *window, GjMessage m)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return;
	}
   
	switch (gj_message_get_x_event_type (m)) {
	case GjMessageXEventTypeOffline:
		gj_gtk_chat_print_offline (window, m);
		break;
	case GjMessageXEventTypeDelivered:
		break;
	case GjMessageXEventTypeDisplayed: 
		break;
	case GjMessageXEventTypeComposing:
		gj_gtk_chat_other_party_composing (window, TRUE);
		break;
	case GjMessageXEventTypeCancelled:
		gj_gtk_chat_other_party_composing (window, FALSE);
		break;
	}
}

static gboolean gj_gtk_chat_other_party_composing (GjChatWindow *window, gboolean composing)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	window->other_party_idle = 0;
 
	if (window->other_party_composing == composing) {
		return TRUE;
	}

	window->other_party_composing = composing;

	if (composing == TRUE) {
		g_timeout_add (1000, gj_gtk_chat_other_party_composing_cb, GINT_TO_POINTER (gj_gtk_chat_get_id (window)));
	}

	g_message ("%s: other party %s", __FUNCTION__, composing ? "is composing still" : "is not composing");

	if (composing == FALSE) {
		gtk_widget_hide (GTK_WIDGET (window->image_composing));
	} else {
		gtk_widget_show (GTK_WIDGET (window->image_composing));
	}
  
	return TRUE;
}

static gboolean gj_gtk_chat_other_party_composing_cb (gpointer data)
{
	gint id = 0;
	GjChatWindow *window = NULL;
  
	if (data == NULL) {
		g_warning ("%s: data was NULL", __FUNCTION__);
		return FALSE;
	}

	id = GPOINTER_TO_INT (data);

	if ((window = gj_gtk_chat_find_by_id (id)) == NULL) {
		g_warning ("%s: GjChatWindow was NULL from id:%d", __FUNCTION__, id);
		return FALSE;
	}

	if (window->other_party_composing == FALSE) {
		return FALSE;
	}

	window->other_party_idle++;

	if (window->other_party_idle >= 5) { 
		g_message ("%s: other party has been idle for %d seconds", __FUNCTION__, window->other_party_idle);

		gj_gtk_chat_other_party_composing (window, FALSE);
		return FALSE;
	}
   
	return TRUE;
}

gboolean gj_gtk_chat_presence_update (GjRosterItem ri, GjPresence pres)
{
	const gchar *stock_id = NULL;
	gchar *text = NULL;

	GjChatWindow *window = NULL;
	GjJID j = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((window = gj_gtk_chat_find_by_ri_id (gj_roster_item_get_id (ri))) == NULL) {
		g_message ("%s: no chat window for presence update for jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
		return FALSE;
	}

	text = gj_gtk_presence_to_show_text_new (gj_presence_get_type (pres), gj_presence_get_show (pres));
	stock_id = gj_gtk_presence_to_stock_id (gj_presence_get_type (pres), gj_presence_get_show (pres));

	/* update image and label */
	gtk_image_set_from_stock (GTK_IMAGE (window->image_status), stock_id, GTK_ICON_SIZE_MENU);
	gtk_label_set_text (GTK_LABEL (window->label_status), text);

	/* clean up */
	g_free (text);

	return TRUE;
}

static gboolean gj_gtk_chat_print_offline (GjChatWindow *window, GjMessage m)
{
	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextMark *mark = NULL;
	GtkTextIter iter;

	gchar *message = NULL;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((textview = GTK_TEXT_VIEW (window->textview_receive)) == NULL) {
		g_warning ("%s: textview was NULL", __FUNCTION__);
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_receive));

	/* set details */
	message = g_strdup_printf (": %s\n", _("User Offline"));

	/* get text direction */
	if (gj_gtk_chat_get_reverse_text_direction (window) == TRUE) {
		gtk_text_buffer_get_start_iter (buffer, &iter);
	} else {
		gtk_text_buffer_get_end_iter (buffer, &iter);
	}

	if (gj_config_get_mpf_hide_time () == FALSE) {
		gchar *time = NULL;
		time = g_strdup_printf ("[%s]", gj_get_timestamp_from_timet (gj_message_get_time (m)));

		/* insert at iter */
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  time, -1,
							  "grey_foreground",
							  NULL);
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
							  " ", -1,
							  "grey_foreground",
							  NULL);

		/* clean up */
		g_free (time);
	}

	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
						  message, -1,
						  "bold", 
						  "red_foreground",
						  NULL);

	if (gj_gtk_chat_get_reverse_text_direction (window) == FALSE) {
		mark = gtk_text_buffer_create_mark (buffer, NULL, &iter, FALSE);
		gtk_text_view_scroll_mark_onscreen (textview, mark);
		gtk_text_buffer_delete_mark (buffer, mark);
	}

	return TRUE;
}

static gboolean gj_gtk_chat_composing_start (GjChatWindow *window)
{
	guint id = 0;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	id = g_timeout_add (1000, (GSourceFunc)gj_gtk_chat_composing_timeout_cb, GINT_TO_POINTER (gj_gtk_chat_get_id (window)));

	gj_gtk_chat_set_composing_timeout_id (window, id);
	gj_gtk_chat_set_composing_timeout_stop (window, FALSE);

	return TRUE;
}

static gboolean gj_gtk_chat_composing_stop (GjChatWindow *window)
{
	guint id = 0;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}

	id = gj_gtk_chat_get_composing_timeout_id (window);
	gj_gtk_chat_set_composing_timeout_id (window, -1);
	gj_gtk_chat_set_composing_timeout_stop (window, TRUE);

	return TRUE;
}

static gboolean gj_gtk_chat_composing (GjChatWindow *window)
{
	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}
    
	if (gj_gtk_chat_get_composing_timeout_id (window) == -1) {
		GjRosterItem ri = NULL;

		/* it is stopped so we start it */
		gj_gtk_chat_composing_start (window);

		if ((ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window))) == NULL) {
			g_warning ("%s: could not find roster item by id:%d", __FUNCTION__, gj_gtk_chat_get_ri_id (window));
			return FALSE;
		}
	
		/* send the composing event */
		if (gj_gtk_chat_get_last_recv_x_event_type (window) & GjMessageXEventTypeComposing) {
			gj_message_send_x_event_by_ri (window->c,
						       GjMessageXEventTypeComposing, 
						       gj_jid_get_full (window->connected_jid),
						       ri, 
						       gj_gtk_chat_get_last_recv_x_event_id (window));
		}
	}

	gj_gtk_chat_set_composing_typing (window, TRUE);

	return TRUE;
}

static gboolean gj_gtk_chat_cancelled (GjChatWindow *window)
{
	GjRosterItem ri = NULL;

	if (window == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return FALSE;
	}
    
	if (gj_gtk_chat_get_composing_timeout_id (window) != -1) {
		/* it is stopped so we start it */
		gj_gtk_chat_composing_stop (window);
	}


	/* send the composing event */
	if ((ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window))) == NULL) {
		g_warning ("%s: could not find roster item by id:%d", __FUNCTION__, gj_gtk_chat_get_ri_id (window));
		return FALSE;
	}
    
	if (gj_gtk_chat_get_last_recv_x_event_type (window) & GjMessageXEventTypeComposing) {
		gj_message_send_x_event_by_ri (window->c,
					       GjMessageXEventTypeCancelled, 
					       gj_jid_get_full (window->connected_jid),
					       ri, 
					       gj_gtk_chat_get_last_recv_x_event_id (window));
	}

	gj_gtk_chat_set_composing_typing (window, FALSE);

	return TRUE;
}

static gboolean gj_gtk_chat_composing_timeout_cb (gpointer user_data)
{
	gint id = 0;
	GjChatWindow *window = NULL;

	id = GPOINTER_TO_INT (user_data);

	if ((window = gj_gtk_chat_find_by_id (id)) == NULL) {
		g_warning ("%s: GjChatWindow was NULL from id:%d", __FUNCTION__, id);
		return FALSE;
	}
  
	if (gj_gtk_chat_get_composing_timeout_stop (window) == TRUE) {
		return FALSE;
	}

	if (gj_gtk_chat_get_composing_typing (window) == TRUE) {
		gj_gtk_chat_set_composing_duration (window, gj_gtk_chat_get_composing_duration (window) + 1);
		gj_gtk_chat_set_composing_stopped (window, 0); 
	} else {
		gj_gtk_chat_set_composing_duration (window, gj_gtk_chat_get_composing_duration (window) + 1);
		gj_gtk_chat_set_composing_stopped (window, gj_gtk_chat_get_composing_stopped (window) + 1);
	}

	/*     g_message ("%s: composing duration:%d seconds, composing_stopped:%d seconds",  */
	/* 	      __FUNCTION__, (gint)gj_gtk_chat_get_composing_duration (window), (gint)gj_gtk_chat_get_composing_stopped (window)); */
    
	if (gj_gtk_chat_get_composing_duration (window) >= 16) {
		GjRosterItem ri = NULL;


		if ((ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window))) == NULL) {
			g_warning ("%s: could not find roster item by id:%d", __FUNCTION__, gj_gtk_chat_get_ri_id (window));
			return FALSE;
		}

		/* send another composing event */
		if (gj_gtk_chat_get_last_recv_x_event_type (window) & GjMessageXEventTypeComposing) {
			gj_message_send_x_event_by_ri (window->c,
						       GjMessageXEventTypeComposing, 
						       gj_jid_get_full (window->connected_jid),
						       ri, 
						       gj_gtk_chat_get_last_recv_x_event_id (window));
		}
	
		/* reset all */
		gj_gtk_chat_set_composing_duration (window, 0);
		gj_gtk_chat_set_composing_stopped (window, 0);
	} else if (gj_gtk_chat_get_composing_stopped (window) >= 5) {
		GjRosterItem ri = NULL;
		
		if ((ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window))) == NULL) {
			g_warning ("%s: could not find roster item by id:%d", __FUNCTION__, gj_gtk_chat_get_ri_id (window));
			return FALSE;
		}
		
		/* send cancelled event */
		if (gj_gtk_chat_get_last_recv_x_event_type (window) & GjMessageXEventTypeComposing) {
			gj_message_send_x_event_by_ri (window->c,
						       GjMessageXEventTypeCancelled, 
						       gj_jid_get_full (window->connected_jid),
						       ri, 
						       gj_gtk_chat_get_last_recv_x_event_id (window));
		}
		
		gj_gtk_chat_composing_stop (window);
		
		/* reset all */
		gj_gtk_chat_set_composing_duration (window, 0);
		gj_gtk_chat_set_composing_stopped (window, 0);
	}
	
	gj_gtk_chat_set_composing_typing (window, FALSE);
    
	return TRUE;
}

static void gj_gtk_chat_emote_menu_cb (GtkMenuItem *menuitem, gpointer user_data)
{
	gint callback_action = -1;
	gint cw_id = -1;

	gpointer data = NULL;

	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter;

	GjChatWindow *window = NULL;

	/* get object data */
	data = g_object_get_data (G_OBJECT (menuitem), "user_data");

	if (data == NULL) {
		g_warning ("%s: get data failed to retrieve GjChatWindow", __FUNCTION__);
		return;
	}

	if (user_data == NULL) {
		g_warning ("%s: user data was NULL", __FUNCTION__);
		return;
	}

	/* get action/cw_id */
	callback_action = GPOINTER_TO_INT (user_data);
	cw_id = GPOINTER_TO_INT (data);

	g_message ("%s: agent menu activated menuitem:0x%.8x, callback action:%d", 
		   __FUNCTION__, (gint)menuitem, callback_action);
  
	/* get Chat Window */
	if ((window = gj_gtk_chat_find_by_id (cw_id)) == NULL) {
		g_warning ("%s: GjChatWindow was NULL", __FUNCTION__);
		return;
	}  
 
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_send));

	gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (buffer), &iter);
	gtk_text_buffer_insert (buffer, &iter, gj_chat_emote_icon_get_as_text (callback_action), -1);

	/* set focus back to the chat text box */
	gtk_widget_grab_focus (GTK_WIDGET (window->textview_send));
}

/*
 * GUI events
 */
static void on_textbuffer_receive_changed (GtkTextBuffer *textbuffer, GjChatWindow *window)
{
	gj_gtk_textbuffer_handle_changed (textbuffer, window);
}

static gboolean on_textview_receive_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjChatWindow *window)
{
	return gj_gtk_textview_handle_motion_notify_event (widget, event, window);
}

static gboolean on_textview_receive_button_press_event (GtkWidget *widget, GdkEventButton *event, GjChatWindow *window)
{
	return gj_gtk_textview_handle_button_press_event (widget, event, window);
}

static gboolean on_textview_send_key_release_event (GtkWidget *widget, GdkEventKey *event, GjChatWindow *window)
{
	GtkTextBuffer *buffer = NULL;

	if ((buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget))) == NULL) {
		g_warning ("%s: buffer was NULL", __FUNCTION__);
		return FALSE;
	}

	/* if was enter then clear box */
	if (event != NULL && event->keyval == GDK_Return && (event->state & GDK_SHIFT_MASK) == FALSE) {
		GtkTextIter iter_start;
		GtkTextIter iter_end; 
		
		gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
		gtk_text_buffer_delete (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	} else {
		gint length = 0;
		
		/* set toolbar icon */  
		length = gtk_text_buffer_get_char_count (buffer);
	}

	return FALSE;
}

static gboolean on_textview_send_key_press_event (GtkWidget *widget, GdkEventKey *event, GjChatWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end;  

	gchar *text = NULL;
	gint length = 0;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	length = gtk_text_buffer_get_char_count (buffer);

	gj_gtk_chat_composing (window);

	if (length < 1) {
		/* if enter was pressed... warn user that we can not do this */
		if (event != NULL && event->keyval == GDK_Return && (event->state & GDK_SHIFT_MASK) == FALSE)
			g_warning ("%s: can't send NULL or 0 length message", __FUNCTION__);
      
		return FALSE;
	}

	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	text = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end, TRUE);
   
	/* send message and clear text box */
	if (event->keyval == GDK_Return && (event->state & GDK_SHIFT_MASK) == FALSE) {
		gj_gtk_chat_send (window, text);
	}
    
	/* cleanup */
	g_free (text);
    
	return FALSE;
}

void on_button_emote_clicked (GtkButton *button, GjChatWindow *window)
{
	GtkWidget *menu = NULL;

	menu = gj_gtk_emote_menu_new ((GjMenuCallback)gj_gtk_chat_emote_menu_cb, 
				      GINT_TO_POINTER (gj_gtk_chat_get_id (window)));

	/* show menu */
	gtk_menu_popup (GTK_MENU (menu),
			NULL,
			NULL, 
			NULL, 
			NULL,
			1, 
			gtk_get_current_event_time ()); 

}

static void on_information_activate (GtkMenuItem *menuitem, GjChatWindow *window) {
	GjRosterItem ri = NULL;

	ri = gj_rosters_find_item_by_id (gj_gtk_chat_get_ri_id (window));

	if (!ri) {
		g_warning ("%s: roster item was NULL, can not load user information", __FUNCTION__);
		return;
	}

	gj_gtk_ui_load (window->c, ri);
}

static void on_history_activate (GtkMenuItem *menuitem, GjChatWindow *window)
{
	GjHistory hist = NULL;
  
	hist = gj_history_find (gj_gtk_chat_get_ri_id (window));

	if (!hist) {
		g_warning ("%s: could not find history based on roster item id:%d", 
			   __FUNCTION__, gj_gtk_chat_get_ri_id (window));
		return;
	}

	gj_gtk_open_url (gj_history_get_url (hist));
}

static void on_realize (GtkWidget *widget, GjChatWindow *window)
{
	gtk_widget_set_events (GTK_WIDGET (window->textview_receive),
			       GDK_EXPOSURE_MASK | 
			       GDK_POINTER_MOTION_MASK | 
			       GDK_POINTER_MOTION_HINT_MASK | 
			       GDK_BUTTON_RELEASE_MASK | 
			       GDK_STRUCTURE_MASK | 
			       GDK_PROPERTY_CHANGE_MASK);

}

static void on_destroy (GtkWidget *widget, GjChatWindow *window)
{
	if (window == NULL) {
		return;
	}

	chat_windows = g_list_remove (chat_windows, window);

	/* members */
	g_free (window->nickname);
	g_free (window->groups);
	g_free (window->thread);
	g_free (window->last_recv_x_event_id);
	g_free (window->last_sent_x_event_id);
    
	if (window->composing_timeout_id != -1) {
		g_source_remove (window->composing_timeout_id);
		window->composing_timeout_id = -1;
	}

	if (window->other_party_idle != -1) {
		g_source_remove (window->other_party_idle);
		window->other_party_idle = -1;
	}

	gj_connection_unref (window->c);
	gj_jid_unref (window->connected_jid);

	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjChatWindow *window)
{
	gtk_widget_hide (window->window);

	return TRUE;
}

#ifdef __cplusplus
}
#endif
