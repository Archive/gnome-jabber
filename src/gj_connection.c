/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_connection.h"
#include "gj_support.h"


struct t_connection {
	gint ref_count;

	guint id;

	gchar *host;
	guint port;

	GTcpSocket *tcpsocket;
	GTcpSocketConnectAsyncID connect_id; /* used to stop async connects */

	gboolean connected;

	GTimer *uptime;

	/* callbacks */
	GjConnectionCallbackIn cb_in;
	GjConnectionCallbackHup cb_hup;
	GjConnectionCallbackConnect cb_connect;
	GjConnectionCallbackDisconnect cb_disconnect;
	GjConnectionCallbackError cb_error;
};


static GjConnection gj_connection_new (const gchar *host, const guint port);
static gboolean gj_connection_free (GjConnection c);

static void gj_connection_stats_foreach (GjConnection c, gpointer user_data);


static GList *connection_list = NULL;


static void gj_connection_init ()
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return;
	}

	initiated = TRUE;
}

void gj_connection_stats ()
{
	g_message ("%s: list size:%d", __FUNCTION__, g_list_length (connection_list));

	if (g_list_length (connection_list) > 0) {
		g_list_foreach (connection_list, (GFunc)gj_connection_stats_foreach, NULL);
	}
}

static void gj_connection_stats_foreach (GjConnection c, gpointer user_data)
{
	g_message ("++++++++++++++++++++++++++++++");
	g_message ("connection, id:%d", c->id);
	g_message ("++++++++++++++++++++++++++++++");
	g_message ("ref count:%d", c->ref_count);
	g_message ("host:'%s', port:%d", c->host, c->port);
	g_message ("connected: %s", c->connected ? "yes" : "no");
	g_message ("tcpsocket: 0x%.8x", c->tcpsocket);
}

GjConnection gj_connection_ref (GjConnection c)
{
	if (!c) {
		return NULL;
	}

	c->ref_count++;

	return c;
}

gboolean gj_connection_unref (GjConnection c)
{
	if (!c) {
		return FALSE;
	}

	c->ref_count--;

	if (c->ref_count < 1) {
		gj_connection_free (c); 
	}

	return TRUE;
}

static GjConnection gj_connection_new (const gchar *host, const guint port)
{
	GjConnection c = NULL;

	if (host == NULL || g_utf8_strlen (host, -1) < 1) {
		g_warning ("%s: host was NULL or length was < 1", __FUNCTION__);
		return NULL;
	}

	if (port < 1 || port > 65535) {
		g_warning ("%s: port:%d was invalid, range is (1-65535)", __FUNCTION__, port);
		return NULL;
	}

	gj_connection_init ();

/* 	if ((c = gj_connection_find_by_host_and_port (host, port)) != NULL) { */
/* 		g_warning ("%s: GjConnection already exists with host:'%s' and port:%d", __FUNCTION__, host, port); */
/* 	} */

	if ((c = (GjConnection) g_new0 (struct t_connection,1)) != NULL) {
		c->ref_count = 1;

		c->id = gj_get_unique_id ();

		c->host = g_strdup (host);
		c->port = port;

		c->tcpsocket = NULL;
		c->connect_id = 0;

		c->connected = FALSE;

		c->cb_in = NULL;
		c->cb_hup = NULL;
		c->cb_connect = NULL;
		c->cb_disconnect = NULL;
		c->cb_error = NULL;

		connection_list = g_list_append (connection_list, c);
	}

	return c;
}

static gboolean gj_connection_free (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	connection_list = g_list_remove (connection_list, c);
  
	if (c->host != NULL) {
		g_free (c->host);
		c->host = NULL;
	}

	if (c->tcpsocket != NULL) {
		if (gj_connection_get_connected (c) == TRUE) {
			gj_connection_disconnect (c);
		}

		c->tcpsocket = NULL;
	}

	if (c->uptime) {
 		g_timer_stop (c->uptime); 
 		g_timer_destroy (c->uptime); 
	}

	g_free (c);
  
	return TRUE;
}

GjConnection gj_connection_find_by_host_and_port (const gchar *host, const guint port)
{
	GList *l = NULL;

	if (host == NULL) {
		g_warning ("%s: host was NULL", __FUNCTION__);
		return NULL;
	}

	if (port < 1 || port > 65535) {
		g_warning ("%s: port:%d was invalid, range is (1-65535)", __FUNCTION__, port);
		return NULL;
	}

	for (l = connection_list; l; l = l->next) {
		GjConnection c = (GjConnection) l->data;

		if (!c) {
			continue;
		}

		if (g_strcasecmp (gj_connection_get_host (c), host) == 0 && gj_connection_get_port (c) == port) {
			return c;
		}
	}

	return NULL;  
}

GjConnection gj_connection_find_by_id (const guint id)
{
	GList *l = NULL;

	if (id < 1) {
		g_warning ("%s: id:%d was < 1", __FUNCTION__, id);
		return NULL;
	}

	for (l = connection_list; l; l = l->next) {
		GjConnection c = (GjConnection) l->data;

		if (!c) {
			continue;
		}

		if (gj_connection_get_id (c) == id) {
			return c;
		}
	}

	return NULL;  
}

/*
 * member functions 
 */

/* gets */ 
const guint gj_connection_get_id (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return 0;
	}

	return c->id;
}

const gchar *gj_connection_get_host (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->host;
}

const guint gj_connection_get_port (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return 0;
	}

	return c->port;
}

GTcpSocket *gj_connection_get_tcpsocket (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->tcpsocket;
}

GTcpSocketConnectAsyncID gj_connection_get_connect_id (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return 0;
	}

	return c->connect_id;    
}

gboolean gj_connection_get_connected (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	return c->connected; 
}

GjConnectionCallbackIn gj_connection_get_cb_in (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->cb_in;
}

GjConnectionCallbackHup gj_connection_get_cb_hup (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->cb_hup;
}

GjConnectionCallbackConnect gj_connection_get_cb_connect (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->cb_connect;
}

GjConnectionCallbackDisconnect gj_connection_get_cb_disconnect (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->cb_disconnect;
}

GjConnectionCallbackError gj_connection_get_cb_error (GjConnection c)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	return c->cb_error;
}

/* sets */
gboolean gj_connection_set_host (GjConnection c, const gchar *host)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	if (host == NULL) {
		g_warning ("%s: host was NULL", __FUNCTION__);
		return FALSE;
	}    

	if (c->host != NULL) {
		g_free (c->host);
		c->host = NULL;
	}

	c->host = g_strdup (host);
	return TRUE;
}

gboolean gj_connection_set_port (GjConnection c, const guint port)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	c->port = port;
	return TRUE;
}

gboolean gj_connection_set_tcpsocket (GjConnection c, GTcpSocket *ts)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  
      
	c->tcpsocket = ts;
	return TRUE;
}

gboolean gj_connection_set_connect_id (GjConnection c, GTcpSocketConnectAsyncID connect_id)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	c->connect_id = connect_id;
	return TRUE;
}

gboolean gj_connection_set_connected (GjConnection c, gboolean connected)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  
  
	c->connected = connected;
	return TRUE;
}

gboolean gj_connection_set_cb_in (GjConnection c, GjConnectionCallbackIn cb)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  

	c->cb_in = cb;
	return TRUE;
}

gboolean gj_connection_set_cb_hup (GjConnection c, GjConnectionCallbackHup cb)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  

	c->cb_hup = cb;
	return TRUE;
}

gboolean gj_connection_set_cb_connect (GjConnection c, GjConnectionCallbackConnect cb)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  

	c->cb_connect = cb;
	return TRUE;
}

gboolean gj_connection_set_cb_disconnect (GjConnection c, GjConnectionCallbackDisconnect cb)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  

	c->cb_disconnect = cb;
	return TRUE;
}

gboolean gj_connection_set_cb_error (GjConnection c, GjConnectionCallbackError cb)
{
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}  

	c->cb_error = cb;
	return TRUE;
}

/*
 * GjConnection functions
 */

#ifndef G_OS_WIN32 

gboolean gj_connection_connect (GjConnection c)
{
	GTcpSocketConnectAsyncID connect_id = 0;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}     
    
	g_message ("%s: connection:0x%.8x, connecting asynchronously to host:'%s', port:%d...", 
		   __FUNCTION__, (gint)c, gj_connection_get_host (c), gj_connection_get_port (c));

	connect_id = gnet_tcp_socket_connect_async (gj_connection_get_host (c),
						    gj_connection_get_port (c), 
						    (GTcpSocketConnectAsyncFunc)gj_connection_connect_cb, 
						    GINT_TO_POINTER (gj_connection_get_id (c)));
    
	/* remember id so we can stop it if we need to */
	g_message ("%s: connection:0x%.8x, connect id is %d", __FUNCTION__, (gint)c, (gint)connect_id);
	gj_connection_set_connect_id (c, connect_id);
    
	return TRUE;
}

#else

/* FIXME: Windows glib does has a bug which does not permit me
   to using the async method of connecting.  For the time being, we will
   use synchronous connecting */
gboolean gj_connection_connect (GjConnection c)
{
	GTcpSocket *ts = NULL;
	GIOChannel *ch = NULL;    

	GjConnectionCallbackConnect cbc = NULL;

	gint id = 0;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}     
    
	g_message ("%s: connection:0x%.8x, connecting synchronously to host:'%s', port:%d...", 
		   __FUNCTION__, (gint)c, gj_connection_get_host (c), gj_connection_get_port (c));

	ts = gnet_tcp_socket_connect (gj_connection_get_host (c),
				      gj_connection_get_port (c));
				   
  
	if (ts == NULL) {
		g_warning ("%s: could not create new socket to host:'%s', port:%d", 
			   __FUNCTION__, gj_connection_get_host (c), gj_connection_get_port (c));
		return FALSE;
	}

	id = gj_get_unique_id ();

/* 	gj_connection_set_connect_id (c, 0); */
/* 	gj_connection_set_tcpsocket (c, ts); */
/* 	gj_connection_set_connected (c, TRUE); */

	g_message ("%s: connection:0x%.8x, connected.", __FUNCTION__, (gint)c);
  
	if ((ch = gnet_tcp_socket_get_io_channel (ts)) == NULL) {
		/* FIXME: delete and clear up at this point */
		g_warning ("%s: connection:0x%.8x, GIOChannel was NULL", __FUNCTION__, (gint)c);
		return FALSE;
	}
  
	/* callbacks */
	g_io_add_watch (ch, G_IO_IN | G_IO_ERR | G_IO_NVAL | G_IO_HUP, 
			gj_connection_watch_cb, GINT_TO_POINTER (gj_connection_get_id (c)));   
  
	if ((cbc = gj_connection_get_cb_connect (c)) != NULL) {
		(cbc) (c);
	}

	return TRUE;
}

#endif

GjConnection gj_connection_create (const gchar *host, const guint port)
{
	GjConnection c = NULL;
	const gchar *localhost = NULL;

	if (host == NULL) {
		g_warning ("%s: host was NULL", __FUNCTION__);
		return NULL;
	}
  
	if (port < 1 || port > 65535) {
		g_warning ("%s: port:%d was invalid, range is (1-65535)", __FUNCTION__, port);
		return NULL;
	}

	c = gj_connection_new (host, port);

	if (c == NULL) {
		g_warning ("%s: could not create new GjConnection (for host:'%s', port:%d)", __FUNCTION__, host, port);
		return NULL;
	}

	g_message ("%s: connection:0x%.8x, local host is:'%s'", __FUNCTION__, (gint)c, localhost);
	g_message ("%s: connection:0x%.8x, host:'%s', port:%d", __FUNCTION__, (gint)c, host, port);

	return c;
}

void gj_connection_connect_cb (GTcpSocket *ts, GTcpSocketConnectAsyncStatus status, gpointer data)
{
	GjConnection c = NULL;
	GjConnectionCallbackConnect cbc = NULL;

	GIOChannel *ch = NULL;    
	guint id = 0;

	if (data == NULL) {
		g_warning ("%s: data is NULL, no way to know which GjConnection this is", __FUNCTION__);
		return;
	}

	id = GPOINTER_TO_INT (data);
    
	if ((c = gj_connection_find_by_id (id)) == NULL) {
		g_warning ("%s: could not find GjConnection id:%d", __FUNCTION__, id);
		return;
	}
    
	if (status != GTCP_SOCKET_CONNECT_ASYNC_STATUS_OK) {
		GjConnectionCallbackError cbe = NULL;

		g_message ("%s: GTcpSocket:0x%.8x, GTcpSocketConnectAsyncStatus:%d->'%s'", 
			   __FUNCTION__, (gint)ts, (gint)status, gj_connection_connect_translate_status (status));

		if ((cbe = gj_connection_get_cb_error (c)) != NULL) {
			(cbe) (c, status);
		}
      
		return;
	}

	gj_connection_set_connect_id (c, 0);
	gj_connection_set_tcpsocket (c, ts);
	gj_connection_set_connected (c, TRUE);

	if (c->uptime) {
		g_timer_destroy (c->uptime);
	}

	c->uptime = g_timer_new ();
	g_timer_start (c->uptime); 
  
	g_message ("%s: connection:0x%.8x, connected.", __FUNCTION__, (gint)c);
  
	if ((ch = gnet_tcp_socket_get_io_channel (ts)) == NULL) {
		g_warning ("%s: connection:0x%.8x, GIOChannel was NULL", __FUNCTION__, (gint)c);
		return;
	}
  
	/* callbacks */
	g_io_add_watch (ch, G_IO_IN | G_IO_ERR | G_IO_NVAL | G_IO_HUP, 
			gj_connection_watch_cb, GINT_TO_POINTER (id));   
  
	if ((cbc = gj_connection_get_cb_connect (c)) != NULL) {
		(cbc) (c);
	}
}

gboolean gj_connection_disconnect (GjConnection c)
{
	GTcpSocket *ts = NULL;
	GjConnectionCallbackDisconnect cb = NULL; 
    
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}      

	if ((ts = gj_connection_get_tcpsocket (c))) {
		gnet_tcp_socket_delete (ts);
	}

	g_message ("%s: connection:0x%.8x, disconnected.", __FUNCTION__, (gint)c);
	gj_connection_set_connected (c, FALSE);

	if (c->uptime) {
		g_timer_stop (c->uptime);
		g_timer_destroy (c->uptime);
		c->uptime = NULL;
	}

	if ((cb = gj_connection_get_cb_disconnect (c)) != NULL) {
		(cb) (c);  
	}

	return TRUE;
}

gboolean gj_connection_watch_cb (GIOChannel* source, GIOCondition condition, gpointer data)
{
	gint id = 0;
	GjConnection c = NULL;
    
	id = GPOINTER_TO_INT (data);
    
	g_log (NULL, G_LOG_LEVEL_DEBUG,"%s: source:0x%.8x, condition:%d->'%s', data:0x%.8x", 
	       __FUNCTION__, (gint)source, condition, gj_connection_io_condition_translate (condition), (gint)data);

	if ((c = gj_connection_find_by_id (id)) == NULL) {
		g_warning ("%s: could not find connection id:%d", __FUNCTION__, id);
		return FALSE;
	}

	if (condition & (G_IO_ERR | G_IO_NVAL)) {
		g_warning ("%s: connection:0x%.8x, GIOChannel:0x%.8x, GIOCondition:%d, G_IO_ERR | G_IO_NVAL received", 
			   __FUNCTION__, (gint)c, (gint)source, condition);

		g_message ("%s: connection:0x%.8x, connection was disconnected", __FUNCTION__, (gint)c);
		gj_connection_disconnect (c);

		return FALSE;
	} else if (condition & G_IO_IN) {
		gchar *text = NULL;
		GjConnectionCallbackIn cb = NULL;
		
		guint total_bytes = 0;
		
		while (TRUE) {
			GError *error = NULL;
			GIOStatus status = 0;
			
			gchar buffer[1024] = ""; 
			guint bytes = 0;     
			gchar *buffer_terminated = NULL;
			
			status = g_io_channel_read_chars (source, buffer, sizeof (buffer), &bytes, &error);
			total_bytes += bytes; 
			
			if (status == G_IO_STATUS_ERROR) {
				g_warning ("%s: connection:0x%.8x, GIOChannel:0x%.8x, read error:%d->'%s'", 
					   __FUNCTION__, 
					   (gint)c, 
					   (gint)source, 
					   status, 
					   gj_connection_io_status_translate (status));
				
				if (error != NULL) {
					GIOChannelError cherr = g_io_channel_error_from_errno (error->code);
					
					g_message ("%s: connection:0x%.8x, GError:%d->'%s' GIOChannelError:%d->'%s'", 
						   __FUNCTION__, 
						   (gint)c, 
						   error->code, 
						   error->message, 
						   cherr, 
						   gj_connection_io_error_translate (cherr));
				}
				
				/* clean up */
				gj_connection_disconnect (c);
				
				break;
			}
			
			if (total_bytes == 0) {
				g_message ("%s: connection:0x%.8x, 0 bytes read, connection closed", 
					   __FUNCTION__, (gint)c);
				gj_connection_disconnect (c);
				
				return FALSE;
			}
			
			/* if already read ONCE, and this time, 0 was returned, 
			   no more to bytes to read */
			if (bytes == 0 && total_bytes > bytes) {
				break;
			}
			
			g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: connection:0x%.8x, status:%d->'%s'", 
			       __FUNCTION__, (gint)c, status, gj_connection_io_status_translate (status));  
			
			/* concat to string */
			buffer_terminated = g_strndup (buffer, bytes);
			
			if (buffer_terminated != NULL) {
				if (text == NULL) {
					text = g_strdup (buffer_terminated);
				} else {
					text = g_strconcat (text, buffer_terminated, NULL);
				}
				
				g_free (buffer_terminated);
			}
			
			/* if one read and buffer is not full OR 
			   bytes is > 0 and size of buffer is not filled */
			if ((bytes < sizeof (buffer) && total_bytes == bytes) || 
			    (bytes < sizeof (buffer) && bytes > 0)) {
				break;
			}
		}
		
		if (text != NULL) {
			if (g_utf8_validate (text, -1, NULL) == FALSE) {
				g_warning ("%s: text is not valid utf8", __FUNCTION__); 
			}
			
			g_message ("%s: connection:0x%.8x, read total of %d bytes (%d chars): -\n<<<< %s", 
				   __FUNCTION__, (gint)c, (gint)total_bytes, (gint)g_utf8_strlen (text, -1), text);
			
			if ((cb = gj_connection_get_cb_in (c)) != NULL) {
					(cb) (c, text); 
			}
		}
	} else if (condition & G_IO_HUP) {
		/* FIXME: this should be GjConnectionCallbackHup - just havnt had time yet. */
		g_message ("%s: connection:0x%.8x, connection was disconnected", __FUNCTION__, (gint)c);
		gj_connection_disconnect (c);
		
		return FALSE;
	} else {
		g_message ("%s: connection:0x%.8x, received unhandled GIOCondition:%d->'%s'", 
			   __FUNCTION__, (gint)c, condition, gj_connection_io_condition_translate (condition));
	}
	
	/* return FALSE to close watch */
	return TRUE;
}

gboolean gj_connection_write (GjConnection c, const gchar *text)
{
	GTcpSocket *ts = NULL;
	GIOChannel *ch = NULL;
	GIOStatus status = 0;
	GError *error = NULL;

	guint bytes = 0;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}      
    
	if (text == NULL || g_utf8_strlen (text, -1) < 1) {
		g_warning ("%s: connection:0x%.8x, text was NULL or length was < 1", __FUNCTION__, (gint)c);
		return FALSE;
	}

	if ((ts = gj_connection_get_tcpsocket (c)) == NULL) {
		g_warning ("%s: connection:0x%.8x, GTcpSocket was NULL", __FUNCTION__, (gint)c);
		return FALSE;
	}

	if ((ch = gnet_tcp_socket_get_io_channel (ts)) == NULL) {
		g_warning ("%s: connection:0x%.8x, GIOChannel was NULL", __FUNCTION__, (gint)c);
		return FALSE;
	}

	status = g_io_channel_write_chars (ch, text, g_utf8_strlen (text, -1), &bytes, &error);
	g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: connection:0x%.8x, GIOStatus is %d->'%s'", 
	       __FUNCTION__, (gint)c, status, gj_connection_io_status_translate (status));
    
	if (error != NULL) {
		g_warning ("%s: connection:0x%.8x, error:%d->'%s'", __FUNCTION__, (gint)c, error->code, error->message);
		return FALSE;
	}

	g_message ("%s: connection:0x%.8x, wrote %.5d bytes: -\n>>>> %s", __FUNCTION__, (gint)c, bytes, text);
    
	return TRUE;
}

gdouble gj_connection_get_uptime (GjConnection c)
{
	gdouble seconds = 0;
	gulong microseconds = 0;

	if (!c) {
		return 0;
	}

	if (c->uptime == NULL) {
		return 0;
	}

	seconds = g_timer_elapsed (c->uptime, &microseconds);

	return seconds;
}

/*
 * translations 
 */
const gchar *gj_connection_connect_translate_status (GTcpSocketConnectAsyncStatus status)
{
	switch (status) {
	case GTCP_SOCKET_CONNECT_ASYNC_STATUS_OK: return _("Connection succeeded");
	case GTCP_SOCKET_CONNECT_ASYNC_STATUS_INETADDR_ERROR: return _("Error, address lookup failed");
	case GTCP_SOCKET_CONNECT_ASYNC_STATUS_TCP_ERROR: return _("Error, could not connect");
	}

	return _("unknown");
}

const gchar *gj_connection_io_status_translate (GIOStatus status)
{
	switch (status) {
	case G_IO_STATUS_ERROR: return _("An error occurred.");
	case G_IO_STATUS_NORMAL: return _("Success.");
	case G_IO_STATUS_EOF: return _("End of file.");
	case G_IO_STATUS_AGAIN: return _("Resource temporarily unavailable.");
	}

	return "unknown";
}

const gchar *gj_connection_io_error_translate (gint error)
{
	switch (error) {
		/* Derived from errno */
	case G_IO_CHANNEL_ERROR_FBIG: return _("File too large.");
	case G_IO_CHANNEL_ERROR_INVAL: return _("Invalid argument.");
	case G_IO_CHANNEL_ERROR_IO: return _("IO error.");
	case G_IO_CHANNEL_ERROR_ISDIR: return _("File is a directory.");
	case G_IO_CHANNEL_ERROR_NOSPC: return _("No space left on device.");
	case G_IO_CHANNEL_ERROR_NXIO: return _("No such device or address.");
	case G_IO_CHANNEL_ERROR_OVERFLOW: return _("Value too large for defined datatype.");
	case G_IO_CHANNEL_ERROR_PIPE: return _("Broken pipe.");
		/* Other */
	case G_IO_CHANNEL_ERROR_FAILED: return _("Some other error.");
	}

	return _("unknown");
}

const gchar *gj_connection_io_condition_translate (gint condition)
{
	switch (condition) {
	case G_IO_IN: return _("There is data to read.");
	case G_IO_OUT: return _("Data can be written (without blocking).");
	case G_IO_PRI: return _("There is urgent data to read.");
	case G_IO_ERR: return _("Error condition.");
	case G_IO_HUP: return _("Hung up (the connection has been broken, usually for pipes and sockets).");
	case G_IO_NVAL: return _("Invalid request. The file descriptor is not open.");
	}

	return _("unknown");
}

#ifdef __cplusplus
}
#endif
