/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_parser.h"
#include "gj_connection.h"

#include "gj_gtk_debug.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *textview_input;
	GtkWidget *textview_output;

	GtkWidget *button_input;
	GtkWidget *button_output;

	GjConnection c;
  
} GjDebugWindow;


static void on_button_input_clicked (GtkButton *button, GjDebugWindow *window);
static void on_button_output_clicked (GtkButton *button, GjDebugWindow *window);

static void on_destroy (GtkWidget *widget, GjDebugWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjDebugWindow *window);


static GjDebugWindow *current_window = NULL;


gboolean gj_gtk_debug_load (GjConnection c)
{
	GladeXML *xml = NULL;
	GjDebugWindow *window = NULL;

	if (current_window) {
		gtk_window_present (GTK_WINDOW (current_window->window));
		return TRUE;
	}

	if (!c) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	current_window = window = g_new0 (GjDebugWindow, 1);

	window->c = gj_connection_ref (c);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "debug",
				     NULL,
				     "debug", &window->window,
				     "textview_input", &window->textview_input,
				     "textview_output", &window->textview_output,
				     "button_input", &window->button_input,
				     "button_output", &window->button_output,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "debug", "delete_event", on_delete_event,
			      "debug", "destroy", on_destroy,
			      "button_input", "clicked", on_button_input_clicked,
			      "button_output", "clicked", on_button_output_clicked,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (window->window->window), 
				    GDK_DECOR_ALL);

	return TRUE;
}

static void on_button_input_clicked (GtkButton *button, GjDebugWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end; 

	gchar *text = NULL;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_input));
	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	text = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end, TRUE);

	g_message ("on_button_debug_input_clicked: inputing buffer to XML parser: '%s'", text);

	if (g_utf8_validate (text, -1, NULL) == FALSE) {
		g_warning ("%s: text is not valid utf8", __FUNCTION__); 
	}
    
	gj_parser_push_input_from_connection (window->c, text);

	/* clean up */
	g_free (text);
}

static void on_button_output_clicked (GtkButton *button, GjDebugWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end; 

	gchar *text = NULL;
    
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview_output));
	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	text = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end, TRUE);

	g_message ("on_button_debug_input_clicked: outputing buffer to server: '%s'", text);

	gj_parser_push_output_from_text (window->c, text);

	/* clean up */
	g_free (text);
}

static void on_destroy (GtkWidget *widget, GjDebugWindow *window)
{
	current_window = NULL;

	gj_connection_unref (window->c);

	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjDebugWindow *window)
{
	gtk_widget_destroy (window->window);
	return TRUE;
}

#ifdef __cplusplus
}
#endif
