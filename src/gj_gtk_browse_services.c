/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_agents.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"
#include "gj_browse.h"
#include "gj_jid.h"
#include "gj_translate.h"
#include "gj_connection.h"

#include "gj_gtk_browse_services.h"
#include "gj_gtk_register_service.h"
#include "gj_gtk_support.h"


/* columns */
enum {
	COL_BROWSE_IMAGE,
	COL_BROWSE_JID,
	COL_BROWSE_NAME,
	COL_BROWSE_CATEGORY,
	COL_BROWSE_TYPE,
	COL_BROWSE_CAN_REGISTER,
	COL_BROWSE_CAN_SEARCH,
	COL_BROWSE_VISIBLE,
	COL_BROWSE_ARE_WE_REGISTERED,
	COL_BROWSE_COUNT
};


typedef struct  {
	GtkWidget *window;
  
	GtkWidget *entry_server;
	GtkWidget *button_server;
	GtkWidget *button_refresh;
    
	GtkWidget *treeview;

	GtkWidget *button_register;
	GtkWidget *button_unregister;
	GtkWidget *button_search;

	GtkWidget *label_waiting;
	GtkWidget *progressbar_waiting;

/* 	GtkWidget *button_close; */
	GtkWidget *button_help;

	/* members */
	GjConnection c;

	gchar *jid; /* jid that the register/unregister was done to */ 

	GtkWidget *progress_dialog;
	gboolean progress_cancelled;

} GjBrowseServicesDialog;


static gboolean gj_gtk_bs_setup (GjBrowseServicesDialog *dialog);

/* void gj_gtk_bs_timer_cb (gboolean timeout_expired, void *userdata); */

static void gj_gtk_bs_enable_controls (GjBrowseServicesDialog *dialog, gboolean enabled);

/*
 * model functions 
 */
static gboolean gj_gtk_bs_model_setup (GjBrowseServicesDialog *dialog);
static gboolean gj_gtk_bs_model_populate_columns (GjBrowseServicesDialog *dialog);

static void gj_gtk_bs_model_cell_image_func (GtkTreeViewColumn *tree_column,
					     GtkCellRenderer *cell,
					     GtkTreeModel *model,
					     GtkTreeIter *iter,
					     GjBrowseServicesDialog *dialog);

static void gj_gtk_bs_model_cell_text_func (GtkTreeViewColumn *tree_column,
					    GtkCellRenderer *cell,
					    GtkTreeModel *model,
					    GtkTreeIter *iter,
					    GjBrowseServicesDialog *dialog);

static gboolean gj_gtk_bs_model_get_selection (GjBrowseServicesDialog *dialog, gchar **jid);

static gboolean gj_gtk_bs_set_agent_registered (GjBrowseServicesDialog *dialog, gchar *jid, gboolean registered);

/* internal callbacks */
static void gj_gtk_bs_request_cb (const gchar *server, 
				  const GList *list, 
				  gint error_code, 
				  const gchar *error_reason, 
				  GjBrowseServicesDialog *dialog);

static void gj_gtk_bs_unregister_cb (GjInfoquery iq, GjInfoquery iq_related, GjBrowseServicesDialog *dialog);
static void gj_gtk_bs_refresh_progress_cb (GtkDialog *widget, gint response, GjBrowseServicesDialog *dialog);


static void gj_gtk_bs_server_cb (GtkMenuItem *menuitem, GjBrowseServicesDialog *dialog);
static void gj_gtk_bs_server_align_cb (GtkMenu *menu, 
				       gint *x, 
				       gint *y, 
				       gboolean *push_in, 
				       GjBrowseServicesDialog *dialog);

/* 
 * gui callbacks 
 */
static void on_button_server_clicked (GtkButton *button, GjBrowseServicesDialog *dialog);
static void on_treeview_selection_changed (GtkTreeSelection *treeselection, GjBrowseServicesDialog *dialog);

static void on_button_refresh_clicked (GtkButton *button, GjBrowseServicesDialog *dialog);

static void on_button_search_clicked (GtkButton *button, GjBrowseServicesDialog *dialog);
static void on_button_register_clicked (GtkButton *button, GjBrowseServicesDialog *dialog);
static void on_button_unregister_clicked (GtkButton *button, GjBrowseServicesDialog *dialog);

static void on_destroy (GtkWidget *button, GjBrowseServicesDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjBrowseServicesDialog *dialog);


static GjBrowseServicesDialog *current_dialog = NULL;


/*
 * window functions
 */
gboolean gj_gtk_bs_load (GjConnection c)
{
	GladeXML *xml = NULL;
	GjBrowseServicesDialog *dialog = NULL;

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (!c) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjBrowseServicesDialog, 1);

	dialog->c = gj_connection_ref (c);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "browse_services",
				     NULL,
				     "browse_services", &dialog->window,
				     "entry_server", &dialog->entry_server,
				     "button_server", &dialog->button_server,
				     "treeview", &dialog->treeview,
				     "button_refresh", &dialog->button_refresh,
				     "button_search", &dialog->button_search,
				     "button_register", &dialog->button_register,
				     "button_unregister", &dialog->button_unregister,
/* 				     "button_close", &dialog->button_close, */
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "browse_services", "response", on_response,
			      "browse_services", "destroy", on_destroy,
			      "button_server", "clicked", on_button_server_clicked,
			      "button_refresh", "clicked", on_button_refresh_clicked,
			      "button_search", "clicked", on_button_search_clicked,
			      "button_register", "clicked", on_button_register_clicked,
			      "button_unregister", "clicked", on_button_unregister_clicked,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU | GDK_DECOR_RESIZEH);
  
	/* setup controls */
	gj_gtk_bs_setup (dialog);

	return TRUE;
}

static gboolean gj_gtk_bs_setup (GjBrowseServicesDialog *dialog)
{
	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}
 
	gtk_entry_set_text (GTK_ENTRY (dialog->entry_server), gj_connection_get_host (dialog->c));

	/* set up list */
	gj_gtk_bs_model_setup (dialog);

	return TRUE;
}

static void gj_gtk_bs_enable_controls (GjBrowseServicesDialog *dialog, gboolean enabled)
{
	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->treeview), enabled);

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->entry_server), enabled);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_server), enabled);

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_refresh), enabled);

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_search), enabled);

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_register), enabled);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_unregister), enabled);
  
	return;
}

/*
 * model functions 
 */
static gboolean gj_gtk_bs_model_setup (GjBrowseServicesDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	/* create store */
	store = gtk_list_store_new (COL_BROWSE_COUNT,
				    GDK_TYPE_PIXBUF,  /* image - registered? */
				    G_TYPE_STRING,    /* jid */
				    G_TYPE_STRING,    /* name */
				    G_TYPE_INT,       /* category */
				    G_TYPE_INT,       /* type */
				    G_TYPE_BOOLEAN,   /* can register */
				    G_TYPE_BOOLEAN,   /* can search */
				    G_TYPE_BOOLEAN,   /* visible */
				    G_TYPE_BOOLEAN);  /* are we registered ? */

	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection), "changed", G_CALLBACK (on_treeview_selection_changed), dialog);

	/* columns */
	gj_gtk_bs_model_populate_columns (dialog);

	/* sort */
  	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model), COL_BROWSE_NAME, GTK_SORT_ASCENDING);

	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE);
	gtk_tree_view_set_search_column (view, COL_BROWSE_NAME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view, FALSE);
	gtk_tree_view_set_headers_clickable (view, TRUE);

	/* clean up */
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

static gboolean gj_gtk_bs_model_populate_columns (GjBrowseServicesDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeViewColumn *column = NULL; 
	GtkCellRenderer *renderer = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);

	/* cell renderer's */
	column = gtk_tree_view_column_new ();

	/* COL_ROSTER_IMAGE */  
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);

	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 (GtkTreeCellDataFunc) gj_gtk_bs_model_cell_image_func,
						 dialog,
						 NULL);

	/* COL_ROSTER_TEXT */
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);

	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 (GtkTreeCellDataFunc) gj_gtk_bs_model_cell_text_func,
						 dialog,
						 NULL);

	/* insert column */
	gtk_tree_view_column_set_title (column, _("Services"));
	gtk_tree_view_column_set_sort_column_id (column, COL_BROWSE_NAME);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);

	return TRUE;
}

static void gj_gtk_bs_model_cell_image_func (GtkTreeViewColumn *tree_column,
					     GtkCellRenderer *cell,
					     GtkTreeModel *model,
					     GtkTreeIter *iter,
					     GjBrowseServicesDialog *dialog)
{
	gboolean registered = FALSE;
	GdkPixbuf *pb = NULL;

	/* is group ? */
	gtk_tree_model_get (model, iter, COL_BROWSE_ARE_WE_REGISTERED, &registered, -1);
	
	if (registered) {
		pb = gj_gtk_pixbuf_new_from_stock (GTK_STOCK_YES);
	}

	g_object_set (cell, 
		      "pixbuf", pb,
		      "visible", TRUE, 
		      NULL);
  
	/* clean up */
	if (pb) {
		g_object_unref (pb); 
	}
}

static void gj_gtk_bs_model_cell_text_func (GtkTreeViewColumn *tree_column,
					    GtkCellRenderer *cell,
					    GtkTreeModel *model,
					    GtkTreeIter *iter,
					    GjBrowseServicesDialog *dialog)
{
	gboolean registered = FALSE;
	gchar *text = NULL;

	/* is group ? */
	gtk_tree_model_get (model, iter, 
			    COL_BROWSE_ARE_WE_REGISTERED, &registered,
			    COL_BROWSE_NAME, &text, 
			    -1);
   
	g_object_set (cell,
		      "attributes", NULL,
		      "weight", registered ? PANGO_WEIGHT_BOLD : PANGO_WEIGHT_NORMAL,
		      "text", text,
		      NULL);

	g_free (text);
}


static gboolean gj_gtk_bs_model_get_selection (GjBrowseServicesDialog *dialog, gchar **jid)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;
  
	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid pointer passed was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	if (gtk_tree_selection_get_selected (selection, &model, &iter) == FALSE) {
		return FALSE;
	}
  
	gtk_tree_model_get (model, &iter, COL_BROWSE_JID, jid, -1);     
	return TRUE;
}

/* sets */
static gboolean gj_gtk_bs_set_agent_registered (GjBrowseServicesDialog *dialog, gchar *jid, gboolean registered)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gboolean valid = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	valid = gtk_tree_model_get_iter_first (model, &iter);
  
	for (;valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gchar *text = NULL;

		gtk_tree_model_get (model, &iter, COL_BROWSE_JID, &text, -1);     

		if (text && strcmp (text, jid) == 0) { 
			gtk_list_store_set (store, &iter, COL_BROWSE_ARE_WE_REGISTERED, registered, -1);    
		}
      
		g_free (text);
	}

	return TRUE;
} 

/* internal callbacks */
static void gj_gtk_bs_request_cb (const gchar *server, 
				  const GList *list, 
				  gint error_code, 
				  const gchar *error_reason, 
				  GjBrowseServicesDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gint i = 0;

	if (list == NULL && error_code == 0 && error_reason == NULL) {
		g_warning ("%s: list was NULL, error code was 0 and error message was NULL", __FUNCTION__);
		return;
	}  

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	if (!dialog->window) {
		g_message ("%s: window is no longer pertinant, not honoring response.", __FUNCTION__);
		return;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* clear list if already pressed */
	gtk_list_store_clear (store);

	for (i=0;i<g_list_length ((GList*)list);i++) {
		GjBrowseItem bi = NULL;
		GjJID j = NULL;

		gboolean can_register = FALSE;
		gboolean can_search = FALSE;
		gboolean registered = FALSE;

		bi = (GjBrowseItem) g_list_nth_data ((GList*)list, i);
		if (bi == NULL) {
			continue;
		}

		j = gj_browse_item_get_jid (bi);
      
		registered = gj_rosters_is_agent_registered (gj_jid_get_full (j));

		can_register = gj_browse_item_include_namespace (bi, "jabber:iq:register");
		can_search = gj_browse_item_include_namespace (bi, "jabber:iq:search");

		/* add */ 
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,    
				    COL_BROWSE_JID, gj_jid_get_full (j),
				    COL_BROWSE_NAME, gj_browse_item_get_name (bi)?gj_browse_item_get_name (bi):"-",
				    COL_BROWSE_CATEGORY, gj_browse_item_get_category (bi),
				    COL_BROWSE_TYPE, gj_browse_item_get_type (bi),
				    COL_BROWSE_CAN_REGISTER, can_register,
				    COL_BROWSE_CAN_SEARCH, can_search,
				    COL_BROWSE_VISIBLE, TRUE,
				    COL_BROWSE_ARE_WE_REGISTERED, registered,
				    -1);

		/* info */
		g_message ("%s: item %d, jid:'%s', name:'%s', category:'%s', type:'%s', "
			   "can register:'%s', can search:'%s' are registered: '%s'",
			   __FUNCTION__,
			   i,
			   gj_jid_get_full (j),
			   gj_browse_item_get_name (bi),
			   gj_translate_browse_category (gj_browse_item_get_category (bi)),
			   gj_translate_browse_type (gj_browse_item_get_type (bi)),
			   can_register ? _("yes") :_("no"),
			   can_search ? _("yes") :_("no"),
			   registered? _("yes") :_("no"));
	}

	/* hide controls */
	gj_gtk_bs_enable_controls (dialog, TRUE);

	/* stop progress dialog */
	if (dialog->progress_dialog) {
		gtk_widget_destroy (dialog->progress_dialog);
		dialog->progress_dialog = NULL;
	}

	/* set buttons disabled */
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_search), FALSE);

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_register), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_unregister), FALSE);

	/* handle error */
	if (error_code != 0 || error_reason != NULL) {
		gj_gtk_show_error (_("Failed to Browse Services."),
				   error_code,
				   error_reason);
	}
}

static void gj_gtk_bs_unregister_cb (GjInfoquery iq, GjInfoquery iq_related, GjBrowseServicesDialog *dialog)
{
	xmlNodePtr node = NULL;
	xmlChar *type = NULL;

	if (!iq_related)
		return;
  
	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	node = gj_iq_get_xml (iq_related);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		gchar *detail = NULL;

		xmlChar *error_code = NULL;
		xmlChar *error_reason = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		error_code = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		error_reason = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		gj_gtk_show_error (_("Failed to Unregister Service."),
				   atoi ((gchar*)error_code),
				   (gchar*)error_reason);

		/* clean up */
		g_free (detail);
		return;
	}

	/* update table of information */
	gj_gtk_bs_set_agent_registered (dialog, dialog->jid, FALSE);
}

static void gj_gtk_bs_refresh_progress_cb (GtkDialog *widget, gint response, GjBrowseServicesDialog *dialog)
{
	if (response == GTK_RESPONSE_CANCEL || response == GTK_RESPONSE_DELETE_EVENT) {
		dialog->progress_cancelled = TRUE;
	}

	if (dialog->progress_cancelled) {
		/* hide controls */
		gj_gtk_bs_enable_controls (dialog, TRUE);

		/* set buttons disabled */
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_register), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_unregister), FALSE);
	}

	gtk_widget_destroy (dialog->progress_dialog);
	dialog->progress_dialog = NULL;
}

static void gj_gtk_bs_server_cb (GtkMenuItem *menuitem, GjBrowseServicesDialog *dialog)
{
	const gchar **server_listing = NULL;
	const gchar *server = NULL;

	gpointer pid;
	gint id;

	server_listing = gnome_jabber_get_server_list ();

	if (!server_listing) {
		return;
	}

	pid = g_object_get_data (G_OBJECT (menuitem), "id");
	id = GPOINTER_TO_INT (pid);
  
	server = server_listing[id];

	if (server) {
		gtk_entry_set_text (GTK_ENTRY (dialog->entry_server), server); 
	}
}

static void gj_gtk_bs_server_align_cb (GtkMenu *menu, 
				       gint *x, 
				       gint *y, 
				       gboolean *push_in, 
				       GjBrowseServicesDialog *dialog)
{
	GtkWidget *button = NULL;
	GtkRequisition req;
	GdkScreen *screen = NULL;
	gint width = 0;
	gint height = 0;
	gint screen_height = 0;
  
	button = dialog->button_server;
	
	gtk_widget_size_request (GTK_WIDGET (menu), &req);
  
	gdk_window_get_origin (GTK_BUTTON (button)->event_window, x, y);
	gdk_drawable_get_size (GTK_BUTTON (button)->event_window, &width, &height);
  
	*x -= req.width - width;
	*y += height;
  
	screen = gtk_widget_get_screen (GTK_WIDGET (menu));
  
	/* Clamp to screen size. */
	screen_height = gdk_screen_get_height (screen) - *y;	

	if (req.height > screen_height) {
		/* It doesn't fit, so we see if we have the minimum space needed. */
		if (req.height > screen_height && *y - height > screen_height) {
			/* Put the menu above the button instead. */
			screen_height = *y - height;
			*y -= (req.height + height);

			if (*y < 0) {
				*y = 0;
			}
		}
	}
}

/*
 * GUI events
 */
static void on_button_server_clicked (GtkButton *button, GjBrowseServicesDialog *dialog)
{
	GtkWidget *menu = NULL;

	GList *servers = NULL;
	const gchar **server_listing = NULL;
	const gchar *str = NULL;
	gint i = 0;

	server_listing = gnome_jabber_get_server_list ();

	menu = gtk_menu_new ();
  
	/* set a limit of a 1000 */
	while ((str = server_listing[i++]) != NULL && i < 1000) {
		GtkWidget *item = NULL;
		
		if (!str) {
			continue;
		}
		
		item = gtk_menu_item_new_with_label (str);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item); 
		
		g_object_set_data (G_OBJECT (item), "id", GINT_TO_POINTER (i - 1));
		
		g_signal_connect ((gpointer) item, "activate", 
				  G_CALLBACK (gj_gtk_bs_server_cb), dialog);
	}

	gtk_widget_show_all (menu);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, 
			(GtkMenuPositionFunc)gj_gtk_bs_server_align_cb,
			dialog, 1, gtk_get_current_event_time ());
  

	/* cleanup */
	g_list_free (servers);
}

static void on_treeview_selection_changed (GtkTreeSelection *treeselection, GjBrowseServicesDialog *dialog)
{
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;

	gboolean can_register = FALSE;
	gboolean can_search = FALSE;
	gboolean registered = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	if (gtk_tree_selection_get_selected (treeselection, &model, &iter) == FALSE) {
		g_message ("%s: there is nothing selected", __FUNCTION__);
		return;
	}

	if (GTK_TREE_MODEL (model) == NULL) {
		g_warning ("%s: model was NULL", __FUNCTION__);
		return;
	}

	gtk_tree_model_get (model, &iter,
			    COL_BROWSE_ARE_WE_REGISTERED, &registered,
			    COL_BROWSE_CAN_REGISTER, &can_register,
			    COL_BROWSE_CAN_SEARCH, &can_search,
			    -1);

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_register), (can_register && !registered));
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_unregister), (can_register && registered));
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_search), can_search);
}

static void on_button_refresh_clicked (GtkButton *button, GjBrowseServicesDialog *dialog)
{
	G_CONST_RETURN gchar *server = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return;
	}

	server = gtk_entry_get_text (GTK_ENTRY (dialog->entry_server));

	gj_browse_request (dialog->c, 
			   server, 
			   (GjBrowseCallback)gj_gtk_bs_request_cb, 
			   dialog);

	dialog->progress_cancelled = FALSE;
	dialog->progress_dialog = gj_gtk_hig_progress_new (GTK_WINDOW (dialog->window),
							   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							   (GCallback)gj_gtk_bs_refresh_progress_cb,
							   dialog,
							   _("Progress"),
							   _("Requesting Services"), 
							   _("Server is \"%s\""), 
							   server); 

	gtk_widget_show_all (dialog->progress_dialog);

	gj_gtk_bs_enable_controls (dialog, FALSE);
}


static void on_button_search_clicked (GtkButton *button, GjBrowseServicesDialog *dialog)
{
	gtk_widget_destroy (dialog->window);
}

static void on_button_register_clicked (GtkButton *button, GjBrowseServicesDialog *dialog)
{
	gchar *jid = NULL;

	if (gj_gtk_bs_model_get_selection (dialog, &jid) == TRUE && jid != NULL) {
		gj_gtk_rs_load (dialog->c, jid);
	}

	gtk_widget_destroy (dialog->window);
  
	/* clean up */
	g_free (jid);
}

static void on_button_unregister_clicked (GtkButton *button, GjBrowseServicesDialog *dialog)
{
	gchar *jid = NULL;

	if (gj_gtk_bs_model_get_selection (dialog, &jid) == TRUE && jid != NULL) {
		gj_gtk_rs_remove (dialog->c, jid);
	}

	gtk_widget_destroy (dialog->window);
    
	/* clean up */
	g_free (jid);
}

static void on_destroy (GtkWidget *widget, GjBrowseServicesDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);

	g_free (dialog->jid);

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjBrowseServicesDialog *dialog)
{
	if (response < 1) {
		gtk_widget_destroy (dialog->window);
	}
}


#ifdef __cplusplus
}
#endif
