/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif


#include <string.h>

#include <glib.h>

#include "gj_jid.h"


struct t_jid  {
	GjJIDType type;
  
	gchar *full;

	gchar *agent;
  
	gchar *no_resource;
	gchar *resource;

	guint ref_count;
};


static void gj_jid_free (GjJID jid);


static void gj_jid_free (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return;
	}

	if (jid->full != NULL) {
		g_free (jid->full);
		jid->full = NULL;
	}

	if (jid->agent != NULL) {
		g_free (jid->agent);
		jid->agent = NULL;
	}

	if (jid->no_resource != NULL) {
		g_free (jid->no_resource);
		jid->no_resource = NULL;
	}

	g_free (jid);
}

GjJID gj_jid_new (const gchar *s)
{
	GjJID jid = NULL;
	GjJIDType type = GjJIDTypeNone;
	gchar *ch = NULL;
	
	if (s == NULL) {
		g_warning ("%s: jid string was NULL", __FUNCTION__);
		return NULL;
	}

	if (gj_jid_string_is_valid_jid_contact (s)) {
		type = GjJIDTypeContact;
	} else if (gj_jid_string_is_valid_jid_agent (s)) {
		type = GjJIDTypeAgent;
	}

	if (type == GjJIDTypeNone) {
		g_warning ("%s: unknown type of JID", __FUNCTION__);
		return NULL;
	}

	jid = g_new (struct t_jid,1);

	jid->ref_count = 1;

	jid->type = type;
	jid->full = g_strdup (s);
	
	ch = strchr (jid->full, '/');
	if (ch) {
		jid->resource = ch + 1;
		jid->no_resource = g_strndup (jid->full, ch - jid->full);
	} else {
		jid->resource = NULL;
		jid->no_resource = g_strdup (jid->full);
	}

	ch = strchr (jid->no_resource, '@');
	if (ch) {
		jid->agent = g_strdup (ch + 1); 
	} else {
		jid->agent = NULL; 
	}

	return jid;
}

const gchar *gj_jid_get_full (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	return jid->full;
}

const gchar *gj_jid_get_without_resource (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	if (jid->no_resource == NULL) {
		return jid->full;
	}

	return jid->no_resource;
}

const gchar *gj_jid_get_resource (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	if (jid->resource == NULL) {
		return NULL;
	}

	return jid->resource;
}

const gchar *gj_jid_get_agent (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	if (jid->agent == NULL) {
		return NULL;
	}

	return jid->agent;
}

GjJIDType gj_jid_get_type (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return GjJIDTypeNone;
	}

	return jid->type;
}


gchar *gj_jid_get_part_name_new (GjJID jid)
{
	gchar *ch = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	for (ch=jid->full; *ch; ++ch) {
		if (*ch == '@') return g_strndup (jid->full, ch - jid->full);
	}

	return g_strdup (jid->full); 
}

/* sets */
void gj_jid_set_resource (GjJID jid, const gchar *resource)
{
	gchar *ch = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return;
	}

	if (resource == NULL) {
		g_warning ("%s: resource was NULL", __FUNCTION__);
		return;
	}  

	/* free */
	g_free (jid->full);
  
	/* set with new resource */
	jid->full = g_strconcat (jid->no_resource, "/", resource, NULL);

	ch = strchr (jid->full, '/');
	if (ch) {
		jid->resource = ch + 1;
	} else {
		jid->resource = NULL;
	}
}


GjJID gj_jid_ref (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return NULL;
	}

	jid->ref_count++;
	return jid;
}
	
void gj_jid_unref (GjJID jid)
{
	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return;
	}

	jid->ref_count--;

	if (jid->ref_count <= 0) {
		gj_jid_free (jid);
	}
}

gboolean gj_jid_equals (GjJID jid_a, GjJID jid_b)
{
	if (jid_a == NULL) {
		g_warning ("%s: jid_a was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid_b == NULL) {
		g_warning ("%s: jid_b was NULL", __FUNCTION__);
		return FALSE;
	}
	
	if (g_strcasecmp (jid_a->full, jid_b->full) == 0) {
		return TRUE;
	}

	return FALSE;
}

gboolean gj_jid_equals_without_resource (GjJID jid_a, GjJID jid_b)
{
	const gchar *a = NULL;
	const gchar *b = NULL;

	if (jid_a == NULL) {
		g_warning ("%s: jid_a was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid_b == NULL) {
		g_warning ("%s: jid_b was NULL", __FUNCTION__);
		return FALSE;
	}
	
	a = gj_jid_get_without_resource (jid_a);
	b = gj_jid_get_without_resource (jid_b);

	if (a == NULL || b == NULL) {
		return FALSE;
	}
  
	if (g_strcasecmp (a, b) == 0) {
		return TRUE;	
	}

	return FALSE;
}

gboolean gj_jid_string_is_valid_jid (const gchar *s)
{
	if (gj_jid_string_is_valid_jid_contact (s) == TRUE || 
	    gj_jid_string_is_valid_jid_agent (s) == TRUE) {
		return TRUE;
	}
  
	return FALSE;
}

gboolean gj_jid_string_is_valid_jid_contact (const gchar *s)
{
	const gchar *at = NULL;
	const gchar *dot = NULL;
	gint len = 0;
	
	if (!s || strcmp (s, "") == 0) {
		return FALSE;
	}

	len = strlen (s);

	at = strchr (s, '@');
	if (!at || at == s || at == s + len - 1) {
		return FALSE;
	}
	
	dot = strchr (at, '.');
	if (dot == at + 1 || dot == s + len - 1 || dot == s + len - 2) {
		return FALSE;
	}

	dot = strrchr (s, '.');
	if (dot == s + len - 1 || dot == s + len - 2) {
		return FALSE;
	}

	return TRUE;
}

gboolean gj_jid_string_is_valid_jid_agent (const gchar *s)
{
	const gchar *at = NULL;
	const gchar *dot = NULL;
	gint len = 0;
	
	if (!s || strcmp (s, "") == 0) {
		return FALSE;
	}

	len = strlen (s);
  
	at = strchr (s, '@');
	if (at) {
		return FALSE;
	}

	dot = strchr (s, '.');
	if (!dot || dot == s || dot == s + len - 1) {
		return FALSE;
	}
  
	return TRUE;
}

#ifdef __cplusplus
}
#endif
