/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include "gj_event.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_file_transfer.h"
#include "gj_translate.h"
#include "gj_support.h"
#include "gj_jid.h"
#include "gj_group_chat.h"


struct t_event {
	guint id;
	GjJID jid;

	gboolean in_event; /* event has been looked at but not free'd, eg. in chat window already */
    
	GjEventType type;
	GjEventCallback cb;
    
	gint ri_id;
    
	time_t time;
    
	void *data;
};


static void gj_event_destroy (GjEvent ev);

static void gj_event_cb_presence_from (GjPresence pres);
static void gj_event_cb_message_from (GjMessage m);
static void gj_event_cb_file_transfer_from (GjFileTransfer ft);

static gboolean gj_event_new_from_presence (GjPresence pres);
static gboolean gj_event_new_from_message (GjMessage m);
static gboolean gj_event_new_from_file_transfer (GjFileTransfer ft);


static GList *event_list = NULL;
static GjEventCallback event_generic_cb = NULL;
static gpointer event_user_data = NULL;


gboolean gj_event_init (GjEventCallback cb, gpointer user_data)
{
	static gboolean initiated = FALSE;
  
	if (initiated == TRUE) {
		return TRUE;
	}

	event_generic_cb = cb;
	event_user_data = user_data;

	if (gj_message_set_cb_from (gj_event_cb_message_from) == FALSE || 
	    gj_presence_set_cb_from (gj_event_cb_presence_from) == FALSE) {
		return FALSE;
	}

	initiated = TRUE;

	return TRUE;
}

void gj_event_term ()
{
	event_generic_cb = NULL;
}

void gj_event_stats ()
{
	g_message ("%s: list size:%d", __FUNCTION__, g_list_length (event_list));
}

GjEvent gj_event_new (GjRoster r, GjJID j, GjEventType type, void *data)
{
	GjEvent ev = NULL;
	GjRosterItem ri = NULL;

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return NULL;
	}

	if (j == NULL) {
		g_warning ("%s: JID was NULL", __FUNCTION__);
		return NULL;
	}

	if (data == NULL) {
		g_warning ("%s: data is NULL", __FUNCTION__);
		return NULL;
	}

	ri = gj_group_chat_find (j);

	if (!ri) {
		ri = gj_roster_find_item_by_jid (r, j); 
	}

	if (ri == NULL) {
		g_message ("%s: no roster items found, creating temporary roster item for jid:'%s'...", 
			   __FUNCTION__, gj_jid_get_full (j));

		if ((ri = gj_roster_item_new_unknown_by_jid (gj_jid_get_full (j))) == NULL) {
			g_warning ("%s: couldn't add temporary roster item for jid:'%s'", __FUNCTION__, gj_jid_get_full (j));
			return NULL;
		}

		/* add to roster */
		gj_roster_add_item (r, ri);
	}

	if (type < 1 || type >= GjEventTypeEnd) {
		g_warning ("%s: event type was out of range", __FUNCTION__);
		return NULL;
	}

	gj_event_init (NULL, NULL);

	if ((ev = g_new0 (struct t_event,1))) {
		ev->id = gj_get_unique_id ();

		ev->jid = gj_jid_ref (j);

		ev->type = type;
		ev->data = data;

		ev->in_event = FALSE;
		ev->cb = NULL;
		ev->ri_id = gj_roster_item_get_id (ri);

		ev->time = time (0);

		event_list = g_list_append (event_list, ev);
	}

	return ev;
}

gboolean gj_event_new_from_presence (GjPresence pres)
{
	GjConnection c = NULL;
	GjEvent ev = NULL;
	GjEventCallback cb = NULL;
	GjRoster r = NULL;
	GjRosterItem ri = NULL;

	if ((cb = gj_event_get_cb ()) == NULL) {
		g_warning ("%s: no response callback to post new event to", __FUNCTION__);
		return FALSE;
	}

	c = gj_presence_get_connection (pres);
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	r = gj_roster_find (c);
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	ev = gj_event_new (r, gj_presence_get_from_jid (pres), GjEventTypePresence, (void*)gj_presence_ref (pres));
	if (!ev) {
		gj_presence_unref (pres);

		g_warning ("%s: could not create new event", __FUNCTION__);
		return FALSE;
	}

	/* set roster presence */
	ri = gj_rosters_find_item_by_id (ev->ri_id);
	if (ri) {
		gj_roster_item_set_presence (ri, pres);
	}

	/* FIXME: we need to add this contact to the roster somewhere.. */

	/* call callback to post new event */
	(cb) (ev->id, event_user_data);
	return TRUE;
}

gboolean gj_event_new_from_message (GjMessage m)
{
	GjConnection c = NULL;
	GjEvent ev = NULL;
	GjEventCallback cb = NULL;
	GjRoster r = NULL;

	if ((cb = gj_event_get_cb ()) == NULL) {
		g_warning ("%s: no response callback to post new event to", __FUNCTION__);
		return FALSE;
	}

	c = gj_message_get_connection (m);
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	r = gj_roster_find (c);
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	ev = gj_event_new (r, gj_message_get_from_jid (m), GjEventTypeMessage, (void*)gj_message_ref (m));

	if (!ev) {
		gj_message_unref (m);
      
		g_warning ("%s: could not create new event", __FUNCTION__);
		return FALSE;
	}

	/* FIXME: we need to add this contact to the roster somewhere.. */

	/* call callback to post new event */
	(cb) (ev->id, event_user_data);
	return TRUE;
}

gboolean gj_event_new_from_file_transfer (GjFileTransfer ft)
{
	GjConnection c = NULL;
	GjEvent ev = NULL;
	GjEventCallback cb = NULL;
	GjRoster r = NULL;

	if ((cb = gj_event_get_cb ()) == NULL) {
		g_warning ("%s: no response callback to post new event to", __FUNCTION__);
		return FALSE;
	}

	c = gj_file_transfer_get_connection (ft);
	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	r = gj_roster_find (c);
	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	ev = gj_event_new (r, gj_file_transfer_get_from_jid (ft), GjEventTypeFileTransfer, (void*)gj_file_transfer_ref (ft));

	if (!ev) {
		gj_file_transfer_unref (ft);
      
		g_warning ("%s: could not create new event", __FUNCTION__);
		return FALSE;
	}

	/* FIXME: we need to add this contact to the roster somewhere.. */

	/* call callback to post new event */
	(cb) (ev->id, event_user_data);
	return TRUE;
}


gboolean gj_event_free (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}

	/* free mem */
	event_list = g_list_remove (event_list, ev);

	/* destroy members */
	gj_event_destroy (ev);

	/* free structure */
	g_free (ev);

	return TRUE;
}

gboolean gj_event_free_by_id (gint event_id)
{
	gint index = 0;

	if (event_id < 0) {
		g_warning ("%s: event_id:%d was < 0", __FUNCTION__, event_id);
		return FALSE;
	}

	/* loop through all events on contacts event list to look for unique event_id */
	for (index=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);
		
		if (ev == NULL) {
			g_warning ("%s: getting nth item (%d) in roster item event list was NULL", __FUNCTION__, index);
			continue;
		}
		
		if (ev->id == event_id) {
			return gj_event_free (ev);
		}   
	}
		
	g_warning ("%s: could not find event by id:%d", __FUNCTION__, event_id);
	return FALSE;
}

static void gj_event_destroy (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return;
	}

	ev->id = 0;

	if (ev->jid != NULL) {
		gj_jid_unref (ev->jid);
		ev->jid = NULL;
	}

	/*
	 * should we be freeing these types here or should the module clear them
	 * up when it is terminated, e.g. infoqueryTerm ();
	 */
	if (ev->data != NULL) {
		switch (ev->type) {
		case GjEventTypeMessage: {
			GjMessage m = (GjMessage) ev->data;
			gj_message_unref (m);
			break;
		}
			
		case GjEventTypePresence: {
			GjPresence pres = (GjPresence) ev->data;
			gj_presence_unref (pres);
			break;
		}
	
		default: {
			break; 
		}
		}
	}

	ev->type = GjEventTypeEnd;
	ev->cb = NULL;

	ev->ri_id = -1;
}

/*
 * response cb for inbound events 
 */
gboolean gj_event_set_cb (GjEventCallback cb)
{
	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	event_generic_cb = cb;

	return TRUE;
}

GjEventCallback gj_event_get_cb ()
{
	return event_generic_cb;
}


/*
 * utils
 */
GjEvent gj_event_nth (gint index)
{
	GjEvent ev = NULL;

	if (event_list == NULL) {
		g_warning ("%s: event list was NULL", __FUNCTION__);
		return NULL;
	}

	if (index < 0 || index > g_list_length (event_list)) {
		g_warning ("%s: index:%d was < 0 || > %d", __FUNCTION__, index, g_list_length (event_list));
		return NULL;
	}

	/* get first item and return event_id */
	ev = (GjEvent)g_list_nth_data (event_list, index);
  
	return ev;
}

GjEvent gj_event_nth_by_roster_item (GjRosterItem ri, gint index)
{
	gint i = 0;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	/* do not warning this - this is perfectly normal */
	if (event_list == NULL || g_list_length (event_list) < 1) {
		return NULL;
	}

	if (index < 0 || index > g_list_length (event_list)) {
		g_warning ("%s: index:%d was < 0 || > %d", __FUNCTION__, index, g_list_length (event_list));
		return NULL;
	}

	for (i=0;i<g_list_length (event_list);i++) {
		GjRosterItem ri_tmp = NULL;
		GjEvent ev = g_list_nth_data (event_list, i);

		if (ev == NULL) {
			continue;
		}

		if ((ri_tmp = gj_rosters_find_item_by_id (gj_event_get_ri_id (ev))) == NULL) {
			continue; 
		}

		if (gj_roster_item_get_id (ri_tmp) == gj_roster_item_get_id (ri) && i == index) {
			return ev; 
		}
	}

	return NULL;
}

gint gj_event_next (GjRosterItem ri)
{
	gint index = 0;
	gint id = -1;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return -1;
	}
  
	id = gj_roster_item_get_id (ri);

	/* do not warning this - this is perfectly normal */
	if (event_list == NULL || g_list_length (event_list) < 1) {
		return -1;
	}

	/* loop through all event_list on contacts event list to look for unique event_id */
	for (index=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);
		
		if (ev == NULL) {
			continue;  
		}
		
		if (ev->ri_id == id) {
			return ev->id;
		}
	}
  
	return -1;
}

gint gj_event_count (GjRosterItem ri)
{
	gint index = 0;
	gint id = -1;
	guint count = 0;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return -1;
	}

	id = gj_roster_item_get_id (ri);

	/* do not warning this - this is perfectly normal */
	if (event_list == NULL) {
		return 0;
	}

	/* loop through all event_list on contacts event list to look for unique event_id */
	for (index=0,count=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);
		if (ev == NULL) { 
			continue;
		}

		if (ev->ri_id == id)  { 
			count++;
		}
	}

	return count;
}

gint gj_event_count_all ()
{
	/* do not warning this - this is perfectly normal */
	if (event_list == NULL) {
		return 0;
	}

	return g_list_length (event_list);
}

GjEvent gj_event_find_by_id (gint id)
{
	gint index = 0;

	if (id < 0) {
		g_warning ("%s: id:%d was < 0", __FUNCTION__, id);
		return NULL;
	}

	/* loop through all event_list on contacts event list to look for unique event_id */
	for (index=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);
      
		if (ev == NULL) {
			continue; 
		}

		if (ev->id == id) {
			return ev;  
		}
	}
 
	return NULL;
}

GjEvent gj_event_find_next_by_type (GjRosterItem ri, GjEventType type)
{
	gint index = 0;
	gint id = -1;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	id = gj_roster_item_get_id (ri);

	if (type < 0) {
		g_warning ("%s: event type:%d was < 0", __FUNCTION__, type);
		return NULL;
	}

	/* do not warning this - this is perfectly normal */
	if (event_list == NULL || g_list_length (event_list) < 1) {
		return NULL;
	}

	/* loop through all events on contacts event list to look for unique event_id */
	for (index=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);

		if (ev == NULL) {
			continue; 
		}

		if (ev->ri_id == id && ev->type == type) {
			return ev; 
		}
	}

	return NULL;
}

GjEvent gj_event_find_next_by_message_type (GjRosterItem ri, GjMessageType message_type)
{
	gint index = 0;
	gint id = -1;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	id = gj_roster_item_get_id (ri);

	/* do not warning this - this is perfectly normal */
	if (event_list == NULL || g_list_length (event_list) < 1) {
		return NULL;
	}

	/* loop through all events on contacts event list to look for unique event_id */
	for (index=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);
      
		if (ev == NULL) {
			continue; 
		}
     
		if (ev->ri_id == id && ev->type == GjEventTypeMessage) {
			GjMessage m = (GjMessage) ev->data;
	    
			if (m == NULL) {
				continue;
			}

			if (gj_message_get_type (m) == message_type) {
				return ev; 
			}
		}
	}
	
	return NULL;
}

gint gj_event_count_by_type (GjRosterItem ri, GjEventType type)
{
	gint index = 0;
	gint count = 0;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return -1;
	}

	if (type < 0) {
		g_warning ("%s: roster item type:%d was < 0", __FUNCTION__, type);
		return -1;
	}

	/* loop through all events on contacts event list to look for unique event_id */
	for (index=0,count=0;index<g_list_length (event_list);index++) {
		GjEvent ev = g_list_nth_data (event_list, index);
      
		if (ev == NULL) {
			continue;
		}

		if (ev->type == type) {
			count++; 
		}
	}

	return count;
}

/*
 * member data
 */ 

/* sets */
gboolean gj_event_set_ri_id (GjEvent ev, gint id)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}

	if (id < 0) {
		g_warning ("%s: roster item id:%d was < 0", __FUNCTION__, id);
		return FALSE;
	}

	ev->ri_id = id;
	return TRUE;
}

/* gets */
gint gj_event_get_id (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return -1;
	}

	return ev->id;
}

const GjJID gj_event_get_jid (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return NULL;
	}

	return ev->jid;
}

GjEventType gj_event_get_type (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return -1;
	}

	return ev->type;
}

void *gj_event_get_data (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return NULL;
	}

	return ev->data;
}

time_t gj_event_get_time (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return -1;
	}

	return ev->time;
}

gint gj_event_get_ri_id (GjEvent ev)
{
	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return -1;
	} 

	return ev->ri_id;
}

/* convenience function */
GjRosterItem gj_event_get_roster_item (GjEvent ev)
{
	gint id = -1;
	GjRosterItem ri = NULL;

	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return NULL;
	} 

	id = gj_event_get_ri_id (ev);
	ri = gj_rosters_find_item_by_id (id);
	return ri;
}

/* 
 * callbacks
 */
static void gj_event_cb_presence_from (GjPresence pres)
{
	GjJID j_to = NULL;
	GjJID j_from = NULL;

	if (pres == NULL) {
		return;
	}

	j_to = gj_presence_get_to_jid (pres);
	j_from = gj_presence_get_from_jid (pres);

	/* show debugging */
	g_message ("++++++++++ received presence id:%3.3d ++++++++++", gj_presence_get_unique_id (pres));
	g_message ("++++");

	if (j_to) {
		g_message ("++++ to:'%s', resource:'%s'", gj_jid_get_full (j_to), gj_jid_get_resource (j_to));
	}

	if (j_from) {
		g_message ("++++ from:'%s', resource:'%s'", gj_jid_get_full (j_from), gj_jid_get_resource (j_from)); 
	}

	if (gj_presence_get_type (pres) != -1) {
		g_message ("++++ type:'%d'->'%s'", gj_presence_get_type (pres), gj_translate_presence_type (gj_presence_get_type (pres))); 
	}

	if (gj_presence_get_show (pres) != -1) {
		g_message ("++++ show:'%d'->'%s'", gj_presence_get_show (pres), gj_translate_presence_show (gj_presence_get_show (pres))); 
	}

	g_message ("++++ priority:'%d'", gj_presence_get_priority (pres));  

	if (gj_presence_get_status (pres) != NULL) {
		g_message ("++++ status:'%s'", gj_presence_get_status (pres)); 
	}

	if (gj_presence_get_error_code (pres) != NULL) {
		g_message ("++++ error code:'%s'", gj_presence_get_error_code (pres)); 
	}

	if (gj_presence_get_error_reason (pres) != NULL) {
		g_message ("++++ error reason:'%s'", gj_presence_get_error_reason (pres)); 
	}

	g_message ("++++");
	g_message ("++++++++++++++++++++++++++++++++++++++++++++++");

	/* automatically subscribe and agree subscription if agent */
	if (gj_jid_get_type (j_from) == GjJIDTypeAgent &&  
	    gj_presence_get_type (pres) == GjPresenceTypeSubscribe) {
		gj_presence_send_to (gj_presence_get_connection (pres),
				     j_from, 
				     TRUE, 
				     GjPresenceTypeSubscribed, 
				     GjPresenceShowEnd, 
				     NULL, 
				     0);
		
		return;
	}

	/* create new event */
	if (gj_event_new_from_presence (pres) == FALSE) {
		g_warning ("%s: failed to create new event", __FUNCTION__);
		return;
	} 
}

static void gj_event_cb_message_from (GjMessage m)
{
	GjJID j_to = NULL;
	GjJID j_from = NULL;

	if (m == NULL) {
		return;
	}

	g_message ("++++++++++ received message id:%3.3d ++++++++++", gj_message_get_unique_id (m));
	g_message ("++++");
	g_message ("++++ type:'%d'->'%s'", gj_message_get_type (m), gj_translate_message_type (gj_message_get_type (m)));
	g_message ("++++");

	j_to = gj_message_get_to_jid (m);
	j_from = gj_message_get_from_jid (m);

	if (gj_message_get_id (m) != NULL) {
		g_message ("++++ id:'%s'", gj_message_get_id (m));
	}

	if (j_to) {
		g_message ("++++ to:'%s'", gj_jid_get_full (j_to)); 
	}

	if (j_from) {
		g_message ("++++ from:'%s'", gj_jid_get_full (j_from));
	}

	if (gj_message_get_subject (m) != NULL) {
		g_message ("++++ subject:'%s'", gj_message_get_subject (m));
	}

	if (gj_message_get_thread (m) != NULL) {
		g_message ("++++ thread:'%s'", gj_message_get_thread (m));
	}

	if (gj_message_get_body (m) != NULL) {
		g_message ("++++ body:'%s'", gj_message_get_body (m));  
	}

	if (gj_message_get_x_event_type (m) != 0) {
		gchar *translation = NULL;
		translation = gj_translate_message_x_event_type (gj_message_get_x_event_type (m));
		g_message ("++++ x:event type:'%d'->'%s'", gj_message_get_x_event_type (m), translation);  
		if (translation != NULL) g_free (translation);
	}

	if (gj_message_get_x_event_message_id (m) != NULL) {
		g_message ("++++ x:event message id:'%s'", gj_message_get_x_event_message_id (m));  
	}

	if (gj_message_get_x_delay_stamp (m) != NULL) {
		g_message ("++++ x:delay stamp:'%s'", gj_message_get_x_delay_stamp (m));   
	}

	if (gj_message_get_x_oob_url (m) != NULL) {
		g_message ("++++ x:oob url:'%s'", gj_message_get_x_oob_url (m));  
	}

	if (gj_message_get_x_oob_description (m) != NULL) {
		g_message ("++++ x:oob description:'%s'", gj_message_get_x_oob_description (m));  
	}

	if (gj_message_get_error_code (m) != NULL) {
		g_message ("++++ error code:'%s'", gj_message_get_error_code (m));  
	}

	if (gj_message_get_error_reason (m) != NULL) {
		g_message ("++++ error reason:'%s'", gj_message_get_error_reason (m));  
	}
 
	g_message ("++++");
	g_message ("+++++++++++++++++++++++++++++++++++++++++++++");

	if (gj_message_get_type (m) != GjMessageTypeXEvent) {
		/* if XEvents ask to be notified when the message has 
		   been displayed, this is the time to send that notification */
		if (gj_message_get_x_event_type (m) & GjMessageXEventTypeDelivered) {
			gj_message_send_x_event (gj_message_get_connection (m),
						 GjMessageXEventTypeDelivered,
						 gj_jid_get_full (j_to),
						 gj_jid_get_full (j_from), 
						 gj_message_get_id (m)); 
		}
	}

	/* prefer method of posting by ri_id. */
	if (gj_event_new_from_message (m) == FALSE) {
		g_warning ("%s: failed to create new event", __FUNCTION__);
		return;
	} 
}

static void gj_event_cb_file_transfer_from (GjFileTransfer ft)
{
	GjJID j_to = NULL;
	GjJID j_from = NULL;

	if (ft == NULL) {
		return;
	}

	g_message ("+++++++ received file transfer: id:%3.3d ++++++", gj_file_transfer_get_unique_id (ft));
	g_message ("++++");

	j_to = gj_file_transfer_get_to_jid (ft);
	j_from = gj_file_transfer_get_from_jid (ft);

	/*   if (gj_file_transfer_get_id (ft) != NULL)  */
	/*     g_message ("++++ id:'%s'", gj_file_transfer_get_id (ft)); */

	if (j_to) {
		g_message ("++++ to:'%s'", gj_jid_get_full (j_to)); 
	}

	if (j_from) {
		g_message ("++++ from:'%s'", gj_jid_get_full (j_from)); 
	}

	/*   if (gj_message_get_error_code (m) != NULL) */
	/*     g_message ("++++ error code:'%s'", gj_message_get_error_code (m));   */
	/*   if (gj_message_get_error_reason (m) != NULL) */
	/*     g_message ("++++ error reason:'%s'", gj_message_get_error_reason (m));   */
 
	g_message ("++++");
	g_message ("+++++++++++++++++++++++++++++++++++++++++++++");

	/* prefer method of posting by ri_id. */
	if (gj_event_new_from_file_transfer (ft) == FALSE) {
		g_warning ("%s: failed to create new event", __FUNCTION__);
		return;
	} 
}


#ifdef __cplusplus
}
#endif
