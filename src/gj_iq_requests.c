/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>

#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_iq_responses.h"
#include "gj_roster.h"
#include "gj_parser.h"
#include "gj_stream.h"
#include "gj_support.h"


/*
 * ACTUAL REQUESTS from outside this module, eg:
 *
 *  - Greeting 
 *  - Auth Method
 *  - Auth
 *  - Roster
 *  - RosterRemove
 *  - RosterAdd
 *  - PasswordChange
 *  - UserChange (name, group...) 
 *  - VCardChange
 *  - VCard
 *  - Version
 *  - Time
 *  - Last
 *  - Agents
 *  - Browse
 *  - Search
 *  - File Transfer (initiate/close)
 *  ...
 *
 */

gboolean gj_iq_request_goodbye (GjConnection c, 
				const gchar *to)
{
	gj_stream_close_by_to (to);
  
	return TRUE;
}

gboolean gj_iq_request_auth_method (GjConnection c,
				    const gchar *username, 
				    GjInfoqueryCallback cb,
				    void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	/* checks */
	if (username == NULL) {
		g_warning ("%s: username was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL)  {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (username, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:auth", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"username", (guchar*)s1);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_auth_with_plain_password (GjConnection c,
						 const gchar *username, 
						 const gchar *password, 
						 const gchar *resource, 
						 GjInfoqueryCallback cb,
						 void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	/* checks */
	if (username == NULL) {
		g_warning ("%s: username was NULL", __FUNCTION__);
		return FALSE;
	}

	if (password == NULL) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery was NULL", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (username, -1);
	s2 = g_markup_escape_text (password, -1);
	s3 = g_markup_escape_text (resource, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:auth", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"username", (guchar*)s1);
	sub = xmlNewChild (node, ns, (guchar*)"password", (guchar*)s2);
	sub = xmlNewChild (node, ns, (guchar*)"resource", (guchar*)s3);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_auth_with_digest_password (GjConnection c,
						  const gchar *username, 
						  const gchar *digest, 
						  const gchar *resource, 
						  GjInfoqueryCallback cb,
						  void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	/* checks */   
	if (username == NULL) {
		g_warning ("%s: username was NULL", __FUNCTION__);
		return FALSE;
	}

	if (digest == NULL) {
		g_warning ("%s: digest was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: information query was NULL", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (username, -1);
	s2 = g_markup_escape_text (digest, -1);
	s3 = g_markup_escape_text (resource, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:auth", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"username", (guchar*)s1);
	sub = xmlNewChild (node, ns, (guchar*)"digest", (guchar*)s2);
	sub = xmlNewChild (node, ns, (guchar*)"resource", (guchar*)s3);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_roster (GjConnection c,
			       GjInfoqueryCallback cb, 
			       void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: query was NULL", __FUNCTION__);
		return FALSE;
	}

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:roster", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);
    
	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_roster_remove_contact (GjConnection c,
					      const gchar *jid, 
					      GjInfoqueryCallback cb, 
					      void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL; 
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:roster", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"item", NULL);
	xmlSetProp (sub, (guchar*)"jid", (guchar*)s1);
	xmlSetProp (sub, (guchar*)"subscription", (guchar*)"remove");
  
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);  
}

gboolean gj_iq_request_roster_add_contact (GjConnection c,
					   const gchar *jid, 
					   const gchar *name, 
					   GList *groups, 
					   GjInfoqueryCallback cb,
					   void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL; 
	xmlNsPtr ns = NULL;

	gint i = 0;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (name == NULL) {
		g_warning ("%s: name was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_markup_escape_text (name, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:roster", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"item", NULL);
	xmlSetProp (sub, (guchar*)"jid", (guchar*)s1);
	xmlSetProp (sub, (guchar*)"name", (guchar*)s2);

	if (groups != NULL) {
		for (i=0;i<g_list_length (groups);i++) {
			gchar *group = NULL;
			gchar *s4 = NULL;

			if ((group = (gchar*) g_list_nth_data (groups, i)) == NULL) {
				continue;
			}

			s4 = g_markup_escape_text (group, -1);
			xmlNewChild (sub, ns, (guchar*)"group", (guchar*)s4);

			g_free (s4);
		}
	}

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);  
}

gboolean gj_iq_request_password_change (GjConnection c,
					const gchar *to, 
					const gchar *password, 
					GjInfoqueryCallback cb,
					void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (password == NULL) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);
	s2 = g_markup_escape_text (password, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:register", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"password", (guchar*)s2);
 
	/* set struct info */
	gj_iq_set_to (iq, s1);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);

	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_user_change (GjConnection c,
				    const gchar *jid, 
				    const gchar *name, 
				    GList *groups, 
				    GjInfoqueryCallback cb,
				    void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (name == NULL) {
		g_warning ("%s: name was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_markup_escape_text (name, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:roster", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"item", NULL);
	xmlSetProp (sub, (guchar*)"jid", (guchar*)jid);
	xmlSetProp (sub, (guchar*)"name", (guchar*)name);

	/* go through groups list and add each item */
	if (groups != NULL) {
		gint i = 0;
      
		for (i=0;i<g_list_length (groups);i++) {
			gchar *g = (gchar*) g_list_nth_data (groups, i);
			gchar *s3 = NULL;

			if (g == NULL) {
				continue; 
			}

			s3 = g_markup_escape_text (g, -1);
			xmlNewChild (sub, ns, (guchar*)"group", (guchar*)s3);

			g_free (s3);
		}
	}
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_vcard_change (GjConnection c,
				     const gchar *fn,
				     const gchar *given,
				     const gchar *family,
				     const gchar *nickname,
				     const gchar *url,
				     const gchar *tel,
				     const gchar *email,
				     const gchar *desc,
				     const gchar *bday,
				     const gchar *street,
				     const gchar *extadd,
				     const gchar *locality,
				     const gchar *region,
				     const gchar *pcode,
				     const gchar *country,
				     const gchar *orgname,
				     const gchar *orgunit,
				     const gchar *title,
				     const gchar *role,
				     GjInfoqueryCallback cb,
				     void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub1 = NULL;
	xmlNodePtr sub2 = NULL;
	xmlNsPtr ns = NULL;

	gchar *s01 = NULL;
	gchar *s02 = NULL;
	gchar *s03 = NULL;
	gchar *s04 = NULL;
	gchar *s05 = NULL;
	gchar *s06 = NULL;
	gchar *s07 = NULL;
	gchar *s08 = NULL;
	gchar *s09 = NULL;
	gchar *s10 = NULL;
	gchar *s11 = NULL;
	gchar *s12 = NULL;
	gchar *s13 = NULL;
	gchar *s14 = NULL;
	gchar *s15 = NULL;
	gchar *s16 = NULL;
	gchar *s17 = NULL;
	gchar *s18 = NULL;
	gchar *s19 = NULL;

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s01 = g_markup_escape_text (fn, -1);
	s02 = g_markup_escape_text (family, -1);
	s03 = g_markup_escape_text (given, -1);
	s04 = g_markup_escape_text (nickname, -1);
	s05 = g_markup_escape_text (url, -1);
	s06 = g_markup_escape_text (tel, -1);
	s07 = g_markup_escape_text (email, -1);
	s08 = g_markup_escape_text (desc, -1);
	s09 = g_markup_escape_text (bday, -1);
	s10 = g_markup_escape_text (street, -1);
	s11 = g_markup_escape_text (extadd, -1);
	s12 = g_markup_escape_text (locality, -1);
	s13 = g_markup_escape_text (region, -1);
	s14 = g_markup_escape_text (pcode, -1);
	s15 = g_markup_escape_text (country, -1);
	s16 = g_markup_escape_text (orgname, -1);
	s17 = g_markup_escape_text (orgunit, -1);
	s18 = g_markup_escape_text (title, -1);
	s19 = g_markup_escape_text (role, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"vCard");
	ns = xmlNewNs (node, (guchar*)"vcard-temp", NULL);
	sub1 = xmlNewChild (node, ns, (guchar*)"FN", (guchar*)s01);

	sub1 = xmlNewChild (node, ns, (guchar*)"N", NULL);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"FAMILY", (guchar*)s02);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"GIVEN", (guchar*)s03);

	sub1 = xmlNewChild (node, ns, (guchar*)"NICKNAME", (guchar*)s04);
	sub1 = xmlNewChild (node, ns, (guchar*)"URL", (guchar*)s05);
	sub1 = xmlNewChild (node, ns, (guchar*)"TEL", (guchar*)s06);
	sub1 = xmlNewChild (node, ns, (guchar*)"EMAIL", (guchar*)s07);
	sub1 = xmlNewChild (node, ns, (guchar*)"DESC", (guchar*)s08);
	sub1 = xmlNewChild (node, ns, (guchar*)"BDAY", (guchar*)s09);

	sub1 = xmlNewChild (node, ns, (guchar*)"ADR", NULL);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"STREET", (guchar*)s10);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"EXTADD", (guchar*)s11);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"LOCALITY", (guchar*)s12);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"REGION", (guchar*)s13);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"PCODE", (guchar*)s14);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"COUNTRY", (guchar*)s15);

	sub1 = xmlNewChild (node, ns, (guchar*)"ORG", NULL);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"ORGNAME", (guchar*)s16);
	sub2 = xmlNewChild (sub1, ns, (guchar*)"ORGUNIT", (guchar*)s17);

	sub1 = xmlNewChild (node, ns, (guchar*)"TITLE", (guchar*)s18);
	sub1 = xmlNewChild (node, ns, (guchar*)"ROLE", (guchar*)s19);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s01);
	g_free (s02);
	g_free (s03);
	g_free (s04);
	g_free (s05);
	g_free (s06);
	g_free (s07);
	g_free (s08);
	g_free (s09);
	g_free (s10);
	g_free (s11);
	g_free (s12);
	g_free (s13);
	g_free (s14);
	g_free (s15);
	g_free (s16);
	g_free (s17);
	g_free (s18);
	g_free (s19);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_vcard (GjConnection c,
			      const gchar *to, 
			      GjInfoqueryCallback cb, 
			      void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"vCard");
	ns = xmlNewNs (node, (guchar*)"vcard-temp", NULL);
 
	/* set struct info */
	gj_iq_set_to (iq, s1);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_version (GjConnection c,
				const gchar *to, 
				GjInfoqueryCallback cb, 
				void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:version", NULL);
 
	/* set struct info */
	gj_iq_set_to (iq,to);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq,user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_time (GjConnection c,
			     const gchar *to, 
			     GjInfoqueryCallback cb, 
			     void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:time", NULL);
 
	/* set struct info */
	gj_iq_set_to (iq,s1);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq,user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_last (GjConnection c,
			     const gchar *to, 
			     GjInfoqueryCallback cb, 
			     void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:last", NULL);
 
	/* set struct info */
	gj_iq_set_to (iq,s1);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_user_data (iq,user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_register_user (GjConnection c,
				      const gchar *to, 
				      const gchar *username, 
				      const gchar *password, 
				      GjInfoqueryCallback cb,
				      void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	/* checks */
	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (username == NULL) {
		g_warning ("%s: username was NULL", __FUNCTION__);
		return FALSE;
	}

	if (password == NULL) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);
	s2 = g_markup_escape_text (username, -1);
	s3 = g_markup_escape_text (password, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:register", NULL);
	sub = xmlNewChild (node, ns, (guchar*)"username", (guchar*)s2);
	sub = xmlNewChild (node, ns, (guchar*)"password", (guchar*)s3);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);
	gj_iq_set_to (iq, s1);

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

#if 0
gboolean gj_iq_request_register (GjConnection c,
				 const gchar *to, 
				 GjInfoqueryCallback cb,
				 void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:register", NULL);
 
	/* set struct info */
	gj_iq_set_to (iq, s1);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}
#endif

gboolean gj_iq_request_register_requirements (GjConnection c,
					      const gchar *to, 
					      GjInfoqueryCallback cb,
					      void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:register", NULL);
 
	/* set struct info */
	gj_iq_set_to (iq, to);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_register (GjConnection c,
				 const gchar *to, 
				 GjInfoqueryCallback cb, 
				 const gchar *key,
				 const gchar *username,
				 const gchar *password,
				 const gchar *email,
				 const gchar *nickname,
				 void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (key == NULL) {
		g_warning ("%s: key was NULL", __FUNCTION__);
		return FALSE;
	}

	if (!username && !password && !email && !nickname) {
		g_warning ("%s: no username, password, email or nickname supplied", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:register", NULL);

	s2 = g_markup_escape_text ((gchar*)key, -1);
	sub = xmlNewChild (node, ns, (guchar*)"key", (guchar*)s2);
	g_free (s2);

	if (username) {
		gchar *str = NULL;
		str = g_markup_escape_text ((gchar*)username, -1);
		sub = xmlNewChild (node, ns, (guchar*)"username", (guchar*)str);
		g_free (str);
	}

	if (password) {
		gchar *str = NULL;
		str = g_markup_escape_text ((gchar*)password, -1);
		sub = xmlNewChild (node, ns, (guchar*)"password", (guchar*)str);
		g_free (str);
	}

	if (email) {
		gchar *str = NULL;
		str = g_markup_escape_text ((gchar*)email, -1);
		sub = xmlNewChild (node, ns, (guchar*)"email", (guchar*)str);
		g_free (str);
	}

	if (nickname) {
		gchar *str = NULL;
		str = g_markup_escape_text ((gchar*)nickname, -1);
		sub = xmlNewChild (node, ns, (guchar*)"nick", (guchar*)str);
		g_free (str);
	}
 
	/* set struct info */
	gj_iq_set_to (iq, s1);
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_unregister (GjConnection c,
				   const gchar *to, 
				   const gchar *key,
				   GjInfoqueryCallback cb,
				   void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	/* checks */
	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return FALSE;
	}

	if (key == NULL) {
		g_warning ("%s: key was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (to, -1);
	s2 = g_markup_escape_text (key, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query"); 
	ns = xmlNewNs (node, (guchar*)"jabber:iq:register", NULL);

	s2 = g_markup_escape_text ((gchar*)key, -1);
	sub = xmlNewChild (node, ns, (guchar*)"key", (guchar*)s2);
	g_free (s2);

	sub = xmlNewChild (node, ns, (guchar*)"remove", NULL);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_agents (GjConnection c,
			       const gchar *server, 
			       GjInfoqueryCallback cb,
			       void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	/* checks */
	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (server, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:agents", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_to (iq,s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_browse (GjConnection c,
			       const gchar *server, 
			       GjInfoqueryCallback cb,
			       void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	/* checks */
	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (server, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:browse", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_to (iq,s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_search_requirements (GjConnection c,
					    const gchar *server, 
					    GjInfoqueryCallback cb,
					    void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;

	/* checks */
	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (server, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:search", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_to (iq,s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_search (GjConnection c,
			       const gchar *server, 
			       GjInfoqueryCallback cb, 
			       void *user_data, 
			       ...)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	va_list args;
	guchar *key = NULL;
	guchar *value = NULL;

	gchar *s1 = NULL;

	/* checks */
	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (server, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"jabber:iq:search", NULL);

	va_start (args, user_data);
	key = (guchar*)va_arg (args, char*);

	for (; ; key = (guchar*)va_arg (args, char*)) {
		gchar *s2 = NULL;
		gchar *s3 = NULL;

		if (key == NULL) {
			break;
		}

		value = (guchar*)va_arg (args, char*);
      
		if (g_utf8_strlen ((gchar*)value, -1) < 1) {
			continue;
		}

		s2 = g_markup_escape_text ((gchar*)key, -1);
		s3 = g_markup_escape_text ((gchar*)value, -1);
  
		sub = xmlNewChild (node, ns, (guchar*)s2, (guchar*)s3);

		g_free (s2);
		g_free (s3);
	}
  
	va_end (args);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq,cb);
	gj_iq_set_to (iq,s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);

	if (gj_iq_build (iq,node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_file_transfer_si_set (GjConnection c,
					     const gchar *jid, 
					     const gchar *sid,
					     const gchar *mime_type,
					     const gchar *file_name,
					     const gchar *file_size,
					     GjInfoqueryCallback cb,
					     void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNodePtr sub_file = NULL;
	xmlNodePtr sub_option = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;
	gchar *s4 = NULL;
	gchar *s5 = NULL;

	/* checks */
	if (jid == NULL) {
		g_warning ("%s: to_jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (mime_type == NULL) {
		g_warning ("%s: mime type was NULL", __FUNCTION__);
		return FALSE;
	}

	if (file_name == NULL) {
		g_warning ("%s: file name was NULL", __FUNCTION__);
		return FALSE;
	}

	if (file_size == NULL) {
		g_warning ("%s: file size was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_markup_escape_text (sid, -1);
	s3 = g_markup_escape_text (mime_type, -1);
	s4 = g_markup_escape_text (file_name, -1);
	s5 = g_markup_escape_text (file_size, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"si");
	xmlSetProp (node, (guchar*)"id", (guchar*)s2);
	xmlSetProp (node, (guchar*)"mime-type", (guchar*)s3);
	xmlSetProp (node, (guchar*)"profile", (guchar*)"http://jabber.org/protocol/si/profile/file-transfer");
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/si", NULL);

	sub_file = xmlNewChild (node, NULL, (guchar*)"file", NULL);
	xmlSetProp (sub_file, (guchar*)"name", (guchar*)s4);
	xmlSetProp (sub_file, (guchar*)"size", (guchar*)s5);
	ns = xmlNewNs (sub_file, (guchar*)"http://jabber.org/protocol/si/profile/file-transfer", NULL);

	sub = xmlNewChild (node, NULL, (guchar*)"feature", NULL);
	ns = xmlNewNs (sub, (guchar*)"http://jabber.org/protocol/feature-neg", NULL);

	sub = xmlNewChild (sub, NULL, (guchar*)"x", NULL);
	xmlSetProp (sub, (guchar*)"type", (guchar*)"form");
	ns = xmlNewNs (sub, (guchar*)"jabber:x:data", NULL);

	sub = xmlNewChild (sub, NULL, (guchar*)"field", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"stream-method");
	xmlSetProp (sub, (guchar*)"type", (guchar*)"list-single");

	sub_option = xmlNewChild (sub, NULL, (guchar*)"option", NULL);
	xmlNewChild (sub_option, NULL, (guchar*)"value", (guchar*)"http://jabber.org/protocol/bytestreams");

	/*   sub_option = xmlNewChild (sub, NULL, (guchar*)"option", NULL); */
	/*   xmlNewChild (sub_option, NULL, (guchar*)"value", (guchar*)"http://jabber.org/protocol/ibb"); */
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);
	g_free (s4);
	g_free (s5);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_file_transfer_si_result (GjConnection c,
						const gchar *qid,
						const gchar *jid, 
						const gchar *sid,
						const gchar *mime_type,
						const gchar *file_name,
						const gchar *file_size,
						GjInfoqueryCallback cb,
						void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNodePtr sub_file = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;
	gchar *s4 = NULL;
	gchar *s5 = NULL;

	/* checks */
	if (jid == NULL) {
		g_warning ("%s: to_jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (mime_type == NULL) {
		g_warning ("%s: mime type was NULL", __FUNCTION__);
		return FALSE;
	}

	if (file_name == NULL) {
		g_warning ("%s: file name was NULL", __FUNCTION__);
		return FALSE;
	}

	if (file_size == NULL) {
		g_warning ("%s: file size was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeResult)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_markup_escape_text (sid, -1);
	s3 = g_markup_escape_text (mime_type, -1);
	s4 = g_markup_escape_text (file_name, -1);
	s5 = g_markup_escape_text (file_size, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"si");
	xmlSetProp (node, (guchar*)"id", (guchar*)s2);
	xmlSetProp (node, (guchar*)"mime-type", (guchar*)s3);
	xmlSetProp (node, (guchar*)"profile", (guchar*)"http://jabber.org/protocol/si/profile/file-transfer");
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/si", NULL);

	sub_file = xmlNewChild (node, NULL, (guchar*)"file", NULL);
	xmlSetProp (sub_file, (guchar*)"name", (guchar*)s4);
	xmlSetProp (sub_file, (guchar*)"size", (guchar*)s5);
	ns = xmlNewNs (sub_file, (guchar*)"http://jabber.org/protocol/si/profile/file-transfer", NULL);

	sub = xmlNewChild (node, NULL, (guchar*)"feature", NULL);
	ns = xmlNewNs (sub, (guchar*)"http://jabber.org/protocol/feature-neg", NULL);

	sub = xmlNewChild (sub, NULL, (guchar*)"x", NULL);
	xmlSetProp (sub, (guchar*)"type", (guchar*)"form");
	ns = xmlNewNs (sub, (guchar*)"jabber:x:data", NULL);

	sub = xmlNewChild (sub, NULL, (guchar*)"field", NULL);
	xmlSetProp (sub, (guchar*)"var", (guchar*)"stream-method");
	xmlSetProp (sub, (guchar*)"type", (guchar*)"list-single");

	xmlNewChild (sub, NULL, (guchar*)"value", (guchar*)"http://jabber.org/protocol/bytestreams");

	/*   sub_option = xmlNewChild (sub, NULL, (guchar*)"option", NULL); */
	/*   xmlNewChild (sub_option, NULL, (guchar*)"value", (guchar*)"http://jabber.org/protocol/ibb"); */
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	if (qid) {
		gj_iq_set_id (iq, qid);
	}

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);
	g_free (s4);
	g_free (s5);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_file_transfer_bytestreams_result (GjConnection c,
							 const gchar *qid,
							 const gchar *jid, 
							 const gchar *sid,
							 const gchar *stream_jid_used,
							 GjInfoqueryCallback cb,
							 void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNodePtr sub = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	/* checks */
	if (qid == NULL) {
		g_warning ("%s: qid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: to_jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (stream_jid_used == NULL) {
		g_warning ("%s: stream jid used was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeResult)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_markup_escape_text (sid, -1);
	s3 = g_markup_escape_text (stream_jid_used, -1);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	xmlSetProp (node, (guchar*)"sid", (guchar*)s2);
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/bytestreams", NULL);

	sub = xmlNewChild (node, NULL, (guchar*)"streamhost-used", NULL);
	xmlSetProp (sub, (guchar*)"jid", (guchar*)s3);

	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	if (qid) {
		gj_iq_set_id (iq, qid);
	}

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_file_transfer_ibb_init_send (GjConnection c,
						    const gchar *jid, 
						    gint block_size,
						    GjInfoqueryCallback cb,
						    void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;
	gchar *s3 = NULL;

	/* checks */
	if (jid == NULL) {
		g_warning ("%s: to_jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (block_size < 1) {
		g_warning ("%s: block_size was < 1", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeSet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_strdup_printf ("%d", gj_get_unique_id ());
	s3 = g_strdup_printf ("%d", block_size);

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"open");
	xmlSetProp (node, (guchar*)"sid", (guchar*)s2);
	xmlSetProp (node, (guchar*)"block-size", (guchar*)s3);
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/ibb", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);
	g_free (s3);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_disco_info (GjConnection c,
				   const gchar *jid, 
				   GjInfoqueryCallback cb,
				   void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	/* checks */
	if (jid == NULL) {
		g_warning ("%s: to_jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_strdup_printf ("%d", gj_get_unique_id ());

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/disco#info", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}

gboolean gj_iq_request_disco_items (GjConnection c,
				    const gchar *jid, 
				    GjInfoqueryCallback cb,
				    void *user_data)
{
	GjInfoquery iq = NULL;

	xmlNodePtr node = NULL;
	xmlNsPtr ns = NULL;

	gchar *s1 = NULL;
	gchar *s2 = NULL;

	/* checks */
	if (jid == NULL) {
		g_warning ("%s: to_jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: cb was NULL", __FUNCTION__);
		return FALSE;
	}

	if ((iq = gj_iq_new (GjInfoqueryTypeGet)) == NULL) {
		g_warning ("%s: infoquery could not be created", __FUNCTION__);
		return FALSE;
	}

	s1 = g_markup_escape_text (jid, -1);
	s2 = g_strdup_printf ("%d", gj_get_unique_id ());

	/* set xml */
	node = xmlNewNode (NULL, (guchar*)"query");
	ns = xmlNewNs (node, (guchar*)"http://jabber.org/protocol/disco#items", NULL);
 
	/* set struct info */
	gj_iq_set_connection (iq,c);
	gj_iq_set_callback (iq, cb);
	gj_iq_set_to (iq, s1);
	gj_iq_set_user_data (iq, user_data);

	/* clean up */
	g_free (s1);
	g_free (s2);

	if (gj_iq_build (iq, node) == FALSE) {
		g_warning ("%s: failed to build query.", __FUNCTION__);
		return FALSE;
	}

	return gj_iq_send (iq);
}


#ifdef __cplusplus
}
#endif
