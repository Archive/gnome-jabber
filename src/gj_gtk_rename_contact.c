/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_jid.h"
#include "gj_connection.h"

#include "gj_gtk_rename_contact.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry;
	GtkWidget *label_jid;
	GtkWidget *label_name;

	GtkWidget *button_ok;

	/* member data */
	GjConnection c;
	GjRosterItem ri;

} GjRenameContactDialog;


static gboolean gj_gtk_rc_save (GjRenameContactDialog *dialog);
     

/*
 * internal callbacks 
 */
static void gj_gtk_rc_user_changed_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data);

/*
 * gui functions 
 */
static void on_entry_changed (GtkEditable *editable, GjRenameContactDialog *dialog);
static void on_entry_activate (GtkEntry *entry, GjRenameContactDialog *dialog);

static void on_destroy (GtkWidget *widget, GjRenameContactDialog *dialog);
static void on_response (GtkDialog *window, gint response, GjRenameContactDialog *dialog);


static GjRenameContactDialog *current_dialog = NULL;

/*
 * window functions
 */
gboolean gj_gtk_rc_load (GjConnection c,
			 GjRosterItem ri)
{
	GladeXML *xml = NULL;
	GjRenameContactDialog *dialog = NULL;
	GjJID j = NULL;

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	if (j == NULL) {
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjRenameContactDialog, 1);

	dialog->c = gj_connection_ref (c);

	dialog->ri = ri;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "rename_contact",
				     NULL,
				     "rename_contact", &dialog->window,
				     "label_jid", &dialog->label_jid,
				     "entry", &dialog->entry,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "rename_contact", "response", on_response,
			      "rename_contact", "destroy", on_destroy,
			      "entry", "changed", on_entry_changed,
			      "entry", "activate", on_entry_activate,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set up window */
	gtk_label_set_text (GTK_LABEL (dialog->label_jid), gj_jid_get_full (j));
	gtk_entry_set_text (GTK_ENTRY (dialog->entry), gj_roster_item_get_name (ri));
	gtk_widget_grab_focus (GTK_WIDGET (dialog->entry));

	return TRUE;
}

static gboolean gj_gtk_rc_save (GjRenameContactDialog *dialog)
{
	G_CONST_RETURN gchar *jid = NULL;
	G_CONST_RETURN gchar *name = NULL;

	GjJID j = NULL;

	const gchar *jid_str = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	jid = gtk_label_get_text (GTK_LABEL (dialog->label_jid));
	name = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	if (dialog->ri == NULL) {
		g_warning ("%s: dialog roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (dialog->ri);

	/* save to jabber server */
	if (gj_jid_get_type (j) == GjJIDTypeAgent) {
		jid_str = gj_jid_get_full (j);
	} else {
		jid_str = gj_jid_get_without_resource (j);
	}

	gj_iq_request_user_change (dialog->c,
				   jid_str,
				   (gchar*)name, 
				   gj_roster_item_get_groups (dialog->ri),
				   gj_gtk_rc_user_changed_response,
				   NULL);
  
	return TRUE;
}

/* callbacks */
static void gj_gtk_rc_user_changed_response (GjInfoquery iq, GjInfoquery iq_related, gpointer user_data)
{
	g_message ("%s: response!", __FUNCTION__);
}

/*
 * GUI events
 */
static void on_entry_changed (GtkEditable *editable, GjRenameContactDialog *dialog)
{
	gchar *group = NULL;

	group = gtk_editable_get_chars (editable, 0, -1);
	if (group != NULL && g_utf8_strlen (group, -1) > 0) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), TRUE);
		g_free (group);
		return;
	}
      
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), FALSE);
}

static void on_entry_activate (GtkEntry *entry, GjRenameContactDialog *dialog)
{
	G_CONST_RETURN gchar *group = NULL;

	group = gtk_entry_get_text (entry);
	if (group != NULL && g_utf8_strlen (group, -1) > 0) {
		on_response (GTK_DIALOG (dialog->window), GTK_RESPONSE_OK, dialog); 
	}
}

static void on_destroy (GtkWidget *widget, GjRenameContactDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);

	g_free (dialog);
}

static void on_response (GtkDialog *window, gint response, GjRenameContactDialog *dialog)
{
	if (response == GTK_RESPONSE_OK) {
		gj_gtk_rc_save (dialog);
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
