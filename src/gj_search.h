/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$ 
 *
 */

#ifndef __gj_register_h
#define __gj_register_h

#include "gj_typedefs.h"

/* list functions */
gboolean searchInit ();
gboolean searchTerm ();

/* list functions */
GList *searchListGet ();

/* item functions */
search searchNew ();
void searchFree (search s);

/* find */
search searchFindByID (gchar *id);
search searchFindByTo (gchar *to);
search searchFindByFrom (gchar *from);

/* sets */
gboolean searchSetID (search s, gchar *id);
gboolean searchSetTo (search s, gchar *to);
gboolean searchSetFrom (search s, gchar *to);
gboolean searchSetInstructions (search s, gchar *instructions);
gboolean searchSetJID (search s, gchar *jid);
gboolean searchSetFirst (search s, gchar *first);
gboolean searchSetLast (search s, gchar *last);
gboolean searchSetNick (search s, gchar *nick);
gboolean searchSetEmail (search s, gchar *email);
gboolean searchSetAllowsFirst (search s, gboolean allowed);
gboolean searchSetAllowsLast (search s, gboolean allowed);
gboolean searchSetAllowsNick (search s, gboolean allowed);
gboolean searchSetAllowsEmail (search s, gboolean allowed);
gboolean searchSetKey (search s, gchar *key);
gboolean searchSetCallback (search s, searchCallback cb);

/* gets */
gchar *searchGetID (search s);
gchar *searchGetTo (search s);
gchar *searchGetFrom (search s);
gchar *searchGetInstructions (search s);
gchar *searchGetJID (search s);
gchar *searchGetFirst (search s);
gchar *searchGetLast (search s);
gchar *searchGetNick (search s);
gchar *searchGetEmail (search s);
gboolean searchGetAllowsFirst (search s);
gboolean searchGetAllowsLast (search s);
gboolean searchGetAllowsEmail (search s);
gboolean searchGetAllowsNickname (search s);
gchar *searchGetKey (search s);
searchCallback searchGetCallback (search s);

/*
 * parsing functions  
 */
gboolean searchRequestAllowsments (gchar *server, searchRequestCallback cb);
gboolean searchRequest (GjConnection c,
			gchar *server, 
			searchRequestCallback cb, 
			gchar *first, 
			gchar *last, 
			gchar *nick, 
			gchar *email);

#endif

