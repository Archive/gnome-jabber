/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <gtk/gtk.h>
#include <glade/glade.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_event.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_translate.h"
#include "gj_support.h"
#include "gj_jid.h"

#include "gj_gtk_event_viewer.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_support.h"


/* columns */
enum { 
	COL_EVENT_IMAGE,
	COL_EVENT_JID,
	COL_EVENT_NAME,
	COL_EVENT_ID,
	COL_EVENT_TYPE,
	COL_EVENT_TYPE_STR,
	COL_EVENT_DETAILS,
	COL_EVENT_TIME,
	COL_EVENT_NEW,
	COL_EVENT_COUNT
};


typedef struct  {
	GtkWidget *window;
  
	GtkWidget *treeview;
	GtkWidget *checkbutton_show_jid;

} GjEventViewerWindow;


/* model functions */
static gboolean gj_gtk_ev_model_setup (GjEventViewerWindow *window);
static gboolean gj_gtk_ev_model_populate_columns (GtkTreeView *view);
static gboolean gj_gtk_ev_model_details_from_event (GjEvent ev, gchar **details, gboolean *warning);

/* gui callbacks */
static void on_checkbutton_show_jid_toggled (GtkToggleButton *togglebutton, GjEventViewerWindow *window);

static void on_destroy (GtkWidget *widget, GjEventViewerWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjEventViewerWindow *window);


static GjEventViewerWindow *current_window = NULL;


gboolean gj_gtk_ev_load (gboolean show)
{
	GladeXML *xml = NULL;
	GjEventViewerWindow *window = NULL;

	if (current_window) {
		gj_gtk_roster_set_status_event_error (FALSE);
		gtk_window_present (GTK_WINDOW (current_window->window));

		/* if we do it on load, the window is hidden, and we get warnings */
		gdk_window_set_decorations (GDK_WINDOW (current_window->window), 
					    GDK_DECOR_ALL);

		return TRUE;
	}

	current_window = window = g_new0 (GjEventViewerWindow, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "event_viewer",
				     NULL,
				     "event_viewer", &window->window,
				     "treeview", &window->treeview,
				     "checkbutton_show_jid", &window->checkbutton_show_jid,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "event_viewer", "delete_event", on_delete_event,
			      "event_viewer", "destroy", on_destroy,
			      "checkbutton_show_jid", "toggled", on_checkbutton_show_jid_toggled,
			      NULL);

	g_object_unref (xml);

	/* setup controls */
	gj_gtk_ev_model_setup (window);

	if (show == TRUE) {
		gtk_window_present (GTK_WINDOW (window->window)); 
	}

	gj_gtk_roster_set_status_event_error (FALSE);

	return TRUE;
}

/* model functions */
static gboolean gj_gtk_ev_model_setup (GjEventViewerWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	view = GTK_TREE_VIEW (window->treeview);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	/* create store */
	store = gtk_list_store_new (COL_EVENT_COUNT,
				    G_TYPE_STRING,    /* stock_id */
				    G_TYPE_STRING,    /* jid */
				    G_TYPE_STRING,    /* name - friendly version */
				    G_TYPE_INT,       /* event id */
				    G_TYPE_INT,       /* type */
				    G_TYPE_STRING,    /* type string */
				    G_TYPE_STRING,    /* details */	
				    G_TYPE_STRING,    /* time */
				    G_TYPE_BOOLEAN);  /* new */

	/* model */
	model  = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	/* columns */
	gj_gtk_ev_model_populate_columns (view);
  
	/* properties */
	gtk_tree_view_set_enable_search (view, TRUE);
	gtk_tree_view_set_search_column (view, COL_EVENT_TIME); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view, TRUE);
	gtk_tree_view_set_headers_clickable (view, TRUE);

	/* clean up */
	g_object_unref (G_OBJECT (store));

	return TRUE;
}

static gboolean gj_gtk_ev_model_populate_columns (GtkTreeView *view)
{
	GtkTreeViewColumn *column = NULL; 
	GtkCellRenderer *renderer = NULL;
	GtkCellRenderer *renderer_image = NULL;
	GtkCellRenderer *renderer_text = NULL;
	guint col_offset = 0;

	if (GTK_TREE_VIEW (view) == NULL) {
		g_warning ("gj_gtk_ev_model_populate_columns: view was NULL");
		return FALSE;
	}  

	/* cell renderer's */
	renderer_image = gtk_cell_renderer_pixbuf_new ();
	renderer_text = gtk_cell_renderer_text_new ();

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, _("Time"));

	/* COL_EVENT_IMAGE */
	g_object_set (G_OBJECT (renderer_image), "yalign", (gfloat)0, NULL);
	gtk_tree_view_column_pack_start (column, renderer_image, FALSE);
	gtk_tree_view_column_add_attribute (column,
					    renderer_image,
					    "stock_id",COL_EVENT_IMAGE);  

	/* COL_EVENT_NAME */
	g_object_set (G_OBJECT (renderer_text), "yalign", (gfloat)0, NULL);
	gtk_tree_view_column_pack_end (column, renderer_text, TRUE);
	gtk_tree_view_column_add_attribute (column,
					    renderer_text,
					    "text",COL_EVENT_TIME);

	/* insert column */
	gtk_tree_view_insert_column (GTK_TREE_VIEW (view), column, -1);

	gtk_tree_view_column_set_sort_column_id (column, COL_EVENT_TIME);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	/* COL_EVENT_JID */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, "JID",
								  renderer, 
								  "text", COL_EVENT_JID,
								  NULL);
  
	g_object_set (G_OBJECT (renderer), "yalign", (gfloat)0, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_EVENT_JID);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_EVENT_JID);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (column), FALSE);

	/* COL_EVENT_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Name"),
								  renderer, 
								  "text", COL_EVENT_NAME,
								  NULL);
  
	g_object_set (G_OBJECT (renderer), "yalign", (gfloat)0, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_EVENT_NAME);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_EVENT_NAME);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	/* COL_EVENT_ID */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("ID"),
								  renderer, 
								  "text", COL_EVENT_ID,
								  NULL);
  
	g_object_set (G_OBJECT (renderer), "yalign", (gfloat)0, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_EVENT_ID);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_EVENT_ID);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	/* COL_EVENT_TYPE_STR */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Type"),
								  renderer, 
								  "text", COL_EVENT_TYPE_STR,
								  NULL);
  
	g_object_set (G_OBJECT (renderer), "yalign", (gfloat)0, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_EVENT_TYPE_STR);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_EVENT_TYPE_STR);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	/* COL_EVENT_DETAILS */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Details"),
								  renderer, 
								  "markup", COL_EVENT_DETAILS,
								  NULL);
  
	g_object_set (G_OBJECT (renderer), "yalign", (gfloat)0, NULL);
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_EVENT_DETAILS);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_EVENT_DETAILS);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	return TRUE;
}

gboolean gj_gtk_ev_model_add_from_event (GjEvent ev)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gchar *stock_id = NULL;
	gchar *details = NULL;
	gboolean warning = FALSE;

	gchar *name = NULL;
	gchar *groups = NULL;
	GjRosterItem ri = NULL;
	GjJID j = NULL;

	GjEventViewerWindow *window = NULL;

	if (!(window = current_window)) {
		return FALSE;
	}

	if (ev == NULL) {
		g_warning ("%s: ev was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (window->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	if ((ri = gj_event_get_roster_item (ev)) == NULL) {
		g_warning ("%s: could not get roster item from event id:%d", 
			   __FUNCTION__, gj_event_get_id (ev));
		return FALSE;
	}
  
	if (gj_gtk_ev_model_details_from_event (ev, &details, &warning) == TRUE) {
		/* set pb */
		if (warning == FALSE) {
			stock_id = GTK_STOCK_DIALOG_INFO;
		} else {
			stock_id = GTK_STOCK_DIALOG_ERROR; 
			gj_gtk_roster_set_status_event_error (TRUE);	
		}
	} else {
		g_warning ("%s: failed to get details, event may be incomplete.", __FUNCTION__); 
	}


	j = gj_event_get_jid (ev);

	groups = gj_roster_item_get_groups_as_string (ri);
	name = g_strdup_printf ("%s %s%s%s",
				gj_roster_item_get_name (ri),
				groups?"[":"",
				groups?groups:"",
				groups?"]":"");
  
	/* add */ 
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,    
			    COL_EVENT_IMAGE, stock_id,
			    COL_EVENT_JID, gj_jid_get_full (j),
			    COL_EVENT_NAME, name,
			    COL_EVENT_ID, gj_event_get_id (ev),
			    COL_EVENT_TYPE, gj_event_get_type (ev),
			    COL_EVENT_TYPE_STR, gj_translate_event_type (gj_event_get_type (ev)), 
			    COL_EVENT_DETAILS, details,
			    COL_EVENT_TIME, gj_get_timestamp_from_timet (gj_event_get_time (ev)),
			    COL_EVENT_NEW, TRUE,
			    -1);

	/* clean up */
	g_free (groups);
	g_free (name);
	g_free (details);
    
	return TRUE;
}

static gboolean gj_gtk_ev_model_details_from_event (GjEvent ev, gchar **details, gboolean *warning)
{
	GjPresence pres = NULL;
	GjMessage m = NULL;

	if (ev == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return FALSE;
	}
  
	if (details == NULL) {
		g_warning ("%s: details pointer was NULL", __FUNCTION__);
		return FALSE;
	}

	if (warning == NULL) {
		g_warning ("%s: warning pointer was NULL", __FUNCTION__);
		return FALSE;
	}

	switch (gj_event_get_type (ev)) {
	case GjEventTypePresence: {
		gchar *type = NULL;
		gchar *show = NULL;
		gchar *status = NULL;
		
		gchar *error_code = NULL;
		gchar *error_reason = NULL;
		
		if ((pres = gj_event_get_data (ev)) == NULL) {
			g_warning ("%s: presence was NULL", __FUNCTION__);
			return FALSE;
		}
		
		if (gj_presence_get_type (pres) == GjPresenceTypeError) {
			*warning = TRUE;
		} else {
			*warning = FALSE;
		}

		/* check for markup problems */
		if (gj_translate_presence_type (gj_presence_get_type (pres)) != NULL) {
			type = g_markup_escape_text (gj_translate_presence_type (gj_presence_get_type (pres)), -1);
		}
		
		if (gj_translate_presence_show (gj_presence_get_show (pres)) != NULL) {
			show = g_markup_escape_text (gj_translate_presence_show (gj_presence_get_show (pres)), -1);
		}
		
		if (gj_presence_get_status (pres) != NULL) {
			status = g_markup_escape_text (gj_presence_get_status (pres), -1);
		}
	
		if (gj_presence_get_error_code (pres) != NULL) {
			error_code = g_markup_escape_text (gj_presence_get_error_code (pres), -1); 
		}
	
		if (gj_presence_get_error_reason (pres) != NULL) {
			error_reason = g_markup_escape_text (gj_presence_get_error_reason (pres), -1);
		}

	
		*details = g_strdup_printf ("%s:<b>%s</b>, %s:<b>%s</b>%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
					    _("type"),
					    type,
					    _("show"), 
					    show,
					    status?", ":"",
					    status?_("status"):"",
					    status?":<b>":"",
					    status?status:"",
					    status?"</b>":"",
					    error_code?" ,":"",
					    error_code?_("error code"):"",
					    error_code?":<b>":"",
					    error_code?error_code:"",
					    error_code?"</b>":"",
					    error_reason?", ":"",
					    error_reason?_("error reason"):"",
					    error_reason?":<b>":"",
					    error_reason?error_reason:"",
					    error_reason?"</b>":"");
		
		
		g_free (type);
		g_free (show);
		g_free (status);

		g_free (error_code);
		g_free (error_reason);

		break;
	}
		
	case GjEventTypeMessage: {
		gchar *type = NULL;
		gchar *body = NULL;
		gchar *thread = NULL;
		
		gchar *xevent = NULL;
		
		gchar *error_code = NULL;
		gchar *error_reason = NULL;
		
		if ((m = gj_event_get_data (ev)) == NULL) {
			g_warning ("%s: message was NULL", __FUNCTION__);
			return FALSE;
		}
		
		if (gj_message_get_type (m) == GjMessageTypeError) {
			*warning = TRUE; 
		} else { 
			*warning = FALSE;  
		}

		/* FIXME: should really check all strings before parsing them to a markup function */
		if (gj_translate_message_type (gj_message_get_type (m)) != NULL) {
			type = g_markup_escape_text (gj_translate_message_type (gj_message_get_type (m)), -1); 
		}
		
		if (gj_message_get_body (m) != NULL) { 
			body = g_markup_escape_text (gj_message_get_body (m), -1); 
		}
	
		if (gj_message_get_thread (m) != NULL) {
				thread = g_markup_escape_text (gj_message_get_thread (m), -1);
		}
	
		if (gj_message_get_x_event_type (m) != 0 && 
		    gj_translate_message_x_event_type (gj_message_get_x_event_type (m)) != NULL) {
			xevent = g_markup_escape_text (gj_translate_message_x_event_type (gj_message_get_x_event_type (m)), -1); 
		}

		if (gj_message_get_error_code (m) != NULL) {
			error_code = g_markup_escape_text (gj_message_get_error_code (m), -1); 
		}
	
		if (gj_message_get_error_reason (m) != NULL) {
			error_reason = g_markup_escape_text (gj_message_get_error_reason (m), -1); 
		}
		
		
		*details = g_strdup_printf ("%s:<b>%s</b>%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", 
					    _("type"),
					    type, 
					    thread?", ":"",
					    thread?_("thread"):"",
					    thread?":<b>":"",
					    thread?thread:"",
					    thread?"</b>":"",
					    xevent?", ":"", 
					    xevent?_("xevent"):"", 
					    xevent?":<b>":"", 
					    xevent?xevent:"",
					    xevent?"</b>":"",
					    error_code?", ":"",
					    error_code?_("error code"):"",
					    error_code?":<b>":"",
					    error_code?error_code:"",
					    error_code?"</b>":"",
					    error_reason?", ":"",
					    error_reason?_("error reason"):"",
					    error_reason?":<b>":"",
					    error_reason?error_reason:"",
					    error_reason?"</b>":"",
					    warning && body?", ":"",
					    warning && body?_("body"):"",
					    warning && body?":\n<b>":"",
					    warning && body?body:"", 
					    warning && body?"</b>":"");
		
		g_free (type);
		g_free (body);
		g_free (thread);
		
		g_free (xevent);
		
		g_free (error_code);
		g_free (error_reason);
		
		break;
		}
		
	default: {
		break;
	}
	}
  
	return TRUE;
}

/* 
 * gui callbacks
 */
static void on_checkbutton_show_jid_toggled (GtkToggleButton *togglebutton, GjEventViewerWindow *window)
{
	GtkTreeView *view = NULL;
	GtkTreeViewColumn *column = NULL;

	gboolean visible = FALSE;

	view = GTK_TREE_VIEW (window->treeview);

	visible = gtk_toggle_button_get_active (togglebutton);
	column = gtk_tree_view_get_column (view, 1);

	if (column == NULL) {
		g_warning ("%s: could not get column", __FUNCTION__);
		return;
	}

	gtk_tree_view_column_set_visible (GTK_TREE_VIEW_COLUMN (column), visible);
}

static void on_destroy (GtkWidget *widget, GjEventViewerWindow *window)
{
	current_window = NULL;
	
	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjEventViewerWindow *window)
{
	gtk_widget_hide (GTK_WIDGET (window->window));

	return TRUE;
}

#ifdef __cplusplus
}
#endif
