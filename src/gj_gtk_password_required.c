/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_iq_requests.h"
#include "gj_parser.h"

#include "gj_gtk_password_required.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry;

	GtkWidget *button_ok;

	gboolean check_password;
	gchar *password;

	GjPasswordRequiredCallback cb;
	gpointer user_data;

} GjPasswordRequiredDialog;


static gboolean gj_gtk_pr_check (GjPasswordRequiredDialog *dialog);

static void on_entry_password_changed (GtkEditable *editable, GjPasswordRequiredDialog *dialog);
static void on_entry_password_activate (GtkEntry *entry, GjPasswordRequiredDialog *dialog);

static void on_destroy (GtkWidget *widget, GjPasswordRequiredDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjPasswordRequiredDialog *dialog);


static GjPasswordRequiredDialog *current_dialog = NULL;


gboolean gj_gtk_pr_load (gboolean check_password, 
			 const gchar *password, 
			 GjPasswordRequiredCallback cb,
			 gpointer user_data)
{
	GladeXML *xml = NULL;
	GjPasswordRequiredDialog *dialog = NULL;

	if (check_password && !password) {
		g_warning ("%s: password was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	if (current_dialog) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjPasswordRequiredDialog, 1);

	if (check_password) {
		dialog->password = g_strdup (password);
	}

	dialog->check_password = check_password;
	dialog->cb = cb;
	dialog->user_data = user_data;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "password_required",
				     NULL,
				     "password_required", &dialog->window,
				     "entry", &dialog->entry,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "password_required", "response", on_response,
			      "password_required", "destroy", on_destroy,
			      "entry", "changed", on_entry_password_changed,
			      "entry", "activate", on_entry_password_activate,
			      NULL);

	g_object_unref (xml);
  
	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);

	/* set up dialog */
	gtk_widget_set_sensitive (dialog->button_ok, FALSE);

	return TRUE;
}

static gboolean gj_gtk_pr_check (GjPasswordRequiredDialog *dialog)
{
	G_CONST_RETURN gchar *password = NULL;

	if (!dialog) {
		return FALSE;
	}

	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	if (!dialog->check_password) {
		return TRUE;
	}

	if (!password || strlen (password) < 1 ||
	    !dialog->password || strlen (dialog->password) < 1) { 
		return FALSE;
	}

	if (strcmp (dialog->password, password) == 0) {
		return TRUE;
	}

	return FALSE;
}

/*
 * GUI events
 */
static void on_entry_password_changed (GtkEditable *editable, GjPasswordRequiredDialog *dialog)
{
	G_CONST_RETURN gchar *password = NULL;
	password = gtk_entry_get_text (GTK_ENTRY (dialog->entry));
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), (password && strlen (password) > 0));
}

static void on_entry_password_activate (GtkEntry *entry, GjPasswordRequiredDialog *dialog)
{
	G_CONST_RETURN gchar *password = NULL; 
	password = gtk_entry_get_text (GTK_ENTRY (entry));

	if (!password || strlen (password) < 1) {
		return;
	}

	on_response (GTK_DIALOG (dialog->window), GTK_RESPONSE_OK, dialog); 
}

static void on_destroy (GtkWidget *widget, GjPasswordRequiredDialog *dialog)
{
	current_dialog = NULL;

	if (!dialog) {
		return;
	}

	g_free (dialog->password);
	
	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjPasswordRequiredDialog *dialog)
{
	if (response == GTK_RESPONSE_OK) {
		GjPasswordRequiredCallback cb = NULL;
		G_CONST_RETURN gchar *password = NULL; 

		password = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

		if (dialog->cb) {
			(dialog->cb) (gj_gtk_pr_check (dialog), password, dialog->user_data);
		}
	} else {
		if (dialog->cb) {
			(dialog->cb) (FALSE, NULL, dialog->user_data);
		}
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
