/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_translate.h"
#include "gj_event.h"
#include "gj_support.h"
#include "gj_config.h"
#include "gj_jid.h"

#include "gj_gtk_roster.h"
#include "gj_gtk_headline.h" 
#include "gj_gtk_preferences.h"
#include "gj_gtk_support.h"
#include "gj_gtk_stock.h"


struct t_GjHeadlineWindow {
	GtkWidget *window;

	GtkWidget *textview;

	GtkWidget *button_clear;
	GtkWidget *button_close;

	gint ri_id;

	/*   gint x; */
	/*   gint y; */
	/*   gint w; */
	/*   gint h; */
};


/* 
 * window functions 
 */
static GjHeadlineWindow *gj_gtk_hw_load (GjRosterItem ri);

static gboolean gj_gtk_hw_setup (GjHeadlineWindow *window);

static GjHeadlineWindow *gj_gtk_hw_find_by_ri (GjRosterItem ri);

static gboolean gj_gtk_hw_add (GjHeadlineWindow *window, GjMessage m);
static gboolean gj_gtk_hw_clear (GjHeadlineWindow *window);

/* 
 * gui callbacks 
 */
static void on_textbuffer_changed (GtkTextBuffer *textbuffer, GjHeadlineWindow *window);
static gboolean on_textview_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjHeadlineWindow *window);
static gboolean on_textview_button_press_event (GtkWidget *widget, GdkEventButton *event, GjHeadlineWindow *window);

static void on_button_clear_clicked (GtkButton *button, GjHeadlineWindow *window);
static void on_button_close_clicked (GtkButton *button, GjHeadlineWindow *window);

static void on_realize (GtkWidget *widget, GjHeadlineWindow *window);
static void on_destroy (GtkWidget *widget, GjHeadlineWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjHeadlineWindow *window);


static GList *headline_windows = NULL;


/*
 * find functions
 */
static GjHeadlineWindow *gj_gtk_hw_load (GjRosterItem ri)
{
	GladeXML *xml = NULL;
	GdkPixbuf *pb = NULL;
	GjHeadlineWindow *window = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	window = (GjHeadlineWindow*) g_new0 (struct t_GjHeadlineWindow, 1);

	window->ri_id = gj_roster_item_get_id (ri);

	headline_windows = g_list_append (headline_windows, window);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "headline",
				     NULL,
				     "headline", &window->window,
				     "textview", &window->textview,
				     "button_clear", &window->button_clear,
				     "button_close", &window->button_close,
				     NULL);


	gj_gtk_glade_connect (xml,
			      window,
			      "headline", "delete_event", on_delete_event,
			      "headline", "destroy", on_destroy,
			      "headline", "realize", on_realize,
			      "button_clear", "clicked", on_button_clear_clicked,
			      "button_close", "clicked", on_button_close_clicked,
			      "textview", "button_press_event", on_textview_button_press_event,
			      "textview", "motion_notify_event", on_textview_motion_notify_event,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (window->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);

	/* set icon */
	pb = gj_gtk_pixbuf_new_from_stock (GJ_STOCK_MESSAGE_HEADLINE);
	gtk_window_set_icon (GTK_WINDOW (window->window), pb);
	g_object_unref (G_OBJECT (pb));

	/* set up model */
	gj_gtk_hw_setup (window);

	return window;
}

gboolean gj_gtk_hw_load_by_ri (GjRosterItem ri)
{
	GjHeadlineWindow *window = NULL;
	GjJID j = NULL;

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);
	window = gj_gtk_hw_find_by_ri (ri);

	if (!window) {
		g_warning ("%s: could not find headline window by get roster item by id:%d (jid:'%s')",
			   __FUNCTION__, gj_roster_item_get_id (ri), gj_jid_get_full (j));
		return FALSE;     
	}

	gtk_window_present (GTK_WINDOW (window->window));

	return TRUE;    
}

static gboolean gj_gtk_hw_setup (GjHeadlineWindow *window)
{
	GtkTextView *view = NULL;
	GtkTextBuffer *buffer = NULL;
	gchar *title = NULL;
  
	GjRosterItem ri = NULL;
	GjJID j = NULL;

	if (window == NULL) {
		return FALSE;
	}

	ri = gj_rosters_find_item_by_id (window->ri_id);

	if (!ri) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_roster_item_get_jid (ri);

	view = GTK_TEXT_VIEW (window->textview);
	buffer = GTK_TEXT_BUFFER (gtk_text_view_get_buffer (view));
  
	/* set function callbacks */
	g_signal_connect (G_OBJECT (buffer), "changed", 
			  G_CALLBACK (on_textbuffer_changed), 
			  NULL);

	/* set styles */
	gtk_text_buffer_create_tag (buffer, "bold", 
				    "weight", PANGO_WEIGHT_BOLD, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "italic", 
				    "style", PANGO_STYLE_ITALIC, 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "blue_foreground", 
				    "foreground", "darkblue", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "red_foreground", 
				    "foreground", "darkred", 
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "black_foreground", 
				    "foreground", "black", 
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "url", 
				    "foreground", "blue", 
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "url_desc", 
				    /* 			     "weight", PANGO_WEIGHT_BOLD,  */
				    "underline", PANGO_UNDERLINE_SINGLE,
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "left_margin_small",
				    "left_margin", 5,
				    NULL);
	gtk_text_buffer_create_tag (buffer, "left_margin_large",
				    "left_margin", 30,
				    NULL);
	gtk_text_buffer_create_tag (buffer, "not_editable",
				    "editable", FALSE, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "underline",
				    "underline", PANGO_UNDERLINE_SINGLE, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "tiny_gap_after_line",
				    "pixels_below_lines", 5, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "small_gap_after_line",
				    "pixels_below_lines", 15, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "big_gap_after_line",
				    "pixels_below_lines", 30, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "big_gap_before_line",
				    "pixels_above_lines", 30, 
				    NULL);
	gtk_text_buffer_create_tag (buffer, "large",
				    "scale", PANGO_SCALE_LARGE, 
				    NULL);
  
	/* set title */
	title = g_strdup_printf ("%s [%s]", _("Headlines for"), gj_jid_get_full (j));
	gtk_window_set_title (GTK_WINDOW (window->window), title);

	/* clean up */
	g_free (title);

	return TRUE;
}

static GjHeadlineWindow *gj_gtk_hw_find_by_ri (GjRosterItem ri)
{
	GList *l = NULL;
	gint id = 0;

	if (!ri) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	id = gj_roster_item_get_id (ri);

	for (l = headline_windows; l; l = l->next) {
		GjHeadlineWindow *window = l->data;

		if (window && window->ri_id == id) {
			return window;
		}
	}

	return NULL;  
}

static gboolean gj_gtk_hw_add (GjHeadlineWindow *window, GjMessage m)
{
	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter;

	const gchar *time = NULL;
	const gchar *message = NULL;
    
	time = gj_get_time_and_date_from_timet (gj_message_get_time (m));
	message = gj_message_get_body (m);

	if (window == NULL) {
		g_warning ("%s: headline window was NULL", __FUNCTION__);
		return FALSE;
	}

	textview = GTK_TEXT_VIEW (window->textview);
	buffer = gtk_text_view_get_buffer (textview);

	if (time == NULL) {
		g_warning ("%s: time was NULL", __FUNCTION__);
		return FALSE;
	}

	if (message == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return FALSE;
	}

	gtk_text_buffer_get_end_iter (buffer, &iter);

	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
						  time, -1,
						  "bold",
						  "black_foreground",
						  "left_margin_small",
						  "tiny_gap_after_line",
						  NULL);

	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
						  "\n", -1,
						  "black_foreground",
						  NULL);

	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
						  gj_message_get_body (m), -1,
						  "red_foreground",
						  "left_margin_large",
						  NULL);

	if (gj_message_get_x_oob_url (m) != NULL && gj_message_get_x_oob_description (m) != NULL) {
		/* show at end of m */
		gtk_text_buffer_remove_all_tags (buffer, &iter, &iter);
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
							  "\n", -1,
							  "black_foreground",
							  "left_margin_large",
							  NULL);

		/* show url */      
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
							  gj_message_get_x_oob_description (m), -1,
							  "left_margin_large",
							  "url_desc",
							  NULL);

		gtk_text_buffer_remove_all_tags (buffer, &iter, &iter);
		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
							  "\n", -1,
							  "black_foreground",
							  NULL);

		gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
							  gj_message_get_x_oob_url (m), -1,
							  "left_margin_large",
							  "url",
							  NULL);
	}

	gtk_text_buffer_remove_all_tags (buffer, &iter, &iter);
	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, 
						  "\n\n\n", -1,
						  "black_foreground",
						  NULL);

	return TRUE;
}

static gboolean gj_gtk_hw_clear (GjHeadlineWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	if (window == NULL) {
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview));
	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
  
	/* clear buffer */
	gtk_text_buffer_delete (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
	gtk_text_buffer_delete_mark_by_name (GTK_TEXT_BUFFER (buffer), "url");

	return TRUE;
}

GjHeadlineWindow *gj_gtk_hw_notification_new (gint event_id, GjMessage m)
{
	GjHeadlineWindow *window = NULL;
	GjEvent ev = NULL;
	GjRosterItem ri = NULL;
	GjJID j = NULL;

	if (m == NULL) {
		g_warning ("%s: message was NULL", __FUNCTION__);
		return NULL;
	}

	if ((ev = gj_event_find_by_id (event_id)) == NULL) {
		g_warning ("%s: event was NULL", __FUNCTION__);
		return NULL;
	}

	if ((ri = gj_rosters_find_item_by_id (gj_event_get_ri_id (ev))) == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return NULL;
	}

	j = gj_roster_item_get_jid (ri);
	window = gj_gtk_hw_find_by_ri (ri);
	
	if (!window) {
		window = gj_gtk_hw_load (ri);
	} else {
		/* should we show it? */
		if (gj_config_get_mpf_raise_window_on_message () == TRUE) {
			gtk_window_present (GTK_WINDOW (window->window));
		}
	}

	gj_gtk_hw_add (window, m);

	return window;
}

/*
 * GUI events
 */
static void on_textbuffer_changed (GtkTextBuffer *textbuffer, GjHeadlineWindow *window)
{
	gj_gtk_textbuffer_handle_changed (textbuffer, window);
}

static gboolean on_textview_motion_notify_event (GtkWidget *widget, GdkEventMotion *event, GjHeadlineWindow *window)
{
	return gj_gtk_textview_handle_motion_notify_event (widget, event, window);
}

static gboolean on_textview_button_press_event (GtkWidget *widget, GdkEventButton *event, GjHeadlineWindow *window)
{
	return gj_gtk_textview_handle_button_press_event (widget, event, window);
}

static void on_button_clear_clicked (GtkButton *button, GjHeadlineWindow *window)
{
	gj_gtk_hw_clear (window);
}

static void on_button_close_clicked (GtkButton *button, GjHeadlineWindow *window)
{
	gtk_widget_hide (window->window);
}

static void on_realize (GtkWidget *widget, GjHeadlineWindow *window)
{
	gtk_widget_set_events (GTK_WIDGET (window->textview),
			       GDK_EXPOSURE_MASK | 
			       GDK_POINTER_MOTION_MASK | 
			       GDK_POINTER_MOTION_HINT_MASK | 
			       GDK_BUTTON_RELEASE_MASK | 
			       GDK_STRUCTURE_MASK | 
			       GDK_PROPERTY_CHANGE_MASK);
}

static void on_destroy (GtkWidget *widget, GjHeadlineWindow *window)
{
	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjHeadlineWindow *window)
{
	gtk_widget_hide (window->window);
	return TRUE;
}

#ifdef __cplusplus
}
#endif
