/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_config_handler_h
#define __gj_config_handler_h

#include <glib.h>


typedef struct {
	gchar *key;
	gchar *value;

} GjConfigLine;


typedef struct {
	gchar *name;
	GList *lines;

} GjConfigSection;


typedef struct {
	GList *sections;

} GjConfigFile;


GjConfigFile *gj_config_handler_new (void);
void gj_config_handler_free (GjConfigFile *cfg);

GjConfigFile *gj_config_handler_open_file (gchar *filename);
GjConfigFile *gj_config_handler_open_default_file (void);

gboolean gj_config_handler_write_file (GjConfigFile *cfg, gchar *filename);
gboolean gj_config_handler_write_default_file (GjConfigFile *cfg);

GjConfigSection *gj_config_handler_find_section (GjConfigFile *cfg, gchar *name);

gboolean gj_config_handler_read_string (GjConfigFile *cfg, gchar *section, gchar *key, gchar ** value);
gboolean gj_config_handler_read_int (GjConfigFile *cfg, gchar *section, gchar *key, gint *value);
gboolean gj_config_handler_read_int8 (GjConfigFile *cfg, gchar *section, gchar *key, gint8 *value);
gboolean gj_config_handler_read_boolean (GjConfigFile *cfg, gchar *section, gchar *key, gboolean *value);
gboolean gj_config_handler_read_float (GjConfigFile *cfg, gchar *section, gchar *key, gfloat *value);
gboolean gj_config_handler_read_double (GjConfigFile *cfg, gchar *section, gchar *key, gdouble *value);

void gj_config_handler_write_string (GjConfigFile *cfg, gchar *section, gchar *key, gchar *value);
void gj_config_handler_write_int (GjConfigFile *cfg, gchar *section, gchar *key, gint value);
void gj_config_handler_write_int8 (GjConfigFile *cfg, gchar *section, gchar *key, gint8 value);
void gj_config_handler_write_boolean (GjConfigFile *cfg, gchar *section, gchar *key, gboolean value);
void gj_config_handler_write_float (GjConfigFile *cfg, gchar *section, gchar *key, gfloat value);
void gj_config_handler_write_double (GjConfigFile *cfg, gchar *section, gchar *key, gdouble value);

void gj_config_handler_remove_key (GjConfigFile *cfg, gchar *section, gchar *key);


#ifdef __cplusplus
};
#endif

#endif
