/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_roster_h
#define __gj_roster_h

#include "gj_typedefs.h"


void gj_roster_stats ();

/*
 * roster 
 */
GjRoster gj_roster_new (const GjConnection c,
		       const gchar *jid_str,
		       GjRosterCallback cb, 
		       GjRosterSetCallback set_cb,
		       gpointer user_data);

void gj_roster_free (GjRoster r);

GjRoster gj_roster_find (const GjConnection c);
GjRosterItem gj_roster_find_item_by_jid (GjRoster r, GjJID j);

/*
 * roster items
 */
gboolean gj_roster_add_item (GjRoster r, GjRosterItem ri);
gboolean gj_roster_del_item (GjRoster r, GjRosterItem ri);
gboolean gj_roster_find_item (GjRoster r, GjRosterItem ri);

/*
 * hash table for all roster items
 */
gboolean gj_roster_index_add_item (GjRosterItem ri);
gboolean gj_roster_index_del_item (GjRosterItem ri);

/*
 * across all rosters
 */
GjRosterItem gj_rosters_find_item_by_id (guint id);
GjRosterItem gj_rosters_find_item_by_jid (GjJID j);
gboolean gj_rosters_is_agent_registered (const gchar *agent);
GjRosterItem gj_rosters_find_item_by_group_chat (GjJID j);


/*
 * roster contacts/groups/agents
 */
const GList *gj_roster_get_list ();


/* utils */
const GList *gj_roster_get_groups (GjRoster r);
const GList *gj_roster_get_agents (GjRoster r);
const GList *gj_roster_get_contacts (GjRoster r);

GList *gj_roster_get_contacts_by_group (GjRoster r, const gchar *group);
GList *gj_roster_get_contacts_by_agent (GjRoster r, const gchar *agent);
GList *gj_roster_get_agents_by_jid (GjRoster r, const gchar *jid);

gboolean gj_roster_is_agent_registered (const GjRoster r, const gchar *agent);

/*
 * other
 */

/* requests */
gboolean gj_roster_request (GjConnection c,
			    GjRoster r);

/* inbound */
gboolean gj_roster_parse_set_request (GjInfoquery iq);

#endif

