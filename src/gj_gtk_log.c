/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <stdio.h>
#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#include "gj_main.h"
#include "gj_log.h"
#include "gj_support.h"

#include "gj_gtk_log.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *textview;
	GtkWidget *button_clear;
	/*   GtkWidget *button_close; */
	GtkWidget *togglebutton_scrolling;

	gboolean stop_scrolling;
  
} GjLogWindow;


/*
 * window functions 
 */ 

static gboolean gj_gtk_log_setup (GjLogWindow *window);
static gboolean gj_gtk_log_clear (GjLogWindow *window);

/*
 * member functions 
 */ 

/* gets */
static gboolean gj_gtk_log_get_stop_scrolling (GjLogWindow *window);

/* sets */
static gboolean gj_gtk_log_set_stop_scrolling (GjLogWindow *window, gboolean stop_scrolling);

/*
 * GUI functions
 */
static void on_togglebutton_scrolling_toggled (GtkToggleButton *togglebutton, GjLogWindow *window);
static void on_button_clear_clicked (GtkButton *button, GjLogWindow *window);

static void on_destroy (GtkWidget *widget, GjLogWindow *window);
static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjLogWindow *window);


static GjLogWindow *current_window = NULL;


gboolean gj_gtk_log_load ()
{
	GladeXML *xml = NULL;
	GjLogWindow *window = NULL;

	if (current_window) {
		gj_gtk_log_show ();
		return TRUE;
	}

	current_window = window = g_new0 (GjLogWindow, 1);

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "log",
				     NULL,
				     "log", &window->window,
				     "textview", &window->textview,
				     "button_clear", &window->button_clear,
				     NULL);

	gj_gtk_glade_connect (xml,
			      window,
			      "log", "delete_event", on_delete_event,
			      "log", "destroy", on_destroy,
			      "togglebutton_scrolling", "toggled", on_togglebutton_scrolling_toggled,
			      "button_clear", "clicked", on_button_clear_clicked,
			      NULL);

	g_object_unref (xml);

	/* set up tags for log */
	gj_gtk_log_setup (window);
	gj_gtk_log_set_stop_scrolling (window, FALSE);

	return TRUE;
}

gboolean gj_gtk_log_show ()
{
	if (!current_window) {
		return FALSE;
	}

	gtk_widget_show (current_window->window);

	/* if we do it on load, the window is hidden, and we get warnings */
	gdk_window_set_decorations (GDK_WINDOW (current_window->window->window), 
				    GDK_DECOR_ALL);

	return TRUE;
}

gboolean gj_gtk_log_hide ()
{
	if (!current_window) {
		return FALSE;
	}

	gtk_widget_hide (current_window->window);
	return TRUE;
}

static gboolean gj_gtk_log_setup (GjLogWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	if (!window) {
		return FALSE;
	}

	/* set buffer iterator at position 0 */
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview));
	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
  
	/* set styles */
	gtk_text_buffer_create_tag (buffer, "bold", 
				    "weight", PANGO_WEIGHT_BOLD, 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);
	gtk_text_buffer_create_tag (buffer, "italic", 
				    "style", PANGO_STYLE_ITALIC, 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "critical", 
				    "foreground", "darkred", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "error", 
				    "foreground", "red", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "warning", 
				    "foreground", "darkorange", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "message", 
				    "foreground", "darkblue", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);  
	gtk_text_buffer_create_tag (buffer, "other", 
				    "foreground", "black", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "debug", 
				    "foreground", "grey", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL); 
	gtk_text_buffer_create_tag (buffer, "details", 
				    "foreground", "darkgreen", 
				    "family", "monospace",
				    "scale", PANGO_SCALE_SMALL,
				    NULL);  

	return TRUE;
}

/*
 * member functions 
 */ 

/* gets */
static gboolean gj_gtk_log_get_stop_scrolling (GjLogWindow *window)
{
	if (!window) {
		return FALSE;
	}

	return window->stop_scrolling;
}

/* sets */
static gboolean gj_gtk_log_set_stop_scrolling (GjLogWindow *window, gboolean stop_scrolling)
{
	if (!window) {
		return FALSE;
	}

	window->stop_scrolling = stop_scrolling;
	return TRUE;
}

static gboolean gj_gtk_log_clear (GjLogWindow *window)
{
	GtkTextBuffer *buffer = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	if (!window) {
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->textview));
	gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
  
	/* clear buffer */
	gtk_text_buffer_delete (GTK_TEXT_BUFFER (buffer), &iter_start, &iter_end);
  
	return TRUE;
}

gboolean gj_gtk_log_new (const gchar *log_domain, 
			 GLogLevelFlags log_level, 
			 const gchar *message, 
			 gpointer user_data)
{
	GtkTextView *textview = NULL;
	GtkTextBuffer *buffer = NULL;
	GtkTextMark *mark = NULL;
	GtkTextIter iter_start;
	GtkTextIter iter_end;

	gchar *details = NULL;
	gchar *format = NULL;
	const gchar *tag = NULL;

	GjLogWindow *window = NULL;
	
	if (!(window = current_window)) {
		return;
	}
	
	if (log_level < 0 || message == NULL) {
		g_print ("%s: failed to add new message, log level:%d "
			 "was < 0 OR message was NULL\n", 
			 __FUNCTION__, log_level);
		return FALSE;
	}

	if ((textview = GTK_TEXT_VIEW (window->textview)) == NULL) {
		fprintf (stderr, "%s: textview was NULL", __FUNCTION__);
		return FALSE;
	}

	buffer = gtk_text_view_get_buffer (textview);
	gtk_text_buffer_get_bounds (buffer, &iter_start, &iter_end);

	/* format strings */
	details = g_strdup_printf ("%s - %s\t",
				   gj_get_timestamp (),
				   log_domain?log_domain:"GJ");

	format = g_strdup_printf ("%s\n",message);

	/* print initial part of message */
	switch (log_level) {
	case G_LOG_LEVEL_ERROR: 
		tag = _("error");
		break;
	case G_LOG_LEVEL_CRITICAL:
		tag = _("critical");
		break;
	case G_LOG_LEVEL_WARNING:
		tag = _("warning");
		break;
	case G_LOG_LEVEL_MESSAGE:
		tag = _("message");
		break;
	case G_LOG_LEVEL_DEBUG:
		tag = _("debug");
		break;
	case G_LOG_LEVEL_INFO:
	default:
		tag = _("other");
		break;
	}

	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter_end,
						  details, -1,
						  "details",
						  NULL);

	gtk_text_buffer_insert_with_tags_by_name (buffer, &iter_end,
						  format, -1,
						  tag,
						  NULL);

	if (gj_gtk_log_get_stop_scrolling (window) == FALSE) {
		mark = gtk_text_buffer_create_mark (buffer, NULL, &iter_end, FALSE);
		gtk_text_view_scroll_mark_onscreen (textview, mark);
		gtk_text_buffer_delete_mark (buffer, mark);
	}

	g_free (details);
	g_free (format);

	return TRUE;
}

/*
 * GUI events
 */
static void on_togglebutton_scrolling_toggled (GtkToggleButton *togglebutton, GjLogWindow *window)
{
	gj_gtk_log_set_stop_scrolling (window, gtk_toggle_button_get_active (togglebutton));
}

static void on_button_clear_clicked (GtkButton *button, GjLogWindow *window)
{
	gj_gtk_log_clear (window);
}

static void on_destroy (GtkWidget *widget, GjLogWindow *window)
{
	current_window = NULL;

	g_free (window);
}

static gboolean on_delete_event (GtkWidget *widget, GdkEvent *event, GjLogWindow *window)
{
	gj_gtk_log_hide (window);

	return TRUE;
}

#ifdef __cplusplus
}
#endif
