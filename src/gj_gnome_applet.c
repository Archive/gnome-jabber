/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_GNOME

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <gnome.h>
#include <libbonoboui.h>
#include <panel-applet.h>

#include "gj_main.h"

#define DOCKED_APPLET_WIDTH 74
#define FLOATING_APPLET_WIDTH 14
#define SHORT_APPLET_HEIGHT 22
#define TALL_APPLET_HEIGHT 44

typedef struct t_GnomeJabberApplet {
	GtkWidget *parent_applet;

	GtkWidget *hbox;
	GtkWidget *eventbox;
	GtkWidget *image;

	gint size;
	PanelAppletOrient orientation;

} GnomeJabberApplet;

/*typedef struct t_ctlApplet *applet;*/

/* applet callbacks */
static void gj_gnome_applet_change_size_cb (PanelApplet *widget, gint size, GnomeJabberApplet *applet);
static void gj_gnome_applet_change_orientation_cb (PanelApplet *widget, PanelAppletOrient orient, GnomeJabberApplet *applet);
static gboolean gj_gnome_applet_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, gpointer data);

/* menu callbacks  */
void gj_gnome_applet_about_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname);
void gj_gnome_applet_help_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname);
void gj_gnome_applet_connection_settings_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname);
void gj_gnome_applet_preferences_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname);
void gj_gnome_applet_show_or_hide_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname);

static gboolean gj_gnome_applet_new (PanelApplet *parent_applet);
static gboolean gj_gnome_applet_factory (PanelApplet *applet, const gchar *iid, gpointer data);

/*
 * applet callbacks
 */
static void gj_gnome_applet_change_size_cb (PanelApplet *widget, gint size, GnomeJabberApplet *applet)
{
	applet->size = size;
}

static void gj_gnome_applet_change_orientation_cb (PanelApplet *widget, PanelAppletOrient orient, GnomeJabberApplet *applet)
{
	applet->orientation = orient;
}

static gboolean gj_gnome_applet_button_press_event_cb (GtkWidget *widget, GdkEventButton *event, gpointer data)
{

	return FALSE;
}

/*
 * menu callbacks 
 */
void gj_gnome_applet_about_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname)
{
}

void gj_gnome_applet_help_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname)
{
#if 0
	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_MODAL,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_CLOSE,
					 ("There was is NO help"));
#endif
}

void gj_gnome_applet_connection_settings_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname)
{
}

void gj_gnome_applet_preferences_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname)
{
}

void gj_gnome_applet_show_or_hide_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname)
{
}

/*
 * applet set up 
 */
static const BonoboUIVerb gj_gnome_applet_menu_verbs [] = {
	BONOBO_UI_VERB ("show_or_hide", gj_gnome_applet_show_or_hide_cb),
	BONOBO_UI_VERB ("connection_settings", gj_gnome_applet_connection_settings_cb),
	BONOBO_UI_VERB ("preferences", gj_gnome_applet_preferences_cb),
	BONOBO_UI_VERB ("help", gj_gnome_applet_help_cb),
	BONOBO_UI_VERB ("about", gj_gnome_applet_about_cb),
       
	BONOBO_UI_VERB_END
};

static gboolean gj_gnome_applet_new (PanelApplet *parent_applet)
{
	GnomeJabberApplet *applet = NULL;

	applet = g_new0 (GnomeJabberApplet, 1);	
	/*gnome_window_icon_set_default_from_file (GNOME_ICONDIR"/gdict.png");*/

	/* set up controls */
	applet->hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (applet->hbox);

	applet->eventbox = gtk_event_box_new ();
	gtk_widget_show (applet->eventbox);
	gtk_box_pack_start (GTK_BOX (applet->hbox), applet->eventbox, TRUE, TRUE, 0);

	/*  image = create_pixmap (window1, NULL);*/
	gtk_widget_show (applet->image);
	gtk_container_add (GTK_CONTAINER (applet->eventbox), applet->image);

	/* add our controls to the container of the parent applet */
	applet->parent_applet = GTK_WIDGET (parent_applet);
	gtk_container_add (GTK_CONTAINER (parent_applet), applet->hbox);

	/* set panel orientation */
	applet->size = panel_applet_get_size (PANEL_APPLET (applet->parent_applet));
	applet->orientation = panel_applet_get_orient (PANEL_APPLET (applet->parent_applet));
 
	/* create app */
	/*  gj_gtk_roster_load (TRUE);*/
 	
	/* set menu */
	panel_applet_setup_menu_from_file (PANEL_APPLET (applet->parent_applet),
					   NULL,
					   "gnome-jabber-applet.xml",
					   NULL,
					   gj_gnome_applet_menu_verbs,
					   applet);                               

	gtk_widget_show (applet->parent_applet);

	gnome_jabber_main (0, NULL);
	
	/* hook up signals */
	g_signal_connect (G_OBJECT (applet->eventbox), "button_press_event",
			  G_CALLBACK (gj_gnome_applet_button_press_event_cb), parent_applet);

	g_signal_connect (G_OBJECT (applet->parent_applet), "change_size",
			  G_CALLBACK (gj_gnome_applet_change_size_cb), applet);
	g_signal_connect (G_OBJECT (applet->parent_applet), "change_orient",
			  G_CALLBACK (gj_gnome_applet_change_orientation_cb), applet);
 	
	return TRUE;
}

static gboolean gj_gnome_applet_factory (PanelApplet *applet, const gchar *iid, gpointer data)
{
	gboolean retval = FALSE;

	if (!strcmp (iid, "OAFIID:GNOME_GnomeJabber")) {
		retval = gj_gnome_applet_new (applet);
	}

	g_message ("gj_gnome_applet_factory: returned %d->%s", retval, retval?"success":"failure"); 
  
	return retval;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_GnomeJabber_Factory",
			     PANEL_TYPE_APPLET,
                             "GnomeJabber",
                             "0",
                             gj_gnome_applet_factory,
                             NULL)

#endif
 	
#ifdef __cplusplus
     }
#endif
