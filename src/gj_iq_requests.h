/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_iq_requests_h
#define __gj_iq_requests_h

#include "gj_typedefs.h"

gboolean gj_iq_request_greeting (GjConnection c,
				 const gchar *to, 
				 guint port, 
				 GjStreamCallback cb_connected, 
				 GjStreamCallback cb_disconnected, 
				 GjStreamCallback cb_error,
				 void *userdata);

gboolean gj_iq_request_goodbye (GjConnection c,
				const gchar *to);

gboolean gj_iq_request_auth_method (GjConnection c,
				    const gchar *username, 
				    GjInfoqueryCallback cb,
				    void *userdata);

gboolean gj_iq_request_auth_with_plain_password (GjConnection c,
						 const gchar *username, 
						 const gchar *password, 
						 const gchar *resource, 
						 GjInfoqueryCallback cb,
						 void *userdata);

gboolean gj_iq_request_auth_with_digest_password (GjConnection c,
						  const gchar *username, 
						  const gchar *password, 
						  const gchar *resource, 
						  GjInfoqueryCallback cb,
						  void *userdata);


gboolean gj_iq_request_roster (GjConnection c,
			       GjInfoqueryCallback cb,
			       void *userdata);

gboolean gj_iq_request_roster_remove_contact (GjConnection c,
					      const gchar *jid, 
					      GjInfoqueryCallback cb,
					      void *userdata);

gboolean gj_iq_request_roster_add_contact (GjConnection c,
					   const gchar *jid, 
					   const gchar *name, 
					   GList *groups, 
					   GjInfoqueryCallback cb,
					   void *userdata);

gboolean gj_iq_request_vcard (GjConnection c,
			      const gchar *to, 
			      GjInfoqueryCallback cb, 
			      void *userdata);

gboolean gj_iq_request_version (GjConnection c,
				const gchar *to, 
				GjInfoqueryCallback cb, 
				void *userdata);

gboolean gj_iq_request_time (GjConnection c,
			     const gchar *to, 
			     GjInfoqueryCallback cb, 
			     void *userdata);

gboolean gj_iq_request_last (GjConnection c,
			     const gchar *to, 
			     GjInfoqueryCallback cb, 
			     void *userdata);

gboolean gj_iq_request_password_change (GjConnection c,
					const gchar *to, 
					const gchar *password, 
					GjInfoqueryCallback cb,
					void *userdata);

gboolean gj_iq_request_user_change (GjConnection c,
				    const gchar *jid, 
				    const gchar *name, 
				    GList *group, 
				    GjInfoqueryCallback cb,
				    void *userdata);

gboolean gj_iq_request_vcard_change (GjConnection c,
				     const gchar *fn,
				     const gchar *given,
				     const gchar *family,
				     const gchar *nickname,
				     const gchar *url,
				     const gchar *tel,
				     const gchar *email,
				     const gchar *desc,
				     const gchar *bday,
				     const gchar *street,
				     const gchar *extadd,
				     const gchar *locality,
				     const gchar *region,
				     const gchar *pcode,
				     const gchar *country,
				     const gchar *orgname,
				     const gchar *orgunit,
				     const gchar *title,
				     const gchar *role,
				     GjInfoqueryCallback cb,
				     void *userdata);

gboolean gj_iq_request_register_user (GjConnection c,
				      const gchar *to, 
				      const gchar *username, 
				      const gchar *password, 
				      GjInfoqueryCallback cb,
				      void *userdata);

gboolean gj_iq_request_register_requirements (GjConnection c,
					      const gchar *to, 
					      GjInfoqueryCallback cb,
					      void *userdata);

gboolean gj_iq_request_register (GjConnection c,
				 const gchar *to, 
				 GjInfoqueryCallback cb, 
				 const gchar *key,
				 const gchar *username,
				 const gchar *password,
				 const gchar *email,
				 const gchar *nickname,
				 void *userdata);

gboolean gj_iq_request_unregister (GjConnection c,
				   const gchar *to, 
				   const gchar *key,
				   GjInfoqueryCallback cb,
				   void *userdata);


gboolean gj_iq_request_agents (GjConnection c,
			       const gchar *server, 
			       GjInfoqueryCallback cb,
			       void *userdata);

gboolean gj_iq_request_browse (GjConnection c,
			       const gchar *server, 
			       GjInfoqueryCallback cb,
			       void *userdata);

gboolean gj_iq_request_search_requirements (GjConnection c,
					    const gchar *to, 
					    GjInfoqueryCallback cb,
					    void *userdata);

gboolean gj_iq_request_search (GjConnection c,
			       const gchar *to, 
			       GjInfoqueryCallback cb, 
			       void *userdata,
			       ...);


gboolean gj_iq_request_file_transfer_si_set (GjConnection c,
					     const gchar *jid, 
					     const gchar *sid,
					     const gchar *mime_type,
					     const gchar *file_name,
					     const gchar *file_size,
					     GjInfoqueryCallback cb,
					     void *userdata);

gboolean gj_iq_request_file_transfer_si_result (GjConnection c,
						const gchar *qid, 
						const gchar *jid, 
						const gchar *sid,
						const gchar *mime_type,
						const gchar *file_name,
						const gchar *file_size,
						GjInfoqueryCallback cb,
						void *userdata);

gboolean gj_iq_request_file_transfer_bytestreams_result (GjConnection c,
							 const gchar *qid,
							 const gchar *jid, 
							 const gchar *sid,
							 const gchar *stream_jid_used,
							 GjInfoqueryCallback cb,
							 void *userdata);

gboolean gj_iq_request_file_transfer_ibb_init_send (GjConnection c,
						    const gchar *jid, 
						    gint block_size,
						    GjInfoqueryCallback cb,
						    void *userdata);


gboolean gj_iq_request_disco_info (GjConnection c,
				   const gchar *jid, 
				   GjInfoqueryCallback cb,
				   void *userdata);

gboolean gj_iq_request_disco_items (GjConnection c,
				    const gchar *jid, 
				    GjInfoqueryCallback cb,
				    void *userdata);

#endif

#ifdef __cplusplus
}
#endif
