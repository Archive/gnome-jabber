/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "gj_main.h"
#include "gj_roster.h"
#include "gj_jid.h"
#include "gj_connection.h"

#include "gj_gtk_send_message.h"
#include "gj_gtk_message.h"
#include "gj_gtk_chat.h"
#include "gj_gtk_support.h"


typedef struct  {
	GtkWidget *window;

	GtkWidget *entry;

	GtkWidget *button_ok;

	/* members */
	GjConnection c;
	GjJID connected_jid;
	GjRoster r;

} GjSendMessageDialog;


static void on_entry_changed (GtkEditable *editable, GjSendMessageDialog *dialog);
static void on_entry_activate (GtkEntry *entry, GjSendMessageDialog *dialog);

static void on_destroy (GtkWidget *widget, GjSendMessageDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjSendMessageDialog *dialog);


static GjSendMessageDialog *current_dialog = NULL;


gboolean gj_gtk_sm_load (GjConnection c,
			 GjJID connected_jid, 
			 GjRoster r)
{
	GladeXML *xml = NULL;
	GjSendMessageDialog *dialog = NULL;

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));
		return TRUE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (connected_jid == NULL) {
		g_warning ("%s: connected jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (r == NULL) {
		g_warning ("%s: roster was NULL", __FUNCTION__);
		return FALSE;
	}

	current_dialog = dialog = g_new0 (GjSendMessageDialog, 1);
    
	dialog->c = gj_connection_ref (c);
	dialog->connected_jid = gj_jid_ref (connected_jid);
	dialog->r = r;

	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "send_message",
				     NULL,
				     "send_message", &dialog->window,
				     "entry", &dialog->entry,
				     "button_ok", &dialog->button_ok,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "send_message", "response", on_response,
			      "send_message", "destroy", on_destroy,
			      "entry", "changed", on_entry_changed,
			      "entry", "activate", on_entry_activate,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE);
  
	return TRUE;
}

/*
 * GUI events
 */
static void on_entry_changed (GtkEditable *editable, GjSendMessageDialog *dialog)
{
	G_CONST_RETURN gchar *jid = NULL;

	jid = gtk_entry_get_text (GTK_ENTRY (dialog->entry));

	if (jid != NULL && g_utf8_strlen (jid, -1) > 0 && gj_jid_string_is_valid_jid (jid) == TRUE) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), TRUE);
		return;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_ok), FALSE);
}

static void on_entry_activate (GtkEntry *entry, GjSendMessageDialog *dialog)
{
	G_CONST_RETURN gchar *jid = NULL;

	jid = gtk_entry_get_text (entry);

	if (jid != NULL && g_utf8_strlen (jid, -1) > 0 && gj_jid_string_is_valid_jid (jid) == TRUE) {
		on_response (GTK_DIALOG (dialog->window), GTK_RESPONSE_OK, NULL); 
	}
}

static void on_destroy (GtkWidget *widget, GjSendMessageDialog *dialog)
{
	current_dialog = NULL;

	gj_connection_unref (dialog->c);
	gj_jid_unref (dialog->connected_jid);

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjSendMessageDialog *dialog)
{
	G_CONST_RETURN gchar *jid = NULL;

	if (response == GTK_RESPONSE_OK) {
		jid = gtk_entry_get_text (GTK_ENTRY (dialog->entry));
		/*       gj_gtk_message_send_new_by_jid ((gchar*)jid);  */
		gj_gtk_chat_send_new_by_jid (dialog->c,
					     dialog->connected_jid,
					     dialog->r, 
					     (gchar*)jid); 
	}

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
