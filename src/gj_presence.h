/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_presence_h
#define __gj_presence_h

#include "gj_typedefs.h"

void gj_presence_stats ();

/* item functions */
GjPresence gj_presence_new (GjPresenceType type, GjPresenceShow show);

GjPresence gj_presence_ref (GjPresence pres);
gboolean gj_presence_unref (GjPresence pres);


/* 
 * member functions 
 */

/* sets */


/* gets */
gint gj_presence_get_unique_id (GjPresence pres);

GjConnection gj_presence_get_connection (GjPresence pres);

GjPresenceType gj_presence_get_type (GjPresence pres);
GjPresenceShow gj_presence_get_show (GjPresence pres);

const gchar *gj_presence_get_status (GjPresence pres);
gint8 gj_presence_get_priority (GjPresence pres);

const GjJID gj_presence_get_to_jid (GjPresence pres);
const GjJID gj_presence_get_from_jid (GjPresence pres);

const gchar *gj_presence_get_error_code (GjPresence pres);
const gchar *gj_presence_get_error_reason (GjPresence pres);

/*
 * entry into module... sending/receiving presence.
 */
gboolean gj_presence_send_to (GjConnection c,
			      GjJID j,
			      gboolean full_jid,
			      GjPresenceType type, 
			      GjPresenceShow show, 
			      const gchar *status, 
			      gint8 priority);

gboolean gj_presence_send (GjConnection c,
			   GjPresenceType type,
			   GjPresenceShow show, 
			   const gchar *status, 
			   gint8 priority);

#if 0
gboolean gj_presence_send_to_ri (GjConnection c,
				 GjRosterItem ri, 
				 GjPresenceType type, 
				 GjPresenceShow show, 
				 const gchar *status, 
				 gint8 priority);
#endif

void gj_presence_receive (GjConnection c, xmlNodePtr node);

/* callbacks */
gboolean gj_presence_set_cb_to (GjPresenceToCallback cb);
gboolean gj_presence_set_cb_from (GjPresenceFromCallback cb);

#endif

