/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * $RCSfile$
 *
 * $Author$
 * $Date$
 *
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_file_transfer_h
#define __gj_file_transfer_h

#include "gj_typedefs.h"

GjFileTransfer gj_file_transfer_ref (GjFileTransfer ft);
gboolean gj_file_transfer_unref (GjFileTransfer ft);

void gj_file_transfer_start (GjConnection c,
			     GjRosterItem ri, 
			     GjFileTransferErrorCallback error_cb, 
			     gpointer user_data);

void gj_file_transfer_accept (GjConnection c,
			      GjFileTransfer ft);

void gj_file_transfer_reject (GjConnection c, 
			      GjFileTransfer ft);

/*
 * member functions 
 */

/* gets */
gint gj_file_transfer_get_unique_id (GjFileTransfer ft);

GjConnection gj_file_transfer_get_connection (GjFileTransfer ft);

GjJID gj_file_transfer_get_to_jid (GjFileTransfer ft);
GjJID gj_file_transfer_get_from_jid (GjFileTransfer ft);

const gchar *gj_file_transfer_get_mime_type (GjFileTransfer ft);

const gchar *gj_file_transfer_get_file_name (GjFileTransfer ft);
const gchar *gj_file_transfer_get_file_size (GjFileTransfer ft);

const gchar *gj_file_transfer_get_description (GjFileTransfer ft);


/* inbound xml */
void gj_file_transfer_si_receive (GjConnection c, xmlNodePtr node);
void gj_file_transfer_bs_receive (GjConnection c, xmlNodePtr node);
void gj_file_transfer_ibb_receive (GjConnection c, xmlNodePtr node);


#endif

#ifdef __cplusplus
}
#endif
