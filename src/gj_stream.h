/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifndef __gj_stream_h
#define __gj_stream_h

#include "gj_typedefs.h"

GjStream gj_stream_new (const char *host);
gboolean gj_stream_free (GjStream stm);

gboolean gj_stream_open (GjStream stm); 
gboolean gj_stream_close (GjStream stm); 
gboolean gj_stream_close_by_to (const gchar *to);

GjStream gj_stream_find_by_connection (GjConnection c);

gboolean gj_stream_build (GjStream stm);

gboolean gj_stream_set_port (GjStream stm, guint port);

gboolean gj_stream_set_connected_cb (GjStream stm, GjStreamCallback cb, gpointer user_data);
gboolean gj_stream_set_disconnected_cb (GjStream stm, GjStreamCallback cb, gpointer user_data);
gboolean gj_stream_set_error_cb (GjStream stm, GjStreamCallback cb, gpointer user_data);

void gj_stream_set_is_disconnect_planned (GjStream stm, gboolean is_disconnect_planned);

const gchar *gj_stream_get_id (GjStream stm);
GjConnection gj_stream_get_connection (GjStream stm);

gboolean gj_stream_get_is_disconnect_planned (GjStream stm);

void gj_stream_receive_error (GjConnection c, xmlNodePtr node);
void gj_stream_receive (GjConnection c, gchar *from, gchar *id);
void gj_stream_receive_closing (GjConnection c);

#endif

