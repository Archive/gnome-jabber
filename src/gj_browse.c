/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include "gj_browse.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_jid.h"
#include "gj_connection.h"


struct t_GjBrowseItem {
	GjJID j;

	GjBrowseCategory category;
	GjBrowseType type;
  
	gchar *name;

	GList *namespaces;
};

typedef struct {
	gchar *server;

	GjBrowseCallback cb; 
	gpointer user_data;

} GjBrowse;


static gboolean gj_browse_item_set_category (GjBrowseItem bi, GjBrowseCategory category);
static gboolean gj_browse_item_set_type (GjBrowseItem bi, GjBrowseType type);

static void gj_browse_cb (GjInfoquery iq, GjInfoquery iq_related, GjBrowse *browse);
static GList *gj_browse_parse (xmlNodePtr parent);


/*
 * items
 */
static gboolean gj_browse_item_set_category (GjBrowseItem bi, GjBrowseCategory category)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (category < 1 || category > GjBrowseCategoryEnd) {
		g_warning ("%s: type was out of range", __FUNCTION__);
		return FALSE;
	}

	bi->category = category;
	return TRUE;
}

static gboolean gj_browse_item_set_type (GjBrowseItem bi, GjBrowseType type)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (type < 1 || type > GjBrowseTypeEnd) {
		g_warning ("%s: type was out of range", __FUNCTION__);
		return FALSE;
	}

	bi->type = type;
	return TRUE;
}


GjBrowseCategory gj_browse_item_get_category (GjBrowseItem bi)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return GjBrowseCategoryNull;
	}
  
	return bi->category;
}

GjBrowseType gj_browse_item_get_type (GjBrowseItem bi)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return GjBrowseTypeNull;
	}
  
	return bi->type;
}

GjJID gj_browse_item_get_jid (GjBrowseItem bi)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return NULL;
	}
  
	return bi->j;
}

const gchar *gj_browse_item_get_name (GjBrowseItem bi)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return NULL;
	}
  
	return bi->name;
}

const GList *gj_browse_item_get_namespaces (GjBrowseItem bi)
{
	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return NULL;
	}
  
	return bi->namespaces;
}

gboolean gj_browse_item_include_namespace (GjBrowseItem bi, const gchar *namespace)
{
	gint i = 0;

	if (bi == NULL) {
		g_warning ("%s: item was NULL", __FUNCTION__);
		return FALSE;
	}

	if (namespace == NULL) {
		g_warning ("%s: namespace was NULL", __FUNCTION__);
		return FALSE;
	}

	for (i=0; i<g_list_length (bi->namespaces); i++) {
		const gchar *ns = NULL;

		ns = g_list_nth_data (bi->namespaces, i);

		if (ns && g_strcasecmp (ns, namespace) == 0) {
			return TRUE;
		}
	}

	return FALSE;
}

/*
 * administration
 */
gboolean gj_browse_request (GjConnection c, 
			    const gchar *server, 
			    GjBrowseCallback cb, 
			    void *user_data)
{
	GjBrowse *data = NULL;

	if (!c) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (server == NULL || strlen (server) < 1) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	data = g_new0 (GjBrowse, 1);
  
	data->server = g_strdup (server);
	data->cb = cb;
	data->user_data = user_data;

	gj_iq_request_browse (c, server, (GjInfoqueryCallback)gj_browse_cb, data);
	return TRUE;
}

static void gj_browse_cb (GjInfoquery iq, GjInfoquery iq_related, GjBrowse *data)
{
	GList *list = NULL;

	if (!iq) {
		return;
	}

	list = gj_browse_parse (gj_iq_get_xml (iq));

	if (list) {
		gint i = 0;

		if (data && data->cb) {
			(data->cb) (data->server, list, 0, NULL, data->user_data);
		}

		g_message ("%s: cleaning up", __FUNCTION__);


		for (i=0; i<g_list_length (list); i++) {
			GjBrowseItem bi = NULL;

			bi = g_list_nth_data (list, i);
			if (!bi) continue;

			if (bi->j) {
				gj_jid_unref (bi->j);
			}

			g_free (bi->name);  

			if (bi->namespaces) {
				g_list_foreach (bi->namespaces, (GFunc)g_free, NULL);
				g_list_free (bi->namespaces);
			}

			g_free (bi);
		}

		g_list_free (list);
	}

	if (data) {
		g_free (data->server);
	}
}

static GList *gj_browse_parse (xmlNodePtr parent)
{
	GList *list = NULL;
	GList *item_list = NULL;
	GList *service_list = NULL;
  
	gint i = 0;
  
	if (parent == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return NULL;
	}
  
	if (!parent->xmlChildrenNode) {
		g_warning ("%s: unexpected - no content to xml message", __FUNCTION__);
		return NULL;
	} 

	/* get item node */
	item_list = gj_parser_find_by_node_on_parent_and_ret_by_list (parent->xmlChildrenNode, (guchar*)"item");
	service_list = gj_parser_find_by_node_on_parent_and_ret_by_list (parent->xmlChildrenNode, (guchar*)"service");

	/* get items in agent */
	for (i=0; i<g_list_length (item_list); i++) {
		xmlNodePtr node = NULL;
		xmlChar *stype = NULL;
		xmlChar *scategory = NULL;
		xmlChar *tmp = NULL;

		GjBrowseItem item = NULL;
		GjJID j = NULL;

		node = (xmlNodePtr) g_list_nth_data (item_list, i);
      
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"jid")) != NULL) {
			j = gj_jid_new ((gchar*)tmp);
		}

		/* REQUIRED */
		if (j == NULL) {
			continue;
		}

		item = g_new0 (struct t_GjBrowseItem, 1);
		item->j = j;

		/* OPTIONAL */
		if ((scategory = gj_parser_find_by_attr_and_ret (node, (guchar*)"category")) != NULL) {
			if (xmlStrcmp (scategory, (guchar*)"application") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryApplication);
			} else if (xmlStrcmp (scategory, (guchar*)"conference") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryConference);
			} else if (xmlStrcmp (scategory, (guchar*)"headline") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryHeadline);
			} else if (xmlStrcmp (scategory, (guchar*)"keyword") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryKeyword);
			} else if (xmlStrcmp (scategory, (guchar*)"render") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryRender);
			} else if (xmlStrcmp (scategory, (guchar*)"service") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryService);
			} else if (xmlStrcmp (scategory, (guchar*)"user") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryUser);
			} else if (xmlStrcmp (scategory, (guchar*)"validate") == 0) {
				gj_browse_item_set_category (item, GjBrowseCategoryValidate);
			}
		}

		if ((stype = gj_parser_find_by_attr_and_ret (node, (guchar*)"type")) != NULL) {
			if (xmlStrcmp (stype, (guchar*)"bot") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeApplicationBot);
			} else if (xmlStrcmp (stype, (guchar*)"calendar") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeApplicationCalendar);
			} else if (xmlStrcmp (stype, (guchar*)"editor") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeApplicationEditor);
			} else if (xmlStrcmp (stype, (guchar*)"fileserver") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeApplicationFileserver);
			} else if (xmlStrcmp (stype, (guchar*)"game") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeApplicationGame);
			} else if (xmlStrcmp (stype, (guchar*)"whiteboard") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeApplicationWhiteboard);
			} else if (xmlStrcmp (stype, (guchar*)"irc") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeConferenceIRC);
			} else if (xmlStrcmp (stype, (guchar*)"list") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeConferenceList);
			} else if (xmlStrcmp (stype, (guchar*)"private") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeConferencePrivate);
			} else if (xmlStrcmp (stype, (guchar*)"public") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeConferencePublic);
			} else if (xmlStrcmp (stype, (guchar*)"topic") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeConferenceTopic);
			} else if (xmlStrcmp (stype, (guchar*)"url") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeConferenceURL);
			} else if (xmlStrcmp (stype, (guchar*)"logger") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeHeadlineLogger);
			} else if (xmlStrcmp (stype, (guchar*)"notice") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeHeadlineNotice);
			} else if (xmlStrcmp (stype, (guchar*)"rss") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeHeadlineRSS);
			} else if (xmlStrcmp (stype, (guchar*)"stock") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeHeadlineStock);
			} else if (xmlStrcmp (stype, (guchar*)"dictionary") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeKeywordDictionary);
			} else if (xmlStrcmp (stype, (guchar*)"dns") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeKeywordDNS);
			} else if (xmlStrcmp (stype, (guchar*)"software") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeKeywordSoftware);
			} else if (xmlStrcmp (stype, (guchar*)"thesaurus") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeKeywordThesaurus);
			} else if (xmlStrcmp (stype, (guchar*)"web") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeKeywordWeb);
			} else if (xmlStrcmp (stype, (guchar*)"whois") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeKeywordWhoIs);
			} else if (xmlStrcmp (stype, (guchar*)"en2fr") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeRenderEn2Fr);
			} else if (xmlStrcmp (stype, (guchar*)"*2*") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeRenderA2B);
			} else if (xmlStrcmp (stype, (guchar*)"tts") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeRenderTTS);
			} else if (xmlStrcmp (stype, (guchar*)"aim") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceAIM);
			} else if (xmlStrcmp (stype, (guchar*)"icq") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceICQ);
			} else if (xmlStrcmp (stype, (guchar*)"irc") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceIRC);
			} else if (xmlStrcmp (stype, (guchar*)"jabber") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceJabber);
			} else if (xmlStrcmp (stype, (guchar*)"jud") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceJUD);
			} else if (xmlStrcmp (stype, (guchar*)"msn") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceMSN);
			} else if (xmlStrcmp (stype, (guchar*)"pager") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServicePager);
			} else if (xmlStrcmp (stype, (guchar*)"serverlist") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceServerList);
			} else if (xmlStrcmp (stype, (guchar*)"sms") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceSMS);
			} else if (xmlStrcmp (stype, (guchar*)"smtp") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceSMTP);
			} else if (xmlStrcmp (stype, (guchar*)"yahoo") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceYahoo);
			} else if (xmlStrcmp (stype, (guchar*)"client") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeUserClient);
			} else if (xmlStrcmp (stype, (guchar*)"forward") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeUserForward);
			} else if (xmlStrcmp (stype, (guchar*)"inbox") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeUserInbox);
			} else if (xmlStrcmp (stype, (guchar*)"portable") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeUserPortable);
			} else if (xmlStrcmp (stype, (guchar*)"voice") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeUserVoice);
			} else if (xmlStrcmp (stype, (guchar*)"grammar") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeValidateGrammar);
			} else if (xmlStrcmp (stype, (guchar*)"spell") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeValidateSpell);
			} else if (xmlStrcmp (stype, (guchar*)"xml") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeValidateXML);
			}
		}

		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"name")) != NULL) {
			item->name = g_strdup_printf ("%s", (gchar*)tmp);
		}

		g_message ("%s: item jid:'%s', category:'%s', type:'%s', name:'%s'",
			   __FUNCTION__, 
			   gj_jid_get_full (item->j), 
			   (gchar*)scategory, 
			   (gchar*)stype, 
			   item->name);

		if (node->xmlChildrenNode != NULL) {
			GList *ns_list = NULL;
			GList *ns_str_list = NULL;

			gint j = 0;

			/* look up ns's */
			ns_list = gj_parser_find_by_node_on_parent_and_ret_by_list (node, (guchar*)"ns");

			for (j=0; j<g_list_length (ns_list); j++) {
				xmlNodePtr ns = NULL;
	      
				ns = g_list_nth_data (ns_list, j);

				ns_str_list = g_list_append (ns_str_list, g_strdup_printf ("%s", (gchar*)gj_parser_get_content (ns)));
				g_message ("%s: \tns is '%s'", __FUNCTION__, (gchar*)gj_parser_get_content (ns));
			}

			item->namespaces = ns_str_list;
		}

		list = g_list_append (list, item);
	}

	/*
	 * backward compatibility
	 */
	for (i=0; i<g_list_length (service_list); i++) {
		xmlNodePtr node = NULL;
		xmlChar *stype = NULL;
		xmlChar *scategory = NULL;
		xmlChar *tmp = NULL;

		GjBrowseItem item = NULL;
		GjJID j = NULL;

		gboolean found = FALSE;

		node = (xmlNodePtr) g_list_nth_data (service_list, i);
      
		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"jid")) != NULL) {
			j = gj_jid_new ((gchar*)tmp);
		}

		if (!j) {
			continue; 
		}

		/* look for re-occurance */{
			GList *l;
			for (l = list; l && !found; l = l->next) {
				GjJID item_j = NULL;
				item_j = gj_browse_item_get_jid (l->data);

				if (gj_jid_equals_without_resource (j, item_j)) {
					found = TRUE;
				}
			}		 
		}

		if (found) {
			gj_jid_unref (j);
			continue;
		}

		item = g_new0 (struct t_GjBrowseItem, 1);
		item->j = j;

		/* OPTIONAL */
		gj_browse_item_set_category (item, GjBrowseCategoryService);

		if ((stype = gj_parser_find_by_attr_and_ret (node, (guchar*)"type")) != NULL) {
			if (xmlStrcmp (stype, (guchar*)"aim") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceAIM);
			} else if (xmlStrcmp (stype, (guchar*)"icq") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceICQ);
			} else if (xmlStrcmp (stype, (guchar*)"irc") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceIRC);
			} else if (xmlStrcmp (stype, (guchar*)"jabber") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceJabber);
			} else if (xmlStrcmp (stype, (guchar*)"jud") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceJUD);
			} else if (xmlStrcmp (stype, (guchar*)"msn") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceMSN);
			} else if (xmlStrcmp (stype, (guchar*)"pager") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServicePager);
			} else if (xmlStrcmp (stype, (guchar*)"serverlist") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceServerList);
			} else if (xmlStrcmp (stype, (guchar*)"sms") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceSMS);
			} else if (xmlStrcmp (stype, (guchar*)"smtp") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceSMTP);
			} else if (xmlStrcmp (stype, (guchar*)"yahoo") == 0) {
				gj_browse_item_set_type (item, GjBrowseTypeServiceYahoo);
			}
		}

		if ((tmp = gj_parser_find_by_attr_and_ret (node, (guchar*)"name")) != NULL)
			item->name = g_strdup_printf ("%s", (gchar*)tmp);

		g_message ("%s: service jid:'%s', category:'%s', type:'%s', name:'%s'",
			   __FUNCTION__, 
			   gj_jid_get_full (item->j), 
			   (gchar*)scategory, 
			   (gchar*)stype, 
			   item->name);

		if (node->xmlChildrenNode != NULL) {
			GList *ns_list = NULL;
			GList *ns_str_list = NULL;

			gint j = 0;

			/* look up ns's */
			ns_list = gj_parser_find_by_node_on_parent_and_ret_by_list (node, (guchar*)"ns");

			for (j=0; j<g_list_length (ns_list); j++) {
				xmlNodePtr ns = NULL;
	      
				ns = g_list_nth_data (ns_list, j);

				ns_str_list = g_list_append (ns_str_list, g_strdup_printf ("%s", (gchar*)gj_parser_get_content (ns)));
				g_message ("%s: \tns is '%s'", __FUNCTION__, (gchar*)gj_parser_get_content (ns));
			}

			item->namespaces = ns_str_list;
		}

		list = g_list_append (list, item);
	}

	if (item_list) {
		g_list_free (item_list);
	}

	if (service_list) {
		g_list_free (service_list); 
	}
  
	return list;
}


#ifdef __cplusplus
}
#endif
