/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include <string.h>
#include <stdlib.h>

#include <glib.h>

#ifndef G_OS_WIN32
#include <libgnome/gnome-i18n.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gj_main.h"
#include "gj_iq.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_roster.h"
#include "gj_roster_item.h"
#include "gj_jid.h"
#include "gj_connection.h"

#include "gj_gtk_subscription_request.h"
#include "gj_gtk_add_contact.h"
#include "gj_gtk_user_information.h"
#include "gj_gtk_roster.h"
#include "gj_gtk_message.h"
#include "gj_gtk_support.h"


typedef struct {
	GtkWidget *window;

	GtkWidget *treeview;

	GtkWidget *button_view_information;
	GtkWidget *button_send_message;

	GtkWidget *button_deny_all;
	GtkWidget *button_allow_all;

	GtkWidget *button_add_all_to_roster;
	GtkWidget *button_remove_all_from_roster;

	GtkWidget *button_add;

	/* members */
	GjConnection c;
	GjJID connected_jid;
	GjRosterItem ri;

} GjSubscriptionRequestDialog;


enum { 
	COL_SUBSCRIPTION_REQUEST_JID,
	COL_SUBSCRIPTION_REQUEST_RI_ID,
	COL_SUBSCRIPTION_REQUEST_RI_IS_PERMANENT,
	COL_SUBSCRIPTION_REQUEST_NICKNAME,
	COL_SUBSCRIPTION_REQUEST_REASON,
	COL_SUBSCRIPTION_REQUEST_ALLOW,
	COL_SUBSCRIPTION_REQUEST_ADD_TO_ROSTER,
	COL_SUBSCRIPTION_REQUEST_COUNT
};


/* 
 * window functions 
 */
static gboolean gj_gtk_sr_send_allow (GjSubscriptionRequestDialog *dialog, gchar *jid);
static gboolean gj_gtk_sr_send_deny (GjSubscriptionRequestDialog *dialog, gchar *jid);

static gboolean gj_gtk_sr_allow_all (GjSubscriptionRequestDialog *dialog);
static gboolean gj_gtk_sr_deny_all (GjSubscriptionRequestDialog *dialog);

static gboolean gj_gtk_sr_add_all_to_roster (GjSubscriptionRequestDialog *dialog);
static gboolean gj_gtk_sr_remove_all_from_roster (GjSubscriptionRequestDialog *dialog);

/*
 * model functions 
 */
static gboolean gj_gtk_sr_model_setup (GjSubscriptionRequestDialog *dialog);

static gboolean gj_gtk_sr_model_populate_columns (GjSubscriptionRequestDialog *dialog);

static gboolean gj_gtk_sr_model_add (GjSubscriptionRequestDialog *dialog, GjPresence pres);
static gboolean gj_gtk_sr_model_get_selection (GjSubscriptionRequestDialog *dialog, 
					       gchar **jid, 
					       gint *ri_id,
					       gboolean *allow, 
					       gboolean *add_to_roster);

/*
 * gui callbacks
 */
static void on_treeview_allow_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjSubscriptionRequestDialog *dialog);
static void on_treeview_selection_changed (GtkTreeSelection *treeselection, GjSubscriptionRequestDialog *dialog);

static void on_button_view_information_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);
static void on_button_send_message_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);

static void on_button_deny_all_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);
static void on_button_allow_all_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);

static void on_button_add_all_to_roster_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);
static void on_button_remove_all_from_roster_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);

static void on_button_add_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog);

static void on_destroy (GtkWidget *widget, GjSubscriptionRequestDialog *dialog);
static void on_response (GtkDialog *widget, gint response, GjSubscriptionRequestDialog *dialog);


static GjSubscriptionRequestDialog *current_dialog = NULL;


/*
 * window functions
 */
gboolean gj_gtk_sr_load (GjConnection c,
			 GjJID connected_jid, 
			 GjPresence pres)
{
	GladeXML *xml = NULL;
	GjSubscriptionRequestDialog *dialog = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (connected_jid == NULL) {
		g_warning ("%s: connected jid was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	if (current_dialog != NULL) {
		gtk_window_present (GTK_WINDOW (current_dialog->window));

		/* need to add to list here... */
		gj_gtk_sr_model_add (current_dialog, pres);

		return TRUE;
	}

	current_dialog = dialog = g_new0 (GjSubscriptionRequestDialog, 1);

	dialog->c = gj_connection_ref (c);
	dialog->connected_jid = gj_jid_ref (connected_jid);
  
	xml = gj_gtk_glade_get_file (gnome_jabber_get_glade_file (),
				     "subscription_request",
				     NULL,
				     "subscription_request", &dialog->window,
				     "treeview", &dialog->treeview,
				     "button_view_information", &dialog->button_view_information,
				     "button_send_message", &dialog->button_send_message,
				     "button_deny_all", &dialog->button_deny_all,
				     "button_allow_all", &dialog->button_allow_all,
				     "button_add_all_to_roster", &dialog->button_add_all_to_roster,
				     "button_remove_all_from_roster", &dialog->button_remove_all_from_roster,
				     "button_add", &dialog->button_add,
				     NULL);

	gj_gtk_glade_connect (xml,
			      dialog,
			      "subscription_request", "response", on_response,
			      "subscription_request", "destroy", on_destroy,
			      "button_view_information", "clicked", on_button_view_information_clicked,
			      "button_send_message", "clicked", on_button_send_message_clicked,
			      "button_deny_all", "clicked", on_button_deny_all_clicked,
			      "button_allow_all", "clicked", on_button_allow_all_clicked,
			      "button_add_all_to_roster", "clicked", on_button_add_all_to_roster_clicked,
			      "button_remove_all_from_roster", "clicked", on_button_remove_all_from_roster_clicked,
			      "button_add", "clicked", on_button_add_clicked,
			      NULL);

	g_object_unref (xml);

	gdk_window_set_decorations (GDK_WINDOW (dialog->window->window), 
				    GDK_DECOR_BORDER | GDK_DECOR_TITLE | GDK_DECOR_MENU);

	/* set up model */
	gj_gtk_sr_model_setup (dialog);

	/* need to add to list here... */
	gj_gtk_sr_model_add (dialog, pres);

	return TRUE;
}

/*
 * model functions 
 */
static gboolean gj_gtk_sr_model_setup (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeSelection *selection = NULL;

	view = GTK_TREE_VIEW (dialog->treeview);
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));

	/* create tree store */
	store = gtk_list_store_new (COL_SUBSCRIPTION_REQUEST_COUNT,
				    G_TYPE_STRING,   /* jid */
				    G_TYPE_INT,      /* ri id */
				    G_TYPE_BOOLEAN,  /* ri is permanent */
				    G_TYPE_STRING,   /* name */
				    G_TYPE_STRING,   /* reason */
				    G_TYPE_BOOLEAN,  /* allow */
				    G_TYPE_BOOLEAN); /* add to roster */
  
	/* model */
	model = GTK_TREE_MODEL (store);
	gtk_tree_view_set_model (view, model);

	/* selection */
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection), "changed", G_CALLBACK (on_treeview_selection_changed), dialog);

	/* populate columns */
	gj_gtk_sr_model_populate_columns (dialog);

	/* properties */
	gtk_tree_view_set_enable_search (view,FALSE);
	gtk_tree_view_set_search_column (view,COL_SUBSCRIPTION_REQUEST_JID); 
	gtk_tree_view_set_rules_hint (view, FALSE);
	gtk_tree_view_set_headers_visible (view,TRUE);
	gtk_tree_view_set_headers_clickable (view,TRUE);
	gtk_tree_view_columns_autosize (view);
	gtk_tree_view_expand_all (view);

	/* clean up */
	g_object_unref (G_OBJECT (model));

	return TRUE;
}

gboolean gj_gtk_sr_model_populate_columns (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeViewColumn *column = NULL; 
	GtkCellRenderer *renderer = NULL;

	guint col_offset = 0;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));

	/* COL_SUBSCRIPTION_REQUEST_ALLOW */
	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled", G_CALLBACK (on_treeview_allow_cell_toggled), dialog);

	column = gtk_tree_view_column_new_with_attributes (_("Allow"),
							   renderer,
							   "active", COL_SUBSCRIPTION_REQUEST_ALLOW,
							   NULL);

	/* set this column to a fixed sizing (of 50 pixels) */
	gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (column), GTK_TREE_VIEW_COLUMN_FIXED); 
	gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (column), 40); 
	gtk_tree_view_column_set_alignment (GTK_TREE_VIEW_COLUMN (column), 0.5);
	gtk_tree_view_append_column (view, column);

	/* COL_SUBSCRIPTION_REQUEST_JID */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, "Jabber ID",
								  renderer, 
								  "text", COL_SUBSCRIPTION_REQUEST_JID,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_SUBSCRIPTION_REQUEST_JID);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_SUBSCRIPTION_REQUEST_JID);
	gtk_tree_view_column_set_resizable (column,TRUE);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

#if 0
	/* COL_SUBSCRIPTION_REQUEST_NAME */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Nickname"),
								  renderer, 
								  "text", COL_SUBSCRIPTION_REQUEST_NICKNAME,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_SUBSCRIPTION_REQUEST_NICKNAME);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_SUBSCRIPTION_REQUEST_NICKNAME);
	gtk_tree_view_column_set_resizable (column,TRUE);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);
#endif

	/* COL_SUBSCRIPTION_REQUEST_REASON */
	renderer = gtk_cell_renderer_text_new ();
	col_offset = gtk_tree_view_insert_column_with_attributes (view,
								  -1, _("Reason"),
								  renderer, 
								  "text", COL_SUBSCRIPTION_REQUEST_REASON,
								  NULL);
  
	/* set extra information */
	g_object_set_data (G_OBJECT (renderer), "column", (gint*)COL_SUBSCRIPTION_REQUEST_REASON);

	column = gtk_tree_view_get_column (view, col_offset - 1);
	gtk_tree_view_column_set_sort_column_id (column, COL_SUBSCRIPTION_REQUEST_REASON);
	gtk_tree_view_column_set_resizable (column,TRUE);
	gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (column), TRUE);

	return TRUE;
}

static gboolean gj_gtk_sr_model_add (GjSubscriptionRequestDialog *dialog, GjPresence pres)
{
	GtkTreeView *view = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;

	gchar *name = NULL;

	GjRosterItem ri = NULL;
	GjJID j = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	if (pres == NULL) {
		g_warning ("%s: presence was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_presence_get_from_jid (pres);
	if (j == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	ri = gj_rosters_find_item_by_jid (j);
	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	name = gj_jid_get_part_name_new (j);

	g_message ("%s: adding jid:'%s', name:'%s', reason:'%s'", 
		   __FUNCTION__, gj_jid_get_full (j), name, gj_presence_get_status (pres));  

	/* add */
	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,    
			    COL_SUBSCRIPTION_REQUEST_JID, gj_jid_get_without_resource (j),
			    COL_SUBSCRIPTION_REQUEST_RI_ID, gj_roster_item_get_id (ri),
			    COL_SUBSCRIPTION_REQUEST_RI_IS_PERMANENT, gj_roster_item_get_is_permanent (ri),
			    COL_SUBSCRIPTION_REQUEST_NICKNAME, name,
			    COL_SUBSCRIPTION_REQUEST_REASON, gj_presence_get_status (pres),
			    COL_SUBSCRIPTION_REQUEST_ALLOW, TRUE,
			    COL_SUBSCRIPTION_REQUEST_ADD_TO_ROSTER, FALSE,
			    -1);
    
	/* clean up */
	if (name != NULL) g_free (name);

	return TRUE;
}

static gboolean gj_gtk_sr_model_get_selection (GjSubscriptionRequestDialog *dialog, 
					       gchar **jid, 
					       gint *ri_id,
					       gboolean *allow, 
					       gboolean *add_to_roster)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkTreeIter iter;

	gboolean is_selection = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	selection = GTK_TREE_SELECTION (gtk_tree_view_get_selection (view));
  
	if (jid == NULL || ri_id == NULL || allow == NULL || add_to_roster == NULL) {
		g_warning ("%s: jid, ri id, allow, or add_to_roster pointers passed were NULL", __FUNCTION__);
		return FALSE;
	}

	is_selection = gtk_tree_selection_get_selected (selection, &model, &iter);
	if (is_selection == FALSE) {
		return FALSE;
	}

	gtk_tree_model_get (model, &iter, 
			    COL_SUBSCRIPTION_REQUEST_JID, jid,
			    COL_SUBSCRIPTION_REQUEST_RI_ID, ri_id,
			    COL_SUBSCRIPTION_REQUEST_ALLOW, allow,
			    COL_SUBSCRIPTION_REQUEST_ADD_TO_ROSTER, add_to_roster,
			    -1);     

	return TRUE;
}

static gboolean gj_gtk_sr_allow_all (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_list_store_set (store, &iter,    
				    COL_SUBSCRIPTION_REQUEST_ALLOW, TRUE,
				    -1);
	}
  
	return TRUE;
}

static gboolean gj_gtk_sr_deny_all (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_list_store_set (store, &iter,    
				    COL_SUBSCRIPTION_REQUEST_ALLOW, FALSE,
				    -1);
	}
  
	return TRUE;
}

static gboolean gj_gtk_sr_add_all_to_roster (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_list_store_set (store, &iter,    
				    COL_SUBSCRIPTION_REQUEST_ADD_TO_ROSTER, TRUE,
				    -1);
	}
  
	return TRUE;
}

static gboolean gj_gtk_sr_remove_all_from_roster (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_list_store_set (store, &iter,    
				    COL_SUBSCRIPTION_REQUEST_ADD_TO_ROSTER, FALSE,
				    -1);
	}
  
	return TRUE;
}

static gboolean gj_gtk_sr_send_allow (GjSubscriptionRequestDialog *dialog, gchar *jid)
{
	GjJID j = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_jid_new (jid);
  
	g_message ("%s: allowing subscription for jid:'%s'", __FUNCTION__, jid);
	gj_presence_send_to (dialog->c,
			     j, 
			     TRUE, 
			     GjPresenceTypeSubscribed, 
			     GjPresenceShowEnd, 
			     NULL, 
			     0);

	/* NOTE: at this point, we want to use the contact_add dialog, and 
           start at the second stage where the name and groups are 
	   specified... */

	if (j != NULL) {
		gj_jid_unref (j); 
	}

	return TRUE;
}

static gboolean gj_gtk_sr_send_deny (GjSubscriptionRequestDialog *dialog, gchar *jid)
{
	GjJID j = NULL;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	if (jid == NULL) {
		g_warning ("%s: jid was NULL", __FUNCTION__);
		return FALSE;
	}

	j = gj_jid_new (jid);

	g_message ("%s: denying subscription for jid:'%s'", __FUNCTION__, jid);
	gj_presence_send_to (dialog->c,
			     j, 
			     FALSE, 
			     GjPresenceTypeUnSubscribed, 
			     GjPresenceShowEnd, 
			     NULL, 
			     0);

	/* remove from roster */
	gj_gtk_roster_model_remove (dialog->ri);

	if (j != NULL){
		gj_jid_unref (j);
	}

	return TRUE;
}

static gboolean gj_gtk_sr_ok (GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
  
	gboolean valid = FALSE;

	if (dialog == NULL) {
		g_warning ("%s: dialog was NULL", __FUNCTION__);
		return FALSE;
	}

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
  
	/* Get the first iter in the list */
	valid = gtk_tree_model_get_iter_first (model, &iter);

	for (; valid == TRUE; valid = gtk_tree_model_iter_next (model, &iter)) {
		gchar *jid = NULL;
		gboolean allow = FALSE;
		gboolean add_to_roster = FALSE;

		gtk_tree_model_get (model, &iter,   
				    COL_SUBSCRIPTION_REQUEST_JID, &jid,
				    COL_SUBSCRIPTION_REQUEST_ALLOW, &allow,
				    COL_SUBSCRIPTION_REQUEST_ADD_TO_ROSTER, &add_to_roster,
				    -1);

		if (jid == NULL) {
			continue;
		}

		if (allow == TRUE) {
			gj_gtk_sr_send_allow (dialog, jid);
		} else {
			gj_gtk_sr_send_deny (dialog, jid); 
		}

#if 0
		if (add_to_roster == TRUE) {
			gj_gtk_ac_load (jid); 
		}
#endif

		/* clean up */
		g_free (jid);
	}
  
	return TRUE;
}

/*
 * GUI events
 */
static void on_treeview_allow_cell_toggled (GtkCellRendererToggle *cell, gchar *path_string, GjSubscriptionRequestDialog *dialog)
{
	GtkTreeView *view = NULL;
	GtkTreeModel *model = NULL;
	GtkListStore *store = NULL;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;

	gboolean enabled = FALSE;
	/*   gboolean add_to_roster = FALSE; */

	view = GTK_TREE_VIEW (dialog->treeview);
	model = GTK_TREE_MODEL (gtk_tree_view_get_model (view));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	path = gtk_tree_path_new_from_string (path_string);

	/* get toggled iter */
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, 
			    COL_SUBSCRIPTION_REQUEST_ALLOW, &enabled, 
			    -1);

	/* do something */
	enabled ^= 1;

	/* set new value */
	gtk_list_store_set (store, &iter, 
			    COL_SUBSCRIPTION_REQUEST_ALLOW, enabled, 
			    -1);

	/* clean up */
	gtk_tree_path_free (path);

}

static void on_treeview_selection_changed (GtkTreeSelection *treeselection, GjSubscriptionRequestDialog *dialog)
{
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;

	if (gtk_tree_selection_get_selected (treeselection, &model, &iter) == FALSE) {
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_view_information), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_send_message), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_add), FALSE);
	} else {
		gboolean permanent = FALSE;

		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_view_information), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_send_message), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->button_add), TRUE);

		gtk_tree_model_get (model, &iter,
				    COL_SUBSCRIPTION_REQUEST_RI_IS_PERMANENT, &permanent,
				    -1);

		if (permanent == FALSE) {
			gtk_widget_show (GTK_WIDGET (dialog->button_add));
		} else {
			gtk_widget_hide (GTK_WIDGET (dialog->button_add));
		}
	}
}

static void on_button_view_information_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog) {
	gchar *jid = NULL;
	gint ri_id = 0;
	gboolean allow = FALSE;
	gboolean add_to_roster = FALSE;

	GjRosterItem ri = NULL;

	if (gj_gtk_sr_model_get_selection (dialog, &jid, &ri_id, &allow, &add_to_roster) == FALSE) {
		return;
	}

	/* clean up */
	g_free (jid);

	ri = gj_rosters_find_item_by_id (ri_id);

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return;
	}

	/* load user information dialog */
	gj_gtk_ui_load (dialog->c, ri);
}

static void on_button_send_message_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog)
{
	gchar *jid = NULL;
	gboolean allow = FALSE;
	gboolean add_to_roster = FALSE;
	gint ri_id = 0;

	GjMessageWindow *window = NULL;

	if (gj_gtk_sr_model_get_selection (dialog, &jid, &ri_id, &allow, &add_to_roster) == FALSE) {
		return;
	}

	/* send message */
	window = gj_gtk_message_send_new_by_jid (dialog->c,
						 dialog->connected_jid, 
						 jid);

	if (window) {
		gj_gtk_message_set_transient_for_window (window, GTK_WINDOW (dialog->window));
	}

	/* if not error ? */
	g_free (jid);
}

static void on_button_allow_all_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog)
{
	gj_gtk_sr_allow_all (dialog);
}

static void on_button_deny_all_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog)
{
	gj_gtk_sr_deny_all (dialog);
}

static void on_button_add_all_to_roster_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog)
{
	gj_gtk_sr_add_all_to_roster (dialog);
}

static void on_button_remove_all_from_roster_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog)
{
	gj_gtk_sr_remove_all_from_roster (dialog);
}

static void on_button_add_clicked (GtkButton *button, GjSubscriptionRequestDialog *dialog)
{
	gchar *jid = NULL;
	gboolean allow = FALSE;
	gboolean add_to_roster = FALSE;
	gint ri_id = 0;

	GjRosterItem ri = NULL;

	if (gj_gtk_sr_model_get_selection (dialog, &jid, &ri_id, &allow, &add_to_roster) == FALSE) {
		return;
	}

	/* clean up */
	g_free (jid);

	ri = gj_rosters_find_item_by_id (ri_id);

	if (ri == NULL) {
		g_warning ("%s: roster item was NULL", __FUNCTION__);
		return;
	}

	gj_gtk_ac_load (dialog->c,
			gj_roster_item_get_roster (ri), 
			ri);  
}

static void on_destroy (GtkWidget *widget, GjSubscriptionRequestDialog *dialog)
{
	current_dialog = NULL;

	g_free (dialog);
}

static void on_response (GtkDialog *widget, gint response, GjSubscriptionRequestDialog *dialog)
{
	if (dialog == NULL)
		return;

	if (response == GTK_RESPONSE_OK) {
		gj_gtk_sr_ok (dialog);
	}

	gj_connection_unref (dialog->c);
	gj_jid_unref (dialog->connected_jid);

	gtk_widget_destroy (dialog->window);
}

#ifdef __cplusplus
}
#endif
