/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifndef __gj_gtk_support_h
#define __gj_gtk_support_h

#include "gj_typedefs.h"


enum {
	gj_presence_menu_available = 1,
	gj_presence_menu_chat,
	gj_presence_menu_away,
	gj_presence_menu_xa,
	gj_presence_menu_dnd,
	gj_presence_menu_invisible,
	gj_presence_menu_status

} gj_presence_menu_ids;


enum {
	gj_presence_preset_menu_custom = 1,
	gj_presence_preset_menu_idle,
	gj_presence_preset_menu_around,
	gj_presence_preset_menu_eating,
	gj_presence_preset_menu_gaming,
	gj_presence_preset_menu_out,
	gj_presence_preset_menu_showering,
	gj_presence_preset_menu_sleeping,
	gj_presence_preset_menu_working

} gj_presence_preset_menu_ids;


typedef void (*GjMenuCallback) (GtkMenuItem *menu_item, gpointer user_data); 


/* external tools - browsing/sound */
gboolean gj_gtk_open_url (const gchar *url);
gboolean gj_gtk_sound_play (const gchar *filename);

/* string functions */
gchar *gj_gtk_filter_number (const gchar *number);
gboolean gj_gtk_find_in_str (const gchar *needle, const gchar *haystack);

/* images/animations */
GtkWidget *gj_gtk_image_new_from_file (const gchar *filename);
GdkPixbuf *gj_gtk_pixbuf_new_from_file (const gchar *filename);
GdkPixbuf *gj_gtk_pixbuf_new_from_stock (const gchar *stock_item);
GdkPixbufAnimation *gj_gtk_animation_new_from_file (const gchar *filename);

#ifdef G_OS_WIN32
# include <windows.h>
HICON gj_gtk_pixbuf_to_hicon (GdkPixbuf *pb);
HICON gj_gtk_pixmap_to_hicon (GdkPixmap *pixmap, GdkBitmap *mask);
#endif

/* labels/entrys with args  */
void gj_gtk_set_label_with_args (GtkWidget *widget, const gchar *fmt, ...);
void gj_gtk_set_entry_with_args (GtkWidget *widget, const gchar *fmt, ...);
void gj_gtk_set_progress_bar_label_with_args (GtkWidget *widget, const gchar *fmt, ...);
void gj_gtk_set_window_title_with_args (GtkWidget *widget, const gchar *fmt, ...);

/* glade */
GladeXML *gj_gtk_glade_get_file (const gchar *filename,
				 const gchar *root,
				 const gchar *domain,
				 const gchar *first_widget, 
				 ...);

void gj_gtk_glade_connect (GladeXML *gui,
			   gpointer user_data,
			   gchar *first_widget, 
			   ...);


/* textview/textbuffer for URL handling */
gboolean gj_gtk_textview_handle_motion_notify_event (GtkWidget *widget,
						     GdkEventMotion *event, 
						     gpointer user_data);

gboolean gj_gtk_textview_handle_button_press_event (GtkWidget *widget, 
						    GdkEventButton *event, 
						    gpointer user_data);

void gj_gtk_textbuffer_handle_changed (GtkTextBuffer *textbuffer, gpointer user_data);

gboolean gj_gtk_textbuffer_insert (GtkTextBuffer *buffer, 
				   GtkTextIter *iter,
				   const gchar *tag,
				   const gchar *message);

gboolean gj_gtk_textbuffer_insert_with_emote_icons (GtkTextBuffer *buffer, 
						    GtkTextIter *iter,
						    const gchar *tag,
						    const gchar *message);


/* presence/message image, text and menu helper functions */
const gchar *gj_gtk_presence_to_stock_id (GjPresenceType type, GjPresenceShow show);
GdkPixbuf *gj_gtk_presence_to_pixbuf_new (GjPresenceType type, GjPresenceShow show);
gchar *gj_gtk_presence_to_show_text_new (GjPresenceType type, GjPresenceShow show);
gchar *gj_gtk_presence_to_status_text_new (GjPresenceType type, GjPresenceShow show);

const gchar *gj_gtk_message_to_stock_id (GjMessageType type);
GdkPixbuf *gj_gtk_message_to_pixbuf_new (GjMessageType type);

/*
 * menus
 */ 


GtkWidget *gj_gtk_presence_menu_new (GjMenuCallback cb, gpointer user_data);
GtkWidget *gj_gtk_presence_preset_menu_new (GjMenuCallback cb);
GtkWidget *gj_gtk_emote_menu_new (GjMenuCallback cb, gpointer user_data);

void gj_gtk_option_menu_setup (GtkWidget *option_menu, GCallback cb, gpointer user_data, gconstpointer first_str, ...);

/*
 * higified dialog
 */
GtkWidget *gj_gtk_hig_dialog_new (GtkWindow *parent,
				  GtkDialogFlags flags,
				  GtkMessageType type,
				  GtkButtonsType buttons,
				  const gchar *header,
				  const gchar *messagefmt,
				  ...);

GtkWidget *gj_gtk_hig_progress_new (GtkWindow *parent,
				    GtkDialogFlags flags,
				    GCallback destroy_cb,
				    gpointer user_data,
				    const gchar *title,
				    const gchar *header,
				    const gchar *messagefmt,
				    ...);

gboolean gj_gtk_show_error (const gchar *action, gint error_code, const gchar *error_reason);
gboolean gj_gtk_show_success (const gchar *action);

/* redrawing */
gboolean gj_gtk_widget_redraw_when_idle (GtkWidget *widget);


#endif

#ifdef __cplusplus
}
#endif



