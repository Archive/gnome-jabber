/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2003      Martyn Russell <ginxd@btopenworld.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * $RCSfile$
 *
 * $Author$
 * $Date$
 * 
 * $Revision$
 *
 */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#include "gj_register.h"
#include "gj_parser.h"
#include "gj_presence.h"
#include "gj_message.h"
#include "gj_iq.h"
#include "gj_iq_requests.h"
#include "gj_connection.h"


struct t_registration {
	gchar *id;
	gchar *to;
	gchar *from;

	gchar *instructions;

	gchar *username;
	gchar *password;
	gchar *email;

	gboolean require_username;
	gboolean require_password;
	gboolean require_email;
	gboolean require_nickname;

	gchar *key;

	gpointer user_data;

	GjConnection c;

	GjRegistrationCallback cb; 
};


static void gj_register_init ();

/* item functions */
static GjRegistration gj_register_new (GjConnection c, const gchar *server);
static void gj_register_free (GjRegistration reg);

/* find */
static GjRegistration gj_register_find_by_id (const gchar *id);
static GjRegistration gj_register_find_by_to (const gchar *to);
static GjRegistration gj_register_find_by_from (const gchar *from);

/* parsing functions */
static void gj_register_requirements_cb (GjInfoquery iq, GjInfoquery iq_related, GjRegistration reg);
static void gj_register_request_cb (GjInfoquery iq, GjInfoquery iq_related, GjRegistration reg);
static void gj_register_remove_cb (GjInfoquery iq, GjInfoquery iq_related, GjRegistration reg);

static GjRegistration gj_register_parse (GjConnection c, xmlNodePtr parent);


static GList *register_list = NULL;


static GjRegistration gj_register_new (GjConnection c, const gchar *server)
{
	GjRegistration reg = NULL;

	if (!c) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return NULL;
	}

	reg = g_new0 (struct t_registration, 1);

	reg->c = gj_connection_ref (c);

	if (server) {
		reg->to = g_strdup (server);
	}

	register_list = g_list_append (register_list, reg);
  
	return reg;  
}

static void gj_register_free (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return;
	}

	/* free mem */
	register_list = g_list_remove (register_list, reg);

	gj_connection_unref (reg->c);

	g_free (reg->id);

	g_free (reg->to);
	g_free (reg->from);

	g_free (reg->instructions);
	g_free (reg->username);
	g_free (reg->password);
	g_free (reg->email);

	g_free (reg->key);

	g_free (reg);
}

/*
 * find 
 */
static GjRegistration gj_register_find_by_id (const gchar *id)
{
	GList *l = NULL;

	if (id == NULL) {
		g_warning ("%s: id was NULL", __FUNCTION__);
		return NULL;
	}

	for (l = register_list; l; l = l->next) {
		GjRegistration reg = (GjRegistration) l->data;

		if (g_strcasecmp (reg->id, id) == 0) {
			return reg;
		}
	}

	return NULL;
}

static GjRegistration gj_register_find_by_to (const gchar *to)
{
	GList *l = NULL;
    
	if (to == NULL) {
		g_warning ("%s: to was NULL", __FUNCTION__);
		return NULL;
	}

	for (l = register_list; l; l = l->next) {
		GjRegistration reg = (GjRegistration) l->data;

		if (g_strcasecmp (reg->to, to) == 0) {
			return reg;
		}
	}

	return NULL;
}

static GjRegistration gj_register_find_by_from (const gchar *from)
{
	GList *l = NULL;

	if (from == NULL) {
		g_warning ("%s: from was NULL", __FUNCTION__);
		return NULL;
	}

	for (l = register_list; l; l = l->next) {
		GjRegistration reg = (GjRegistration) l->data;

		if (g_strcasecmp (reg->from, from) == 0) {
			return reg;
		}
	}

	return NULL;
}

/*
 * member functions  
 */

/* gets */
static const gchar *gj_register_get_id (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->id;
}

GjConnection gj_register_get_connection (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->c;
}

const gchar *gj_register_get_to (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->to;
}

const gchar *gj_register_get_from (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->from;
}

const gchar *gj_register_get_instructions (GjRegistration reg) {
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->instructions;
}

static const gchar *gj_register_get_password (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->password;
}

const gchar *gj_register_get_email (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->email;
}

gboolean gj_register_get_require_username (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return FALSE;
	}

	return reg->require_username;
}

gboolean gj_register_get_require_password (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return FALSE;
	}

	return reg->require_password;
}

gboolean gj_register_get_require_email (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return FALSE;
	}

	return reg->require_email;
}

gboolean gj_register_get_require_nickname (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return FALSE;
	}

	return reg->require_nickname;
}

const gchar *gj_register_get_key (GjRegistration reg)
{
	if (reg == NULL) {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return NULL;
	}

	return reg->key;
}

gboolean gj_register_requirements (GjConnection c, 
				   const gchar *server, 
				   GjRegistrationCallback cb, 
				   gpointer user_data)
{
	GjRegistration reg = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	reg = gj_register_new (c, server);
  
	reg->cb = cb;
	reg->user_data = user_data;

	/* do request */
	gj_iq_request_register_requirements (c, server, (GjInfoqueryCallback)gj_register_requirements_cb, reg);

	return TRUE;
}

static void gj_register_requirements_cb (GjInfoquery iq, GjInfoquery iq_related, GjRegistration reg)
{
	GjConnection c = NULL;
	GjRegistrationCallback cb = NULL;
  
	gpointer user_data = NULL;

	gint error_code = 0;
	gchar *error_reason = NULL;

	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	c = gj_iq_get_connection (iq);

	if (reg) {
		cb = (GjRegistrationCallback) reg->cb;
		user_data = reg->user_data;

		gj_register_free (reg);
	} else {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return;
	}
  
	if (!cb) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		error_code = atoi ((gchar*)errorCode);
		error_reason = g_strdup ((gchar*)errorMessage);
	}

	/* parse list */
	if ((reg = gj_register_parse (c, node)) == NULL) {
		g_warning ("%s: could not parse xml node", __FUNCTION__);
		return;
	}

	/* return list */
	(cb) (reg, error_code, error_reason, user_data);

	/* clean up */
	g_free (error_reason);
}


gboolean gj_register_request (GjConnection c,
			      const gchar *server, 
			      const gchar *key, 
			      const gchar *username, 
			      const gchar *password, 
			      const gchar *email, 
			      const gchar *nickname,
			      GjRegistrationCallback cb, 
			      gpointer user_data)
{
	GjRegistration reg = NULL;

	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	reg = gj_register_new (c, server);

	reg->cb = cb;
	reg->user_data = user_data;

	/* do request */
	gj_iq_request_register (c, server, (GjInfoqueryCallback) gj_register_request_cb, 
				key, username, password, email, nickname, reg);

	return TRUE;
}


static void gj_register_request_cb (GjInfoquery iq, GjInfoquery iq_related, GjRegistration reg)
{
	GjRegistrationCallback cb = NULL;
  
	gpointer user_data = NULL;

	gint error_code = 0;
	gchar *error_reason = NULL;

	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	if (!iq) {
		return;
	}

	if (reg) {
		cb = (GjRegistrationCallback) reg->cb;
		user_data = reg->user_data;
      
		gj_register_free (reg);
	} else {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return;
	}
  
	if (!cb) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		error_code = atoi ((gchar*)errorCode);
		error_reason = g_strdup ((gchar*)errorMessage);
	}

	/* parse list */
	if ((reg = gj_register_parse (gj_register_get_connection (reg), node)) == NULL) {
		g_warning ("%s: could not parse xml node", __FUNCTION__);
		return;
	}

	/* return list */
	(cb) (reg, error_code, error_reason, user_data);

	/* clean up */
	g_free (error_reason);
}

static void gj_register_remove_cb (GjInfoquery iq, GjInfoquery iq_related, GjRegistration reg)
{
	GjRegistrationCallback cb = NULL;

	gpointer user_data = NULL;

	gint error_code = 0;
	gchar *error_reason = NULL;

	xmlChar *type = NULL;
	xmlNodePtr node = NULL;

	if (!iq) {
		return;
	}

	if (reg) {
		cb = (GjRegistrationCallback) reg->cb;
		user_data = reg->user_data;

		gj_register_free (reg);
	} else {
		g_warning ("%s: registration was NULL", __FUNCTION__);
		return;
	}
  
	if (!cb) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return;
	}

	node = gj_iq_get_xml (iq);
	type = gj_parser_find_by_attr_and_ret (node, (guchar*)"type");

	if (type != NULL && xmlStrcmp (type, (guchar*)"error") == 0) {
		xmlChar *errorCode = NULL;
		xmlChar *errorMessage = NULL;

		if ((node = gj_parser_find_by_node (node, (guchar*)"error")) == NULL) {
			g_warning ("%s: failed to find node 'error'", __FUNCTION__);
			return;
		}

		errorCode = gj_parser_find_by_attr_and_ret (node, (guchar*)"code");
		errorMessage = gj_parser_find_by_node_and_ret (node, (guchar*)"error");

		error_code = atoi ((gchar*)errorCode);
		error_reason = g_strdup ((gchar*)errorMessage);
	}

	/* parse list */
	if ((reg = gj_register_parse (gj_iq_get_connection (iq), node)) == NULL) {
		g_warning ("%s: could not parse xml node", __FUNCTION__);
		return;
	}

	/* return list */
	(cb) (reg, error_code, error_reason, user_data);

	/* clean up */
	g_free (error_reason);
}

gboolean gj_register_remove (GjConnection c, 
			     const gchar *server, 
			     const gchar *key, 
			     const gchar *username, 
			     const gchar *password, 
			     const gchar *email, 
			     const gchar *nickname,
			     GjRegistrationCallback cb, 
			     gpointer user_data)
{
	GjRegistration reg = NULL;

	if (c == NULL) {
		g_warning ("%s: connection was NULL", __FUNCTION__);
		return FALSE;
	}

	if (server == NULL) {
		g_warning ("%s: server was NULL", __FUNCTION__);
		return FALSE;
	}

	if (cb == NULL) {
		g_warning ("%s: callback was NULL", __FUNCTION__);
		return FALSE;
	}

	reg = gj_register_new (c, server);

	reg->cb = cb;
	reg->user_data = user_data;

	/* do request */
	gj_iq_request_unregister (c, server, key, (GjInfoqueryCallback)gj_register_remove_cb, reg);

	return TRUE;
}

static GjRegistration gj_register_parse (GjConnection c, xmlNodePtr parent)
{
	xmlChar *tmp = NULL;
	GjRegistration reg = NULL;

	if (parent == NULL) {
		g_warning ("%s: xml node was NULL", __FUNCTION__);
		return NULL;
	}

	/* create new roster item */
	if ((reg = gj_register_new (c, NULL)) == NULL) {
		g_warning ("%s: could not create registration", __FUNCTION__);
		return NULL;
	}

	/* get important information */
	if ((tmp = gj_parser_find_by_attr_and_ret (parent, (guchar*)"id")) != NULL) {
		reg->id = g_strdup_printf ("%s", (gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (parent, (guchar*)"to")) != NULL) {
		reg->to = g_strdup_printf ("%s", (gchar*)tmp);
	}

	if ((tmp = gj_parser_find_by_attr_and_ret (parent, (guchar*)"from")) != NULL) {
		reg->from = g_strdup_printf ("%s", (gchar*)tmp);
	}
  
	/* get instructions */
	if ((tmp = gj_parser_find_by_node_and_ret (parent, (guchar*)"instructions")) != NULL) {
		reg->instructions = g_strdup_printf ("%s", (gchar*)tmp);
	}

	/* get user/pass... */
	if (gj_parser_find_by_node (parent, (guchar*)"username") != NULL) {
		reg->require_username = TRUE;
	}

	if (gj_parser_find_by_node (parent, (guchar*)"password") != NULL) {
		reg->require_password = TRUE;
	}

	if (gj_parser_find_by_node (parent, (guchar*)"email") != NULL) {
		reg->require_email = TRUE;
	}

	if (gj_parser_find_by_node (parent, (guchar*)"nick") != NULL) {
		reg->require_nickname = TRUE;
	}

	if ((tmp = gj_parser_find_by_node_and_ret (parent, (guchar*)"key")) != NULL) {
		reg->key = g_strdup_printf ("%s", (gchar*)tmp);
	}

	return reg;
}

#ifdef __cplusplus
}
#endif
